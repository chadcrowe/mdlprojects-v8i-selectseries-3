/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/vba2mdl/vba2mdl.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <string.h>
#include    <stdio.h>
#include    <widechar.h>
#include    "vba2mdl.h"
#include    "msvba.fdf"
#include    <mscexpr.fdf>
#include    <msdialog.fdf>
#include    <msparse.fdf>
#include    <msoutput.fdf>
#include    <msrsrc.fdf>
#include    <mssystem.fdf>
#include    <toolsubs.h>

#include    "vba2mdlcmd.h"
/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   External variables							|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Published Global variables						|
|									|
+----------------------------------------------------------------------*/

Public Mdl2VbaInfo mdl2VbaInfo;	 // Shared structure between MDL and VBA

/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/


/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/

Private SymbolSet *pSymbolSet;	// pointer to SymbolSet area
Private RscFileHandle	rscFileH;

/*----------------------------------------------------------------------+
|									|
| name		mdl2VbaInfo_printStructure				|
|
|          Prints out shared structure, called from VBA
|									|
| author	BentleySystems				08/01		|
|									|
+----------------------------------------------------------------------*/
extern "C" void	mdl2VbaInfo_printStructure
(

)
    {
    printf ("mdl2VbaInfo structure from MDL\n");
    printf ("  .asciiString = %s\n", mdl2VbaInfo.asciiString);
    printf ("  .wideString = %S\n", mdl2VbaInfo.wideString);
    printf ("  .s1 = %d\n", mdl2VbaInfo.s1);
    printf ("  .d1 = %f\n", mdl2VbaInfo.d1);
    printf ("  .i1 = %d\n", mdl2VbaInfo.i1);
    }

/*----------------------------------------------------------------------+
|									|
| name		mdl2VbaInfo_function					|
|
|           Prints out argument list of function, called from VBA
|           Returns second(int) value + 3
|									|
| author	BentleySystems     				08/01		|
|									|
+----------------------------------------------------------------------*/
extern "C" int     mdl2VbaInfo_function
(
double		 d,
int		 i,
short		 s
)
    {
    printf ("d = %f\n", d);
    printf ("i = %d\n", i);
    printf ("s = %d\n", s);

    mdl2VbaInfo.d1 = d;
    mdl2VbaInfo.i1 = i;
    mdl2VbaInfo.s1 = s;

    return  i + 3;
    }

/*----------------------------------------------------------------------+
|									|
| name		vba2mdlCommand						|
|
|           Copies string from Keyin in VBA to shared structure string
|									|
| author	BentleySystems     				08/01		|
|									|
+----------------------------------------------------------------------*/
DLLEXPORT void    vba2mdlCommand  
(
char		*pInput
)//cmdNumber      CMD_VBA2MDL_COMMAND
    {
    strcpy (mdl2VbaInfo.asciiString, pInput);
    }

/*----------------------------------------------------------------------+
|									|
| name		runVbaMacro						|
|									|
| author	BentleySystems     				08/01		|
|									|
+----------------------------------------------------------------------*/
DLLEXPORT void    runVbaMacro  
(
char		*unparsed
)//cmdNumber      CMD_VBA2MDL_EXECUTE
    {
	long status;

// Initialize shared structure members
    mdl2VbaInfo.d1 = -1234.567;
    mdl2VbaInfo.i1 = -345;
    mdl2VbaInfo.s1 = 27; 
    strcpy (mdl2VbaInfo.asciiString, "INITIAL ASCII STRING");
    wcscpy (mdl2VbaInfo.wideString, L"INITIAL WIDE CHARACTERSTRING");

// Execute VBA command
    status = mdlVBA_runMacro ("mdl2vba", "vba2mdl", "Vba2MdlTest");

    if  (mdl2VbaInfo.d1 != -1234.567)
	mdlDialog_openAlert ("d1 was not restored");

    if (mdl2VbaInfo.i1 != -345)
	mdlDialog_openAlert ("i1 was not restored");

    if (mdl2VbaInfo.s1 != 27)
	mdlDialog_openAlert ("s1 was not restored");
	
    if (strcmp (mdl2VbaInfo.asciiString, "INITIAL ASCII STRING"))
	mdlDialog_openAlert ("asciiString was not restored");

    if (wcscmp (mdl2VbaInfo.wideString, L"INITIAL WIDE CHARACTERSTRING"))
	mdlDialog_openAlert ("wideString was not restored");
    }

/*ff Major Public Code Section */
/*----------------------------------------------------------------------+
|									|
|   Major Public Code Section						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BentleySystems     				08/01		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    CType       *pType;
    mdlResource_openFile (&rscFileH, NULL, 0);
    
    Private MdlCommandNumber  commandNumbers [] =
    {
    {runVbaMacro,CMD_VBA2MDL_EXECUTE},
    {vba2mdlCommand ,CMD_VBA2MDL_COMMAND},
    0,
    };
    
    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);


    mdlParse_loadCommandTable (NULL);

    //  The VBA interface looks for symbol sets marked VISIBILITY_CALCULATOR
    //  or VISIBILITY_DIALOG_BOX
    pSymbolSet = mdlCExpression_initializeSet (VISIBILITY_CALCULATOR, 0, FALSE);


    pType = mdlCExpression_typeFromRsc (pSymbolSet, "mdl2VbaInfo", 0);

    if  (pType == NULL)
    	{
	char     message [100];

	sprintf (message, "Could not publish the type: %d", errno);
	mdlDialog_openAlert (message);
    	}

    mdlCExpression_symbolPublish (pSymbolSet, "mdl2VbaInfo", SYMBOL_CLASS_VAR,
			pType, (void *)&mdl2VbaInfo);

    mdlCExpression_symbolPublish (pSymbolSet, "mdl2VbaInfo_function", 
				      SYMBOL_CLASS_FUNCTION, 0,
				      mdl2VbaInfo_function);

    mdlCExpression_symbolPublish (pSymbolSet, "mdl2VbaInfo_printStructure", 
				      SYMBOL_CLASS_FUNCTION, 0,
				      mdl2VbaInfo_printStructure);
    return  SUCCESS;
    }
