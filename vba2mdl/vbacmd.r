/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/vba2mdl/vbacmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   vbacmd.r - Command Table hierarchy 	    	    	    	|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>


/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_VBA2MDL        1

DllMdlApp DLLAPP_VBA2MDL =
    {
    "VBA2MDL", "vba2mdl"          // taskid, dllName
    }




/*----------------------------------------------------------------------+
|									|
|   Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE	    0
#define	CT_MAIN	    1
#define CT_COMMANDS 2

/*----------------------------------------------------------------------+
|                                                                       |
|   Main (Root) command word table					|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_MAIN =
{ 
    { 1,  CT_COMMANDS,	PLACEMENT,	REQ,		"VBA2MDL" }, 
};


Table	CT_COMMANDS =
{ 
    { 1,  CT_NONE,	INHERIT,	NONE,		"execute" }, 
    { 2,  CT_NONE,	INHERIT,	NONE,		"command" }, 
};
