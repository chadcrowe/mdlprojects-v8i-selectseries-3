/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/vba2mdl/vba2mdl.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#if !defined (__mdl2vba__)
#define	__mdl2vba__

#include    <basedefs.h>
#include    <basetype.h>
/*----------------------------------------------------------------------+
|									|
|  Defines								|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|  Typedefs								|
|									|
+----------------------------------------------------------------------*/

typedef struct mdl2VbaInfo
    {
    char	    asciiString [200];
    MSWChar	    wideString [200];
    short	    s1;
    double	    d1;
    long	    i1;
    } Mdl2VbaInfo;

#endif
