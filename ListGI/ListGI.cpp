/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ListGI/ListGI.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   ListGI  $
|   $Revision: 1.1.12.1 $
|       $Date: 2013/07/01 20:35:35 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ListGI - ListGI source code. |
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|                                                                       |
|   Function -                                                          |
|                                                                       |
|   listGI.cpp -- Example of dialog tree structure and element preview  |
|                                                                       |
|                                                                       |
|   This example application shows how to use tree struct               |
|   to create a specific dialog.     |
|   The application also allows user to make variable line temperlates  |
|   and show preview of the customized element.                         |
|   User can also place the specific element by pushing done button.    |
|                                                                       |
|       - - - - - - - - - - - - - - - - - - - - - - - - - - - - -       |
|                                                                       |
|   Public Routine Summary -                                                            |
|                                                                       |
|   openDialogFunction - Open a dialog to allow user create different   |
|       element                     |
|       setListView - show the information of current dgn file              |

|       main - main entry point                                                                 |
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#define __NORECTANGLE__     /* so we can include windows.h */
#include    <windows.h>        /* for GetPrivateProfileString */
#include    <math.h>
#undef  __NORECTANGLE__
#include    <malloc.h>
#include    <direct.h>  
#include    <rtypes.h>
#include    <system.h>
#include    <cexpr.h>
#include    <cmdlist.h>
#include    <cmdclass.h>

#include    <msstrlst.h >
#include    <mdlerrs.h>
#include    <msscancrit.h>
#include        <toolsubs.h>
#include    <mselems.h>
#include    <msrmgr.h>

#include    <MstnColorMap.h>

//scan code
#include        <elementref.h>

#include    <mscexpr.fdf>
#include    <msrsrc.fdf>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <ditemlib.fdf>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>
#include    <listModel.fdf>
#include    <msscancrit.fdf>
#include    <mswrkdgn.fdf>
#include    <treemodel.fdf>
#include    <msvar.fdf>   /* contains the extern declaration of the global variables*/
#include    <modelindex.fdf>
#include    <mswindow.fdf>
#include    <msrmatrx.fdf >
#include    <mssystem.fdf>
#include    <msparse.fdf>
#include    <msvec.fdf>
#include    <miscilib.fdf >
#include    <msritem.fdf>
#include    <rdbmslib.fdf>
//locate code
#include        <msdependency.fdf>
#include        <msassoc.fdf>
#include        <msmisc.fdf>
#include        <mslocate.fdf>
#include        <msstate.fdf>
#include        <msoutput.fdf>
#include    <mskisolid.fdf>
//place command
#include        <mstmatrx.fdf>
char    *g_str;

#include "ListGICmd.h"
#include "ListGI.h"


USING_NAMESPACE_BENTLEY;
USING_NAMESPACE_BENTLEY_USTN;
USING_NAMESPACE_BENTLEY_USTN_ELEMENT;

int      gCurrentContainer;
BoolInt  gDetailList=FALSE;


TreeXmplInfo    MYInfo;
Private UInt32 color = 1;
Private Int32 style = 0;
Private UInt32 weight = 0;
Private long command = NULL;

typedef struct  mytreedata
    {
    MSWChar     *pwDisplayText;
    BoolInt     bAllowsChildren;
    int         containerId;
    struct mytreedata   **children;
    BoolInt     isThreeD;
    } MyTreeData;




/* --- Hierarchy of data nodes --- */

/* Composers */
//

MyTreeData  cylinder = 
            {L"Cylinder", FALSE, CONTAINERID_CellElemView, 
                NULL, TRUE};

MyTreeData  spheres = 
            {L"Spheres", FALSE, CONTAINERID_CellElemView, 
                NULL, TRUE};


MyTreeData  cone = 
            {L"Cone", FALSE, CONTAINERID_CellElemView, 
                NULL, TRUE};
 

MyTreeData  line = 
            {L"Line", FALSE, CONTAINERID_CellElemView, 
                NULL, FALSE};


MyTreeData  ellipse = 
            {L"Ellipse", FALSE, CONTAINERID_CellElemView, 
                NULL, FALSE};


MyTreeData  rectangle = 
            {L"Rectangle", FALSE, CONTAINERID_CellElemView, 
                NULL, FALSE};


/* Element type */
MyTreeData  *aElem3D[] =
            {&cylinder, &spheres, &cone, NULL};
MyTreeData  ThreeDimension = 
            {L"ThreeDimension", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aElem3D, TRUE};
MyTreeData  *aElem2D[] =
            {&line, &ellipse, &rectangle, NULL};
MyTreeData  TwoDimension = 
            {L"TwoDimension", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aElem2D, FALSE};


/* Dimension node */
MyTreeData  *aDemension[] =
            {&TwoDimension, &ThreeDimension, NULL};
MyTreeData  Dimension = 
            {L"Dimension", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aDemension, TRUE};


/* Root node */
MyTreeData  *aRoot[] =
            {&Dimension, NULL};
MyTreeData  Root = 
            {L"Presentations", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aRoot, TRUE};




/*----------------------------------------------------------------------+*//**
* This method builds the list model.
* @bsimethod BuildListModel                                        **
* @param        pList   IN OUT the list model that is to be populated        *
* @param        dbP IN  the dialog box that holds the items           *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/

Private  int BuildListModel
(
ListModel       *pList, /*<==> the listModel to populate */
DialogBox       *dbP    /*<== the dialog box that holds the listmodel */ 
)
   {
    int count = 0;
    DgnIndexIteratorP   pDgnIndexIterator;
    DgnFileObjP         pLibObj;
    MSWChar             wCellName[MAX_CELLNAME_LENGTH];//this should be unicode
    MSWChar             wCellDescription[MAX_CELLDSCR_LENGTH];
    BoolInt             isThreeD,isHidden,isLocked;
    int                 cellType;
    DgnIndexItemP       pDgnIndexItem;
    int                 iStatus,row,col;
    GuiTreeNode         *pNode,*pChildNode;
    GuiTreeCell         *pCell;
    ListRow             *pRow;
    ListCell            *pListCell;
    char                *pString;
    BoolInt             found;

    DialogItem  *pItem = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_Tree,TREEID_CellList,0);
    mdlDialog_treeLastCellClicked (&row,&col,pItem->rawItemP);

    mdlDialog_treeGetNextSelection (&found, &row, &col, pItem->rawItemP);
    pNode = mdlTreeModel_getDisplayRowAtIndex (mdlDialog_treeGetTreeModelP (pItem->rawItemP) ,row);
    pCell = mdlTreeNode_getCellAtIndex (pNode,0);

    iStatus = mdlTreeCell_getStringValue (pCell,&pString);

    //iStatus = mdlCell_getLibraryObject (&pLibObj,pString,TRUE);
    pLibObj = mdlDgnFileObj_getMasterFile ();
    pDgnIndexIterator = mdlModelIterator_create (pLibObj);
    //mdlModelIterator_setAcceptCellsOnly (pDgnIndexIterator,TRUE);

    pChildNode = mdlTreeNode_getFirstChild (pNode);
    while (pDgnIndexItem = mdlModelIterator_getNext (pDgnIndexIterator))
        {

        double      lastSaved;
        pRow = mdlListRow_create (pList);
        //do the name column
        mdlModelItem_getName (pDgnIndexItem, wCellName, MAX_CELLNAME_LENGTH);
        pListCell = mdlListRow_getCellAtIndex (pRow, CELLNAMECOL);
        mdlListCell_setStringValueW (pListCell,wCellName,TRUE);
        iStatus = mdlListCell_setInfoField (pListCell,0,(long)pChildNode);
        long    infoField;
        iStatus = mdlListCell_getInfoField (pListCell,0,&infoField);
        //do the description column
        mdlModelItem_getDescription (pDgnIndexItem, wCellDescription, MAX_CELLDSCR_LENGTH);
        pListCell = mdlListRow_getCellAtIndex (pRow,CELLDESCCOL);
        mdlListCell_setStringValueW (pListCell,wCellDescription,TRUE);
        mdlModelItem_getData (pDgnIndexItem,&cellType, &isThreeD, &isLocked,&isHidden,&lastSaved);
        cellType = mdlModelItem_getCellType (pDgnIndexItem);
            {
            pListCell = mdlListRow_getCellAtIndex (pRow,CELLTYPECOL);

            switch (cellType)
                {
                case CELLLIBTYPE_GRAPHIC:
                        mdlListCell_setStringValueW (pListCell,L"Grph",TRUE);
                        break;
                case CELLLIBTYPE_POINT:
                        mdlListCell_setStringValueW (pListCell,L"Point",TRUE);
                        break;
                case CELLLIBTYPE_MENU:
                        mdlListCell_setStringValueW (pListCell,L"Menu",TRUE);
                        break;
                case CELLLIBTYPE_CBMENU:
                        mdlListCell_setStringValueW (pListCell,L"CBMenu",TRUE);
                        break;
                case CELLLIBTYPE_MATRIXMENU:
                        mdlListCell_setStringValueW (pListCell,L"Matrix",TRUE);
                        break;
                default:
                        mdlListCell_setStringValueW (pListCell,L"Undef",TRUE);
                        break;
                }
            }

        pListCell = mdlListRow_getCellAtIndex (pRow,CELLTHREEDCOL);
        if (isThreeD)
            mdlListCell_setStringValueW (pListCell,L"3",TRUE);
        else
            mdlListCell_setStringValueW (pListCell,L"2",TRUE);

        pListCell = mdlListRow_getCellAtIndex (pRow,CELLLOCKEDCOL);
        if (isLocked)
            mdlListCell_setStringValueW (pListCell,L"L",TRUE);
        else
            mdlListCell_setStringValueW (pListCell,L"U",TRUE);

        pListCell = mdlListRow_getCellAtIndex (pRow,CELLMODELIDCOL);
        ValueDescr  valDescr;
        valDescr.formatType=FMT_LONG;
        valDescr.value.sLongFormat=mdlModelItem_getModelID (pDgnIndexItem);
        mdlListCell_setValue (pListCell,&valDescr,FALSE);
        if(!isHidden)
            mdlListModel_addRow (pList,pRow);
        pChildNode = mdlTreeNode_getNextChild (pNode,pChildNode);

        count++;
        }
    mdlModelIterator_free (pDgnIndexIterator);
    return count;
   }

/*----------------------------------------------------------------------+*//**
* Handle the resizing of the dialog                                                                        *
* @bsimethod cellExp_adjustVSashDialogItems                                        *                                               *
* @param        db  IN  The dialog that is being resized            *
* @param        forceCTErase    IN wether to force the update of the entire container before      *
* @param        pOldContent IN  the old extents before this resize          *
* @param        refreshItems    IN redraw after the resize            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private void    cellExp_adjustVSashDialogItems

(
DialogBox       *db,
BoolInt         forceCTErase,
BSIRect         *pOldContent,
BoolInt         refreshItems
)
    {
    DialogItem  *sashDiP;
    DialogItem  *treeDiP;
    DialogItem  *ctPanelDiP;
    BoolInt     eraseFirst=TRUE;
    int         attrib;
    int         containerId;

    
    /* Get the 3 main items on the dialog */
    sashDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Sash, SASHID_VDivider, 0);
    treeDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Tree, TREEID_CellList, 0);
    ctPanelDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ContainerPanel, CTPANELID_Detail, 0);
        

            /* Only erase-first if forced or if non- ALIGN_PARENT container */
    eraseFirst = forceCTErase || (MYInfo.containerId != CONTAINERID_SingleColumn &&
                                  MYInfo.containerId != CONTAINERID_CellIconView);

    /* Resize, reposition and draw the items */

    mdlDialog_containerPanelGetInfo (&attrib,&containerId,NULL,NULL,ctPanelDiP->rawItemP);
    //handle the resize of the items to make them always fit.

    switch (containerId)
        {
        case CONTAINERID_CellListing: //2
            {
            DialogItem  *pListBox = mdlDialog_itemGetByTypeAndId (db,RTYPE_ListBox,LISTBOXID_CellListing,0);
            Sextent extents = ctPanelDiP->extent;
        
            extents.origin.y = extents.origin.y+2*XC;
            extents.height = extents.height-(2*XC);
            extents.width = ctPanelDiP->extent.width;

            if (pListBox)
            mdlDialog_itemSetExtent (db,pListBox->itemIndex,&extents,TRUE);
            eraseFirst = TRUE;
            /* Resize, reposition and draw the items */
            mdlDialog_itemsAdjustFlushWithSash (db, NULL, pOldContent, sashDiP, 
                                                treeDiP, refreshItems, refreshItems,
                                                ctPanelDiP, eraseFirst, refreshItems);
            
            }

            break;
        
                case CONTAINERID_CellElemView: //1
            {
            DialogItem  *pPreview = mdlDialog_itemGetByTypeAndId (db,RTYPE_Generic,GENERICID_CellPreview,0);
            DialogItem  *pTextDescr = mdlDialog_itemGetByTypeAndId (db,RTYPE_Text,TEXTID_DialogElement,0);
        
            Sextent     extents = pPreview->extent;
            extents.origin.x = ctPanelDiP->extent.origin.x+(XC);
            extents.origin.y = (pTextDescr->extent.origin.y)+(10*XC);
            extents.width=ctPanelDiP->extent.width-(2*XC);
            extents.height=ctPanelDiP->extent.height-(extents.origin.y)-(2*XC);
            mdlDialog_itemSetExtent (db,pPreview->itemIndex,&extents,FALSE);
            /* Resize, reposition and draw the items */
            mdlDialog_itemsAdjustFlushWithSash (db, NULL, pOldContent, sashDiP, 
                                                treeDiP, refreshItems, refreshItems,
                                                ctPanelDiP, eraseFirst, refreshItems);
            
            }

            break;
        case CONTAINERID_CellIconView: //3
            {
            DialogItem *pTemp = mdlDialog_itemGetByTypeAndId (db,RTYPE_Container,CONTAINERID_CellIconView,0);
            /* Resize, reposition and draw the items */
            mdlDialog_itemsAdjustFlushWithSash (db, NULL, pOldContent, sashDiP, 
                                                treeDiP, refreshItems, refreshItems,
                                                ctPanelDiP, eraseFirst, refreshItems);


            }
            break;

        default:

            break;
        }


    }

/*----------------------------------------------------------------------+*//**
* When a tree node is selcted this starts the rebuilding of the list model on the main panel*
* @bsimethod populateCellListing                                        **
* @param        pNode   IN  the node that is currently selected        *
* @param        dbP IN  the dialog box that holds the items           *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void populateCellListing
(
GuiTreeNode     *pNode,
DialogBox       *dbP
)
    {
    ListModel *pList;
    
    DialogItem  *pItem = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_ListBox,LISTBOXID_CellListing,0);
    
    pList = mdlDialog_listBoxGetListModelP (pItem->rawItemP);
    
    mdlListModel_empty (pList,TRUE);
    
    BuildListModel (pList,dbP);
    
    mdlDialog_listBoxSetListModelP (pItem->rawItemP,pList,CELLLISTMODELCOLS);

    }

/*----------------------------------------------------------------------+*//**
* Draws the cell if it is a 2d library                                                                        *
* @bsimethod Draw2DCell                                                 *                                               *
* @param        cellP IN the cell to display            *
* @param        dbP IN  the dialog to display the cell on             *
* @param        localRectP IN the rectangle to draw into            *
* @param        nDices  IN is the file 2d or 3d            *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void Draw2DCell
(
MSElementDescr      *cellP,
DialogBox           *dbP,
BSIRect             *localRectP,
ViewFlags           *viewflagsP,
BoolInt             nDices
)
    {
    RotMatrix       rMatrix;
    Dpoint3d        origin, newextent;
        
    DVector3d       dRangeVec;
    UInt32          colorMap[MAX_CMAPENTRIES];
    byte            colorTable[MAX_CTBLBYTES];
    
    mdlColor_getColorTableByModelRef (colorTable, MASTERFILE);
    mdlColor_matchLongColorMap (colorMap, colorTable, 0, FALSE);

    mdlRMatrix_getIdentity (&rMatrix);

    //mdlRMatrix_rotateRange (&range.org,&range.end,&rMatrix);
    mdlCnv_scanRangeToDRange (&dRangeVec, &cellP->el.hdr.dhdr.range);

    origin = dRangeVec.org;
    origin.z = 0.0;

    mdlVec_subtractPoint (&newextent,&dRangeVec.end,&origin);

    newextent.z = fc_1000;
    if (newextent.x < fc_1) newextent.x = fc_1000;
    if (newextent.y < fc_1) newextent.y = fc_1000;
        
    mdlDialog_rectInset(localRectP, 2, 2);

    mdlWindow_clipRectSet ((MSWindow *)dbP, localRectP);

    mdlElmdscr_extendedDisplayToWindow ((MSWindow *)dbP, localRectP,viewflagsP ,cellP, &rMatrix,
                &origin, &newextent, nDices, -1,colorMap,FALSE,NULL);

    mdlWindow_clipRectSet ((MSWindow *)dbP, NULL);

    }


/*----------------------------------------------------------------------+*//**
* Draws the cell if it is a 3d library                                                                        *
* @bsimethod Draw3DCell                                                 *                                               *
* @param        cellP IN the cell to display            *
* @param        dbP IN  the dialog to display the cell on             *
* @param        localRectP IN the rectangle to draw into            *
* @param        nDices  IN is the file 2d or 3d            *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void Draw3DCell
(
MSElementDescr      *cellP,
DialogBox           *dbP,
BSIRect             *localRectP,
ViewFlags           *viewflagsP,
BoolInt             nDices
)
    {
    BSIRect         localRectArray[4];
    int             deltaX;
    int             deltaY;
    int             winNumber;
    RotMatrix       rMatrixArray[4];
    Dpoint3d        newextent;
    DVector3d       dRangeVec;
        /*MstnColorMap          colorMap[MAX_CMAPENTRIES];
   
    IViewportP      sampleVp =  Bentley::Ustn::IViewManager::GetManager().GetFirstOpenView();
    if (sampleVp)
        for (int indexColor =0;indexColor<MAX_CMAPENTRIES  ;indexColor++ )
            {
            colorMap[indexColor] = Bentley::Ustn::ISessionMgr::GetMasterDgnFile()->GetColorMap()[indexColor];
            }
     
        colorMap [255] = sampleVp->GetBackgroundColor();
    if (userPrefsP->extFlags.invertBackground)
        colorMap [255] = 0xffffff;*/
    UInt32          colorMap[MAX_CMAPENTRIES];
    byte            colorTable[MAX_CTBLBYTES];
    
    mdlColor_getColorTableByModelRef (colorTable, MASTERFILE);
    mdlColor_matchLongColorMap (colorMap, colorTable, 0, FALSE);

    //set up the views
    mdlView_getStandard (&rMatrixArray[0],STANDARDVIEW_Top);
    mdlView_getStandard (&rMatrixArray[1],STANDARDVIEW_Iso);
    mdlView_getStandard (&rMatrixArray[2],STANDARDVIEW_Front);
    mdlView_getStandard (&rMatrixArray[3],STANDARDVIEW_Right);

    deltaX = localRectP->corner.x - localRectP->origin.x;
    deltaY = localRectP->corner.y - localRectP->origin.y;

    localRectArray[0] = *localRectP;
    localRectArray[0].corner.y = localRectP->origin.y+(deltaY/2);
    localRectArray[0].corner.x = localRectP->origin.x+(deltaX/2);

    localRectArray[1] = *localRectP;
    localRectArray[1].origin.x = localRectP->origin.x+(deltaX/2);
    localRectArray[1].corner.y = localRectP->origin.y+(deltaY/2);

    localRectArray[2] = *localRectP;
    localRectArray[2].origin.y = localRectP->origin.y+(deltaY/2);
    localRectArray[2].corner.x = localRectP->corner.x-(deltaX/2);

    localRectArray[3]  = *localRectP;
    localRectArray[3].origin.x = localRectP->origin.x+(deltaX/2);
    localRectArray[3].origin.y = localRectP->origin.y+(deltaY/2);

    for (winNumber  = 0;winNumber <4  ;winNumber++ )
        {
        mdlCnv_scanRangeToDRange (&dRangeVec, &cellP->el.hdr.dhdr.range);

        mdlRMatrix_multiplyRange (&dRangeVec.org,&dRangeVec.end,&rMatrixArray[winNumber]);
        mdlVec_subtractPoint (&newextent,&dRangeVec.end,&dRangeVec.org);

        mdlRMatrix_unrotatePoint (&dRangeVec.org, &rMatrixArray[winNumber]);

        if (newextent.z< fc_1) newextent.z = fc_1000;
        if (newextent.x < fc_1) newextent.x = fc_1000;
        if (newextent.y < fc_1) newextent.y = fc_1000;


        mdlDialog_rectInset(&localRectArray[winNumber], 2, 2);

        mdlWindow_clipRectSet ((MSWindow *)dbP, localRectP);

        mdlElmdscr_extendedDisplayToWindow ((MSWindow *)dbP, &localRectArray[winNumber],viewflagsP ,cellP, &rMatrixArray[winNumber],
                                            &dRangeVec.org, &newextent, 0, -1,colorMap,FALSE,NULL);

        mdlDialog_rectDrawEdge (dbP,&localRectArray[winNumber],TRUE);
        mdlWindow_clipRectSet ((MSWindow *)dbP, NULL);

        }
        
        
    }
/*----------------------------------------------------------------------+*//**
* create element for each cell                                                                        *
* @bsimethod Draw3DCell                                                 *                                               *
* @param        elName the element name to create           *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  int elemType
(
MSWChar         *elName
)
        {     
        int eleNum;
        if (0 == wcscmp (elName,L"Line"))
            {
            eleNum = LINE;
            command = CMD_PLACE_LINE;
            }
        else if (0 == wcscmp (elName,L"Ellipse"))
                {
                eleNum = ELLIPSE;
                command = CMD_PLACE_ELLIPSE_CENTER;
                }
        else if (0 == wcscmp (elName,L"Rectangle"))
                {
                eleNum = RECTANGLE;
                command = CMD_PLACE_BLOCK;
                }
        else if (0 == wcscmp (elName,L"Cone"))
                {
                eleNum = CONE;
                command = CMD_PLACE_CONE;
                }
        else if (0 == wcscmp (elName,L"Spheres"))
                {
                eleNum = SPHERES;
                command = CMD_PLACE_SPHERE;
                }
        else if (0 == wcscmp (elName,L"Cylinder"))
                {
                eleNum = CYLINDER;
                command = CMD_PLACE_CYLINDER;
                }

        return eleNum;
        }
/*----------------------------------------------------------------------+*//**
* create element                                                                         *
* @bsimethod Draw3DCell    
* @param        element descriptor to hold the element being created                                              *
* @param        elName the element name to create           *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  int elemCreate 
(
MSElementDescr  **elDescrPP, 
MSWChar         *elName
)
    {

    int status;
    int eleNum = elemType (elName);
    switch (eleNum)
{
    case LINE:
        {
        Dpoint3d points[2];

        MSElement     element;
        points[0].x = 10;
        points[0].y = 10;
        points[0].z = 0;
        points[1].x = 30;
        points[1].y = 30;
        points[1].z = 0;

        status = mdlLine_create (&element, NULL,points);
        status = mdlElmdscr_new (elDescrPP, NULL, &element);

        break;
        }

        case ELLIPSE:
        {
        Dpoint3d ptStack;

        MSElement     element;
        ptStack.x = 15;
        ptStack.y = 15;
        ptStack.z = 0;
        double radius1 = 10;
        double radius2 = 5;

        status = mdlEllipse_create (&element, NULL, &ptStack, radius1, radius2, NULL, 0);
        status = mdlElmdscr_new (elDescrPP, NULL, &element);

        break;
        }

        case RECTANGLE:
        {
        Dpoint3d points[5];

        MSElement     element;
        points [0].x = 10.0;
        points [0].y = 50.0;
        points [0].z = 0.0;
        points [1].x = 10.0;
        points [1].y = 60.0;
        points [1].z = 0.0;
        points [2].x = 20.0;
        points [2].y = 60.0;
        points [2].z = 0.0;
        points [3].x = 20.0;
        points [3].y = 50.0;
        points [3].z = 0.0;
        points [4].x = 10.0;
        points [4].y = 50.0;
        points [4].z = 0.0;
        status = mdlLineString_create (&element, NULL, points, 5);
        status = mdlElmdscr_new (elDescrPP, NULL, &element);

        break;
        }

        case  CYLINDER:
        {
        KIBODY *bodyP;
        double height = 2;
        double radius = 1;
        status = mdlKISolid_makeCone (&bodyP, height, radius, radius);

        status = mdlKISolid_bodyToElementD (elDescrPP, bodyP, FALSE, -1, NULL, MASTERFILE, TRUE);
        break;
        }

        case  SPHERES:
        {
        double radius = 1;
        KIBODY *bodyP;
        status = mdlKISolid_makeSphere (&bodyP, radius);

        status = mdlKISolid_bodyToElementD (elDescrPP, bodyP, FALSE, -1, NULL, MASTERFILE, TRUE);
        break;
        }

        case  CONE:
        {
        double base = 2;
        double height = 1;
        double top = 1;
        KIBODY *bodyP;
        status = mdlKISolid_makeCone (&bodyP, height, base, top);
        status = mdlKISolid_bodyToElementD (elDescrPP, bodyP, FALSE, -1, NULL, MASTERFILE, TRUE);
        break;
        }
    }

    return status;
    }


/*----------------------------------------------------------------------+*//**
* The function draws the cell to a generic item.  
* It needs the dialog box the generic item and the cell to draw                                                                        *
* @bsimethod drawCellDetail                                             *                                               *
* @param        *dbP    IN The dialog box that holds the item to preview            *
* @param        diP     IN The generic item that will be drawn to            *
* @param        cellName  IN    The name of the cell that is to be drawn            *
* @param        is3d    IN is this a 3d cell 
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void drawCellDetail
(
DialogBox       *dbP,
DialogItem      *diP,
MSWChar         *cellName,
BoolInt         is3d
)
    {
    ViewFlags       viewflags;
    MSElementDescr *cellDP = NULL;

    BoolInt         nDices;
    int             status;

    status = elemCreate (&cellDP,cellName);
        
    if (SUCCESS!=status)
        {
        mdlOutput_messageCenter  (MESSAGE_ERROR,"no Preview Available","unable to preview error while getting data",FALSE);
        return;
        }

    mdlElmdscr_validate (cellDP, MASTERFILE);
    
    nDices = mdlModelRef_is3D (MASTERFILE);
    
    memset (&viewflags, 0, sizeof(viewflags));
    viewflags.patterns   = 1;
    viewflags.ed_fields  = 1;
    viewflags.on_off     = 1;
    viewflags.points     = 1;
    viewflags.constructs = 1;
    viewflags.dimens     = 1;
        
        mdlElement_setSymbology ( &cellDP->el, &color, &weight, &style);


        if (!is3d)
        Draw2DCell (cellDP, dbP, &diP->rect, &viewflags, nDices);
    else
        Draw3DCell (cellDP, dbP, &diP->rect, &viewflags, nDices);
    

    mdlElmdscr_freeAll (&cellDP);
    }




/*----------------------------------------------------------------------+*//**
* The function draw preview of customized element                                                                       *
* @bsimethod     drawPreview                                                *                                               *
* @param        *dbP    IN The dialog box that holds the item to preview            *
* @param        diP     IN The generic item that will be drawn to               *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private void    drawPreview
(
DialogBox       *dbP,
DialogItem      *diP
)
    {
    int             row,col;
    BoolInt         found;
    GuiTreeNode    *pNode;
    GuiTreeCell    *pCell;
    MSWChar        *pWString=NULL;
    DialogItem     *pItem;
    ValueDescr      val;

    pItem  = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_Tree,TREEID_CellList,0);
    mdlDialog_treeLastCellClicked (&row,&col,pItem->rawItemP);

    mdlDialog_treeGetNextSelection (&found, &row, &col, pItem->rawItemP);
    pNode = mdlTreeModel_getDisplayRowAtIndex (mdlDialog_treeGetTreeModelP (pItem->rawItemP) ,row);
    pCell = mdlTreeNode_getCellAtIndex (pNode,0);
    mdlTreeCell_getDisplayTextW (pCell,&pWString);

    mdlTreeCell_getValue (pCell,&val);



    MyTreeData    *pMytreedata=(MyTreeData*)val.value.voidPFormat;

    drawCellDetail (dbP,diP,pWString,pMytreedata->isThreeD);


    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_nodeLoadChildren                               |
|                                                                       |
| author        BSI                                     07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_nodeLoadChildren
(
GuiTreeModel    *pModel,
GuiTreeNode     *pParentNode
)
    {
    GuiTreeNode     *pNode;
    MyTreeData      *pMyData; 
    MyTreeData      **pChildren;
    MyTreeData      *pChild;
    ValueDescr      valueDescr;

    mdlTreeNode_getValue (pParentNode, &valueDescr);
    pMyData = (MyTreeData *) valueDescr.value.voidPFormat;
    pChildren  = pMyData->children;

    pChild = *pChildren;
    while (pChild != NULL)
        {
        pNode = mdlTreeNode_create (pModel, pChild->bAllowsChildren);
        mdlTreeNode_setDisplayTextW (pNode, pChild->pwDisplayText);
        mdlTreeNode_setEditor (pNode, 0, 0, NULL, 1, 1);

        valueDescr.formatType = FMT_VOIDP;
        valueDescr.value.voidPFormat = pChild;
        mdlTreeNode_setValue (pNode, &valueDescr, FALSE);

        if  (pChild->bAllowsChildren)
            {
            mdlTreeNode_setCollapsedIcon (pNode, ICONID_SubDirectoryFolder, RTYPE_Icon, NULL);
            mdlTreeNode_setExpandedIcon (pNode, ICONID_OpenDirectoryFolder, RTYPE_Icon, NULL);
            mdlTreeNode_setExpandedSelectedIcon (pNode, ICONID_CurrentDirectoryFolder, RTYPE_Icon, NULL);
            }

        else
            {
            mdlTreeNode_setLeafIcon (pNode, ICONID_Element, RTYPE_Icon, mdlSystem_getCurrMdlDesc());
            }

        mdlTreeNode_addChild (pParentNode, pNode);

        pChildren++;
        pChild = *pChildren;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treeHook                                        |
|                                                                       |
| author        BSI                                     07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treeHook
(
DialogItemMessage   *dimP       /* => a ptr to a dialog item message */
)
    {
    GuiTreeModel    *pModel;
    DialogItem      *diP = dimP->dialogItemP;
    RawItemHdr      *riP = diP->rawItemP;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_CREATE:
            {
            GuiTreeNode     *pRoot;
            ValueDescr      valueDescr;

            /* Create TreeModel with 1 column */
            pModel = mdlTreeModel_create (1);   
            
            /* Get root node from the TreeModel */
            pRoot = mdlTreeModel_getRootNode (pModel);
            mdlTreeNode_setDisplayTextW (pRoot, Root.pwDisplayText);

            valueDescr.formatType = FMT_VOIDP;
            valueDescr.value.voidPFormat = &Root;
            mdlTreeNode_setValue (pRoot, &valueDescr, FALSE);

            mdlTreeNode_setCollapsedIcon (pRoot, ICONID_VolumeFolder, RTYPE_Icon, NULL);
            mdlTreeNode_setExpandedIcon (pRoot, ICONID_OpenVolumeFolder, RTYPE_Icon, NULL);
            mdlTreeNode_setExpandedSelectedIcon (pRoot, ICONID_CurrentVolumeFolder, RTYPE_Icon, NULL);

            /* Load the root's children; other parent nodes will load their */
            /* children and expand during the STATECHANGING message */
            treexmpl_nodeLoadChildren (pModel, pRoot);

            /* Start with the root expanded */
            mdlTreeNode_expand (pRoot);

            mdlDialog_treeSetTreeModelP (riP, pModel);

            MYInfo.pTreeModel = pModel;

            break;
            }

        case DITEM_MESSAGE_STATECHANGING:
            {
            GuiTreeNode     *pNode;
            int             row;

            if  (dimP->u.stateChanging.changeType == STATECHANGING_TYPE_NODEEXPANDING)
                {
                pModel = mdlDialog_treeGetTreeModelP (riP);

                row = dimP->u.stateChanging.row;

                /* Get this parent node & if not already populated, fill it */
                if (NULL != (pNode = mdlTreeModel_getDisplayRowAtIndex (pModel, row)) && !mdlTreeNode_isParentPopulated (pNode))
                    {
                    treexmpl_nodeLoadChildren (pModel, pNode);
                    }
                }

            dimP->u.stateChanging.response = STATECHANGING_RESPONSE_ALLOW;
            break;
            }

        case DITEM_MESSAGE_STATECHANGED:
            {
            int             row=-1, col=-1;
            BoolInt         found;
            GuiTreeNode     *pNode;

            pModel = mdlDialog_treeGetTreeModelP (riP);

            mdlDialog_treeLastCellClicked (&row, &col, riP);
            if  (row != INVALID_ITEM && col != INVALID_ITEM)
                {
                row = -1;
                col = -1;

                /* Single selection */
                //mdlDialog_treeGetNextSelection (&found, &row, &col, riP);
                /* Multi-selection */
                found = !(mdlDialog_treeGetLocationCursor (&row, &col, riP));
                
                if  (found)
                    {
                    if (NULL != (pNode = mdlTreeModel_getDisplayRowAtIndex (pModel, row)))
                        {
                        MyTreeData  *pMyData;
                        ValueDescr  valueDescr;
                        BoolInt     bChanged=FALSE;

                        mdlTreeNode_getValue (pNode, &valueDescr);
                        pMyData = (MyTreeData *) valueDescr.value.voidPFormat;

                        /* Display the correct Container */
                        if (pMyData->containerId > 0 &&
                            pMyData->containerId != MYInfo.containerId)
                            {
                            MYInfo.containerId = pMyData->containerId;
                            mdlDialog_treeSetContainer (riP, MYInfo.containerId,
                                                            mdlSystem_getCurrMdlDesc (),
                                                            FALSE);
                                
                            bChanged = TRUE;
                            }
                        /* If showing hierarchy, display children or detail */
                        if (MYInfo.containerId == CONTAINERID_CellElemView )
                            {
                            mdlDialog_treeSetContainer (dimP->dialogItemP->rawItemP,CONTAINERID_CellElemView,mdlSystem_getCurrMdlDesc(),FALSE);
                            gCurrentContainer = CONTAINERID_CellElemView;
                            bChanged = TRUE;
                            GuiTreeCell    *pCell;
                            MSWChar        *pWString=NULL;
                            pCell = mdlTreeNode_getCellAtIndex (pNode,0);
                            mdlTreeCell_getDisplayTextW (pCell,&pWString);

                            DialogItem  *pTextDescrName = mdlDialog_itemGetByTypeAndId (dimP->db,RTYPE_Text,TEXTID_CellName,0);
                            BoolInt valueChanged;
                            mdlDialog_itemSetStringValueW (&valueChanged, pWString, dimP->db, pTextDescrName->itemIndex);
                            }
                            if  (bChanged)
                                {
                                /* Redraw 3 main items in dialog (Tree, Sash, CTPanel) */
                                                cellExp_adjustVSashDialogItems (dimP->db, TRUE, NULL, FALSE);
                                }
                            else
                                {
                                /* Redraw the ContainerPanel */
                                DialogItem *ctPDiP;
                                ctPDiP = mdlDialog_itemGetByTypeAndId (dimP->db,
                                                                RTYPE_ContainerPanel, CTPANELID_Detail, 0);
                                if (ctPDiP)
                                    mdlDialog_itemDrawEx (dimP->db, ctPDiP->itemIndex, FALSE);

                                }

                        }
                    }
                }
            break;
            }

        case DITEM_MESSAGE_DESTROY:
            {
            GuiTreeModel   *pTreeModel = mdlDialog_treeGetTreeModelP (dimP->dialogItemP->rawItemP);
            //need to free the structure
            if  (NULL != pTreeModel)
                mdlTreeModel_destroy (pTreeModel, TRUE);
            }
            break;
            

        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    
    }

/*----------------------------------------------------------------------+*//**
* This function is left as an example of reacting to the sash item moving.                                                                        *
* @bsimethod populateCellListing                                        *                                               *
* @param        mfaP     Information passed to this function as the sash is moved.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private void cellExp_vSashMotionFunc   /* called each time user moves mouse
                                           while dragging sash */
(
MotionFuncArg   *mfaP   /* <> mfaP->pt.x = where sash upper left corner will go */
                        /* => mfaP->pt.y = where cursor x position is */
                        /* => mfaP->dragging = TRUE if cursor is within window */
)
    {

    }


/*----------------------------------------------------------------------+*//**
* The callback function for the sash item                                                                       *
* @bsimethod treeHook                                                   *                                               *
* @param        dimP     The structure passed to the dialog item handler.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void dividerHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_BUTTON:
            {
            if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
                {
                BSIRect  contentRect;
                int      fontHeight = mdlDialog_fontGetCurHeight (dimP->db);

                mdlWindow_contentRectGetLocal (&contentRect,(MSWindow *) dimP->db);
                }
            else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
                {
                Sash_ButtonUpAuxInfo *buaiP =
                    (Sash_ButtonUpAuxInfo *) dimP->auxInfoP;

                /* use buaiP->newXPos to determine where upperLeft corner
                   of sash beveled rect will go.  This message is sent after
                   sash has been erased from old position & moved, but before
                   it has been drawn */
                if (buaiP->newXPos != buaiP->oldXPos)
                    cellExp_adjustVSashDialogItems (dimP->db, TRUE, NULL, TRUE);
                                   
                }

            break;
            }

        default:
            dimP->msgUnderstood = FALSE;
            break;
        }

    return ;
    }
/*----------------------------------------------------------------------+*//**
* A callback for the container panel                                                                        *
* @bsimethod treeHook                                                   *                                               *
* @param        dimP     the structure that the dialog manager passes to the hook function.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void ctpanelHook
(
DialogItemMessage   *dimP
)
    {
    return ;
    }




/*----------------------------------------------------------------------+*//**
*                                                                       *
* @bsimethod treeHook                                                   *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void textItemHook
(
DialogItemMessage       *dimP,
DialogMessage       *dmP
)
    {

    return ;
    }


/*----------------------------------------------------------------------+*//**
* The hook function to allow change line temperlate                                                                        *
* @bsimethod chooseHook                                     *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       none                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void chooseHook
(
DialogItemMessage       *dimP
)
    {
        
    dimP->msgUnderstood=TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_STATECHANGED:
            {
           /* tcb->symbology.color = color;
            tcb->symbology.style = style;
            tcb->symbology.weight = weight; */
            mdlParams_setActive (&color,ACTIVEPARAM_COLOR);   
            mdlParams_setActive (&weight,ACTIVEPARAM_LINEWEIGHT);
            mdlParams_setActive (&style,ACTIVEPARAM_LINESTYLE);

            cellExp_adjustVSashDialogItems (dimP->db, FALSE, NULL, TRUE);
            }
            break;
        default:
            {
            mdlParams_setActive (&color,ACTIVEPARAM_COLOR);   
            mdlParams_setActive (&weight,ACTIVEPARAM_LINEWEIGHT);
            mdlParams_setActive (&style,ACTIVEPARAM_LINESTYLE);
            dimP->msgUnderstood = FALSE;
                        }
            break;
        }

    return ;
 
    }

/*----------------------------------------------------------------------+*//**
* The callback function for the Generic item.  This will draw the graphics on the dialog item.                                                                        *
* @bsimethod genericPreviewItemHook                                     *                                               *
* @param        dimP     the structure that is passed to the dialog item handler.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void genericPreviewItemHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood=TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_DRAW:
            {
            mdlDialog_rectFill(dimP->db, &dimP->dialogItemP->rect,WHITE_INDEX);

        /*draw the generic item */
            drawPreview (dimP->db,dimP->dialogItemP);
            break;
            }

        default :
            dimP->msgUnderstood = FALSE;
            break;
        }
    return ;
    }

/*----------------------------------------------------------------------+*//**
* the callback function for the dialog.                                                                        *
* @bsimethod dialogHook                                                 *                                               *
* @param        dmP     The structure passed to the dialog handler.            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void dialogHook
(
DialogMessage       *dmP

)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
        {
        case DIALOG_MESSAGE_CREATE:
            {
            dmP->u.create.interests.windowMoving= TRUE;
            dmP->u.create.interests.resizes     = TRUE;
            dmP->u.create.interests.updates     = TRUE;
            break;
            }

        case DIALOG_MESSAGE_INIT:
        case DIALOG_MESSAGE_UPDATE:
            {
            DialogItem  *diP;

            diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_Sash,
                                                SASHID_VDivider, 0);
            if (!diP)
                break;

            cellExp_adjustVSashDialogItems (dmP->db, FALSE, NULL, TRUE);
            break;
            }

        case DIALOG_MESSAGE_WINDOWMOVING:
            {
            int         minSize = (10*XC);

            /* Don't process if only moving dialog box */
            if (dmP->u.windowMoving.whichCorners == CORNER_ALL ||
                dmP->u.windowMoving.whichCorners == CORNER_ALL_RESIZED)
                break;

            /* Minimum size for dialog */
            if (dmP->u.windowMoving.newWidth  < minSize)
                dmP->u.windowMoving.newWidth  = minSize;
            if (dmP->u.windowMoving.newHeight < minSize)
                dmP->u.windowMoving.newHeight = minSize;

            dmP->u.windowMoving.handled     = TRUE;

            break;
            }

        case DIALOG_MESSAGE_RESIZE:
            {
            BSIRect     oldContent;

            /* Don't process if only moving dialog box */
            if (dmP->u.resize.whichCorners == CORNER_ALL)
                break;

            mdlWindow_pointToLocal (&oldContent.origin, (MSWindow *)dmP->db,
                                    &dmP->u.resize.oldContent.origin);
            mdlWindow_pointToLocal (&oldContent.corner, (MSWindow *)dmP->db,
                                    &dmP->u.resize.oldContent.corner);

            cellExp_adjustVSashDialogItems (dmP->db, TRUE, &oldContent, 
                (dmP->u.resize.oldContent.origin.x != dmP->u.resize.newContent.origin.x ||
                 dmP->u.resize.oldContent.origin.y != dmP->u.resize.newContent.origin.y));
            break;
            }

        default:
            dmP->msgUnderstood = FALSE;
            break;
        }
    }


/*----------------------------------------------------------------------+*//**
* The callback function for the cell listmodel                                                                        *
* @bsimethod CellListlingHook                                           *                                               *
* @param        dimP     The structure passed to the dialog item handler            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void CellListlingHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood=TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_CREATE:
            {
            ListColumn  *pColumn;
            ListModel   *pListModel = mdlListModel_create (CELLLISTMODELCOLS);

            if  (NULL != (pColumn = mdlListModel_getColumnAtIndex (pListModel, 0)))
                mdlListColumn_setInfoFieldCount (pColumn, 1);

            int     numRows = BuildListModel (pListModel,dimP->db);
            mdlDialog_listBoxSetListModelP (dimP->dialogItemP->rawItemP,pListModel,CELLLISTMODELCOLS);
            }
            break;
        case DITEM_MESSAGE_DESTROY:
            {
            mdlListModel_destroy (mdlDialog_listBoxGetListModelP (dimP->dialogItemP->rawItemP),TRUE);
            }
            break;
        
        case DITEM_MESSAGE_BUTTON:
            {
            if ((BUTTONTRANS_UP==dimP->u.button.buttonTrans)&&(dimP->u.button.upNumber==1))
                {
                int         row,col;
                mdlDialog_listBoxLastCellClicked (&row,&col,dimP->dialogItemP->rawItemP);
                if (row >=0)
                    {
                    BoolInt     found;
                    ListRow    *pRow;
                    MSWChar     *pWString=NULL;
                    ValueDescr  val;
                    mdlDialog_listBoxGetNextSelection (&found, &row, &col, dimP->dialogItemP->rawItemP);
                    pRow = mdlListModel_getRowAtIndex (mdlDialog_listBoxGetListModelP (dimP->dialogItemP->rawItemP) ,row);
                    ListCell    *pCell = mdlListRow_getCellAtIndex (pRow,0);
                    mdlListCell_getValue (pCell, &val);
                    mdlListCell_getDisplayTextW (pCell,&pWString);
                    if (pWString)
                        {
                        ListCell *pCell;
                        mdlParams_setActive ((void*)pWString,ACTIVEPARAM_WCHAR_CELLNAME);
                        if (NULL != (pCell = mdlListModel_getCellAtIndexes (mdlDialog_listBoxGetListModelP (dimP->dialogItemP->rawItemP), row, 0)))
                            {
                            long        infoField;

                    /* Get the infoField from the cell, which is the GuiTreeNode ptr */
                            if (SUCCESS == mdlListCell_getInfoField (pCell, 0, &infoField))
                                {
                                GuiTreeNode *pNode = (GuiTreeNode *) infoField;

                        /* Expand or collapse the node */
                                if  (mdlTreeNode_isExpanded (pNode))
                                    {
                                    mdlTreeNode_collapse (pNode, FALSE);
                                    }
                                else if  (mdlTreeNode_isParent (pNode))
                                    {
                                    mdlTreeNode_makeDisplayable (pNode);
                                    mdlTreeNode_expand (pNode);
                                    }
                                else 
                                    {
                                    mdlTreeNode_makeDisplayable (pNode);
                                    }
                        
                                DialogItem *treeDiP = mdlDialog_itemGetByTypeAndId (dimP->db,RTYPE_Tree,TREEID_CellList,0);

                        /* Update the Tree */
                                mdlDialog_treeModelUpdated (treeDiP->rawItemP, TRUE);

                                row = mdlTreeModel_getDisplayRowIndex (mdlDialog_treeGetTreeModelP (treeDiP->rawItemP) , pNode);
                                if (row >= 0)
                                    mdlDialog_treeSelectCells (treeDiP->rawItemP, row, row, 0, 0, TRUE, TRUE);
                                }
                            }
                        }
                    }
                }
            else if((BUTTONTRANS_UP==dimP->u.button.buttonTrans)&&(dimP->u.button.upNumber==2))
                {
                int         row,col;
                mdlDialog_listBoxLastCellClicked (&row,&col,dimP->dialogItemP->rawItemP);
                if (row > 0 )
                    mdlDialog_cmdNumberQueue (FALSE,CMD_PLACE_CELL_ICON,"",TRUE);
                }
            
            }
            break;

        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    return ;
    }


/*----------------------------------------------------------------------+*//**
* The icon view of the cells                                                                        *
* @bsimethod genericIconViewHook                                        *                                               *
* @param        dimP     The structure that is passed to the dialog item            *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void genericIconViewHook
(
DialogItemMessage       *dimP
)
    {
     dimP->msgUnderstood=TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_DRAW:
            {
            mdlDialog_rectFill(dimP->db, &dimP->dialogItemP->rect, 
                                BLUE_INDEX);
           
            }
            break;
        case DITEM_MESSAGE_BUTTON:
            {

            }
            break;
        default:
            dimP->msgUnderstood = FALSE;
            break;
        }

    return ;
    }

/*----------------------------------------------------------------------+*//**
* !!!Describe Function Completely!!!                                                                        *
* @bsimethod iconContainerHook                                          *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void iconContainerHook
(
DialogItemMessage       *dimP
)
    {
        
    return ;
    }

/*----------------------------------------------------------------------+*//**
* Start cmd to place customized element                                                                         *
* @bsimethod doneHook                                          *                                               *
* @param        dimP     The structure that is passed to the dialog item          *
* @return       none                          *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/

Private void    doneHook
(
DialogItemMessage       *dimP 
)

    {
        dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_QUEUECOMMAND:
            {

            mdlDialog_cmdNumQueueExt (FALSE, command,
                              mdlSystem_getCurrTaskID(), TRUE, TRUE);
            mdlState_startDefaultCommand ();
            break;
            };
        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
                
                
    return;
    }

/*----------------------------------------------------------------------+*//**
* Unload the mdl application                                                                       *
* @bsimethod cancelHook                                          *                                               *
* @param        dimP     The structure that is passed to the dialog item           *
* @return       none                            *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Public  void cancelHook
(
DialogItemMessage       *dimP   /* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_QUEUECOMMAND:
            {

            mdlDialog_cmdNumQueueExt (FALSE, CMD_MDL_UNLOAD,
                              mdlSystem_getCurrTaskID(), TRUE, TRUE);
            mdlState_startDefaultCommand ();
            break;
            };
        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    }




/*----------------------------------------------------------------------+*//**
* The command entry point for the dialog.                                                                        *
* @bsimethod openDialogFunction                                         *                                               *
* @param        unparsed     unused unparsed argument to the command            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT void openDialogFunction
(
char        *unparsed
)
    {
    DialogBox* dbP;
    dbP = mdlDialog_open (NULL, DIALOGID_CellExp);

    return ;
    }

//because the dialog hook call back function now has a typedef
#if defined (MSVERSION) && (MSVERSION >= 0x890)
#define Handler PFDialogHook
#else
typedef void (*Handler)();
#endif

Private DialogHookInfo uHooks[] = 
    {
    {HOOKID_Dialog,(Handler)dialogHook  }, 
    {HOOKITEMID_Tree, (Handler)treeHook         },
    {HOOKITEMID_Sash_VDivider, (Handler)dividerHook     }, 
    {HOOKITEMID_CTPanel, (Handler)ctpanelHook   }, 
    {HOOKITEMID_Choose, (Handler)chooseHook        }, 
    {HOOKITEMID_GenericCellPreview, (Handler)genericPreviewItemHook     },  
    {HOOKITEMID_CellListing, (Handler)CellListlingHook  }, 
    {HOOKITEMID_GenericCellIconView,(Handler)genericIconViewHook        }, 
    {HOOKITEMID_CellIconContainer,(Handler)iconContainerHook    },  
    {HOOKITEMID_Done, (Handler)doneHook         }, 
    {HOOKITEMID_Cancel, (Handler)cancelHook         },
    };

/*----------------------------------------------------------------------+*//**
* The main entry point for the native code MDL application.
* This is different from the main or initialize from other application environments.                                                                        *
* @bsimethod MdlMain                                                    *                                               *
* @param        argc     The count of args passed to the application            *
* @param        argv[]     The array of args passed to this application as char arrays            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
    RscFileHandle   rfHandle;
    SymbolSet           *setP;

    mdlResource_openFile (&rfHandle,NULL,RSC_READONLY);
    
    static  MdlCommandName cmdNames[] = 
        {                
        {openDialogFunction,"OPENDIALOG"  },
        0,
        };

    mdlSystem_registerCommandNames (cmdNames);

    static MdlCommandNumber cmdNumbers[] =
        {       
        {openDialogFunction,CMD_LISTGI_OPEN },
        0,
        };

    gCurrentContainer = CONTAINERID_CellElemView;
    mdlSystem_registerCommandNumbers (cmdNumbers);
    
    mdlParse_loadCommandTable (NULL);

    mdlDialog_hookPublish (sizeof uHooks/sizeof DialogHookInfo,uHooks);
            
    /* Initialize the C Expression environment and publish our variables    */
    /* to the environment so the dialog box mananger can access them        */

    setP=mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);

    /* set up the variables we are going to set from dialog boxes */
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, FALSE);


    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType(TYPECODE_LONG), "color",
                &color);
    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType(TYPECODE_LONG), "style",
                &style);
    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType(TYPECODE_LONG), "weight",
                &weight);
    return SUCCESS;
    }

