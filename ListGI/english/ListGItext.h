/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ListGI/english/ListGItext.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   ListGI  $
|   $Revision: 1.1.12.1 $
|   	$Date: 2013/07/01 20:35:35 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ListGI - ListGI source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#define         TEXTITEMLABEL_CELLNAME  "Element Name:"
#define         TXT_Tree                "Elements"
#define         TITLETEXT               "Element Explorer"
#define         OPTIONSMENUTEXT         "Options"
#define         TEXTADDLIB              "~Add Library"
#define         OPENDIALOG              "opendialog"
#define         TREETITLETEXT           "cell library"


#define		TXT_CellName            "Element Name"
#define		TXT_CellDescription	"Description"
#define		TXT_CellType		"Type"
#define		TXT_CellISThreeD	"3d/2d"
#define		TXT_CellIHidden		"Hidden"
#define		TXT_CellILocked		"Locked"
#define		TXT_General		"General"
#define  	TXT_Color				"Color #:"
#define     TXT_SampleFrame       	    "Sample Frame"