/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ListGI/ListGI.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   ListGI  $
|   $Revision: 1.1.12.1 $
|       $Date: 2013/07/01 20:35:35 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ListGI - ListGI source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include <rscdefs.h>


#define     DLLAPPID 1

/* associate app with dll */
DllMdlApp DLLAPPID = 
    {
    "ListGI", "ListGI"
    }

