/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ListGI/ListGIdlg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   ListGI  $
|   $Revision: 1.1.12.1 $
|       $Date: 2013/07/01 20:35:35 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ListGI - ListGI source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogids.h>
#include    <rscdefs.h>
#include    <cmdlist.h>


#include    "ListGI.h"
#include    "ListGIcmd.h"
#include    <ListGItext.h>


/*----------------------------------------------------------------------+
|                                                                       |
|    Push Button Items                                                  |
|                                                                       |
+----------------------------------------------------------------------*/




/*----------------------------------------------------------------------+
|                                                                       |
|   Basic Dialog Box                                                    |
|                                                                       |
+----------------------------------------------------------------------*/
#define     XSIZE 80*XC
#define     YSIZE 25*YC
#define     SX  15*XC

DialogBoxRsc        DIALOGID_CellExp =
    {
    DIALOGATTR_DEFAULT|DIALOGATTR_GROWABLE,XSIZE,YSIZE,NOHELP,MHELP,
    HOOKID_Dialog,NOPARENTID,TITLETEXT,
    {   
    {{0,D_ROW(2),0,0},  Tree,       TREEID_CellList,     ON,  0, "", ""},
    {{SX,0,0,0},Sash,       SASHID_VDivider,        ON,  0, "", ""},
    {{0,0,0,0}, ContainerPanel, CTPANELID_Detail,   ON,  0, "", ""},
    }
    };

#undef  YSIZE
#undef  XSIZE
#undef  SX
/*----------------------------------------------------------------------+
|                                                                       |
|   Tree Item                                                           |
|                                                                       |
+----------------------------------------------------------------------*/

DItem_TreeRsc   TREEID_CellList =
    {
    NOHELP,MHELP,HOOKITEMID_Tree, 0,TREEATTR_NOSHOWROOT|TREEATTR_NOROOTHANDLE|
    TREEATTR_NOKEYSEARCH|TREEATTR_HORIZSCROLLBAR|TREEATTR_DOUBLECLICKEXPANDS,
    20, CTPANELID_Detail,TREETITLETEXT,
        {
        {0, 25*XC, 256, 0, TXT_Tree},
        }
    };
/*----------------------------------------------------------------------+
|                                                                       |
|   Sash Item                                                           |
|                                                                       |
+----------------------------------------------------------------------*/

DItem_SashRsc      SASHID_VDivider = 
    {
    NOHELP, MHELP, 
    HOOKITEMID_Sash_VDivider, 0, 
    10*XC, 10*XC, 
    SASHATTR_VERTICAL | SASHATTR_ALLGRAB | SASHATTR_SOLIDTRACK | 
    SASHATTR_WIDE | SASHATTR_SAVEPOSITION | SASHATTR_NOENDS
    };
/*----------------------------------------------------------------------+
|                                                                       |
|   Container items                                                     |
|                                                                       |
+----------------------------------------------------------------------*/

DItem_ContainerPanelRsc    CTPANELID_Detail = 
    {
    NOHELP, MHELP, 
    HOOKITEMID_CTPanel, 0, 
    CTPANELATTR_BOUNDINGBOX,
    CONTAINERID_CellIconView, 0,
    ""
    };

DItem_ContainerRsc      CONTAINERID_CellListing =
    {
    NOCMD,MCMD,NOHELP,MHELP,NOHOOK,NOARG,0,
    DILISTID_CellListing
    };

DItem_ContainerRsc      CONTAINERID_CellIconView =
    {
    NOCMD,MCMD,NOHELP,MHELP,HOOKITEMID_CellIconContainer,NOARG,0,DILISTID_CellIconView
    };

DItem_ContainerRsc         CONTAINERID_CellElemView = 
    {
    NOCMD, LCMD, NOHELP, MHELP, HOOKITEMID_CellIconContainer, NOARG, 0,
    DILISTID_SimpleInfo    
    };


#define     CBX 20*XC

DItem_ContainerRsc CONTAINERID_SingleColumn = 
    {
    NOCMD, LCMD, NOHELP, MHELP, NOHOOK, NOARG, 0,
    DILISTID_SingleColumn
    };




/*----------------------------------------------------------------------+
|                                                                       |
|   DialogItemList Resources                                            |
|                                                                       |
+----------------------------------------------------------------------*/

DialogItemListRsc DILISTID_SingleColumn =
    {
    {
    {{0,0,0,0}, ListBox,    LISTBOXID_SingleColumn,     ON, ALIGN_PARENT, "", ""},
    }
    };    

DialogItemListRsc   DILISTID_CellIconView = 
    {
    {
    //{{1*XC,D_ROW(2),23,23 },Generic,GENERICID_CellIcon,ON,0,"",""},
    }
    };

DialogItemListRsc   DILISTID_CellListing =
    {
    {
    {{1*XC,D_ROW(2),46*XC,22*YC}, ListBox, LISTBOXID_CellListing, ON, 0, "", ""},
    }
    };

DialogItemListRsc DILISTID_SimpleInfo =
    {
    {

    {{CBX-10*XC,D_ROW(10), 30*XC, 30*XC}, Generic, GENERICID_CellPreview, ON, 0, "", ""},
    {{CBX+7*XC, D_ROW(4), 0,     0}, ColorPicker, COLORPICKERID_DialogElement, ON, 0, "", ""},
    {{CBX, D_ROW(4), 5*XC, 0},  Text,         TEXTID_DialogElement, ON, 0, "", ""},
    {{CBX+18*XC,  D_ROW(4), 10*XC, 0}, OptionButton, OPTIONBUTTONID_DialogLineStyle, ON, 0, "", ""},
    {{CBX+40*XC,  D_ROW(4), 10*XC, 0}, OptionButton, OPTIONBUTTONID_DialogLineWeight, ON, 0, "", ""},
    {{CBX+5*XC,  D_ROW(6), 10*XC, 0}, PushButton, PUSHBUTTONID_DialogDone, ON, 0, "", ""},
    {{CBX+32*XC,  D_ROW(6), 10*XC, 0}, PushButton, PUSHBUTTONID_DialogCancel, ON, 0, "", ""},
    {{CBX,D_ROW(2),0,0}, Text, TEXTID_CellName, ON, 0, "", ""},
    }
    };    

/*----------------------------------------------------------------------+
|                                                                       |
|   ListBox Resources                                                   |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_SingleColumn =
    {
    NOHELP, MHELP, 
    NOHOOK, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_SELBROWSE | LISTATTR_HORIZSCROLLBAR | 
    LISTATTR_NOTRAVERSAL | LISTATTR_DRAWPREFIXICON,
    6, 0, "",
        {
        {256*XC, 256, 0, ""},
        }
    }
    extendedIntAttributes
    {
        {
        {EXTINTATTR_ITEMATTRS, LISTATTRX_FOCUSOUTLOOK}
        }
    };

DItem_ListBoxRsc    LISTBOXID_CellListing =
    {
    NOHELP,MHELP,HOOKITEMID_CellListing,NOARG,LISTATTR_SORTCOLUMNS|LISTATTR_HORIZSCROLLBAR|LISTATTR_GRID|LISTATTR_RESIZABLECOLUMNS|LISTATTR_COLHEADINGBORDERS,
    15,0,"",
        {
        {12*XC,512,ALIGN_LEFT,TXT_CellName},
        {20*XC,512,ALIGN_LEFT,TXT_CellDescription},
        {8*XC,512,ALIGN_LEFT,TXT_CellType},
        {8*XC,512,ALIGN_LEFT,TXT_CellISThreeD},
        {8*XC,512,ALIGN_LEFT,TXT_CellILocked},
        }
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Icon Item Resource                                                  |
|                                                                       |
+----------------------------------------------------------------------*/
IconRsc ICONID_Element =
    {
    16,    12,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "",
        {
                11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,
        11,11,11,11, 0,10,10,10,10,10,10, 0,11,11,11,11,
        11,11,11,11, 0,10, 0, 0, 0, 0, 0, 0,11,11,11,11,
        11,11,11,11, 0,10, 0,11,11,11,11,11,11,11,11,11,
        11,11,11,11, 0,10, 0, 0, 0, 0, 0, 0,11,11,11,11,
        11,11,11,11, 0,10, 0, 0, 0, 0, 0, 0,11,11,11,11,
        11,11,11,11, 0,10, 0,11,11,11,11,11,11,11,11,11,
        11,11,11,11, 0,10, 0, 0, 0, 0, 0, 0,11,11,11,11,
        11,11,11,11, 0,10,10,10,10,10,10, 0,11,11,11,11,
        11,11,11,11, 0,10, 0, 0, 0, 0, 0, 0,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
            }
    };

IconRsc ICONID_Container =
    {
    16,    12,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "",
        {
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11, 0,10,10,10,10,10,10,10,10,10,10, 0,11,11,
        11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        }
    };



        DItem_TextRsc   TEXTID_CellName = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP,
    HOOKITEMID_CellName, NOARG,
    25, "%s", "%s", "", "", NOMASK, TEXT_READONLY,
        TEXTITEMLABEL_CELLNAME,""
    };


DItem_GenericRsc    GENERICID_CellPreview = 
    {
    NOHELP,MHELP,HOOKITEMID_GenericCellPreview,NOARG
    };



DItem_OptionButtonRsc OPTIONBUTTONID_DialogLineStyle =
    {
    SYNONYMID_DialogElement, NOHELP, MHELP, 
    HOOKITEMID_Choose, OPTNBTNATTR_NEWSTYLE | NOARG, 
    "~Line:", "style", 
    {
    {Icon, ICONID_LineStyle0, CMD_ACTIVE_STYLE_CSELECT, MCMD, 0, NOMASK, ON, "~0"},
    {Icon, ICONID_LineStyle1, CMD_ACTIVE_STYLE_CSELECT, MCMD, 1, NOMASK, ON, "~1"},
    {Icon, ICONID_LineStyle2, CMD_ACTIVE_STYLE_CSELECT, MCMD, 2, NOMASK, ON, "~2"},
    {Icon, ICONID_LineStyle3, CMD_ACTIVE_STYLE_CSELECT, MCMD, 3, NOMASK, ON, "~3"},
    {Icon, ICONID_LineStyle4, CMD_ACTIVE_STYLE_CSELECT, MCMD, 4, NOMASK, ON, "~4"},
    {Icon, ICONID_LineStyle5, CMD_ACTIVE_STYLE_CSELECT, MCMD, 5, NOMASK, ON, "~5"},
    {Icon, ICONID_LineStyle6, CMD_ACTIVE_STYLE_CSELECT, MCMD, 6, NOMASK, ON, "~6"},
    {Icon, ICONID_LineStyle7, CMD_ACTIVE_STYLE_CSELECT, MCMD, 7, NOMASK, ON, "~7"},
    }
    };


DItem_OptionButtonRsc OPTIONBUTTONID_DialogLineWeight =
    {
    SYNONYMID_DialogElement, NOHELP, MHELP, 
    HOOKITEMID_Choose, OPTNBTNATTR_NEWSTYLE | NOARG, 
    "~Weight:", "weight", 
    {
    {Icon, ICONID_LineWeight0, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 0, NOMASK, ON, "~0"},
    {Icon, ICONID_LineWeight1, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 1, NOMASK, ON, "~1"},
    {Icon, ICONID_LineWeight2, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 2, NOMASK, ON, "~2"},
    {Icon, ICONID_LineWeight3, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 3, NOMASK, ON, "~3"},
    {Icon, ICONID_LineWeight4, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 4, NOMASK, ON, "~4"},
    {Icon, ICONID_LineWeight5, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 5, NOMASK, ON, "~5"},
    {Icon, ICONID_LineWeight6, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 6, NOMASK, ON, "~6"},
    {Icon, ICONID_LineWeight7, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 7, NOMASK, ON, "~7"},
    {Icon, ICONID_LineWeight8, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 8, NOMASK, ON, "~8"},
    {Icon, ICONID_LineWeight9, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 9, NOMASK, ON, "~9"},
    {Icon, ICONID_LineWeight10, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 10, NOMASK, ON, "~10"},
    {Icon, ICONID_LineWeight11, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 11, NOMASK, ON, "~11"},
    {Icon, ICONID_LineWeight12, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 12, NOMASK, ON, "~12"},
    {Icon, ICONID_LineWeight13, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 13, NOMASK, ON, "~13"},
    {Icon, ICONID_LineWeight14, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 14, NOMASK, ON, "~14"},
    {Icon, ICONID_LineWeight15, CMD_ACTIVE_WEIGHT_CSELECT, MCMD, 15, NOMASK, ON, "~15"},
    }
    };


/*----------------------------------------------------------------------+
|                                                                       |
|   Color Picker Item Resource                                          |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ColorPickerRsc COLORPICKERID_DialogElement =
    {
    NOCMD, LCMD, SYNONYMID_DialogElement, NOHELP, MHELP,
    HOOKITEMID_Choose, NOARG,
    TEXTID_DialogElement, NOMASK,
    "",
    "color"
    };

DItem_TextRsc TEXTID_DialogElement =
    {
    NOCMD, LCMD, SYNONYMID_DialogElement, NOHELP, MHELP,
    HOOKITEMID_Choose, NOARG,
    3, "%-ld", "%ld", "0", "253", NOMASK, NOCONCAT,
    "~Color:",
    "color"
    };
        

/*----------------------------------------------------------------------+
|                                                                       |
|   Push Button Item Resource Definitions                               |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_DialogDone =
   {
   DEFAULT_BUTTON, NOHELP, MHELP, 
   HOOKITEMID_Done, NOARG, NOCMD, MCMD, "", 
   "~Done"
   };

DItem_PushButtonRsc PUSHBUTTONID_DialogCancel =
   {
   DEFAULT_BUTTON, NOHELP, MHELP, 
   HOOKITEMID_Cancel, NOARG, NOCMD, LCMD, "", 
   "~Cancel"
   };



