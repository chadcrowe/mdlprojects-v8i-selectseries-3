/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ListGI/ListGI.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   ListGI  $
|   $Revision: 1.1.12.1 $
|       $Date: 2013/07/01 20:35:35 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ListGI - ListGI source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#if !defined (__ListGIH__)
#define     __ListGIH__


#define DIALOGID_MDLDialog          1
#define HOOKDIALOGID_MDLDialog      (DIALOGID_MDLDialog * 100) + 1
#define DIALOGID_HostedDialog       2
#define HOOKDIALOGID_HostedDialog   (DIALOGID_HostedDialog * 100) + 1



//#define PUSHBUTTONID_ModalDialog      2
#define PUSHBUTTONID_ModelessDialog     3
#define PUSHBUTTONID_HostedDialog       4
#define PUSHBUTTONID_MDLButton          10
#define PUSHBUTTONID_DockableDialog     5
#define PUSHBUTTONID_ToolSettingsDialog 6




#define MESSAGELISTID_Commands      1
#define MESSAGELISTID_Messages      2
#define MESSAGELISTID_Prompts       3
#define MESSAGELISTID_Misc          4
#define MESSAGEID_ResourceLoadError 5

#define MESSAGEID_ToolSettings  1
#define MESSAGEID_abc           2
#define COMMANDID_PlaceDate     3

#define PROMPTMESSAGEID_abc     1
#define PROMPTID_EnterPoint     2

#define MISCMESSAGEID_abc       1


#define DIALOGID_CellExp            3

#define     TREEID_CellList             1
#define     LISTBOXID_CellListing       2

#define     CELLLISTMODELCOLS           7

#define     CELLNAMECOL                 0
#define     CELLDESCCOL                 1
#define     CELLTYPECOL                 2
#define     CELLTHREEDCOL               3
#define     CELLLOCKEDCOL               4
#define     CELLHIDDENCOL               5
#define     CELLMODELIDCOL              6

#define     SASHID_VDivider             1

#define     CTPANELID_Detail            1

#define     CONTAINER_TextInfo          1
#define     CONTAINERID_CellListing     2
#define     CONTAINERID_CellIconView    3
#define         CONTAINERID_SingleColumn        4
#define         CONTAINERID_MultiColumn         5
#define     CONTAINERID_CellElemView    6



#define     DILISTID_SimpleInfo         1
#define     DILISTID_CellListing        2
#define     DILISTID_CellIconView       3
#define     DILISTID_CellElemView       4

#define     TEXTID_CellName             1
#define     TEXTID_CellDescription      2

#define     HOOKITEMID_Tree             1
#define     HOOKITEMID_Sash_VDivider    2
#define     HOOKITEMID_CTPanel          3
#define     HOOKITEMID_CellName         4
#define     HOOKID_Dialog               5
#define     HOOKITEMID_GenericCellPreview   6
#define     HOOKITEMID_Choose  7
#define     HOOKITEMID_CellListing      8
#define     HOOKITEMID_GenericCellIconView  9
#define     HOOKITEMID_CellIconContainer    10
#define     HOOKITEMID_Done             11
#define     HOOKITEMID_Cancel             12

#define     GENERICID_CellPreview       1
#define     GENERICID_CellIcon          2
#define     GENERICID_CellElem          3


//from treexmpl
#define LISTBOXID_SingleColumn          3
#define LISTBOXID_MultiColumn           4
#define DILISTID_SingleColumn           4
#define DILISTID_MultiColumn            5




/*----------------------------------------------------------------------+
|                                                                       |
|   Icon ID's                                                           |
|                                                                       |
+----------------------------------------------------------------------*/
#define ICONID_Element                  1
#define ICONID_Container                2

#define LINE                                    1
#define ELLIPSE                                 2
#define RECTANGLE                               3
#define CYLINDER                   4
#define SPHERES                 5
#define CONE                   6

#define COLORPICKERID_DialogElement             1
#define TEXTID_DialogElement               3
#define SYNONYMID_DialogElement             1
#define OPTIONBUTTONID_DialogLineStyle    1
#define OPTIONBUTTONID_DialogLineWeight   2
#define PUSHBUTTONID_DialogDone          1
#define PUSHBUTTONID_DialogCancel          2
#define COMMANDID_PlaceElement           4



#if !defined (resource)
typedef struct treexmplinfo
    {
        MSWChar *pwDisplayText;
    int             containerId;
    void            *pTreeModel;
    } TreeXmplInfo;
#endif

#endif