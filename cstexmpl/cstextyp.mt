/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cstexmpl/cstextyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/vault/cstexmpl/cstextyp.mtv  $
|   $Workfile:   cstextyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:36:19 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	New Version 1.0 dialog Combo Spin Tab items example dialog box structures	|
|									|
+----------------------------------------------------------------------*/

#include    "cstexmpl.h"
    
publishStructures (cstinfo);
