/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cstexmpl/english/cstexmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   cstexmsg.r  $
|   $Revision: 1.2.76.1 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "cstexmpl.h"

/*----------------------------------------------------------------------+
|									|
|   Messages List Resource Definition					|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_Messages =
{
    {
    {MESSAGEID_DialogOpenError,		"Unable to create/open Dialog CSTEXMPL"},
    {MESSAGEID_ResourceLoadError,	"Resource could not be loaded"},
    }
};

