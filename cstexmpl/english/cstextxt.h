/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cstexmpl/english/cstextxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   cstextxt.h - language specific definitions 			    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__cstextxtH__)
#define __cstextxtH__

/*----------------------------------------------------------------------+
|																		|
|   Defines																|
|		   																|
+----------------------------------------------------------------------*/
#define TXT_DialogTitle		    "CST Example"
#define TXT_Dynamic		    	"Dynamic"
#define TXT_Static		    	"Static"
#define TXT_FontName	    	"Font Name"
#define TXT_Insert	    		"Editable"
#define TXT_ComboBox1	    	"ComboBox 1:"
#define TXT_States		    	"States:"
#define TXT_Code	    		"Code"
#define TXT_StateName	    	"State Name"
#define TXT_ComboBox3	    	"#3:"

#define TXT_Min			    	"Min"
#define TXT_Max			    	"Max"
#define TXT_Increment	    	"Increment"
#define TXT_Format		    	"Format"
#define	TXT_SpinBoxSettings		"Spin Box & Settings"

#define TXT_NULL		    	""

#define	TXT_TabsTop     		"~Top Tabs"
#define	TXT_TabsBottom     		"~Bottom Tabs"
#define	TXT_TabsLeft     		"~Left Tabs"
#define	TXT_TabsRight     		"~Right Tabs"

#define	TXT_SpinBoxLabel   		"Spin Box Label:"
#define	TXT_SetDisplayRows   	"Display for Item Above:"
#define	TXT_SetListWidth	   	"List Width for Item Above:"
#define	TXT_SetGapWidth			"Gap Width for Item Above:"

#endif /* #if !defined (__cstextxtH__) */
