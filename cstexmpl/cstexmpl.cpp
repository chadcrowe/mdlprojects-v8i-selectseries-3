/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cstexmpl/cstexmpl.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   cstexmpl.mc  $
|   $Revision: 1.1.32.1 $
|	$Date: 2013/07/01 20:36:12 $
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Include Files							|
|									|
+----------------------------------------------------------------------*/

#include    <stdlib.h>
#include    <stdio.h>
#include    <string.h>
#include    <mdl.h>
#include    <tcb.h>
#include    <mselems.h>
#include    <global.h>
#include    <scanner.h>
#include    <msdefs.h>
#include    <userfnc.h>
#include    <cexpr.h>
#include    <cmdlist.h>
#include    <rscdefs.h>
#include    <dlogitem.h>
#include    <dlogids.h>
#include    <mdlerrs.h>

#include    <dlogman.fdf>
#include    <mselemen.fdf>
#include    <mslocate.fdf>
#include    <mssystem.fdf>
#include    <msstate.fdf>
#include    <msoutput.fdf>
#include    <mscexpr.fdf>
#include    <msparse.fdf>
#include    <msrsrc.fdf>
#include    <listModel.fdf>
#include    "cstexmpl.h"

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
CSTInfo		MYinfo;


/*----------------------------------------------------------------------+
|									|
| name		checkItem						    |
|									|
| author	BSI					3/97		|
|									|
+----------------------------------------------------------------------*/
Private	int	checkItem
(
)
    {
    BoolInt	    status;
    DialogItemHandlerInfo *itemHandlerPP;
    long	    itemType;	// defined in dlogbox.h

    itemType = RTYPE_ComboBox;	// check to see	if the ComboBox	item exist

    status = mdlDialog_itemHandlerGetInfo (	/* TRUE	if error; 5.0 */
					   &itemHandlerPP,	/* <= handler info */
					   itemType);	/* => item type	to get info on */
    if (status == TRUE)
	return (status);

    itemType = RTYPE_SpinBox;	// check to see	if the SpinBox item exist

    status = mdlDialog_itemHandlerGetInfo (	/* TRUE	if error; 5.0 */
					   &itemHandlerPP,	/* <= handler info */
					   itemType);	/* => item type	to get info on */
    if (status == TRUE)
	return (status);

    itemType = RTYPE_TabPage;	// check to see	if the TabPage item exist

    status = mdlDialog_itemHandlerGetInfo (	/* TRUE	if error; 5.0 */
					   &itemHandlerPP,	/* <= handler info */
					   itemType);	/* => item type	to get info on */
    return (status);
    }

/*----------------------------------------------------------------------+
|									|
| name		setComboItemValue									|
|									|
| author	BSI					02/97								|
|									|
+----------------------------------------------------------------------*/
Public int	setComboItemValue
(
DialogItem     *diP,		/* => a	ptr to a dialog	item  */
DialogBox      *dbP		/* => a	ptr to a dialog	box  */
)
    {
    //StringList	   *strListP;
    ListModel      *listModelP;
  //  char	   *stringValueP; //stringP[33];
    int		    valueChangedP, stateChangedP, indexP;
    int		    status = TRUE;	/* this	means an error */
    ValueDescr      valP;
    ListCell        *pCell;

    //strListP = mdlDialog_comboBoxGetStrListP (diP->rawItemP);
    listModelP = mdlDialog_comboBoxGetListModelP (diP->rawItemP);

    mdlDialog_itemGetValue (&valP.formatType, &valP.value, NULL , dbP, diP->itemIndex, 33);
    
    
    if (MDLERR_NOMATCH ==  mdlListModel_search (listModelP,&valP,NULL,0,-1,0,&indexP)
   /* mdlStringList_binarySearchByColumn (&indexP,
					strListP, stringP, NULL, NULL, 1, 0)*/
					)
	{
        ValueDescr  valLMP;
	/* stringP is not in strListP so set stringValueP to be	the
	   first entry in the string list */
	//mdlStringList_getMember	(&stringValueP,	NULL, strListP,	0);
        pCell = mdlListModel_getCellAtIndexes (listModelP,0,0);
        mdlListCell_getValue (pCell,&valLMP);

	mdlValueDescr_dup (&valP, &valLMP);
	}

    /* set the internal	value of the specified dialog box item */
    mdlDialog_itemSetValue (&valueChangedP, valP.formatType, NULL,valP.value.charPFormat,
			    dbP, diP->itemIndex);
    /*force the	specified dialog box item's external state to match its
       internal	value */
    if (valueChangedP)
	status = mdlDialog_itemSetState	(&stateChangedP, dbP, diP->itemIndex);

    return status;

    }

/*----------------------------------------------------------------------+
|									|
| name		initFontNameList											|
|									|
| author	BSI				    02/97									|
|									|
+----------------------------------------------------------------------*/
Private	int	initFontNameList
(
//StringList    **strListP	/* <=> string list to populate */
ListModel       **pListModel
)
    {
    int		    status=0;
    int		    fontNum, numActive = 0;
    char	    fontName[32];

    /* Get the number of members before	creating the string list */
    for	(fontNum = 0; fontNum <	256; fontNum++)
	{
	if (mdlText_getFontName	(fontName, fontNum))	/* TRUE	if font	exist */
	    numActive++;
	}

    /* Creating	the string list	at its initial size will save memory and time 
       by not using mdlStringList_insertMember */
    //*strListP =	mdlStringList_create (numActive, 1);
    *pListModel= mdlListModel_create (1);
    numActive =	0;		/*initial back to zero */

    /* populate	the string list	with mdlStringList_setMember */
    for	(fontNum = 0; fontNum <	256; fontNum++)
	{
	if (mdlText_getFontName	(fontName, fontNum))
	    {
      //	    status = mdlStringList_setMember (*strListP, numActive, fontName, NULL);
            ListRow *pRow;
            ListCell *pCell;

            pRow = mdlListRow_create (*pListModel);
            pCell = mdlListRow_getCellAtIndex (pRow,0);
            mdlListCell_setStringValue(pCell,fontName,TRUE);
            mdlListModel_addRow (*pListModel,pRow);

	    numActive++;	/* use this to increment the string list index */

	    if (status != SUCCESS)
		break;
	    }
	}
    return status;
    }

/*----------------------------------------------------------------------+
|									|
| name		setFontNameComboBoxList								|
|									|
| author	BSI					02/97								|
|									|
+----------------------------------------------------------------------*/
Public int	setFontNameComboBoxList
(
DialogItem     *diP		/* => a	ptr to a dialog	item  */
)
    {
    //StringList	   *strListP = NULL;
    ListModel               *pListModel = NULL;
    if (diP->rawItemP)
	{
	if (SUCCESS == initFontNameList	(&pListModel))
	    {
	    //mdlDialog_comboBoxSetStrListP (diP->rawItemP, strListP, 1);
            mdlDialog_comboBoxSetListModelP (diP->rawItemP,pListModel);
	    return SUCCESS;
	    }
	}
    return ERROR;
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_extentSet					|
|									|
| author	BSI					02/97		|
|									|
+----------------------------------------------------------------------*/
Private	void	item53_extentSet
(
DialogBox      *dbP		/* => a	ptr to a dialog	box  */
)
    {
    int		    height = 0,	width =	0, numItems, i;
    DialogItem	   *diP;

    numItems = mdlDialog_itemsGetNumberOf (dbP);
    for	(i = 0;	i < numItems; i++)
	{
	if (diP	= mdlDialog_itemGetByIndex (dbP, i))
	    if (!diP->attributes.hidden)
		{
		if ((diP->rect.corner.x	> width) && (diP->type != RTYPE_MenuBar))
		    width = diP->rect.corner.x;
		if (diP->rect.corner.y > height)
		    height = diP->rect.corner.y;
		}

	if (diP->type == RTYPE_TabPageList || diP->type	== RTYPE_MultilineText)
	    {
	    if (diP->rawItemP->itemRect.corner.x > width)
		width =	diP->rawItemP->itemRect.corner.x;
	    if (diP->rawItemP->itemRect.corner.y > height)
		height = diP->rawItemP->itemRect.corner.y;
	    }
	}
    /* Resize the dialog box based upon	current	item */
    width += 10;
    height += 10;

    mdlWindow_extentSet((GuiWindowP) dbP, width, height);
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_tabPageRadioBtnHook				|
|									|
| author	BSI					02/97		|
|									|
+----------------------------------------------------------------------*/
Private	void	item53_tabPageRadioBtnHook
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    UInt32	    attributes;
    int		    numPages, firstPage, lastPage;
    int		    oldPlacement, newPlacement;
    DialogItem	   *listDiP = NULL;
    oldPlacement = newPlacement = 1;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
		{

		dimP->msgUnderstood = TRUE;

		listDiP	= mdlDialog_itemGetByTypeAndId (dimP->db,
					RTYPE_TabPageList, TPLISTID_ONE, 0);
		if (listDiP)
		    {
		    if (mdlDialog_tabPageListGetInfo (&numPages, &firstPage,
				      &lastPage, NULL, NULL, &attributes,
				      listDiP->rawItemP) == 0)
			{
			if (attributes & TABATTR_TABSBOTTOM)
			    oldPlacement = 2;

			else if	(attributes & TABATTR_TABSLEFT)
			    oldPlacement = 3;

			else if	(attributes & TABATTR_TABSRIGHT)
			    oldPlacement = 4;

			else
			    oldPlacement = 1;

			}

		    attributes &= !(TABATTR_TABSBOTTOM);
		    attributes &= !(TABATTR_TABSLEFT);
		    attributes &= !(TABATTR_TABSRIGHT);
		    attributes &= !(TABATTR_TABSFIXEDWIDTH);
		    attributes &= !(TABATTR_TABSFITPAGEWIDTH);

		    switch (MYinfo.tabPlacement	/*rbDiP->id */ )
			{
			case RBUTTONID_TabsTop:
				{
				newPlacement = 1;
				attributes |= TABATTR_DEFAULT;
				break;
				}

			case RBUTTONID_TabsBottom:
				{
				newPlacement = 2;
				attributes |=	TABATTR_TABSBOTTOM |
						TABATTR_TABSFITPAGEWIDTH;
				break;
				}

			case RBUTTONID_TabsLeft:
				{
				newPlacement = 3;
				attributes |=	TABATTR_TABSLEFT |
						TABATTR_TABSFIXEDWIDTH |
						TABATTR_LABELLEFTJUSTIFY;
				break;
				}

			case RBUTTONID_TabsRight:
				{
				newPlacement = 4;
				attributes |=	TABATTR_TABSRIGHT |
						TABATTR_TABSFIXEDWIDTH |
						TABATTR_LABELRIGHTJUSTIFY;
				break;
				}

			default:
				{
				dimP->msgUnderstood = FALSE;
				break;
				}
			}

		    if (dimP->msgUnderstood && newPlacement != oldPlacement)
			{

			/* Reset the attributes	for the	selected tab placement */
			mdlDialog_tabPageListSetInfo (listDiP->rawItemP, NULL,
						      NULL, &attributes, FALSE);

			/* Reset the extents for the dialog box	*/
			item53_extentSet (dimP->db);
			}
		    }
		else
		    dimP->msgUnderstood	= FALSE;

		break;
		}

	default:
	    dimP->msgUnderstood	= FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_spinBoxSetDRows								|
|									|
| author	BSI					02/97								|
|									|
+----------------------------------------------------------------------*/
Public void	item53_spinBoxSetDRows
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    dimP->msgUnderstood	= TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
		{
		DialogItem     *diP = NULL;
		RawItemHdr     *riP = NULL;
		UShort		gapWidth;
		gapWidth = (UShort) MYinfo.GapWidth;

		if ((diP = mdlDialog_itemGetByTypeAndId	(dimP->db,
					RTYPE_ComboBox,	COMBOBOXID_Insert, 0)))
		    riP	= diP->rawItemP;

		if (riP)
		    mdlDialog_comboBoxSetInfo ( NULL, NULL, NULL, NULL, NULL,
					    NULL, NULL, NULL, NULL, &gapWidth,
					    NULL, NULL, TRUE, riP );
		break;
		}

	default:
		{
		dimP->msgUnderstood = FALSE;
		break;
		}
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_SpinBoxTest1										|
|									|
| author	BSI					02/97								|
|									|
+----------------------------------------------------------------------*/
Public void	item53_SpinBoxTest1
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    RawItemHdr	   *rihP = NULL;

    dimP->msgUnderstood	= TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_INIT:
		{
		rihP = dimP->dialogItemP->rawItemP;
		if (rihP)
		    {
		    if (!mdlDialog_spinBoxGetInfo (NULL, NULL, NULL,
					    MYinfo.FormatSpin, NULL,
					    &MYinfo.MinSpin, &MYinfo.MaxSpin,
					    &MYinfo.IncSpin, NULL, NULL,rihP ))

			/* I do not need to call the mdlDialog_itemsSynch function
			   here	because	the text items that use	these access strings are
			   created after the spin box item.  If	you were to specify 
			   the spin box	after the text itmes in	the item list resource
			   you would then need to call mdlDialog_itemsSynch */

			mdlDialog_itemsSynch (dimP->db);
		    }

		break;
		}
	default:
		{
		dimP->msgUnderstood = FALSE;
		break;
		}
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_TextItemSetSpinSettings				|
|									|
| author	BSI					02/97		|
|									|
+----------------------------------------------------------------------*/
Public void	item53_TextItemSetSpinSettings
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    char	    buffer[50];
    RawItemHdr	   *rihP = NULL;
    DialogItem	   *diP	= NULL;

    dimP->msgUnderstood	= TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
		{
		diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_SpinBox,
						    SPINBOXID_Test1, 0 );
		if (diP)
		    rihP = diP->rawItemP;

		switch (dimP->dialogItemP->id)
		    {

		    case TEXTID_Min:
			    {
			    if (MYinfo.MinSpin >= MYinfo.MaxSpin )
				{
				sprintf	(buffer, "Min value is greater than \
						    or\n equal to Max value");
				mdlDialog_openInfoBox (buffer );

				mdlDialog_spinBoxGetInfo (NULL,	NULL, NULL, NULL,
						    NULL, NULL, &MYinfo.MinSpin,
						    NULL, NULL, NULL, rihP );
				MYinfo.MinSpin = MYinfo.MaxSpin;
				mdlDialog_itemSynch (dimP->db, dimP->itemIndex);
				}

			    mdlDialog_spinBoxSetInfo (NULL, NULL, NULL,	NULL, NULL,
						    &MYinfo.MinSpin, NULL, NULL,
						    NULL, NULL, TRUE, rihP);
			    break;
			    }
		    case TEXTID_Max:
			    {
			    if (MYinfo.MaxSpin <= MYinfo.MinSpin )
				{
				sprintf	(buffer, "Max value is Less than \
						    or\n equal to Min value");
				mdlDialog_openInfoBox (buffer );

				mdlDialog_spinBoxGetInfo (NULL,	NULL, NULL, NULL,
							  NULL,	&MYinfo.MinSpin,
							  NULL,	NULL, NULL, NULL,
							  rihP );
				MYinfo.MaxSpin = MYinfo.MinSpin;
				mdlDialog_itemSynch (dimP->db, dimP->itemIndex );
				}

			    mdlDialog_spinBoxSetInfo (NULL, NULL, NULL,	NULL, NULL,
						    NULL, &MYinfo.MaxSpin, NULL,
						    NULL, NULL, TRUE, rihP);

			    break;
			    }
		    case TEXTID_Inc:
			    {

			    mdlDialog_spinBoxSetInfo (
						    NULL, NULL, NULL, NULL, NULL,
						    NULL, NULL, &MYinfo.IncSpin,
						    NULL, NULL, TRUE, rihP);

			    break;
			    }
		    case TEXTID_Format:
			    {

			    mdlDialog_spinBoxSetInfo (NULL, NULL, NULL,
						    MYinfo.FormatSpin, NULL,
						    NULL, NULL, NULL, NULL,
						    NULL, TRUE,	rihP);

			    break;
			    }

		    default:
			    {
			    mdlOutput_printf (MSG_ERROR,"Should have never received	this message\n");
			    break;
			    }
		    }

		break;
		}

	default:
		{
		dimP->msgUnderstood = FALSE;
		break;
		}
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_FontNameComboBoxHook							|
|									|
| author	BSI					02/97								|
|									|
+----------------------------------------------------------------------*/
Public void	item53_FontNameComboBoxHook
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    dimP->msgUnderstood	= TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_FOCUSIN:
		{
		RawItemHdr     *riP;
		UShort		nRows;
		UShort		listWidth;

		nRows =	(UShort) MYinfo.DisplayRows;
		listWidth = (UShort) MYinfo.ListWidth *	XC;

		riP = dimP->dialogItemP->rawItemP;
		if (riP)
		    {
		    mdlDialog_comboBoxSetInfo (NULL, NULL, NULL, NULL, NULL, NULL,
					       NULL, NULL, &nRows, NULL,
					       &listWidth, NULL, FALSE,	riP);
		    }
		break;
		}

	case DITEM_MESSAGE_STATECHANGED:
		{
		char		valueStr[32];

		mdlDialog_itemGetValue (NULL, NULL, valueStr, dimP->db,
					dimP->itemIndex, 32);
		break;
		}

	default:
		{
		dimP->msgUnderstood = FALSE;
		break;
		}
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_toggleEnableStateHook				|
|									|
| author	BSI					02/97		|
|									|
+----------------------------------------------------------------------*/
Public void	item53_toggleEnableStateHook
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    DialogItem	   *diP;
    int		    status;

    dimP->msgUnderstood	= TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
		{
		ValueUnion	value;

		mdlDialog_itemGetValue (NULL, &value, NULL, dimP->db,
					dimP->itemIndex, 0 );

		if ((diP = mdlDialog_itemGetByTypeAndId	(dimP->db,
				    RTYPE_ComboBox, COMBOBOXID_FontName, 0 )))
		    {
		    mdlDialog_itemSetEnabledState (dimP->db, diP->itemIndex,
					   value.sLongFormat & 0x01, FALSE );

		    if (value.sLongFormat )  /* process only if iteme is enabled */
			{

			status = setFontNameComboBoxList (diP );

			if (status == SUCCESS)
			    setComboItemValue (diP, dimP->db );
			}
		    }

		break;
		}


	default:
		{
		dimP->msgUnderstood = FALSE;
		break;
		}
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_comboBoxHook     				|
|						    			|
| author	BSI		2/97					|
|						    			|
+----------------------------------------------------------------------*/
Private	void	item53_comboBoxHook
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    dimP->msgUnderstood	= TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
		{
		RawItemHdr     *riP;
		int		iTest;
		//StringList     *strListP;
                ListModel       *pListModel;
		char		buffer[20];

		riP = dimP->dialogItemP->rawItemP;
		if (riP)
		    {
		    //strListP = mdlStringList_create (8,	1);

		    pListModel = mdlListModel_create (1);
		    //mdlDialog_comboBoxSetStrListP (riP,	strListP, 2);
                    mdlDialog_comboBoxSetListModelP (riP,pListModel);
		    if (pListModel)
			{
                        ListRow *pRow;
                        ListCell *pCell;

		      //	int		rc;
			
			for (iTest = 0;	iTest <= 20; iTest++)
			    {
			    sprintf (buffer, "%d", iTest * 5);
                        
                            pRow = mdlListRow_create(pListModel);
                            pCell= mdlListRow_getCellAtIndex (pRow,0);
                            mdlListCell_setStringValue (pCell, buffer,TRUE);
			    mdlListModel_addRow (pListModel,pRow);
			    /*rc = mdlStringList_setMember (strListP, iTest,
							  buffer, NULL);
			    if (rc != SUCCESS)
				{
				long		index;
				mdlStringList_insertMember (&index, strListP,
							    iTest, 1);

				rc = mdlStringList_setMember (strListP,	iTest,
							      buffer, NULL);

				if (rc != SUCCESS)
				    mdlOutput_printf (MSG_ERROR,"NewItems: Unable to add string \
					    to listbox string list\n");
				}*/
			    }
			}
		    }

		break;
		}

	case DITEM_MESSAGE_GETSTATE:
		{

		dimP->u.value.formatType = FMT_DPFP;
		dimP->u.value.value.doubleFormat = 50;
		dimP->u.value.hookHandled = TRUE;

		break;
		}

	case DITEM_MESSAGE_STATECHANGED:
		{
		char		valueStr[41];

		mdlDialog_itemGetValue (NULL, NULL, valueStr,
					dimP->db, dimP->itemIndex, 40);

		break;
		}
         case DITEM_MESSAGE_DESTROY:
            {
            ListModel   *pListModel = NULL;
            pListModel = mdlDialog_comboBoxGetListModelP (dimP->dialogItemP->rawItemP);
            mdlListModel_destroy (pListModel,TRUE);
            break;
            }
	default:
	    dimP->msgUnderstood	= FALSE;

	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		item53_comboBoxStatesHook				|
|									|
| author	BSI		    2/97				|
|									|
+----------------------------------------------------------------------*/
Private	void	item53_comboBoxStatesHook
(
DialogItemMessage *dimP		/* => a	ptr to a dialog	item message */
)
    {
    RawItemHdr	   *riP	= dimP->dialogItemP->rawItemP;

    dimP->msgUnderstood	= TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_FOCUSIN:
		{
		if (riP	&& dimP->u.focusIn.focusOutType	== FOCUSOUT_KEYSWITCH)
		    mdlDialog_comboBoxSetPopupState (riP, TRUE);

		break;
		}

	case DITEM_MESSAGE_GETSTATE:
		{

		dimP->u.value.formatType = FMT_LONG;
		dimP->u.value.value.sLongFormat	= 74;
		dimP->u.value.hookHandled = TRUE;

		break;
		}

	case DITEM_MESSAGE_STATECHANGED:
		{
		int		format;
		ValueUnion	value;
		char		valueStr[40];

		format = FMT_LONG;
		mdlDialog_itemGetValue (&format, &value, valueStr, dimP->db,
					dimP->itemIndex, 40);
		/*printf ("Index of new	state: %d\n", value.sLongFormat); */

		break;
		}

	default:
	    dimP->msgUnderstood	= FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|																		|
| name		item53_dialogHook										|
|																		|
| author	BSI				2/97										|
|																		|
+----------------------------------------------------------------------*/
Public void	item53_dialogHook
(
DialogMessage  *dmP
)
    {
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_DESTROY:
		{
		mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
					  mdlSystem_getCurrTaskID (), TRUE);
		break;
		}

	default:
		{
		dmP->msgUnderstood = FALSE;
		break;
		}
	}
    }


Private	DialogHookInfo uHooks[]	=
{
 {HOOKDIALOGID_CSTExmpl,	(PFDialogHook)item53_dialogHook},
 {HOOKITEMID_FontNameComboBox,  (PFDialogHook)item53_FontNameComboBoxHook},
 {HOOKITEMID_ToggleComboBox,    (PFDialogHook)item53_toggleEnableStateHook},
 {HOOKITEMID_ComboStates,       (PFDialogHook)item53_comboBoxStatesHook},
 {HOOKITEMID_ComboBoxes,	(PFDialogHook)item53_comboBoxHook},
 {HOOKITEMID_TabPageRadioBtns,  (PFDialogHook)item53_tabPageRadioBtnHook},
 {HOOKITEMID_SpinBoxSetDRows,   (PFDialogHook)item53_spinBoxSetDRows},
 {HOOKITEMID_TextItem,		(PFDialogHook)item53_TextItemSetSpinSettings},
 {HOOKITEMID_SpinBoxTest1,      (PFDialogHook)item53_SpinBoxTest1},
};

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI					3/97		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int		argc,
char	       *argv[]
)
    {
    SymbolSet	   *setP;	/* a ptr to a "C expression symbol set"	*/
    RscFileHandle   rscFileH;	/* a resource file handle */

    if (SUCCESS	!= checkItem ())
	{
	mdlDialog_openInfoBox ("Config variable	MS_GUIHAND may not be defined \
Please set \"MS_GUIHAND	= ditem53\"  This should be set	in a file in the \
$(MS)/CONFIG/SYSTEM directory. Also make sure the ditem53.ma and ditem53.msl \
are in the $(MS)/MDLAPPS directory ");

	mdlOutput_printf (MSG_MESSAGE, "cstexmpl unload");

	/* force unload	the application	*/
	//mdlSystem_exit (0, 1);
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				  mdlSystem_getCurrTaskID (), TRUE);
	}

/*-----------------------------------------------------------------------
mdlResource_openFile:

This function opens a resource file, thus making its contents
available to the application.  In this case, we	need to	open
CSTEXMPL.MA as a resource file so that we have access to the the
string lists for our messages.					       
-----------------------------------------------------------------------*/
    if (SUCCESS	!= mdlResource_openFile	(&rscFileH, NULL, 0))
	{
	mdlOutput_rscPrintf (MSG_ERROR,	NULL, MESSAGELISTID_Messages,
			     MESSAGEID_ResourceLoadError);

	/* unload the application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				  mdlSystem_getCurrTaskID (), TRUE);
	}

/*-----------------------------------------------------------------------
To make	the Dialog Box Manager aware of	the function pointers
that we	have equated to	our hook ids we	need to	publish	the
DialogHookInfo structure, uHooks.
-----------------------------------------------------------------------*/
    mdlDialog_hookPublish (sizeof(uHooks) / sizeof(DialogHookInfo), uHooks);

    mdlState_registerStringIds (MESSAGELISTID_Messages,	MESSAGELISTID_Messages);

    /* set up the variables we are going to set	from dialog boxes */
    setP = mdlCExpression_initializeSet	(VISIBILITY_DIALOG_BOX,	0, FALSE);
    mdlDialog_publishComplexVariable (setP, "cstinfo", "MYinfo", &MYinfo);

    MYinfo.DisplayRows = 5;	/* number of rows to display in	combo box pop down */
    MYinfo.ListWidth = 20;	/* width of string list	in combo box pop down */

    MYinfo.tabPlacement	= 1;	/* set radio button to show tabs top selected */

    /* open the	toolbox	*/
    if (NULL ==	mdlDialog_open (NULL, DIALOGID_CSTExmpl))
	{
	mdlOutput_rscPrintf (MSG_ERROR,	NULL, MESSAGELISTID_Messages,
			     MESSAGEID_DialogOpenError);

	/* unload this application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				  mdlSystem_getCurrTaskID (), TRUE);
	}
    return  SUCCESS;
    }				/* Finish main function	*/
