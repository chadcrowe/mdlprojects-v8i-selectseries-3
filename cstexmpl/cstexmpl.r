/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cstexmpl/cstexmpl.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   cstexmpl.r -  Combo, Spin, and Tab Example resource definitions	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include <cmdlist.h>

#include "cstexmpl.h"
#include "cstextxt.h"
#include "cstexcmd.h"

#define WIDTH       60* XC
#define HEIGHT      28* YC

#define X1	    1 * XC

#define Y1	    GENY(1)

#define TabPageW    52 * XC

/*----------------------------------------------------------------------+
|									|
|   Dialog Box Resource Specification					|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_CSTExmpl = 
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE | DIALOGATTR_GROWABLE,
    WIDTH, HEIGHT, NOHELP, MHELP, 
    HOOKDIALOGID_CSTExmpl, NOPARENTID, 
    TXT_DialogTitle,
	{
	{{X1, Y1, TabPageW, 0}, TabPageList, TPLISTID_ONE, ON, 0,"",""},
	}
    };

DItem_TabPageListRsc  TPLISTID_ONE =
    {
    0, 0,
    NOSYNONYM, 
    NOHELP, MHELP, NOHOOK, NOARG,
    TABATTR_DEFAULT,
    TXT_DialogTitle,
	{
	{{0,0,0,0}, TabPage, TABPAGEID_DYNAMIC,	ON, 0,"",""},
	{{0,0,0,0}, TabPage, TABPAGEID_STATIC,	ON, 0,"",""},
	}
    };

#undef  WIDTH
#undef  HEIGHT

#undef  X1
#undef  Y1
#undef  TabPageW

/*-----------------------------------------------------------------------
Start of Tab Page DYNAMIC folder items.
-----------------------------------------------------------------------*/
#define X1		10 * XC
#define X2		14 * XC
#define X3		18.2 * XC
#define X4		21 * XC
#define X5		28 * XC

#define Y1		GENY(3.5)
#define Y2		GENY(6)
#define Y3		GENY(15.5)
#define Y4		GENY(16.5)
#define Y5		GENY(17.5)
#define Y6		GENY(11.5)
#define Y7		GENY(8.5)
#define Y8		GENY(14)

#define BW1		20 * XC
#define BW2		12 * XC
#define BH1		1.4 * YC

DItem_TabPageRsc  TABPAGEID_DYNAMIC =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    TABATTR_DEFAULT,
    NOTYPE, NOICON,
    TXT_Dynamic,
{
{{X2, Y1, 0,	0}, ToggleButton,TOGGLEBTNID_ToggleCombo,   ON,	0, "", ""},
{{X3, Y1, BW1, BH1},ComboBox,   COMBOBOXID_FontName, 	    OFF,0, "", ""},
{{X3, Y2, BW2, BH1},SpinBox,	SPINBOXID_SetDisplayRows,   ON, 0, "", ""},
{{X3, Y7, BW2, BH1},SpinBox,	SPINBOXID_SetListWidth,	    ON, 0, "", ""},

{{X3, Y6, BW1, BH1},ComboBox,	COMBOBOXID_Insert,	    ON, 0, "", ""},
{{X3, Y8, BW2, BH1},SpinBox,	SPINBOXID_SetGapWidth,	    ON, 0, "", ""},

{{X4, Y3, 0,	0}, RadioButton,RBUTTONID_TabsTop,	    ON, 0, "", ""},
{{X4, Y5, 0,	0}, RadioButton,RBUTTONID_TabsBottom,	    ON, 0, "", ""},
{{X2, Y4, 0,	0}, RadioButton,RBUTTONID_TabsLeft,	    ON, 0, "", ""},
{{X5, Y4, 0,	0}, RadioButton,RBUTTONID_TabsRight,	    ON, 0, "", ""},
}
	};

#undef  X1
#undef  X2
#undef  X3
#undef  X4
#undef  X5

#undef  Y1
#undef  Y2
#undef  Y3
#undef  Y4
#undef  Y5
#undef  Y6
#undef  Y7
#undef  Y8

#undef  BW1
#undef  BW2
#undef  BH1

/*-----------------------------------------------------------------------
End of Tab Page DYNAMIC folder items.
-----------------------------------------------------------------------*/


/*-----------------------------------------------------------------------
Start of Tab Page STATIC folder items.
-----------------------------------------------------------------------*/
#define X1		16 * XC
#define X2		13 * XC
#define X3		28 * XC
#define X4		20 * XC

#define Y1		GENY(4.5)
#define Y2		GENY(6.6)
#define Y3		GENY(8.7)
#define Y4		GENY(11.5)
#define Y5		GENY(14)
#define Y6		GENY(16.5)

#define BW1		8 * XC
#define BW2		8 * XC
#define BH1		1.4 * YC

#define TW		5* XC

DItem_TabPageRsc  TABPAGEID_STATIC =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    TABATTR_DEFAULT,
    Icon, ICONID_ComboBox,
    TXT_Static,
{
{{X1,Y1, TW*2.5,BH1}, SpinBox,  SPINBOXID_Test1, ON, 0, "", ""},

{{X1, Y2, BW1,0},  Text,	TEXTID_Min,	ON, TEXT_MASK_LABELABOVE, "", ""},
{{X3, Y2, BW1,0},  Text,	TEXTID_Max,	ON, TEXT_MASK_LABELABOVE, "", ""},
{{X1, Y3, BW1,0},  Text,	TEXTID_Inc,	ON, TEXT_MASK_LABELABOVE, "", ""},
{{X3, Y3, BW2,0},  Text,	TEXTID_Format,  ON, TEXT_MASK_LABELABOVE, "", ""},

{{X2, GENY(2.8), 25*XC+9, 9*YC},GroupBox, 0, ON, 0, TXT_SpinBoxSettings, ""},

{{X1, Y4, TW*2.7,BH1}, ComboBox,COMBOBOXID_Test1, ON, 0, "", ""},
{{X4, Y5, TW*2.4,BH1}, ComboBox,COMBOBOXID_Test2, ON, 0, "", ""},
{{X1, Y6, TW*3,  BH1}, ComboBox,COMBOBOXID_Test3, ON, 0, "", ""},

}
	};

#undef  X1
#undef  X2
#undef  X3
#undef  X4

#undef  Y1
#undef  Y2
#undef  Y3
#undef  Y4
#undef  Y5
#undef  Y6

#undef  BW1
#undef  BW2
#undef  BH1

/*-----------------------------------------------------------------------
End of Tab Page STATIC folder items.
-----------------------------------------------------------------------*/


/*----------------------------------------------------------------------+
|									|
|   ToggleButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEBTNID_ToggleCombo =
    {
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_ToggleComboBox,
    NOARG, 0x1, NOINVERT, TXT_NULL, "MYinfo.ToggleCombo"
    };


/*----------------------------------------------------------------------+
|									|
|   ComboBox Item Resources    						|
|									|
+----------------------------------------------------------------------*/
 DItem_ComboBoxRsc COMBOBOXID_FontName =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_FontNameComboBox, NOARG,
    32, "", "", "", "", NOMASK,
    0, 5, 1, 0, 0, 
    COMBOATTR_READONLY | COMBOATTR_SORT | COMBOATTR_LABELABOVE, 
    TXT_FontName,
    "MYinfo.FontName",
{
{0, 32, ALIGN_LEFT, TXT_NULL},	/* set width to 0 for dynamic width capability */
}
    };

 DItem_ComboBoxRsc COMBOBOXID_Insert =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    32, "", "", "", "", NOMASK,
    0, 5, 1, 0, 0, 
    COMBOATTR_AUTOADDNEWSTRINGS | COMBOATTR_SORT | COMBOATTR_LABELABOVE, 
    TXT_Insert,
    "MYinfo.FontName",
{
{32*XC, 32, ALIGN_LEFT, TXT_NULL},
}
    };


DItem_ComboBoxRsc COMBOBOXID_Test1 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    10, "", "", "", "", NOMASK,
    STRLISTID_ComboBox1, 20, 5, 0, 0, 
    COMBOATTR_DRAWPREFIXICON | COMBOATTR_READONLY |
	COMBOATTR_SORT | COMBOATTR_LABELABOVE, 
    TXT_ComboBox1,
    "MYinfo.comboText",
{
{TW*3, 10, 0, TXT_NULL},
}
    };

DItem_ComboBoxRsc COMBOBOXID_Test2 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_ComboStates, NOARG,
    200, "%I64u", "%I64u", "", "", NOMASK,
    STRLISTID_ComboBoxStates, 15, 0, TW*4, 0, 
    COMBOATTR_READONLY | COMBOATTR_INDEXISVALUE , 
    TXT_States,
    "MYinfo.test2",
{
{TW, 2, 0, TXT_Code},
{0, 15, 0, TXT_StateName},
}
    };

DItem_ComboBoxRsc COMBOBOXID_Test3 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_ComboBoxes, NOARG,
    12, "%.1lf", "%lf", "0.0", "100.0", NOMASK,
    0, 6, 4, 0, 0, 
    COMBOATTR_AUTOADDNEWSTRINGS, 
    TXT_ComboBox3,
    "MYinfo.test3",
{
{0, 12, 0, TXT_NULL},
}
    };

/*----------------------------------------------------------------------+
|									|
|   SpinBox Item Resources     						|
|									|
+----------------------------------------------------------------------*/
DItem_SpinBoxRsc SPINBOXID_SetDisplayRows =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    17, "%.0lf Rows", "%lf",  
    2, 16, 1,
    NOMASK, SPINATTR_LABELABOVE, 
    TXT_SetDisplayRows,
    "MYinfo.DisplayRows"
    };

DItem_SpinBoxRsc SPINBOXID_SetListWidth =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    17, "%.0lf Width", "%lf",  
    2, 32, 1,
    NOMASK, SPINATTR_LABELABOVE, 
    TXT_SetListWidth,
    "MYinfo.ListWidth"
    };
	
DItem_SpinBoxRsc SPINBOXID_SetGapWidth =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_SpinBoxSetDRows, NOARG,
    17, "%.0lf Gap", "%lf",  
    1, 20, 1,
    NOMASK, SPINATTR_LABELABOVE, 
    TXT_SetGapWidth,
    "MYinfo.GapWidth"
    };

DItem_SpinBoxRsc SPINBOXID_Test1 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_SpinBoxTest1, NOARG,
    16, "%.1lf in", "%lf",  
    1.0, 100.0, 0.5,
    NOMASK, SPINATTR_LABELABOVE, 
    TXT_SpinBoxLabel,
    "MYinfo.spinBoxValue"
    };

/*----------------------------------------------------------------------+
|																		|
|   Radio Button List Resources     									|
|																		|
+----------------------------------------------------------------------*/
DItem_RadioButtonListRsc RBLISTID_TabPlacement =
    {
{
RBUTTONID_TabsTop,
RBUTTONID_TabsBottom,
RBUTTONID_TabsLeft,
RBUTTONID_TabsRight,
}	
    };
	
/*----------------------------------------------------------------------+
|									|
|   Radio Button Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_RadioButtonRsc RBUTTONID_TabsTop = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsTop, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsTop,
    "MYinfo.tabPlacement"
    };

DItem_RadioButtonRsc RBUTTONID_TabsBottom = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsBottom, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsBottom,
    "MYinfo.tabPlacement"
    };

DItem_RadioButtonRsc RBUTTONID_TabsLeft = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsLeft, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsLeft,
    "MYinfo.tabPlacement"
    };

DItem_RadioButtonRsc RBUTTONID_TabsRight = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsRight, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsRight,
    "MYinfo.tabPlacement"
    };

/*----------------------------------------------------------------------+
|									|
|   Text Item Resources     						|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_Min = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_TextItem, NOARG, 
    6, "%.1f", "%lf", "0", "100", NOMASK, NOCONCAT,
    TXT_Min,
    "MYinfo.MinSpin"
    };

DItem_TextRsc TEXTID_Max = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_TextItem, NOARG, 
    6, "%.1lf", "%lf", "1", "100", NOMASK, NOCONCAT,
    TXT_Max,
    "MYinfo.MaxSpin"
    };

DItem_TextRsc TEXTID_Inc =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_TextItem, NOARG, 
    6, "%lf", "%lf", "0.0001", "", NOMASK, NOCONCAT,
    TXT_Increment,
    "MYinfo.IncSpin"
    };

DItem_TextRsc TEXTID_Format = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_TextItem, NOARG, 
    12, "", "", "", "", NOMASK, NOCONCAT,
    TXT_Format,
    "MYinfo.FormatSpin"
    };

/*----------------------------------------------------------------------+
|									|
|   Icon Resources		    					|
|									|
+----------------------------------------------------------------------*/
IconRsc ICONID_ComboBox =
    {
    12,    12,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0xff, 0xf8, 0x01, 0xbf, 0xd9, 0xf9, 0x8f, 0x18,
        0x61, 0x80, 0x18, 0x01, 0x9f, 0x98, 0xf1, 0x80,
        0x1f, 0xff, 
        }
    };

IconRsc ICONID_TabOptions =
    {
    12,    12,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x07, 0xfe, 0x7f, 0xe6, 0x66, 0x06, 0x00,
        0x60, 0x06, 0x00, 0x60, 0x06, 0x00, 0xf0, 0x0f,
        0x00, 0x00, 
        }
    };

IconRsc ICONID_RadioBtnOptions =
    {
    12,    12,    ICONFORMAT_FIXEDCOLORS,    BLUE_INDEX, "",
        {
        11,11,11, 9, 9, 9, 9, 9,11,11,11,11,
        11, 9, 9, 9, 0, 0, 0, 9, 9, 9,11,11,
        11, 9, 0, 0, 1, 1, 1, 1, 4, 9,11,11,
         9, 9, 0, 6, 6, 6, 6, 6, 4, 9, 9, 7,
         9, 0, 1, 6, 0, 0, 0, 6, 4,11, 9, 7,
         9, 0, 1, 6, 0,11, 0, 6, 4,11, 9, 7,
         9, 0, 1, 6, 0, 0, 0, 6, 4,11, 9, 7,
         9, 9, 1, 6, 6, 6, 6, 6, 4, 9, 9, 7,
        11, 9, 4, 4, 4, 4, 4, 4, 4, 9, 7,11,
        11, 9, 9, 9,11,11,11, 9, 9, 9, 7,11,
        11,11, 7, 9, 9, 9, 9, 9, 7, 7,11,11,
        11,11,11, 7, 7, 7, 7, 7, 7,11,11,11,
        }
    };

IconRsc ICONID_ScaleOptions =
    {
    12,    12,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0xff, 0xf9, 0x69, 0xb6, 0xdf,
        0x6f, 0xb6, 0xd9, 0x69, 0xff, 0xf0, 0x00, 0x00,
        0x00, 0x00, 
        }
    };


StringList STRLISTID_ComboBoxStates =
{
  1,
  {
    { {0}, "AL" }, { {0}, "Alabama" },
    { {0}, "AK" }, { {0}, "Alaska" },
    { {0}, "AZ" }, { {0}, "Arizona" },
    { {0}, "AR" }, { {0}, "Arkansas" },
    { {0}, "CA" }, { {0}, "California" },
    { {0}, "CO" }, { {0}, "Colorado" },
    { {0}, "CT" }, { {0}, "Connecticut" },
    { {0}, "DE" }, { {0}, "Delaware" },
    { {0}, "FL" }, { {0}, "Florida" },
    { {0}, "GA" }, { {0}, "Georgia" },
    { {0}, "HI" }, { {0}, "Hawaii" },
    { {0}, "ID" }, { {0}, "Idaho" },
    { {0}, "IL" }, { {0}, "Illinois" },
    { {0}, "IN" }, { {0}, "Indiana" },
    { {0}, "IA" }, { {0}, "Iowa" },
    { {0}, "KS" }, { {0}, "Kansas" },
    { {0}, "KY" }, { {0}, "Kentucky" },
    { {0}, "LA" }, { {0}, "Louisiana" },
    { {0}, "ME" }, { {0}, "Maine" },
    { {0}, "MD" }, { {0}, "Maryland" },
    { {0}, "MA" }, { {0}, "Massachusetts" },
    { {0}, "MI" }, { {0}, "Michigan" },
    { {0}, "MN" }, { {0}, "Minnesota" },
    { {0}, "MS" }, { {0}, "Mississippi" },
    { {0}, "MO" }, { {0}, "Missouri" },
    { {0}, "MT" }, { {0}, "Montana" },
    { {0}, "NE" }, { {0}, "Nebraska" },
    { {0}, "NV" }, { {0}, "Nevada" },
    { {0}, "NH" }, { {0}, "New Hampshire" },
    { {0}, "NJ" }, { {0}, "New Jersey" },
    { {0}, "NM" }, { {0}, "New Mexico" },
    { {0}, "NY" }, { {0}, "New York" },
    { {0}, "NC" }, { {0}, "North Carolina" },
    { {0}, "ND" }, { {0}, "North Dakota" },
    { {0}, "OH" }, { {0}, "Ohio" },
    { {0}, "OK" }, { {0}, "Oklahoma" },
    { {0}, "OR" }, { {0}, "Oregon" },
    { {0}, "PA" }, { {0}, "Pennsylvania" },
    { {0}, "RI" }, { {0}, "Rhode Island" },
    { {0}, "SC" }, { {0}, "South Carolina" },
    { {0}, "SD" }, { {0}, "South Dakota" },
    { {0}, "TN" }, { {0}, "Tennessee" },
    { {0}, "TX" }, { {0}, "Texas" },
    { {0}, "UT" }, { {0}, "Utah" },
    { {0}, "VT" }, { {0}, "Vermont" },
    { {0}, "VA" }, { {0}, "Virginia" },
    { {0}, "WA" }, { {0}, "Washington" },
    { {0}, "WV" }, { {0}, "West Virginia" },
    { {0}, "WI" }, { {0}, "Wisconsin" },
    { {0}, "WY" }, { {0}, "Wyoming" },
  }
};


StringList STRLISTID_ComboBox1 =
{
  4,
  {
    { {0, 0, ICONID_TabOptions, 0}, "V Row 22" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "W Row 23" },
    { {0, 0, ICONID_ScaleOptions, 0}, "X Row 24" },
    { {0, 0, ICONID_TabOptions, 0}, "Y Row 25" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "Z Row 26" },
    { {0, 0, ICONID_ScaleOptions, 0}, "F Row 6" },
    { {0, 0, ICONID_TabOptions, 0}, "G Row 7" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "H Row 8" },
    { {0, 0, ICONID_ScaleOptions, 0}, "I Row 9" },
    { {0, 0, ICONID_TabOptions, 0}, "J Row 10" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "K Row 11" },
    { {0, 0, ICONID_ScaleOptions, 0}, "L Row 12" },
    { {0, 0, ICONID_TabOptions, 0}, "M Row 13" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "N Row 14" },
    { {0, 0, ICONID_ScaleOptions, 0}, "O Row 15" },
    { {0, 0, ICONID_TabOptions, 0}, "A Row 1" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "B Row 2" },
    { {0, 0, ICONID_ScaleOptions, 0}, "C Row 3" },
    { {0, 0, ICONID_TabOptions, 0}, "D Row 4" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "E Row 5" },
    { {0, 0, ICONID_TabOptions, 0}, "P Row 16" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "Q Row 17" },
    { {0, 0, ICONID_ScaleOptions, 0}, "R Row 18" },
    { {0, 0, ICONID_TabOptions, 0}, "S Row 19" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "T Row 20" },
    { {0, 0, ICONID_ScaleOptions, 0}, "U Row 21" },
  }
};




