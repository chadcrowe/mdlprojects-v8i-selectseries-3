/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cstexmpl/cstexmpl.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   cstexmpl.h  $
|   $Revision: 1.3.32.1 $
|	$Date: 2013/07/01 20:36:14 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   cstexmpl.h								|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <basetype.h>
/*----------------------------------------------------------------------+
|									|
|   Dialog ID's								|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_CSTExmpl	1
#define DIALOGID_MyAppFrame     2
#define TOOLBOXID_MyCommands    3

/*----------------------------------------------------------------------+
|									|
|   Tab	ID's								|
|									|
+----------------------------------------------------------------------*/
#define	TPLISTID_ONE		1

#define	TABPAGEID_DYNAMIC	1
#define	TABPAGEID_STATIC	2

/*----------------------------------------------------------------------+
|									|
|   Dialog Hook	ID's							|
|									|
+----------------------------------------------------------------------*/
#define	HOOKDIALOGID_CSTExmpl		1
#define	HOOKITEMID_ToggleComboBox	2
#define	HOOKITEMID_FontNameComboBox	3
#define	HOOKITEMID_ComboStates		4
#define	HOOKITEMID_ComboBoxes		5
#define	HOOKITEMID_TabPageRadioBtns	6
#define	HOOKITEMID_SpinBoxSetDRows	7
#define	HOOKITEMID_TextItem		8
#define	HOOKITEMID_SpinBoxTest1		9

/*----------------------------------------------------------------------+
|									|
|   String List	ID's							|
|									|
+----------------------------------------------------------------------*/
#define	STRLISTID_ComboBox1		1
#define	STRLISTID_ComboBoxStates	2

/*----------------------------------------------------------------------+
|									|
|   Spin box ID's							|
|									|
+----------------------------------------------------------------------*/
#define	SPINBOXID_SetDisplayRows	1
#define	SPINBOXID_SetListWidth		2
#define	SPINBOXID_SetGapWidth		3
#define	SPINBOXID_SetComboWidth		4
#define	SPINBOXID_Test1			5

/*----------------------------------------------------------------------+
|									|
|   COMBOBOX ID's						|
|									|
+----------------------------------------------------------------------*/
#define	COMBOBOXID_FontName		1
#define	COMBOBOXID_Test1		2
#define	COMBOBOXID_Test2		3
#define	COMBOBOXID_Test3		4
#define	COMBOBOXID_Insert		5

/*----------------------------------------------------------------------+
|									|
|   TOGGLE Button ID's						|
|									|
+----------------------------------------------------------------------*/
#define	TOGGLEBTNID_ToggleCombo		1

/*----------------------------------------------------------------------+
|									|
|   Radio Button ID's						|
|									|
+----------------------------------------------------------------------*/
#define	RBLISTXID_TabPlacement		1

#define	RBLISTID_TabPlacement		1

#define	RBUTTONID_TabsTop		1
#define	RBUTTONID_TabsBottom		2
#define	RBUTTONID_TabsLeft		3
#define	RBUTTONID_TabsRight		4

/*----------------------------------------------------------------------+
|																		|
|   Text Item ID's														|
|																		|
+----------------------------------------------------------------------*/
#define	TEXTID_Min			1
#define	TEXTID_Max			2
#define	TEXTID_Inc			3
#define	TEXTID_Format			4

/*----------------------------------------------------------------------+
|									|
|   Icon ID's							|
|									|
+----------------------------------------------------------------------*/
#define	ICONID_ScaleOptions		1
#define	ICONID_RadioBtnOptions		2
#define	ICONID_TabOptions		3
#define	ICONID_ComboBox			4
#define ICONCMDFRAMEID_MyApp            5
#define ICONCMDID_CSTEXMPL              6
#define ICONPOPUPID_CSTEXLaunch         7

/*----------------------------------------------------------------------+
|									|
|   Message list defines						|
|									|
+----------------------------------------------------------------------*/
#define	MESSAGELISTID_Messages		1

/*----------------------------------------------------------------------+
|									|
|   Message List Entry Ids						|
|									|
+----------------------------------------------------------------------*/
#define	MESSAGEID_DialogOpenError   	1
#define	MESSAGEID_ResourceLoadError	2


typedef	struct cstinfo
    {
    char	FontName[32];
    char	comboText[80];
    int		ToggleCombo;

    double	IncSpin;
    double	MinSpin;
    double	MaxSpin;
    char	FormatSpin[16];

    double	DisplayRows;
    double	ListWidth;
    double	GapWidth;
    double	spinBoxValue;
    int	  	tabPlacement;
    ElementID  	test2;
    double      test3;
    } CSTInfo;

