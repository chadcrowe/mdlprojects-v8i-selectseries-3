/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevDispAMR/english/LevDispAMRmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevDispAMR/english/LevDispAMRmsg.r_v  $
|   $Workfile:   LevDispAMRmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevDispAMR application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "LevDispAMR.h"	/* LevDispAMR dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_LevDispAMRErrors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_LevDispAMRDialog,   "Unable to open LevDispAMR dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
