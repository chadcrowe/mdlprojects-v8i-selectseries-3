/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevDispAMR/LevDispAMR.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevDispAMR/LevDispAMR.h_v  $
|   $Workfile:   LevDispAMR.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in LevDispAMR dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __LevDispAMRH__
#define	    __LevDispAMRH__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_LevDispAMR		1   /* dialog id for LevDispAMR Dialog */
#define DIALOGID_LevDispAMRModal	2   /* dialog id for LevDispAMR Modal Dialog */

#define OPTIONBUTTONID_LevDispAMR	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_LevDispAMR		1   /* id for "parameter 1" text item */
#define TOGGLEID_LevDispAMR		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_LevDispAMR		1   /* id for synonym resource */

#define MESSAGELISTID_LevDispAMRErrors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_LevDispAMRDialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_LevDispAMR	1   /* id for toggle item hook func */
#define HOOKDIALOGID_LevDispAMR		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct LevDispAMRglobals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } LevDispAMRGlobals;

#endif
