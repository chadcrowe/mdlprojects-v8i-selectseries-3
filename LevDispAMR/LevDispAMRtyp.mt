/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevDispAMR/LevDispAMRtyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevDispAMR/LevDispAMRtyp.mtv  $
|   $Workfile:   LevDispAMRtyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevDispAMR Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "LevDispAMR.h"

publishStructures (LevDispAMRglobals);

    
