
#define PI 3.1415926535897932384626433
#include    <mdl.h>
#include    <toolsubs.h>
#include    <basetype.h>
#include    <malloc.h>
#include    <msrmgr.h>
#include    <mstypes.h>
#include    <string.h>
#include    <msparse.fdf>
#include    <msstate.fdf>
#include    <mssystem.fdf>
#include    <dlmsys.fdf>
#include    <mscnv.fdf>
#include    <msfile.fdf>
#include    <msdialog.fdf>
#include    <dlogids.h>
#include    <mswindow.fdf>
#include    <msdgnmodelref.fdf>
#include    <msdgnlib.fdf>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mscell.fdf>
#include    <msmline.fdf>
#include    <mstextstyle.fdf>
#include    <listmodel.fdf>
#include    <changetrack.fdf>
#include    <mselems.h>
#include    <scanner.h>
#include    <userfnc.h>
#include    <cmdlist.h>
#include    <string.h>
#include    <toolsubs.h>
#include    <dlogman.fdf>
#include    <mssystem.fdf>
#include    <mslinkge.fdf>
#include    <msscan.fdf>
#include    <msoutput.fdf>
#include    <msparse.fdf>
#include    <mselemen.fdf>
#include    <msrsrc.fdf>
#include    <mslocate.fdf>
#include    <msstate.fdf>
#include    <msscancrit.fdf>

#include <mstypes.h>
#include <msscancrit.fdf>
#include <elementref.h>

#include <msdgnobj.fdf>

#include    <msdgncache.h>
#include    <ditemlib.fdf>
//#include	<FontManager.h>
#include    <mselementtemplate.fdf>
#include    <msdimstyle.fdf>
#include    <msdim.fdf>
#include    <namedexpr.fdf>
#if !defined (DIM)
#define DIM(a) ((sizeof(a)/sizeof((a)[0])))
#endif


USING_NAMESPACE_BENTLEY_USTN
USING_NAMESPACE_BENTLEY_USTN_ELEMENT
USING_NAMESPACE_TEXT
#include <iostream>
#define LINEAR_TEMPLATE_NAME      L"CreatedGroup\\Created Linear Template"
#define SHAPE_TEMPLATE_NAME       L"CreatedGroup\\Created Shape Template"
#define HATCH_TEMPLATE_NAME       L"CreatedGroup\\Created Hatch Template"
#define AREAPATTERN_TEMPLATE_NAME L"CreatedGroup\\Created AreaPattern Template"
#define CELL_TEMPLATE_NAME        L"CreatedGroup\\Created Cell Template"
#define TEXT_TEMPLATE_NAME        L"CreatedGroup\\Created Text Template"
#define MLINE_TEMPLATE_NAME       L"CreatedGroup\\Created MLine Template"
#define DIMENSION_TEMPLATE_NAME   L"CreatedGroup\\Created Dimension Template"
#define HEADER_TEMPLATE_NAME      L"CreatedGroup\\Header"
#define COMPONENT1_TEMPLATE_NAME  L"CreatedGroup\\Component1"
#define COMPONENT2_TEMPLATE_NAME  L"CreatedGroup\\Component2"
#include <iostream>
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
int ScanForLinearElements(DgnModelRefP modelRef);
int ElementRefScanCallback(ElementRef, void *callbackArg, ScanCriteriaP);
#include <sstream>
#include <msselect.fdf>
#include <msmisc.fdf>
#include <ElemHandle.h>
#include <ElementAgenda.h>
#include <msreffil.fdf>
#include <mselmdsc.fdf>
//__declspec(dllexport) ElementID mdlElement_getID();

using namespace std;

void Add_Class_Descriptions(MSElementDescrP newDescr, MSElementDescrP oldDescr, ChangeTrackInfo* info, BoolInt* cantBeUndoneFlag);
void Add_Class_Descriptions(MSElementDescrP newDescr, MSElementDescrP oldDescr, ChangeTrackInfo* info, BoolInt* cantBeUndoneFlag)
{
	DgnModelRefP dgnModelRef = mdlModelRef_getActive();
	UInt32 filePos = mdlElmdscr_getFilePos(newDescr);
	ElementRef eleR = mdlModelRef_getElementRef(dgnModelRef, filePos);
	EditElemHandle eh(eleR, dgnModelRef);
	MSElementP eleP = eh.GetElementP();
	UInt32 color = UInt32(3);
	mdlElement_setSymbology(eleP, &color, NULL, NULL);
	UInt32 filePosAppend = mdlElement_rewrite(eleP, NULL, filePos);
}

extern "C"  DLLEXPORT int   MdlMain
(){

	ChangeTrackFunc_Changed * func = Add_Class_Descriptions;
	mdlDialog_cmdNumberQueue(FALSE, CMD_MDL_UNLOAD, mdlSystem_getCurrTaskID(), TRUE);
	return SUCCESS;


}

