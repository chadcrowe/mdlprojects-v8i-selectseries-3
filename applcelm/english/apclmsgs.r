/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/applcelm/english/apclmsgs.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/applcelm/english/apclmsgs.r_v  $
|   $Workfile:   apclmsgs.r  $
|   $Revision: 1.3.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Application element example message text			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include "applcelm.h"

/*----------------------------------------------------------------------+
|									|
|   Message text							|
|									|
+----------------------------------------------------------------------*/

MessageList STRINGID_Messages =
{
    {
    { MSGID_CantOpenResource,	    "Cannot open this resource file." },
    { MSGID_ErrorWritingApplElem,   "Error writing application element." },
    { MSGID_ErrorReadingApplElem,   "Error reading application element." },
    { MSGID_FormatStr01,	    "Variable    Original Data"
				    "         Stored/Retrieved Data" },
    { MSGID_FormatStr02,	    "-------------------------"
				    "------------------------------" },
    { MSGID_FormatStr03,	    "Long        %-10ld            %-10ld" },
    { MSGID_FormatStr04,	    "Double      %5.2f                %5.2f" },
    { MSGID_FormatStr05,	    "Double2     %5.2f                %5.2f" },
    { MSGID_FormatStr06,	    "Short       %-5d                 %-5d" },
    { MSGID_FormatStr07,	    "Short       %-5d                 %-5d" },
    { MSGID_FormatStr08,	    "String      %-20s  %-20s" },
    }
};