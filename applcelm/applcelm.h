/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/applcelm/applcelm.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   applcelm.h  $
|   $Revision: 1.3.76.1 $
|   	$Date: 2013/07/01 20:35:38 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Application Element Example definitions and structures		|
|									|
+----------------------------------------------------------------------*/
#if !defined (__applcelmH__)
#define __applcelmH__

/*----------------------------------------------------------------------+
|									|
|   Header File Dependencies						|
|									|
+----------------------------------------------------------------------*/
#include <mselems.h>

/*----------------------------------------------------------------------+
|									|
|   Defines								|
|									|
+----------------------------------------------------------------------*/
#define EXAMPLE_SIGNATURE		102

#define RSCID_AppData_DataDefBlock      1

#define	STRINGID_Messages		1

#define MSGID_CantOpenResource		2
#define MSGID_ErrorWritingApplElem	5
#define MSGID_ErrorReadingApplElem	6
#define MSGID_FormatStr01		7
#define MSGID_FormatStr02		8
#define MSGID_FormatStr03		9
#define MSGID_FormatStr04		10
#define MSGID_FormatStr05		11
#define MSGID_FormatStr06		12
#define MSGID_FormatStr07		13
#define MSGID_FormatStr08		14

#if !defined (resource)
/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct  applicationdata
   {
   long		longValue1;
   double       doubleValue1;
   double       doubleValue2;
   short	shortValue1;
   short	shortValue2;
   char		string[20];
   } ApplicationData;
#endif

#endif /* if !defined (__applcelmH__) */

