/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/applcelm/applcelm.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   applcelm.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:35:38 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   applcelm.cpp -- Example of application element (type 66, level 20)   |
|   	    	   element manipulation. 	    	    	    	|
|									|
|   This example application shows how to use the IGDS type 66 level 20 |
|   elements to store application specific data. The application    	|
|   elements are application defined, and have no real design data  	|
|   except for the element headers. This application also shows the	|
|   use of the Binary Compatibility extensions to MDL, using both   	|
|   the mdlCnv_bufferToFileFormat and mdlCnv_bufferFromFileFormat       |
|   routines.								|
|   	    	    	    	    	    	    	    	    	|
|   To use this application, key-in mdl load applcelm			|
|									|
|   The topics illustrated in this example apply both to application 	|
|   elements and user data linkages. For more information	    	|
|   on the use of user data linkages, see the SCANFILE example.    	|
|   	    	    	    	    	    	    	    	    	|
|   See mselems.h for a definition of the ApplicationElm.		|
|                                                                       |
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -						|
|									|
|	applcelm_createApplicationHeader - Create application element	|
|		header							|
|	applcelm_retrieveApplicationData - Get copy of application	|
|		element from design file				|
|	applcelm_storeApplicationData - Write application element to	|
|		design file						|
|	main - main entry point						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <byteman.h>
#include    <mselems.h>
#include    <scanner.h>
#include    <cexpr.h>
#include    <rscdefs.h>
#include    <stdarg.h>
#include    <string.h>
#include    <stdlib.h>

#include    "applcelm.h"

#include    <msbnrypo.fdf>
#include    <msrsrc.fdf>
#include    <msdialog.fdf>
#include    <msoutput.fdf>
#include    <mselemen.fdf>
#include    <msscan.fdf>
#include    <msfile.fdf>
#include    <mssystem.fdf>
#include    <mscnv.fdf>
#include    <cmdlist.h>

/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
typedef struct      scanbuffer
    {
    ULong	    filePos;
#if defined (ELEMENTS_8BYTE_ALIGNED)
    long	    filler;
#endif
    MSElementUnion  elm;
    } ScanBuffer;

/*======================================================================+
|									|
|   Minor Code Section - These routines are specific to an application, |
|   	    	    	 but can be used with very little modification  |
|   	    	    	 for many cases                        	    	|
|									|
+======================================================================*/

Private void applcelm_exit
(
int         reason
)
    {
    mdlDialog_cmdNumberQueue (FALSE,CMD_MDL_UNLOAD, mdlSystem_getCurrTaskID(),TRUE);
    
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          applcelm_createApplicationHeader - create application	|
|			header common to all application elements	|
|                                                                       |
| author        BSI                                    9/91             |
|                                                                       |
+----------------------------------------------------------------------*/
Private void 	applcelm_createApplicationHeader
(
MSElement       *elP,		    /* <=> element to set up header info */
int		fileFormatDataSize, /*  => size (in bytes) of application data */
int 	    	signature	    /*  => assigned signature value */
)
    {
    /* ------------------------------------------------------------------
       Set up the required portions of the header :
       Set type to the MicroStation application element.
       Set level to the assigned level.
       Set elementSize to size of our element in words.
       ------------------------------------------------------------------ */
    memset (elP, 0, sizeof (MSElement));

    elP->ehdr.type        = MICROSTATION_ELM;
    elP->ehdr.level       = MSAPPINFO_LEVEL;
    elP->ehdr.elementSize = (offsetof (ApplicationElm, appData) + fileFormatDataSize + 1)/2;

    /* ------------------------------------------------------------------
       We set the offset to attributes in the element header.
       In this case we have no attribute data on the
       element, so it is the same as the element size.
       ------------------------------------------------------------------ */
    elP->ehdr.attrOffset  = elP->ehdr.elementSize;

    /* the rest of the header information is already correctly set to zero */

    /* ------------------------------------------------------------------
       Set the signature word in the element. We need to swap the signature
       word because the scanner operates on the first 20 words of the      
       element. When the scanner returns this to us, it will be correctly  
       aligned and swapped.
       ------------------------------------------------------------------ */
    elP->applicationElm.signatureWord = EXAMPLE_SIGNATURE;
    SWAP_BYTE (&elP->applicationElm.signatureWord);
    }

/*======================================================================+
|									|
|   Private Utility Routines - These routines are very specific to  	|
|   	    	    	       our particular example          	    	|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          applcelm_retrieveApplicationData - get application data |
|			from design file				|
|                                                                       |
| author        BSI                                     9/91            |
|                                                                       |
+----------------------------------------------------------------------*/
Private int  	applcelm_retrieveApplicationData
(
ApplicationData	*appDataP,    	    /* <= pointer to data for storage */
ULong		ddbId
)
    {
    int		status, scanSize;
    short	signature;
    ULong	filePos, scanBuf[MIN_ELEMDATA_SCANBUF_SIZE];
    Scanlist	scanList;
    ScanBuffer	*sbP = NULL;

    /* zero everything we won't need */
    memset (scanBuf, 0, sizeof (scanBuf));

    /* initialize the scanList */
    mdlScan_initScanlist (&scanList);
    mdlScan_noRangeCheck (&scanList);

    scanList.scantype   = ELEMTYPE | LEVELS | ONEELEM | BOTH;
    scanList.typmask[4] = TMSK4_MICROSTATION_ELM;       /* scan for type 66 only */
    scanList.levmask[1] = ELMBITMSK (MSAPPINFO_LEVEL);  /* scan for level 20 only */

    /* use mdlScan_initialize, mdlScan_file to find element */
    mdlScan_initialize (ACTIVEMODEL, &scanList);

    do
	{
	status = mdlScan_file (scanBuf, &scanSize, sizeof (scanBuf), &filePos);

	if (scanSize)
	    {
	    sbP = (ScanBuffer *) &scanBuf[0];

	    /*-----------------------------------------------------------
	    Check signature word
	    -----------------------------------------------------------*/
	    signature = sbP->elm.applicationElm.signatureWord;
	    SWAP_BYTE (&signature);

	    if (signature == EXAMPLE_SIGNATURE)
		{
		/* Convert the rest of the element to internal format. */
		status = mdlCnv_bufferFromFileFormat (NULL, NULL,
				    (byte *) appDataP, NULL,
				    (byte *) sbP->elm.applicationElm.appData, ddbId, NULL);
		return SUCCESS;
		}
	    }

	} while (scanSize && (status == BUFF_FULL));

    return ERROR;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          applcelm_storeApplicationData - store application data	|
|			in file						|
| author        BSI                                     9/91            |
|                                                                       |
+----------------------------------------------------------------------*/
Private int  	applcelm_storeApplicationData
(
ApplicationData *appDataP,  /* => Data to be converted and saved. */
ULong		ddbId       /* => RscId for data conv. rules. */
)
    {
    int		fileFormatDataSize;
    ULong       filePos = 0L;

    /* Compute size of application data to set elementSize */
    if (SUCCESS == mdlCnv_calcFileSizeFromDataDef (&fileFormatDataSize, NULL, 
						   (byte *) appDataP, ddbId, NULL))
	{
	MSElement   el;

	applcelm_createApplicationHeader (&el, fileFormatDataSize, EXAMPLE_SIGNATURE);

	/* Copy and convert the data portion of the element */
	if (SUCCESS == mdlCnv_bufferToFileFormat (NULL, (byte *) el.applicationElm.appData,
						  NULL, (byte *) appDataP, ddbId, NULL))
	    {
	    /* store the element at the end of file */
	    filePos = mdlElement_add (&el);
	    }
	}
	    
    /* check return from mdlElement_add. If file position is 0, a */
    /* problem occured with the write, and mdlErrno has the error */
    /* number in it. Otherwise return SUCCESS. 	    	    	  */
    return (filePos == 0L ? ERROR : SUCCESS);
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Name	applcelm_rscSprintf					|
|                                                                       |
|   Author	BSI                                         3/93        |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    applcelm_rscSprintf
(
char		*stringP,	    /* <=  Result of sprintf from resource	*/
int		messageNumber,	    /*  => Index into msg list for format str	*/
...				    /*  => Any other optional arguments		*/
)
    {
    va_list     ap;
    char	tempStr[1024];

    va_start (ap, messageNumber);

    *stringP = tempStr[0] = '\0';
    mdlResource_loadFromStringList (tempStr, NULL, STRINGID_Messages, messageNumber);
    vsprintf (stringP, tempStr, ap);

    va_end (ap);
    }

/*======================================================================+
|									|
|   Major Public Code Section						|
|									|
+======================================================================*/
extern "C" DLLEXPORT  int MdlMain
(
int		argc,
char		*argv[]
)
    {
    RscFileHandle	    rfHandle;
    ApplicationData	    appData, savedAppData;
    int			    status;
    char		    msgBuffer[128];

    memset (&appData, 0, sizeof (ApplicationData));
    memset (&savedAppData, 0, sizeof (ApplicationData));

    /* Open our resource file for access to Data Definition resources. */
    if (mdlResource_openFile (&rfHandle, NULL, RSC_READONLY) != SUCCESS)
        applcelm_exit(1);

    /* Initialize example application data. */
    savedAppData.longValue1     = 44;
    savedAppData.doubleValue1   = 120.45;
    savedAppData.doubleValue2   = 145.72;
    savedAppData.shortValue1    = 60;
    savedAppData.shortValue2    = 30;
    strcpy (savedAppData.string, "the 20 char string");

    /* Retrieve application data from file */
    if (SUCCESS != (status = applcelm_retrieveApplicationData (&appData, RSCID_AppData_DataDefBlock)))
        {
    	/* Write application data to design file. */
    	if (SUCCESS != (status = applcelm_storeApplicationData (&savedAppData, RSCID_AppData_DataDefBlock)))
	    {
	    mdlOutput_rscPrintf (MSG_ERROR, NULL, STRINGID_Messages, MSGID_ErrorWritingApplElem);
            applcelm_exit(1);
            }

    	/* Retrieve application data from file */
    	if (SUCCESS != (status = applcelm_retrieveApplicationData (&appData, RSCID_AppData_DataDefBlock)))
	    {
	    mdlOutput_rscPrintf (MSG_ERROR, NULL, STRINGID_Messages, MSGID_ErrorReadingApplElem);
            applcelm_exit(1);
	    }
	}

    /* compare data we set and data we retrieved to make sure they are the same*/
    mdlResource_loadFromStringList (msgBuffer, NULL, STRINGID_Messages, MSGID_FormatStr01);
    mdlResource_loadFromStringList (msgBuffer, NULL, STRINGID_Messages, MSGID_FormatStr02);

    applcelm_rscSprintf (msgBuffer, MSGID_FormatStr03, savedAppData.longValue1, appData.longValue1);
    mdlDialog_dmsgsPrint (msgBuffer);

    applcelm_rscSprintf (msgBuffer, MSGID_FormatStr04, savedAppData.doubleValue1, appData.doubleValue1);
    mdlDialog_dmsgsPrint (msgBuffer);

    applcelm_rscSprintf (msgBuffer, MSGID_FormatStr05, savedAppData.doubleValue2, appData.doubleValue2);
    mdlDialog_dmsgsPrint (msgBuffer);

    applcelm_rscSprintf (msgBuffer, MSGID_FormatStr06, savedAppData.shortValue1, appData.shortValue1);
    mdlDialog_dmsgsPrint (msgBuffer);

    applcelm_rscSprintf (msgBuffer, MSGID_FormatStr07, savedAppData.shortValue2, appData.shortValue2);
    mdlDialog_dmsgsPrint (msgBuffer);

    applcelm_rscSprintf (msgBuffer, MSGID_FormatStr08, savedAppData.string, appData.string);
    mdlDialog_dmsgsPrint (msgBuffer);

    applcelm_exit(0);
    return  0;
    }
