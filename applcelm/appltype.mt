/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/applcelm/appltype.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/applcelm/appltype.mtv  $
|   $Workfile:   appltype.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:38 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Application Element Example Program Data Definitions		|
|									|
+----------------------------------------------------------------------*/
#pragma packedLittleEndianData

#include 	"applcelm.h"

createDataDef (applicationdata, RSCID_AppData_DataDefBlock);
