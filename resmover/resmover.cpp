/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/resmover.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: resmover.cpp,v $
|   $Revision: 1.2.16.1 $
|   	$Date: 2013/07/01 20:40:51 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   resmover.mc - example MDL program to manipulate resource files.	|
|									|
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -						|
|									|
|	main - main entry point						|
|	resMover_aliasTextHook - Resource alias text item hook function	|
|	resMover_buildClassList - Build list of resource classes	|
|	resMover_buttonHook - Push button hook function			|
|	resMover_changeAlias - Change resource alias			|
|	resMover_clearRscList - Clear list of resources			|
|	resMover_close - Close a resource file				|
|	resMover_closeAndReopenFile - Close and reopen a resource file	|
|	resMover_closeCurrentFile - Close currrent destination rsc file	|
|	resMover_copyResource - Copy a resource from one file to another|
|	resMover_create - Create a resource file			|
|	resMover_deleteResource - Delete a resource from a file		|
|	resMover_dialogHook - Dialog Box hook				|
|	resMover_displayError - Display an error message		|
|	resMover_getAndOpenFile - Ger a resource file and open it	|
|	resMover_getDataBasedOnLogicalName - Get a resource by alias	|
|	resMover_getSelClass - Get current resource class selection	|
|	resMover_getSelRscInfo - Get selected resource information	|
|	resMover_listHook - List Box hook function for all list boxes	|
|	resMover_openFile - Open a resource file			|
|	resMover_processClassListClick - Handle button events in	|
|	    resource class list box					|
|	resMover_processResourceListClick - Handle button events in	|
|	    resource list box						|
|	resMover_quit - Exit resmover					|
|	resMover_saveUsersFavoriteColor - Write/Update a resource	|
|	resMover_selectFile - Select a resource file			|
|	resMover_showRscList - Show list of resources in a file		|
|	resMover_unloadFunction - Unload async function			|
|	resMover_validateFileName - Make sure file name is good		|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogitem.h>
#include    <cexpr.h>
#include    <mdl.h>
#include    <mdlio.h>
#include    <mdlerrs.h>
#include    <userfnc.h>
#include    <string.h>
#include    <stdlib.h>
#include    <dlogids.h>
#include    <cmdlist.h>

#include    "resmover.h"
#include    "rmovrcmd.h"

#include    <dlogman.fdf>
#include    <msrsrc.fdf>
#include    <mssystem.fdf>
#include    <msoutput.fdf>
#include    <msparse.fdf>
#include    <mscexpr.fdf>
#include    <dlmsys.fdf>

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
#define	APPEND_INDEX	-1  /* Used during mdlStringList_insertMember () */
#define	DELETE_ALL	-1  /* Used during mdlStringList_deleteMember () */
#define SEARCH_FROM_BEGINNING -1 /* Used during mdlDialog_getNextSelection. */
#define NULL_ID	'    '
#define RSC_LIST_COLUMNS 2

/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
typedef struct infofields
    {
    ULong    reservedForListManager;
    ULong    id; /* resourceclass or resourceID */
    } InfoFields;

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
ResMoverGlobals *resMoverP;
SymbolSet      	*setP;

/*----------------------------------------------------------------------+
|									|
|   Local function declarations 					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   External variables							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Major Public Code Section						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name      resMover_displayError					|
|									|
| author    BSI						12/90		|
|									|
+----------------------------------------------------------------------*/
int resMover_displayError
(
int    	errorMsgId
)
    {
    int     actionButton;
    char    alertMsg[80];

    mdlResource_loadFromStringList (alertMsg, resMoverP->resMoverRfHandle,
	    	    	    	    	STRINGID_Errors, errorMsgId);

    actionButton = mdlDialog_openMessageBox(DIALOGID_MsgBoxOKCancel,
			    alertMsg, MSGBOX_ICON_WARNING); 
    return (actionButton);
    }


Private void resMover_exit
(
int     status,
int     programExit
)
    {
    mdlDialog_cmdNumberQueue(FALSE,CMD_MDL_UNLOAD,mdlSystem_getCurrTaskID(),TRUE);  //used to exit the app.
    }


/*----------------------------------------------------------------------+
|									|
| name      resMover_clearRscList					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void resMover_clearRscList
(
int    currFile	    /* => File corresponding to list box being manipulated. */
)
    {
    ResourceFileInfo	*rscFileInfoP = &resMoverP->rscFileInfo [currFile];

    if (rscFileInfoP->rfHandle == FILE_UNUSED)
	return;

    mdlStringList_deleteMember (rscFileInfoP->rscListP, 0, DELETE_ALL);
    mdlDialog_listBoxNRowsChanged (rscFileInfoP->rscListBoxHdrP);
    mdlDialog_listBoxDrawContents (rscFileInfoP->rscListBoxHdrP, -1, -1);
    rscFileInfoP->selRscId = NULL_ID;
    rscFileInfoP->selAlias[0] = '\0';
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_validateFileName					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private int resMover_validateFileName 	    /* <= SUCCESS or -1 */
(
char	*filename    /* => See if this file is already open. */
)
    {
    if ( ((resMoverP->rscFileInfo [0].rfHandle != FILE_UNUSED) &&
    	  (!strcmp (filename, resMoverP->rscFileInfo [0].fileName)))
	    	    	    ||
    	 ((resMoverP->rscFileInfo [1].rfHandle != FILE_UNUSED) &&
    	  (!strcmp (filename, resMoverP->rscFileInfo [1].fileName))))
	return (-1);
    return (SUCCESS);
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_getSelClass					|
|									|
| author    BSI						12/90		|
|									|
+----------------------------------------------------------------------*/
void	    resMover_getSelClass
(
int	    currFile
)
    {
    ResourceFileInfo	*rscFileInfoP = &resMoverP->rscFileInfo [currFile];
    BoolInt    	    	 foundSelection;
    InfoFields        	*classInfoFields;
    int	    	    	 row, col;

    /* Get currently selected resourceclass. */
    row = col = SEARCH_FROM_BEGINNING;
    if ((mdlDialog_listBoxGetNextSelection (&foundSelection,
	    	    	    	    	    &row, &col,
	    	    	    	    	    rscFileInfoP->classListBoxHdrP)
	 != SUCCESS)
	||
        (mdlStringList_getMember (NULL, (long **)&classInfoFields,
	    	    	    	  rscFileInfoP->classListP, row)
	 != SUCCESS))
	return;

    rscFileInfoP->selClass = classInfoFields->id;
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_getSelRscInfo					|
|									|
| author    BSI						12/90		|
|									|
+----------------------------------------------------------------------*/
void	    resMover_getSelRscInfo
(
int	    currFile
)
    {
    ResourceFileInfo	*rscFileInfoP = &resMoverP->rscFileInfo [currFile];
    BoolInt    	    	 foundSelection;
    InfoFields        	*rscInfoFields;
    int	    	    	 row, col, selectedAliasIndex;
    char    	    	*aliasP;

    /* Get currently selected resource. */
    row = col = SEARCH_FROM_BEGINNING;
    if (mdlDialog_listBoxGetNextSelection (&foundSelection,
	    	    	    	    	    &row, &col,
	    	    	    	    	    rscFileInfoP->rscListBoxHdrP)
	 != SUCCESS)
        return;

    selectedAliasIndex = (row * RSC_LIST_COLUMNS) + 1;

    if (mdlStringList_getMember (&aliasP, (long **)&rscInfoFields,
	    	    	    	  rscFileInfoP->rscListP,
				  selectedAliasIndex)
	 == SUCCESS)
    	{
    	rscFileInfoP->selRscId = rscInfoFields->id;
	if (aliasP)
	    {
    	    strncpy (rscFileInfoP->selAlias, aliasP, MAX_ALIAS_LENGTH-1);
    	    strncpy (resMoverP->editAlias, aliasP, MAX_ALIAS_LENGTH-1);
    	    mdlDialog_itemSynch ((DialogBox *)resMoverP->dialogBoxP,
			    	 ALIASTEXTITEMNUM);
	    }
	}
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_changeAlias					|
|									|
| author    BSI						2/91		|
|									|
+----------------------------------------------------------------------*/
void	    resMover_changeAlias
(
void
)
    {
    ResourceFileInfo *rscFileInfoP;
    int	    	      status;

    /* See if at least one file is open. */
    if ((resMoverP->rscFileInfo[0].rfHandle == FILE_UNUSED) &&
	(resMoverP->rscFileInfo[1].rfHandle == FILE_UNUSED))
	{
	resMover_displayError (15);
	return;
	}

    if (resMoverP->rscFileInfo [0].selRscId != NULL_ID)
	rscFileInfoP = &resMoverP->rscFileInfo [0];
    else if (resMoverP->rscFileInfo [1].selRscId != NULL_ID)
	rscFileInfoP = &resMoverP->rscFileInfo [1];
    else
	{
	resMover_displayError (7);
	return;
	}

    /* Change the resource's alias. */
    if (status = mdlResource_changeAlias (rscFileInfoP->rfHandle,
    	    	    	    	 rscFileInfoP->selClass,
    	    	    	    	 rscFileInfoP->selRscId,
    	    	    	    	 resMoverP->editAlias)
         != SUCCESS)
        {
	switch (status)
	    {
	    case MDLERR_RSCALREADYEXISTS:
    	    	resMover_displayError (19);
		break;

	    default:
    	    	resMover_displayError (18);
		break;
	    }
	return;
        }
    else
        {
    	BoolInt    	    	 foundSelection;
    	InfoFields        	*rscInfoFields;
    	int	    	    	 row, col, selectedAliasIndex;

    	/* Get currently selected resource. */
    	row = col = SEARCH_FROM_BEGINNING;
    	if (mdlDialog_listBoxGetNextSelection (&foundSelection,
	    	    	    	    	       &row, &col,
	    	       	    	    	       rscFileInfoP->rscListBoxHdrP)
	    != SUCCESS)
	    {
	    int	targetFile = rscFileInfoP - &resMoverP->rscFileInfo [0];
	    resMover_clearRscList (targetFile);
            return;
	    }

       	selectedAliasIndex = (row * RSC_LIST_COLUMNS) + 1;

    	if (mdlStringList_getMember (NULL, (long **)&rscInfoFields,
	    	    	    	         rscFileInfoP->rscListP,
				      	 selectedAliasIndex)
	    == SUCCESS)
    	    {
	    int	topRowIndex;
	    mdlStringList_setMember (rscFileInfoP->rscListP,
	        	    	     selectedAliasIndex,
	    			     resMoverP->editAlias,
	    	    	    	     (long *)rscInfoFields);

    	    mdlDialog_listBoxGetDisplayRange (&topRowIndex, NULL,
		    	    	    	      NULL, NULL,
					      rscFileInfoP->rscListBoxHdrP);

    	    mdlDialog_listBoxDrawContents (rscFileInfoP->rscListBoxHdrP,
		    	    	    	   row-topRowIndex, 1);
	    }
	}
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_buildClassList					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private int resMover_buildClassList
(
int    currFile
)
    {
    int	    	    status, i;
    long            memberIndex;
    UInt32	    numClasses;
    UInt32   	    *classes = NULL;
    char    	    asciiClass[10];
    InfoFields 	    *classInfoFields;
    ResourceFileInfo *rscFileInfoP = &resMoverP->rscFileInfo [currFile];

    if ((status = mdlResource_queryFile (&numClasses,
	    	       	     	    	 rscFileInfoP->fileName,
			     	    	 RSC_QRY_NUMTYPES )) != SUCCESS)
    	{
    	rscFileInfoP->rfHandle = FILE_UNUSED;
	return (-1);
	}

    if (numClasses)
	{
	if ((classes = (UInt32*)dlmSystem_mdlMalloc (numClasses * sizeof(ULong))) == NULL)
	    return (-1);

    	if ((status = mdlResource_queryFile (classes,
	    	    	     	    	     rscFileInfoP->fileName,
			     	    	     RSC_QRY_CLASS )) != SUCCESS)
	    {
	    dlmSystem_mdlFree (classes);
	    return (-1);
	    }
	}

    for (i=0; i<abs((int)numClasses); i++)
	{
	status = mdlStringList_insertMember (&memberIndex,
	    	    	    	    	     rscFileInfoP->classListP,
	    	    	    	     	     APPEND_INDEX, 1);

        mdlStringList_getMember (NULL, (long **)&classInfoFields,
	    	    	    	 rscFileInfoP->classListP, memberIndex);

    	/* Set appropriate fields in member. */
	asciiClass [0] = (char)((classes[i] & 0xFF000000) >> 24);
	asciiClass [1] = (char)((classes[i] & 0x00FF0000) >> 16);
	asciiClass [2] = (char)((classes[i] & 0x0000FF00) >> 8);
	asciiClass [3] = (char)((classes[i] & 0x000000FF));
	asciiClass [4] = '\0';
	classInfoFields->id = classes[i];

	status = mdlStringList_setMember (rscFileInfoP->classListP,
	    	    	    	      	  memberIndex, asciiClass,
				          (long *)classInfoFields);
	}

    if (numClasses)
	{
        dlmSystem_mdlFree (classes);
    	status = mdlDialog_listBoxNRowsChanged (rscFileInfoP->classListBoxHdrP);
    	status = mdlDialog_listBoxDrawContents (rscFileInfoP->classListBoxHdrP,
		    	    	    	    	    -1, -1);
	}
    return (SUCCESS);
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_openFile						|
|									|
| author    BSI						    		|
|									|
+----------------------------------------------------------------------*/
void	    resMover_openFile
(
char	    *fileName,	    	/* => Specific file to open. */
int	    currFile	    	/* => file number (0-left or 1-right). */
)
    {
    int    fileNameLength;
    RscFileHandle *rfHandleP;
    char    *fileNameP = fileName;

    if (resMoverP->rscFileInfo [currFile].rfHandle != FILE_UNUSED)
	{
	resMover_displayError (4);
	return;
	}

    /* Make sure the file is not already open. */
    if (resMover_validateFileName(fileNameP))
        {
        resMover_displayError (3);
        return;
        }

    rfHandleP = &resMoverP->rscFileInfo [currFile].rfHandle;
    if ((mdlResource_openFile (rfHandleP, fileNameP, RSC_READWRITE))
         == SUCCESS)
        {
	int itemNumber = (currFile == 0)
	    	    	? CLASSLIST1ITEMNUMBER : CLASSLIST2ITEMNUMBER;

    	/* Save name of successfully opened file. */
	strcpy (resMoverP->rscFileInfo [currFile].fileName, fileNameP);
        if ((fileNameLength = strlen(fileName)) > (LISTWIDTHS-2))
            {
            char    tempfilename [LISTWIDTHS];

    	    strncpy (tempfilename, fileNameP, LISTWIDTHS - 17);
	    tempfilename [LISTWIDTHS - 17] = '\0';
	    strcat (tempfilename, "...");
	    strcat (tempfilename, &fileNameP [fileNameLength-12]);
	    fileNameP = tempfilename;
	    }
	/* Display the newly opened file's name. */
    	mdlDialog_itemSetLabel ((DialogBox *)resMoverP->dialogBoxP, 
			    	itemNumber, fileNameP);
	resMover_buildClassList (currFile);
	}
    else /* File open failed. */
	{
	char	errorMsgTemplate [80];
	char	alertMsg [80];
    	mdlResource_loadFromStringList (errorMsgTemplate,
	       	    	    	    	resMoverP->resMoverRfHandle,
	       	    	    	    	STRINGID_Errors, 2);
	sprintf (alertMsg, errorMsgTemplate, fileNameP);

    	mdlDialog_openMessageBox(DIALOGID_MsgBoxOK,
			    alertMsg, MSGBOX_ICON_WARNING); 

	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          resMover_close	    	    	    	    	    	|
|                                                                       |
|               Close a resource file.	    	    	    	    	|
|                                                                       |
| author        BSI                                         	    	|
|                                                                       |
+----------------------------------------------------------------------*/
void	    	resMover_close
(
int	    	targetFile    /* => file to close. */
)
    {
    int			itemNumber;
    ResourceFileInfo   *rscFileInfoP = &resMoverP->rscFileInfo [targetFile];
    char		label[64];

    if (rscFileInfoP->rfHandle == FILE_UNUSED)
	return;

    mdlResource_closeFile (rscFileInfoP->rfHandle);
    rscFileInfoP->fileName[0] = '\0';
    rscFileInfoP->rfHandle = FILE_UNUSED;
    mdlStringList_deleteMember (rscFileInfoP->classListP, 0, DELETE_ALL);
    mdlStringList_deleteMember (rscFileInfoP->rscListP,	0, DELETE_ALL);
    mdlDialog_listBoxNRowsChanged (rscFileInfoP->classListBoxHdrP);
    mdlDialog_listBoxDrawContents (rscFileInfoP->classListBoxHdrP, -1, -1);
    mdlDialog_listBoxNRowsChanged (rscFileInfoP->rscListBoxHdrP);
    mdlDialog_listBoxDrawContents (rscFileInfoP->rscListBoxHdrP, -1, -1);

    if (targetFile == 0)
	{
    	mdlResource_loadFromStringList (label, NULL, STRINGID_Messages, 4);
    	itemNumber = CLASSLIST1ITEMNUMBER;
	}
    else
	{
    	mdlResource_loadFromStringList (label, NULL, STRINGID_Messages, 5);
    	itemNumber = CLASSLIST2ITEMNUMBER;
	}

    mdlDialog_itemSetLabel ((DialogBox *)resMoverP->dialogBoxP, 
			    itemNumber, label);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          resMover_quit                                           |
|                                                                       |
| author        BSI                                         	    	|
|                                                                       |
+----------------------------------------------------------------------*/
Private void	resMover_quit
(
char    *unparsedP
)
//cmdNumber	CMD_RESMOVER_QUIT
    {
    resMover_exit (0,0);
    }

/*----------------------------------------------------------------------+
|									|
|   Supporting Routines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name      resMover_showRscList					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void resMover_showRscList
(
int    currFile      /* => File whose list box is being manipulated. */
)
    {
    int	    	    	 numResources, i, status;
    long                 memberIndex;
    ULong    	    	 selectedClass, rscId;
    char    	    	 asciiResourceId[20];
    ResourceFileInfo	*rscFileInfoP = &resMoverP->rscFileInfo [currFile];
    InfoFields        	*rscInfoFields;

    if (rscFileInfoP->rfHandle == FILE_UNUSED)
	return;

    selectedClass = rscFileInfoP->selClass;

    mdlResource_queryClass (&numResources, rscFileInfoP->rfHandle,
	    	    	    selectedClass, RSC_QRY_COUNT, NULL);

    for (i=0; i<numResources; i++)
	{
    	mdlResource_queryClass (&rscId, rscFileInfoP->rfHandle,
	    	    	    	selectedClass, RSC_QRY_ID, &i);

    	mdlResource_queryClass (rscFileInfoP->selAlias, rscFileInfoP->rfHandle,
	    	    	    	selectedClass, RSC_QRY_ALIAS, &i);

	status = mdlStringList_insertMember (&memberIndex,
	    	    	    	    	     rscFileInfoP->rscListP,
	    	    	    	     	     APPEND_INDEX, 2);

        mdlStringList_getMember (NULL, (long **)&rscInfoFields,
	    	    	    	 rscFileInfoP->rscListP, memberIndex);

	rscInfoFields->id = rscId;
	sprintf (asciiResourceId, "0x%08X", rscId);

	status = mdlStringList_setMember (rscFileInfoP->rscListP,
	    	    	    	      	  memberIndex, asciiResourceId,
				          (long *)rscInfoFields);

	status = mdlStringList_setMember (rscFileInfoP->rscListP,
	    	    	    	      	  memberIndex+1, rscFileInfoP->selAlias,
				          (long *)rscInfoFields);
	}

    status = mdlDialog_listBoxNRowsChanged (rscFileInfoP->rscListBoxHdrP);
    status = mdlDialog_listBoxDrawContents (rscFileInfoP->rscListBoxHdrP,
		    	    	    	    	    -1, -1);
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_closeAndReopenFile					|
|									|
| author    BSI						    		|
|									|
+----------------------------------------------------------------------*/
int resMover_closeAndReopenFile
(
int targetFile
)
    {
    char	saveFileName [128];

    /* Save the destination file name. */
    strcpy (saveFileName, resMoverP->rscFileInfo [targetFile].fileName);
    resMover_close (targetFile);
    resMover_openFile (saveFileName, targetFile);
    resMoverP->rscFileInfo [targetFile].selClass =
        resMoverP->rscFileInfo [targetFile].selClass;
    resMover_showRscList (targetFile);

    return  SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_selectFile						|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private int resMover_selectFile
(
DialogItemMessage   *dimP
)
    {
    if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
    	{
    	switch (resMoverP->destinationFile)
            {
            case 0: /* Switching from left file to right file. */
    	    	/* Set up to manipulate the first file. */
            	resMoverP->sourceFile = 0;
            	resMoverP->destinationFile = 1;
    	    	mdlDialog_itemSetLabel ((DialogBox *)resMoverP->dialogBoxP,
    	    	    	    	    	BUTTON_ITEMNUM, ">>");
    	    	mdlDialog_focusItemIndexSet ((DialogBox *)resMoverP->dialogBoxP,
    	    	    	    	    	CLASSLIST2ITEMNUMBER, FALSE);
    	    	break;

            case 1: /* Switching from right file to left file. */
    	    	/* Set up to manipulate the second file. */
            	resMoverP->sourceFile = 1;
            	resMoverP->destinationFile = 0;
    	    	mdlDialog_itemSetLabel ((DialogBox *)resMoverP->dialogBoxP,
    	    	    	    	    	BUTTON_ITEMNUM, "<<");
    	    	mdlDialog_focusItemIndexSet ((DialogBox *)resMoverP->dialogBoxP,
    	    	    	    	    	CLASSLIST1ITEMNUMBER, FALSE);
    	    	break;
            }
    	}
    return  SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_copyResource					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void	resMover_copyResource
(
DialogItemMessage   *dimP
)
    {
    int	    status, errMsgNumber, rscSize,
	    src = resMoverP->sourceFile,
    	    dest = resMoverP->destinationFile;

    void   *rscP;
    char   *origAlias;

    if (dimP->u.button.buttonTrans != BUTTONTRANS_UP)
	return;

    if ((resMoverP->rscFileInfo[0].rfHandle == FILE_UNUSED) ||
	(resMoverP->rscFileInfo[1].rfHandle == FILE_UNUSED))
	{
	resMover_displayError (5);
	return;
	}

    if (resMoverP->rscFileInfo [src].selRscId == NULL_ID)
	{
	resMover_displayError (7);
	return;
	}

    /* Attempt to load the specified resource. */
    if ((rscP = mdlResource_load (
	    	    resMoverP->rscFileInfo [src].rfHandle,
    	    	    resMoverP->rscFileInfo [src].selClass,
    	    	    resMoverP->rscFileInfo [src].selRscId))
	 == NULL)
	{
	resMover_displayError (8);
	return;
	}

    mdlResource_query (&rscSize, rscP, RSC_QRY_SIZE);
    mdlResource_query (&origAlias, rscP, RSC_QRY_ALIAS);

    status = mdlResource_add (
	    	    resMoverP->rscFileInfo [dest].rfHandle,
    	    	    resMoverP->rscFileInfo [src].selClass,
    	    	    resMoverP->rscFileInfo [src].selRscId,
		    rscP, rscSize, origAlias);

    mdlResource_free (rscP);

    if (status == SUCCESS)
	{
	/* ---------------------------------------------------------------
	   Note: Upon a successful "mdlResource_add", it IS NOT necessary
	         to close and reopen the resource file. The file is reopened
		 below as a simple way of rebuilding the class list and
		 resource list in the dialog box.
	   --------------------------------------------------------------- */
    	resMover_closeAndReopenFile (dest);
	}
    else
	{
    	switch (status)
	    {
	    case MDLERR_RSCWRITEERROR:
	    case MDLERR_RSCFILEERROR:
	    	errMsgNumber = 9;
	    	break;

	    case MDLERR_RSCWRITEVIOLATION:
	    	errMsgNumber = 10;
	    	break;

	    case MDLERR_RSCALREADYEXISTS:
	    	errMsgNumber = 11;
	    	break;

	    case MDLERR_RSCINSFMEM:
	    	errMsgNumber = 12;
	    	break;

	    case MDLERR_RSCTYPEINVALID:
	    	errMsgNumber = 13;
	    	break;

	    default:
	    	errMsgNumber = 14;
	    	break;
	    }
	resMover_displayError (errMsgNumber);
	}
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_deleteResource					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void resMover_deleteResource
(
DialogItemMessage   *dimP
)
    {
    int	    status, errMsgNumber, targetFile;

    if (dimP->u.button.buttonTrans != BUTTONTRANS_UP)
	return;

    /* See if at least one file is open. */
    if ((resMoverP->rscFileInfo[0].rfHandle == FILE_UNUSED) &&
	(resMoverP->rscFileInfo[1].rfHandle == FILE_UNUSED))
	{
	resMover_displayError (15);
	return;
	}

    if (resMoverP->rscFileInfo [0].selRscId != NULL_ID)
	targetFile = 0;
    else if (resMoverP->rscFileInfo [1].selRscId != NULL_ID)
	targetFile = 1;
    else
	{
	resMover_displayError (7);
	return;
	}

    if (resMover_displayError (17) == ACTIONBUTTON_CANCEL)
	return;

    /* Attempt to delete the specified resource. */
    status = mdlResource_delete (
	    	    resMoverP->rscFileInfo [targetFile].rfHandle,
    	    	    resMoverP->rscFileInfo [targetFile].selClass,
    	    	    resMoverP->rscFileInfo [targetFile].selRscId);

    if (status == SUCCESS)
	{
	/* ---------------------------------------------------------------
	   Note: Upon a successful "mdlResource_delete", it IS NOT necessary
	         to close and reopen the resource file. The file is reopened
		 below as a simple way of rebuilding the class list and
		 resource list in the dialog box.
	   --------------------------------------------------------------- */
	resMover_closeAndReopenFile (targetFile);
	}
    else
	{
    	switch (status)
	    {
	    case MDLERR_RSCINUSE:
	    	errMsgNumber = 16;
	    	break;

	    case MDLERR_RSCWRITEVIOLATION:
	    	errMsgNumber = 10;
	    	break;

	    default:
	    	errMsgNumber = 14;
	    	break;
	    }
	resMover_displayError (errMsgNumber);
	}
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_processClassListClick				|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
int resMover_processClassListClick
(
DialogItemMessage   *dimP
)
    {
    DialogItem *diP = dimP->dialogItemP;

    switch (dimP->u.button.upNumber)
        {
        case 1: /* Single click. */
            resMover_clearRscList (0);
            resMover_clearRscList (1);
	    break;

    	case 2:	/* Double click. */
    	    if (dimP->u.button.clicked == TRUE)
       	    	{
                switch (diP->id)
             	   {
    	    	   case LISTID_File1_Classes:
    	    	       resMover_getSelClass (0);
        	       resMover_showRscList (0);
        	       break;

    	    	   case LISTID_File2_Classes:
    	    	       resMover_getSelClass (1);
        	       resMover_showRscList (1);
        	       break;
    	    	   }
		}
	    break;
    	}
    return  SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_processResourceListClick				|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
int resMover_processResourceListClick
(
DialogItemMessage   *dimP
)
    {
    DialogItem *diP = dimP->dialogItemP;

    switch (dimP->u.button.upNumber)
        {
	case 1: /* Single click */
	case 2: /* or double click. */
            switch (diP->id)
               {
    	       case LISTID_File1_Resources:
            	    resMover_clearRscList (1);
		    resMover_getSelRscInfo (0);
		    break;

    	       case LISTID_File2_Resources:
            	    resMover_clearRscList (0);
		    resMover_getSelRscInfo (1);
		    break;
	       }
	    break;
    	}
    return  SUCCESS;
    }


/*----------------------------------------------------------------------+
|									|
|    Other Resource Manager Routines					|
|									|
|    The routines listed below do not do anything with regard to    	|
|    the functionality of the RESMOVER program. They are shown below	|
|    as examples of the remaining Resource Manager calls not shown in	|
|    the program, above.						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
| name      Example of "mdlResource_getRscIdByAlias"			|
|									|
| author    BSI						8/90		|
|									|
+----------------------------------------------------------------------*/
Public int resMover_getDataBasedOnLogicalName
(
void          *resourceInfoP, /* <= Copy Resource Info to this memory area  */
RscFileHandle  rfHandle,      /* => Resource file handle from earlier open  */
ULong	       resourceclass, /* => Resource type    	           	    */
char	       *aliasName     /* => Logical Name of resource	    	    */
)
    {
    void     *rscP;
    UInt32      rscID;
    int	      rscSize;

    /* Using the resource's alias name, retrieve its actual resourceID. */
    if (mdlResource_getRscIdByAlias (&rscID, rfHandle, resourceclass,
	    	    	    	    	aliasName))
	return (ERROR);

    /* Now use the resourceID to load the resource. */
    if (rscP = mdlResource_load (rfHandle, resourceclass, rscID))
	{
	/* Find out how many bytes need to be copied.
	    Note: The caller of this routine MUST BE SURE THAT THE MEMORY
	    POINTED TO BY "*resourceInfoP" IS LARGE ENOUGH TO ACCEPT THE
	    FULL RESOURCE. */
	if (mdlResource_query (&rscSize, rscP, RSC_QRY_SIZE) == SUCCESS)
	    {
            memcpy (resourceInfoP, rscP, rscSize);
	    mdlResource_free (rscP);
	    return (SUCCESS);
	    }
	}

    return (ERROR);
    }

/*----------------------------------------------------------------------+
|									|
| name      Example of "mdlResource_write" and				|
|		       "mdlResource_resize"				|
| author    BSI						8/90		|
|									|
+----------------------------------------------------------------------*/
Public int resMover_saveUsersFavoriteColor
(
char *usersFavoriteColorP,   /* => This is the user's favorite color. */
int   maxResourceSize,       /* => Maimum size of input string, above. */
RscFileHandle rfHandle	     /* => Resource file where data is to be stored. */
)
    {
#define RTYPE_COLOR_CLASS 'COLR' /* arbitrary resourceclass for color info. */
#define RSCID_FAVORITE_COLOR 0   /* arbitrary resourceID for favorite color. */

    char    *existingColorP;
    int	     rscSize;

    /* First see if this user preference is already existing in the file. */
    if ((existingColorP = (char*)mdlResource_load (rfHandle, RTYPE_COLOR_CLASS,
	    	    	    	    	    RSCID_FAVORITE_COLOR)) != NULL)
	{
	if (mdlResource_query (&rscSize, existingColorP,
	    	    	    	RSC_QRY_SIZE) != SUCCESS)
	    return (ERROR);

	if (rscSize < maxResourceSize)
	    existingColorP = (char*)mdlResource_resize (existingColorP,
		    	    	    	    	 maxResourceSize);
	if (existingColorP != NULL)
	    {
	    memcpy (existingColorP, usersFavoriteColorP, maxResourceSize);
	    mdlResource_write (existingColorP);
	    mdlResource_free (existingColorP);
	    }
	else
	    return (ERROR);
	}
    else /* Since the "favorite color" resource was not already in the file,
	    we will add it now. */
	{
    	return (mdlResource_add (rfHandle, RTYPE_COLOR_CLASS,
	    	    	    	  RSCID_FAVORITE_COLOR, usersFavoriteColorP,
			          maxResourceSize, NULL));
	}
    return (ERROR);
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_unloadFunction					|
|									|
| author    BSI						2/91		|
|									|
+----------------------------------------------------------------------*/
Private int	resMover_unloadFunction ()
    {
    if (resMoverP->rscFileInfo [0].classListP != NULL)
	mdlStringList_destroy (resMoverP->rscFileInfo [0].classListP);
    if (resMoverP->rscFileInfo [1].classListP != NULL)
	mdlStringList_destroy (resMoverP->rscFileInfo [1].classListP);
    if (resMoverP->rscFileInfo [0].rscListP != NULL)
	mdlStringList_destroy (resMoverP->rscFileInfo [0].rscListP);
    if (resMoverP->rscFileInfo [1].rscListP != NULL)
	mdlStringList_destroy (resMoverP->rscFileInfo [1].rscListP);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|       Dialog Box Hooks                                                |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name		resMover_dialogHook     				|
|									|
| author	BSI     				    		|
|									|
+----------------------------------------------------------------------*/
Private void	resMover_dialogHook
(
DialogMessage	*dmP
)
    {
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    dmP->u.create.interests.mouses	   = TRUE;
	    resMoverP->dialogBoxP = (int) dmP->db;
	    resMoverP->sourceFile = 1;
	    resMoverP->destinationFile = 0;
	    resMoverP->rscFileInfo[0].rfHandle =
		    resMoverP->rscFileInfo[1].rfHandle = FILE_UNUSED;
	    break;

	case DIALOG_MESSAGE_INIT:
	    break;

	case DIALOG_MESSAGE_DESTROY:
	    mdlCExpression_freeSet (setP);
	    break;

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		resMover_listHook         				|
|									|
| author	BSI     				    		|
|									|
+----------------------------------------------------------------------*/
Private void	resMover_listHook
(
DialogItemMessage	*dimP
)
    {
    DialogItem *diP = dimP->dialogItemP;
    StringList **strListP;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    switch (diP->id)
		{
		case LISTID_File1_Classes:
    	    	case LISTID_File2_Classes:
		    {
		    int	index;

		    if (diP->id == LISTID_File1_Classes)
			index = 0;
		    else
			index = 1;

		    resMoverP->rscFileInfo [index].classListBoxHdrP =
    	    	    	    	    	    	    	    diP->rawItemP;
		    strListP = &resMoverP->rscFileInfo[index].classListP;
        	    if ((*strListP = mdlStringList_create (0, 2))
		     	== NULL)
	    	    	dimP->u.create.createFailed = TRUE;

    	    	    if (mdlDialog_listBoxSetStrListP (diP->rawItemP,
	        	    	    	    	*strListP, 1)
	    	    	!= SUCCESS)
	    	    	dimP->u.create.createFailed = TRUE;
		    break;
		    }

    	    	case LISTID_File1_Resources:
    	    	case LISTID_File2_Resources:
		    {
		    int	index;

		    if (diP->id == LISTID_File1_Resources)
			index = 0;
		    else
			index = 1;

		    resMoverP->rscFileInfo [index].rscListBoxHdrP =
			    	    	    	    	    diP->rawItemP;
		    strListP = &resMoverP->rscFileInfo[index].rscListP;
		    resMoverP->rscFileInfo[index].selRscId = NULL_ID;
		    resMoverP->rscFileInfo[index].selAlias[0] = '\0';

        	    if ((*strListP = mdlStringList_create (0, 2))
		     	== NULL)
	    	    	dimP->u.create.createFailed = TRUE;

    	    	    if (mdlDialog_listBoxSetStrListP (diP->rawItemP,
			    	    	    	    	*strListP,
							RSC_LIST_COLUMNS)
		    	!= SUCCESS)
	    	    	dimP->u.create.createFailed = TRUE;
		    break;
		    }

		default:
	    	    dimP->u.create.createFailed = TRUE;
		    return;
		}
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    switch (diP->id)
		{
		case LISTID_File1_Classes:
		    mdlStringList_destroy (
			resMoverP->rscFileInfo [0].classListP);
		    resMoverP->rscFileInfo [0].classListP = NULL;
		    break;
    	    	case LISTID_File2_Classes:
		    mdlStringList_destroy (
			resMoverP->rscFileInfo [1].classListP);
		    resMoverP->rscFileInfo [1].classListP = NULL;
		    break;
    	    	case LISTID_File1_Resources:
		    mdlStringList_destroy (
			resMoverP->rscFileInfo [0].rscListP);
		    resMoverP->rscFileInfo [0].rscListP = NULL;
		    break;
    	    	case LISTID_File2_Resources:
		    mdlStringList_destroy (
			resMoverP->rscFileInfo [1].rscListP);
		    resMoverP->rscFileInfo [1].rscListP = NULL;
		    break;
		}
	    break;
	    }

	case DITEM_MESSAGE_BUTTON:
	    {
            switch (diP->id)
            	{
            	case LISTID_File1_Classes:
            	case LISTID_File2_Classes:
	    	    if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		    	resMover_processClassListClick (dimP);
		    break;

            	case LISTID_File1_Resources:
            	case LISTID_File2_Resources:
	    	    if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		    	resMover_processResourceListClick (dimP);
		    break;
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		resMover_buttonHook				    	|
|									|
| author	BSI    	    	    	    	    	    	    	|
|									|
+----------------------------------------------------------------------*/
Public void resMover_buttonHook
(
DialogItemMessage   *dimP
)
    {
    DialogItem *diP = dimP->dialogItemP;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_BUTTON:
	    {
	    switch (diP->id)
		{
		case PUSHBUTTONID_FilePtr:
		    resMover_selectFile (dimP);
		    break;

		case PUSHBUTTONID_Copy:
		    resMover_copyResource (dimP);
		    break;

		case PUSHBUTTONID_Del:
		    resMover_deleteResource (dimP);
		    break;
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name      resMover_aliasTextHook					|
|									|
| author    BSI						2/91		|
|									|
+----------------------------------------------------------------------*/
Public void resMover_aliasTextHook
(
DialogItemMessage   *dimP
)
    {

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    dimP->u.create.createFailed = FALSE;
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    switch (dimP->dialogItemP->id)
		{
		case TEXTID_ChangeAlias:
		    resMover_changeAlias ();
		    break;

		default:
    		    dimP->msgUnderstood = FALSE;
		    break;
		}
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|       Command Handling routines                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          resMover_create                                         |
|                                                                       |
|               Create a resource file.	    	    	    	    	|
|                                                                       |
| author        BSI                                         	    	|
|                                                                       |
+----------------------------------------------------------------------*/
Private void	resMover_create
(
char    *unparsedP
)
//cmdNumber	CMD_RESMOVER_CREATE
    {
    int     status, currFile = resMoverP->destinationFile;
    char    titleString	[80], filename[128];

    if (resMoverP->rscFileInfo [currFile].rfHandle != FILE_UNUSED)
	{
	resMover_displayError (4);
	return;
	}

    mdlResource_loadFromStringList (titleString, resMoverP->resMoverRfHandle,
	    	    	    	    STRINGID_Messages, 0);

    status = mdlDialog_fileCreate (filename, NULL, 0, NULL, "*.rsc",
	    	    	    	   "MS_RSRCPATH", titleString);

    if ((status == SUCCESS) && (filename[0] != '\0'))
	{
	mdlResource_createFile (filename, "", 0L);
	resMover_openFile (filename, currFile);
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          resMover_closeCurrentFile                               |
|                                                                       |
|               Close the currently selected resource file.	    	|
|                                                                       |
| author        BSI                                         	    	|
|                                                                       |
+----------------------------------------------------------------------*/
Private void	resMover_closeCurrentFile
(
char    *unparsedP
)
//cmdNumber	CMD_RESMOVER_CLOSE
    {
    resMover_close (resMoverP->destinationFile);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          resMover_getAndOpenFile                                 |
|                                                                       |
|               Get a resource file name and open it.  	    	    	|
|                                                                       |
| author        BSI                                         	    	|
|                                                                       |
+----------------------------------------------------------------------*/
Private	void	resMover_getAndOpenFile
(
char    *unparsedP
)
//cmdNumber	CMD_RESMOVER_OPEN
    {
    int     status, currFile = resMoverP->destinationFile;
    char    titleString	[80], filename[MAXFILELENGTH];

    if (resMoverP->rscFileInfo [currFile].rfHandle != FILE_UNUSED)
	{
	resMover_displayError (4);
	return;
	}

    mdlResource_loadFromStringList (titleString, resMoverP->resMoverRfHandle,
	    	    	    	    STRINGID_Messages, 1);

    /* Obtain a file name from the user. */
    status = mdlDialog_fileOpen (filename, NULL, 0, NULL,
        	    "*.rsc", "MS_RSRCPATH", titleString);

    if ((status == SUCCESS) && (filename[0] != '\0'))
	resMover_openFile (filename, currFile);
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI     						|
|									|
+----------------------------------------------------------------------*/
Private DialogHookInfo uHooks[] =
    {
    {HOOKDIALOGID_ResMover,			(PFDialogHook)resMover_dialogHook},
    {HOOKITEMID_List_ResMover,	    	    	(PFDialogHook)resMover_listHook},
    {HOOKITEMID_Button_ResMover,	    	(PFDialogHook)resMover_buttonHook},
    {HOOKITEMID_ChangeAlias,		    	(PFDialogHook)resMover_aliasTextHook},
    };

extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    DialogBox 	    *dbP;

    if ((resMoverP =(ResMoverGlobals *)dlmSystem_mdlMalloc (sizeof(ResMoverGlobals))) == NULL)
        resMover_exit (0,0);

    memset (resMoverP, 0, sizeof(ResMoverGlobals));

    /* --------------------------------------------------------------------
       Open the resource file from which this application was loaded.
       This is accomplished by specifying "NULL" as the filename to the
       mdlResource_openFile call.
       -------------------------------------------------------------------- */
    if (mdlResource_openFile (&resMoverP->resMoverRfHandle, NULL, 0)
	!= SUCCESS)
	{
	char	buffer[128];

	mdlResource_loadFromStringList (buffer, NULL, STRINGID_Errors, 20);

    	mdlDialog_openMessageBox(DIALOGID_MsgBoxOK,
			    buffer, MSGBOX_ICON_WARNING); 

        resMover_exit (0,0);
    	}
    Private MdlCommandNumber  commandNumbers [] =
    {
    {resMover_getAndOpenFile,CMD_RESMOVER_OPEN},
    {resMover_closeCurrentFile,CMD_RESMOVER_CLOSE},
    {resMover_create,CMD_RESMOVER_CREATE},
    {resMover_quit,CMD_RESMOVER_QUIT},
    0,
    };
    
    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);

    /* --------------------------------------------------------------------
       Load the command table for this application. Specifying NULL as the
       file will cause the Resource Manager to look for the command table
       in the Resource File opened above.
       -------------------------------------------------------------------- */
    if (mdlParse_loadCommandTable (NULL) == NULL)
	mdlOutput_rscPrintf (MSG_ERROR, NULL, STRINGID_Errors, 0);

    /* set up the variables we are going to set from dialog boxes */
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);

    mdlDialog_publishComplexPtr (setP, "resmoverglobals",
					    "resMoverP", &resMoverP);

    /* publish our hooks */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    /* start the dialog box */
    if ((dbP = (DialogBox *) mdlDialog_open (NULL, DIALOGID_ResMover)) == NULL)
	mdlOutput_rscPrintf (MSG_ERROR, NULL, STRINGID_Errors, 1);

    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM, resMover_unloadFunction);

    return  SUCCESS;
    }

