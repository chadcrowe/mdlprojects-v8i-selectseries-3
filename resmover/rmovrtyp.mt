/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/rmovrtyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/resmover/rmovrtyp.mtv  $
|   $Workfile:   rmovrtyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:51 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Dialog box structures published					|
|									|
+----------------------------------------------------------------------*/
#include    <rscdefs.h>
#include    <dlogbox.h>
#include    <dlogitem.h>

#include    "resmover.h"
    
publishStructures (resmoverglobals);
