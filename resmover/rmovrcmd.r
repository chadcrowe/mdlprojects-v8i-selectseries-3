/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/rmovrcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/resmover/rmovrcmd.r_v  $
|   $Workfile:   rmovrcmd.r  $
|   $Revision: 1.4.16.1 $
|   	$Date: 2013/07/01 20:40:51 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Resmover command tables						|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>
#include "resmover.h"

/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_RESMOVER        1

DllMdlApp DLLAPP_RESMOVER =
    {
    "RESMOVER", "resmover"          // taskid, dllName
    }


/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE		 0
#define CT_RESMOVER	 1
#define CT_RMOVRSUB	 2

/*----------------------------------------------------------------------+
|                                                                       |
| 	Main RESMOVER Commands						|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_RESMOVER =
{
    {  1, CT_RMOVRSUB, INPUT,  NONE,		"RESMOVER" },
};

/*----------------------------------------------------------------------+
|                                                                       |
| 	RESMOVER Subcommands						|
|                                                                       |
+----------------------------------------------------------------------*/
Table	 CT_RMOVRSUB =
{
    {  1, CT_NONE,     INHERIT,	NONE,		"CREATE"  },
    {  2, CT_NONE,     INHERIT,	NONE,		"OPEN" },
    {  3, CT_NONE,     INHERIT,	NONE,		"CLOSE" },
    {  4, CT_NONE,     INHERIT,	NONE,		"QUIT" },
};

