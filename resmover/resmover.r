/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/resmover.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/resmover/resmover.r_v  $
|   $Workfile:   resmover.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:51 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Resource Mover Resources					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>
#include <dlogbox.h>
#include <dlogids.h>
#include "resmover.h"
#include "rmovrcmd.h"
#include "rmovrtxt.h"

/*----------------------------------------------------------------------+
|									|
|   Resource Mover Dialog Box						|
|									|
+----------------------------------------------------------------------*/
#define XW  DLOGCOLS*XC    	    	    /* Dialog Width. 	    	  */
#define	YH  DLOGROWS*YC    	    	    /* Dialog Height. 	    	  */
#define	X10 XC    	    	    	    /* Dialog Left Margin.    	  */
#define X20 (XW/2)	        	    /* Dlog center.        	  */

#define	X11 X10   	    	    	    /* File 1 Class List x-coord. */
#define	X12 X11+((CLASS_LIST_WIDTH+6) * XC) /* File 1 Rsc   List x-coord. */
#define X21 (X20 + 5*XC + XC/2)	    	    /* File 2 Class List x-coord. */
#define	X22 X21+((CLASS_LIST_WIDTH+6) * XC) /* File 2 Rsc   List x-coord. */
#define X30 X20-(2*XC)	    	    	    /* File Ptr Button   x-coord. */
#define X40 X20-(4*XC)	    	    	    /* All other buttons x-coord. */
#define X50 (X20 - 5*XC)    	 	    /* Change Alias Text x-coord. */

#define Y10 2*YC			    /* File Ptr Button   y-coord  */
#define Y11 Y10+(2*YC)    	    	    /* List boxes        y-coord. */
#define Y20 Y11+(3*YC)    	    	    /* Copy Button    	 y-coord. */
#define Y30 Y20+(3*YC)    	    	    /* Delete Button	 y-coord. */
#define Y40 Y30+(7*YC)	    	    	    /* Change Alias Text y-coord. */

DialogBoxRsc DIALOGID_ResMover =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XW, YH,
    NOHELP, MHELP, HOOKDIALOGID_ResMover, NOPARENTID,
    TXT_ResMoverTitle,
{
{{  0, 	 0, 0, 	    0}, MenuBar,    MENUBARID_ResMover,     ON, 0, "", ""},
{{X11, Y11, 0, 	    0}, ListBox,    LISTID_File1_Classes,   ON, 0,
    	    	    	    	    	    	    TXT_ResFile1Label, ""},
{{X12, Y11, 0, 	    0}, ListBox,    LISTID_File1_Resources, ON, 0, "", ""},
{{X21, Y11, 0, 	    0}, ListBox,    LISTID_File2_Classes,   ON, 0,
    	    	    	    	    	    	    TXT_ResFile2Label, ""},
{{X22, Y11, 0, 	    0}, ListBox,    LISTID_File2_Resources, ON, 0, "", ""},
{{X30, Y10, 4*XC,   0}, PushButton, PUSHBUTTONID_FilePtr,   ON, 0, "", ""},
{{X40, Y20, BUTTON_STDWIDTH, 0}, PushButton,
    	    	    	    	 PUSHBUTTONID_Copy,    	    ON, 0, "", ""},
{{X40, Y30, BUTTON_STDWIDTH, 0}, PushButton,
    	    	    	    	 PUSHBUTTONID_Del,  	    ON, 0, "", ""},
{{X50, Y40, MAX_ALIAS_LENGTH*XC, 0},  Text, TEXTID_ChangeAlias, ON, 0,
    	    	    	    	    	    	  TXT_AliasFieldLabel, ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   ResMover MenuBar							|
|									|
+----------------------------------------------------------------------*/
DItem_MenuBarRsc MENUBARID_ResMover =
    {
    NOHOOK, NOARG,
	{
	{PulldownMenu, PULLDOWNMENUID_ResFile},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   ResMover Menus  							|
|									|
+----------------------------------------------------------------------*/
DItem_PulldownMenuRsc PULLDOWNMENUID_ResFile =
    {
    NOHELP, OHELPTASKIDCMD, NOHOOK, ON | ALIGN_LEFT, TXT_FileMenuLabel,
{
{TXT_NewCmdLabel,   NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD, NOHOOK, 0,
		    CMD_RESMOVER_CREATE, OTASKID, ""},
{TXT_OpenCmdLabel,  NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD, NOHOOK, 0,
		    CMD_RESMOVER_OPEN, OTASKID, ""},
{TXT_CloseCmdLabel, NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD, NOHOOK, 0,
		    CMD_RESMOVER_CLOSE, OTASKID, ""},
{"-",		    NOACCEL, OFF,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD, NOHOOK, 0,
		    NOCMD, OTASKID, ""},
{TXT_QuitCmdLabel,  NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD, NOHOOK, 0,
		    CMD_RESMOVER_QUIT, OTASKID, ""}
}
    };


/*----------------------------------------------------------------------+
|									|
|   Item Instances							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    Text Fields							|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_ChangeAlias =
    {
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_ChangeAlias,
    NOARG, (MAX_ALIAS_LENGTH-1), "%s", "%s", "","", NOMASK, NOCONCAT,
    "",
    "resMoverP->editAlias"
    };

/*----------------------------------------------------------------------+
|									|
|   List Items	    	         					|
|									|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTID_File1_Classes =
    {
    NOHELP, MHELP, HOOKITEMID_List_ResMover, NOARG, 0, LISTHEIGHT, 0, "",
	{
	/* resourceclass is the only column in this list */
    	   {(CLASS_LIST_WIDTH+1)*XC,  CLASS_LIST_WIDTH, 0, TXT_ListRscClass},
	}
    };

DItem_ListBoxRsc LISTID_File1_Resources =
    {
    NOHELP, MHELP, HOOKITEMID_List_ResMover, NOARG, 0, LISTHEIGHT, 0, "",
	{
	/* resourceID    column */
	    {(RSCID_COL_WIDTH+1)*XC,  RSCID_COL_WIDTH, 0, TXT_ListRscId},
	/* alias    	 column */
	    {(ALIAS_COL_WIDTH+1)*XC,  ALIAS_COL_WIDTH, 0, TXT_ListAlias},
	}
    };

DItem_ListBoxRsc LISTID_File2_Classes =
    {
    NOHELP, MHELP, HOOKITEMID_List_ResMover, NOARG, 0, LISTHEIGHT, 0, "",
	{
	/* resourceclass is the only column in this list */
    	   {(CLASS_LIST_WIDTH+1)*XC,  CLASS_LIST_WIDTH, 0, TXT_ListRscClass},
	}
    };

DItem_ListBoxRsc LISTID_File2_Resources =
    {
    NOHELP, MHELP, HOOKITEMID_List_ResMover, NOARG, 0, LISTHEIGHT, 0, "",
	{
	/* resourceID    column */
	    {(RSCID_COL_WIDTH+1)*XC,  RSCID_COL_WIDTH, 0, TXT_ListRscId},
	/* alias    	 column */
	    {(ALIAS_COL_WIDTH+1)*XC,  ALIAS_COL_WIDTH, 0, TXT_ListAlias},
	}
    };

/*----------------------------------------------------------------------+
|									|
|    PushButton Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_FilePtr =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP,
    HOOKITEMID_Button_ResMover, 0, NOCMD, MCMD, "",
    "<<"
    }

DItem_PushButtonRsc PUSHBUTTONID_Copy =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP,
    HOOKITEMID_Button_ResMover, 0, NOCMD, MCMD, "",
    TXT_ButtonCopy
    }

DItem_PushButtonRsc PUSHBUTTONID_Del =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP,
    HOOKITEMID_Button_ResMover, 0, NOCMD, MCMD, "",
    TXT_ButtonDelete
    }
