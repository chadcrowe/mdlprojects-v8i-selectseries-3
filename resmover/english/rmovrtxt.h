/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/english/rmovrtxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/english/rmovrtxt.h,v $
|   $Workfile:   rmovrtxt.h  $
|   $Revision: 1.3.76.1 $
|   	$Date: 2013/07/01 20:40:51 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	English language strings for Resource Mover Resources		|
|									|
+----------------------------------------------------------------------*/
#if !defined (__rmovrtxtH__)
#define __rmovrtxtH__

/*----------------------------------------------------------------------+
|									|
|    ResMover Dialog Box    	    	    	    	    	    	|
|									|
+----------------------------------------------------------------------*/
#define TXT_ResMoverTitle    	"Resource Mover"
#define TXT_AliasFieldLabel     "Change Alias"
#define	TXT_ResFile1Label	"Filename 1"
#define	TXT_ResFile2Label	"Filename 2"

/*----------------------------------------------------------------------+
|									|
|    File PullDownMenu							|
|									|
+----------------------------------------------------------------------*/
#define TXT_FileMenuLabel    	"File"
#define TXT_NewCmdLabel	    	"New..."
#define TXT_OpenCmdLabel    	"Open..."
#define TXT_CloseCmdLabel    	"Close"
#define TXT_QuitCmdLabel    	"Unload"

/*----------------------------------------------------------------------+
|									|
|    List Box Text							|
|									|
+----------------------------------------------------------------------*/
#define TXT_ListRscClass    	"Class"
#define TXT_ListRscId	    	"ResourceID"
#define TXT_ListAlias	    	"Alias"

/*----------------------------------------------------------------------+
|									|
|    Pushbutton Text							|
|									|
+----------------------------------------------------------------------*/
#define TXT_ButtonCopy	    	"Copy"
#define TXT_ButtonDelete    	"Delete"

#endif /* !defined (__rmovrtxtH__) */
