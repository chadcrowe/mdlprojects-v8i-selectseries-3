/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/english/rmvrmsgs.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/english/rmvrmsgs.r,v $
|   $Workfile:   rmvrmsgs.r  $
|   $Revision: 1.3.76.1 $
|   	$Date: 2013/07/01 20:40:52 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Resmover message resources					|
|									|
+----------------------------------------------------------------------*/
#include    <dlogids.h>
#include    <rscdefs.h>
#include    <resmover.h>

/*----------------------------------------------------------------------+
|                                                                       |
|	   Message Resources						|
|                                                                       |
+----------------------------------------------------------------------*/
/* Prompt and Status message */

MessageList STRINGID_Messages = 
{
  {
    { 0, "Create Resource File" },
    { 1, "Open Resource File" },
    { 2, "Close Resource File" },
    { 3, "     (null)     " },  /* Try to keep centered in 16 char field. */
    { 4, "Filename 1" },
    { 5, "Filename 2" },
  }
};

MessageList STRINGID_Errors = 
{
    {
    {  0, "Unable to load command table." },
    {  1, "Unable to load/find dialog box." },
    {  2, "Cannot open %s" },
    {  3, "That resource file is already open." },
    {  4, "You must close the existing file." },
    {  5, "Two resource files must be opened." },
    {  7, "You must first select a resource." },
    {  8, "Unable to load the specified resource." },
    {  9, "Error while attempting to write the new resource to file." },
    { 10, "Write protection violation occurred during file access." },
    { 11, "The resource already exists in the destination file." },
    { 12, "Insufficient memory to complete the request." },
    { 13, "An invalid resourceclass was specified." },
    { 14, "Undefined error occurred." },
    { 15, "At least one resource file must be opened first." },
    { 16, "The resource is in use. It cannot be deleted now." },
    { 17, "Are you sure you want to delete the selected resource?" },
    { 18, "The attempt to change the alias failed." },
    { 19, "A resource already exists with that ID and alias." },
    { 20, "Cannot open RESMOVER resource file" },
    }
};
