/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/resmover/resmover.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/resmover/resmover.h_v  $
|   $Workfile:   resmover.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:51 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|  resmover.h - Resource Definition for Resource Mover Resources	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__resmoverH__)
#define	    __resmoverH__

#define	    DIALOGID_ResMover		1
#define	    STRINGID_Messages		1
#define	    STRINGID_Errors		2
#define	    MENUBARID_ResMover		1
#define	    TEXTID_File1		1
#define	    TEXTID_File2		2
#define     TEXTID_ChangeAlias		3
#define	    LISTID_File1_Classes	1
#define	    LISTID_File2_Classes	2
#define	    LISTID_File1_Resources	3
#define	    LISTID_File2_Resources	4
#define	    PUSHBUTTONID_FilePtr	1
#define	    PUSHBUTTONID_Copy		2
#define	    PUSHBUTTONID_Del		3
#define	    PULLDOWNMENUID_ResFile	1

#define	    HOOKDIALOGID_ResMover	1
#define	    HOOKITEMID_List_ResMover	2
#define	    HOOKITEMID_Button_ResMover	3
#define	    HOOKITEMID_ChangeAlias	4

#define	    CLASSLIST1ITEMNUMBER	1
#define	    CLASSLIST2ITEMNUMBER	3
#define	    BUTTON_ITEMNUM		5
#define     ALIASTEXTITEMNUM		8

#define	    CLASS_LIST_WIDTH		4
#define	    RSCID_COL_WIDTH		10
#define	    ALIAS_COL_WIDTH		16
#define	    LISTWIDTHS (CLASS_LIST_WIDTH + RSCID_COL_WIDTH + ALIAS_COL_WIDTH)
#define	    LISTHEIGHT			10
#define	    FILE_UNUSED			-1
#define     MAX_ALIAS_LENGTH		(16 + 1)

#define     DLOGCOLS ( ((LISTWIDTHS + 11)*2) + (BUTTON_STDWIDTH/XC) + 6 )
#define     DLOGROWS	(LISTHEIGHT + 9)

#if !defined (resource)
typedef struct resourcefileinfo
    {
    char		fileName [128];
    RscFileHandle	rfHandle;
    ULong		selClass;
    ULong		selRscId;
    char    	    	selAlias [MAX_ALIAS_LENGTH];
    StringList		*classListP;
    RawItemHdr	    	*classListBoxHdrP;
    StringList		*rscListP;
    RawItemHdr	    	*rscListBoxHdrP;
    } ResourceFileInfo;

typedef struct resmoverglobals
    {
    char    	    	editAlias [MAX_ALIAS_LENGTH];
    int			dialogBoxP;
    RscFileHandle	resMoverRfHandle;
    int			sourceFile;
    int			destinationFile;
    ResourceFileInfo	rscFileInfo [2];
    } ResMoverGlobals;
#endif /* !defined (resource) */

#endif /* !defined (__resmoverH__) */
