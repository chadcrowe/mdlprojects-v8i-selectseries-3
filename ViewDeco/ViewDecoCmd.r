/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/ViewDecoCmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   ViewDeco  $
|   $Revision: 1.2.12.1 $
|   	$Date: 2013/07/01 20:35:36 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ViewDeco - ViewDeco source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

/*----------------------------------------------------------------------+
|									|
|   Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	    CT_NONE	    0
#define	    CT_MAIN	    1
#define     CT_ACTION	    2
#define     CT_ACTYPE        3
#define     CT_DIALOG       4

/*----------------------------------------------------------------------+
|                                                                       |
|   Application command syntax   					|
|                                                                       |
+----------------------------------------------------------------------*/
Table CT_MAIN =
{ 
    { 1, CT_ACTION, PLACEMENT, REQ,	"VIEWDECO" }, 
}

Table CT_ACTION =
{ 
    { 1, CT_ACTYPE,       INHERIT,	NONE,      "ACTION" },
}

Table CT_ACTYPE =
{ 
    { 1, CT_NONE,       INHERIT,	NONE,      "DIALOG" },
    { 2, CT_NONE,       INHERIT,	NONE,      "DECORATE" },
    { 3, CT_NONE,       INHERIT,    NONE,       "SETTITLE"},
    { 4, CT_NONE,       INHERIT,    NONE,       "ADDSPRITE"},
    { 5, CT_NONE,       INHERIT,    NONE,       "DELSPRITE"},
    { 6, CT_NONE,       INHERIT,    NONE,      "UPDATE"},
    { 7, CT_NONE,       INHERIT,    NONE,      "TOOL"},
}

