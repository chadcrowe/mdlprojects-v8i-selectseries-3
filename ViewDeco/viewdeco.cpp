/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/viewdeco.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   ViewDeco  $
|   $Revision: 1.2.12.1 $
|       $Date: 2013/07/01 20:35:36 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ViewDeco - ViewDeco source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Function -                                                          |
|                                                                       |
|   ViewDeco.cpp -- Example of sprite and decorator of                  |
|                  element manipulation. 
|                   Example of view title set and dgn file display      |
|                                                                       |
|   This example application shows how to use IViewDraw and             |
|   IViewTransients structs to add or delete an icon to selected        |
|   element. The application also allows user to select multiple        |
|   elements with ctrl key but only change the color and display        |
|   decorators for selected arc elements. User can change the color     |
|   for decorator by update cmd. The size of decorators can be adjusted |
|   by dialog with max and min hooks in extended cases.                 |
|                                                                       |
|   To use this application, key-in mdl load viewdemo                           |
|                                                                       |
|       - - - - - - - - - - - - - - - - - - - - - - - - - - - - -       |
|                                                                       |
|   Public Routine Summary -                                            |
|                                                                       |
|   ViewDeco_exampleTool - allow users to select multiple elements and  |
|       set selected arc element to color 4                             |
|       ViewDeco_spriteStuffAdd - Add an icon to selected element               |
|       ViewDeco_spriteStuffDel - Delete an icon to selected element        |
|       ViewDeco_viewDecoratorDialogOpen - Open a dialog with max and min       |
|               item hooks                              |
|       ViewDeco_viewDecoratorCmd - Disaplay decorators for selected arc        |
|               elements                                |
|   ViewDeco_setRGBValues - Update decorator color corresponding to     |
|       element color 
|   ViewDeco_setViewTitle - Set the callback to allow the view title    |
|       to be changed               |
|   ViewDeco_viewDisplayMode - Displays information of the active       |
|       dgnfile                     |

|       main - main entry point                                         |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
//#undef Private 
#include <windows.h>

#include <MicroStationAPI.h>
#include <mstnviewport.h>
#if defined (CF_MAX)
#undef CF_MAX
#endif

#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <mselems.h>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <msstrlst.h>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>

#include    <msrmgr.h>
#include    <mssystem.fdf>
#include    <msparse.fdf>

#include        <toolsubs.h>
//scan code
#include        <elementref.h>
//locate code
#include        <msdependency.fdf>
#include        <msassoc.fdf>
#include        <msmisc.fdf>
#include        <mslocate.fdf>
#include        <msstate.fdf>
#include        <msoutput.fdf>

#include    <mstypes.h>
//place command
#include        <mstmatrx.fdf>


#include    <basetype.h>
#include    <string.h>
#include    <mdlerrs.h>
#include    <dlogitem.h>
#include    <userfnc.h>
#include    <msrmgr.h>

#include    <changetrack.fdf>
#include    <embeddeddpoint3darray.fdf>
#include    <mselemen.fdf>
#include    <msparse.fdf>
#include    <mscexpr.fdf>

#include    <RefCounted.h>
#include    <interface/IViewDraw.h>
#include    <interface/MstnTool.h>
#include    <interface/element/Handler.h>
#include    <interface/element/DisplayHandler.h>

static ISpriteP        s_pSprite;

/*number of selected arc elements*/
static int num = 0;

#include "ViewDecoCmd.h"
#include "ViewDeco.h"
#include "exampletool.h"
LgndInfo                lgndInfo;

USING_NAMESPACE_BENTLEY
USING_NAMESPACE_BENTLEY_USTN
USING_NAMESPACE_BENTLEY_USTN_ELEMENT
USING_NAMESPACE_TEXT

/*----------------------------------------------------------------------+
|                                                                       |
|   Local type definitions                                              |
|                                                                       |
+----------------------------------------------------------------------*/
/*=================================================================================**//**
* Example per-element info.
*
* @bsiclass
+===============+===============+===============+===============+===============+======*/
struct          ExampleToolInfo : RefCounted <IElementState>
{
private:
    DPoint3d    m_point;

public:
    ExampleToolInfo::ExampleToolInfo (DPoint3d& location) {m_point = location;}

    DPoint3d const* GetLocation () {return &m_point;}

}; // ExampleLocateInfo

/*=================================================================================**//**
* Contrived example tool:
* 
* Allow user to locate multiple elements using ctrl selection.
* Tool completes when a minimum of a single element is selected,
* the control key is used to modify the selection. 
* Only the color of arc elements will be changed.
*
* Demonstrates using WantAdditionalLocate and graphic and
* named groups are supported in this exampletool.
*
* @bsiclass
+===============+===============+===============+===============+===============+======*/
struct          ExampleTool : MstnElementSetTool
{

public:

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/

virtual UsesSelection   ExampleTool::AllowSelection () override {return USES_SS_None;}
virtual bool            ExampleTool::WantDynamics () override {return false;}
virtual bool            ExampleTool::NeedAcceptPoint () override {return true;}

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
void            ExampleTool::SetupForLocate (int msgId)
    {
    bool        doLocate = (PROMPT_ExampleToolFirst == msgId || PROMPT_ExampleToolNext == msgId);

    __super::SetLocateCursor (doLocate ? true : false, -1);

    mdlOutput_rscPrintf (MSG_PROMPT, NULL, STRINGLISTID_Prompts, msgId);
    }

/*---------------------------------------------------------------------------------**//**
* @description  OnElementModify - Calculate number of arc elements and set them to color 4.
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
virtual StatusInt   ExampleTool::OnElementModify (EditElemHandleR elHandle) override
    {
    UInt32      color = 1;

        if (ARC_ELM == elHandle.GetElementType())
        {
                /*set element color*/
                mdlElmdscr_setSymbology (elHandle.GetElemDescrP (), &color, NULL, NULL, NULL);  
                /*set correspoinding decorator's color*/
                lgndInfo.colorIndex[num] = color;
                num++;
        }

    return SUCCESS;
    }

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
virtual bool    ExampleTool::OnPostLocate (HitPathCP path, char *cantAcceptReason) override
    {
    if (!__super::OnPostLocate (path, cantAcceptReason))
        return false;

    ElemHandle  elHandle (mdlDisplayPath_getCursorElem ((DisplayPathP) path), mdlDisplayPath_getPathRoot ((DisplayPathP) path));

    return true;
    }
/*---------------------------------------------------------------------------------**//**
* @description  WantAdditionalLocate - Make sure at least two elements are selected.
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
virtual bool    ExampleTool::WantAdditionalLocate (MstnButtonEventCP ev) override
    {
    if (NULL == ev)
        return true; // This is a multi-locate tool...

    /*Require a minumum of a single element, if control is down select additional elements*/
    return (GetElemAgendaP ()->GetCount () < 1 || ev->IsControlKey ());
    }

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
virtual bool    ExampleTool::OnModifierKeyTransition (bool wentDown, int key) override
    {
    bool        changed = __super::OnModifierKeyTransition (wentDown, key);

    /*Control key state change, may need to enable/disable auto-locate, change cursor, prompts. etc.*/
    if (TOGGLESELECT_MODKEY != key)
        return changed;

    if (GetElemAgendaP ()->GetCount () < 1)
        return changed; 
        
    SetupForLocate (wentDown ? PROMPT_ExampleToolNext : PROMPT_ExampleToolPhase1Complete);

    return true;
    }

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
virtual void    ExampleTool::SetupAndPromptForNextAction () override
    {
    /*All changes to auto-locate/accusnap state and user prompts are done here!!!*/
    SetupForLocate (GetElemAgendaP ()->GetCount () < 1 ? PROMPT_ExampleToolFirst : PROMPT_ExampleToolComplete);
    }

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
virtual void    ExampleTool::OnRestartCommand () override
    {
    /*Must restart or call ExitTool!*/
    ExampleTool* newTool = new ExampleTool ();

    newTool->InstallTool ();
    }

}; // ExampleTool

/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_exampleTool - allow users to select multiple elements and  
*       set selected arc element to color 4                            
* @param        unparsed      The unparsed information sent to the command
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void ViewDeco_exampleTool 
(
char * unparsed
)
    {
         ExampleTool *pTool = new ExampleTool ();

         pTool->InstallTool();
    }

//using my own local color def structure.
typedef struct colordef
{
   int red;
   int green;
   int blue;
} ColorDef;

ColorDef     colors [11] = 
        {
        {255,0,0},
        {255,192,203},
        {255,99,71},
        {255,215,0},
        {255,255,0},
        {238,130,238},
        {148,0,211},
        {30,144,255},
        {0,0,255},
        {240,248,255},
        {0,0,0},
        };

/*----------------------------------------------------------------------+
|                                                                       |
|   Local type definitions                                              |
|                                                                       |
+----------------------------------------------------------------------*/
Public  void    DrawSpriteSample
(
 IViewContextP context
 )
    {
    IViewDrawP      pViewDraw = context->GetIViewDraw();
    DVec3d        dirVec;
   /*This will follow the last data point on the view.*/   
    dirVec.x = statedata.inPoint.dpUors.x;
    dirVec.y = statedata.inPoint.dpUors.y;
    dirVec.z = statedata.inPoint.dpUors.z;
        /*Draw a sprite at a specific location*/
    pViewDraw->DrawSprite (s_pSprite,&dirVec,NULL,0);
    }
/*=================================================================================**//**
* @bsiclass
+===============+===============+===============+===============+===============+======*/
struct DisplaySpriteSample : IViewTransients
    {
        /*Update a view*/
    virtual void _DrawTransients (IViewContextP context, bool isPreUpdate) override
        {
        if (isPreUpdate|| (NULL != context->GetIPickGeom()))
            return;
        IViewportP viewport = context->GetViewport();
        if (NULL== viewport)
            return;
        DrawSpriteSample(context);
        }
    };

static DisplaySpriteSample s_SpriteSample;

/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_spriteStuffAdd
* @param        unparsed      char 
* @add an icon to selected element
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void ViewDeco_spriteStuffAdd
(
 char   *unparsed
 )
    {
        NotifyMessageDetails  message(MESSAGE_ERROR,"Icon Not Found","Check the icon id",0,MESSAGE_ALERT_DIALOG);
    /*Check the avaliable icon*/  
    if (NULL != s_pSprite->GetIcon ())
        IViewManager::GetManager().AddViewTransientHandler(&s_SpriteSample);
    else
        NotificationManager::OutputMessage (message);
    }

/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_spriteStuffDel
* @param        unparsed      char 
* @delete an icon to selected element
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void ViewDeco_spriteStuffDel
(
 char   *unparsed
 )
    {
    IViewManager::GetManager().DropViewTransientHandler(&s_SpriteSample);
    }


/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_maxItemHook
* @param        dmP      The DialogItemMessage 
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void  ViewDeco_maxItemHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
            case DITEM_MESSAGE_STATECHANGED:
                {
              /*  ValueUnion  val;
                int         fmt;

                
                mdlDialog_itemGetValue (&fmt,&val,NULL,dimP->db,dimP->itemIndex,0);
                lgndInfo.max = val.doubleFormat;*/
                }
            break;
            default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    }


/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_minItemHook
* @param        dmP      The DialogItemMessage 
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void  ViewDeco_minItemHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
            case DITEM_MESSAGE_STATECHANGED:
                {
                //ValueUnion  val;
                //int         fmt;

                //
                //mdlDialog_itemGetValue (&fmt,&val,NULL,dimP->db,dimP->itemIndex,0);
                //lgndInfo.min = val.doubleFormat;
                }
            break;
        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Local type definitions                                              |
|                                                                       |
+----------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------**//**
* @description  DisplayDecoratorSample : IViewDecoration - Disaplay decorators for 
*    selected arc elements              
* 
* @bsiclass                                                     BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/

struct DisplayDecoratorSample : IViewDecoration
{

  Public virtual bool _DrawDecoration(ViewportP vp) override
    {
    /*return for all but view 0*/
    if (vp->GetViewNumber() > 0)
        return true;
    ViewFlags    viewFlags;
    UInt32       renderMode;

    IViewOutput* pViewOutput = vp->GetIViewOutput();
    if (!pViewOutput)
        return true;

        /*Set the coordinate system temporarily to COORDSYS_View*/
    pViewOutput->SetToViewCoords (true);
    /*Get the view settings flags for a view*/
    mdlView_getFlags(&viewFlags,vp->GetViewNumber());

    //pViewOutput->SetRenderMode = VIEWMODE_WIREFRAME;
    renderMode = viewFlags.renderMode;

        /*Set the render mode (SMOOTH, PHONG, CONSTANT, etc.), defined in msdefs.h */
    viewFlags.renderMode = VIEWMODE_WIREFRAME;
    
        /*Set the view settings flags for IViewOutput*/
    pViewOutput->SetDrawViewFlags (&viewFlags);
        /*Set the view settings flags for a view*/
    mdlView_setFlags (&viewFlags,vp->GetViewNumber());
    
    DPoint3d cursorPtView[4];
    int i = 0;
        
    WString     str;
    DPoint2d    scale;
    scale.x = lgndInfo.textScale;
    scale.y = lgndInfo.textScale;
    TextParamWide textParam;
    memset (&textParam,0,sizeof textParam);
    textParam.font = lgndInfo.fontId;
    RotMatrix   rMatrix;
    DVec3d    ptTest;
    ptTest.x = 1.0;

    DVec3dCP        primary = new DVec3d(ptTest);
    ptTest.x = 0.0;
    ptTest.y = 1.0;
    DVec3dCP        secondary= new DVec3d(ptTest);

    /*because the coordinates are reversed in the viewoutput set to viewcoords.*/
    mdlView_getStandard(&rMatrix,STANDARDVIEW_Bottom);
    //initialize this data.
    cursorPtView[2].y = 0;
    char buffer[50];  //needs to have the format string size!
    BSIRect     viewRect;
    DPoint3dCP  centerPt;
    double      quaterion[4];
    mdlRMatrix_toQuat (quaterion,&rMatrix);

    vp->GetViewRect(&viewRect,COORDSYS_View);//upper left is 0,0 (origin) and lower right is corner.
    double incrementAmount = (lgndInfo.max - lgndInfo.min)/lgndInfo.increment;
    double amt = lgndInfo.max;

        for (i=0;i<num;++i )
        {
        /*move to the right side of the view*/
        cursorPtView[0].x = viewRect.corner.x - lgndInfo.boxX;
        cursorPtView[0].y = 0 + lgndInfo.boxY*(i+1);//since this is always in the upper part.
        cursorPtView[0].z = 0.0;

        cursorPtView[1].x = viewRect.corner.x;
        cursorPtView[1].y = cursorPtView[0].y;
        cursorPtView[1].z = 0.0;

        cursorPtView[2].x = cursorPtView[1].x;
        cursorPtView[2].y = cursorPtView[1].y -(lgndInfo.boxY);
        cursorPtView[2].z = 0.0;

        cursorPtView[3].x = cursorPtView[0].x;
        cursorPtView[3].y = cursorPtView[2].y;
        cursorPtView[3].z = 0.0;
        //kind of a silly way to set up the gradient but works for demo.
                
        //lgndInfo.max - (incrementAmount*i)
        sprintf (buffer,"%5.2lf, %s",amt,lgndInfo.unitString);
                
                
        amt -= lgndInfo.increment;
        str =  WString (buffer);

        //make up some color sillyness....
                
        /*Calculate colorValue for decorator background*/
        int colorValue = vp->MakeTrgbColor (255,255,255,lgndInfo.transparencyValue);

        vp->SetSymbologyRGB (colorValue, colorValue, 0, 0);
        
        centerPt = new DPoint3d (cursorPtView[0]);
        
        pViewOutput->DrawShape3d (4,cursorPtView,true,NULL);
                
        vp->SetSymbologyRGB (vp->GetBackgroundColor (), vp->GetBackgroundColor (), 1, 0);
        
        delete (centerPt);       

        DPoint3d IncoPts[4];
        IncoPts[0].x = cursorPtView[0].x + 10;
        IncoPts[0].y = cursorPtView[0].y - 5;
        IncoPts[0].z = 0.0;
        IncoPts[1].x = cursorPtView[1].x - 10;
        IncoPts[1].y = cursorPtView[1].y - 5;
        IncoPts[1].z = 0.0;
        IncoPts[2].x = cursorPtView[2].x - 10;
        IncoPts[2].y = cursorPtView[2].y + 5;
        IncoPts[2].z = 0.0;
        IncoPts[3].x = cursorPtView[3].x + 10;
        IncoPts[3].y = cursorPtView[3].y + 5;
        IncoPts[3].z = 0.0;

        /*Calculate colorValue for decorator*/
        int colorValueInco = vp->MakeTrgbColor (colors[i].red,colors[i].green,colors[i].blue,lgndInfo.transparencyValue);

        vp->SetSymbologyRGB (colorValueInco, colorValueInco, 0, 0);
        
        centerPt = new DPoint3d (cursorPtView[0]);
        
        pViewOutput->DrawShape3d (4,IncoPts,true,NULL);
                
        vp->SetSymbologyRGB (vp->GetBackgroundColor (), vp->GetBackgroundColor (), 1, 0);
        
        delete (centerPt);  
        double dep = 1.0;
        }

    viewFlags.renderMode = renderMode;
    /*reset viewFlags*/
    pViewOutput->SetDrawViewFlags (&viewFlags);
    mdlView_setFlags (&viewFlags,vp->GetViewNumber());
        /*reset coordinates*/
    pViewOutput->SetToViewCoords (false);
        
    return  true;
    }

};
/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_setRGBValues
* @param        
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
void ViewDeco_setRGBValues
(
)
    {
    byte    rgb[3];
    int     mstrElemColor;
    UInt32  status;
    int     i;

    for (i=0;i<=10;++i)
        {
        status = mdlColor_elementColorToRGB (rgb,&mstrElemColor,mdlModelRef_getActive (),lgndInfo.colorIndex[i],NULL);
        colors[i].red = rgb[0];
        colors[i].green = rgb[1];
        colors[i].blue = rgb[2];
        }
    }

static DisplayDecoratorSample s_DecoratorSample;
/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_viewDecoratorCmd
* @param        unparsed      on to turn on the decorator anything else turns off the decorator 
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void ViewDeco_viewDecoratorCmd
(
char*   unparsed
)
{

   if ((unparsed)&&(0!=unparsed))
        if ((0==strcmpi ("on", unparsed))&&(lgndInfo.max - lgndInfo.min > 0))
            {
                                /*Use element colors for decorators*/
                ViewDeco_setRGBValues();
                                /*add the decorator*/
                IViewManager::GetManager().AddViewDecoration (&s_DecoratorSample); 
                                
            }
        else // if they send in off or anything else we turn off.
                    {
                                /*Remove the decorator*/
                            IViewManager::GetManager().DropViewDecoration(&s_DecoratorSample);
                    }
   else //no arg sent in then turn this off.
            /*Remove the decorator*/
        IViewManager::GetManager().DropViewDecoration(&s_DecoratorSample);
}
/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_viewDecoratorDialogOpen
* @param        unparsed      not used. 
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void ViewDeco_viewDecoratorDialogOpen
(
char*   pUnparsed
)
    {
    DialogBox*  pDbox = NULL;

        /*Set parameters for decorators*/
    if (NULL == (pDbox = mdlDialog_find (DIALOGID_legend,0)))
         {
        lgndInfo.textScale = 40;
        lgndInfo.boxX = 40;
        lgndInfo.boxY = 20;
        lgndInfo.increment = 10;//this makes a nice demo set.
        lgndInfo.max = 100.0;
        lgndInfo.min = 0.0;
       // lgndInfo.textScale.y = 20;
        lgndInfo.fontId = 0;

        mdlDialog_open (0,DIALOGID_legend);
    }
    else
        mdlDialog_show (pDbox);
    }
/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_viewDecoratorUpdateColors a command to set the internal color by element color
* @param        unparsed      Not used. 
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void ViewDeco_viewDecoratorUpdateColors
(
char*   unparsed
)
{
    ViewDeco_setRGBValues();
}
/*---------------------------------------------------------------------------------**//**
* @description  MyViewMonitor:Bentley::Ustn::IViewMonitor
*
* @bsiclass                                                     BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
struct MyViewMonitor:Bentley::Ustn::IViewMonitor
{
        WString   str;       //i just set this one time as a demo.
        void    _OnViewChanged (IViewportP vp){}
        void    _onViewOpen (IViewportP vp){}
        void    _OnViewClose (IViewportP vp){} 
        void    _OnGetViewTitle (IViewportP vp,WStringR title)
        {
                //we could look at the viewport to see what the view number is and do 
                //something based on that info.  but this is a simple test.
                 title=str;
        }
        void    _OnViewEnterOrExit (int changeType){}
};
static MyViewMonitor    s_viewMon;
/*---------------------------------------------------------------------------------**//**
* @description  ViewDeco_setViewTitle sets the callback to allow the view title to be changed.
* @param        unparsed      The string to add to the view title 
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void  ViewDeco_setViewTitle
(
char*   unparsed
)
{
     WString   str;
     str = WString (unparsed);
         int    viewIndex;
         /*add a ViewMonitor*/
         IViewManager::GetManager().AddViewMonitor (&s_viewMon);
     /*copy str to ViewMonitor*/
         s_viewMon.str = str;
     viewIndex = IViewManager::GetManager().GetSelectedView();
     
         /*set the selected view title*/
     IViewManager::GetManager ().SetViewTitle (viewIndex);
         //for this experiment I don't need to keep this.
        // IViewManager::GetManager().DropViewMonitor (&s_viewMon);

         return;
}

Public DialogHookInfo uHooks[]=
    {
    {HOOKITEMID_MaxInfo,        (PFDialogHook)ViewDeco_maxItemHook},
    {HOOKITEMID_MinInfo,        (PFDialogHook)ViewDeco_minItemHook},
    };

/*---------------------------------------------------------------------------------**//**
* @description  MdlMain
* @param        argc      The number of command line parameters sent to the application.
* @param        argv[]    The array of strings sent to the application on the command line.
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
        RscFileHandle   rfHandle;
    SymbolSet       *setP;
    int             iconID = 102;
    HINSTANCE       ustn = GetModuleHandleW(L"ViewDeco.dll");

    mdlResource_openFile (&rfHandle,NULL,RSC_READONLY);

    //load up the sprite when the app is loaded.
    s_pSprite = ISprite::CreateFromIconResource(iconID,ustn);

    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);

    mdlDialog_publishComplexVariable (setP, "lgndinfo", "lgndInfo", &lgndInfo);

     lgndInfo.max = 0.0;
     lgndInfo.min = 0.0;
     lgndInfo.increment = 1;

    // Map command name to function (usage: MDL COMMAND COMPEXPORT)
    static  MdlCommandName cmdNames[] = 
    {
        {ViewDeco_exampleTool, "ViewDeco_ExampleTool"  },
        {ViewDeco_viewDecoratorDialogOpen, "ViewDeco_viewDecoratorDialogOpen"  },
        {ViewDeco_viewDecoratorCmd,"ViewDeco_viewDecoratorCmd"},
        {ViewDeco_setViewTitle,"ViewDeco_setViewTitle"},
        {ViewDeco_spriteStuffAdd,"ViewDeco_spriteStuffAdd"},
        {ViewDeco_spriteStuffDel,"ViewDeco_spriteStuffDel"},
        0,
    };

    mdlSystem_registerCommandNames (cmdNames);

    // Map key-in to function
    static MdlCommandNumber cmdNumbers[] =
    {
         {ViewDeco_exampleTool,  CMD_VIEWDECO_ACTION_TOOL },
         {ViewDeco_viewDecoratorDialogOpen,CMD_VIEWDECO_ACTION_DIALOG },
         {ViewDeco_viewDecoratorCmd,CMD_VIEWDECO_ACTION_DECORATE },
         {ViewDeco_setViewTitle,CMD_VIEWDECO_ACTION_SETTITLE},
         {ViewDeco_spriteStuffAdd,CMD_VIEWDECO_ACTION_ADDSPRITE},
         {ViewDeco_spriteStuffDel,CMD_VIEWDECO_ACTION_DELSPRITE},
         {ViewDeco_viewDecoratorUpdateColors ,CMD_VIEWDECO_ACTION_UPDATE},
        0,
    };

    mdlSystem_registerCommandNumbers (cmdNumbers);
        
    mdlParse_loadCommandTable (NULL);
        
        
        return SUCCESS;
    }
