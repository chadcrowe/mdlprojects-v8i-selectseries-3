/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/exampletool.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#pragma once

#define STRINGLISTID_Commands               0
#define STRINGLISTID_Prompts                1

#define CMDNAME_ExampleTool                 1

#define PROMPT_ExampleToolFirst             1
#define PROMPT_ExampleToolNext              2
#define PROMPT_ExampleToolComplete          3