/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/english/ViewDecomsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   ViewDeco  $
|   $Revision: 1.1.12.1 $
|   	$Date: 2013/07/01 20:35:37 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ViewDeco - ViewDeco source code. |
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|																		|
|   Include Files														|
|																		|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <dlogids.h>

#include "..\ViewDeco.h"

/*----------------------------------------------------------------------+
|																		|
|   Messages List Resource Definition									|
|																		|
+----------------------------------------------------------------------*/

MessageList MESSAGELISTID_Commands =
{
    {
    {MESSAGEID_ToolSettings,	"Native Tool Settings Content"},
    {COMMANDID_PlaceDate,		"Place My Date"},
    }
};


MessageList MESSAGELISTID_Messages =
{
    {
    {MESSAGEID_abc,					""},
	{MESSAGEID_ResourceLoadError,	"Unable to open resource file"},
    }
};


MessageList MESSAGELISTID_Prompts =
{
    {
    {PROMPTMESSAGEID_abc,	""},
    {PROMPTID_EnterPoint,	"Enter data point"},
    }
};

MessageList MESSAGELISTID_Misc =
{
    {
    {MISCMESSAGEID_abc,		""},
    }
};
