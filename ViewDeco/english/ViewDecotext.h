/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/english/ViewDecotext.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   ViewDeco  $
|   $Revision: 1.1.12.1 $
|   	$Date: 2013/07/01 20:35:37 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ViewDeco - ViewDeco source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#define TXT_DialogTitle		"Native MDL Dialog"
#define TXT_HostedDialogTitle	"Hosted Dialog"
#define TXT_MDLLabel		"This is an MDL label"
