/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/ViewDeco.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   ViewDeco  $
|   $Revision: 1.1.12.1 $
|   	$Date: 2013/07/01 20:35:36 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ViewDeco - ViewDeco source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#define DIALOGID_MDLDialog	    1
#define HOOKDIALOGID_MDLDialog	    (DIALOGID_MDLDialog * 100) + 1
#define DIALOGID_HostedDialog	    2
#define HOOKDIALOGID_HostedDialog   (DIALOGID_HostedDialog * 100) + 1



#define PUSHBUTTONID_ModalDialog	2
#define PUSHBUTTONID_ModelessDialog	3
#define PUSHBUTTONID_HostedDialog	4
#define PUSHBUTTONID_MDLButton		10
#define PUSHBUTTONID_DockableDialog	5
#define PUSHBUTTONID_ToolSettingsDialog	6

#define MESSAGELISTID_Commands	    1
#define MESSAGELISTID_Messages	    2
#define MESSAGELISTID_Prompts	    3
#define MESSAGELISTID_Misc	    4
#define	MESSAGEID_ResourceLoadError 5

#define MESSAGEID_ToolSettings  1
#define MESSAGEID_abc		2
#define COMMANDID_PlaceDate	3

#define PROMPTMESSAGEID_abc	1
#define PROMPTID_EnterPoint	2

#define MISCMESSAGEID_abc	1

#define DIALOGID_legend         42
#define TEXTID_MaxInfo          43
#define TEXTID_MinInfo          44

#define HOOKITEMID_MaxInfo      6
#define HOOKITEMID_MinInfo    7

/*----------------------------------------------------------------------+
|									|
|   Typedefs for Dialog access strings					|
|									|
+----------------------------------------------------------------------*/
typedef struct lgndinfo
{
    double	          max;
    double            min;
    int               increment;  //cannot be 0;  must be the number of  boxes to show 1 or more!
    int               ascDsc;
    char              unitString[25];
    int               fontId;
    double            boxX;
    double            boxY;
    double            textScale;
    int               colorIndex[11];//needs to cover from 0 to max
    int               transparencyValue;
} LgndInfo;

/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/ViewDeco.h,v $
|    $RCSfile: ViewDeco.h,v $
|   $Revision: 1.1.12.1 $
|       $Date: 2013/07/01 20:35:36 $
|     $Author: George.Dulchinos $
|
| Copyright (c) 2001-2003;  Bentley Systems, Inc., 685 Stockton Drive,
|                      Exton PA, 19341-1151, USA.  All Rights Reserved.
|
| This program is confidential, proprietary and unpublished property of Bentley Systems
| Inc. It may NOT be copied in part or in whole on any medium, either electronic or
| printed, without the express written consent of Bentley Systems, Inc.
|
+--------------------------------------------------------------------------------------*/
#pragma once

#define STRINGLISTID_Commands               0
#define STRINGLISTID_Prompts                1
#define STRINGLISTID_Messages               2

#define CMDNAME_ExampleTool                 1

#define PROMPT_ExampleToolFirst             1
#define PROMPT_ExampleToolNext              2
#define PROMPT_ExampleToolPhase1Complete    3
#define PROMPT_ExampleToolPhase2Complete    4

#define MSG_LocateErrorPhase1               1 
#define MSG_LocateErrorPhase2               2 
