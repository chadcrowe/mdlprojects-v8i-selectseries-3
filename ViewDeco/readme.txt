========================================================================
    MDLProjectWizard : "ViewDeco" Project Overview
========================================================================

MDLProjectWizard has created this "ViewDeco" project for you as a starting point.

This file contains a summary of what you will find in each of the files that make up your project.

MDLProjectWizard.vcproj
    This is the main project file for projects generated using an Application Wizard. 
    It contains information about the version of the product that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

ViewDemo.cpp
    This is the workfile for MDL Application.
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   ViewDeco.cpp -- Example of sprite and decorator of                  |
|   	    	   element manipulation. 
|                   Example of view title set and dgn file display      |
|									|
|   This example application shows how to use IViewDraw and             |
|   IViewTransients structs to add or delete an icon to selected        |
|   element. The application also allows user to select multiple        |
|   elements with ctrl key but only change the color and display        |
|   decorators for selected arc elements. User can change the color     |
|   for decorator by update cmd. The size of decorators can be adjusted |
|   by dialog with max and min hooks in extended cases.                 |
|   	    	    	    	    	    	    	    	    	|
|									|
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -						|
|									|
|       ViewDeco_exampleTool - allow users to select multiple elements  |
|           and set selected arc element to color 4                     |
|                                                                       |
|       ViewDeco_spriteStuffAdd - Add an icon to selected element	|
|                                                                       |
|       ViewDeco_spriteStuffDel - Delete an icon to selected element	|
|                                                                       |
|       ViewDeco_viewDecoratorDialogOpen - Open a dialog with max and   |
|           min item hooks			                        |
|                                                                       |
|       ViewDeco_viewDecoratorCmd - Disaplay decorators for selected    |
|           arc	elements     	                                        |
|			                                                |
|       ViewDeco_setRGBValues - Update decorator color corresponding to |
|           element color                                               |
|                                                                       |
|       ViewDeco_setViewTitle - Set the callback to allow the view title|
|           to be changed                                               |
|                                                                       |
|       ViewDeco_viewDisplayMode - Displays information of the active   |
|           dgnfile                                                     |
|                                                                       |
|	main - main entry point						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   User Instruction -							|
|									|
|	To load this application, key-in mdl load viewdemo        	|
|                                                                       |
|       To disapley decorators, do the following steps                  |
|                                                                       |
|       1. select multiple elements with ViewDeco_exampleTool cmd       |
|          key-in viewdemo action tool                                  |
|                                                                       |
|       2. Open a dialog with ViewDeco_viewDecoratorDialogOpen cmd      |
|          key-in viewdemo action dialog                                |
|                                                                       |
|       3. display decorators with VViewDeco_viewDecoratorCmd cmd       |
|          key-in viewdemo action decorate                              |
|                                                                       |
|	To use ViewDeco_spriteStuffAdd function, key-in viewdemo        | 
|           action addsprite                                            |
|                                                                       |
|	To use ViewDeco_spriteStuffDel function, key-in viewdemo        | 
|           action delsprite                                            |
|                                                                       |
|	To use ViewDeco_setRGBValues function, key-in viewdemo          | 
|           action update                                               |
|                                                                       |
|	To use ViewDeco_setViewTitle function, key-in viewdemo action	|
|           settitle and title. e.g., key-in viewdemo action settitle   |
|           viewdemo to set title as "viewdemo"                         | 
|									|
|       To use ViewDeco_viewDisplayMode function, key-in                |
|           viewdemo action displaymode	                                |
|	                                                                |
+----------------------------------------------------------------------*/