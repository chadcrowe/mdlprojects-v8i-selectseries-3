/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/ViewDeco/ViewDecodlg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   ViewDeco  $
|   $Revision: 1.1.12.1 $
|   	$Date: 2013/07/01 20:35:36 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   ViewDeco - ViewDeco source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogids.h>
#include    <rscdefs.h>

#include    "ViewDeco.h"
#include    "ViewDecocmd.h"
#include    <ViewDecotext.h>


DialogBoxRsc    DIALOGID_legend = 
{
    DIALOGATTR_DEFAULT,
    25*XC, 15*YC,
    NOHELP,MHELP,0,NOPARENTID,
    "Legend",
        {
        {{7*XC,  GENY(3), 12*XC, 0}, Text, TEXTID_MaxInfo, ON, 0, "", ""},
        {{7*XC,  GENY(5), 12*XC, 0}, Text, TEXTID_MinInfo, ON, 0, "", ""},
        }
};



DItem_TextRsc TEXTID_MaxInfo =
   {
   NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_MaxInfo, NOARG, 
   25, "%lf", "%lf", "", "", NOMASK, NOCONCAT, 
   "Max", 
   "lgndInfo.max"
   };


DItem_TextRsc TEXTID_MinInfo =
   {
   NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_MinInfo, NOARG, 
   25, "%lf", "%lf", "", "", NOMASK, NOCONCAT, 
   "Min", 
   "lgndInfo.min"
   };

/*----------------------------------------------------------------------+
|                                                                       |
|    Push Button Items                                                  |
|                                                                       |
+----------------------------------------------------------------------*/
