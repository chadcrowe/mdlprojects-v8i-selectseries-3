/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlogdemo/dlogdemo.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   dlogdemo.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:37:53 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	dlogdemo.mc -- Dialog Box Demonstration				|
|									|
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -						|
|									|
|	dialogDemo_command - Command to open main dialog box		|
|	dialogDemo_dialogHook - Dialog box hook				|
|	dialogDemo_listBoxHook - List box hook				|
|	dialogDemo_pullDownMenuHook - Pulldown menu hook		|
|	dialogDemo_scrollBarHook - Scroll bar hook			|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <mdlio.h>
#include <dlogbox.h>
#include <dlogitem.h>
#include <rscdefs.h>

#include <mscexpr.fdf>
#include <msstrngl.fdf>
#include <msoutput.fdf>
#include <msparse.fdf>
#include <msrsrc.fdf>
#include <dlogman.fdf>  /* Dialog Box Manager Function Prototypes -
                             included as a debugging aid */
#include <msvar.fdf>
#include <mssystem.fdf>
#include "dlogdids.h"	/* contains the dialog ids for this example */
#include "dlogdcmd.h"	/* contains the command numbers for this example */

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
#define STRINGLIST_NMEMBERS	16

/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Private global variables						|
|									|
+----------------------------------------------------------------------*/
Private int dlogdemo_colorNumber = 5;
Private int dlogdemo_scrollNumber = 500;
Private int dlogdemo_optionbtnNumber1 = 1;
Private int dlogdemo_optionbtnNumber2 = 0;

/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   External variables							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Local function declarations 					|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   DialogDemo Code Section						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name		dialogDemo_command					|
|									|
| comments	Opens main dialog box.					|
|									|
| author	BSI 					10/90		|
|									|
+----------------------------------------------------------------------*/
void dialogDemo_command
(
char    *unparsedP
) 
//cmdNumber CMD_DLOGDEMO

    {
    /* Open Dialog Box */
    mdlDialog_open (NULL, DIALOGID_DialogDemo);
    mdlDialog_open (NULL, DIALOGID_DialogDemoPalette);
    }

/*----------------------------------------------------------------------+
|									|
| name		dialogDemo_dialogHook					|
|									|
| author	BSI				    10/90		|
|									|
+----------------------------------------------------------------------*/
Private void dialogDemo_dialogHook
(
DialogMessage	*dmP
)
    {
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	/* Handle the create message and say we are interested in:  */
	/*  1) Item draw messages (Updates)			    */
	/*  2) Mouse events					    */
	/*  3) Keystroke events					    */
	/*  4) Dialog box focus changes				    */
	/*  5) Dialig item focus changes			    */
	/*  6) Dialog box resize events				    */
	case DIALOG_MESSAGE_CREATE:
	    dmP->u.create.interests.updates	    = TRUE;
	    dmP->u.create.interests.mouses	    = TRUE;
	    dmP->u.create.interests.keystrokes	    = TRUE;
	    dmP->u.create.interests.dialogFocuses   = TRUE;
	    dmP->u.create.interests.itemFocuses     = TRUE;
	    dmP->u.create.interests.resizes	    = TRUE;
	    break;

	case DIALOG_MESSAGE_INIT:

	case DIALOG_MESSAGE_DESTROY:

	case DIALOG_MESSAGE_UPDATE:

	case DIALOG_MESSAGE_BUTTON:

	case DIALOG_MESSAGE_KEYSTROKE:

	case DIALOG_MESSAGE_FOCUSIN:

	case DIALOG_MESSAGE_FOCUSOUT:

	case DIALOG_MESSAGE_ITEMFOCUSIN:

	case DIALOG_MESSAGE_ITEMFOCUSOUT:

	case DIALOG_MESSAGE_ACTIONBUTTON:

	case DIALOG_MESSAGE_USER:

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		dialogDemo_listBoxHook					|
|									|
| author	BSI 					10/90		|
|									|
+----------------------------------------------------------------------*/
Private void dialogDemo_listBoxHook
(
DialogItemMessage	*dimP
)
    {
    RawItemHdr	*listBoxP = dimP->dialogItemP->rawItemP;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    StringList	*stringListP;
	    int		 iMember;
	    char	 formatStr[64];

	    /* This list box will have 8 rows and 2 columns, so it needs
	       16 cells created in the string list. */
	    stringListP = mdlStringList_create (STRINGLIST_NMEMBERS, 1);
	    mdlDialog_listBoxSetStrListP (listBoxP, stringListP, 1);

	    mdlResource_loadFromStringList (formatStr, NULL,
					    STRINGID_Messages, MSGID_ListBox);

	    /* Place dummy data in the string list */
	    for (iMember=0; iMember<STRINGLIST_NMEMBERS; iMember++)
		{
		char	buffer[80];
		int	line, column;

		line   = iMember / 2;
		column = iMember % 2;

		sprintf (buffer, formatStr, line, column);
		mdlStringList_setMember (stringListP, iMember, buffer, NULL);
		}
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    StringList	*strListP = mdlDialog_listBoxGetStrListP(listBoxP);

	    /* free the string list if it is allocated */
	    if (NULL != strListP)
		{
		mdlStringList_destroy (strListP);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }
/*----------------------------------------------------------------------+
|									|
| name		dialogDemo_pullDownMenuHook				|
|									|
| author	BSI 					10/90		|
|									|
+----------------------------------------------------------------------*/
Private void dialogDemo_pullDownMenuHook
(
DialogItemMessage	*dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_BUTTON:
	    break;

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Name	dialogDemo_scrollBarHook                                |
|                                                                       |
|   Author	BSI                                         11/92       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void dialogDemo_scrollBarHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	/* Handle state changed message and synchronize all scroll bar	*/
	/* related items if the value of the scroll bar access string	*/
	/* really did change						*/
	case DITEM_MESSAGE_STATECHANGED:
	    if (dimP->u.stateChanged.reallyChanged)
		mdlDialog_synonymsSynch (NULL, SYNONYMID_DialogDemoScroll, NULL);
	    break;

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI					10/90		|
|									|
+----------------------------------------------------------------------*/
Private DialogHookInfo uHooks[] =
    {
    {HOOKID_DialogDemo,			    (PFDialogHook )dialogDemo_dialogHook},
    {HOOKITEMID_ListBox_DialogDemo,	    (PFDialogHook)dialogDemo_listBoxHook},
    {HOOKITEMID_PullDownMenu_DialogDemo,    (PFDialogHook)dialogDemo_pullDownMenuHook},
    {HOOKITEMID_ScrollBar_DialogDemo,	    (PFDialogHook)dialogDemo_scrollBarHook},
    };

extern "C" DLLEXPORT  int MdlMain
(
int	argc,	/* => Number of arguments passed in pargv */
char   *argv[]	/* => Array of pointers to arguments */
)
    {
    RscFileHandle   rfHandle;
    SymbolSet           *setP;
    //char	    fName[MAXFILELENGTH];

    /* Open dlogdemo.ma file for access to our dialog */
    mdlResource_openFile (&rfHandle, NULL, RSC_READ);

    MdlCommandNumber    commandNumbers [] = 
        {
        {dialogDemo_command, CMD_DLOGDEMO },
        0
        };
    mdlSystem_registerCommandNumbers (commandNumbers);
    /* load our command table, from resources merged into dlogdemo.ma */
    if (mdlParse_loadCommandTable (NULL) == NULL)
	mdlOutput_rscPrintf (MSG_ERROR, NULL, STRINGID_Messages,
			     MSGID_CommandTable);

    /* Initialize the C Expression environment and publish our variables    */
    /* to the environment so the dialog box mananger can access them	    */
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, FALSE);

    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType(TYPECODE_LONG), "dlogdemo_colorNumber",
	&dlogdemo_colorNumber);

    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType(TYPECODE_LONG), "dlogdemo_scrollNumber",
	&dlogdemo_scrollNumber);

    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType(TYPECODE_LONG), "dlogdemo_optionbtnNumber1",
	&dlogdemo_optionbtnNumber1);

    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType(TYPECODE_LONG), "dlogdemo_optionbtnNumber2",
	&dlogdemo_optionbtnNumber2);

    /* Publish the dialog item hooks */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo),uHooks);

    dialogDemo_command("");
    return  0;	/*meaningless int value returned by main */
    }
