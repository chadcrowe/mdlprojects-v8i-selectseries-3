/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlogdemo/english/dlogdtxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   dlogdtxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:38:06 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Dialog Box demonstration application static text defines	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__dlogdtxtH__)
#define __dlogdtxtH__

#define	TXT_MainDialogBoxDemo			"Main Dialog Box Demo"
#define	TXT_ButtonTypes				"Button Types (groupbox)"
#define	TXT_ListBox				"ListBox"
#define	TXT_LevelMap				"LevelMap"
#define	TXT_PullDown				"~PullDown"
#define	TXT_PulldownColorPicker			"Pulldown~ColorPicker"
#define	TXT_PulldownOption			"Pulldown~Option"
#define	TXT_TextSubMenu				"Text~SubMenu"
#define	TXT_PulldownText			"Pulldown~Text"
#define	TXT_PullDown2				"PullDown~2"
#define	TXT_PullText21				"PullText2-1"
#define	TXT_PullText22				"PullText2-2"
#define	TXT_PullDown_Option			"PullDown_Option"
#define	TXT_PullOption1				"PullOption-1"
#define	TXT_PullOption2				"PullOption-2"
#define	TXT_PullOption3				"PullOption-3"
#define	TXT_Pulldown_Colorpicker		"Pulldown_Colorpicker"
#define	TXT_OptionButton1			"OptionButton1:"
#define	TXT_OptionButton2			"OptionButton2:"
#define	TXT_Option11				"Option1-1"
#define	TXT_Option12				"Option1-2"
#define	TXT_Option13				"Option1-3"
#define	TXT_Option21				"Option2-1"
#define	TXT_Option22				"Option2-2"
#define	TXT_PBut1				"Pbut1"
#define	TXT_PBut2				"Pbut2"
#define	TXT_Text				"Text:"
#define	TXT_Color				"Color #:"
#define	TXT_ScrollbarValue			"Scrollbar Value:"
#define TXT_ToggleButton1			"ToggleButton1"
#define TXT_ToggleButton2			"ToggleButton2"
#define TXT_ToggleButton3			"ToggleButton3"
#define	TXT_Frame				"Frame"
#define	TXT_Palette1				"Palette 1"
#define	TXT_Palette2				"Palette 2"

/* Balloon and flyover help text */
#define TXT_Flyover_DialogDemo1		    	"Dialog demo number 1"
#define TXT_Balloon_DialogDemo1		    	"Dialog Demo 1"
#define TXT_Flyover_DialogDemoPal1Sub1	    	"Dialog demo palette 1 sub-item 1"
#define TXT_Balloon_DialogDemoPal1Sub1	    	"Dialog Demo Item1-1"
#define TXT_Flyover_DialogDemoPal1Sub2	    	"Dialog demo palette 1 sub-item 2"
#define TXT_Balloon_DialogDemoPal1Sub2	    	"Dialog Demo Item1-2"
#define TXT_Flyover_DialogDemo2		    	"Dialog demo number 2"
#define TXT_Balloon_DialogDemo2		    	"Dialog Demo 2"
#define TXT_Flyover_DialogDemoPal2Sub1	    	"Dialog demo palette 2 sub-item 1"
#define TXT_Balloon_DialogDemoPal2Sub1	    	"Dialog Demo Item2-1"
#define TXT_Flyover_DialogDemoPal2Sub2	    	"Dialog demo palette 2 sub-item 2"
#define TXT_Balloon_DialogDemoPal2Sub2	    	"Dialog Demo Item2-2"
#define TXT_Flyover_DialogDemoPal2Sub3	    	"Dialog demo palette 2 sub-item 3"
#define TXT_Balloon_DialogDemoPal2Sub3	    	"Dialog Demo Item2-3"
#define TXT_Flyover_DialogDemoPal2Sub4	    	"Dialog demo palette 2 sub-item 4"
#define TXT_Balloon_DialogDemoPal2Sub4	    	"Dialog Demo Item2-4"
#define TXT_Flyover_DialogDemoPal2Sub5	    	"Dialog demo palette 2 sub-item 5"
#define TXT_Balloon_DialogDemoPal2Sub5	    	"Dialog Demo Item2-5"
#define TXT_Flyover_DialogDemoPal2Sub6	    	"Dialog demo palette 2 sub-item 6"
#define TXT_Balloon_DialogDemoPal2Sub6	    	"Dialog Demo Item2-6"
#define TXT_Flyover_DialogDemo3		    	"Dialog demo number 3"
#define TXT_Balloon_DialogDemo3		    	"Dialog Demo 3"
#define TXT_Flyover_DialogDemo4		    	"Dialog demo number 4"
#define TXT_Balloon_DialogDemo4		    	"Dialog Demo 4"

#endif /* if !defined (__dlogdtxtH__) */