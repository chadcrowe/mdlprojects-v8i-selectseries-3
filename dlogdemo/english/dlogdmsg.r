/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlogdemo/english/dlogdmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlogdemo/english/dlogdmsg.r_v  $
|   $Workfile:   dlogdmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:38:05 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Dialog Box demonstration application messages			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogdids.h>

MessageList STRINGID_Messages =
{
    {
    { MSGID_CommandTable,	"Unable to load command table" },
    { MSGID_ListBox,		"Listbox line %d, column %d" },
    }
};
