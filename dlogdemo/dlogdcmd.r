/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlogdemo/dlogdcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlogdemo/dlogdcmd.r_v  $
|   $Workfile:   dlogdcmd.r  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:37:52 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Dialog Box Demonstration Application command table		|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_DLOGDEMO       1

DllMdlApp DLLAPP_DLOGDEMO =
    {
    "DLOGDEMO", "dlogdemo"          // taskid, dllName
    }



/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define   CT_NONE    	    	      0
#define   CT_DLOGDEMO		      1

/*----------------------------------------------------------------------+
|									|
|   Main Commands						|
|									|
+----------------------------------------------------------------------*/
Table	CT_DLOGDEMO =
{ 
    { 1, CT_NONE,	    VIEWING,		NONE,	"DLOGDEMO" }, 
    { 2, CT_NONE,	    VIEWING,		NONE,	"DLOGPAL" }, 
}; 
