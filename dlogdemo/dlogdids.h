/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlogdemo/dlogdids.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   dlogdids.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:38:01 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Dlogdids.h -- Dialog Box Demonstration Resource Id's		|
|									|
+----------------------------------------------------------------------*/
#if !defined (__dlogdidsH__)
#define __dlogdidsH__

/*----------------------------------------------------------------------+
|									|
|   Dialog Box IDs							|
|									|
+----------------------------------------------------------------------*/
#define DIALOGID_DialogDemo		1
#define DIALOGID_DialogDemoPalette	2
#define TOOLBOXID_DialogDemo1       	3
#define TOOLBOXID_DialogDemo2       	4
#define BASEID_DialogDemo		(DIALOGID_DialogDemo * 100)
/*----------------------------------------------------------------------+
|									|
|   Dialog Item IDs							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Option Button IDs							|
|									|
+----------------------------------------------------------------------*/
#define OPTIONBUTTONID_DialogDemo1	1
#define OPTIONBUTTONID_DialogDemo2	2

/*----------------------------------------------------------------------+
|									|
|   Toggle Button IDs							|
|									|
+----------------------------------------------------------------------*/
#define TOGGLEID_DialogDemo1		1
#define TOGGLEID_DialogDemo2		2
#define TOGGLEID_DialogDemo3		3

/*----------------------------------------------------------------------+
|									|
|   Push Button IDs							|
|									|
+----------------------------------------------------------------------*/
#define PUSHBUTTONID_DialogDemo1	1
#define PUSHBUTTONID_DialogDemo2	2

/*----------------------------------------------------------------------+
|									|
|   ListBox Item ID							|
|									|
+----------------------------------------------------------------------*/
#define LISTBOXID_DialogDemo		1

/*----------------------------------------------------------------------+
|									|
|   Color Picker ID							|
|									|
+----------------------------------------------------------------------*/
#define COLORPICKERID_DialogDemo	1

/*----------------------------------------------------------------------+
|									|
|   Text IDs								|
|									|
+----------------------------------------------------------------------*/
#define TEXTID_DialogDemo		1
#define TEXTID_DialogDemoColor		2
#define TEXTID_DialogDemoScroll		3

/*----------------------------------------------------------------------+
|									|
|   Menubar IDs								|
|									|
+----------------------------------------------------------------------*/
#define MENUBARID_DialogDemo		1

/*----------------------------------------------------------------------+
|									|
|   Pulldown Menu IDs							|
|									|
+----------------------------------------------------------------------*/
#define PULLDOWNMENUID_DialogDemo1	    1
#define PULLDOWNMENUID_DialogDemo2	    2

#define PULLDOWNOPTIONMENUID_DialogDemo	    1
#define PULLDOWNCPICKERMENUID_DialogDemo    1

/*----------------------------------------------------------------------+
|									|
|   Scroll Bar ID							|
|									|
+----------------------------------------------------------------------*/
#define SCROLLBARID_DialogDemo		1

/*----------------------------------------------------------------------+
|									|
|   Level Map ID							|
|									|
+----------------------------------------------------------------------*/
#define LEVELMAPID_DialogDemo		1

/*----------------------------------------------------------------------+
|									|
|   Hook IDs								|
|									|
+----------------------------------------------------------------------*/
#define HOOKID_DialogDemo		    1

#define HOOKITEMID_ListBox_DialogDemo	    (BASEID_DialogDemo+1)
#define HOOKITEMID_PullDownMenu_DialogDemo  (BASEID_DialogDemo+2)
#define HOOKITEMID_ScrollBar_DialogDemo	    (BASEID_DialogDemo+3)

/*----------------------------------------------------------------------+
|									|
|   Synonym Id's							|
|									|
+----------------------------------------------------------------------*/
#define SYNONYMID_DialogDemoColor	1
#define SYNONYMID_DialogDemoScroll	2

/*----------------------------------------------------------------------+
|									|
|   Icon Command Frame Id						|
|									|
+----------------------------------------------------------------------*/
#define ICONCMDFRAMEID_DialogDemo		1

/*----------------------------------------------------------------------+
|									|
|   Icon Command Palette Id's						|
|									|
+----------------------------------------------------------------------*/
#define ICONCMDPALETTEID_DialogDemo1	1
#define ICONCMDPALETTEID_DialogDemo2	2

/*----------------------------------------------------------------------+
|									|
|   Icon Command Id's							|
|									|
+----------------------------------------------------------------------*/
#define ICONCMDID_DialogDemo1		1
#define ICONCMDID_DialogDemo2		2
#define ICONCMDID_DialogDemo3		3
#define ICONCMDID_DialogDemo4		4

#define ICONCMDID_DialogDemoPal1Sub1	11
#define ICONCMDID_DialogDemoPal1Sub2	12

#define ICONCMDID_DialogDemoPal2Sub1	21
#define ICONCMDID_DialogDemoPal2Sub2	22
#define ICONCMDID_DialogDemoPal2Sub3	23
#define ICONCMDID_DialogDemoPal2Sub4	24
#define ICONCMDID_DialogDemoPal2Sub5	25
#define ICONCMDID_DialogDemoPal2Sub6	26

/*----------------------------------------------------------------------+
|									|
|   Message list defines						|
|									|
+----------------------------------------------------------------------*/
#define	STRINGID_Messages		1

#define	MSGID_CommandTable		1
#define	MSGID_ListBox			2

#endif
