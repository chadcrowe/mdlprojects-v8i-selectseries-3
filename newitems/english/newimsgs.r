/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/newitems/english/newimsgs.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   newimsgs.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:49 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	New dialog items message resources				|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>

#include "newitems.h"

MessageList MESSAGELISTID_NewItems =
    {
      {
      { MSGID_CommandTable,	    "Unable to load command table."},
      { MSGID_DialogBox,	    "Unable to open New Items dialog box."},
      {	MSGID_NewItems,		    "~New Items"},
      { MSGID_VerySlow,		    "Very Slow" },
      { MSGID_Slow,		    "Slow" },
      { MSGID_Normal,		    "Normal" },
      { MSGID_Fast,		    "Fast" },
      { MSGID_VeryFast,		    "Very Fast" },
      { MSGID_Line,		    "Line %d" },
      { MSGID_Right,    	    "Right %d" },
      { MSGID_Left,     	    "%c Left %d" },
      { MSGID_LeftAfter,	    "%c Left %d After" },
      { MSGID_LeftBefore,	    "%c Left %d Before" },
      { MSGID_Critical,		    "Informs user of a serious system-related or application-related problem that must be corrected before work can continue with the application." },
      { MSGID_Warning,		    "Alerts user to an error condition or situation that requires user decision and input before proceeding, such as an impending action with potentially destructive, irreversible consequences." },
      { MSGID_Information,	    "Provides information about results of commands. Offers no choices; user acknowledges by clicking OK button." },
      { MSGID_Question,		    "Alerts user to an error condition or situation that requires user decision and input before proceeding, such as an impending action with potentially destructive, irreversible consequences. Takes the form of a question (for example, Save changes to floor.dgn?)."},
      { MSGID_RadioBtnChoice,	    "Radio button %d chosen" },
      }
    };
