/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/newitems/english/newitext.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/newitems/english/newitext.h_v  $
|   $Workfile:   newitext.h  $
|   $Revision: 1.4.66.1 $
|   	$Date: 2013/07/01 20:40:49 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Static text defines for the dialog box resource labels		|
|									|
+----------------------------------------------------------------------*/
#if !defined (__newitextH__)
#define __newitextH__

#define TXT_LeftJustifiedBtnLabel	"~Left\nJustified"
#define TXT_RightJustifiedBtnLabel	"~Right\nJustified"
#define TXT_TwoTagBtnLabel		"~Two Tag\nButton"
#define TXT_ThreeTagBtnLabel		"T~hree\nTag\nButton"
#define	TXT_RadioButtons		"RadioButtons"
#define	TXT_RadioButtons_A		"~RadioButtons"
#define	TXT_RadioButtons1		"~RadioButtons 1"
#define	TXT_RadioButtons1a		"RadioButtons 1"
#define	TXT_RadioButtons2		"Radio~Buttons 2"
#define	TXT_ModalDialogBox		"Modal Dialog Box"
#define	TXT_HorizontalScales		"Horizontal Scales"
#define	TXT_HorizontalScales_A		"~Horizontal Scales"
#define	TXT_VerticalScales		"Vertical Scales"
#define	TXT_VerticalScales_A		"~Vertical Scales"
#define	TXT_Range1			"Range~1"
#define	TXT_Range1_A			"Range~1:"
#define	TXT_Range2			"Range~2"
#define	TXT_Range2_A			"Range~2:"
#define	TXT_Range3			"Range~3"
#define	TXT_Range3_A			"Range~3:"
#define	TXT_Speed			"~Speed"
#define	TXT_Speed_A			"~Speed:"
#define	TXT_Sash			"Sash"
#define	TXT_Sash_A			"~Sash"
#define	TXT_VSash			"Vertical Sash"
#define	TXT_VSash_A			"~Vertical Sash"
#define	TXT_ButtonGroup			"ButtonGroup"
#define	TXT_ButtonGroup_A		"~Button Group"
#define	TXT_ColoredItems		"Colored Items"
#define	TXT_ColoredItems_A		"~Colored Items"
#define	TXT_2DArrows			"2D Arrows"
#define	TXT_2DArrows_A			"~2D Arrows"
#define	TXT_CurrentSelection		"~Current Selection:"
#define	TXT_ExampleList			"~Example List:"
#define	TXT_Row				"Row"
#define	TXT_Col				"Col"
#define	TXT_Examples			"E~xamples"
#define	TXT_MultiLinePushButtons	"Multi-Line ~PushButtons"
#define	TXT_MultiSelectionList		"Multi-Selection ~List"
#define	TXT_Exit			"E~xit"
#define	TXT_Tests			"~Tests"
#define	TXT_Modify			"~Modify"
#define	TXT_Select			"~Select"
#define	TXT_Position			"~Position"
#define	TXT_Location			"~Location"
#define	TXT_ResetList			"~Reset List"
#define	TXT_RecreateList		"Re~create List"
#define	TXT_AddItemAtTop		"Add Item At ~Top"
#define	TXT_AddItemAtBottom		"Add Item At ~Bottom"
#define	TXT_AddBeforeSelection		"Add Before ~Selection"
#define	TXT_DeleteSelection		"~Delete Selection"
#define	TXT_ToggleAddMode		"Toggle ~Add Mode"
#define	TXT_SelectTopItem		"Select ~Top Item"
#define	TXT_SelectBottomItem		"Select ~Bottom Item"
#define	TXT_SelectEveryOther		"Select ~Every Other"
#define	TXT_SelectAll			"~Select All"
#define	TXT_DeselectTopItem		"Deselect Top ~Item"
#define	TXT_DeselectBottomItem		"Dese~lect Bottom Item"
#define	TXT_DeselectAll			"~Deselect All"
#define	TXT_Make1stTop			"Make ~1st Top"
#define	TXT_MakeLastBottom		"Make ~Last Bottom"
#define	TXT_Make1stSelectionTop		"Make 1st Selection ~Top"
#define	TXT_MoveToTopItem		"Move To ~Top Item"
#define	TXT_MoveToBottomItem		"Move To ~Bottom Item"
#define	TXT_MoveToBeginningofSelection	"Move To Beginning of ~Selection"
#define	TXT_Parameter1			"~Parameter 1:"
#define	TXT_Value1			"Value ~1"
#define	TXT_Value2			"Value ~2"
#define	TXT_Value3			"Value ~3"
#define	TXT_BGroupItem			"~BGroup Item:"
#define	TXT_Row0Col0			"Row 0, Col 0"
#define	TXT_Row0Col1			"Row 0, Col 1"
#define	TXT_Row1Col0			"Row 1, Col 0"
#define	TXT_Row1Col1			"Row 1, Col 1"
#define	TXT_Row2Col0			"Row 2, Col 0"
#define	TXT_Row2Col1			"Row 2, Col 1"
#define	TXT_Row3Col0			"Row 3, Col 0"
#define	TXT_Row3Col1			"Row 3, Col 1"
#define	TXT_SelectionMode		"~Selection Mode:"
#define	TXT_Browse			"~Browse"
#define	TXT_Single			"~Single"
#define	TXT_Multiple			"~Multiple"
#define	TXT_Extended			"~Extended"
#define	TXT_DefaultPushButton		"~Default PushButton:"
#define	TXT_None			"~None"
#define	TXT_OpenModal			"Open ~Modal"
#define	TXT_SampleTag			"~Sample\nTag"
#define	TXT_Example			"~Example\n "
#define	TXT_Example1			"E~xample\n"
#define	TXT_Example2			" \n \nExa~mple"
#define	TXT_SampleTag1			"Sample\nT~ag\n"
#define	TXT_Range4			"Range~4:"
#define	TXT_String			"~String:"
#define	TXT_ColoredToggleLabel		"ListBox & Text Background Is ~Colored"

/* Radio Button Item Labels */
#define	TXT_RadioButton1		"Option 1-~1"
#define	TXT_RadioButton2		"Option 1-~2"
#define	TXT_RadioButton3		"Option 1-~3"
#define	TXT_RadioButton4		"~Option 2-1"
#define	TXT_RadioButton5		"Op~tion 2-2"
#define	TXT_RadioButton6		"Optio~n 2-3"

/* Alert Boxes Dialog and Items */
#define	TXT_DialogAlert			"Alert Boxes"
#define	TXT_CriticalPushbtnLabel	"~Critical"
#define	TXT_InformationPushbtnLabel 	"~Information"
#define	TXT_QuestionPushbtnLabel   	"~Question"
#define	TXT_WarningPushbtnLabel  	"~Warning"
#define	TXT_AlertBoxMenuLabel		"~Alert Boxes"

/* TabPage Testing */
#define	TXT_TabsBoxMenuLabel		"~Tabs"
#define	TXT_DialogTabs			"Tabs Testing"
#define	TXT_TabPageRadio		"Radio Buttons"
#define	TXT_TabPageOptions		"Tab Options"
#define	TXT_TabPageScales		"Scales"
#define	TXT_TabsTop     		"~Top"
#define	TXT_TabsBottom     		"~Bottom"
#define	TXT_TabsLeft     		"~Left"
#define	TXT_TabsRight     		"~Right"
#define	TXT_TabPlacement     		"Tab Placement"
#define TXT_TabPageToggle		"~Show New Tab Page"
#define TXT_ComboSpinBoxes		"Combo/Spin"
#define	TXT_TestPopupPushbtnLabel  	"Test Popup"
#define	TXT_FreeTabPagePush		"Free 1st Tab"

#define TXT_NewitemsElementAttributes                   "Element Attributes"
#define TXT_NewitemsElementAttributesLevel              "Level"
#define TXT_NewitemsElementAttributesColor              "Color"
#define TXT_NewitemsElementAttributesStyle              "Style"
#define TXT_NewitemsElementAttributesWeight             "Weight"
#define TXT_NewitemsElementAttributesGroupBoxWithHook   "Dialog items with hooks"
#define TXT_NewitemsElementAttributesGroupBox           "Dialog items without hooks"

#endif /* #if !defined (__newitextH__) */
