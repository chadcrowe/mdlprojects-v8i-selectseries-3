/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/newitems/newitems.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: newitems.r,v $
|   $Revision: 1.10.66.1 $
|   	$Date: 2013/07/01 20:40:48 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Newitems Dialog Example Resources				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */
#include <keys.h>
#include <cmdlist.h>
#include <leveltable.h>

#include "newitems.h"	/* newitems dialog box example constants & structs */
#include "newicmd.h"	/* newitems dialog box command numbers */
#include "newitext.h"	/* newitems static text definitions */

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
#define YBUT		YSEP+YC
#define	YSIZE		(YBUT+2*YC+YC)

/*----------------------------------------------------------------------+
|									|
|   RadioButtons Dialog Box						|
|									|
+----------------------------------------------------------------------*/
#define XSIZE	(38*XC)
#define GBW (15*XC)
#define XW  (11*XC)

#define X1 (14*XC)
#define X2 (3*XC)
#define X3 (X2 + 1.5*XC)
#define X4 (X2 + GBW + 2*XC)
#define X5 (X4 + 1.5*XC)

#define BTN_WIDTH   (13*XC)	/* push button width */
#define B1X	    ((XSIZE-BTN_WIDTH)/2)
#define YSEP	    (GENY(4)-YC + 5*YC + 0.75*YC)

DialogBoxRsc DIALOGID_RadioButtons =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    38*XC, YSIZE, 
    NOHELP, MHELP, HOOKID_Dialog_Dummy, NOPARENTID,
    TXT_RadioButtons,
{
{{X1,GENY(1.5),XW,0}, OptionButton, OPTIONBTNID_NewItem, ON, 0, "", ""},

{{X2, GENY(4)-YC,GBW,5*YC}, GroupBox, 0, ON, 0, TXT_RadioButtons1, ""},

{{X3,GENY(4),0,0}, RadioButtonListX, RBLISTXID_ONE, ON, 0, "", ""},
/*
{{X3,GENY(4),0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
{{X3,GENY(5),0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
{{X3,GENY(6),0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},
*/

{{X4, GENY(4)-YC,GBW,5*YC}, GroupBox, 0, ON, 0, TXT_RadioButtons2, ""},

{{X5,GENY(4),0,0}, RadioButton,  RBUTTONID_Option4, ON, 0, "", ""},
{{X5,GENY(5),0,0}, RadioButton,  RBUTTONID_Option5, ON, 0, "", ""},
{{X5,GENY(6),0,0}, RadioButton,  RBUTTONID_Option6, ON, 0, "", ""},

{{B1X,YBUT,BTN_WIDTH,0}, PushButton, PUSHBUTTONID_OModalRadioButtons, ON, 0,"",""},
}
    };


DItem_RadioButtonListXRsc  RBLISTXID_ONE =
    {
    SYNONYMID_OptionButton, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG,
    0,
    "",
{
{{X3,GENY(4),0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
{{X3,GENY(5),0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
{{X3,GENY(6),0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},
}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef GBW
#undef XW

#undef X1
#undef X2
#undef X3
#undef X4
#undef X5

#undef BTN_WIDTH
#undef B1X
#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Modal Dialog Box w/ RadioButtons					|
|									|
+----------------------------------------------------------------------*/
#define XSIZE (30*XC)
#define GBW (15*XC)

#define X1 (8*XC)
#define X2 (X1 + 1.5*XC)

#define XBORDER ((XSIZE - 2*BUTTON_STDWIDTH)/3)
#define B1X XBORDER
#define B2X (XSIZE - BUTTON_STDWIDTH - XBORDER)

#define YSEP (GENY(2)-YC + 5*YC + 0.75*YC)

DialogBoxRsc DIALOGID_ModalRadioButtons =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_MODAL,
    XSIZE, YSIZE, 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_ModalDialogBox,
{
{{X1, GENY(2)-YC,GBW,5*YC}, GroupBox, 0, ON, 0, TXT_RadioButtons1a, ""},

{{X2,GENY(2),0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
{{X2,GENY(3),0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
{{X2,GENY(4),0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},

{{ 0.00*XC, YSEP, 0, 0},       Separator, 0, ON,0,"",""},
{{B1X,YBUT,BUTTON_STDWIDTH,0}, PushButton,  PUSHBUTTONID_OK, ON, 0, "", ""},
{{B2X,YBUT,BUTTON_STDWIDTH,0}, PushButton,  PUSHBUTTONID_Cancel, ON, 0, "", ""}, 
}
    };

#undef XSIZE
#undef GBW

#undef X1		/* undef symbols so they can be reused */
#undef X2

#undef XBORDER
#undef B1X 
#undef B2X

#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Horizontal Scales Dialog Box					|

    The recommended layout for horizontal scale items with keyin fields
    is either of the following examples for the range1 & range2 
    (w/ arrows) scales.  The min & max limits should be displayed.
    The Y distance between such scale/text items
    should be GENY(1.5) = 1.5 * (YC+GAP) = 1.5 * (the standard distance
    between items). The X distance between the text and scale item
    should be 2*XC + (# of chars in min label)*XC. The text label
    should end with a colon.

    The recommended layout for scale items without keyin fields is shown
    by the scale item for range3.  The min & max limits and the current
    value should be displayed.  The Y distance between such scale
    items should be GENY(2.25) = 2.25 * (YC+GAP).

|									|
+----------------------------------------------------------------------*/
#define XSIZE	(40*XC)
#define SW	(14*XC)
#define TW	(5*XC)	    /* width of text items */

#define X1  (10*XC)
#define X2  (X1+TW + 3*XC)

#define BTN_WIDTH (12*XC)	/* push button width */
#define B1X	((XSIZE-BTN_WIDTH)/2)
#define YSEP	(GENY(10.5) + 1.5*YC)

DialogBoxRsc DIALOGID_HScales =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_HorizontalScales,
{
{{X1,GENY(2),   TW,0},	Text,	TEXTID_Range1, ON, 0, "", ""},
{{X1,GENY(3.50),TW,0},Text,	TEXTID_Range2, ON, 0, "", ""},

{{X2,GENY(2),  SW,0},	Scale,  SCALEID_Range1, ON, 0, "", ""},
{{X2,GENY(3.50),SW,0},	Scale,  SCALEID_Range2, ON, 0, "", ""},

{{X2,GENY(6),SW,0},	Scale,  SCALEID_Range3, ON, 0, "", ""},
{{X2,GENY(8.25),20*XC,0},	Scale,  SCALEID_Speed,	     ON, 0, "", ""},
{{X2,GENY(10.50),SW,0},	Scale,  SCALEID_TiedToSpeed, OFF, 0, "", ""},

{{B1X,YBUT,BTN_WIDTH,0}, PushButton, PUSHBUTTONID_OModalScales, ON, 0,"",""},
}
    };

#undef XSIZE    
#undef SW
#undef TW

#undef X1		/* undef symbols so they can be reused */
#undef X2

#undef BTN_WIDTH
#undef B1X
#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Vertical Scales Dialog Box						|

    The recommended layout for vertical scale items with keyin fields
    is either of the following examples for the range1 & range2 
    (w/ arrows) scales. The min & max limits should be displayed. 
    The Y distance between the text and bottom of the scale 
    item should be 1.25*YC. The text item should be centered underneath
    the scale item.  The label of the scale item should NOT end
    with a colon. The text item should NOT have a label.

    The recommended layout for vertical scale items without keyin 
    fields is shown by the scale item for range3.  The min & max 
    limits and the current value should be displayed.  

|									|
+----------------------------------------------------------------------*/
#define XSIZE	(45*XC)
#define SW	(14*XC)
#define TW	(5*XC)	    /* width of text items */

#define X1	(4*XC)
#define X2	(X1 + 7*XC)
#define X3	(X2 + 10*XC)
#define X4	(X3 + 12*XC)
#define X5	(X4 + 7*XC)

#define Y1	(GENY(3.5)+SW + 1.25*YC)

#define BTN_WIDTH (12*XC)	/* push button width */
#define B1X	((XSIZE-BTN_WIDTH)/2)
#define YSEP	(Y1 + YC + 0.75*YC)

DialogBoxRsc DIALOGID_VScales =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_VerticalScales,
{
{{X1+(YC-TW)/2,Y1,TW,0},   Text,	TEXTID_Range1, ON, 0, " ", ""},
{{X2+(YC-TW)/2,Y1,TW,0},   Text,	TEXTID_Range2, ON, 0, " ", ""},

{{X1,GENY(3.5), 0, SW},	Scale,  SCALEID_Range1, ON, 0, TXT_Range1, ""},
{{X2,GENY(3.5), 0, SW},	Scale,  SCALEID_Range2, ON, 0, TXT_Range2, ""},
{{X3,GENY(3.5), 0, SW},	Scale,  SCALEID_Range3, ON, 0, TXT_Range3, ""},

{{X4,GENY(3.5),0,20*XC},Scale,  SCALEID_Speed,	ON, 0, TXT_Speed, ""},
{{X5,GENY(3.5),0,SW},	Scale,  SCALEID_TiedToSpeed, OFF, 0, "", ""},

{{B1X,YBUT,BTN_WIDTH,0}, PushButton, PUSHBUTTONID_OModalScales, ON, 0,"",""},
}
    };

#undef XSIZE    
#undef SW
#undef TW

#undef X1
#undef X2
#undef X3
#undef X4
#undef X5

#undef Y1

#undef BTN_WIDTH
#undef B1X
#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Modal Sub-Dialog Box w/ Scales					|
|									|
+----------------------------------------------------------------------*/
#define XSIZE (30*XC)
#define SW  (14*XC)

#define X1 (11*XC)

#define XBORDER ((XSIZE - 2*BUTTON_STDWIDTH)/3)
#define B1X XBORDER
#define B2X (XSIZE - BUTTON_STDWIDTH - XBORDER)

#define YSEP (GENY(2) + YC + 0.75*YC)

DialogBoxRsc DIALOGID_ModalScales =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_MODAL,
    XSIZE, YSIZE, 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_ModalDialogBox,
{
{{X1,GENY(2),SW,0},	Scale,  SCALEID_Range3, ON, 0, "", ""},


{{ 0.00*XC, YSEP, 0, 0},       Separator, 0, ON,0,"",""},
{{B1X,YBUT,BUTTON_STDWIDTH,0}, PushButton,  PUSHBUTTONID_OK, ON, 0, "", ""},
{{B2X,YBUT,BUTTON_STDWIDTH,0}, PushButton,  PUSHBUTTONID_Cancel, ON, 0, "", ""},
}
    };

#undef XSIZE
#undef SW

#undef X1		/* undef symbols so they can be reused */

#undef XBORDER
#undef B1X 
#undef B2X

#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Multi-Line PushButtons						|
|									|
+----------------------------------------------------------------------*/
#define XSIZE	(50*XC)

#define BTN_WIDTH   (13*XC)	/* push button width */
#define XMARGIN	    ((XSIZE - 3*BTN_WIDTH)/4)
#define B1X	    (XMARGIN)
#define B2X	    (2*XMARGIN+BTN_WIDTH)
#define B3X	    (3*XMARGIN+2*BTN_WIDTH)
#define Y1	    (GENY(2))
#define Y2	    (GENY(6))
#define Y3	    (GENY(10))
#define Y4	    (GENY(15))

#define YSEP	    (Y4 - 0.5*YC)

DialogBoxRsc  DIALOGID_MLPushButtons =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    "Multi-Line PushButtons",
{
{{B1X,Y1,BTN_WIDTH,0}, PushButton,  PUSHBUTTONID_MLine11, ON, 0,"",""},
{{B2X,Y1,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine12, ON, 0,"",""},
{{B3X,Y1,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine13, ON, 0,"",""},

{{B1X,Y2,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine21, ON, 0,"",""},
{{B2X,Y2,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine22, ON, 0,"",""},
{{B3X,Y2,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine23, ON, 0,"",""},

{{B1X,Y3,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine31, ON, 0,"",""},
{{B2X,Y3,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine32, ON, 0,"",""},
{{B3X,Y3,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_MLine33, ON, 0,"",""},


{{B2X,Y4,0,0}, OptionButton, OPTIONBTNID_DefaultPushButton, ON, 0,"",""},
}
    };

#undef XSIZE		/* undef symbols so they can be reused */

#undef BTN_WIDTH
#undef B1X
#undef B2X
#undef B3X

#undef Y1
#undef Y2
#undef Y3
#undef Y4
#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Sash Example Dialog							|
|									|
+----------------------------------------------------------------------*/
#define XSIZE	(30*XC)

#define X1	    (2*XC)
#define X2	    (15*XC)
#define X3	    (16*XC)
#define Y1	    (GENY(1))
#define Y2	    (GENY(5))
#define Y3	    (GENY(6))

DialogBoxRsc  DIALOGID_Sash =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_GROWABLE | DIALOGATTR_MOTIONTOITEMS,
    XSIZE, 20*YC, 
    NOHELP, MHELP, HOOKID_Dialog_Sash, NOPARENTID,
    TXT_Sash,
{
{{X1,Y1,0,0},	ListBox,    LISTBOXID_Sash1,    ON, 0, "", ""},
{{ 0,Y2,0,0},	Sash,	    SASHID_Example,     ON, 0,"",""},
{{X1,Y3,0,0},	ListBox,    LISTBOXID_Sash2,    ON, 0, "", ""},
}
    };

DialogBoxRsc  DIALOGID_VSash =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_GROWABLE | DIALOGATTR_MOTIONTOITEMS,
    XSIZE, 20*YC, 
    NOHELP, MHELP, HOOKID_Dialog_VSash, NOPARENTID,
    TXT_VSash,
{
{{0,0,0,0},	ListBox,    LISTBOXID_Sash1,    ON, 0, "", ""},
{{X2,0,0,0},	Sash,	    SASHID_VExample,    ON, 0,"",""},
{{X3,0,0,0},	ListBox,    LISTBOXID_Sash2,    ON, 0, "", ""},
}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef X1
#undef X2
#undef X3
#undef Y1
#undef Y2
#undef Y3
#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Button Group Example Dialog						|
|									|
+----------------------------------------------------------------------*/
#define XSIZE	(30*XC)

#define X1	    (2*XC)
#define X2	    (15*XC)
#define Y1	    (GENY(2))
#define Y2	    (GENY(4))
#define YSEP	    (Y2+11*YC)

DialogBoxRsc  DIALOGID_BGroup =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_ButtonGroup,
{
{{X2,Y1,0,0}, OptionButton, OPTIONBTNID_BGroup, ON, 0,"",""},
{{X1,Y2,0,0}, ButtonGroup,  BGROUPID_EditIconTools, ON, 0,"",""},
}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef X1
#undef X2
#undef Y1
#undef Y2
#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Colored Items Example Dialog					|
|									|
+----------------------------------------------------------------------*/
#define XSIZE	(38*XC)
#define GBW (15*XC)
#define XW  (11*XC)

#define X1 (3*XC)
#define X2 (X1 + 1.5*XC)
#define X3 (10*XC)
#define X4 (X3 + 1.5*XC)

#define YSEP (GENY(12)-YC+5*YC)

DialogBoxRsc  DIALOGID_Colors =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, HOOKID_Dialog_Colors, NOPARENTID,
    TXT_ColoredItems,
{
{{X1,GENY(1.5),0,0}, ToggleButton, TOGGLEBTNID_Colored, ON, 0, "", ""},
{{X3,GENY(2.75),10*XC,0}, Text, TEXTID_Colored, ON, 0, "", ""},

{{X1,GENY(4),0,0},	ListBox,    LISTBOXID_Sash1,    ON, 0, "", ""},

{{X1, GENY(12)-YC,GBW,5*YC}, GroupBox, 0, ON, 0, TXT_RadioButtons1a, ""},

{{X2,GENY(12),0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
{{X2,GENY(13),0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
{{X2,GENY(14),0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},

}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef GBW
#undef XW

#undef X1
#undef X2
#undef X3
#undef X4
#undef X5

#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Multi-Selection ListBox Example Dialog				|
|									|
+----------------------------------------------------------------------*/
#define GBW (15*XC)
#define XW  (11*XC)

#define X1 (16*XC)
#define X2 (2*XC)
#define X3 (X2 + 22*XC + 6*XC)
#define XSIZE	(X3 + 33*XC + 3*XC + 7*XC)

#define Y1   (GENY(5) + 10*(YC+YC/6)+YC)

#define YSEP (Y1)

DialogBoxRsc  DIALOGID_MultiList =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE | DIALOGATTR_MOTIONTOITEMS,
    XSIZE, YSIZE, 
    NOHELP, MHELP, HOOKID_Dialog_MultiList, NOPARENTID,
    "Multi-Selection ListBox",
{
{{0,0,0,0},	PopupMenu,  POPUPMENUID_MultiList,  HIDDEN, 0, "", ""},
{{0,0,0,0},	MenuBar,    MENUBARID_MultiList,    ON,	    0, "", ""},

{{X1,GENY(2.5),0,0},	OptionButton, OPTIONBTNID_SelectionMode, ON, 0, "", ""},

{{X2,GENY(5),0,0},	ListBox,    LISTBOXID_Multi,		ON, 0, "", ""},
{{X3,GENY(5),40*XC,0},	ListBox,    LISTBOXID_SelectionList,	ON, 0, "", ""},

{{X2, Y1, 0, 0},	Label,	    0,				ON, 0, "", ""},
}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef GBW     
#undef XW      
#undef X1     
#undef X2     
#undef X3     
#undef Y1       
#undef YSEP 
#undef YSIZE 
    
/*----------------------------------------------------------------------+
|									|
|   2D Arrows Dialog -- Empty Dialog Drawn at update time		|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc  DIALOGID_2DArrows =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    20*XC, 20*YC, 
    NOHELP, MHELP, HOOKID_Dialog_2DArrows, NOPARENTID,
    TXT_2DArrows,
{
}
    };

/*----------------------------------------------------------------------+
|									|
|   PushButtons Dialog for AlertBox Tests			      	|
|									|
+----------------------------------------------------------------------*/
#define XSIZE	(17*XC)
#define YSIZE	GENY(11)

#define BTN_WIDTH   (13*XC)	/* push button width */
#define B1X	    (2*XC)
#define Y1	    (GENY(2))
#define Y2	    (GENY(4))
#define Y3	    (GENY(6))
#define Y4	    (GENY(8))

#define YSEP	    (Y4 - 0.5*YC)

DialogBoxRsc  DIALOGID_Alerts =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_DialogAlert,
{
{{B1X,Y1,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_Alert1, ON, 0,"",""},
{{B1X,Y2,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_Alert2, ON, 0,"",""},
{{B1X,Y3,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_Alert3, ON, 0,"",""},
{{B1X,Y4,BTN_WIDTH,0}, PushButtonX, PUSHBUTTONID_Alert4, ON, 0,"",""},
}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef YSIZE

#undef BTN_WIDTH
#undef B1X

#undef Y1
#undef Y2
#undef Y3
#undef Y4
#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   TabPage Dialog Test						      	|
|									|
+----------------------------------------------------------------------*/
#define XALT    (105)
#define XALTR   (150)
#define YALT    (GENY(4))

#define XSIZE	(60*XC + XALTR)
#define YSIZE	GENY(25)

#define SW	(14*XC)
#define TW	(5*XC)	    
#define GBW 	(15*XC)

#define B1X	(2*XC)
#define TPLW    (40*XC + XALTR)
#define X1 	(3*XC + XALT)
#define X2 	(10*XC + XALT)
#define X3  	(X2+TW + 3*XC)
#define X4  	(X1+TW*3.4)
#define XRB     (X1 + 1.5*XC)
#define XRBL    (5*XC + XALT)
#define XGRP    (3*XC + XALT)

#define Y10     (GENY(3) + YALT)
#define Y11     (GENY(4) + YALT)
#define Y12     (GENY(5) + YALT)
#define Y13     (GENY(5.5) + YALT)
#define Y14     (GENY(6) + YALT)
#define Y15     (GENY(7) + YALT)
#define Y16     (GENY(9) + YALT)
#define Y17     (GENY(11) + YALT)

DialogBoxRsc  DIALOGID_Tabs =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE | DIALOGATTR_GROWABLE,
    XSIZE+20, YSIZE+20, 
    NOHELP, MHELP, 
    HOOKID_TabsDialog, NOARG,
    TXT_DialogTabs,
{
{{B1X,GENY(1),0,0}, PushButton, PUSHBUTTONID_FreeTabPage, ON, 0, "", ""},

{{B1X,GENY(3),0/*TPLW*/,0}, TabPageList, TPLISTID_ONE, ON, 0,"",""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   Level, color, style and weight items Dialog Test                    |
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc  DIALOGID_Attributes =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    32*XC, D_ROW(13), 
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_NewitemsElementAttributes,
{
{{   XC, D_ROW(1), 30*XC, D_ROW(6)},    GroupBox,                                           0,    ON, 0, TXT_NewitemsElementAttributesGroupBoxWithHook, ""},
{{10*XC, D_ROW(2), 18*XC, 0},           ComboBox,    COMBOBOXID_NewItems_ElementLevelWithHook,    ON, 0, "", ""},
{{10*XC, D_ROW(3), 18*XC, 0},           ComboBox,    COMBOBOXID_NewItems_ElementColorWithHook,    ON, 0, "", ""},
{{10*XC, D_ROW(4), 18*XC, 0},           ComboBox,    COMBOBOXID_NewItems_ElementStyleWithHook,    ON, 0, "", ""},
{{10*XC, D_ROW(5), 18*XC, 0},           ComboBox,    COMBOBOXID_NewItems_ElementWeightWithHook,   ON, 0, "", ""},


{{   XC, D_ROW(7), 30*XC, D_ROW(6)},    GroupBox,                          0,    ON, 0, TXT_NewitemsElementAttributesGroupBox, ""},
{{10*XC, D_ROW(8), 18*XC, 0},           ComboBox,    COMBOBOXID_ElementLevel,    ON, LEVELLIST_BOLD_USED | LEVELLIST_FILTER | LEVELLIST_SHOW_LEVEL_LIBRARIES, TXT_NewitemsElementAttributesLevel, "access=\"niG.currentLevel\""},
{{10*XC, D_ROW(9), 18*XC, 0},           ComboBox,    COMBOBOXID_ElementColor,    ON, 0, TXT_NewitemsElementAttributesColor, "access=\"niG.currentColor\""},
{{10*XC, D_ROW(10), 18*XC, 0},          ComboBox,    COMBOBOXID_ElementStyle,    ON, 0, TXT_NewitemsElementAttributesStyle, "access=\"niG.currentStyle\""},
{{10*XC, D_ROW(11), 18*XC, 0},          ComboBox,    COMBOBOXID_ElementWeight,   ON, 0, TXT_NewitemsElementAttributesWeight, "access=\"niG.currentWeight\""},
}
    };

DItem_TabPageListRsc  TPLISTID_ONE =
    {
    0, 0,
    NOSYNONYM, 
    NOHELP, MHELP, HOOKID_TabPageList, NOARG,
    TABATTR_DEFAULT | TABATTR_MULTIROW /*| TABATTR_TABSLEFT | TABATTR_TABSFIXEDWIDTH*/,
    TXT_DialogTabs,
{

{{0,0,0,0}, TabPage, TABPAGEID_OPTIONS, ON, 0,"",""},
{{0,0,0,0}, TabPage, TABPAGEID_BOXES,   ON, 0,"",""},
{{0,0,0,0}, TabPage, TABPAGEID_SCALES,  ON, 0,"",""},
{{0,0,0,0}, TabPage, TABPAGEID_BUTTONGRP,  ON, 0,"",""},

}
    };

DItem_TabPageRsc  TABPAGEID_OPTIONS =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPage, NOARG,
    TABATTR_DEFAULT,
    Icon, ICONID_TabOptions,
    TXT_TabPageOptions,
{
{{XRBL,Y11,0,0}, RadioButtonListX, RBLISTXID_TABPLACEMENT, ON, 0, "", ""},
{{XGRP,Y11-YC,GBW,7*YC}, GroupBox, 0, ON, 0, TXT_TabPlacement, ""},
/*({{XRB,Y11,0,0}, RadioButton,  RBUTTONID_TabsTop, ON, 0, "", ""},
{{XRB,Y12,0,0}, RadioButton,  RBUTTONID_TabsBottom, ON, 0, "", ""},
{{XRB,Y14,0,0}, RadioButton,  RBUTTONID_TabsLeft, ON, 0, "", ""},
{{XRB,Y15,0,0}, RadioButton,  RBUTTONID_TabsRight, ON, 0, "", ""},*/

{{X1,Y16,0,0}, ToggleButton, TOGGLEBTNID_AddTabPage, ON, 0, "", ""},
}
    };

DItem_TabPageRsc  TABPAGEID_BOXES =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKID_TabPage, NOARG,
    TABATTR_DEFAULT,
    Icon, ICONID_ComboBox,
    TXT_ComboSpinBoxes,
{
{{X1,Y11,TW*2.5,1.4*YC}, SpinBox, SPINBOXID_Test1, ON, 0, "", ""},
{{X1,Y15,TW*2.7,1.4*YC}, ComboBox, COMBOBOXID_Test1, ON, 0, "", ""},
{{X1,Y16,TW*2.4,1.4*YC}, ComboBox, COMBOBOXID_Test2, ON, 0, "", ""},
{{X1,Y17,TW*3,1.4*YC}, ComboBox, COMBOBOXID_Test3, ON, 0, "", ""},

}
    };

DItem_TabPageRsc  TABPAGEID_RADIOS =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK/*ID_TabPage*/, NOARG,
    TABATTR_DEFAULT,
    Icon, ICONID_RadioBtnOptions,
    TXT_TabPageRadio,
{

{{XRBL,Y11,0,0}, RadioButtonListX, RBLISTXID_TWO, ON, 0, "", ""},
{{XGRP,Y11-YC,GBW,5*YC}, GroupBox, 0, ON, 0, TXT_RadioButtons1, ""},
/*
{{XRB,Y11,0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
{{XRB,Y12,0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
{{XRB,Y14,0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},
*/
}
    };

DItem_TabPageRsc  TABPAGEID_SCALES =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPage, NOARG,
    TABATTR_DEFAULT,
    Icon, ICONID_ScaleOptions,
    TXT_TabPageScales,
{
{{X2,Y11,   TW,0},	Text,	TEXTID_Range1, ON, 0, "", ""},
{{X2,Y13,TW,0},  Text,	TEXTID_Range2, ON, 0, "", ""},

{{X3,Y11,  SW,0},	Scale,  SCALEID_Range1, ON, 0, "", ""},
{{X3,Y13,SW,0},	Scale,  SCALEID_Range2, ON, 0, "", ""},
}
    };

DialogItemListRsc DITEMLISTID_newPage =
    {
    	{
{{B1X,GENY(1),0,0}, TabPage, TABPAGEID_RADIOS, ON, 0,"",""},
	}
};

DItem_RadioButtonListXRsc  RBLISTXID_TABPLACEMENT =
    {
    NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPageRadioBtns, NOARG,
    0,
    "",
{
{{XRB,Y11,0,0}, RadioButton,  RBUTTONID_TabsTop, ON, 0, "", ""},
{{XRB,Y12,0,0}, RadioButton,  RBUTTONID_TabsBottom, ON, 0, "", ""},
{{XRB,Y14,0,0}, RadioButton,  RBUTTONID_TabsLeft, ON, 0, "", ""},
{{XRB,Y15,0,0}, RadioButton,  RBUTTONID_TabsRight, ON, 0, "", ""},
}
    };

DItem_RadioButtonListXRsc  RBLISTXID_TWO =
    {
    SYNONYMID_OptionButton, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG,
    0,
    "",
{
{{XRB,Y11,0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
{{XRB,Y12,0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
{{XRB,Y14,0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},
}
    };

DItem_TabPageRsc  TABPAGEID_BUTTONGRP =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPage, NOARG,
    TABATTR_DEFAULT,
    NOTYPE, NOICON,
    TXT_ButtonGroup,
{
{{X1,Y11,0,0}, ButtonGroup,  BGROUPID_Colors, ON, 0,"",""},
}
    };

DItem_SpinBoxRsc SPINBOXID_Test1 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_SpinBox, NOARG,
    8, "%.1lf in", "%lf",  
    1.0, 100.0, 0.5,
    NOMASK, SPINATTR_LABELABOVE, 
    "~Spin Box Label:",
    "niG.spinBoxValue"
    };

/*----------------------------------------------------------------------+
|									|
|   Combo box items                                                     |
|									|
+----------------------------------------------------------------------*/
DItem_ComboBoxRsc COMBOBOXID_Test1 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    10, "", "", "", "", NOMASK,
    STRLISTID_ComboBox1, 20, 5, 0, 0, 
    COMBOATTR_DRAWPREFIXICON | COMBOATTR_READONLY | COMBOATTR_SORT | COMBOATTR_LABELABOVE, 
    "~ComboBox 1:",
    "",
	{
	{TW*3, 10, 0, ""},
	}
    };

DItem_ComboBoxRsc COMBOBOXID_Test2 =
    {
    NOCMD, LCMD,
    SYNONYMID_RadioButtons1, NOHELP, MHELP, 
    HOOKID_ComboStates, NOARG,
    2, "", "", "", "", NOMASK,
    STRLISTID_ComboBoxStates, 15, 0, TW*4, 0, 
    COMBOATTR_READONLY | COMBOATTR_INDEXISVALUE, 
    "S~tates:",
    "niG.stateIndex",
	{
	{TW, 2, 0, "Code"},
	{0, 15, 0, "State Name"},
	}
    };

DItem_ComboBoxRsc COMBOBOXID_Test3 =
    {
    CMD_NEWITEMS_MODALOPEN_RADIOBUTTONS, LCMD, 
    NOSYNONYM, NOHELP, MHELP, 
    HOOKID_ComboBoxes, NOARG,
    12, "%.0lf", "%lf", "0.0", "100.0", NOMASK,
    0, 6, 4, 0, 0, 
    COMBOATTR_AUTOADDNEWSTRINGS, 
    "#~3:",
    "",
	{
	{0, 12, 0, ""},
	}
    };

DItem_ComboBoxRsc COMBOBOXID_NewItems_ElementLevelWithHook =
    {
    NOCMD, LCMD, SYNONYMID_ElementAttributes, NOHELP, MHELPTOPIC, 
    HOOKITEMID_NewItems_ElementLevel, NOARG,
    MAX_LINKAGE_STRING_LENGTH, "", "", "", "", NOMASK,
    0, 15, 4, 0, 0, 
    COMBOATTR_READONLY | COMBOATTR_DISPLAYALLCOLUMNS | COMBOATTR_USEMODELVALUE | 
    COMBOATTR_FONTBYCOLUMN,
    TXT_NewitemsElementAttributesLevel,
    "niG.currentLevel",
	{
	{MAX_LINKAGE_STRING_LENGTH,  MAX_LINKAGE_STRING_LENGTH, ALIGN_LEFT, ""},
	}
    };

DItem_ComboBoxRsc COMBOBOXID_NewItems_ElementColorWithHook =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELPTOPIC, 
    HOOKITEMID_NewItems_ElementColor, NOARG,
    128, "", "", "", "", NOMASK,
    0, 15, 4, 0, 0, 
    COMBOATTR_READONLY | COMBOATTR_DISPLAYALLCOLUMNS | COMBOATTR_USEMODELVALUE | 
    COMBOATTR_DRAWPREFIXICON | COMBOATTR_NODISABLEICON,
    TXT_NewitemsElementAttributesColor,
    "niG.currentColor",
	{
	{128,  128, ALIGN_LEFT, ""},
	}
    };

DItem_ComboBoxRsc COMBOBOXID_NewItems_ElementStyleWithHook =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELPTOPIC, 
    HOOKITEMID_NewItems_ElementStyle, NOARG,
    128, "", "", "", "", NOMASK,
    0, 15, 4, 0, 0, 
    COMBOATTR_READONLY | COMBOATTR_DISPLAYALLCOLUMNS | COMBOATTR_USEMODELVALUE | 
    COMBOATTR_DRAWPREFIXICON | COMBOATTR_NODISABLEICON,
    TXT_NewitemsElementAttributesStyle,
    "niG.currentStyle",
	{
	{128,  128, ALIGN_LEFT, ""},
	}
    };

DItem_ComboBoxRsc COMBOBOXID_NewItems_ElementWeightWithHook =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELPTOPIC, 
    HOOKITEMID_NewItems_ElementWeight, NOARG,
    128, "", "", "", "", NOMASK,
    0, 15, 4, 0, 0, 
    COMBOATTR_READONLY | COMBOATTR_DISPLAYALLCOLUMNS | COMBOATTR_USEMODELVALUE | 
    COMBOATTR_DRAWPREFIXICON | COMBOATTR_NODISABLEICON,
    TXT_NewitemsElementAttributesWeight,
    "niG.currentWeight",
	{
	{128,  128, ALIGN_LEFT, ""},
	}
    };


#undef XSIZE		/* undef symbols so they can be reused */
#undef YSIZE
#undef BTN_WIDTH

#undef XALT
#undef XALTR
#undef YALT

#undef B1X

#undef GBW
#undef X1
#undef X2
#undef X3
#undef X4
#undef SW
#undef TW

#undef XRB
#undef XRBL
#undef XGRP
#undef Y10
#undef Y11
#undef Y12
#undef Y13
#undef Y14
#undef Y15
#undef Y16

/*----------------------------------------------------------------------+
|									|
|   Item Resource Specifications					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Button Group Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_ButtonGroupRsc BGROUPID_EditIconTools =
    {
    SYNONYMID_BGroup, NOHELP, MHELP, NOHOOK, NOARG, 
    0, 2, 4, 0, 0, WHITE_INDEX,
    "", 
    "niG.parameter2",
{
{RTYPE_IconCmdLargeIcon, ICONCMDID_PlaceFenceBlock, NOCMD, LCMD,
    0, NOMASK, 0, 0, "Place Fence Block"},
{Icon, ICONID_Eraser,	    NOCMD, LCMD,
    1, NOMASK, 0, 0, "Eraser"},
{Icon, ICONID_Line,	    NOCMD, LCMD, 
    2, NOMASK, 0, 0, "Line"},
{Icon, ICONID_Brush, NOCMD, LCMD,
    3, NOMASK, 0, 0, "Brush"},
{Icon, ICONID_Rect, NOCMD, LCMD,
    4, NOMASK, 0, 0, "Rectangle"},
{Icon, ICONID_FillRect, NOCMD, LCMD, 
    5, NOMASK, 0, 0, "Filled Rectangle"},
{Icon, ICONID_Circle, NOCMD, LCMD, 
    6, NOMASK, 0, 0, "Circle"},
{Icon, ICONID_FillCircle, NOCMD, LCMD, 
    7, NOMASK, 0, 0, "Filled Circle"},
}
    };

DItem_ButtonGroupRsc BGROUPID_Colors =
    {
    NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 
    0, 9, 2, 0, 0, -1,
    "", 
    "niG.colorIndex",
{
{Icon, ICONID_BGRP_BLACK,       NOCMD, LCMD, BLACK_INDEX,       NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_DARKBLUE,    NOCMD, LCMD, DARKBLUE_INDEX,    NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_DARKGREEN,   NOCMD, LCMD, DARKGREEN_INDEX,   NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_TURQUOISE,   NOCMD, LCMD, TURQUOISE_INDEX,   NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_MAROON,      NOCMD, LCMD, MAROON_INDEX,      NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_VIOLET,      NOCMD, LCMD, VIOLET_INDEX,      NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_DARKYELLOW,  NOCMD, LCMD, DARKYELLOW_INDEX,  NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_DGREY,       NOCMD, LCMD, DGREY_INDEX,       NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_LGREY,       NOCMD, LCMD, LGREY_INDEX,       NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_WHITE,       NOCMD, LCMD, WHITE_INDEX,       NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_BLUE,	NOCMD, LCMD, BLUE_INDEX,	NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_GREEN,       NOCMD, LCMD, GREEN_INDEX,       NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_CYAN,	NOCMD, LCMD, CYAN_INDEX,	NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_RED,		NOCMD, LCMD, RED_INDEX,		NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_MAGENTA,     NOCMD, LCMD, MAGENTA_INDEX,     NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_YELLOW,      NOCMD, LCMD, YELLOW_INDEX,      NOMASK, 0, 0, ""},
{Icon, ICONID_BGRP_MGREY,       NOCMD, LCMD, MGREY_INDEX,       NOMASK, 0, 0, ""},
}
    };

IconRsc ICONID_Eraser =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x00,
        0x00, 0x06, 0x0c, 0x00, 0x00, 0x18, 0x30, 0x00,
        0x00, 0x60, 0xc0, 0x00, 0x01, 0x83, 0x00, 0x00,
        0x06, 0x0c, 0x10, 0x00, 0x18, 0x30, 0x60, 0x00,
        0x60, 0xc1, 0x80, 0x01, 0x83, 0x06, 0x00, 0x06,
        0x0c, 0x18, 0x00, 0x1e, 0x30, 0x60, 0x00, 0x66,
        0xc1, 0x80, 0x01, 0xe7, 0x06, 0x00, 0x06, 0x66,
        0x18, 0x00, 0x1e, 0x66, 0x60, 0x00, 0x3e, 0x67,
        0x80, 0x00, 0x7e, 0x66, 0x00, 0x00, 0x7e, 0x78,
        0x00, 0x00, 0x7e, 0x60, 0x00, 0x00, 0x7f, 0x80,
        0x00, 0x00, 0x7e, 0x00, 0x00, 0x00, 0x78, 0x00,
        0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };

IconRsc	ICONID_Line =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
	{
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00,
        0x00, 0x00, 0x70, 0x00, 0x00, 0x01, 0xc0, 0x00,
        0x00, 0x07, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00,
        0x00, 0x70, 0x00, 0x00, 0x01, 0xc0, 0x00, 0x00,
        0x07, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00,
        0x70, 0x00, 0x00, 0x01, 0xc0, 0x00, 0x00, 0x07,
        0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x70,
        0x00, 0x00, 0x01, 0xc0, 0x00, 0x00, 0x07, 0x00,
        0x00, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x70, 0x00,
        0x00, 0x01, 0xc0, 0x00, 0x00, 0x07, 0x00, 0x00,
        0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };
    
IconRsc	ICONID_Brush =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x00,
        0x00, 0x06, 0x0c, 0x00, 0x00, 0x18, 0x30, 0x00,
        0x00, 0x60, 0xc0, 0x00, 0x01, 0x83, 0x00, 0x00,
        0x06, 0x0c, 0x10, 0x00, 0x18, 0x30, 0x60, 0x00,
        0x60, 0xc1, 0x80, 0x01, 0x83, 0x06, 0x00, 0x02,
        0x0c, 0x18, 0x00, 0x0c, 0x30, 0x60, 0x00, 0x1c,
        0xc1, 0x80, 0x00, 0x67, 0x06, 0x00, 0x00, 0xc2,
        0x18, 0x00, 0x03, 0x04, 0x60, 0x00, 0x06, 0x07,
        0x80, 0x00, 0x1c, 0x0e, 0x00, 0x00, 0x3c, 0x78,
        0x00, 0x00, 0xff, 0xc0, 0x00, 0x01, 0xfe, 0x00,
        0x00, 0x07, 0xf0, 0x00, 0x00, 0x0f, 0x80, 0x00,
        0x00, 0x3c, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };
    
IconRsc	ICONID_Rect =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
        0xff, 0xff, 0x80, 0x07, 0xff, 0xff, 0x00, 0x0c,
        0x00, 0x06, 0x00, 0x18, 0x00, 0x0c, 0x00, 0x30,
        0x00, 0x18, 0x00, 0x60, 0x00, 0x30, 0x00, 0xc0,
        0x00, 0x60, 0x01, 0x80, 0x00, 0xc0, 0x03, 0x00,
        0x01, 0x80, 0x06, 0x00, 0x03, 0x00, 0x0c, 0x00,
        0x06, 0x00, 0x18, 0x00, 0x0c, 0x00, 0x30, 0x00,
        0x18, 0x00, 0x7f, 0xff, 0xf0, 0x00, 0xff, 0xff,
        0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };
    
IconRsc	ICONID_FillRect =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
        0xff, 0xff, 0x80, 0x07, 0xff, 0xff, 0x00, 0x0f,
        0xff, 0xfe, 0x00, 0x1f, 0xff, 0xfc, 0x00, 0x3f,
        0xff, 0xf8, 0x00, 0x7f, 0xff, 0xf0, 0x00, 0xff,
        0xff, 0xe0, 0x01, 0xff, 0xff, 0xc0, 0x03, 0xff,
        0xff, 0x80, 0x07, 0xff, 0xff, 0x00, 0x0f, 0xff,
        0xfe, 0x00, 0x1f, 0xff, 0xfc, 0x00, 0x3f, 0xff,
        0xf8, 0x00, 0x7f, 0xff, 0xf0, 0x00, 0xff, 0xff,
        0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };

IconRsc ICONID_Circle =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x01, 0xfc, 0x00, 0x00,
        0x0f, 0xfe, 0x00, 0x00, 0x78, 0x0f, 0x00, 0x01,
        0xc0, 0x07, 0x00, 0x03, 0x00, 0x06, 0x00, 0x0c,
        0x00, 0x06, 0x00, 0x18, 0x00, 0x0c, 0x00, 0x60,
        0x00, 0x0c, 0x00, 0xc0, 0x00, 0x18, 0x01, 0x80,
        0x00, 0x30, 0x03, 0x00, 0x00, 0x60, 0x06, 0x00,
        0x00, 0xc0, 0x0c, 0x00, 0x01, 0x80, 0x18, 0x00,
        0x03, 0x00, 0x18, 0x00, 0x0c, 0x00, 0x30, 0x00,
        0x18, 0x00, 0x30, 0x00, 0x60, 0x00, 0x70, 0x01,
        0xc0, 0x00, 0x78, 0x0f, 0x00, 0x00, 0x3f, 0xf8,
        0x00, 0x00, 0x1f, 0xc0, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };

IconRsc ICONID_FillCircle =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x01, 0xfc, 0x00, 0x00,
        0x0f, 0xfe, 0x00, 0x00, 0x7f, 0xff, 0x00, 0x01,
        0xff, 0xff, 0x00, 0x03, 0xff, 0xfe, 0x00, 0x0f,
        0xff, 0xfe, 0x00, 0x1f, 0xff, 0xfc, 0x00, 0x7f,
        0xff, 0xfc, 0x00, 0xff, 0xff, 0xf8, 0x01, 0xff,
        0xff, 0xf0, 0x03, 0xff, 0xff, 0xe0, 0x07, 0xff,
        0xff, 0xc0, 0x0f, 0xff, 0xff, 0x80, 0x1f, 0xff,
        0xff, 0x00, 0x1f, 0xff, 0xfc, 0x00, 0x3f, 0xff,
        0xf8, 0x00, 0x3f, 0xff, 0xe0, 0x00, 0x7f, 0xff,
        0xc0, 0x00, 0x7f, 0xff, 0x00, 0x00, 0x3f, 0xf8,
        0x00, 0x00, 0x1f, 0xc0, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };

IconRsc ICONID_TabOptions =
    {
    12,    12,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x07, 0xfe, 0x7f, 0xe6, 0x66, 0x06, 0x00,
        0x60, 0x06, 0x00, 0x60, 0x06, 0x00, 0xf0, 0x0f,
        0x00, 0x00, 
        }
    };

IconRsc ICONID_RadioBtnOptions =
    {
    12,    12,    ICONFORMAT_FIXEDCOLORS,    BLUE_INDEX, "",
        {
        11,11,11, 9, 9, 9, 9, 9,11,11,11,11,
        11, 9, 9, 9, 0, 0, 0, 9, 9, 9,11,11,
        11, 9, 0, 0, 1, 1, 1, 1, 4, 9,11,11,
         9, 9, 0, 6, 6, 6, 6, 6, 4, 9, 9, 7,
         9, 0, 1, 6, 0, 0, 0, 6, 4,11, 9, 7,
         9, 0, 1, 6, 0,11, 0, 6, 4,11, 9, 7,
         9, 0, 1, 6, 0, 0, 0, 6, 4,11, 9, 7,
         9, 9, 1, 6, 6, 6, 6, 6, 4, 9, 9, 7,
        11, 9, 4, 4, 4, 4, 4, 4, 4, 9, 7,11,
        11, 9, 9, 9,11,11,11, 9, 9, 9, 7,11,
        11,11, 7, 9, 9, 9, 9, 9, 7, 7,11,11,
        11,11,11, 7, 7, 7, 7, 7, 7,11,11,11,
        }
    };

IconRsc ICONID_ScaleOptions =
    {
    12,    12,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0xff, 0xf9, 0x69, 0xb6, 0xdf,
        0x6f, 0xb6, 0xd9, 0x69, 0xff, 0xf0, 0x00, 0x00,
        0x00, 0x00, 
        }
    };

IconRsc ICONID_ComboBox =
    {
    12,    12,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0xff, 0xf8, 0x01, 0xbf, 0xd9, 0xf9, 0x8f, 0x18,
        0x61, 0x80, 0x18, 0x01, 0x9f, 0x98, 0xf1, 0x80,
        0x1f, 0xff, 
        }
    };

IconRsc ICONID_BGRP_BLACK =
    {
    16,    16,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_DARKBLUE =
    {
    16,    16,    FORMAT_MONOBITMAP,    DARKBLUE_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_DARKGREEN =
    {
    16,    16,    FORMAT_MONOBITMAP,    DARKGREEN_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_TURQUOISE =
    {
    16,    16,    FORMAT_MONOBITMAP,    TURQUOISE_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_MAROON =
    {
    16,    16,    FORMAT_MONOBITMAP,    MAROON_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_VIOLET =
    {
    16,    16,    FORMAT_MONOBITMAP,    VIOLET_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_DARKYELLOW =
    {
    16,    16,    FORMAT_MONOBITMAP,    DARKYELLOW_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_DGREY =
    {
    16,    16,    FORMAT_MONOBITMAP,    DGREY_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_LGREY =
    {
    16,    16,    FORMAT_MONOBITMAP,    LGREY_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_WHITE =
    {
    16,    16,    FORMAT_MONOBITMAP,    WHITE_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_BLUE =
    {
    16,    16,    FORMAT_MONOBITMAP,    BLUE_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_GREEN =
    {
    16,    16,    FORMAT_MONOBITMAP,    GREEN_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_CYAN =
    {
    16,    16,    FORMAT_MONOBITMAP,    CYAN_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_RED =
    {
    16,    16,    FORMAT_MONOBITMAP,    RED_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_MAGENTA =
    {
    16,    16,    FORMAT_MONOBITMAP,    MAGENTA_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_YELLOW =
    {
    16,    16,    FORMAT_MONOBITMAP,    YELLOW_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };

IconRsc ICONID_BGRP_MGREY =
    {
    16,    16,    FORMAT_MONOBITMAP,    MGREY_INDEX, "",
        {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        }
    };
    
/*----------------------------------------------------------------------+
|									|
|   ListBox Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_Sash1 =
    {
    NOHELP, MHELP, 
    HOOKID_ListBoxSash, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_COLOREDROWS | LISTATTR_SELSINGLE | LISTATTR_HORIZSCROLLBAR, 
    6, 0, "",
	{
	{24*XC, 60, 0, ""},
	}
    };

DItem_ListBoxRsc LISTBOXID_Sash2 =
    {
    NOHELP, MHELP, 
    HOOKID_ListBoxSash, NOARG, 
    LISTATTR_HORIZSCROLLBAR, 
    6, 0, "",
	{
	{24*XC, 60, 0, ""},
	}
    };

DItem_ListBoxRsc LISTBOXID_Multi =
    {
    NOHELP, MHELP, 
    HOOKID_ListBoxMulti, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_SELBROWSE | LISTATTR_INDEPENDENTCOLS, 
    10, 0, TXT_ExampleList,
	{
	{14*XC, 60, 0, ""},
	{8*XC, 60, 0, ""},
	}
    };

DItem_ListBoxRsc LISTBOXID_SelectionList =
    {
    NOHELP, MHELP, 
    HOOKID_ListBoxSelection, NOARG,	   
    LISTATTR_DYNAMICSCROLL | LISTATTR_NEVERSELECTION | LISTATTR_COLHEADINGBORDERS | 
    LISTATTR_HORIZSCROLLBAR | LISTATTR_SORTCOLUMNS | LISTATTR_SAVECOLUMNINFO, 
    10, 2, TXT_CurrentSelection,
	{
	{1*XC, 1, 0, ""},		/* { */
	{5*XC, 8, ALIGN_RIGHT, TXT_Row},
	{5*XC, 8, ALIGN_RIGHT, TXT_Col},
	{2*XC, 1, 0, ""},		/* } */
	{14*XC, 60, 0, ""},
	{8*XC, 60, 0, ""},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   Menu Bar Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_MenuBarRsc MENUBARID_NewItems =
    {
    NOHOOK, NOARG,
	{
	{PulldownMenu, PDMID_Examples},
	}
    };

DItem_MenuBarRsc MENUBARID_MultiList =
    {
    NOHOOK, NOARG,
	{
	{PulldownMenu, PDMID_MultiListTests},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   Popup Menu Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_PopupMenuRsc POPUPMENUID_MultiList = 
    {
    NOHELP, LHELP, NOHOOK, NOARG, 0,
    PulldownMenu, PDMID_MultiListSelect    
    };
			
/*----------------------------------------------------------------------+
|									|
|   Pulldown Menu Resources						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   File Menu								|
|									|
+----------------------------------------------------------------------*/
DItem_PulldownMenuRsc PDMID_Examples =
    {
    NOHELP, OHELPTASKIDCMD,
    NOHOOK,
    ON | ALIGN_LEFT, TXT_Examples,
{
{TXT_RadioButtons_A,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_RADIOBUTTONS, OTASKID, ""},
{TXT_HorizontalScales_A,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_HSCALES, OTASKID, ""},
{TXT_VerticalScales_A,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_VSCALES, OTASKID, ""},
{TXT_MultiLinePushButtons,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_MLPBUTS, OTASKID, ""},
{TXT_Sash_A,	    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_SASH, OTASKID, ""},
{TXT_VSash_A,	    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_VSASH, OTASKID, ""},
{TXT_ButtonGroup_A, NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_BGROUP, OTASKID, ""},
{TXT_ColoredItems_A,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_COLORS, OTASKID, ""},
{TXT_MultiSelectionList,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_MULTILIST, OTASKID, ""},

{TXT_AlertBoxMenuLabel,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_ALERTS, OTASKID, ""},

{TXT_TabsBoxMenuLabel,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_TABS, OTASKID, ""},

{TXT_NewitemsElementAttributes,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_LEVELATTRIBUTES, OTASKID, ""},

#if defined (commentOut)
{TXT_2DArrows_A,    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_OPEN_2DARROWS, OTASKID, ""},
#endif			
{"-",		    NOACCEL,OFF,NOMARK,0,NOSUBMENU,NOHELP,OHELPTASKIDCMD,
		    NOHOOK, NOARG,NOCMD, OTASKID,""},
{TXT_Exit,	    VBIT_CTRL|'Q', ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, 0,
		    CMD_NEWITEMS_EXIT, OTASKID, ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   Multi-Selection List Tests Menu					|
|									|
+----------------------------------------------------------------------*/
DItem_PulldownMenuRsc PDMID_MultiListTests = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Tests,
{
{TXT_Modify,	NOACCEL, ON,  NOMARK, PulldownMenu, PDMID_MultiListModify,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{TXT_Select,	NOACCEL, ON,  NOMARK, PulldownMenu, PDMID_MultiListSelect,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{TXT_Position,	NOACCEL, ON,  NOMARK, PulldownMenu, PDMID_MultiListPosition,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{TXT_Location,	NOACCEL, ON,  NOMARK, PulldownMenu, PDMID_MultiListLocation,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{"-",		NOACCEL,OFF,NOMARK,0,NOSUBMENU,NOHELP,OHELPTASKIDCMD,
		NOHOOK, NOARG,NOCMD, OTASKID,""},
{TXT_Exit,	NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
	        CMD_DMSG_ACTION_SYSMENUCLOSE, MTASKID, ""},
}
    };
    
DItem_PulldownMenuRsc PDMID_MultiListModify = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Modify,
{	
{TXT_ResetList,	    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_ResetList,
		    NOCMD, OTASKID, ""},
{TXT_RecreateList,  NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_RecreateList,
		    NOCMD, OTASKID, ""},
{TXT_AddItemAtTop,  NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_AddTop,
		    NOCMD, OTASKID, ""},
{TXT_AddItemAtBottom,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_AddBottom,
		    NOCMD, OTASKID, ""},
{TXT_AddBeforeSelection,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_AddSelection,
		    NOCMD, OTASKID, ""},
{TXT_DeleteSelection,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_DeleteSelection,
		    NOCMD, OTASKID, ""},
{TXT_ToggleAddMode, VBIT_SHIFT|VKEY_F8, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_ToggleAddMode,
		    CMD_DMSG_ACTION_ADDMODE, MTASKID, ""},
}
    };
    
DItem_PulldownMenuRsc PDMID_MultiListSelect = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Select,
{	
{TXT_SelectTopItem, NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_SelectTop,
		    NOCMD, OTASKID, ""},
{TXT_SelectBottomItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_SelectBottom,
		    NOCMD, OTASKID, ""},
{TXT_SelectEveryOther,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_SelectEveryOther,
		    NOCMD, OTASKID, ""},
{TXT_SelectAll,	    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_SelectAll,
		    NOCMD, OTASKID, ""},
{TXT_DeselectTopItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_DeselectTop,
		    NOCMD, OTASKID, ""},
{TXT_DeselectBottomItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_DeselectBottom,
		    NOCMD, OTASKID, ""},
{TXT_DeselectAll,   NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_DeselectAll,
		    NOCMD, OTASKID, ""},
}
    };
			
			
DItem_PulldownMenuRsc PDMID_MultiListPosition = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Position,
{	
{TXT_Make1stTop,    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_Top1st,
		    NOCMD, OTASKID, ""},
{TXT_MakeLastBottom,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_BottomLast,
		    NOCMD, OTASKID, ""},
{TXT_Make1stSelectionTop,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_TopSelection,
		    NOCMD, OTASKID, ""},
}
    };

DItem_PulldownMenuRsc PDMID_MultiListLocation = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Select,
{	
{TXT_MoveToTopItem, NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_LocationTop,
		    NOCMD, OTASKID, ""},
{TXT_MoveToBottomItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_LocationBottom,
		    NOCMD, OTASKID, ""},
{TXT_MoveToBeginningofSelection,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MSID_LocationSelection,
		    NOCMD, OTASKID, ""},
}
    };
    
/*----------------------------------------------------------------------+
|									|
|   Option Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
DItem_OptionButtonRsc  OPTIONBTNID_NewItem =
    {
    SYNONYMID_RadioButtons1, NOHELP, MHELP, HOOKID_Dummy, OPTNBTNATTR_NEWSTYLE | NOARG, 
    TXT_Parameter1,
    "niG.parameter1",
	{
	{NOTYPE, NOICON, NOCMD, LCMD, 1, 0xFF, ON, TXT_Value1},
	{NOTYPE, NOICON, NOCMD, LCMD, 2, 0xFF, ON, TXT_Value2},
	{NOTYPE, NOICON, NOCMD, LCMD, 3, 0xFF, ON, TXT_Value3},
	}
    };

DItem_OptionButtonRsc  OPTIONBTNID_BGroup =
    {
    SYNONYMID_BGroup, NOHELP, MHELP, HOOKID_Dummy, OPTNBTNATTR_NEWSTYLE | NOARG, 
    TXT_BGroupItem,
    "niG.parameter2",
	{
	{NOTYPE, NOICON, NOCMD, LCMD, 0, NOMASK, ON, TXT_Row0Col0},
	{NOTYPE, NOICON, NOCMD, LCMD, 1, NOMASK, ON, TXT_Row0Col1},
	{NOTYPE, NOICON, NOCMD, LCMD, 2, NOMASK, ON, TXT_Row1Col0},
	{NOTYPE, NOICON, NOCMD, LCMD, 3, NOMASK, ON, TXT_Row1Col1},
	{NOTYPE, NOICON, NOCMD, LCMD, 4, NOMASK, ON, TXT_Row2Col0},
	{NOTYPE, NOICON, NOCMD, LCMD, 5, NOMASK, ON, TXT_Row2Col1},
	{NOTYPE, NOICON, NOCMD, LCMD, 6, NOMASK, ON, TXT_Row3Col0},
	{NOTYPE, NOICON, NOCMD, LCMD, 7, NOMASK, ON, TXT_Row3Col1},
	}
    };

DItem_OptionButtonRsc  OPTIONBTNID_SelectionMode =
    {
    SYNONYMID_MultiList, NOHELP, MHELP, NOHOOK, 
    OPTNBTNATTR_NEWSTYLE | NOARG, 
    TXT_SelectionMode,
    "niG.selectionMode",
	{
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_BROWSE,   0xFF, ON,	TXT_Browse},
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_SINGLE,   0xFF, ON,	TXT_Single},
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_MULTI,    0xFF, ON,	TXT_Multiple},
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_EXTENDED, 0xFF, ON, TXT_Extended},
	}
    };

DItem_OptionButtonRsc  OPTIONBTNID_DefaultPushButton =
    {
    NOSYNONYM, NOHELP, MHELP, HOOKID_OptionBtnDefaultPB, 
    OPTNBTNATTR_NEWSTYLE | NOARG, 
    TXT_DefaultPushButton,
    "niG.defaultPushButtonId",
	{
{NOTYPE, NOICON, NOCMD, LCMD, 0, NOMASK, ON, TXT_None},
{NOTYPE, NOICON, NOCMD, LCMD, PUSHBUTTONID_MLine21, NOMASK, ON, TXT_LeftJustifiedBtnLabel},
{NOTYPE, NOICON, NOCMD, LCMD, PUSHBUTTONID_MLine23, NOMASK, ON, TXT_RightJustifiedBtnLabel},
{NOTYPE, NOICON, NOCMD, LCMD, PUSHBUTTONID_MLine13, NOMASK, ON, TXT_TwoTagBtnLabel},
{NOTYPE, NOICON, NOCMD, LCMD, PUSHBUTTONID_MLine31, NOMASK, ON, TXT_ThreeTagBtnLabel},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   PushButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_OModalRadioButtons =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_Dummy, 0, CMD_NEWITEMS_MODALOPEN_RADIOBUTTONS, LCMD, "",
    TXT_OpenModal
    };

DItem_PushButtonRsc PUSHBUTTONID_OModalScales =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_Dummy, 0, CMD_NEWITEMS_MODALOPEN_SCALES, LCMD, "", 
    TXT_OpenModal
    };

DItem_PushButtonRsc PUSHBUTTONID_MLine11 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_MLPushButton, 0, NOCMD, LCMD, "",
    TXT_SampleTag	/* Using "Tag" instead of "Label" to check descenders */
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine12 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_RadioBtnOptions, "", 
    TXT_Example
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine13 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_ScaleOptions, "", 
    TXT_TwoTagBtnLabel
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine21 =
    {
    NOT_DEFAULT_BUTTON | PBUTATTR_ALIGNLEFT | PBUTATTR_ICONRIGHT, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_TabOptions, "", 
    TXT_LeftJustifiedBtnLabel
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine22 =
    {
    NOT_DEFAULT_BUTTON | PBUTATTR_ICONRIGHT, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_RadioBtnOptions, "", 
    TXT_Example1
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine23 =
    {
    NOT_DEFAULT_BUTTON | PBUTATTR_ALIGNRIGHT | PBUTATTR_ICONRIGHT, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_ScaleOptions, "", 
    TXT_RightJustifiedBtnLabel
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine31 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_TabOptions, "", 
    TXT_ThreeTagBtnLabel
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine32 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_RadioBtnOptions, "", 
    TXT_Example2
    };

DItem_PushButtonXRsc PUSHBUTTONID_MLine33 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    HOOKID_Dummy, 0, NOCMD, LCMD, 
    Icon, ICONID_ScaleOptions, "", 
    TXT_SampleTag1
    };

DItem_PushButtonXRsc PUSHBUTTONID_Alert1 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    NOHOOK, 0, CMD_NEWITEMS_ALERT_CRITICAL, LCMD, 
    Icon, ICONID_TabOptions, "", 
    TXT_CriticalPushbtnLabel
    };
DItem_PushButtonXRsc PUSHBUTTONID_Alert2=
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    NOHOOK, 0, CMD_NEWITEMS_ALERT_INFORMATION, LCMD, 
    Icon, ICONID_RadioBtnOptions, "", 
    TXT_InformationPushbtnLabel
    };
DItem_PushButtonXRsc PUSHBUTTONID_Alert3 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    NOHOOK, 0, CMD_NEWITEMS_ALERT_QUESTION, LCMD, 
    Icon, ICONID_ScaleOptions, "", 
    TXT_QuestionPushbtnLabel
    };
DItem_PushButtonXRsc PUSHBUTTONID_Alert4 =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    NOHOOK, 0, CMD_NEWITEMS_ALERT_WARNING, LCMD, 
    Icon, ICONID_TabOptions, "", 
    TXT_WarningPushbtnLabel
    };

DItem_PushButtonRsc PUSHBUTTONID_FreeTabPage =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
    NOHOOK, 0, CMD_NEWITEMS_FREETABPAGE, LCMD, "",
    TXT_FreeTabPagePush
    };

/*----------------------------------------------------------------------+
|									|
|   Radio Button Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_RadioButtonListRsc RBLISTID_Options1 =
    {
{
    RBUTTONID_Option1,
    RBUTTONID_Option2,
    RBUTTONID_Option3,
}	
    };

DItem_RadioButtonRsc RBUTTONID_Option1 = 
    {
    NOCMD, LCMD, SYNONYMID_OptionButton, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG, 
/*    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,*/
    1, 0x00ff, RBLISTID_Options1,
    TXT_RadioButton1,
    "radioButtonULong" //"niG.parameter1"
    };

DItem_RadioButtonRsc RBUTTONID_Option2 = 
    {
    NOCMD, LCMD, SYNONYMID_OptionButton, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG, 
/*    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,*/
    2, 0x00ff, RBLISTID_Options1,
    TXT_RadioButton2,
    "radioButtonULong" //"niG.parameter1"
    };

DItem_RadioButtonRsc RBUTTONID_Option3 = 
    {
    NOCMD, LCMD, SYNONYMID_OptionButton, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG, 
/*    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,*/
    3, 0x00ff, RBLISTID_Options1,
    TXT_RadioButton3,
    "radioButtonULong" //"niG.parameter1"
    };

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
DItem_RadioButtonListRsc RBLISTID_Options2 =
    {
{
    RBUTTONID_Option4,
    RBUTTONID_Option5,
    RBUTTONID_Option6,
}	
    };

DItem_RadioButtonRsc RBUTTONID_Option4 = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG, 
    1, 0x00ff, RBLISTID_Options2,
    TXT_RadioButton4,
    "niG.parameter4"
    };

DItem_RadioButtonRsc RBUTTONID_Option5 = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG, 
    2, 0x00ff, RBLISTID_Options2,
    TXT_RadioButton5,
    "niG.parameter4"
    };

DItem_RadioButtonRsc RBUTTONID_Option6 = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_RadioButtons, NOARG, 
    3, 0x00ff, RBLISTID_Options2,
    TXT_RadioButton6,
    "niG.parameter4"
    };

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
DItem_RadioButtonListRsc RBLISTID_TabPlacement =
    {
{
    RBUTTONID_TabsTop,
    RBUTTONID_TabsBottom,
    RBUTTONID_TabsLeft,
    RBUTTONID_TabsRight,
}	
    };

DItem_RadioButtonRsc RBUTTONID_TabsTop = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsTop, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsTop,
    "niG.tabPlacement"
    };

DItem_RadioButtonRsc RBUTTONID_TabsBottom = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsBottom, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsBottom,
    "niG.tabPlacement"
    };

DItem_RadioButtonRsc RBUTTONID_TabsLeft = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsLeft, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsLeft,
    "niG.tabPlacement"
    };

DItem_RadioButtonRsc RBUTTONID_TabsRight = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_TabPageRadioBtns, NOARG,
    RBUTTONID_TabsRight, 0x00ff, RBLISTID_TabPlacement,
    TXT_TabsRight,
    "niG.tabPlacement"
    };

/*----------------------------------------------------------------------+
|									|
|   Sash Item Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_SashRsc	SASHID_Example =
    {
    NOHELP, MHELP, HOOKID_Sash, 0, 
    5*YC, 5*YC, SASHATTR_ALLGRAB | SASHATTR_SOLIDTRACK | SASHATTR_WIDE | SASHATTR_SAVEPOSITION
    }

DItem_SashRsc	SASHID_VExample =
    {
    NOHELP, MHELP, HOOKID_VSash, 0, 
    5*YC, 5*YC, SASHATTR_VERTICAL | SASHATTR_ALLGRAB | SASHATTR_SOLIDTRACK | SASHATTR_WIDE | SASHATTR_SAVEPOSITION
    }

/*----------------------------------------------------------------------+
|									|
|   Scale Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_ScaleRsc SCALEID_Range1 =
    {
    NOCMD, LCMD, SYNONYMID_Range1, NOHELP, MHELP, 
    NOHOOK/*HOOKID_Dummy*/, NOARG, 
    0.0, 100.0, 1.0, 10.0, 
    TEXTID_Range1, 
    0, "%.0lf", 
    "",
    "niG.range1",
    "0", "100"
    };
    
DItem_ScaleRsc SCALEID_Range2 =
    {
    NOCMD, LCMD, SYNONYMID_Range2, NOHELP, MHELP, 
    HOOKID_Dummy, NOARG, 
    0.0, 100.0, 1.0, 10.0, 
    TEXTID_Range2, 
    SCALE_HASARROWS, "%.0lf", 
    "",
    "niG.range2",	    /* note range 2 is int and this still works */
    "0", "100"
    };
    
DItem_ScaleRsc SCALEID_Range3 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_Dummy, NOARG, 
    0.0, 100.0, 0.5, 10.0, 0, SCALE_SHOWVALUE, "%.0lf", 
    TXT_Range3_A,
    "niG.range3",
    "0", "100"
    };
    
DItem_ScaleRsc SCALEID_Range4 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    HOOKID_Dummy, NOARG, 
    0.0, 100.0, 1.0, 10.0, 0, SCALE_SHOWVALUE | SCALE_LIMITSONSIDE, "%.0lf", 
    TXT_Range4,
    "niG.range4",
    "0", "100"
    };
    
DItem_ScaleRsc SCALEID_Speed =
    {
    NOCMD, LCMD, SYNONYMID_Speed, NOHELP, MHELP, 
    HOOKID_ScaleSpeed, NOARG, 
    0.0, 100.0, 1.0, 10.0, 0, SCALE_SHOWVALUE | SCALE_HASARROWS, "%.0lf", 
    TXT_Speed_A,
    "niG.speed",
    "", ""
    };
    
DItem_ScaleRsc SCALEID_TiedToSpeed =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    0.0, 100.0, 1.0, 10.0, 0, SCALE_SHOWVALUE | SCALE_LIMITSONSIDE, "%.0lf", 
    "",
    "niG.speed",
    "", ""
    };
    
/*----------------------------------------------------------------------+
|									|
|   Text Item Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_Range1 = 
    {
    NOCMD, LCMD, SYNONYMID_Range1, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    5, "%.0lf", "%lf", "0", "100", NOMASK, 0, 
    TXT_Range1_A,
    "niG.range1"
    };

DItem_TextRsc TEXTID_Range2 = 
    {
    NOCMD, LCMD, SYNONYMID_Range2, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    5, "%ld", "%ld", "0", "100", NOMASK, 0, 
    TXT_Range2_A,
    "niG.range2"
    };

DItem_TextRsc TEXTID_Colored = 
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    40, "%s", "%s", "", "", NOMASK, 0, 
    TXT_String,
    "niG.string"
    };

/*----------------------------------------------------------------------+
|									|
|   ToggleButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEBTNID_Colored =
    {
    NOCMD, LCMD, NOSYNONYM, 
    NOHELP, LHELPTOPIC, 
    HOOKID_ToggleBtnColor, NOARG, 0x1, NOINVERT,
    TXT_ColoredToggleLabel, 
    "niG.parameter3"
    };

DItem_ToggleButtonRsc TOGGLEBTNID_AddTabPage =
    {
    NOCMD, LCMD, NOSYNONYM, 
    NOHELP, LHELPTOPIC, 
    HOOKID_TabPageToggleBtn, NOARG, 0x1, NOINVERT,
    TXT_TabPageToggle, 
    "niG.tabPageToggle"
    };

/*----------------------------------------------------------------------+
|									|
|   Synonym List Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_SynonymsRsc SYNONYMID_OptionButton = 
    {
    	{
	{OptionButton,	OPTIONBTNID_NewItem},
	{RadioButton,	RBUTTONID_Option1},
	{RadioButton,	RBUTTONID_Option2},
	{RadioButton,	RBUTTONID_Option3},
	{TabPageList,	TPLISTID_ONE},
	{TabPage,		TABPAGEID_BOXES},
	{TabPage,		TABPAGEID_RADIOS},
	{ComboBox,		COMBOBOXID_Test2},
	}
    };

DItem_SynonymsRsc SYNONYMID_RadioButtons1 = 
    {
    	{
	{OptionButton,	OPTIONBTNID_NewItem},
	{RadioButton,	RBUTTONID_Option1},
	{RadioButton,	RBUTTONID_Option2},
	{RadioButton,	RBUTTONID_Option3},
	{TabPageList,	TPLISTID_ONE},
	{TabPage,		TABPAGEID_BOXES},
	{TabPage,		TABPAGEID_RADIOS},
	{ComboBox,		COMBOBOXID_Test2},
	}
    };

DItem_SynonymsRsc SYNONYMID_Range1 =
    {
	{
	{Text,	TEXTID_Range1},
	{Scale, SCALEID_Range1},
	}
    };

DItem_SynonymsRsc SYNONYMID_Range2 =
    {
	{
	{Text,	TEXTID_Range2},
	{Scale, SCALEID_Range2},
	}
    };

DItem_SynonymsRsc SYNONYMID_Speed =
    {
	{
	{Scale, SCALEID_TiedToSpeed},
	}
    };

DItem_SynonymsRsc SYNONYMID_BGroup =
    {
	{
	{ButtonGroup,  BGROUPID_EditIconTools},
	{OptionButton, OPTIONBTNID_BGroup},
	}
    };
    
DItem_SynonymsRsc SYNONYMID_MultiList =
    {
	{
	{ListBox,  LISTBOXID_Multi},
	}
    };

DItem_SynonymsRsc SYNONYMID_ElementAttributes =
    {
	{
        {ComboBox,  COMBOBOXID_NewItems_ElementLevelWithHook},
	{ComboBox,  COMBOBOXID_ElementLevel},
	{ComboBox,  COMBOBOXID_ElementColor},
	{ComboBox,  COMBOBOXID_ElementStyle},
	{ComboBox,  COMBOBOXID_ElementWeight},
	}
    };

StringList STRLISTID_ComboBox1 =
{
  4,
  {
    { {0, 0, ICONID_TabOptions, 0}, "V Row 22" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "W Row 23" },
    { {0, 0, ICONID_ScaleOptions, 0}, "X Row 24" },
    { {0, 0, ICONID_TabOptions, 0}, "Y Row 25" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "Z Row 26" },
    { {0, 0, ICONID_ScaleOptions, 0}, "F Row 6" },
    { {0, 0, ICONID_TabOptions, 0}, "G Row 7" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "H Row 8" },
    { {0, 0, ICONID_ScaleOptions, 0}, "I Row 9" },
    { {0, 0, ICONID_TabOptions, 0}, "J Row 10" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "K Row 11" },
    { {0, 0, ICONID_ScaleOptions, 0}, "L Row 12" },
    { {0, 0, ICONID_TabOptions, 0}, "M Row 13" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "N Row 14" },
    { {0, 0, ICONID_ScaleOptions, 0}, "O Row 15" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "A Row 1" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "B Row 2" },
    { {0, 0, ICONID_ScaleOptions, 0}, "C Row 3" },
    { {0, 0, ICONID_TabOptions, 0}, "D Row 4" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "E Row 5" },
    { {0, 0, ICONID_TabOptions, 0}, "P Row 16" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "Q Row 17" },
    { {0, 0, ICONID_ScaleOptions, 0}, "R Row 18" },
    { {0, 0, ICONID_TabOptions, 0}, "S Row 19" },
    { {0, 0, ICONID_RadioBtnOptions, 0}, "T Row 20" },
    { {0, 0, ICONID_ScaleOptions, 0}, "U Row 21" },
  }
};

StringList STRLISTID_ComboBoxStates =
{
  1,
  {
    { {0}, "AL" }, { {0}, "Alabama" },
    { {0}, "AK" }, { {0}, "Alaska" },
    { {0}, "AZ" }, { {0}, "Arizona" },
    { {0}, "AR" }, { {0}, "Arkansas" },
    { {0}, "CA" }, { {0}, "California" },
    { {0}, "CO" }, { {0}, "Colorado" },
    { {0}, "CT" }, { {0}, "Connecticut" },
    { {0}, "DE" }, { {0}, "Delaware" },
    { {0}, "FL" }, { {0}, "Florida" },
    { {0}, "GA" }, { {0}, "Georgia" },
    { {0}, "HI" }, { {0}, "Hawaii" },
    { {0}, "ID" }, { {0}, "Idaho" },
    { {0}, "IL" }, { {0}, "Illinois" },
    { {0}, "IN" }, { {0}, "Indiana" },
    { {0}, "IA" }, { {0}, "Iowa" },
    { {0}, "KS" }, { {0}, "Kansas" },
    { {0}, "KY" }, { {0}, "Kentucky" },
    { {0}, "LA" }, { {0}, "Louisiana" },
    { {0}, "ME" }, { {0}, "Maine" },
    { {0}, "MD" }, { {0}, "Maryland" },
    { {0}, "MA" }, { {0}, "Massachusetts" },
    { {0}, "MI" }, { {0}, "Michigan" },
    { {0}, "MN" }, { {0}, "Minnesota" },
    { {0}, "MS" }, { {0}, "Mississippi" },
    { {0}, "MO" }, { {0}, "Missouri" },
    { {0}, "MT" }, { {0}, "Montana" },
    { {0}, "NE" }, { {0}, "Nebraska" },
    { {0}, "NV" }, { {0}, "Nevada" },
    { {0}, "NH" }, { {0}, "New Hampshire" },
    { {0}, "NJ" }, { {0}, "New Jersey" },
    { {0}, "NM" }, { {0}, "New Mexico" },
    { {0}, "NY" }, { {0}, "New York" },
    { {0}, "NC" }, { {0}, "North Carolina" },
    { {0}, "ND" }, { {0}, "North Dakota" },
    { {0}, "OH" }, { {0}, "Ohio" },
    { {0}, "OK" }, { {0}, "Oklahoma" },
    { {0}, "OR" }, { {0}, "Oregon" },
    { {0}, "PA" }, { {0}, "Pennsylvania" },
    { {0}, "RI" }, { {0}, "Rhode Island" },
    { {0}, "SC" }, { {0}, "South Carolina" },
    { {0}, "SD" }, { {0}, "South Dakota" },
    { {0}, "TN" }, { {0}, "Tennessee" },
    { {0}, "TX" }, { {0}, "Texas" },
    { {0}, "UT" }, { {0}, "Utah" },
    { {0}, "VT" }, { {0}, "Vermont" },
    { {0}, "VA" }, { {0}, "Virginia" },
    { {0}, "WA" }, { {0}, "Washington" },
    { {0}, "WV" }, { {0}, "West Virginia" },
    { {0}, "WI" }, { {0}, "Wisconsin" },
    { {0}, "WY" }, { {0}, "Wyoming" },
  }
};
