/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/newitems/newicmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/newitems/newicmd.r_v  $
|   $Workfile:   newicmd.r  $
|   $Revision: 1.5.32.1 $
|   	$Date: 2013/07/01 20:40:48 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	New Version 5.0 dialog items example application		|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>
#include "newitems.h"

/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_NEWITEMS  1

DllMdlApp DLLAPP_NEWITEMS =
    {
    "NEWITEMS", "newitems"          // taskid, dllName
    }


/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE		 0
#define CT_MAIN		 1
#define CT_NEWITEMS	 2
#define CT_OPEN		 3
#define CT_MODALOPEN	 4
#define CT_ALERT	 5

/*----------------------------------------------------------------------+
|                                                                       |
| 	Newitems commands						|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_MAIN =
{
    {  1, CT_NEWITEMS, INPUT, HID,		"NEWITEMS" },
};

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
Table	CT_NEWITEMS =
{
    {  1, CT_NONE,	INPUT, NONE,		"EXIT" },
    {  2, CT_OPEN,	INPUT, NONE,		"OPEN" },
    {  3, CT_MODALOPEN,	INPUT, NONE,		"MODALOPEN" },
    {  4, CT_ALERT,	INPUT, NONE,		"ALERT" },
    {  5, CT_NONE,	INPUT, NONE,		"FREETABPAGE" },

};

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
Table	CT_OPEN =
{
    {  DIALOGID_RadioButtons,	CT_NONE,    INPUT, NONE,    "RADIOBUTTONS" },
    {  DIALOGID_HScales,	CT_NONE,    INPUT, NONE,    "HSCALES" },
    {  DIALOGID_VScales,	CT_NONE,    INPUT, NONE,    "VSCALES" },
    {  DIALOGID_MLPushButtons,	CT_NONE,    INPUT, NONE,    "MLPBUTS" },
    {  DIALOGID_Sash,		CT_NONE,    INPUT, NONE,    "SASH" },
    {  DIALOGID_VSash,		CT_NONE,    INPUT, NONE,    "VSASH" },
    {  DIALOGID_BGroup,		CT_NONE,    INPUT, NONE,    "BGROUP" },
    {  DIALOGID_Colors,		CT_NONE,    INPUT, NONE,    "COLORS" },
    {  DIALOGID_MultiList,	CT_NONE,    INPUT, NONE,    "MULTILIST" },
    {  DIALOGID_2DArrows,	CT_NONE,    INPUT, NONE,    "2DARROWS" },
    {  DIALOGID_Alerts,		CT_NONE,    INPUT, NONE,    "ALERTS" },
    {  DIALOGID_Tabs,		CT_NONE,    INPUT, NONE,    "TABS" },
    {  DIALOGID_Attributes,     CT_NONE,    INPUT, NONE,    "LEVELATTRIBUTES" },
};

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
Table	CT_MODALOPEN =
{
    {  1, CT_NONE,	INPUT, NONE,		"RADIOBUTTONS" },
    {  2, CT_NONE,	INPUT, NONE,		"SCALES" },
};

Table	CT_ALERT =
{
    {  1, CT_NONE,	INPUT, NONE,		"CRITICAL" },
    {  2, CT_NONE,	INPUT, NONE,		"INFORMATION" },
    {  3, CT_NONE,	INPUT, NONE,		"QUESTION" },
    {  4, CT_NONE,	INPUT, NONE,		"WARNING" },

};
