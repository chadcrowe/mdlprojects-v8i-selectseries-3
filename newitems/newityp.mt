/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/newitems/newityp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/newitems/newityp.mtv  $
|   $Workfile:   newityp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:48 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	New Version 5.0 dialog items example dialog box structures	|
|									|
+----------------------------------------------------------------------*/
#include    "newitems.h"

publishStructures (newitemsglobals);

