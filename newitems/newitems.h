/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/newitems/newitems.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: newitems.h,v $
|   $Revision: 1.6.32.1 $
|       $Date: 2013/07/01 20:40:48 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|  Constants & types used in new items dialog example			|
|									|
+----------------------------------------------------------------------*/
#if !defined  (__newitemsH__)
#define      __newitemsH__

#ifndef __dlogitemH__
#include <dlogitem.h>
#endif

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define  DIALOGID_RadioButtons       1
#define DIALOGID_ModalRadioButtons  2
#define DIALOGID_HScales       3
#define DIALOGID_VScales       4
#define DIALOGID_ModalScales      5
#define DIALOGID_MLPushButtons       6
#define DIALOGID_Sash          7
#define DIALOGID_BGroup        8
#define DIALOGID_Colors        9
#define DIALOGID_MultiList     10
#define DIALOGID_2DArrows      11
#define DIALOGID_Alerts        12
#define DIALOGID_Tabs          13
#define DIALOGID_VSash		14
#define DIALOGID_Attributes     15

#define BGROUPID_EditIconTools   1
#define BGROUPID_Colors		2

#define ICONID_Eraser      1
#define ICONID_Line     2
#define ICONID_Brush    3
#define ICONID_Rect     4
#define ICONID_FillRect    5
#define ICONID_Circle      6
#define ICONID_FillCircle  7
#define ICONID_TabOptions  8
#define ICONID_RadioBtnOptions  9
#define ICONID_ScaleOptions  	10
#define ICONID_ComboBox	  	11
#define ICONID_BGRP_BLACK       12
#define ICONID_BGRP_DARKBLUE    13
#define ICONID_BGRP_DARKGREEN   14
#define ICONID_BGRP_TURQUOISE   15
#define ICONID_BGRP_MAROON      16
#define ICONID_BGRP_VIOLET      17
#define ICONID_BGRP_DARKYELLOW  18
#define ICONID_BGRP_DGREY       19
#define ICONID_BGRP_LGREY       20
#define ICONID_BGRP_WHITE       21
#define ICONID_BGRP_BLUE	22
#define ICONID_BGRP_GREEN       23
#define ICONID_BGRP_CYAN	24
#define ICONID_BGRP_RED		25
#define ICONID_BGRP_MAGENTA     26
#define ICONID_BGRP_YELLOW      27
#define ICONID_BGRP_MGREY       28

#define LISTBOXID_Sash1        1
#define LISTBOXID_Sash2        2
#define LISTBOXID_Multi        3
#define LISTBOXID_SelectionList      4

#define MENUBARID_NewItems 1
#define MENUBARID_MultiList   2

#define PDMID_Examples     1
#define PDMID_MultiListTests  2
#define  PDMID_MultiListModify   3
#define PDMID_MultiListSelect 4
#define  PDMID_MultiListPosition 5
#define PDMID_MultiListLocation  6

#define POPUPMENUID_MultiList 1

#define  OPTIONBTNID_NewItem     1
#define  OPTIONBTNID_BGroup      2
#define  OPTIONBTNID_SelectionMode  3
#define OPTIONBTNID_DefaultPushButton  4

#define PUSHBUTTONID_OModalRadioButtons   1
#define PUSHBUTTONID_OModalScales   2

#define PUSHBUTTONID_MLine11  3
#define PUSHBUTTONID_MLine12  4
#define PUSHBUTTONID_MLine13  5
#define PUSHBUTTONID_MLine21  6
#define PUSHBUTTONID_MLine22  7
#define PUSHBUTTONID_MLine23  8
#define PUSHBUTTONID_MLine31  9
#define PUSHBUTTONID_MLine32  10
#define PUSHBUTTONID_MLine33  11
#define PUSHBUTTONID_Test  22

#define PUSHBUTTONID_Alert1   23
#define PUSHBUTTONID_Alert2   24
#define PUSHBUTTONID_Alert3   25
#define PUSHBUTTONID_Alert4   26
#define PUSHBUTTONID_FreeTabPage  27


#define RBUTTONID_Option1  1
#define RBUTTONID_Option2  2
#define RBUTTONID_Option3  3
#define RBUTTONID_Option4  4
#define RBUTTONID_Option5  5
#define RBUTTONID_Option6  6

#define RBUTTONID_TabsTop     7
#define RBUTTONID_TabsBottom     8
#define RBUTTONID_TabsLeft    9
#define RBUTTONID_TabsRight   10

#define  RBLISTID_Options1       1
#define  RBLISTID_Options2       2
#define RBLISTID_TabPlacement   4

#define SASHID_Example     1
#define SASHID_VExample    2

#define SCALEID_Range1     1
#define SCALEID_Range2     2
#define SCALEID_Range3     3
#define SCALEID_Range4     4
#define SCALEID_Speed      5
#define SCALEID_TiedToSpeed   6

#define TEXTID_Range1      1
#define TEXTID_Range2      2
#define TEXTID_Colored     3

#define TOGGLEBTNID_Colored   1
#define TOGGLEBTNID_AddTabPage  2

#define SYNONYMID_OptionButton   1
#define SYNONYMID_RadioButtons1  2
#define SYNONYMID_Range1   3
#define SYNONYMID_Range2   4
#define SYNONYMID_Speed    5
#define SYNONYMID_BGroup   6
#define SYNONYMID_MultiList   7
#define SYNONYMID_ElementAttributes 8

#define MESSAGELISTID_NewItems   1

/*----------------------------------------------------------------------+
|									|
|   Message List Entry Ids						|
|									|
+----------------------------------------------------------------------*/
#define  MSGID_CommandTable   1
#define  MSGID_DialogBox      2
#define  MSGID_NewItems    3
#define  MSGID_VerySlow    4
#define  MSGID_Slow     5
#define  MSGID_Normal      6
#define  MSGID_Fast     7
#define  MSGID_VeryFast    8
#define  MSGID_Line     9
#define  MSGID_Right    10
#define  MSGID_Left     11
#define  MSGID_LeftAfter      12
#define  MSGID_LeftBefore  13
#define  MSGID_Critical    14
#define  MSGID_Warning     15
#define  MSGID_Information 16
#define  MSGID_Question    17
#define MSGID_RadioBtnChoice  18

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKID_RadioButtons   1
#define HOOKID_ScaleSpeed  2
#define HOOKID_Sash     3
#define HOOKID_ListBoxSash 4
#define HOOKID_Dialog_Sash 5
#define HOOKID_Dialog_Colors  6
#define HOOKID_ToggleBtnColor 7
#define HOOKID_ListBoxMulti   8
#define HOOKID_PDMMultiListTests    9
#define HOOKID_Dialog_2DArrows       10
#define HOOKID_OptionBtnDefaultPB   11
#define HOOKID_Dialog_MultiList      12
#define HOOKID_TabPageRadioBtns     13
#define HOOKID_TabPageToggleBtn     14
#define HOOKID_ComboBoxes     15
#define HOOKID_ComboStates    16
#define HOOKID_TabPageList    17
#define HOOKID_TabPage		18
#define HOOKID_SpinBox		19
#define HOOKID_TabsDialog	20
#define HOOKID_MLPushButton     21
#define HOOKID_VSash		22
#define HOOKID_ListBoxVSash     23
#define HOOKID_Dialog_VSash     24
#define HOOKID_ListBoxSelection 25
#define HOOKITEMID_NewItems_ElementLevel  26
#define HOOKITEMID_NewItems_ElementColor  27
#define HOOKITEMID_NewItems_ElementStyle  28
#define HOOKITEMID_NewItems_ElementWeight 29

/*----------------------------------------------------------------------+
|									|
|   Menu Search Ids							|
|									|
+----------------------------------------------------------------------*/
#define MSID_ResetList     1
#define MSID_RecreateList  2
#define MSID_AddTop     3
#define MSID_AddBottom     4
#define MSID_AddSelection  5
#define MSID_DeleteSelection  6
#define MSID_ToggleAddMode 7

#define MSID_SelectTop     1
#define MSID_SelectBottom  2
#define MSID_SelectEveryOther 3
#define MSID_SelectAll     4
#define MSID_DeselectTop   5
#define MSID_DeselectBottom   6
#define MSID_DeselectAll   7

#define MSID_Top1st     1
#define MSID_BottomLast    2
#define MSID_TopSelection  3

#define MSID_LocationTop   1
#define MSID_LocationBottom   2
#define MSID_LocationSelection   3

/*----------------------------------------------------------------------+
|									|
|   Selection Modes							|
|									|
+----------------------------------------------------------------------*/
#define SELECTIONMODE_BROWSE  0
#define SELECTIONMODE_SINGLE  1
#define SELECTIONMODE_MULTI   2
#define SELECTIONMODE_EXTENDED   3

/*----------------------------------------------------------------------+
|									|
|   Color Descriptor Array Indices					|
|									|
+----------------------------------------------------------------------*/
#define LCOLOR_BACKGROUND  0
#define LCOLOR_TOPSHADOW   1
#define LCOLOR_BOTTOMSHADOW   2
#define LCOLOR_SELECT      3
#define LCOLOR_FGTEXT      4
#define LCOLOR_BGTEXT      5
#define LCOLOR_DGN      6
#define  LCOLOR_LABELTEXT  7

#define NLOCAL_COLORS      8

/*----------------------------------------------------------------------+
|									|
|   Tab Page Lists							|
|									|
+----------------------------------------------------------------------*/
#define TPLISTID_ONE       1

#define TABPAGEID_OPTIONS     2
#define TABPAGEID_RADIOS      3
#define TABPAGEID_SCALES      4
#define TABPAGEID_BUTTONGRP   5
#define TABPAGEID_BOXES     6


#define DITEMLISTID_newPage      1

#define SPINBOXID_Test1		1

#define COMBOBOXID_Test1                            1
#define COMBOBOXID_Test2                            2           
#define COMBOBOXID_Test3                            3
#define COMBOBOXID_NewItems_ElementLevelWithHook    4
#define COMBOBOXID_NewItems_ElementColorWithHook    5
#define COMBOBOXID_NewItems_ElementStyleWithHook    6
#define COMBOBOXID_NewItems_ElementWeightWithHook   7

#define STRLISTID_ComboBox1	1
#define STRLISTID_ComboBoxStates	2

#define ICONCMDID_Test		1
/*#define CMD_NEWITEMS_CMDTEST    1*/

/*----------------------------------------------------------------------+
|									|
|   Radio Button Lists							|
|									|
+----------------------------------------------------------------------*/
#define RBLISTXID_ONE      1
#define RBLISTXID_TWO      2
#define RBLISTXID_TABPLACEMENT   3

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
#if !defined (resource)
typedef struct newitemsglobals
	 {
	 int      parameter1;
	 int      parameter2;
	 int      parameter3;
	 int      parameter4;

	 double  range1;
	 int      range2;
	 double  range3;
	 double  range4;

	 double  speed;

	 int      defaultPushButtonId;      /* id of default button */

	 RawItemHdr *currentSpeedScaleP; /* mouse is down in this speed scale */
	 int      minSashY;
	 int      maxSashY;
	 int      minSashX;
	 int      maxSashX;
	 int      selectionMode;

	 char  string[80];
	 DialogBox  *multiSelectDbP;
	 DialogItem *labelDiP;     /* label item in multi-select dialog */
	 DialogItem *multiListDiP;
	 DialogItem *selectionListDiP;
	 int      nColumns;     /* # of columns in multiSelect list */

	 int     tabPlacement;
	 int     tabPageToggle;
	 char	 comboText[80];
	 double  spinBoxValue;
	 double  stateIndex;
	 int     colorIndex;
                 
	 LevelID		currentLevel;     // COMBOBOXID_ElementLevel
         UInt32		currentColor;     // COMBOBOXID_ElementColor
         UInt32		currentWeight;    // COMBOBOXID_ElementWeight
	 long		currentStyle;     // COMBOBOXID_ElementStyle

	 /* Global color information */
	 BSIColorDescr *localColorsP[NLOCAL_COLORS];
	 BSIColorPalette *localPalP;  
	 } NewItemsGlobals;
#endif

#endif /* #if !defined  (__newitemsH__) */
