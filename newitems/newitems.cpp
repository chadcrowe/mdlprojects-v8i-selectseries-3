/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/newitems/newitems.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: newitems.cpp,v $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:40:48 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MDL example to show new items					|
|									|
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -						|
|									|
|	newItems_radioButtonHook - Radio button hook function		|
|	newItems_moveTiedToSpeedScale - Scale motion function		|
|	newItems_speedScaleHook - Scale hook function			|
|	newItems_changeDefaultPushButton - Change a default push button	|
|	newItems_defaultPBOptionBtnHook - Option button hook function	|
|	newItems_adjustSashDialogItems - Move sashes in dialog box	|
|	newItems_sashMotionFunc - Sash motion function			|
|	newItems_sashHook - Sash hook function				|
|	newItems_sashListBoxHook - Sash list box hook function		|
|	newItems_sashDialogHook - Sash dialog box hook function		|
|	newItems_colorsDialogHook - Colors dialog box hook function	|
|	newItems_colorsToggleBtnHook - Colors toggle button hook func	|
|	newItems_setSelectionList - Set up selection list entries	|
|	newItems_trackList - List box motion function			|
|	newItems_multiListInitStrList - Multi-list init function	|
|	newItems_multiListInitDisabled - Multi-list disable function	|
|	newItems_multiListBoxHook - Mulit-list list box hook		|
|	newItems_maintainMultiListModifyMenu,				|
|	    newItems_maintainMultiListSelectMenu,			|
|	    newItems_maintainMultiListPositionMenu,			|
|	    newItems_maintainMultiListLocationMenu - Misc multi-list	|
|		    functions						|
|	newItems_multiListInsertItem - Add item to list			|
|	newItems_doMultiListModify - Handle multi-list modify		|
|	newItems_doMultiListSelect - Handle multi-list select		|
|	newItems_doMultiListPosition - Handle multi-list position	|
|	newItems_doMultiListLocation - Handle multi-list location	|
|	newItems_multiListTestsMenusHook - Multi-list hook function	|
|	newItems_setupPopupMenu - Init popup menus			|
|	newItems_multiListDialogHook - Multi-list dialog box hook	|
|	newItems_draw2DArrows -  2D arrow draw function			|
|	newItems_2DArrowsDialogHook - 2D arrow dialog hook		|
|	newItems_open - Open command function				|
|	newItems_openModal - Open modal dialog				|
|	newItems_exit - Exit application				|
|	newItems_rscSprintf - sprintf from resource file		|
|	newItems_errorPrint - Print error message			|
|	newItems_unloadFunction - Unload function			|
|	newItems_scaleColor - Scale color determination			|
|	newItems_handleRemap - Color table asynch function		|
|	main - Main entry point						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>	    /* MDL Library funcs structures & constants */
#include    <dlogitem.h>    /* Dialog Box Manager structures & constants */
#include    <cexpr.h>	    /* C Expression structures & constants */
#include    <userfnc.h>
#include    <cmdlist.h>	    /* MicroStation command numbers */
#include    <colrname.h>
#include    <stdarg.h>
#include    <dlogids.h>
#include    <stdlib.h>
#include    <string.h>
#include    <stdio.h>
#include    <leveltable.h>
#include    <filtertable.h>

#include    "newitems.h"    /* basic dialog box example constants & structs */
#include    "newicmd.h"	    /* basic dialog box command numbers */

#include    <dlogman.fdf>   /* dialog box manager function prototypes */
#include    <listmodel.fdf>
#include    <mscolor.fdf>   /* color manager function prototypes */
#include    <msparse.fdf>
#include    <mscexpr.fdf>
#include    <msdialog.fdf>
#include    <msrsrc.fdf>
#include    <mssystem.fdf>
#include    <msoutput.fdf>
#include    <msritem.fdf>
#include    <leveltable.fdf>
#include    <dlmsys.fdf>
#include    <msvar.fdf>

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
#define	NMULTILIST_LINES	30

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
static NewItemsGlobals	niG = {1, 0};
static ULong radioButtonULong;

/*----------------------------------------------------------------------+
|									|
|   Utility routines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Name	newItems_rscSprintf					|
|                                                                       |
|   Author	BSI                                         3/93        |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_rscSprintf
(
char   *stringP,	    /* <=  Result of sprintf from resource	*/
int	messageNumber,	    /*  => Index into msg list for format str	*/
...			    /*  => Any other optional arguments		*/
)
    {
    va_list ap;
    char    tempStr[1024];

    va_start (ap, messageNumber);

    *stringP = tempStr[0] = '\0';
    mdlResource_loadFromStringList (tempStr, NULL, MESSAGELISTID_NewItems,
				    messageNumber);
    vsprintf (stringP, tempStr, ap);

    va_end (ap);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_errorPrint -- print an error message into	|
|			      Dialog Box Manager Messages dialog box	|
|                                                                       |
| author        BSI                                     08/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_errorPrint
(
int errorNumber		    /* => number of error to print */
)
    {
    char    errorMsg[80];

    if (mdlResource_loadFromStringList (errorMsg, NULL,
					MESSAGELISTID_NewItems, errorNumber))
	return;	/* unable to find message with number "errorNumber" */

    mdlDialog_dmsgsPrint (errorMsg);
    }
/*----------------------------------------------------------------------+
|									|
| name		newItems_extentSet					|
|									|
| author	BSI					09/96		|
|									|
+----------------------------------------------------------------------*/
Private void	newItems_extentSet
(
DialogBox  *dbP
)
    {
    int		height = 0, width = 0, numItems, i;
    DialogItem *diP;

    numItems = mdlDialog_itemsGetNumberOf (dbP);
    for (i = 0; i < numItems; i++)
	if (diP = mdlDialog_itemGetByIndex (dbP, i))
	    if (!diP->attributes.hidden)
		{
		if ((diP->rect.corner.x > width) &&
		    (diP->type != RTYPE_MenuBar))
		    width = diP->rect.corner.x;
		if (diP->rect.corner.y > height)
		    height = diP->rect.corner.y;

		if (diP->type == RTYPE_TabPageList ||
		    diP->type == RTYPE_MultilineText)
		    {
		    if (diP->rawItemP->itemRect.corner.x > width)
			width = diP->rawItemP->itemRect.corner.x;
		    if (diP->rawItemP->itemRect.corner.y > height)
			height = diP->rawItemP->itemRect.corner.y;
		    }
		}
    /* Resize the dialog box based upon current item */
    width += 10;
    height += 10;

    mdlWindow_extentSet ((GuiWindowP) dbP, width, height);
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Hook Functions							|
|                                                                       |
+----------------------------------------------------------------------*/ 
/*----------------------------------------------------------------------+
|									|
| name		newItems_radioButtonHook				|
|									|
| author	BSI     				08/92		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_radioButtonHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    break;

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    int     chosenRadioButton;
	    char    msgtemplate[25];
	    char    buffer[40];
	    DialogItem	   *spinDiP;

	    mdlResource_loadFromStringList(msgtemplate, NULL,
			    MESSAGELISTID_NewItems, MSGID_RadioBtnChoice);

	    spinDiP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_SpinBox, 
						    SPINBOXID_Test1, 0);

	    chosenRadioButton = niG.parameter1;

	    switch (niG.parameter1 /*diP->id*/)
	    	{
		case RBUTTONID_Option1:
		    {
		    if (spinDiP)
		    	{
			int	    mxsz;
			double      mn, mx, inc;
			char	    formatToDisplay[20];

			mxsz = 10;
			mn = 0;
			mx = 500;
			inc = 10;
			strcpy (formatToDisplay, "%.2lf cm");

			mdlDialog_spinBoxSetInfo (NULL,
			    NULL,
			    &mxsz,
			    formatToDisplay,
			    NULL,
			    &mn,
			    &mx,
			    &inc,
			    NULL,
			    NULL,
			    FALSE,
			    spinDiP->rawItemP);

			mdlDialog_itemSetValue (NULL, NULL, NULL,
			    "100", dimP->db, spinDiP->itemIndex);
		    	}

		    chosenRadioButton = niG.parameter1;
		    break;
		    }

		case RBUTTONID_Option2:
		    {
		    if (spinDiP)
		    	{
				char	label[50];
				sprintf (label, "Label w/ %c symbol:", 176);
			mdlDialog_itemSetLabel (dimP->db, spinDiP->itemIndex,
						label);
			}

		    chosenRadioButton = niG.parameter1;
		    break;
		    }

		case RBUTTONID_Option3:
		    {
		    if (spinDiP)
		    	{
			ValueUnion  value;
			value.doubleFormat = 90;
			mdlDialog_itemSetValue (NULL, FMT_DPFP, &value,
			    NULL, dimP->db, spinDiP->itemIndex);
			}

		    break;
		    }

		default:
		    {
		    chosenRadioButton = niG.parameter4;
		    break;
		    }
	    	}

	    sprintf(buffer, msgtemplate, chosenRadioButton);
	    mdlOutput_status (buffer);
    
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_tabPageRadioBtnHook				|
|									|
| author	BSI     				09/96		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_tabPageRadioBtnHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    UInt32		attributes;
    int			numPages, firstPage, lastPage;
    int			oldPlacement, newPlacement;
    int			oldX, oldY, newX, newY;
    DialogItem		*listDiP;

    newPlacement = 1; // initialize to top.
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    break;

	case DITEM_MESSAGE_STATECHANGED:
	    {

	    dimP->msgUnderstood = TRUE;
	    oldPlacement = 1;
	    oldX = oldY = 0;

	    listDiP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_TabPageList, 
						    TPLISTID_ONE, 0);
	    if (listDiP)
	    	{
		if (mdlDialog_tabPageListGetInfo (&numPages, &firstPage, &lastPage, 
						    NULL, NULL, &attributes, 
						    listDiP->rawItemP) == 0)
		    {
		    if (attributes & TABATTR_TABSBOTTOM)
		    	{
			oldPlacement = 2;
			oldY = -30;
		    	}
		    else if (attributes & TABATTR_TABSLEFT)
		    	{
			oldPlacement = 3;
			oldX = 120;
			oldY = -30;
		    	}
		    else if (attributes & TABATTR_TABSRIGHT)
		    	{
			oldPlacement = 4;
			oldY = -30;
		    	}
		    }

		newX = newY = 0;
		attributes &= !(TABATTR_TABSBOTTOM);
		attributes &= !(TABATTR_TABSLEFT);
		attributes &= !(TABATTR_TABSRIGHT);
		attributes &= !(TABATTR_TABSFIXEDWIDTH);
		attributes &= !(TABATTR_TABSFITPAGEWIDTH);

		switch (niG.tabPlacement)
		    {
		    case RBUTTONID_TabsTop:
			{
			newPlacement = 1;
			attributes |= TABATTR_MULTIROW;
			break;
			}

		    case RBUTTONID_TabsBottom:
			{
			newPlacement = 2;
			newY = -30;
			attributes |= TABATTR_TABSBOTTOM /*| TABATTR_TABSFITPAGEWIDTH
							   | TABATTR_TABSTALL*/;
			break;
			}

		    case RBUTTONID_TabsLeft:
			{
			newPlacement = 3;
			newX = 120;
			newY = -30;
			attributes |= TABATTR_TABSLEFT | TABATTR_TABSFIXEDWIDTH 
							| TABATTR_LABELLEFTJUSTIFY;
			break;
			}

		    case RBUTTONID_TabsRight:
			{
			newPlacement = 4;
			newY = -30;
			attributes |= TABATTR_TABSRIGHT | TABATTR_TABSFIXEDWIDTH | TABATTR_LABELRIGHTJUSTIFY
							/*| TABATTR_TABSSHORT*/;
			break;
			}

		    default:
			{
			dimP->msgUnderstood = FALSE;
			break;
			}
		    }

		if (dimP->msgUnderstood &&
		    newPlacement != oldPlacement)
		    {
//		    int 	iItem;
		    Point2d	fromPt;
		    Point2d	toPt;

		    /* Reset the attributes for the selected tab placement */
		    mdlDialog_tabPageListSetInfo (listDiP->rawItemP, NULL, NULL, &attributes, FALSE);

		    /* Loop thru the pages to move the children on each page */
		    fromPt.x = oldX;
		    fromPt.y = oldY;
		    toPt.x   = newX;
		    toPt.y   = newY;
/*		    for (iItem = firstPage; iItem <= lastPage; iItem++)
			{
			pageDiP = mdlDialog_tabPageListGetPageByIndex (listDiP->rawItemP, iItem);
			if (pageDiP)
			    mdlDialog_tabPageMoveItems (pageDiP->rawItemP, &fromPt, &toPt, TRUE);
			}
*/
		    /* Reset the extents for the dialog box */
		    newItems_extentSet (dimP->db);
		    }
		}
	    else
		dimP->msgUnderstood = FALSE;

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_comboBoxHook   				|
|									|
| author	BSI     				10/96		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_comboBoxHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    RawItemHdr		*riP;
    ListModel		*pListModel;

    dimP->msgUnderstood = TRUE;

    riP = dimP->dialogItemP->rawItemP;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int			iTest;
	    char		buffer[20];
	    char		label[50];

	    sprintf (label, "Label w/ %c symbol:", 176);
	    mdlDialog_itemSetLabel (dimP->db, dimP->itemIndex, label);

	    pListModel = mdlListModel_create (1);
	    if (pListModel)
		{
		for (iTest=0; iTest<=20; iTest++)
		    {
		    sprintf (buffer, "%d", iTest*5);
		    mdlListModel_insertString (pListModel, buffer, iTest);
		    }

		mdlDialog_comboBoxSetListModelP (riP, pListModel);
		}

	    break;
	    }

	case DITEM_MESSAGE_GETSTATE:
	    {
	    dimP->u.value.formatType = FMT_DPFP;
	    dimP->u.value.value.doubleFormat = 50;
	    dimP->u.value.hookHandled = TRUE;

	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    char	    valueStr[41];

	    mdlDialog_itemGetValue (NULL, NULL, valueStr, dimP->db, dimP->itemIndex, 40);
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    if  (NULL != (pListModel = mdlDialog_comboBoxGetListModelP (riP)))
	    	{
		mdlListModel_destroy (pListModel, TRUE);
	    	}
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_comboBoxStatesHook   				|
|									|
| author	BSI     				11/96		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_comboBoxStatesHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    RawItemHdr    	*riP;

    dimP->msgUnderstood = TRUE;
    riP = dimP->dialogItemP->rawItemP;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    niG.stateIndex = 74;

	    break;
	    }

	case DITEM_MESSAGE_FOCUSIN:
	    {
	    if (riP && dimP->u.focusIn.focusOutType == FOCUSOUT_KEYSWITCH)
		mdlDialog_comboBoxSetPopupState (riP, TRUE);
	    
	    break;
	    }
	case DITEM_MESSAGE_STATECHANGED:
	    {
	    int		    format;
	    ValueUnion      value;
	    char	    valueStr[41];

	    format = FMT_LONG;
	    mdlDialog_itemGetValue (&format, &value, valueStr, dimP->db, dimP->itemIndex, 40);

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_spinBoxHook       				|
|									|
| author	BSI     				02/97		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_spinBoxHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {

	    break;
	    }

	case DITEM_MESSAGE_GETSTATE:
	    {
	    dimP->u.value.formatType = FMT_DPFP;
	    dimP->u.value.value.doubleFormat = 2;
	    dimP->u.value.hookHandled = TRUE;

	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    int		    format;
	    ValueUnion      value;
	    char	    valueStr[41];

	    format = FMT_DPFP;
	    mdlDialog_itemGetValue (&format, &value, valueStr, dimP->db, dimP->itemIndex, 40);

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }

    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_moveTiedToSpeedScale				|
|                                                                       |
| author        BSI					08/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_moveTiedToSpeedScale
(
Scale_MotionFuncArg *mfaP
)
    {
    if (niG.currentSpeedScaleP)
	{
	DialogItem  *tiedToSpeedScaleDiP =
	    (DialogItem *) niG.currentSpeedScaleP->userDataP;

	if (tiedToSpeedScaleDiP)
	    {
	    ValueUnion  value;

	    value.doubleFormat = mfaP->value;
	    mdlDialog_itemSetValue (NULL, FMT_DPFP, &value, NULL,
				(DialogBox*)tiedToSpeedScaleDiP->rawItemP->ownerDialogP,
				tiedToSpeedScaleDiP->itemIndex);
	    }
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_speedScaleHook					|
|									|
| author	BSI     				08/92		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_speedScaleHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_ALLCREATED:
	    {
	    RawItemHdr	*scaleP = dimP->dialogItemP->rawItemP;

	    /* store a ptr to the "tiedToSpeed" scale item for later use by
	       motionFunc */
	    scaleP->userDataP =	mdlDialog_itemGetByTypeAndId (dimP->db,
						    RTYPE_Scale,
						    SCALEID_TiedToSpeed, 0);
	    break;
	    }

	case DITEM_MESSAGE_BUTTON:
	    if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
		{
	        niG.currentSpeedScaleP = dimP->dialogItemP->rawItemP;
		dimP->u.button.motionFunc = (void(*)(void))newItems_moveTiedToSpeedScale;
		}
	    else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		{
	        niG.currentSpeedScaleP = NULL;
		}

	    break;

	case RTYPE_Scale:
	    {
	    char    msg[20];

	    if (dimP->auxMessageType == SCALE_AUXMESSAGE_FORMATVALUE)
		{
		Scale_FormatValue   *formatValueP =
		    (Scale_FormatValue *) dimP->auxInfoP;

		if (formatValueP->value < 15.0)
		    mdlResource_loadFromStringList (msg, NULL,
						    MESSAGELISTID_NewItems,
						    MSGID_VerySlow);
		else if (formatValueP->value < 30.0)
		    mdlResource_loadFromStringList (msg, NULL,
						    MESSAGELISTID_NewItems,
						    MSGID_Slow);
		else if (formatValueP->value < 70.0)
		    mdlResource_loadFromStringList (msg, NULL,
						    MESSAGELISTID_NewItems,
						    MSGID_Normal);
		else if (formatValueP->value < 85.0)
		    mdlResource_loadFromStringList (msg, NULL,
						    MESSAGELISTID_NewItems,
						    MSGID_Fast);
		else
		    mdlResource_loadFromStringList (msg, NULL,
						    MESSAGELISTID_NewItems,
						    MSGID_VeryFast);
		strcpy (formatValueP->valueStrP, msg);

		formatValueP->hookHandled = TRUE;
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
|   Multi-Line PushButton Dialog Hooks					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_changeDefaultPushButton			|
|                                                                       |
| author        BSI					01/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_changeDefaultPushButton
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    /* see if there is not supposed to be any default pushButton */
    if (niG.defaultPushButtonId == 0)
	{
	int currentDefaultItem = mdlDialog_itemGetDefault (dimP->db);

	if (currentDefaultItem >= 0)
	    {
	    DialogItem	*oldDefaultDiP;
	    RawItemHdr	*oldRihP;

	    oldDefaultDiP = mdlDialog_itemGetByIndex (dimP->db,
						      currentDefaultItem);
	    if (oldDefaultDiP && (oldDefaultDiP->type == RTYPE_PushButton) &&
		(oldRihP = oldDefaultDiP->rawItemP))
		{
		mdlDialog_pushButtonSetDefault (oldRihP, FALSE);
		}
	    }
	}
    else
	{
	DialogItem	*diP;

	diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_PushButton,
					    niG.defaultPushButtonId, 0);
	if (diP && diP->rawItemP && diP->type == RTYPE_PushButton)
	    mdlDialog_pushButtonSetDefault (diP->rawItemP, TRUE);
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_defaultPBOptionBtnHook				|
|                                                                       |
| author        BSI					01/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_defaultPBOptionBtnHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
	    if (dimP->u.stateChanged.reallyChanged)
		{
		newItems_changeDefaultPushButton (dimP);
		}
	    break;

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
|   Sash Example Dialog Routines					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_adjustSashDialogItems				|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_adjustSashDialogItems
(
DialogBox   *db,
int	     deltaWidth
)
    {
    DialogItem	*diP;
    RawItemHdr	*sashP;
    RawItemHdr	*topListP;
    RawItemHdr	*bottomListP;
    int		 size, colWidth, rowHeight;
    UInt32        nRows;
    Point2d	 pt;
    BSIRect	 contentRect;
    int		 fontHeight = mdlDialog_fontGetCurHeight (db);

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Sash, SASHID_Example, 0);
    if (!diP)
	return;
    sashP = diP->rawItemP;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ListBox, LISTBOXID_Sash1, 0);
    if (!diP)
	return;
    topListP = diP->rawItemP;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ListBox, LISTBOXID_Sash2, 0);
    if (!diP)
	return;
    bottomListP = diP->rawItemP;

    mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) db);
    mdlWindow_rectClear ((GuiWindowP) db, &contentRect, NULL);

    /* adjust size of column 0 so listBox is horizontally resized */
    mdlDialog_listBoxGetColInfo (&colWidth, NULL, NULL, NULL, topListP, 0);
    if (deltaWidth != 0)
	{
	int listWidth;	    /* size list including scrollBar & bevel */
	int colWidthPixels; /* width of column in pixels */
	int adjustWidth;    /* subtracted from listWidth gives width col in pixels */

	listWidth      = mdlDialog_rectWidth (&topListP->diP->rect);
	colWidthPixels = mdlDialog_toPixels (db, colWidth);
	adjustWidth    = listWidth - colWidthPixels;
	colWidthPixels = (sashP->itemRect.corner.x - fontHeight/2) - adjustWidth;

	colWidth = mdlDialog_toDCoord (db, colWidthPixels);
	mdlDialog_listBoxSetColInfo (&colWidth, NULL, NULL, NULL, topListP,
				     0, FALSE);
	}

    /* adjust horizontal positions of lists so scrollBars line up with sash
       (see Visual Design with OSF/Motif, Kobara, pages 93-94) */
    pt.y = -topListP->itemRect.origin.y;   /* neg num means pixels specified */
    pt.x = -(topListP->itemRect.origin.x +
	    (sashP->itemRect.corner.x - topListP->diP->rect.corner.x));
    mdlDialog_itemMove (db, topListP->diP->itemIndex, &pt, FALSE);

    /* fix vertical size of top List */
    size  = sashP->itemRect.origin.y - topListP->itemRect.origin.y -
	    (fontHeight*3/4);
    mdlDialog_listBoxGetHeights (&rowHeight, NULL, NULL, topListP);
    nRows = size / rowHeight;
    mdlDialog_listBoxSetInfo (NULL, &nRows, NULL, FALSE, topListP);
    mdlDialog_listBoxNRowsChangedRedraw (topListP, FALSE);
    mdlDialog_itemDrawEx (db, topListP->diP->itemIndex, FALSE);

    /* adjust size of column 0 so listBox is horizontally resized */
    if (deltaWidth != 0)
	{
	mdlDialog_listBoxSetColInfo (&colWidth, NULL, NULL, NULL, bottomListP,
				     0, FALSE);
	}

    /* Move bottom list; neg num means pixels specified */
    pt.y = -(sashP->itemRect.corner.y + fontHeight/2);
    pt.x = -(bottomListP->itemRect.origin.x +
	    (sashP->itemRect.corner.x - bottomListP->diP->rect.corner.x));
    mdlDialog_itemMove (db, bottomListP->diP->itemIndex, &pt, FALSE);

    /* fix size of bottom List */
    size  = contentRect.corner.y + pt.y - fontHeight*3/4;
    mdlDialog_listBoxGetHeights (&rowHeight, NULL, NULL, bottomListP);
    nRows = size / rowHeight;
    mdlDialog_listBoxSetInfo (NULL, &nRows, NULL, FALSE, bottomListP);
    mdlDialog_listBoxNRowsChangedRedraw (bottomListP, FALSE);
    mdlDialog_itemDrawEx (db, bottomListP->diP->itemIndex, FALSE);

    mdlDialog_itemDrawEx (db, sashP->diP->itemIndex, FALSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_sashMotionFunc					|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_sashMotionFunc	/* called each time user moves mouse
					   while dragging sash */
(
MotionFuncArg	*mfaP	/* <> mfaP->pt.y = where sash upper left corner will go */
			/* => mfaP->pt.x = where cursor y position is */
			/* => mfaP->dragging = TRUE if cursor is within window */
)
    {
    if (mfaP->pt.y < niG.minSashY)
	mfaP->pt.y = niG.minSashY;

    if (mfaP->pt.y > niG.maxSashY)
	mfaP->pt.y = niG.maxSashY;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_sashHook					|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_sashHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_BUTTON:
	    {
	    if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
		{
		BSIRect	 contentRect;
		int	 fontHeight = mdlDialog_fontGetCurHeight (dimP->db);

		dimP->u.button.motionFunc = (void(*)(void))newItems_sashMotionFunc;

		mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) dimP->db);
		niG.minSashY = 5*fontHeight;
		niG.maxSashY =
		    mdlDialog_rectHeight (&contentRect) -
		    5*fontHeight;
		}
	    else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		{
		Sash_ButtonUpAuxInfo *buaiP =
		    (Sash_ButtonUpAuxInfo *) dimP->auxInfoP;

		/* use buaiP->newYPos to determine where upperLeft corner
		   of sash beveled rect will go.  This message is sent after
		   sash has been erased from old position & moved, but before
		   it has been drawn */
		if (buaiP->newYPos != buaiP->oldYPos)
		    newItems_adjustSashDialogItems (dimP->db, 0);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_sashListBoxHook				|
|                                                                       |
| author        BSI					08/92		|
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_sashListBoxHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int		 iString;
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    ListModel   *pListModel;
	    char	 formatStr[20];

	    /* Create ListModel with 1 column */
	    rihP->userDataP = pListModel = mdlListModel_create (1);

	    if (!pListModel)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    mdlResource_loadFromStringList (formatStr, NULL,
					    MESSAGELISTID_NewItems,
					    MSGID_Line);
	    for (iString=0; iString<15; iString++)
		{
		char    buffer[80];

		sprintf (buffer, formatStr, iString);
		mdlListModel_insertString (pListModel, buffer, iString);
		}

	    mdlDialog_listBoxSetListModelP (rihP, pListModel, 1);
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;

	    if (rihP->userDataP)
		{
		mdlListModel_destroy ((ListModel *) rihP->userDataP, TRUE);
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_sashDialogHook					|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void	newItems_sashDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    dmP->u.create.interests.resizes   = TRUE;
	    dmP->u.create.interests.mouses    = TRUE;
	    dmP->u.create.interests.nonDataPoints = TRUE;
	    break;
	    }

	case DIALOG_MESSAGE_INIT:
	    {
	    DialogItem	*diP;
	    BSIRect     contentRect;

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_Sash,
						SASHID_Example, 0);
	    if (!diP)
		break;

	    mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) dmP->db);

	    newItems_adjustSashDialogItems (dmP->db, contentRect.corner.x);
	    break;
	    }

	case DIALOG_MESSAGE_RESIZE:
	    {
	    int	    newWidth, oldWidth;

	    if (dmP->u.resize.whichCorners == CORNER_ALL)
		break;
	    if (1 == mdlDialog_rectHeight (&dmP->u.resize.newContent))
		break;	    /* dialog was minimized */

	    newWidth = mdlDialog_rectWidth (&dmP->u.resize.newContent);
	    oldWidth = mdlDialog_rectWidth (&dmP->u.resize.oldContent);

	    newItems_adjustSashDialogItems (dmP->db, newWidth-oldWidth);
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
|   Vertical Sash Example Dialog Routines				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_adjustVSashDialogItems				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_adjustVSashDialogItems
(
DialogBox   *db
)
    {
    DialogItem	*diP;
    RawItemHdr	*sashP;
    RawItemHdr	*leftListP;
    RawItemHdr	*rightListP;
    int		 width, height;
    BSIRect	 contentRect;
    Sextent      sextent;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Sash, SASHID_VExample, 0);
    if (!diP)
	return;
    sashP = diP->rawItemP;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ListBox, LISTBOXID_Sash1, 0);
    if (!diP)
	return;
    leftListP = diP->rawItemP;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ListBox, LISTBOXID_Sash2, 0);
    if (!diP)
	return;
    rightListP = diP->rawItemP;

    mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) db);

    height  = contentRect.corner.y - contentRect.origin.y + 1;

    /* Set left listbox width */
    width = sashP->diP->rect.origin.x - 1;
    sextent.origin.x = 0;
    sextent.origin.y = 0;
    sextent.height   = -1*(short)height;
    sextent.width    = -1*(short)width;
    mdlDialog_itemSetExtent (db, leftListP->diP->itemIndex, &sextent, FALSE);

    /* Set right listbox width */
    width = contentRect.corner.x - sashP->diP->rect.corner.x;
    sextent.origin.x = -1*((short)sashP->diP->rect.corner.x+1);
    sextent.origin.y = 0;
    sextent.height   = -1*(short)height;
    sextent.width    = -1*(short)width;
    mdlDialog_itemSetExtent (db, rightListP->diP->itemIndex, &sextent, FALSE);

    mdlDialog_itemDrawEx (db, leftListP->diP->itemIndex, FALSE);
    mdlDialog_itemDrawEx (db, sashP->diP->itemIndex, FALSE);
    mdlDialog_itemDrawEx (db, rightListP->diP->itemIndex, FALSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_vSashMotionFunc				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_vSashMotionFunc	/* called each time user moves mouse
					   while dragging sash */
(
MotionFuncArg	*mfaP	/* <> mfaP->pt.x = where sash upper left corner will go */
			/* => mfaP->pt.y = where cursor x position is */
			/* => mfaP->dragging = TRUE if cursor is within window */
)
    {
    if (mfaP->pt.x < niG.minSashX)
	mfaP->pt.x = niG.minSashX;

    if (mfaP->pt.x > niG.maxSashX)
	mfaP->pt.x = niG.maxSashX;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_vSashHook					|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_vSashHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_BUTTON:
	    {
	    if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
		{
		BSIRect	 contentRect;
		int	 fontHeight = mdlDialog_fontGetCurHeight (dimP->db);

		dimP->u.button.motionFunc = (void(*)(void))newItems_vSashMotionFunc;

		mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) dimP->db);
		niG.minSashX = 5*fontHeight;
		niG.maxSashX =
		    mdlDialog_rectWidth (&contentRect) -
		    5*fontHeight;
		}
	    else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		{
		Sash_ButtonUpAuxInfo *buaiP =
		    (Sash_ButtonUpAuxInfo *) dimP->auxInfoP;

		/* use buaiP->newXPos to determine where upperLeft corner
		   of sash beveled rect will go.  This message is sent after
		   sash has been erased from old position & moved, but before
		   it has been drawn */
		if (buaiP->newXPos != buaiP->oldXPos)
		    newItems_adjustVSashDialogItems (dimP->db);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_vSashDialogHook				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void	newItems_vSashDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    dmP->u.create.interests.resizes   = TRUE;
	    dmP->u.create.interests.mouses    = TRUE;
	    dmP->u.create.interests.nonDataPoints = TRUE;
	    break;
	    }

	case DIALOG_MESSAGE_INIT:
	    {
	    DialogItem	*diP;

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_Sash,
						SASHID_VExample, 0);
	    if (!diP)
		break;

	    newItems_adjustVSashDialogItems (dmP->db);
	    break;
	    }

	case DIALOG_MESSAGE_RESIZE:
	    {
	    if (dmP->u.resize.whichCorners == CORNER_ALL)
		break;
	    if (1 == mdlDialog_rectHeight (&dmP->u.resize.newContent))
		break;	    /* dialog was minimized */

	    newItems_adjustVSashDialogItems (dmP->db);
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_colorsDialogHook				|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void	newItems_colorsDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    dmP->u.create.interests.mouses    = TRUE;
	    dmP->u.create.interests.keystrokes = TRUE;
	    break;
	    }

	case DIALOG_MESSAGE_INIT:
	    {
	    DialogItem	*diP;

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_ToggleButton,
						TOGGLEBTNID_Colored, 0);
	    if (diP)
		{
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_BACKGROUND,
					 niG.localColorsP[LCOLOR_BACKGROUND]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_FOREGROUND,
					 niG.localColorsP[LCOLOR_LABELTEXT]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_TOPSHADOW,
					 niG.localColorsP[LCOLOR_TOPSHADOW]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_BOTTOMSHADOW,
					 niG.localColorsP[LCOLOR_BOTTOMSHADOW]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_SELECT,
					 niG.localColorsP[LCOLOR_SELECT]);
		}

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_RadioButton,
						RBUTTONID_Option1, 0);
	    if (diP)
		{
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_BACKGROUND,
					 niG.localColorsP[LCOLOR_BACKGROUND]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_TOPSHADOW,
					 niG.localColorsP[LCOLOR_TOPSHADOW]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_BOTTOMSHADOW,
					 niG.localColorsP[LCOLOR_BOTTOMSHADOW]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_SELECT,
					 niG.localColorsP[LCOLOR_SELECT]);
		}

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_ListBox,
						LISTBOXID_Sash1, 0);
	    if (diP)
		{
		ListModel   *pListModel;
		ListRow     *pRow;
		ListCell    *pCell;
		int	    iCount,i;

		
		pListModel = mdlDialog_listBoxGetListModelP (diP->rawItemP);
		iCount = mdlListModel_getRowCount (pListModel);
		for (i=0;i<iCount ;i++    )
		    {
		    pRow = mdlListModel_getRowAtIndex (pListModel,i);
		    pCell = mdlListRow_getCellAtIndex (pRow,1);
		    mdlListCell_setBgColorDescr (pCell,niG.localColorsP[LCOLOR_BOTTOMSHADOW]);
		    mdlListCell_setColorDescr (pCell,niG.localColorsP[LCOLOR_DGN]);
		    }
		}
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_colorsToggleBtnHook				|
|									|
| author	BSI     				10/92		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_colorsToggleBtnHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    DialogItem		*diP;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
	    {
	    if (!dimP->u.stateChanged.reallyChanged)
		break;

	    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ListBox,
		    LISTBOXID_Sash1, 0);
	    if (diP)
		{
		if (niG.parameter3)
		    {
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_BACKGROUND,
					     niG.localColorsP[LCOLOR_BACKGROUND]);
		    mdlDialog_itemDraw (dimP->db, diP->itemIndex);
		    }
		else
		    {
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_BACKGROUND,
					     NULL);
		    mdlDialog_itemDraw (dimP->db, diP->itemIndex);
		    }
		}

	    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text,
		    TEXTID_Colored, 0);
	    if (diP)
		{
		if (niG.parameter3)
		    {
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_BACKGROUND,
					     niG.localColorsP[LCOLOR_BGTEXT]);
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_FOREGROUND,
					     niG.localColorsP[LCOLOR_FGTEXT]);
		    mdlDialog_itemDraw (dimP->db, diP->itemIndex);
		    }
		else
		    {
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_BACKGROUND,
					     NULL);
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_FOREGROUND,
					     NULL);
		    mdlDialog_itemDraw (dimP->db, diP->itemIndex);
		    }
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_tabPageToggleBtnHook				|
|									|
| author	BSI     				09/96		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_tabPageToggleBtnHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    DialogItemListRsc   *rscP;
    DialogItem		*diP, *listDiP;
    ValueUnion		value;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
	    {
	    if (!dimP->u.stateChanged.reallyChanged)
		break;

	    if (niG.tabPageToggle)
		{
		/* Load page test */
		if ((rscP = (DialogItemListRsc *)mdlResource_load (NULL, RTYPE_DialogItemList,
						      DITEMLISTID_newPage)) != NULL)
		    {
		    listDiP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_TabPageList,
							TPLISTID_ONE, 0);
		    if (listDiP)
		    	{
			mdlDialog_tabPageListLoadPages (listDiP->rawItemP, rscP, NULL, NULL, 0);

			value.sLongFormat = 1;
			mdlDialog_itemSetValue (NULL, FMT_LONG, &value, NULL, dimP->db, 
						listDiP->itemIndex);
			}
		    }
		}
	    else
		{
		/* Free page test */
		listDiP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_TabPageList,
						    TPLISTID_ONE, 0);
		if (listDiP)
		    {
		    diP = mdlDialog_tabPageListGetPageById (listDiP->rawItemP,
						    TABPAGEID_RADIOS, 0);
		    if (diP)
		    	{
			mdlDialog_tabPageFreeItems (diP->rawItemP,
						    0, -1);
			mdlDialog_tabPageListFreePages (listDiP->rawItemP,
						    diP->itemIndex, diP->itemIndex);
			}
		    }
		}

	    /* Reset the extents for the dialog box */
	    newItems_extentSet (dimP->db);

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_tabPageListHook				|
|									|
| author	BSI     				12/96		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_tabPageListHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    DialogItem		*listDiP;
    ValueUnion		value;
    int			format;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
	    {
	    if (!dimP->u.stateChanged.reallyChanged)
		break;

	    listDiP = dimP->dialogItemP;
	    
	    mdlDialog_itemGetValue (&format, &value, NULL, dimP->db,
					listDiP->itemIndex, 0);

	    break;
	    }

	case DITEM_MESSAGE_GETSTATE:
		/* Start on the 4th tab page */
		dimP->u.value.value.sLongFormat = 3;
		dimP->u.value.hookHandled = TRUE;
	            break;


	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_tabPageHook    				|
|									|
| author	BSI     				12/96		|
|									|
+----------------------------------------------------------------------*/
Private void newItems_tabPageHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    DialogItem		*diP;
    ValueUnion		value;
    int			format;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGED:
	    {
	    if (!dimP->u.stateChanged.reallyChanged)
		break;

	    diP = dimP->dialogItemP;
	    
	    mdlDialog_itemGetValue (&format, &value, NULL, dimP->db, diP->itemIndex, 0);

            if (diP->id == TABPAGEID_OPTIONS && TABPAGEID_OPTIONS)	    
                {
		DialogItem      *pPBDiP;
		
		pPBDiP = mdlDialog_tabPageGetItemByTypeAndId (diP->rawItemP, 
                                RTYPE_PushButton, PUSHBUTTONID_FreeTabPage);
                if (NULL != pPBDiP)
                    mdlDialog_rItemColorSet (pPBDiP->rawItemP, DITEM_COLORTYPE_FOREGROUND, 
                                (BSIColorDescr *) mdlWindow_fixedColorIndexGet ((GuiWindowP) dimP->db, RED_INDEX));
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_setSelectionList				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_setSelectionList
(
void
)
    {
    int		 iRow;
    RawItemHdr	*listP, *mListP;
    StringList	*mStrListP;
    ListModel   *pListModel;
    int		 nSelections;
    Spoint2d	*selections;
    Spoint2d	*curSelectionP;

    if (!niG.selectionListDiP || !niG.selectionListDiP->rawItemP)
	return;
    listP = niG.selectionListDiP->rawItemP;

    pListModel = mdlDialog_listBoxGetListModelP (listP);
    if (!pListModel)
	return;

    if (!niG.multiListDiP || !niG.multiListDiP->rawItemP)
	return;

    mListP = niG.multiListDiP->rawItemP;
    mStrListP = mdlDialog_listBoxGetStrListP (mListP);
    if (!mStrListP)
	return;

    mdlDialog_listBoxGetSelections (&nSelections, &selections, mListP);

    /* empty the list model */
    mdlListModel_removeRows (pListModel, 0, -1, TRUE);

    if (nSelections > 0)
	{
	char	    buffer[40];
	char	    *stringP;
	ListRow     *pRow;
	int	    iCell;
	ListCell    *pCell;
	int	    status;

	for (iRow=0, curSelectionP=selections;
	     iRow<nSelections;
	     iRow++, curSelectionP++)
	    {
	    pRow = mdlListRow_create (pListModel);
	    iCell = 0;

	    pCell = mdlListRow_getCellAtIndex (pRow, iCell++);
	    mdlListCell_setStringValue (pCell, "{", TRUE);

	    sprintf (buffer, "%d,", curSelectionP->y);
	    pCell = mdlListRow_getCellAtIndex (pRow, iCell++);
	    mdlListCell_setLongValue (pCell, curSelectionP->y);
	    mdlListCell_setDisplayText (pCell, buffer);

	    sprintf (buffer, "%d ", curSelectionP->x);
	    pCell = mdlListRow_getCellAtIndex (pRow, iCell++);
	    mdlListCell_setLongValue (pCell, curSelectionP->x);
	    mdlListCell_setDisplayText (pCell, buffer);

	    pCell = mdlListRow_getCellAtIndex (pRow, iCell++);
	    mdlListCell_setStringValue (pCell, "}", TRUE);

	    mdlStringList_getMember (&stringP, NULL, mStrListP,
				     curSelectionP->y * niG.nColumns);
	    pCell = mdlListRow_getCellAtIndex (pRow, iCell++);
	    mdlListCell_setLongValue (pCell, curSelectionP->y);
	    mdlListCell_setDisplayText (pCell, stringP);

	    if (niG.nColumns > 1)
		{
		mdlStringList_getMember (&stringP, NULL, mStrListP,
					 curSelectionP->y * niG.nColumns+1);
		pCell = mdlListRow_getCellAtIndex (pRow, iCell++);
		mdlListCell_setLongValue (pCell, curSelectionP->y);
		mdlListCell_setDisplayText (pCell, stringP);
		}

	    status = mdlListModel_insertRow (pListModel, pRow, iRow);
	    }
	}

    mdlDialog_listBoxNRowsChanged (listP);
    mdlDialog_listBoxSetTopRowRedraw (listP, 0, TRUE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_trackList					|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_trackList
(
Point2d	*ptP
)
    {
    char    buffer[40];

    if (!niG.labelDiP)
	return;

    buffer[0] = '\0';
    if (ptP->y != -1)
	sprintf (buffer, "{%3d, %3d}", ptP->y, ptP->x);

    mdlWindow_cursorTurnOff();
    mdlDialog_itemSetLabel (niG.multiSelectDbP, niG.labelDiP->itemIndex,
			    buffer);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_multiListInitStrList				|
|                                                                       |
| author        BSI					11/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_multiListInitStrList
(
StringList  *strListP
)
    {
    int	    iString;
    char    fStr1[20], fStr2[20];

    mdlResource_loadFromStringList (fStr1, NULL, MESSAGELISTID_NewItems,
				    MSGID_Right);
    mdlResource_loadFromStringList (fStr2, NULL, MESSAGELISTID_NewItems,
				    MSGID_Left);
    for (iString=0; iString<NMULTILIST_LINES*2; iString++)
	{
	char    buffer[80];

	if (iString & 1)
	    sprintf (buffer, fStr1, iString/2);
	else
	    sprintf (buffer, fStr2, iString/2+'A', iString/2);

	mdlStringList_setMember (strListP, iString, buffer, NULL);
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_multiListInitDisabled				|
|                                                                       |
| author        BSI					11/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_multiListInitDisabled
(
RawItemHdr  *listP
)
    {
    mdlDialog_listBoxEnableCells (listP, 5, 6, -1, -1, FALSE, FALSE);
    mdlDialog_listBoxEnableCells (listP, 12, 12, -1, -1, FALSE, FALSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_multiListBoxHook				|
|                                                                       |
| author        BSI					08/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_multiListBoxHook
(
DialogItemMessage   *dimP
)
    {
    RawItemHdr	*listP = dimP->dialogItemP->rawItemP;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    StringList	*strListP;

	    /* NOTE: nInfoFields is 2 so 2nd infoField can be used
	       to store a ptr to a BSIColorDesc */
	    listP->userDataP =
		strListP = mdlStringList_create (NMULTILIST_LINES*2, 2);
	    if (!strListP)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    mdlDialog_listBoxSetStrListP (listP, strListP, 2);
	    newItems_multiListInitStrList (strListP);
	    newItems_multiListInitDisabled (listP);
	    niG.multiListDiP = dimP->dialogItemP;
	    break;
	    }

	case DITEM_MESSAGE_ALLCREATED:
	    {

	    niG.multiSelectDbP = dimP->db;
	    niG.labelDiP =
		mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Label, 0, 0);
	    
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    if (listP->userDataP)
		{
		mdlStringList_destroy ((StringList*)listP->userDataP);
		}
	    break;

	case DITEM_MESSAGE_SYNCHRONIZE:
	    {
	    UInt32   attributes;

	    mdlDialog_listBoxGetInfo (&attributes, NULL, NULL, listP);

	    attributes &= ~(LISTATTR_SELBROWSE | LISTATTR_SELSINGLE |
			    LISTATTR_SELMULTI | LISTATTR_SELEXTENDED);
	    switch (niG.selectionMode)
		{
		case 0:
		    attributes |= LISTATTR_SELBROWSE;
		    break;

		case 1:
		    attributes |= LISTATTR_SELSINGLE;
		    break;

		case 2:
		    attributes |= LISTATTR_SELMULTI;
		    break;

		case 3:
		    attributes |= LISTATTR_SELEXTENDED;
		    break;
		}

	    mdlDialog_listBoxSetInfo (&attributes, NULL, NULL, TRUE, listP);
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    if (!dimP->u.stateChanged.reallyChanged)
		break;

	    newItems_setSelectionList ();
	    break;

	case DITEM_MESSAGE_BUTTON:
	    if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
		{
		dimP->u.button.motionFunc = (void(*)(void))newItems_trackList;
		}
	    else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		{
		mdlDialog_itemSetLabel (dimP->db, niG.labelDiP->itemIndex, "");
		}
	    break;

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_selectionListBoxHook				|
|                                                                       |
| author        BSI					08/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_selectionListBoxHook
(
DialogItemMessage   *dimP
)
    {
    RawItemHdr	*listP = dimP->dialogItemP->rawItemP;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    ListModel	*pListModel;

	    /* Create a ListModel with 6 columns */
	    listP->userDataP =
		pListModel = mdlListModel_create (6);
	    if (!pListModel)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    /* Set the ListModel ptr in the ListBox (alternative to StringList) */
	    mdlDialog_listBoxSetListModelP (listP, pListModel, 6);
	    niG.selectionListDiP = dimP->dialogItemP;
	    break;
	    }

	case DITEM_MESSAGE_ALLCREATED:
	    {
	    short       sortStatus;
	    ListModel	*pListModel;

	    /* Set secondary sort for column 2 to column 1 */
	    pListModel = mdlDialog_listBoxGetListModelP (listP);
	    if  (NULL != pListModel)
	    	{
		ListColumn  *pColumn;

		pColumn = mdlListModel_getColumnAtIndex (pListModel, 2);
		if  (NULL != pColumn)
		    {
		    mdlListColumn_setSortFunction  (pColumn, NULL, NULL, 1);
		    }
		}

	    /* Turn sort ability off in column 3 */
	    sortStatus = NOSORT;
	    mdlDialog_listBoxSetColInfoEx (NULL, NULL, NULL, NULL, &sortStatus, NULL, NULL,
					    NULL, dimP->dialogItemP->rawItemP, 3, FALSE);
	    
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    if (listP->userDataP)
		{
		mdlListModel_destroy ((ListModel*)listP->userDataP, TRUE);
		}
	    break;

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_maintainMultiListModifyMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_maintainMultiListModifyMenu
(
DItem_PulldownMenu  *menuP
)
    {
    int			    nSelections;
    RawItemHdr		   *listP;
    DItem_PulldownMenuItem  menuItem;

    if (!niG.multiListDiP || !niG.multiListDiP->rawItemP)
	return;

    listP = niG.multiListDiP->rawItemP;
    mdlDialog_listBoxGetSelections (&nSelections, NULL, listP);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MSID_AddSelection))
        mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MSID_DeleteSelection))
        mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MSID_ToggleAddMode))
        mdlDialog_textPDMItemSetEnabled (&menuItem,
				niG.selectionMode == SELECTIONMODE_EXTENDED);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_maintainMultiListSelectMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_maintainMultiListSelectMenu
(
DItem_PulldownMenu  *menuP
)
    {
    DItem_PulldownMenuItem  menuItem;
    BoolInt		    onlyOneElement =
				(niG.selectionMode == SELECTIONMODE_BROWSE ||
				 niG.selectionMode == SELECTIONMODE_SINGLE);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MSID_SelectEveryOther))
	mdlDialog_textPDMItemSetEnabled (&menuItem, !onlyOneElement);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MSID_SelectAll))
	mdlDialog_textPDMItemSetEnabled (&menuItem, !onlyOneElement);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MSID_DeselectAll))
	mdlDialog_textPDMItemSetEnabled (&menuItem,
				niG.selectionMode != SELECTIONMODE_BROWSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_maintainMultiListPositionMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_maintainMultiListPositionMenu
(
DItem_PulldownMenu  *menuP
)
    {
    int			    nSelections;
    RawItemHdr		   *listP;
    DItem_PulldownMenuItem  menuItem;

    if (!niG.multiListDiP ||
	!niG.multiListDiP->rawItemP)
	return;

    listP = niG.multiListDiP->rawItemP;
    mdlDialog_listBoxGetSelections (&nSelections, NULL, listP);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
			    0, MSID_TopSelection))
        mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		newItems_maintainMultiListLocationMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_maintainMultiListLocationMenu
(
DItem_PulldownMenu  *menuP
)
    {
    int			    nSelections;
    RawItemHdr		   *listP;
    DItem_PulldownMenuItem  menuItem;

    if (!niG.multiListDiP || !niG.multiListDiP->rawItemP)
	return;

    listP = niG.multiListDiP->rawItemP;
    mdlDialog_listBoxGetSelections (&nSelections, NULL, listP);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL, 0,
				    MSID_LocationSelection))
    mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_multiListInsertItem				|
|                                                                       |
| author        BSI					11/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_multiListInsertItem
(
RawItemHdr  *listP,
StringList  *strListP,
int	     iRow,	    /* => row to insert before */
int	     nDisplayedRows,
int	     oldTopRow,
BoolInt	     appending
)
    {
    long	    iString;
    int	    newTopRow;
    char    buffer[80];

    iString = iRow * niG.nColumns;
    mdlStringList_insertMember (&iString, strListP, iString, niG.nColumns);

    if (appending)
	newItems_rscSprintf (buffer, MSGID_LeftAfter, iRow-1+'A', iRow-1);
    else
	newItems_rscSprintf (buffer, MSGID_LeftBefore, iRow+'A', iRow);
    mdlStringList_setMember (strListP, iString, buffer, NULL);

    if (niG.nColumns > 1)
	{
	if (appending)
	    newItems_rscSprintf (buffer, MSGID_Right, iRow-1);
	else
	    newItems_rscSprintf (buffer, MSGID_Right, iRow);

	mdlStringList_setMember (strListP, iString+1, buffer, NULL);
	}

    mdlDialog_listBoxNRowsChanged (listP);

    newTopRow = iRow - nDisplayedRows/2;
    if (newTopRow < 0)  newTopRow = 0;
    mdlDialog_listBoxSetTopRowRedraw (listP, newTopRow, TRUE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_doMultiListModify				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_doMultiListModify
(
RawItemHdr  *listP,
int	     searchId
)
    {
    int		minRow, maxRow, nDisplayedRows;
    StringList *strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / niG.nColumns;
	}

    mdlDialog_listBoxGetDisplayRange (&minRow, &maxRow, NULL, NULL, listP);
    nDisplayedRows = maxRow - minRow + 1;

    switch (searchId)
	{
	case MSID_ResetList:
	    mdlDialog_listBoxSelectCells (listP, 0, -1, 0, -1, FALSE, FALSE);
	    mdlStringList_deleteMember (strListP, 0, -1);
	    mdlStringList_insertMember (NULL, strListP, -1,
					NMULTILIST_LINES * 2);
	    newItems_multiListInitStrList (strListP);

	    mdlDialog_listBoxNRowsChanged (listP);
	    newItems_multiListInitDisabled (listP);

	    mdlDialog_listBoxSetTopRowRedraw (listP, 0, TRUE);
	    break;

	case MSID_RecreateList:
	    mdlStringList_destroy (strListP);
	    listP->userDataP = strListP =
				mdlStringList_create (NMULTILIST_LINES*2, 2);
	    newItems_multiListInitStrList (strListP);
	    mdlDialog_listBoxSetStrListP (listP, strListP, 2);
	    newItems_multiListInitDisabled (listP);
	    mdlDialog_listBoxSetTopRowRedraw (listP, 0, TRUE);

	    /* a stateChanged message won't be generated, fixup list
		manually */
	    newItems_setSelectionList ();
	    break;

	case MSID_AddTop:
	    newItems_multiListInsertItem (listP, strListP, 0, nDisplayedRows,
					  minRow, FALSE);
	    break;

	case MSID_AddBottom:
	    newItems_multiListInsertItem (listP, strListP, nRows,
					  nDisplayedRows, minRow, TRUE);
	    break;

	case MSID_AddSelection:
	    {
	    int		nSelections;
	    Spoint2d	*selections;

	    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
	    if (nSelections > 0 && selections)
		newItems_multiListInsertItem (listP, strListP, selections->y,
					      nDisplayedRows, minRow, FALSE);
	    break;
	    }

	case MSID_DeleteSelection:
	    {
	    int		nSelections;
	    Spoint2d	*selections;

	    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
	    if (nSelections > 0 && selections)
		{
		int	     newTopRow;
		Spoint2d    *curSelectionP;

		for (curSelectionP = selections+nSelections-1;
		     curSelectionP >= selections; curSelectionP--)
		    {
		    mdlStringList_deleteMember (strListP,
					    curSelectionP->y * niG.nColumns,
					    niG.nColumns);
		    }

		/* browse should always have something selected */
		if (niG.selectionMode == SELECTIONMODE_BROWSE)
		    mdlDialog_listBoxSelectCells (listP, selections->y,
						  selections->y, 0, -1, TRUE,
						  FALSE);

		mdlDialog_listBoxNRowsChanged (listP);
		newTopRow = selections->y - nDisplayedRows/2;
		if (newTopRow < 0)
		    newTopRow = 0;

		mdlDialog_listBoxSetTopRowRedraw (listP, newTopRow, TRUE);
		}
	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_doMultiListSelect				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_doMultiListSelect
(
RawItemHdr  *listP,
int	     searchId
)
    {
    StringList	*strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / niG.nColumns;
	}

    switch (searchId)
	{
	case MSID_SelectTop:
	    {
	    Point2d	selectionPt;

	    selectionPt.x = selectionPt.y = 0;
	    mdlDialog_listBoxSetSelectionsX (listP, 1, &selectionPt, TRUE,
					    TRUE);
	    break;
	    }

	case MSID_SelectBottom:
	    {
	    Point2d	selectionPt;

	    selectionPt.y = nRows - 1;
	    selectionPt.x = 0;
	    mdlDialog_listBoxSetSelectionsX (listP, 1, &selectionPt, TRUE,
					    TRUE);
	    break;
	    }

	case MSID_SelectEveryOther:
	    if ((niG.selectionMode == SELECTIONMODE_MULTI) ||
		(niG.selectionMode == SELECTIONMODE_EXTENDED))
		{
		int	     iRow;
		int	     nSelectedRows = 0;
		Point2d    *curSelectionP, *selectionsP = NULL;

		selectionsP = (Point2d*)dlmSystem_mdlMalloc ((nRows/2+1)*sizeof(Spoint2d));
		if (!selectionsP)
		    break;

		for (iRow=0, curSelectionP=selectionsP; iRow<nRows;
			iRow+=2, curSelectionP++, nSelectedRows++)
		     {
		     curSelectionP->y = iRow;
		     curSelectionP->x = 0;
		     }

		mdlDialog_listBoxSetSelectionsX (listP, nSelectedRows,
						selectionsP, TRUE, TRUE);
		dlmSystem_mdlFree (selectionsP);
		}
	    break;

	case MSID_SelectAll:
	    if ((niG.selectionMode == SELECTIONMODE_MULTI) ||
		(niG.selectionMode == SELECTIONMODE_EXTENDED))
		{
		mdlDialog_listBoxSelectCells (listP, 0, -1, 0, -1, TRUE,
					      TRUE);
		}
	    break;

	case MSID_DeselectTop:
	    mdlDialog_listBoxSelectCells (listP, 0, 0, 0, -1, FALSE, TRUE);
	    break;

	case MSID_DeselectBottom:
	    mdlDialog_listBoxSelectCells (listP, nRows-1, nRows-1, 0, -1,
					  FALSE, TRUE);
	    break;

	case MSID_DeselectAll:
	    mdlDialog_listBoxSelectCells (listP, 0, -1, 0, -1, FALSE, TRUE);
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_doMultiListPosition				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_doMultiListPosition
(
RawItemHdr  *listP,
int	     searchId
)
    {
    int		minRow, maxRow, nDisplayedRows;
    StringList	*strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / niG.nColumns;
	}

    mdlDialog_listBoxGetDisplayRange (&minRow, &maxRow, NULL, NULL, listP);
    nDisplayedRows = maxRow - minRow + 1;

    switch (searchId)
	{
	case MSID_Top1st:
	    mdlDialog_listBoxSetTopRow (listP, 0);
	    break;

	case MSID_BottomLast:
	    {
	    int	newTopRow;

	    newTopRow = nRows - nDisplayedRows;
	    if (newTopRow < 0)
		newTopRow = 0;

	    mdlDialog_listBoxSetTopRow (listP, newTopRow);
	    break;
	    }

	case MSID_TopSelection:
	    {
	    int		nSelections;
	    Spoint2d	*selections;

	    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
	    if (nSelections > 0 && selections)
		mdlDialog_listBoxSetTopRow (listP, selections->y);
	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_doMultiListLocation				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_doMultiListLocation
(
RawItemHdr  *listP,
int	     searchId
)
    {
    StringList	*strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / niG.nColumns;
	}

    switch (searchId)
	{
	case MSID_LocationTop:
	    {
	    int newTopRow;

	    mdlDialog_listBoxSetLocationCursor (listP, 0, 0);

	    /* need to get location cursor since first rows may be disabled */
	    mdlDialog_listBoxGetLocationCursor (&newTopRow, NULL, listP);
	    mdlDialog_listBoxSetTopRow (listP, newTopRow);
	    break;
	    }

	case MSID_LocationBottom:
	    {
	    int newTopRow;
	    int	minRow, maxRow, nDisplayedRows;

	    mdlDialog_listBoxSetLocationCursor (listP, nRows-1, 0);

	    /* need to get location cursor since last rows may be disabled */
	    mdlDialog_listBoxGetLocationCursor (&newTopRow, NULL, listP);

	    mdlDialog_listBoxGetDisplayRange (&minRow, &maxRow, NULL, NULL,
					      listP);
	    nDisplayedRows = maxRow - minRow + 1;
	    newTopRow = newTopRow - nDisplayedRows + 1;
	    if (newTopRow < 0)
		newTopRow = 0;

	    mdlDialog_listBoxSetTopRow (listP, newTopRow);
	    break;
	    }

	case MSID_LocationSelection:
	    {
	    int		nSelections;
	    Spoint2d	*selections;

	    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
	    if (nSelections > 0 && selections)
		{
		mdlDialog_listBoxSetLocationCursor (listP, selections->y, 0);
		newItems_doMultiListPosition (listP, MSID_TopSelection);
		}
	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_multiListTestsMenusHook			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_multiListTestsMenusHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_DRAW:
	    {
	    PulldownMenuMessage	   *pdmmP =
					(PulldownMenuMessage *)dimP->auxInfoP;
	    DItem_PulldownMenu	   *menuP = pdmmP->menuP;

	    if (menuP->type != RTYPE_PulldownMenu)
		break;
	    /* need to switch on menuP->id because multiple menus were attached
	       to this hook function */
	    switch (menuP->id)
		{
		case PDMID_MultiListModify:
		    newItems_maintainMultiListModifyMenu (menuP);
		    break;

		case PDMID_MultiListSelect:
		    newItems_maintainMultiListSelectMenu (menuP);
		    break;

		case PDMID_MultiListPosition:
		    newItems_maintainMultiListPositionMenu (menuP);
		    break;

		case PDMID_MultiListLocation:
		    newItems_maintainMultiListLocationMenu (menuP);
		    break;
		}

	    break;
	    }

	case DITEM_MESSAGE_BUTTON:
	    {
	    PulldownMenuMessage	   *pdmmP =
					(PulldownMenuMessage *)dimP->auxInfoP;
	    DItem_PulldownMenu	   *menuP;
	    RawItemHdr		   *listP;
	    int			    searchId;

	    if (dimP->u.button.buttonTrans != BUTTONTRANS_UP)
		break;
	    menuP = pdmmP->menuP;

	    if (!niG.multiListDiP || !niG.multiListDiP->rawItemP)
		break;
	    listP = niG.multiListDiP->rawItemP;
	    searchId = pdmmP->u.activateItem.searchId;

	    /* need to switch on menuP->id because multiple menus were
		attached to this hook function */
	    switch (menuP->id)
		{
		case PDMID_MultiListModify:
		    newItems_doMultiListModify (listP, searchId);
		    break;

		case PDMID_MultiListSelect:
		    newItems_doMultiListSelect (listP, searchId);
		    break;

		case PDMID_MultiListPosition:
		    newItems_doMultiListPosition (listP, searchId);
		    break;

		case PDMID_MultiListLocation:
		    newItems_doMultiListLocation (listP, searchId);
		    break;
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_setupPopupMenu					|
|                                                                       |
| author        BSI					03/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_setupPopupMenu
(
DialogMessage   *dmP
)
    {
    int			 iItem;
    int			 menuId;
    RawItemHdr		*popupP;
    DialogItem		*popupDiP;
    DialogItem		*downDiP;
    DItem_PulldownMenu	*popupMenuP;
    DialogBox		*dbP = dmP->db;

    /* find the index of the item the reset button went down in */
    iItem = mdlDialog_findItemIndex (dbP, &dmP->u.button.pt, FALSE);
    if (iItem < 0)
	return;

    popupDiP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_PopupMenu,
					     POPUPMENUID_MultiList, 0);
    if (!popupDiP)
	return;
    popupP  = popupDiP->rawItemP;

    downDiP = mdlDialog_itemGetByIndex (dbP, iItem);
    switch (downDiP->type)
	{
	case RTYPE_ListBox:
	    if (downDiP->id == LISTBOXID_Multi)
		menuId = PDMID_MultiListPosition;
	    else
		menuId = PDMID_MultiListSelect;
	    break;

	case RTYPE_OptionButton:
	    menuId = PDMID_MultiListTests;
	    break;

	default:
	    return;
	}

    /* Popup Menus are a restricted form of MenuBar (they only have 1 top
       level menu). mdlDialog_menuBarxxx functions can be called with ptrs
       to popup menus. */
    popupMenuP = mdlDialog_menuBarGetMenu (popupP, 0);
    if (!popupMenuP)
	return;
    if (popupMenuP->id != menuId)
	{
	mdlDialog_menuBarDeleteMenu (popupMenuP, popupP, NULL, 0);
	mdlDialog_menuBarInsMenu (popupP, RTYPE_PulldownMenu, menuId, NULL);
	}

    /* force the button event to go to the popupMenu item */
    dmP->u.button.buttonRiP = popupP;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_multiListDialogHook				|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void	newItems_multiListDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    dmP->u.create.interests.preButtons = TRUE;
	    dmP->u.create.interests.nonDataPoints = TRUE;
	    break;

	case DIALOG_MESSAGE_PREBUTTON:
	    if ((dmP->u.button.buttonTrans == BUTTONTRANS_DOWN) &&
		(dmP->u.button.buttonNumber == RESET))
		{
		newItems_setupPopupMenu (dmP);
		}
	    break;

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_tabsDialogHook					|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void	newItems_tabsDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    dmP->u.create.interests.dialogFocuses = TRUE;
	    dmP->u.create.interests.resizes   = TRUE;
	    break;

	case DIALOG_MESSAGE_FOCUSIN:
	    break;

	case DIALOG_MESSAGE_FOCUSOUT:
	    break;

	case DIALOG_MESSAGE_RESIZE:
	    dmP->u.resize.forceCompleteRedraw = TRUE;
	    break;

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_multiLinePushButton				|
|                                                                       |
| author        BSI					07/2000		|
|                                                                       |
+----------------------------------------------------------------------*/
Private void	newItems_multiLinePushButton
(
DialogItemMessage   *dimP
)
    {
    RawItemHdr	*pbtnP = dimP->dialogItemP->rawItemP;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    UInt32	    iconAlignment = PBUTATTR_ICONRIGHT;
	    UInt32	    iconType = RTYPE_Icon;
	    long	    iconId = ICONID_TabOptions;

	    mdlDialog_pushButtonSetIcon (pbtnP, 
					&iconAlignment,
					&iconId,
					&iconType,
					(void**)&pbtnP->ownerMD);

	    break;
	    }
	default:
	    {
	    dimP->msgUnderstood = FALSE;
	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| author        BSI                                     06/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    newItems_handleElementLevelHook
(
DialogItemMessage   *dimP
)
    {
    ULong   listAttr = LEVELLIST_FILTER | LEVELLIST_SHOW_LEVEL_LIBRARIES | LEVELLIST_BOLD_USED;
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int 	     numCols=1;
	    ListModel	    *pListModel;

	    if (NULL == (pListModel = mdlListModel_create (numCols)))
		{
		dimP->u.create.createFailed  = TRUE;
		break;
		}

	    if (SUCCESS != mdlLevelList_getLevelNamesListModelEx (pListModel, MASTERFILE, niG.currentLevel, listAttr))
		{
		mdlListModel_destroy (pListModel, TRUE);
		dimP->u.create.createFailed = TRUE;
		break;
		}

	    mdlDialog_comboBoxSetListModelP (dimP->dialogItemP->rawItemP, pListModel);
	    break;
	    }

	case DITEM_MESSAGE_SETSTATE:
	    {
            mdlLevelList_handleLevelNameSetState (dimP, niG.currentLevel);
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    mdlDialog_synonymsSynch (NULL, SYNONYMID_ElementAttributes, NULL);
	    break;
	    }

	case DITEM_MESSAGE_RELOADDATA:
	    {
	    ListModel       *pListModel = NULL;

	    if (pListModel = mdlDialog_comboBoxGetListModelP (dimP->dialogItemP->rawItemP))
		{

		mdlListModel_empty (pListModel, TRUE);

		mdlLevelList_getLevelNamesListModelEx (pListModel, MASTERFILE, niG.currentLevel, listAttr);
		if  (dimP->u.reloadData.redraw)
		    {
		    mdlDialog_rItemSynch (dimP->dialogItemP->rawItemP);
		    mdlDialog_rItemDraw (dimP->dialogItemP->rawItemP);
		    }
		}
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    ListModel	    *pListModel;

	    if (pListModel = mdlDialog_comboBoxGetListModelP (dimP->dialogItemP->rawItemP))
		{
		mdlListModel_destroy (pListModel, TRUE);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}	    
    }


/*----------------------------------------------------------------------+
|                                                                       |
| author        BSI                                     06/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    newItems_handleElementColorHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int		     numCols = 1;
	    ListModel	    *pListModel;

	    if (NULL == (pListModel = mdlListModel_create (numCols)))
		{
		dimP->u.create.createFailed  = TRUE;
		break;
		}

	    if (SUCCESS != mdlLevelList_getColorListModel (pListModel, dimP->db, MASTERFILE, &niG.currentLevel))
		{
		mdlListModel_destroy (pListModel, TRUE);
		dimP->u.create.createFailed  = TRUE;
		break;
		}
                
	    mdlDialog_comboBoxSetListModelP (dimP->dialogItemP->rawItemP, pListModel);

	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    mdlDialog_synonymsSynch (NULL, SYNONYMID_ElementAttributes, NULL);
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    ListModel	    *pListModel;

	    if (pListModel = mdlDialog_comboBoxGetListModelP (dimP->dialogItemP->rawItemP))
		mdlListModel_destroy (pListModel, TRUE);

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}	    
    }


/*----------------------------------------------------------------------+
|                                                                       |
| author        BSI                                     06/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    newItems_handleElementStyleHook
(
DialogItemMessage   *dimP
)
    {
    static  UShort  minComboWidth;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int 	     numCols=1;
	    ListModel	    *pListModel;

	    if (NULL == (pListModel = mdlListModel_create (numCols)))
		{
		dimP->u.create.createFailed  = TRUE;
		break;
		}

	    if (SUCCESS != mdlLevelList_getStyleListModel (pListModel, TRUE, MASTERFILE, &niG.currentLevel))
		{
		mdlListModel_destroy (pListModel, TRUE);
		dimP->u.create.createFailed  = TRUE;
		break;
		}

	    mdlDialog_comboBoxSetListModelP (dimP->dialogItemP->rawItemP, pListModel);
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    mdlDialog_synonymsSynch (NULL, SYNONYMID_ElementAttributes, NULL);
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    ListModel	    *pListModel;

	    if (pListModel = mdlDialog_comboBoxGetListModelP (dimP->dialogItemP->rawItemP))
		{
		mdlListModel_destroy (pListModel, TRUE);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}	    
    }

/*----------------------------------------------------------------------+
|                                                                       |
| author        BSI                                     06/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    newItems_handleElementWeightHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int 	     numCols=1;
	    ListModel	    *pListModel;

	    if (NULL == (pListModel = mdlListModel_create (numCols)))
		{
		dimP->u.create.createFailed  = TRUE;
		break;
		}

	    if (SUCCESS != mdlLevelList_getWeightListModel (pListModel, MASTERFILE, &niG.currentLevel))
		{
		mdlListModel_destroy (pListModel, TRUE);
		dimP->u.create.createFailed  = TRUE;
		break;
		}
                
	    mdlDialog_comboBoxSetListModelP (dimP->dialogItemP->rawItemP, pListModel);
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    mdlDialog_synonymsSynch (NULL, SYNONYMID_ElementAttributes, NULL);
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    ListModel	    *pListModel;

	    if (pListModel = mdlDialog_comboBoxGetListModelP (dimP->dialogItemP->rawItemP))
		{
		mdlListModel_destroy (pListModel, TRUE);
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
|   2D Arrows								|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_draw2DArrows					|
|                                                                       |
| author        BSI					12/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_draw2DArrows
(
DialogBox   *dbP,
Point2d	    *ptP,
BoolInt	     enabled,
BoolInt	     drawShaft,
BoolInt	     fill
)
    {
    Point2d	    pt;
    BSIRect	    rect;
    BSIColorDescr  *whiteP;
    int		    fontHeight = mdlDialog_fontGetCurHeight (dbP);

    if (!(fontHeight & 1))
	fontHeight++;			    /* make odd */
    whiteP = (BSIColorDescr *)mdlWindow_fixedColorIndexGet ((GuiWindowP) dbP, WHITE_INDEX);

    pt.x = ptP->x + fontHeight;
    pt.y = ptP->y;
    mdlDialog_rectSet (&rect, pt.x, pt.y, pt.x+fontHeight-1,
		       pt.y+fontHeight-1);
    mdlDialog_rectFill (dbP, &rect, LGREY_INDEX);
    mdlDialog_rectDraw (dbP, &rect, BLACK_INDEX);
    pt.x += 3;
    pt.y += 3;
    mdlDialog_2DArrowDrawCD (dbP, &pt, fontHeight-6, enabled, drawShaft,
	SCROLL_ARROW_UP, NULL, NULL, NULL, whiteP, NULL);

    pt.x = ptP->x + fontHeight;
    pt.y = ptP->y + fontHeight*2;
    mdlDialog_rectSet (&rect, pt.x, pt.y, pt.x+fontHeight-1,
		       pt.y+fontHeight-1);
    mdlDialog_rectFill (dbP, &rect, LGREY_INDEX);
    mdlDialog_rectDraw (dbP, &rect, BLACK_INDEX);
    pt.x += 3;
    pt.y += 3;
    mdlDialog_2DArrowDrawCD (dbP, &pt, fontHeight-6, enabled, drawShaft,
	SCROLL_ARROW_DOWN, NULL, NULL, NULL, whiteP, NULL);

    pt.x = ptP->x;
    pt.y = ptP->y + fontHeight;
    mdlDialog_rectSet (&rect, pt.x, pt.y, pt.x+fontHeight-1,
		       pt.y+fontHeight-1);
    mdlDialog_rectFill (dbP, &rect, LGREY_INDEX);
    mdlDialog_rectDraw (dbP, &rect, BLACK_INDEX);
    pt.x += 3;
    pt.y += 3;
    mdlDialog_2DArrowDrawCD (dbP, &pt, fontHeight-6, enabled, drawShaft,
	SCROLL_ARROW_LEFT, NULL, NULL, NULL, whiteP, NULL);

    pt.x = ptP->x + fontHeight*2;
    pt.y = ptP->y + fontHeight;
    mdlDialog_rectSet (&rect, pt.x, pt.y, pt.x+fontHeight-1,
		       pt.y+fontHeight-1);
    mdlDialog_rectFill (dbP, &rect, LGREY_INDEX);
    mdlDialog_rectDraw (dbP, &rect, BLACK_INDEX);
    pt.x += 3;
    pt.y += 3;
    mdlDialog_2DArrowDrawCD (dbP, &pt, fontHeight-6, enabled, drawShaft,
			     SCROLL_ARROW_RIGHT, NULL, NULL, NULL, whiteP,
			     NULL);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		newItems_2DArrowsDialogHook				|
|                                                                       |
| author        BSI					12/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void	newItems_2DArrowsDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    dmP->u.create.interests.updates   = TRUE;
	    break;

	case DIALOG_MESSAGE_UPDATE:
	    {
	    Point2d pt;
	    int	fontHeight   = mdlDialog_fontGetCurHeight (dmP->db);

	    pt.x = fontHeight;
	    pt.y = fontHeight;
	    newItems_draw2DArrows (dmP->db, &pt, FALSE, FALSE, TRUE);

	    pt.x += fontHeight*4;
	    newItems_draw2DArrows (dmP->db, &pt, TRUE,	 FALSE, TRUE);

	    pt.y += fontHeight*4;
	    newItems_draw2DArrows (dmP->db, &pt, TRUE,  TRUE, TRUE);

	    pt.x -= fontHeight*4;
	    newItems_draw2DArrows (dmP->db, &pt, FALSE, TRUE, TRUE);

	    pt.x = fontHeight;
	    pt.y = fontHeight + fontHeight*5;
	    newItems_draw2DArrows (dmP->db, &pt, FALSE, FALSE, FALSE);

	    pt.x += fontHeight*4;
	    newItems_draw2DArrows (dmP->db, &pt, TRUE,	 FALSE, FALSE);

	    pt.y += fontHeight*4;
	    newItems_draw2DArrows (dmP->db, &pt, TRUE,  TRUE, FALSE);

	    pt.x -= fontHeight*4;
	    newItems_draw2DArrows (dmP->db, &pt, FALSE, TRUE, FALSE);
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|       Command Handling routines                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          newitems_open						|
|                                                                       |
| author        BSI                                     12/90           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_open
(
char	*unparsedP	/* => unparsed part of command */
)
/*cmdNumber   CMD_NEWITEMS_OPEN_RADIOBUTTONS,
	    CMD_NEWITEMS_OPEN_HSCALES, CMD_NEWITEMS_OPEN_VSCALES,
	    CMD_NEWITEMS_OPEN_MLPBUTS, CMD_NEWITEMS_OPEN_SASH,
	    CMD_NEWITEMS_OPEN_BGROUP, CMD_NEWITEMS_OPEN_COLORS,
	    CMD_NEWITEMS_OPEN_MULTILIST, CMD_NEWITEMS_OPEN_2DARROWS,
	    CMD_NEWITEMS_OPEN_ALERTS, CMD_NEWITEMS_OPEN_TABS,
	    CMD_NEWITEMS_OPEN_VSASH, CMD_NEWITEMS_OPEN_LEVELATTRIBUTES */
    {
    DialogBox  *dbP;	/* a ptr to a dialog box */
    long	dialogId;

    dialogId = (mdlCommandNumber & 0xFF00) >> 8;
    if (dialogId == 0)
	return;

    if (dialogId == DIALOGID_MultiList)
	{
	/* Did this way because didn't feel like adding a dialog hook */
	niG.selectionMode = SELECTIONMODE_BROWSE;
	niG.nColumns = 2;
	}

    /* open the newItems dialog box */
    if ((dbP = mdlDialog_open (NULL, dialogId)) == NULL)
	newItems_errorPrint (MSGID_DialogBox);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newitems_openModal                                      |
|                                                                       |
| author        BSI                                     12/90           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_openModal
(
char	*unparsedP	/* => unparsed part of command */
)
/* cmdNumber   CMD_NEWITEMS_MODALOPEN_RADIOBUTTONS, CMD_NEWITEMS_MODALOPEN_SCALES */
    {
    int	    lastAction;
    long    dialogId = 0;

    /* open child modal dialog box */
    switch (mdlCommandNumber)
	{
	case CMD_NEWITEMS_MODALOPEN_RADIOBUTTONS:
	    dialogId = DIALOGID_ModalRadioButtons;
	    break;

	case CMD_NEWITEMS_MODALOPEN_SCALES:
	    dialogId = DIALOGID_ModalScales;
	    break;
	}

    if (dialogId == 0)
	return;

    if (mdlDialog_openModal (&lastAction, NULL, dialogId))
	newItems_errorPrint (MSGID_DialogBox);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_alertCommands					|
|                                                                       |
| author        BSI					12/93           |
|									|
|   Example uses of mdlDialog_openxxxxxAlert.			    	|
|			    						|
+----------------------------------------------------------------------*/
Private void newItems_alertCommands
(
char	*unparsedP	/* => unparsed part of command */
)
/* cmdNumber   CMD_NEWITEMS_ALERT_CRITICAL,CMD_NEWITEMS_ALERT_INFORMATION,
	    CMD_NEWITEMS_ALERT_QUESTION,CMD_NEWITEMS_ALERT_WARNING */

    {
    char	msgBuf[300];

    switch (mdlCommandNumber)
	{
	case CMD_NEWITEMS_ALERT_CRITICAL:
	    mdlResource_loadFromStringList (msgBuf, 0L, MESSAGELISTID_NewItems,
					    MSGID_Critical);
	    mdlDialog_openMessageBox (DIALOGID_MsgBoxYesNoCancel, msgBuf,
				      MSGBOX_ICON_CRITICAL);
	    break;

	 case CMD_NEWITEMS_ALERT_INFORMATION:
	    mdlResource_loadFromStringList (msgBuf, 0L, MESSAGELISTID_NewItems,
					    MSGID_Information);
	    mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgBuf,
				      MSGBOX_ICON_INFORMATION);
	    break;

	case CMD_NEWITEMS_ALERT_QUESTION:
	    mdlResource_loadFromStringList (msgBuf, 0L, MESSAGELISTID_NewItems,
					    MSGID_Question);
	    mdlDialog_openMessageBox (DIALOGID_MsgBoxOKCancel, msgBuf,
				      MSGBOX_ICON_QUESTION);
	    break;

	case CMD_NEWITEMS_ALERT_WARNING:
	    mdlResource_loadFromStringList (msgBuf, 0L, MESSAGELISTID_NewItems,
					    MSGID_Warning);
	    mdlDialog_openMessageBox (DIALOGID_MsgBoxYesNo, msgBuf,
				      MSGBOX_ICON_WARNING);
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_exit						|
|                                                                       |
| author        BSI					08/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_exit
(
char	*unparsedP	/* => unparsed part of command */
)
//cmdNumber   CMD_NEWITEMS_EXIT
    {
    mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_SILENTUNLOAD,
                                          mdlSystem_getCurrTaskID(), TRUE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_cmdFreeTabPage					|
|                                                                       |
| author        BSI					07/97           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void newItems_cmdFreeTabPage
(
char	*unparsedP	/* => unparsed part of command */
)
// cmdNumber   CMD_NEWITEMS_FREETABPAGE
    {
    DialogBox       *db;
    DialogItem      *listDiP, *diP;

    db = mdlDialog_findByTypeAndId (RTYPE_DialogBox, DIALOGID_Tabs, NULL);
    if (db)
    	{
	/* Free page test */
	listDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_TabPageList,
					    TPLISTID_ONE, 0);
	if (listDiP)
	    {
	    diP = mdlDialog_tabPageListGetPageByIndex (listDiP->rawItemP, 0);
	    if (diP)
		{
		mdlDialog_tabPageFreeItems (diP->rawItemP,
					    0, -1);
		mdlDialog_tabPageListFreePages (listDiP->rawItemP,
					    diP->itemIndex, diP->itemIndex);
		}
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_unloadFunction					|
|                                                                       |
| author        BSI                                     09/91           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int newItems_unloadFunction
(
int reasonCode
)
    {
    mdlDialog_menuBarUnloadApp	(NULL, NULL, 0);
    if (niG.localPalP != NULL)
    	mdlColorPal_destroy (&niG.localPalP);

    return (FALSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| author        BSI                                     06/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    newItems_onLevelChange
(
int		fileNum,
ULong		levelId,
int		type
)
    {
    if  (type == LEVEL_TABLE_REWRITE)
	{
	mdlDialog_synonymReloadData (SYNONYMID_ElementAttributes, TRUE);
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| author        BSI                                     06/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    newItems_onFilterChange
(
MSWChar		*wFilterName,
ULong		 filterId,
int		 type
)
    {
    if  (type == FILTER_CHANGE_ACTIVE)
	{
	mdlDialog_synonymReloadData (SYNONYMID_ElementAttributes, TRUE);
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          newItems_scaleColor					|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private UChar newItems_scaleColor
(
UChar	color,
double	scaleFactor
)
    {
    double  temp = color * scaleFactor;
    color = (UChar) temp;
    if (color > 255)
	color = 255;

    return (color);
    }

/*----------------------------------------------------------------------+
|									|
| name		newItems_initializeColorDescriptors			|
|									|
| author	BSI				    11/94		|
|									|
+----------------------------------------------------------------------*/
Private int	newItems_initializeColorDescriptors
(
BSIColorPalette **palPP,
BSIColorDescr   **colorsPP,	    /* <= color descr array to initialize */
int	    	numColorDescrs      /* => number of colors in the array */
)
    {
    int     	status;
    long	iColor;
    BSIColorDescr **cdPP;
    RGBColorDef	backgroundRgb;
    RGBColorDef	rgb;

    /*-------------------------------------------------------------------
	Create a palette of color descriptors to use for our color
	customized items.
    -------------------------------------------------------------------*/
    if ((status = mdlColorPal_create (palPP, numColorDescrs))
	!= SUCCESS)
	return status;

    /* setup local color array for easy access to color descriptors */
    for (iColor=0, cdPP=colorsPP; iColor < numColorDescrs; iColor++, cdPP++)
	*cdPP = mdlColorPal_getColorDescr (*palPP, iColor);

    /* Use the X color id for Light Blue to set a color descriptor to be used
       as a lt blue background color */
    mdlColorDescr_setByColorId (colorsPP[LCOLOR_BACKGROUND], LIGHT_BLUE, FALSE);
    mdlColorDescr_getRgb (&backgroundRgb, colorsPP[LCOLOR_BACKGROUND]);

    /* Make the select color a little darker than background */
    rgb = backgroundRgb;
    rgb.red   = newItems_scaleColor (rgb.red, 0.85);
    rgb.green = newItems_scaleColor (rgb.green, 0.85);
    rgb.blue  = newItems_scaleColor (rgb.blue, 0.85);
    mdlColorDescr_setByRgb (colorsPP[LCOLOR_SELECT], &rgb, FALSE);

    /* Make the top shadow color a little lighter than the background */
    rgb = backgroundRgb;
    rgb.red   = newItems_scaleColor (rgb.red, 1.5);
    rgb.green = newItems_scaleColor (rgb.green, 1.5);
    rgb.blue  = newItems_scaleColor (rgb.blue, 1.5);
    mdlColorDescr_setByRgb (colorsPP[LCOLOR_TOPSHADOW], &rgb, FALSE);

    /* Make the bottom shadow color alot darker */
    rgb = backgroundRgb;
    rgb.red   = newItems_scaleColor (rgb.red, 0.50);
    rgb.green = newItems_scaleColor (rgb.green, 0.50);
    rgb.blue  = newItems_scaleColor (rgb.blue, 0.50);
    mdlColorDescr_setByRgb (colorsPP[LCOLOR_BOTTOMSHADOW], &rgb, FALSE);

    /* Use some of the standard "menu colors" (Window Manager colors). */
    mdlColorDescr_setByMenuColor (colorsPP[LCOLOR_BGTEXT],  	MGREY_INDEX);
    mdlColorDescr_setByMenuColor (colorsPP[LCOLOR_FGTEXT],  	WHITE_INDEX);
    mdlColorDescr_setByMenuColor (colorsPP[LCOLOR_LABELTEXT], 	RED_INDEX);

    /* Use an element color for one of our descriptors */
    mdlColorDescr_setByElemColorNumber (colorsPP[LCOLOR_DGN], 4);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI     				08/92		|
|									|
+----------------------------------------------------------------------*/
static DialogHookInfo uHooks[] =
    {
    {HOOKID_RadioButtons,		(PFDialogHook)newItems_radioButtonHook},
    {HOOKID_ScaleSpeed,			(PFDialogHook)newItems_speedScaleHook},
    {HOOKID_Sash,			(PFDialogHook)newItems_sashHook},
    {HOOKID_ListBoxSash,		(PFDialogHook)newItems_sashListBoxHook},
    {HOOKID_Dialog_Sash,		(PFDialogHook)newItems_sashDialogHook},
    {HOOKID_VSash,			(PFDialogHook)newItems_vSashHook},
    {HOOKID_Dialog_VSash,		(PFDialogHook)newItems_vSashDialogHook},
    {HOOKID_Dialog_Colors,		(PFDialogHook)newItems_colorsDialogHook},
    {HOOKID_ToggleBtnColor,		(PFDialogHook)newItems_colorsToggleBtnHook},
    {HOOKID_ListBoxMulti,		(PFDialogHook)newItems_multiListBoxHook},
    {HOOKID_ListBoxSelection,		(PFDialogHook)newItems_selectionListBoxHook},
    {HOOKID_PDMMultiListTests,		(PFDialogHook)newItems_multiListTestsMenusHook},
    {HOOKID_Dialog_2DArrows,		(PFDialogHook)newItems_2DArrowsDialogHook},
    {HOOKID_OptionBtnDefaultPB,		(PFDialogHook)newItems_defaultPBOptionBtnHook},
    {HOOKID_Dialog_MultiList,		(PFDialogHook)newItems_multiListDialogHook},
    {HOOKID_TabPageToggleBtn,		(PFDialogHook)newItems_tabPageToggleBtnHook},
    {HOOKID_TabPageRadioBtns,		(PFDialogHook)newItems_tabPageRadioBtnHook},
    {HOOKID_ComboBoxes,     		(PFDialogHook)newItems_comboBoxHook},
    {HOOKID_ComboStates,     		(PFDialogHook)newItems_comboBoxStatesHook},
    {HOOKID_TabPageList,		(PFDialogHook)newItems_tabPageListHook},
    {HOOKID_TabPage,    		(PFDialogHook)newItems_tabPageHook},
    {HOOKID_SpinBox,    		(PFDialogHook)newItems_spinBoxHook},
    {HOOKID_TabsDialog,    		(PFDialogHook)newItems_tabsDialogHook},
    {HOOKID_MLPushButton,    		(PFDialogHook)newItems_multiLinePushButton},
    {HOOKITEMID_NewItems_ElementLevel,  (PFDialogHook)newItems_handleElementLevelHook },
    {HOOKITEMID_NewItems_ElementColor,	(PFDialogHook)newItems_handleElementColorHook },
    {HOOKITEMID_NewItems_ElementStyle,	(PFDialogHook)newItems_handleElementStyleHook },
    {HOOKITEMID_NewItems_ElementWeight, (PFDialogHook)newItems_handleElementWeightHook },
    };

extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    SymbolSet		   *setP;	/* ptr to "C expression symbol set" */
    RscFileHandle	    rscFileH;	/* a resource file handle */
    char		    buffer[80];
    DItem_PulldownMenuItem  menuItem;

    /* open the resource file that we came out of */
    mdlResource_openFile (&rscFileH, NULL, 0);

Private MdlCommandNumber  commandNumbers [] =

{
{newItems_exit,CMD_NEWITEMS_EXIT},
{newItems_open,CMD_NEWITEMS_OPEN_RADIOBUTTONS},
{newItems_open,CMD_NEWITEMS_OPEN_HSCALES},
{newItems_open,CMD_NEWITEMS_OPEN_VSCALES},
{newItems_open,CMD_NEWITEMS_OPEN_MLPBUTS},
{newItems_open,CMD_NEWITEMS_OPEN_SASH},
{newItems_open,CMD_NEWITEMS_OPEN_VSASH},
{newItems_open,CMD_NEWITEMS_OPEN_BGROUP},
{newItems_open,CMD_NEWITEMS_OPEN_COLORS},
{newItems_open,CMD_NEWITEMS_OPEN_MULTILIST},
{newItems_open,CMD_NEWITEMS_OPEN_2DARROWS},
{newItems_open,CMD_NEWITEMS_OPEN_ALERTS},
{newItems_open,CMD_NEWITEMS_OPEN_TABS},
{newItems_open,CMD_NEWITEMS_OPEN_LEVELATTRIBUTES},
{newItems_openModal,CMD_NEWITEMS_MODALOPEN_RADIOBUTTONS},
{newItems_openModal,CMD_NEWITEMS_MODALOPEN_SCALES},
{newItems_alertCommands,CMD_NEWITEMS_ALERT_CRITICAL},
{newItems_alertCommands,CMD_NEWITEMS_ALERT_INFORMATION},
{newItems_alertCommands,CMD_NEWITEMS_ALERT_QUESTION},
{newItems_alertCommands,CMD_NEWITEMS_ALERT_WARNING},
{newItems_cmdFreeTabPage ,CMD_NEWITEMS_FREETABPAGE},
0,
};
    
    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);


    /* load the command table */
    if (mdlParse_loadCommandTable (NULL) == NULL)
	newItems_errorPrint (MSGID_CommandTable);

    /* set up variables that will be evaluated within C expression strings */
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, FALSE);
    mdlDialog_publishComplexVariable (setP, "newitemsglobals", "niG", &niG);
    mdlDialog_publishBasicVariable (setP, mdlCExpression_getType (TYPECODE_INT), "radioButtonULong", &radioButtonULong);

    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM,   newItems_unloadFunction);
    mdlSystem_setFunction (SYSTEM_LEVEL_CHANGE,     newItems_onLevelChange);
    mdlSystem_setFunction (SYSTEM_FILTER_CHANGE,    newItems_onFilterChange);

    /* publish our hook functions */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    niG.range1 = 0.0;
    niG.range2 = 50;
    niG.range3 = 100.0;
    niG.parameter4 = 1;
    strcpy (niG.comboText, "A Row 1");
    niG.currentLevel = LEVEL_DEFAULT_LEVEL_ID;
    niG.currentColor = 0;
    niG.currentStyle = 0;
    niG.currentWeight = 0;

    mdlResource_loadFromStringList (buffer, NULL, MESSAGELISTID_NewItems, MSGID_NewItems);

    mdlDialog_menuBarRegister (&menuItem, MENUBARID_NewItems, buffer, FALSE, TRUE);

    mdlDialog_menuBarActivate (&menuItem, NULL, 0, FALSE, FALSE, FALSE);

    newItems_initializeColorDescriptors (&niG.localPalP, niG.localColorsP, NLOCAL_COLORS);
    return  SUCCESS;
    }
