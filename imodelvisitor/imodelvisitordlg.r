/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/imodelvisitor/imodelvisitordlg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   imodelvisitor  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:38:44 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   imodelvisitor - imodelvisitor source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogids.h>
#include    <rscdefs.h>

#include    "imodelvisitor.h"
#include    "imodelvisitorcmd.h"
#include    <imodelvisitortext.h>


/*----------------------------------------------------------------------+
|                                                                       |
|    Push Button Items                                                  |
|                                                                       |
+----------------------------------------------------------------------*/





