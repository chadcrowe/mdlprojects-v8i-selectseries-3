/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/imodelvisitor/english/imodelvisitortext.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   imodelvisitor  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:38:45 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   imodelvisitor - imodelvisitor source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#define TXT_DialogTitle		"Native MDL Dialog"
#define TXT_HostedDialogTitle	"Hosted Dialog"
#define TXT_MDLLabel		"This is an MDL label"
