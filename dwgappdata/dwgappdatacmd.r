/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dwgappdata/dwgappdatacmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   delbyidcmd.r - DELBYID Command Table		    	 	|
|									|
|    $Logfile: $
|    $RCSfile: dwgappdatacmd.r,v $
|   $Revision: 1.3.32.1 $
|      	$Date: 2013/07/01 20:38:31 $
|     $Author: George.Dulchinos $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>
/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_DWGAPPDATA       1

DllMdlApp DLLAPP_DWGAPPDATA =
    {
    "DWGAPPDATA", "dwgappdata"          // taskid, dllName
    }


/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE		        0
#define CT_MAIN		        1
#define CT_XDATA	        2
#define CT_XDATA_SHOW           3
#define CT_XDICTIONARY          4
#define CT_XDICTIONARY_SHOW     5

/*----------------------------------------------------------------------+
|                                                                       |
| 	Main commands							|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_MAIN =
{ 
    {  1, CT_XDATA,	    SHOW, NONE,         "XDATA" }, 
    {  2, CT_XDICTIONARY,   SHOW, NONE,         "XDICTIONARY" }, 
};


/*----------------------------------------------------------------------+
|                                                                       |
| 	XDATA commands							|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_XDATA =
{ 
    {  1, CT_NONE,       SHOW, NONE,		"TESTADD" }, 
    {  2, CT_XDATA_SHOW, SHOW, NONE,		"SHOW" }, 
};

/*----------------------------------------------------------------------+
|                                                                       |
| 	XDICTIONARY commands						|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_XDICTIONARY =
{ 
    {  1, CT_XDICTIONARY_SHOW, SHOW, NONE,	"SHOW" }, 
};

/*----------------------------------------------------------------------+
|                                                                       |
| 	XDATA SHOW commands						|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_XDATA_SHOW =
{ 
    {  1, CT_NONE, SHOW, DEF,		        "ALL" }, 
    {  2, CT_NONE, SHOW, NONE,		        "TEST" }, 
    {  3, CT_NONE, SHOW, NONE,		        "DATABASE" }, 
};

/*----------------------------------------------------------------------+
|                                                                       |
| 	XDICTIONARY SHOW commands					|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_XDICTIONARY_SHOW =
{ 
    {  1, CT_NONE, SHOW, DEF,		        "ALL" }, 
    {  2, CT_NONE, SHOW, NONE,		        "DATABASE" }, 
    {  3, CT_NONE, SHOW, NONE,		        "MLINESTYLE" }, 
    {  4, CT_NONE, SHOW, NONE,		        "GROUP" }, 
    
};


