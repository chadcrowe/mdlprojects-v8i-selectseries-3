/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dwgappdata/dwgappdata.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------+
|  DwgAppData - Example program for extracting DWG Application Data from the DGN 
|   representation of a DWG file.
|
|   This application implements the following commands.
|       XDATA TESTADD		    -   Add Test XData to a selected element
|       XDATA SHOW ALL		    -   Show all XData on a selected element
|       XDATA SHOW TEST		    -   Show test data from this application on selected element.
|       XDATA SHOW DATABASE	    -   Show AutoCAD Database related XData on selected element.
|
|       XDICTIONARY SHOW ALL	    -   Show entire XDictionary hierarchy.
|       XDICTIONARY SHOW DATABASE   -   Show DataBase XDictionary hierarchy.
|
+--------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <msdefs.h>
#include    <mselems.h>
#include    <msvar.fdf>
#include    "dwgappdatacmd.h"
#include    <string.h>
#include    <stdio.h>
#include    <toolsubs.h>
#include    <msassoc.fdf>
#include    <msdwgappdata.fdf>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <msstate.fdf>
#include    <msdgnmodelref.fdf>
#include    <msdgnobj.fdf>
#include    <mslocate.fdf>
#include    <mslinkge.fdf>
#include    <msparse.fdf>
#include    <mssystem.fdf>


/*----------------------------------------------------------------------+
|									|
|   Static Data								|
|									|
+----------------------------------------------------------------------*/
static      MSWChar    *s_applicationName		 = L"TEST_APPLICATION";

static      MSWChar    *s_acadDatabaseAppName		 = L"DCO15";
static      MSWChar    *s_acadDatabaseIndexDictionary    = L"ASE_INDEX_DICTIONARY";
static      MSWChar    *s_acadDatabaseConLDefDictionary  = L"CONLDEFDictionary";

/*----------------------------------------------------------------------+
|									|
|   Local Function Declarations						|
|									|
+----------------------------------------------------------------------*/
Private void     displayDictionary
(
MSElementDescr  *pDescr,
MSWChar		*pName, 
DgnFileObjP     fileObj,
int		indent
);


/*----------------------------------------------------------------------+
|                                                                       |
| name          displayIndent                                           |
|                                                                       |
| author        BentleySystems                              04/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayIndent
(
int	    indent
)
    {
    int     i;

    for (i=0; i<indent; i++)
        printf ("    ");
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          displayGroupCode                                        |
|                                                                       |
| author        BentleySystems                              04/02           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayGroupCode
(
XDataValueUnion		*pData,
int			dataType,
ULong			dataSize,
int			groupCode,
DgnFileObjP		fileObj,
int			indent
)
    {
    displayIndent (indent);
    printf ("GroupCode: %d - ", groupCode);
    switch (dataType)
	{
	case XDATAVALUE_Int16:
	    printf ("16 Bit Integer: %d\n", pData->int16);
            break;

	case XDATAVALUE_Int32:
	    printf ("32 Bit Integer: %d\n", pData->int32);
            break;
	
	case XDATAVALUE_Double:	
	    printf ("Double Precision: %f\n", pData->doubleValue);
	    break;

	case XDATAVALUE_ElementId:
	    {
	    MSWChar     applicationName[1024];
	    if (DWGXDATA_Application_Name == groupCode &&
		SUCCESS == mdlRegApp_nameFromId (applicationName, fileObj, pData->elementId))
		{
                printf ("Application Name: %S, ID: %I64x\n", applicationName, pData->elementId);
                }
	    else
                {
		printf ("Element ID: %I64x\n", pData->elementId);
                }
	    break;
            }

	case XDATAVALUE_Point:
	    printf ("Point: (%f, %f, %f)\n", pData->point.x, pData->point.y, pData->point.z);
	    break;

	case XDATAVALUE_String:
	    printf ("String: %s\n", pData->pString);
            free (pData->pString);
	    break;

	case XDATAVALUE_Binary:
	    printf ("Binary Data, %d Bytes <not shown>\n", dataSize);
            free (pData->pBinaryData);
            break;
	}
    }
/*----------------------------------------------------------------------+
|                                                                       |
| name          displayLinkageXData                                     |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayLinkageXData
(
MSElementDescr      *pDescr,
UInt32		    index,
int		    indent,
DgnFileObjP	    fileObj
)
    {
    XDataValueUnion     data;
    int			dataType, groupCode;
    UInt32		dataSize;

    while (SUCCESS == mdlLinkage_getXDataGroupCode (&data, &dataType, &dataSize, &groupCode, &pDescr->el, &index))
	displayGroupCode (&data, dataType, dataSize, groupCode, fileObj, indent);
    }
 
/*----------------------------------------------------------------------+
|                                                                       |
| name          acceptAddXData                                          |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     acceptAddXData
(
)
    {
    MSElementDescr      *pDescr = NULL;
    DgnModelRefP	modelRef;
    long		filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &modelRef);
    
    if (0 != mdlElmdscr_read (&pDescr, filePos, modelRef, FALSE, NULL))
        {
	ElementId       regAppID;
	
	if (SUCCESS == mdlRegApp_idFromName (&regAppID, s_applicationName, mdlModelRef_getDgnFile (modelRef), TRUE))
	    {
	    char	    *pTestString = "Test String";
	    DPoint3d	    testPoint = {1.0, 2.0, 3.0};
	    
	    mdlLinkage_addXDataGroupCode (&pDescr, DWGXDATA_Application_Name, &regAppID, sizeof(regAppID));
	    mdlLinkage_addXDataGroupCode (&pDescr, DWGXDATA_String, pTestString, strlen (pTestString));
	    mdlLinkage_addXDataGroupCode (&pDescr, DWGXDATA_ControlString, "{", 1);
	    mdlLinkage_addXDataGroupCode (&pDescr, DWGXDATA_Point, &testPoint, sizeof(&testPoint));
	    mdlLinkage_addXDataGroupCode (&pDescr, DWGXDATA_ControlString, "}", 1);
	    
	    mdlElmdscr_rewrite (pDescr, NULL, filePos);
	    }
	mdlElmdscr_freeAll (&pDescr);
	}
    mdlLocate_restart (TRUE);    
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          addXData                                                |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
void addXData
(
char    *unparsedP
)
//cmdNumber       CMD_XDATA_TESTADD
    {
    mdlState_startModifyCommand (addXData, acceptAddXData, NULLFUNC, NULLFUNC, NULLFUNC, 0, 0, 0, 0);
    mdlLocate_init ();
    mdlLocate_normal ();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          acceptShowXData                                         |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     acceptShowXData
(
MSWChar	    *pApplicationName
)
    {
    MSElementDescr      *pDescr = NULL;
    DgnModelRefP	modelRef;
    UInt32		filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &modelRef);
    DgnFileObjP		fileObj =  mdlModelRef_getDgnFile (modelRef);
    ElementID		regAppID = 0;
    
    if (NULL != pApplicationName &&
	SUCCESS != mdlRegApp_idFromName (&regAppID, pApplicationName, fileObj, FALSE))
        {
	printf ("The Application: %S is not registered\n", pApplicationName);
	return;
	}
    
    if (0 != mdlElmdscr_read (&pDescr, filePos, modelRef, FALSE, NULL))
        {
	UInt32		index = 0;

	if (NULL == pApplicationName ||
	    SUCCESS == mdlLinkage_findApplicationXData (&index, &pDescr->el, regAppID))
	    {
	    displayLinkageXData (pDescr, index, 0, mdlModelRef_getDgnFile (pDescr->h.dgnModelRef));
	    }
	else
	    {
	    printf ("This element contains no application Data for application: %S\n", pApplicationName);
	    }
	mdlElmdscr_freeAll (&pDescr);
	}
    mdlLocate_restart (TRUE);    
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          acceptShowTestXData                                     |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     acceptShowTestXData
(
)
    {
    acceptShowXData (s_applicationName);
    }
    
/*----------------------------------------------------------------------+
|                                                                       |
| name          showTestXDataCommand                                    |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
void showTestXDataCommand
(
char    *unparsedP
)
//cmdNumber       CMD_XDATA_SHOW_TEST
    {
    mdlState_startModifyCommand (showTestXDataCommand, acceptShowTestXData, NULLFUNC, NULLFUNC, NULLFUNC, 0, 0, 0, 0);
    mdlLocate_init ();
    mdlLocate_normal ();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          acceptShowAllXData                                      |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     acceptShowAllXData
(
)
    {
    acceptShowXData (NULL);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          showAllXDataCommand                                     |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
void showAllXDataCommand
(
char    *unparsedP
)
//cmdNumber       CMD_XDATA_SHOW_ALL
    {
    mdlState_startModifyCommand (showAllXDataCommand, acceptShowAllXData, NULLFUNC, NULLFUNC, NULLFUNC, 0, 0, 0, 0);
    mdlLocate_init ();
    mdlLocate_normal ();
    }

 
/*----------------------------------------------------------------------+
|                                                                       |
| name          acceptShowDatabaseXData                                 |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     acceptShowDatabaseXData
(
)
    {
    acceptShowXData (s_acadDatabaseAppName);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          showDatabaseXDataCommand                                |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
void showDatabaseXDataCommand
(
)
//cmdNumber       CMD_XDATA_SHOW_DATABASE
    {
    mdlState_startModifyCommand (showDatabaseXDataCommand, acceptShowDatabaseXData, NULLFUNC, NULLFUNC, NULLFUNC, 0, 0, 0, 0);
    mdlLocate_init ();
    mdlLocate_normal ();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          displayXRecord                                          |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayXRecord
(
MSElementDescr  *pDescr,
MSWChar		*pName, 
DgnFileObjP     fileObj,
int		indent
)
    {
    int		dataSize = 0;
    void	*pData = NULL;
    
    displayIndent (indent);
    printf ("XRecord ID: %I64x, Name: %S\n", pDescr->el.ehdr.uniqueId, pName);
    
    if (SUCCESS == mdlXRecord_extractGroupData (&pData, &dataSize, pDescr))
	{
	int			valueType, groupCode;
	UInt32			groupCodeDataSize, index = 0;
	XDataValueUnion		groupCodeValue;
	
	while (SUCCESS == mdlXRecord_getGroupCode (&groupCodeValue, &valueType, &groupCodeDataSize, &groupCode, &index, pData, dataSize))
	    displayGroupCode (&groupCodeValue, valueType, groupCodeDataSize, groupCode, fileObj, indent + 1);
	
	free (pData);
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          displayObjectPtr					|
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayObjectPtr
(
MSElementDescr  *pDescr,
MSWChar		*pName, 
int		indent
)
    {
    int		dataBits, dataSize;

    if (SUCCESS == mdlDwgObjectPtr_extract (&dataBits, NULL, &dataSize, pDescr))
	{
	displayIndent (indent);

        printf ("ObjectPtr: %S, ID: %I64x, DataBits: %d, DataSize: %d\n", pName, pDescr->el.ehdr.uniqueId, dataBits, dataSize);

	displayLinkageXData (pDescr, 0, indent + 1, mdlModelRef_getDgnFile (pDescr->h.dgnModelRef));
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          displayProxyObject					|
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayProxyObject
(
MSElementDescr  *pDescr,
MSWChar		*pName, 
int		indent
)
    {
    int		dataBits, objectDrawingFormat, origDataFormat, classId, dataSize;

    if (SUCCESS == mdlDwgProxyObject_extract (&dataBits, &objectDrawingFormat, &origDataFormat, &classId, NULL, &dataSize, NULL, NULL, NULL, pDescr))
	{
	displayIndent (indent);

        printf ("Proxy Object: %S, ID: %I64x, DataBits: %d, Drawing Format: %d, origDataFormat: %d, classID: %d DataSize: %d\n", pName, pDescr->el.ehdr.uniqueId, dataBits, objectDrawingFormat, origDataFormat, classId, dataSize);

	displayLinkageXData (pDescr, 0, indent + 1, mdlModelRef_getDgnFile (pDescr->h.dgnModelRef));
        }
    }



/*----------------------------------------------------------------------+
|                                                                       |
| name          displayDictionaryEntry					|
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayDictionaryEntry
(
MSElementDescr  *pDescr,
MSWChar		*pName, 
DgnFileObjP     fileObj,
int		indent
)
    {                                                              
    if (mdlElement_isDictionary (&pDescr->el))
        {
	displayDictionary (pDescr, pName, fileObj, indent);
        }
    else if (mdlElement_isXRecord (&pDescr->el))
        {
	displayXRecord (pDescr, pName, fileObj, indent);
        }
    else if (mdlElement_isObjectPtr (&pDescr->el))
        {
        displayObjectPtr (pDescr, pName, indent);
        }
    else if (mdlElement_isProxyObject (&pDescr->el))
        {
        displayProxyObject (pDescr, pName, indent);
        }
    else
        {
	displayIndent (indent);
	printf ("Element: %S, Type: %d, Level: %d\n", pName, pDescr->el.ehdr.type, pDescr->el.ehdr.level);
	}
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          displayDictionary					|           
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     displayDictionary
(
MSElementDescr  *pDescr,
MSWChar		*pName, 
DgnFileObjP     fileObj,
int		indent
)
    {
    MSElementDescr  *pChild;
    
    displayIndent (indent);
    printf ("Dictionary: %S, ID: %I64x\n", pName, pDescr->el.ehdr.uniqueId);
    for (pChild = pDescr->h.firstElem; NULL != pChild; pChild = pChild->h.next)
        {
	MSWChar		    entryName[1024];
	ElementId	    entryId;
	MSElementDescr      *pEntryDescr = NULL;
	
        if (SUCCESS == mdlDictionary_extractEntry (&entryId, entryName, 1024, pChild))
	    {
            if (SUCCESS ==  mdlAssoc_getElementDescr (&pEntryDescr, NULL, entryId, MASTERFILE, FALSE))
		{
		displayDictionaryEntry (pEntryDescr, entryName, fileObj, indent+1);
		mdlElmdscr_freeAll (&pEntryDescr);
		}
	    else
		{
		displayIndent (indent);
		printf ("No Element Found for %S, ID: %I64x\n", entryName, entryId);
		}
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          showAllDictionaries                                     |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     showAllDictionaries
(
char    *unparsedP
)
//cmdNumber       CMD_XDICTIONARY_SHOW_ALL
    {
    MSElementDescr      *pMainDictionary = NULL;
    
    if (SUCCESS != mdlAssoc_getElementDescr (&pMainDictionary, NULL, tcb->mainDictionaryId, MASTERFILE, FALSE))
        printf ("Main Dictionary Not Found");
	
    displayDictionary (pMainDictionary, L"Main Dictionary", mdlDgnFileObj_getMasterFile(), 0);
    
    mdlElmdscr_freeAll (&pMainDictionary);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          showGroupDictionary					|
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     showGroupDictionary
(
char    *unparsedP
)
//cmdNumber       CMD_XDICTIONARY_SHOW_GROUP
    {
    MSElementDescr      *pDictionary = NULL;
    
    if (SUCCESS == mdlDictionary_findDictionary (&pDictionary, L"ACAD_GROUP", MASTERFILE))
        {
	displayDictionary (pDictionary, L"ACAD_GROUP", mdlDgnFileObj_getMasterFile(), 0);
        mdlElmdscr_freeAll (&pDictionary);
	}
    }
    
    
/*----------------------------------------------------------------------+
|                                                                       |
| name          showMLineStyleDictionary				|
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     showMLineStyleDictionary
(
char    *unparsedP
)
//cmdNumber       CMD_XDICTIONARY_SHOW_MLINESTYLE
    {
    MSElementDescr      *pDictionary = NULL;
    
    if (SUCCESS == mdlDictionary_findDictionary (&pDictionary, L"ACAD_MLINESTYLE", MASTERFILE))
        {
	displayDictionary (pDictionary, L"ACAD_MLINESTYLE", mdlDgnFileObj_getMasterFile(), 0);
        mdlElmdscr_freeAll (&pDictionary);
	}
    }
    
    
    
/*----------------------------------------------------------------------+
|                                                                       |
| name          showDatabaseDictionaries                                |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
Private void     showDatabaseDictionaries
(
char    *unparsedP
)
//cmdNumber       CMD_XDICTIONARY_SHOW_DATABASE
    {
    MSElementDescr      *pDictionary = NULL;

    if (SUCCESS == mdlDictionary_findDictionary (&pDictionary, s_acadDatabaseIndexDictionary, MASTERFILE))
        {
	displayDictionary (pDictionary, s_acadDatabaseIndexDictionary, mdlDgnFileObj_getMasterFile(), 0);
        mdlElmdscr_freeAll (&pDictionary);
        }
    if (SUCCESS == mdlDictionary_findDictionary (&pDictionary, s_acadDatabaseConLDefDictionary, MASTERFILE))
        {
	displayDictionary (pDictionary, s_acadDatabaseConLDefDictionary, mdlDgnFileObj_getMasterFile(), 0);
        mdlElmdscr_freeAll (&pDictionary);
        }
    }
    
 
/*----------------------------------------------------------------------+
|                                                                       |
| name          main                                                    |
|                                                                       |
| author        BentleySystems                              04/02       |
|                                                                       |
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int	argc,	/* => Number of arguments passed in pargv */
char   *argv[]	/* => Array of pointers to arguments */
)
    {
    MdlCommandNumber    commandNumbers [] = 
        {
        {showAllXDataCommand, CMD_XDICTIONARY_SHOW_ALL },
        {showDatabaseDictionaries, CMD_XDICTIONARY_SHOW_DATABASE },
        {showMLineStyleDictionary, CMD_XDICTIONARY_SHOW_MLINESTYLE },
        {showGroupDictionary, CMD_XDICTIONARY_SHOW_GROUP },
        {showAllXDataCommand, CMD_XDATA_SHOW_ALL },
        {showTestXDataCommand,CMD_XDATA_SHOW_TEST },
        {addXData, CMD_XDATA_TESTADD },
        0
        };
    mdlSystem_registerCommandNumbers (commandNumbers);

    mdlParse_loadCommandTable (NULL);
    return  SUCCESS;
    }
