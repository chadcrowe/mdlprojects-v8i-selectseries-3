/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp4/testapp4.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp4/testapp4.h_v  $
|   $Workfile:   testapp4.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in testapp4 dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __testapp4H__
#define	    __testapp4H__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_testapp4		1   /* dialog id for testapp4 Dialog */
#define DIALOGID_testapp4Modal	2   /* dialog id for testapp4 Modal Dialog */

#define OPTIONBUTTONID_testapp4	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_testapp4		1   /* id for "parameter 1" text item */
#define TOGGLEID_testapp4		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_testapp4		1   /* id for synonym resource */

#define MESSAGELISTID_testapp4Errors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_testapp4Dialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_testapp4	1   /* id for toggle item hook func */
#define HOOKDIALOGID_testapp4		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct testapp4globals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } testapp4Globals;

#endif
