
#define PI 3.1415926535897932384626433
#include    <mdl.h>
#include    <toolsubs.h>
#include    <basetype.h>
#include    <malloc.h>
#include    <msrmgr.h>
#include    <mstypes.h>
#include    <string.h>
#include    <msparse.fdf>
#include    <msstate.fdf>
#include    <mssystem.fdf>
#include    <dlmsys.fdf>
#include    <mscnv.fdf>
#include    <msfile.fdf>
#include    <msdialog.fdf>
#include    <dlogids.h>
#include    <mswindow.fdf>
#include    <msdgnmodelref.fdf>
#include    <msdgnlib.fdf>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mscell.fdf>
#include    <msmline.fdf>
#include    <mstextstyle.fdf>
#include    <listmodel.fdf>

#include    <mselems.h>
#include    <scanner.h>
#include    <userfnc.h>
#include    <cmdlist.h>
#include    <string.h>
#include    <toolsubs.h>
#include    <dlogman.fdf>
#include    <mssystem.fdf>
#include    <mslinkge.fdf>
#include    <msscan.fdf>
#include    <msoutput.fdf>
#include    <msparse.fdf>
#include    <mselemen.fdf>
#include    <msrsrc.fdf>
#include    <mslocate.fdf>
#include    <msstate.fdf>
#include    <msscancrit.fdf>

#include <mstypes.h>
#include <msscancrit.fdf>
#include <elementref.h>

#include <msdgnobj.fdf>

#include    <msdgncache.h>
#include    <ditemlib.fdf>
//#include	<FontManager.h>
#include    <mselementtemplate.fdf>
#include    <msdimstyle.fdf>
#include    <msdim.fdf>
#include    <namedexpr.fdf>
#if !defined (DIM)
#define DIM(a) ((sizeof(a)/sizeof((a)[0])))
#endif


USING_NAMESPACE_BENTLEY_USTN
USING_NAMESPACE_BENTLEY_USTN_ELEMENT
USING_NAMESPACE_TEXT
#include <iostream>
#define LINEAR_TEMPLATE_NAME      L"CreatedGroup\\Created Linear Template"
#define SHAPE_TEMPLATE_NAME       L"CreatedGroup\\Created Shape Template"
#define HATCH_TEMPLATE_NAME       L"CreatedGroup\\Created Hatch Template"
#define AREAPATTERN_TEMPLATE_NAME L"CreatedGroup\\Created AreaPattern Template"
#define CELL_TEMPLATE_NAME        L"CreatedGroup\\Created Cell Template"
#define TEXT_TEMPLATE_NAME        L"CreatedGroup\\Created Text Template"
#define MLINE_TEMPLATE_NAME       L"CreatedGroup\\Created MLine Template"
#define DIMENSION_TEMPLATE_NAME   L"CreatedGroup\\Created Dimension Template"
#define HEADER_TEMPLATE_NAME      L"CreatedGroup\\Header"
#define COMPONENT1_TEMPLATE_NAME  L"CreatedGroup\\Component1"
#define COMPONENT2_TEMPLATE_NAME  L"CreatedGroup\\Component2"
#include <iostream>
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
int ScanForLinearElements(DgnModelRefP modelRef);
int ElementRefScanCallback(ElementRef, void *callbackArg, ScanCriteriaP);
#include <sstream>
#include <msselect.fdf>
#include <msmisc.fdf>
#include <ElemHandle.h>
#include <ElementAgenda.h>
#include <msreffil.fdf>
__declspec(dllexport) ElementID mdlElement_getID();

using namespace std;

typedef struct scancallback_
{
	int numVerts;

} ScanCallBackData;

extern "C"  DLLEXPORT int   MdlMain
(){
	DgnModelRefP    dgnModelRef = NULL;
	DgnFileObjP     dgnFile;
	ModelID         modelID;
	int             status;

	dgnFile = mdlDgnFileObj_getMasterFile();

	modelID = mdlModelRef_getModelID(ACTIVEMODEL);

	status = mdlModelRef_createWorking(&dgnModelRef, dgnFile, modelID, FALSE, FALSE);

	int number_of_line_elements = ScanForLinearElements(dgnModelRef);



	mdlDialog_cmdNumberQueue(FALSE, CMD_MDL_UNLOAD, mdlSystem_getCurrTaskID(), TRUE);
	return SUCCESS;


}
/*Scan Function

//	The scanning function that does most of the work*/
int ScanForLinearElements(DgnModelRefP modelRef)
{
	UShort              typeMask[6];
	ScanCallBackData    data;
	data.numVerts = 0;
	ScanCriteriaP	criteria = mdlScanCriteria_create();
	mdlScanCriteria_setModel(criteria, modelRef);
	//	Tell the scanner to call our function for each element found
	mdlScanCriteria_setReturnType(criteria, MSSCANCRIT_ITERATE_ELMREF, FALSE, TRUE);
	mdlScanCriteria_setElemRefCallback(criteria, ElementRefScanCallback, &data);
	//	Tell the scanner we're interested only in certain types of element
	//	See mselems.h for the TMSK0_ macros 
	memset(typeMask, 0, sizeof(typeMask));
	typeMask[0] = TMSK0_TEXT_NODE;
	typeMask[1] = TMSK1_TEXT;
	mdlScanCriteria_setElementTypeTest(criteria, typeMask, sizeof(typeMask));
	//	Execute the scan
	mdlScanCriteria_scan(criteria, NULL, NULL, NULL);
	mdlScanCriteria_free(criteria);
	return data.numVerts;
}

/*Your Callback Function

//	Scan callback function set by mdlScanCriteria_setElemRefCallback */
int ElementRefScanCallback(ElementRef eleR, void *callbackArg, ScanCriteriaP)
{
	ScanCallBackData*	data = (ScanCallBackData*)callbackArg;

	DgnModelRefP dgnModelRef = mdlModelRef_getActive();

	if (elementRef_isHilited(eleR, dgnModelRef)){
		EditElemHandle eh(eleR, dgnModelRef);

		MSElementP eleP;

		UInt32 filePos = elementRef_getFilePos(eleR);

		eleP = eh.GetElementP();

		//mdlTextNode_createEx(eleP);
		ElementID eleID = mdlElement_getID(eleP);
		mdlSelect_removeElement(filePos, dgnModelRef, TRUE);
		int format = 0;
		BoolInt threeD = 0;
		DgnModelRefP workingModel = 0;
		mdlWorkDgn_openFile(&workingModel, &format, &threeD, "attachment file path", L"attachment model name", FALSE)
		//mdlElement_rewrite(eleP, NULL, filePos);
	}
		
	//	We're doing nothing but count elements
	return SUCCESS;
}