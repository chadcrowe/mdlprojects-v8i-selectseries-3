/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/english/mdlbatxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mdlbasic/english/mdlbatxt.h_v  $
|   $Workfile:   mdlbatxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:37 $
|									|
+----------------------------------------------------------------------*/

#if !defined (__mdlbatxtH__)
#define       __mdlbatxtH__

#define TXT_ColumnWidth  	"Column Width"
#define TXT_RowHeight 		"Row Height"
#define TXT_TextHeight 		"Text Height"
#define TXT_TextWidth       	"Text Width"
#define TXT_FileName	    	"File Name:"
#define TXT_WantHdgSep 		"Want Hdg Sep"
#define TXT_WantColumnSep 	"Want Column Sep"
#define TXT_WantRowSep 		"Want Row Sep"
#define TXT_LineWeight 		"Line Weight"
#define TXT_LineStyle 		"Line Style"
#define TXT_BasicDialogTitle	"Basic Variable Values"

#endif	/* if !defined (__mdlbatxtH__) */
