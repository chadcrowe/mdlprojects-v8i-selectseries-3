/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/english/mdlbamsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mdlbasic/english/mdlbamsg.r_v  $
|   $Workfile:   mdlbamsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:36 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   mdlbamsg.r                                                          |
|                                                                       |
+----------------------------------------------------------------------*/
#include "rscdefs.h"

#include "mdlbadlg.h"
	    
MessageList MESSAGELISTID_Messages =
{
    {
    { MSGID_LoadCmdTableError, 	"Unable to load command table" },
    }
};

    

