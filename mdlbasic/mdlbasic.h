/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/mdlbasic.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mdlbasic/mdlbasic.h_v  $
|   $Workfile:   mdlbasic.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:33 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   mdlbasic.h
|                                                                       |
+----------------------------------------------------------------------*/
#if !defined (__mdlbasicH__)
#define       __mdlbasicH__

/*----------------------------------------------------------------------+
|									|
|   TypeDefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct tablesymbology
{
    short color;
    short weight;
    short style;
} TableSymbology;

typedef struct  mdltableparams
{
    double columnWidth;
    double rowHeight;
    double textHeight;
    double textWidth;
    short  wantHdgSep;
    short  wantColSep;
    short  wantRowSep;
    TableSymbology lineSymb[5];
} MdlTableParams;

#endif	/* if !defined (__mdlbasicH__) */