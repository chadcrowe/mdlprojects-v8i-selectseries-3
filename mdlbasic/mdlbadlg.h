/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/mdlbadlg.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mdlbasic/mdlbadlg.h_v  $
|   $Workfile:   mdlbadlg.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:28 $
|									|
+----------------------------------------------------------------------*/

#if !defined (__mdlbadlgH__)
#define       __mdlbadlgH__

#define DIALOGID_Table			1

#define TEXTID_ColumnWidth		1
#define TEXTID_RowHeight		2
#define TEXTID_TxtHeight		3
#define TEXTID_TxtWidth		    	4
#define TEXTID_Weight1			5
#define TEXTID_Weight2			6
#define TEXTID_Weight3			7
#define TEXTID_Weight4			8
#define TEXTID_Style1			9
#define TEXTID_Style2			10
#define TEXTID_Style3			11
#define TEXTID_Style4			12

#define COLORPICKERID_Color1	    	13
#define COLORPICKERID_Color2	    	14
#define COLORPICKERID_Color3	    	15
#define COLORPICKERID_Color4	    	16

#define TOGGLEID_WantHdgSep	     	17
#define TOGGLEID_WantColSep	    	18
#define TOGGLEID_WantRowSep	    	19

#define TEXTID_FileName			20

#define MESSAGELISTID_Messages      	1

#define MSGID_LoadCmdTableError     	1

#endif	/* if !defined (__mdlbadlgH__) */