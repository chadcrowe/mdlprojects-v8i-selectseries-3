/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/mdlbasic.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   mdlbasic.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:39:31 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <cexpr.h>
#include <rscdefs.h>
#include <cmdlist.h>            /* MicroStation Command List */
#include <tcb.h>		/* Terminal Control Block */

#include <msoutput.fdf>
#include <msbasic.fdf>
#include <msdialog.fdf>
#include <ditemlib.fdf>
#include <mscexpr.fdf>
#include <msrsrc.fdf>
#include <mssystem.fdf>
#include <msparse.fdf>

#include "mdlbacmd.h"		/* Command Table for MDLBasic */
#include "mdlbasic.h"		/* Constants for MDL application */
#include "mdlbadlg.h"		/* Dialog Box Constants */

/*----------------------------------------------------------------------+
|									|
|   Global Variables    						|
|									|
+----------------------------------------------------------------------*/
    MdlTableParams  tableInfo;
    char     	    fileName[255];

/*----------------------------------------------------------------------+
 The following sequence of events occur:
 1) The BASIC macro populates global variables with information and opens a dialog box
 to display this information to the user.  If the user presses the OK
 button, then the MDL application MDLBASIC is loaded.  
 2) In MDL application:
 The application gets the global information using mdlBasic_getPublicVariable and
 displays it to the user in an MDL dialog box.  The user has the opportunity
 to change the information.  If the user presses the OK button, then
 the information is written back to the global variables with
 mdlBasic_setPublicVariable.  The MDL application is unloaded.
 3) In the BASIC macro:
 The changed information is then displayed by this macro in the dialog box.
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
| name	   mdlBasicDraw						    	|
|									|
| author   BSI					    6/95		|
|									|
+----------------------------------------------------------------------*/
void mdlBasicDraw
(
char*   unparsedP
)
//cmdNumber CMD_MDLBASIC_DRAW
{
    /*----------------------------------------------------------------------+
    |
    |	This routine accesses a group of public variables in the
    |	MicroStation BASIC enviroment and opens a dialog box to display these
    |   values. The modified values are then updated back into the 
    |	MicroStation BASIC environment.
    |
    +-----------------------------------------------------------------------*/
    int		    status; 	    /* Status of Get/Set operations    */
    BoolInt  	    status_TF;      /* Status of Open Modal True/False */
    int	     	    lastAction;     /* Action Button Pressed on Modal */

    /* Get MicroStation BASIC variables */
    /* Basic Variables must be retrived/set at the atomic level */
    /* each item retrived from a MicroStation BASIC Public variable
       must be a simple type no records */
    status = mdlBasic_getPublicVariable (&tableInfo.columnWidth, sizeof(double), 
						    "tableInfo.columnWidth");
    status = mdlBasic_getPublicVariable (&tableInfo.rowHeight, sizeof(double),  
						    "tableInfo.rowHeight");
    status = mdlBasic_getPublicVariable (&tableInfo.textHeight, sizeof(double),  
						    "tableInfo.textHeight");
    status = mdlBasic_getPublicVariable (&tableInfo.textWidth, sizeof(double),  
						    "tableInfo.textWidth");

    status = mdlBasic_getPublicVariable (&tableInfo.wantHdgSep, sizeof(short),  
						    "tableInfo.wantHdgSep");
    status = mdlBasic_getPublicVariable (&tableInfo.wantColSep, sizeof(short),  
						    "tableInfo.wantColSep");
    status = mdlBasic_getPublicVariable (&tableInfo.wantRowSep, sizeof(short),  
						    "tableInfo.wantRowSep");

    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[1].color,  sizeof(short),  
						    "lineSymb_outline.color");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[1].weight, sizeof(short),  
						    "lineSymb_outline.weight");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[1].style,  sizeof(short),  
						    "lineSymb_outline.style");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[2].color,  sizeof(short),  
						    "lineSymb_hdgsep.color");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[2].weight, sizeof(short),  
						    "lineSymb_hdgsep.weight");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[2].style,  sizeof(short),  
						    "lineSymb_hdgsep.style");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[3].color,  sizeof(short),  
						    "lineSymb_rowsep.color");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[3].weight, sizeof(short),  
						    "lineSymb_rowsep.weight");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[3].style,  sizeof(short),  
						    "lineSymb_rowsep.style");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[4].color,  sizeof(short),  
						    "lineSymb_colsep.color");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[4].weight, sizeof(short),  
						    "lineSymb_colsep.weight");
    status = mdlBasic_getPublicVariable (&tableInfo.lineSymb[4].style,  sizeof(short),  
						    "lineSymb_colsep.style");

    status = mdlBasic_getPublicVariable (fileName, sizeof(fileName),"fileName");

    /* open Dialog Box */
    status_TF = mdlDialog_openModal (&lastAction, NULL, DIALOGID_Table);
    if (ACTIONBUTTON_CANCEL == lastAction)
    	{
	/* user pressed cancel - unload the application & return */
    	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
	return;
    	}

    /* Return values to MicroStation BASIC */
    status = mdlBasic_setPublicVariable (&tableInfo.columnWidth, sizeof(double),
						    	"tableInfo.columnWidth");
    status = mdlBasic_setPublicVariable (&tableInfo.rowHeight, sizeof(double), 
							"tableInfo.rowHeight");
    status = mdlBasic_setPublicVariable (&tableInfo.textHeight, sizeof(double), 
						    	"tableInfo.textHeight");
    status = mdlBasic_setPublicVariable (&tableInfo.textWidth, sizeof(double), 
						    	"tableInfo.textWidth");
    status = mdlBasic_setPublicVariable (&tableInfo.wantHdgSep, sizeof(short), 
						    	"tableInfo.wantHdgSep");
    status = mdlBasic_setPublicVariable (&tableInfo.wantColSep, sizeof(short), 
						    	"tableInfo.wantColSep");
    status = mdlBasic_setPublicVariable (&tableInfo.wantRowSep, sizeof(short), 
						    	"tableInfo.wantRowSep");

    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[1].color,  sizeof(short), 
						    	"lineSymb_outline.color");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[1].weight, sizeof(short), 
						    	"lineSymb_outline.weight");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[1].style,  sizeof(short), 
						    	"lineSymb_outline.style");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[2].color,  sizeof(short), 
						    	"lineSymb_hdgsep.color");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[2].weight, sizeof(short), 
						    	"lineSymb_hdgsep.weight");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[2].style,  sizeof(short), 
						    	"lineSymb_hdgsep.style");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[3].color,  sizeof(short), 
							"lineSymb_rowsep.color");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[3].weight, sizeof(short), 
						    	"lineSymb_rowsep.weight");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[3].style,  sizeof(short), 
						    	"lineSymb_rowsep.style");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[4].color,  sizeof(short), 
						    	"lineSymb_colsep.color");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[4].weight, sizeof(short), 
						    	"lineSymb_colsep.weight");
    status = mdlBasic_setPublicVariable (&tableInfo.lineSymb[4].style,  sizeof(short), 
							"lineSymb_colsep.style");
    status = mdlBasic_setPublicVariable (fileName, sizeof(fileName),"fileName");

    /* Unload this Application */
    mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);

    return;
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI				    6/95		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int	     argc,
char	    *argv[]
)
    {
    RscFileHandle	rfHandle;  /* File Handle for Resource File */
    SymbolSet		*setP;     /* Published Variable Set Pointer */


    mdlResource_openFile (&rfHandle, NULL, FALSE);

    MdlCommandNumber commandNumbers [] =
        {
        {mdlBasicDraw, CMD_MDLBASIC_DRAW},
        0
        };

    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);


    /*    mdlParse_loadCommandTable: */
    if (mdlParse_loadCommandTable (NULL) == NULL)
    	{
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Messages,
	    	MSGID_LoadCmdTableError);
    	return SUCCESS;    	
    	}

    /* Publish variables to allow dialog box manager to display values */
    setP=mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);
    mdlDialog_publishComplexVariable (setP, "mdltableparams", "tableInfo", &tableInfo);
    mdlDialog_publishBasicArray (setP, mdlCExpression_getType (TYPECODE_CHAR) , "fileName",
				 fileName, sizeof(fileName));

    /* Queue a command to process information from the BASIC program */
    mdlDialog_cmdNumberQueue (TRUE, CMD_MDLBASIC_DRAW,
				      NULL, TRUE);
    return  SUCCESS;

    }   
