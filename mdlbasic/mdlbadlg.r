/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/mdlbadlg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   mdlbadlg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:29 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>
#include <dlogbox.h>
#include <dlogids.h>
#include <keys.h>
#include <msdefs.h>

#include "mdlbadlg.h"
#include "mdlbatxt.h"

/*----------------------------------------------------------------------+
|                                                                       |
|   Dialog Box                                                          |
|                                                                       |
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_Table =
    {
    DIALOGATTR_DEFAULT,
    60*XC, 18*YC,
    NOHELP,    MHELP,
    NOHOOK,
    NOPARENTID,
    TXT_BasicDialogTitle,	  
{
{{13*XC, GENY(1),  0, 0}, Text,         TEXTID_FileName,      ON, 0,  "", ""},
{{13*XC, GENY(2),  0, 0}, Text,         TEXTID_ColumnWidth,   ON, 0,  "", ""},
{{13*XC, GENY(3),  0, 0}, Text,         TEXTID_RowHeight,     ON, 0,  "",   ""},
{{13*XC, GENY(4),  0, 0}, Text,         TEXTID_TxtHeight,     ON, 0,  "", ""},
{{13*XC, GENY(5),  0, 0}, Text,         TEXTID_TxtWidth,      ON, 0,  "", ""},
{{ 8*XC, GENY(6),  0, 0}, ToggleButton, TOGGLEID_WantHdgSep,  ON, 0,  "", ""},
{{ 8*XC, GENY(7),  0, 0}, ToggleButton, TOGGLEID_WantColSep,  ON, 0,  "", ""},
{{ 8*XC, GENY(8),  0, 0}, ToggleButton, TOGGLEID_WantRowSep,  ON, 0,  "", ""},
{{13*XC, GENY(9),  0, 0}, ColorPicker,  COLORPICKERID_Color1, ON, 0,  "", ""},
{{13*XC, GENY(10), 0, 0}, Text,         TEXTID_Weight1,       ON, 0,  "", ""},
{{13*XC, GENY(11), 0, 0}, Text,         TEXTID_Style1,        ON, 0,  "", ""},
{{40*XC, GENY(2),  0, 0}, ColorPicker,  COLORPICKERID_Color2, ON, 0,  "", ""},
{{40*XC, GENY(3),  0, 0}, Text,         TEXTID_Weight2,       ON, 0,  "", ""},
{{40*XC, GENY(4),  0, 0}, Text,         TEXTID_Style2,        ON, 0,  "", ""},
{{40*XC, GENY(5),  0, 0}, ColorPicker,  COLORPICKERID_Color3, ON, 0,  "", ""},
{{40*XC, GENY(6),  0, 0}, Text,         TEXTID_Weight3,       ON, 0,  "", ""},
{{40*XC, GENY(7),  0, 0}, Text,         TEXTID_Style3,        ON, 0,  "", ""},
{{40*XC, GENY(8),  0, 0}, ColorPicker,  COLORPICKERID_Color4, ON, 0,  "", ""},
{{40*XC, GENY(9),  0, 0}, Text,         TEXTID_Weight4,       ON, 0,  "", ""},
{{40*XC, GENY(10), 0, 0}, Text,         TEXTID_Style4,        ON, 0,  "", ""},
{{38*XC, GENY(12), 12*XC, 0}, PushButton, PUSHBUTTONID_Cancel, ON, 0, "", ""},
{{21*XC, GENY(12), 12*XC, 0}, PushButton, PUSHBUTTONID_OK, ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Item Instances                                                      |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   PushButton Item Resource                                            |
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|                                                                       |
|   Text Item Resource                                         		|
|                                                                       |
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_ColumnWidth =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%lf",
    "%lf",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_ColumnWidth,
    "tableInfo.columnWidth"
    };

DItem_TextRsc TEXTID_RowHeight =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%lf",
    "%lf",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_RowHeight,
    "tableInfo.rowHeight"
    };
DItem_TextRsc TEXTID_TxtHeight =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%lf",
    "%lf",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_TextHeight,
    "tableInfo.textHeight"
    };
DItem_TextRsc TEXTID_TxtWidth =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%lf",
    "%lf",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_TextWidth,
    "tableInfo.textWidth"
    };

DItem_TextRsc TEXTID_FileName =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    MAXFILELENGTH,
    "%s",
    "%s",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT | TEXT_READONLY | TEXT_ABBREVFILENAME,
    TXT_FileName,
    "fileName"
    };
/*----------------------------------------------------------------------+
|                                                                       |
|   Toggle Item Resource                                                |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEID_WantHdgSep = 
{
    NOCMD, MCMD,
    NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, NOMASK, NOINVERT,
    TXT_WantHdgSep,
    "tableInfo.wantHdgSep"
};

DItem_ToggleButtonRsc TOGGLEID_WantColSep = 
{
    NOCMD, MCMD,
    NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, NOMASK, NOINVERT,
    TXT_WantColumnSep,
    "tableInfo.wantColSep"
};
DItem_ToggleButtonRsc TOGGLEID_WantRowSep = 
{
    NOCMD, MCMD,
    NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, NOMASK, NOINVERT,
    TXT_WantRowSep,
    "tableInfo.wantRowSep"
};

/*----------------------------------------------------------------------+
|                                                                       |
|   ColorPicker Item Resource                                           |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ColorPickerRsc COLORPICKERID_Color1 = 
{
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 0, 
    NOMASK, "", "tableInfo.lineSymb[1].color"};


DItem_TextRsc TEXTID_Weight1 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineWeight,
    "tableInfo.lineSymb[1].weight"
    };			     
DItem_TextRsc TEXTID_Style1 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineStyle,
    "tableInfo.lineSymb[1].style"
    };
/*----------------------------------------------------------------------+
|                                                                       |
|   ColorPicker Item Resource                                           |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ColorPickerRsc COLORPICKERID_Color2 = 
{
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 0, 
    NOMASK, "", "tableInfo.lineSymb[2].color"};


DItem_TextRsc TEXTID_Weight2 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineWeight,
    "tableInfo.lineSymb[2].weight"
    };
DItem_TextRsc TEXTID_Style2 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineStyle,
    "tableInfo.lineSymb[2].style"
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   ColorPicker Item Resource                                           |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ColorPickerRsc COLORPICKERID_Color3 = 
{
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 0, 
    NOMASK, "", "tableInfo.lineSymb[3].color"};


DItem_TextRsc TEXTID_Weight3 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineWeight,
    "tableInfo.lineSymb[3].weight"
    };
DItem_TextRsc TEXTID_Style3 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineStyle,
    "tableInfo.lineSymb[3].style"
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   ColorPicker Item Resource                                           |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ColorPickerRsc COLORPICKERID_Color4 = 
{
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 0, 
    NOMASK, "", "tableInfo.lineSymb[4].color"};


DItem_TextRsc TEXTID_Weight4 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineWeight,
    "tableInfo.lineSymb[4].weight"
    };
DItem_TextRsc TEXTID_Style4 =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    5,
    "%d",
    "%d",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    TXT_LineStyle,
    "tableInfo.lineSymb[4].style"
    };
