/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/mdlbacmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mdlbasic/mdlbacmd.r_v  $
|   $Workfile:   mdlbacmd.r  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:39:22 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <dlogbox.h>
#include <cmdlist.h>
#include <cmdclass.h>


/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_MDLBASIC        1

DllMdlApp DLLAPP_MDLBASIC =
    {
    "MDLBASIC", "mdlbasic"          // taskid, dllName
    }


/*----------------------------------------------------------------------+
|									|
|   Command Table Definitions						|
|									|
+----------------------------------------------------------------------*/
#define  CT_NONE		0
#define  CT_MAIN		1
#define  CT_MDLBASIC		2

Table CT_MAIN =
    {
        {1, CT_MDLBASIC, PLACEMENT, REQ, "MDLBASIC"}
    };

Table CT_MDLBASIC =
    {
        {1, CT_NONE, INHERIT, REQ, "DRAW"},
    };
