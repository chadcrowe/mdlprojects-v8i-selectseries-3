/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlbasic/mdlbatyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mdlbasic/mdlbatyp.mtv  $
|   $Workfile:   mdlbatyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:35 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <basedefs.h>
#include <basetype.h>
#include <msdefs.h>

#include "mdlbasic.h"

publishStructures (tablesymbology);
publishStructures (mdltableparams);
