/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listbox.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listbox.mcv  $
|   $Workfile:   listbox.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:38:59 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MDL example to show new items					|
|									|
|	main - Main entry point						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>	    /* MDL Library funcs structures & constants */
#include    <dlogitem.h>    /* Dialog Box Manager structures & constants */
#include    <userfnc.h>
#include    <dlogids.h>

#include    <dlogman.fdf>   /* dialog box manager function prototypes */
#include    <msrsrc.fdf>
#include    <msparse.fdf>
#include    <mssystem.fdf>
#include    <mscexpr.fdf>
#include    <msvar.fdf>
#include    "listbox.h"     /* dialog box example constants & structs */
#include    "listbcmd.h"    /* dialog box command numbers */

#include    "listicon.fdf"  /* dialog hook function definitions */
#include    "listsash.fdf"  /* dialog hook function definitions */
#include    "listmult.fdf"  /* dialog hook function definitions */
#include    "listcolr.fdf"  /* dialog hook function definitions */

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
static DItem_PulldownMenuItem	gAppMenuItem;
Public ListBoxGlobals       	listboxGlobs;

/*----------------------------------------------------------------------+
|                                                                       |
|   Asynchronous Functions						|
|                                                                       |
+----------------------------------------------------------------------*/ 
/*----------------------------------------------------------------------+
|                                                                       |
| name          listbox_unloadFunction					|
|                                                                       |
| author        BSI                                     05/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int listbox_unloadFunction
(
int unloadType
)
    {
    /* no need to clean up menus if shutting down */
    if (SYSTEM_TERMINATED_SHUTDOWN == unloadType)
	return (0);

    mdlDialog_menuBarDeleteCmdWinMenu (&gAppMenuItem);
    return (0);
    }

/*----------------------------------------------------------------------+
|									|
|   Utility routines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          listbox_errorPrint -- print an error message into	|
|			      Dialog Box Manager Messages dialog box	|
|                                                                       |
| author        BSI                                     05/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Public void listbox_errorPrint
(
int errorNumber		    /* => number of error to print */
)
    {
    char    errorMsg[80];

    if (SUCCESS != mdlResource_loadFromStringList (errorMsg, NULL,
				MESSAGELISTID_ListBox, errorNumber))
    	{
    	/* unable to find message with number "errorNumber" */
	return;	
	}

    mdlDialog_dmsgsPrint (errorMsg);
    }

/*----------------------------------------------------------------------+
|                                                                       |
|       Command Handling routines                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          listbox_openDialogBox					|
|                                                                       |
| author        BSI                                     05/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listbox_openDialogBox
(
char	*unparsedP	/* => unparsed part of command */
)
/*cmdNumber   CMD_LISTBOX_OPEN_LISTICON, 
	    CMD_LISTBOX_OPEN_SASH,
	    CMD_LISTBOX_OPEN_COLORS, 
	    CMD_LISTBOX_OPEN_MULTILIST */
    {
    long	dialogId;
    DialogBox  *dbP;	/* a ptr to a dialog box */

    /* Find the dialog id from the command number */
    dialogId = (mdlCommandNumber & 0xFF00) >> 8;
    if (dialogId == 0)
	return;

    /* open the newItems dialog box */
    if (NULL == (dbP = mdlDialog_open (NULL, dialogId)))
	listbox_errorPrint (MSGID_DialogBox);
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI     				05/95		|
|									|
+----------------------------------------------------------------------*/
static DialogHookInfo uHooks[] =
    {
    {HOOKDIALOGID_ListIcon,		    (PFDialogHook)listicon_mainBoxHook},
    {HOOKDIALOGID_DialogSash,		    (PFDialogHook)listsash_sashDialogHook},
    {HOOKDIALOGID_MultiList,		    (PFDialogHook)listmult_multiListDialogHook},
    {HOOKDIALOGID_Colors,		    (PFDialogHook)listcolr_colorsDialogHook},
    {HOOKITEMID_ListIcon_FileListDirectory, (PFDialogHook)listicon_iconListBoxHook},
    {HOOKITEMID_CheckListBox,		    (PFDialogHook)listicon_checkListBoxHook},
    {HOOKITEMID_Sash,			    (PFDialogHook)listsash_sashHook},
    {HOOKITEMID_ListBoxSash,		    (PFDialogHook)listsash_sashListBoxHook},
    {HOOKITEMID_ListBoxMulti,		    (PFDialogHook)listmult_multiListBoxHook},
    {HOOKITEMID_PDMMultiListTests,	    (PFDialogHook)listmult_multiListTestsMenusHook},
    {HOOKITEMID_ToggleBtnColor,		    (PFDialogHook)listcolr_colorsToggleBtnHook},
    {HOOKITEMID_ColoredListBox,		    (PFDialogHook)listcolr_coloredListBoxHook},

    };


extern "C" DLLEXPORT  int MdlMain
(
int   argc,		/* => number of args in next array */
char *argv[]		/* => array of cmd line arguments */
)
    {
    SymbolSet		   *setP;	/* ptr to "C expression symbol set" */
    RscFileHandle	    rscFileH;	/* a resource file handle */

    /* open the resource file that we came out of */
    mdlResource_openFile (&rscFileH, NULL, 0);

    MdlCommandNumber    commandNumbers [] =
        {
        {listbox_openDialogBox,CMD_LISTBOX_OPEN_LISTICON},
        {listbox_openDialogBox,CMD_LISTBOX_OPEN_SASH},
        {listbox_openDialogBox,CMD_LISTBOX_OPEN_COLORS},
        {listbox_openDialogBox,CMD_LISTBOX_OPEN_MULTILIST},
        0,
        };

    mdlSystem_registerCommandNumbers (commandNumbers);

    /* Load our command table */
    if (NULL == mdlParse_loadCommandTable (NULL))
	listbox_errorPrint(MSGID_CommandTable);

    /* Publish a function for the unload program event */
    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM, listbox_unloadFunction);

    /* publish our hook functions */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    /* set up variables that will be evaluated within C expression strings */
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, FALSE);
    mdlDialog_publishComplexVariable (setP, "listboxglobals", "listboxGlobs", &listboxGlobs);

    listcolr_initializeColorDescriptors (&listboxGlobs.localPalP, listboxGlobs.localColorsP, NLOCAL_COLORS);
    
    /* add our commands to the application menu */
    mdlDialog_menuBarAddCmdWinMenu (&gAppMenuItem, PULLDOWNMENUID_ListBox, TRUE);
    return  SUCCESS;
    }

