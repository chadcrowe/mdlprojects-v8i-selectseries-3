/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listmult.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listmult.r_v  $
|   $Workfile:   listmult.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:15 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Multi-selection list box Example Resources			|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */
#include <keys.h>
#include <cmdlist.h>

#include "listbox.h"	/* newitems dialog box example constants & structs */
#include "listbtxt.h"	/* newitems static text definitions */

/*----------------------------------------------------------------------+
|									|
|   Multi-Selection ListBox Example Dialog				|
|									|
+----------------------------------------------------------------------*/
#define GBW 	(15*XC)
#define XW  	(11*XC)

#define X1  	(16*XC)
#define X2  	(2*XC)
#define X3  	(X2 + 22*XC + 6*XC)
#define XSIZE	(X3 + 33*XC + 3*XC + 5*XC)
#define Y1   	(GENY(5) + 10*(YC+YC/6)+YC)
#define YSEP 	(Y1)
#define YBUT	YSEP+YC
#define	YSIZE	(YBUT+2*YC+YC)


DialogBoxRsc  DIALOGID_MultiList =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, HOOKDIALOGID_MultiList, NOPARENTID,
    TXT_MultiSelectDialogTitle,
{
{{0,0,0,0},	    	MenuBar,    MENUBARID_MultiList,    ON,	    0, "", ""},
{{X1,GENY(2.5),0,0},	OptionButton, OPTIONBTNID_SelectionMode, ON, 0, "", ""},
{{X2,GENY(5),0,0},	ListBox,    LISTBOXID_Multi,		ON, 0, "", ""},
{{X3,GENY(5),0,0},	ListBox,    LISTBOXID_SelectionList,	ON, 0, "", ""},
{{X2, Y1, 0, 0},	Label,	    0,				ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   Item Resource Specifications					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   ListBox Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_Multi =
    {
    NOHELP, MHELP, 
    HOOKITEMID_ListBoxMulti, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_SELBROWSE, 
    10, 0, TXT_ExampleList,
	{
	{14*XC, 60, 0, ""},
	{8*XC, 60, 0, ""},
	}
    };

DItem_ListBoxRsc LISTBOXID_SelectionList =
    {
    NOHELP, MHELP, 
    NOHOOK, NOARG,
    LISTATTR_DYNAMICSCROLL | LISTATTR_NEVERSELECTION, 
    10, 2, TXT_CurrentSelection,
	{
	{1*XC, 1, 0, ""},		/* { */
	{4*XC, 8, ALIGN_RIGHT, TXT_Row},
	{4*XC, 8, ALIGN_RIGHT, TXT_Col},
	{2*XC, 1, 0, ""},		/* } */
	{14*XC, 60, 0, ""},
	{8*XC, 60, 0, ""},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   Menu Bar Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_MenuBarRsc MENUBARID_MultiList =
    {
    NOHOOK, NOARG,
	{
	{PulldownMenu, PULLDOWNMENUID_MultiListTests},
	}
    };


/*----------------------------------------------------------------------+
|									|
|   Multi-Selection List Tests Menu					|
|									|
+----------------------------------------------------------------------*/
DItem_PulldownMenuRsc PULLDOWNMENUID_MultiListTests = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKITEMID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Tests,
{
{TXT_Modify,	NOACCEL, ON,  NOMARK, PulldownMenu, PULLDOWNMENUID_MultiListModify,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{TXT_Select,	NOACCEL, ON,  NOMARK, PulldownMenu, PULLDOWNMENUID_MultiListSelect,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{TXT_Position,	NOACCEL, ON,  NOMARK, PulldownMenu, PULLDOWNMENUID_MultiListPosition,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{TXT_Location,	NOACCEL, ON,  NOMARK, PulldownMenu, PULLDOWNMENUID_MultiListLocation,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
		NOCMD, OTASKID, ""},
{"-",		NOACCEL,OFF,NOMARK,0,NOSUBMENU,NOHELP,OHELPTASKIDCMD,
		NOHOOK, NOARG,NOCMD, OTASKID,""},
{TXT_Exit,	NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		NOHELP, OHELPTASKIDCMD,
		NOHOOK, 0,
	        CMD_DMSG_ACTION_SYSMENUCLOSE, MTASKID, ""},
}
    };
    
DItem_PulldownMenuRsc PULLDOWNMENUID_MultiListModify = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKITEMID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Modify,
{	
{TXT_ResetList,	    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_ResetList,
		    NOCMD, OTASKID, ""},
{TXT_RecreateList,  NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_RecreateList,
		    NOCMD, OTASKID, ""},
{TXT_AddItemAtTop,  NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_AddTop,
		    NOCMD, OTASKID, ""},
{TXT_AddItemAtBottom,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_AddBottom,
		    NOCMD, OTASKID, ""},
{TXT_AddBeforeSelection,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_AddSelection,
		    NOCMD, OTASKID, ""},
{TXT_DeleteSelection,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_DeleteSelection,
		    NOCMD, OTASKID, ""},
{TXT_ToggleAddMode, VBIT_SHIFT|VKEY_F8, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_ToggleAddMode,
		    CMD_DMSG_ACTION_ADDMODE, MTASKID, ""},
}
    };
    
DItem_PulldownMenuRsc PULLDOWNMENUID_MultiListSelect = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKITEMID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Select,
{	
{TXT_SelectTopItem, NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_SelectTop,
		    NOCMD, OTASKID, ""},
{TXT_SelectBottomItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_SelectBottom,
		    NOCMD, OTASKID, ""},
{TXT_SelectEveryOther,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_SelectEveryOther,
		    NOCMD, OTASKID, ""},
{TXT_SelectAll,	    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_SelectAll,
		    NOCMD, OTASKID, ""},
{TXT_DeselectTopItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_DeselectTop,
		    NOCMD, OTASKID, ""},
{TXT_DeselectBottomItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_DeselectBottom,
		    NOCMD, OTASKID, ""},
{TXT_DeselectAll,   NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_DeselectAll,
		    NOCMD, OTASKID, ""},
}
    };
			
			
DItem_PulldownMenuRsc PULLDOWNMENUID_MultiListPosition = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKITEMID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Position,
{	
{TXT_Make1stTop,    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_Top1st,
		    NOCMD, OTASKID, ""},
{TXT_MakeLastBottom,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_BottomLast,
		    NOCMD, OTASKID, ""},
{TXT_Make1stSelectionTop,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_TopSelection,
		    NOCMD, OTASKID, ""},
}
    };

DItem_PulldownMenuRsc PULLDOWNMENUID_MultiListLocation = 
    {
    NOHELP, OHELPTASKIDCMD,
    HOOKITEMID_PDMMultiListTests,
    ON | ALIGN_LEFT, TXT_Select,
{	
{TXT_MoveToTopItem, NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_LocationTop,
		    NOCMD, OTASKID, ""},
{TXT_MoveToBottomItem,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_LocationBottom,
		    NOCMD, OTASKID, ""},
{TXT_MoveToBeginningofSelection,
		    NOACCEL, ON,  NOMARK, 0, NOSUBMENU,
		    NOHELP, OHELPTASKIDCMD,
		    NOHOOK, MENUSEARCHID_LocationSelection,
		    NOCMD, OTASKID, ""},
}
    };
    

/*----------------------------------------------------------------------+
|									|
|   Option Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
DItem_OptionButtonRsc  OPTIONBTNID_SelectionMode =
    {
    SYNONYMID_MultiList, NOHELP, MHELP, NOHOOK, 
    OPTNBTNATTR_NEWSTYLE | NOARG, 
    TXT_SelectionMode,
    "listboxGlobs.selectionMode",
	{
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_BROWSE,   0xFF, ON,	TXT_Browse},
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_SINGLE,   0xFF, ON,	TXT_Single},
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_MULTI,    0xFF, ON,	TXT_Multiple},
	{NOTYPE, NOICON, NOCMD, LCMD, SELECTIONMODE_EXTENDED, 0xFF, ON, TXT_Extended},
	}
    };


/*----------------------------------------------------------------------+
|									|
|   Synonym List Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_SynonymsRsc SYNONYMID_MultiList =
    {
	{
	{ListBox,  LISTBOXID_Multi},
	}
    };
