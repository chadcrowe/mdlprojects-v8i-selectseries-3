/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listicon.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listicon.r_v  $
|   $Workfile:   listicon.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:12 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   listicon.r								|
|									|
|	Dialog Resources					    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */
#include <keys.h>

#include "listbox.h"	/* newitems dialog box example constants & structs */
#include "listbtxt.h"	/* newitems static text definitions */

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
#define YBUT		YSEP+YC
#define	YSIZE		(YBUT+2*YC+YC)

/*----------------------------------------------------------------------+
|									|
|   Example Dialog containing a list box with icons			|
|									|
+----------------------------------------------------------------------*/
#define XSIZE	(38*XC)
#define GBW (15*XC)
#define XW  (11*XC)

#define X1 (3*XC)
#define X2 (X1 + 1.5*XC)
#define X3 (10*XC)
#define X4 (X3 + 1.5*XC)

#define YSEP (GENY(12)-YC+5*YC)

DialogBoxRsc  DIALOGID_ListIcon =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, HOOKDIALOGID_ListIcon, NOPARENTID,
    TXT_ListIcon,
{
{{X1,GENY(1), (WIDTH_FILEOPENDIRS_LISTBOX + 10)*XC,0}, Text, TEXTID_Directory, ON, 0, "", ""},
{{X1,GENY(2), 0,0}, ListBox, LISTBOXID_Directories, ON, 0, "", ""},
{{X1,GENY(12),0,0}, ListBox, LISTBOXID_Choices, ON, 0, "", ""},

}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef GBW
#undef XW

#undef X1
#undef X2
#undef X3
#undef X4
#undef X5

#undef YSEP

/*----------------------------------------------------------------------+
|									|
|   Item Resource Specifications					|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   ListBox Resources (With ICONS)					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   List Box which includes ICONS					|
|									|
| Sample list box resource using DRAWPREFIXICON - Directory List on file|
| open.									|
|Include the LISTATTR_DRAWPREFIXICON attribute in the list box resource.| 
|This attribute is defined in "dlogbox.h".				|
|The programmer must set up the list box resource to allow space for the|
| icon to be drawn.							|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_Directories =
    {
    NOHELP, MHELP, HOOKITEMID_ListIcon_FileListDirectory, NOARG, 
#if defined (MSVERSION)	&& (MSVERSION >= 0x551)
    LISTATTR_DYNAMICSCROLL | LISTATTR_DRAWPREFIXICON | LISTATTR_NOSELECTION,
#else
    LISTATTR_DYNAMICSCROLL | LISTATTR_NOSELECTION,
#endif
    10, 0, "",
	{
	{(WIDTH_FILEOPENDIRS_LISTBOX + 10)*XC, 32, 0, ""},
	}
    };


DItem_ListBoxRsc LISTBOXID_Choices =
    {
    NOHELP, MHELP, 
    HOOKITEMID_CheckListBox, NOARG, 
#if defined (MSVERSION)	&& (MSVERSION >= 0x551)
    LISTATTR_DYNAMICSCROLL | LISTATTR_DRAWPREFIXICON | LISTATTR_NOSELECTION, 
#else
    LISTATTR_DYNAMICSCROLL | LISTATTR_NOSELECTION,
#endif
    5, 0, "",
	{
	{(WIDTH_FILEOPENDIRS_LISTBOX + 10)*XC, 30, 0, ""},
	}
    };


/*----------------------------------------------------------------------+
|                                                                       |
|   Text Item Resource Definitions					|
|                                                                       |
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_Directory =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 
   MAXDIRLENGTH, "%s", "%s", "", "", NOMASK, 
   TEXT_READONLY | TEXT_ABBREVFILENAME,
   "", 
   "listboxGlobs.directoryName"
   };
