/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listicon.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   listicon.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:39:10 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   listicon.mc - listicon source code.				    	|
|	    Demonstrates icons displayed in a list box.		    	|
|								    	|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>	    /* MDL Library funcs structures & constants */
#include    <dlogitem.h>    /* Dialog Box Manager structures & constants*/
#include    <cmdlist.h>	    /* MicroStation command numbers */
#include    <dlogids.h>	    /* Contains ICONIDs for listbox */
#include    <filelist.h>
#include    <string.h>

#include    <dlogman.fdf>   /* dialog box manager function prototypes */
#include    <msfile.fdf>
#include    <mssystem.fdf>
#include    <msstate.fdf>
#include    <msrsrc.fdf>

#include    "listbox.h"     /* dialog box constants & structs */

#include    "listbox.fdf"   /* function definitions */

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
extern ListBoxGlobals listboxGlobs;

/*----------------------------------------------------------------------+
|                                                                       |
|       Command Handling routines                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Utility routines							|
|									|
+----------------------------------------------------------------------*/
#if defined (MSVERSION)	&& (MSVERSION >= 0x551)
/*----------------------------------------------------------------------+
|									|
| name		listicon_checkboxIsOn					|
|									|
| author	BSI				4/95			|
|									|
+----------------------------------------------------------------------*/
Private BoolInt listicon_checkboxIsOn
(
int iconRscID
)
    {
    BoolInt onFlag;

    /*  if the icon is ON then return TRUE else return FALSE */

    switch (iconRscID)
	{

	/* If any size icon id is ON then return TRUE */
    	case ICONID_ToggleOn8Pt:
    	case ICONID_ToggleOn10Pt:
    	case ICONID_ToggleOn12Pt:
    	case ICONID_ToggleOn14Pt:
    	case ICONID_ToggleOn18Pt:
    	case ICONID_ToggleOn24Pt:
	    {
	    onFlag = TRUE;
	    break;
	    }

	default:
	    {
	    onFlag = FALSE;
	    break;
	    }
    	}

    return onFlag;
    }

/*----------------------------------------------------------------------+
|									|
| name		listicon_setCheckboxIconSize				|
|									|
| author	BSI				3/94			|
|									|
+----------------------------------------------------------------------*/
Private void	listicon_setCheckboxIconSize
(
int		fontHeight,      /* => */
long		*infoFieldsP,
int		*newRscID
)
    {
    int		iconRscID;

    /*
    //  if the icon resource id was previously set then use its value
    //		otherwise set the id to zero 
    */
    iconRscID = (infoFieldsP) ? infoFieldsP[2] : 0;

    if (fontHeight < 9)
	{
	*newRscID = (listicon_checkboxIsOn(iconRscID)) ? 
			    ICONID_ToggleOn8Pt : ICONID_ToggleOff8Pt;
	}
    else if (fontHeight < 11)
	{
	*newRscID = (listicon_checkboxIsOn(iconRscID)) ? 
			    ICONID_ToggleOn10Pt : ICONID_ToggleOff10Pt;
	}
    else if (fontHeight < 13)
	{
	*newRscID = (listicon_checkboxIsOn(iconRscID)) ? 
			    ICONID_ToggleOn12Pt : ICONID_ToggleOff12Pt;
	}
    else if (fontHeight < 17)
	{
	*newRscID = (listicon_checkboxIsOn(iconRscID)) ? 
			    ICONID_ToggleOn14Pt : ICONID_ToggleOff14Pt;
	}
    else if (fontHeight < 23)
	{
	*newRscID = (listicon_checkboxIsOn(iconRscID)) ? 
			    ICONID_ToggleOn18Pt : ICONID_ToggleOff18Pt;
	}
    else
	{
	*newRscID = (listicon_checkboxIsOn(iconRscID)) ? 
			    ICONID_ToggleOn24Pt : ICONID_ToggleOff24Pt;
	}

    return;
    }

/*----------------------------------------------------------------------+
|									|
| name		listicon_changeCheckboxIconSize				|
|									|
| author	BSI				3/94			|
|									|
+----------------------------------------------------------------------*/
Private int listicon_changeCheckboxIconSize
(
StringList      *strListP,      /* <> */
int		fontHeight      /* => */
)
    {
    int 	i, newIconID, listSize, status=ERROR;
    long	*infoFieldsP;

    if (-1 == (listSize = mdlStringList_size(strListP)))
	status = ERROR;

    for (i=0; i < listSize, status == SUCCESS; i++)
	{
	status=mdlStringList_getMember(NULL, &infoFieldsP, strListP, i);

	if (SUCCESS == status)
	    {
	    listicon_setCheckboxIconSize(fontHeight, infoFieldsP, &newIconID);

	    mdlStringList_setInfoField (strListP, i, 2, newIconID);
	    }
	}

    return status;
    }

/*----------------------------------------------------------------------+
|									|
| name		listicon_fileListSetFolderIcon  			|
|									|
| author	BSI				3/94			|
|									|
+----------------------------------------------------------------------*/
Private void	listicon_fileListSetFolderIcon
(
StringList      *strListP,  	/* <> string list to store icon info */
int		numAncestors,   /* => */
int		listIndex,  	/* => index into string list*/
int		fontHeight 	/* => current dialog font height*/
)
    {
    int		iconRscID;  	/* resource identifier for the icon */
    int		pixelOffset;	/* number of pixels that icon is indented*/

    /* set pixel offset to zero pixels */
    pixelOffset = 0;

    /*----------------------------------------------------------------------
    //	Set the icon id to the correct folder size according to the current
    //	font height.
    //	MicroStation provides icons for the Sub directory, current directory,
    //	and open directory in sizes small, medium and large.
    //
    //	Sub directory folders used in this example (from dlogids.h):
    //	ICONID_SubDirectoryFolder   	-small sub directory folder
    //	ICONID_SubDirectoryFolderMD 	-medium sub directory folder
    //	ICONID_SubDirectoryFolderLG 	-large sub directory folder
    ----------------------------------------------------------------------*/
    if (fontHeight < 17)
    	{
	iconRscID = ICONID_SubDirectoryFolder;
    	}
    else if (fontHeight < 23)
    	{
	iconRscID = ICONID_SubDirectoryFolderMD;
    	}
    else
    	{
	iconRscID = ICONID_SubDirectoryFolderLG;
    	}

    /* 
    // Set the third info field (index 2) to the resource ID of icon
    // to display preceding this row of text 
    */
    mdlStringList_setInfoField (strListP, listIndex, 2, iconRscID);

    /* 
    // Set the fourth info field (index 3) to the number of pixels to
    // indent before drawing the icon
    */
    mdlStringList_setInfoField (strListP, listIndex, 3, pixelOffset);
    }

/*----------------------------------------------------------------------+
|									|
| name		listicon_toggleCheckBox	  				|
|									|
| author	BSI				3/94			|
|									|
+----------------------------------------------------------------------*/
Private void	listicon_toggleCheckBox
(
StringList      *strListP,  	/* <> string list to store icon info */
int		listIndex,  	/* => index into string list*/
int		fontHeight, 	/* => current dialog font height*/
long		*infoFieldsP	/* => current information field values*/
)
    {
    int		iconRscID;
    int		pixelOffset;	/* number of pixels that icon is indented*/

    /*  
    // if the icon resource id was previously set then use its value
    // otherwise set the id to zero 
    */
    iconRscID = (infoFieldsP) ? infoFieldsP[2] : 0;

    /* indent the icon by one pixel */
    pixelOffset = 1;

    /*
    //  if the toggle is OFF then turn it ON.  The toggle is initially set to OFF 
    */
    if (fontHeight < 9)
	{
	iconRscID = (iconRscID == ICONID_ToggleOff8Pt) ? 
			    ICONID_ToggleOn8Pt : ICONID_ToggleOff8Pt;
	}
    else if (fontHeight < 11)
	{
	iconRscID = (iconRscID == ICONID_ToggleOff10Pt) ? 
			    ICONID_ToggleOn10Pt : ICONID_ToggleOff10Pt;
	}
    else if (fontHeight < 13)
	{
	iconRscID = (iconRscID == ICONID_ToggleOff12Pt) ? 
			    ICONID_ToggleOn12Pt : ICONID_ToggleOff12Pt;
	}
    else if (fontHeight < 17)
	{
	iconRscID = (iconRscID == ICONID_ToggleOff14Pt) ? 
			    ICONID_ToggleOn14Pt : ICONID_ToggleOff14Pt;
	}
    else if (fontHeight < 23)
	{
	iconRscID = (iconRscID == ICONID_ToggleOff18Pt) ? 
			    ICONID_ToggleOn18Pt : ICONID_ToggleOff18Pt;
	}
    else
	{
	iconRscID = (iconRscID == ICONID_ToggleOff24Pt) ? 
			    ICONID_ToggleOn24Pt : ICONID_ToggleOff24Pt;
	}
   
    /* 
    // Set the third info field (index 2) to the resource ID of icon
    // to display preceding this row of text 
    */
    mdlStringList_setInfoField (strListP, listIndex, 2, iconRscID);

    /* 
    // Set the fourth info field (index 3) to the number of pixels to
    // indent before drawing the icon
    */
    mdlStringList_setInfoField (strListP, listIndex, 3, pixelOffset);

    }
#endif /* MSVERSION >= 0x551 */

/*----------------------------------------------------------------------+
|                                                                       |
| name          listicon_toggleRow	 				|
|                                                                       |
| author        BSI                                     04/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private BoolInt	listicon_toggleRow
(
DialogItemMessage   *dimP    /* => a ptr to a dialog item message */
)
    {

    int		fontHeight = mdlDialog_fontGetCurHeight (dimP->db);
    int		rowIndex, colIndex, topRowIndex;
    long	*infoFieldsP;
    BoolInt	found = FALSE;
    DialogItem	*listBoxP = dimP->dialogItemP;
    RawItemHdr	*rihP = listBoxP->rawItemP;
    StringList	*strListP = mdlDialog_listBoxGetStrListP(rihP);

    /* 
    //	if a valid string list is associated to the list box then
    //      toggle the check box in the selected row
    */
    if (NULL != strListP)
	{
	rowIndex = -1;
	colIndex = -1;
	topRowIndex = -1;

	/* Find the selected row */
    	mdlDialog_listBoxGetNextSelection(&found, &rowIndex, &colIndex, rihP);
	if (TRUE == found)
	    {
	    /* get the information fields of the selected row */
	    mdlStringList_getMember(NULL, &infoFieldsP, strListP, rowIndex);

	    /* toggle the check box in the selected row */
	    listicon_toggleCheckBox(strListP, rowIndex, fontHeight, infoFieldsP);

	    /*------------------------------------------------------------------
	    //	When to use relative or actual string list index:
	    //
	    //	If you pass the string list in to the function, use the string
	    //	list index;
	    //	If you pass the raw item header of the list box in to the function,
	    //	use the relative list box index.
	    //
	    //	Relative index - index of a row within the currently displayed
	    //      	rows; relative to the current top row.
	    ------------------------------------------------------------------*/

	    /* find topRowIndex to calculate relative row index */
	    mdlDialog_listBoxGetDisplayRange (&topRowIndex, NULL, NULL,
					    	 NULL, rihP);

	    /* redraw the updated row (use relative index) */
            mdlDialog_listBoxDrawContents (rihP, (rowIndex - topRowIndex), -1);
	    }
	}

    return found;
    }

/*----------------------------------------------------------------------+
|									|
| name		listicon_checkListPopulate 				|
|									|
| author	BSI				4/95			|
|									|
+----------------------------------------------------------------------*/
Private int listicon_checkListPopulate
(
StringList      *strListP,    /* <= string list to populate */
int		fontHeight    /* => current dialog font height */
)
    {
    int     status = SUCCESS;
    int     i;
    long    newIndex;

    if (NULL == strListP)
    	{
	return ERROR;
    	}

    for (i=0; i<10;i++)
	{
	char buff[50];

	sprintf(buff, "%d Choice", i);

	status = mdlStringList_insertMember(&newIndex, strListP, -1, 1);
	if (SUCCESS != status)
	    {
	    break;
	    }

	mdlStringList_setMember(strListP, newIndex, buff, NULL);

#   	if defined (MSVERSION)	&& (MSVERSION >= 0x551)
	/* Add the check box icon information to the string list */
	listicon_toggleCheckBox(strListP, newIndex, fontHeight, NULL);
#   	endif
	}

    return status;
    }

/*----------------------------------------------------------------------+
|									|
| name		listicon_fileListPopulate	  			|
|									|
| author	BSI				3/95			|
|									|
+----------------------------------------------------------------------*/
Private int	listicon_fileListPopulate
(
StringList      *strListP,    /* <= string list to populate */
int		fontHeight    /* => current dialog font height */
)
    {
    int		status = SUCCESS, i, numAncestors;
    long        newIndex;
    char	*fileName;
    StringList	*filesListP;

    /* Create an empty string list for the file list */
    filesListP = mdlStringList_create(0, 1);

    /*  
    //	Find all directories in the current working directory
    //	Exclude the parent directory with attribute FILELISTATTR_NOPARENTDIR
    */
    if (SUCCESS != (status=mdlFileList_get(filesListP, 
	    	FILELISTATTR_DIRECTORIES | FILELISTATTR_NOPARENTDIR,
			listboxGlobs.directoryName, "")))
	{
    	mdlStringList_destroy (filesListP);

	return status;
	}

    numAncestors = 0;

    for (i=0; status == SUCCESS; i++)
	{
	status = mdlStringList_getMember(&fileName, NULL, filesListP, i);
	if (SUCCESS != status)
	    {
	    /* assume no more files in the list & reset status */
	    status = SUCCESS;
	    break;
	    }

	/* add a member to the list box's stringlist for this file */
	status = mdlStringList_insertMember(&newIndex, strListP, -1, 1);
	if (SUCCESS != status)
	    {
	    break;
	    }

	/* set the current string list member to the current fileName */
	mdlStringList_setMember(strListP, newIndex, fileName, NULL);

#   	if defined (MSVERSION)	&& (MSVERSION >= 0x551)
	listicon_fileListSetFolderIcon(strListP, numAncestors, newIndex, fontHeight);
#   	endif
	}

    mdlStringList_destroy (filesListP);

    return status;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Hook Functions							|
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          listicon_mainBoxHook  					|
|                                                                       |
| author        BSI                                     1/95            |
|									|
+----------------------------------------------------------------------*/
Public void    listicon_mainBoxHook
(
DialogMessage   *dmP    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {

	    /* get the MicroStation root directory as the starting point */
    	    mdlSystem_getCfgVar(listboxGlobs.directoryName, "MSDIR", sizeof(listboxGlobs.directoryName)-1);

	    /* 
	    //	Establish an interest in Dialog font changes 
	    // 	so we can change our icon sizes when the dialog
	    // 	manager gives us the DIALOG_MESSAGE_FONTCHANGED message
	    */
	    dmP->u.create.interests.fontChanges = TRUE;
	    break;
	    };

	case DIALOG_MESSAGE_FONTCHANGED:
	    {
	    /*
	    // Icon resources are of a fixed size.  List box text sizes change automatically
	    // when the current dialog font changes.  The programmer must update the icon
	    // resource identifer in the StringList according to the new font height.
	    */

	    /* use the new font height given in the message */
    	    int		fontHeight = dmP->u.fontChanged.newFontHeight;
	    DialogItem	*diP;
	    StringList	*strListP;

	    /*----------------------------------------------------------+
	    |	update the DirectoryList listbox icons		    	|
	    +----------------------------------------------------------*/

	    /* get a pointer to the list box */
	    diP = mdlDialog_itemGetByTypeAndId(dmP->db, RTYPE_ListBox, LISTBOXID_Directories, 0);

	    /* get a pointer to the list box's string list */
	    strListP = mdlDialog_listBoxGetStrListP(diP->rawItemP);

	    if (NULL != strListP)
		{
		/* delete all members of the stringlist */
		mdlStringList_deleteMember(strListP, 0, -1);

	    	/* Populate the string list */
	    	if (SUCCESS != listicon_fileListPopulate(strListP, fontHeight))
		    {
		    /* re-draw the contents of the list box */
		    mdlDialog_listBoxDrawContents (diP->rawItemP, -1, -1);
		    return;
		    }
		}

	    /*-----------------------------------------------------------------+
	    |	update the Choices listbox icons by re-creating the string list|
	    +-----------------------------------------------------------------*/

	    /* get a pointer to the list box */
	    diP = mdlDialog_itemGetByTypeAndId(dmP->db, RTYPE_ListBox, LISTBOXID_Choices, 0);

	    /* get a pointer to the list box's string list */
	    strListP = mdlDialog_listBoxGetStrListP(diP->rawItemP);

	    if (NULL != strListP)
		{
	    	/* change the icon size according to the new font height */
		if (listicon_changeCheckboxIconSize (strListP, fontHeight) == SUCCESS)
		    {
		    /* re-draw the contents of the list box */
    		    mdlDialog_listBoxDrawContents (diP->rawItemP, -1, -1);
		    }
		}

	    break;
	    }


	case DIALOG_MESSAGE_DESTROY:
	    {
	    mdlState_startDefaultCommand ();
	    break;
	    };

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listicon_iconListBoxHook				|
|                                                                       |
| author        BSI					03/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Public void listicon_iconListBoxHook
(
DialogItemMessage   *dimP    /* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
    	    int		fontHeight = mdlDialog_fontGetCurHeight (dimP->db);
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP;

#	    if defined (MSVERSION) && (MSVERSION >= 0x551)

	    /*----------------------------------------------------------------
	    // Create the string list with at least 4 info fields.  These info
	    // fields will be used as follows:
    	    //      
    	    // 0 - Reserved for the dialog manager
    	    // 1 - Reserved for colored row information (see documentation of 
    	    //	    LISTATTR_COLOREDROWS) - see MDL example NEWITEMS 
    	    // 2 - Resource ID of icon to display preceding that row of text. 
    	    // 3 - Number of pixels to indent before drawing the icon.
    	    ----------------------------------------------------------------*/
	    strListP = mdlStringList_create (0, 4);
#	    else
	    strListP = mdlStringList_create (0, 1);
#	    endif

	    /*---------------------------------------------------------------- 
	    // if string list create failed then tell the dialog manager 
	    // that create failed
	    ----------------------------------------------------------------*/
	    if (NULL == strListP)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}


	    /* Populate the string list */
	    if (SUCCESS != listicon_fileListPopulate(strListP, fontHeight))
		{
	    	/*----------------------------------------------------------------
	    	// if string list populate failed then tell the dialog manager 
	    	// that create failed
	    	----------------------------------------------------------------*/
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    /* associate the string list to the list box */
	    mdlDialog_listBoxSetStrListP (rihP, strListP, 1);
	    break;
	    }

	case DITEM_MESSAGE_BUTTON:
	    {
	    if (BUTTONTRANS_UP == dimP->u.button.buttonTrans)
		{
		/* check for double click */
		if (2 == dimP->u.button.upNumber)
		    {
		    int		rowIndex, colIndex;
		    char	*fileName;
		    char    	msgString[256];
		    char    	msgtemplate[50];
		    BoolInt	found = FALSE;
       	    	    DialogItem	*listBoxP = dimP->dialogItemP;
	    	    RawItemHdr	*rihP = listBoxP->rawItemP;
	    	    StringList	*strListP = mdlDialog_listBoxGetStrListP(rihP);

		    rowIndex = -1;
		    colIndex = -1;

	    	    if (NULL != strListP)
		    	{
		    	/* Find the selected row */
		    	mdlDialog_listBoxGetNextSelection(&found, &rowIndex,
							     &colIndex, rihP);
		    	if (TRUE == found)
			    {

			    mdlStringList_getMember(&fileName, 
					    	NULL, strListP, rowIndex);

			    if (SUCCESS == mdlResource_loadFromStringList(msgtemplate,
				    0L, MESSAGELISTID_ListBox, MSGID_DirSelection))
			    	{
			    	sprintf(msgString, msgtemplate, fileName);
			    	}
			    else
			    	{
			    	strcpy(msgString, fileName);
			    	}

			    mdlDialog_openMessageBox(DIALOGID_MsgBoxOK, msgString,
				    	MSGBOX_ICON_INFORMATION);
			    }
		    	}
		    }
		}
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP = mdlDialog_listBoxGetStrListP(rihP);

	    if (NULL != strListP)
		{
		mdlStringList_destroy (strListP);
		}
	    break;
	    }

	default:
	    {
	    dimP->msgUnderstood = FALSE;
	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listicon_iconListBoxHook				|
|                                                                       |
| author        BSI					03/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Public void listicon_checkListBoxHook
(
DialogItemMessage   *dimP    /* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
    	    int		fontHeight = mdlDialog_fontGetCurHeight (dimP->db);
	    int		status;
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP;

#if defined (MSVERSION)	&& (MSVERSION >= 0x551)
	    /*----------------------------------------------------------------
	    // Create the string list with at least 4 info fields.  These info
	    // fields will be used as follows:
    	    //      
    	    // 0 - Reserved for the dialog manager
    	    // 1 - Reserved for colored row information (see documentation of 
    	    //	    LISTATTR_COLOREDROWS) - see MDL example NEWITEMS 
    	    // 2 - Resource ID of icon to display preceding that row of text. 
    	    // 3 - Number of pixels to indent before drawing the icon.
    	    ----------------------------------------------------------------*/
	    strListP = mdlStringList_create (0, 4);
#else
	    strListP = mdlStringList_create (0, 1);
#endif
	    /*---------------------------------------------------------------- 
	    // if string list create failed then tell the dialog manager 
	    // that the create failed
	    ----------------------------------------------------------------*/
	    if (NULL == strListP)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    /*---------------------------------------------------------------- 
	    // populate the string list with text and icon information 
	    // based on the current font height 
	    ----------------------------------------------------------------*/
	    if (SUCCESS != (status=listicon_checkListPopulate(strListP, fontHeight)))
	    	{
		listbox_errorPrint(MSGID_CheckBoxPopulateErr);
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    /* associate the string list to the list box */
	    mdlDialog_listBoxSetStrListP (rihP, strListP, 1);
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {

	    /* toggle the check box icon in the currently selected row */
	    listicon_toggleRow(dimP);

	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP = mdlDialog_listBoxGetStrListP(rihP);

	    if (NULL != strListP)
		{
		mdlStringList_destroy (strListP);
		}
	    break;
	    }

	default:
	    {
	    dimP->msgUnderstood = FALSE;
	    break;
	    }
	}
    }

