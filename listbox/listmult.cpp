/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listmult.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: listmult.cpp,v $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:39:13 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MDL example to show new items					|
|									|
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -						|
|									|
|	main - Main entry point						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>	    /* MDL Library funcs structures & constants */
#include    <dlogitem.h>    /* Dialog Box Manager structures & constants */
#include    <stdarg.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <keys.h>
#include    <dlmsys.fdf>
#include    <dlogman.fdf>   /* dialog box manager function prototypes */
#include    <msrsrc.fdf>

#include    "listbox.h"     /* basic dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
#define	NMULTILIST_LINES	30

/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
extern ListBoxGlobals listboxGlobs;

/*----------------------------------------------------------------------+
|									|
|   Utility routines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Name	listmult_deleteSelections				|
|                                                                       |
|   Author	BSI                                         2/96        |
|                                                                       |
|   Deletes the selected rows from a list box.                          |
+----------------------------------------------------------------------*/
Private int listmult_deleteSelections
(
RawItemHdr  *listP	    /*  => List box raw item header pointer	*/
)
    {
    int     	status = SUCCESS;
    int		nSelections;
    int		minRow, maxRow, nDisplayedRows;
    Spoint2d	*selections;
    StringList	*strListP;

    if (NULL == listP)
    	{
    	return (ERROR);
    	}

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (NULL == strListP)
	{
    	return (ERROR);
	}

    mdlDialog_listBoxGetDisplayRange (&minRow, &maxRow, NULL, NULL, listP);
    nDisplayedRows = maxRow - minRow + 1;

    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
    if (nSelections > 0 && selections)
    	{
    	int	     newTopRow;
    	Spoint2d    *curSelectionP;

    	for (curSelectionP = selections+nSelections-1;
    	     curSelectionP >= selections; curSelectionP--)
    	    {
	    mdlStringList_deleteMember (strListP,
			    curSelectionP->y * listboxGlobs.nColumns,
			    listboxGlobs.nColumns);
	    }

    	/* browse should always have something selected */
    	if (listboxGlobs.selectionMode == SELECTIONMODE_BROWSE)
	    {
    	    mdlDialog_listBoxSelectCells (listP, selections->y,
				  selections->y, 0, -1, TRUE,
				  FALSE);
	    }

	mdlDialog_listBoxNRowsChanged (listP);
    	newTopRow = selections->y - nDisplayedRows/2;
    	if (newTopRow < 0)
	    {
	    newTopRow = 0;
	    }

    	mdlDialog_listBoxSetTopRowRedraw (listP, newTopRow, TRUE);
	}


    return  (status);
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Name	listmult_rscSprintf					|
|                                                                       |
|   Author	BSI                                         3/93        |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_rscSprintf
(
char   *stringP,	    /* <=  Result of sprintf from resource	*/
int	messageNumber,	    /*  => Index into msg list for format str	*/
...			    /*  => Any other optional arguments		*/
)
    {
    va_list ap;
    char    tempStr[1024];

    va_start (ap, messageNumber);

    *stringP = tempStr[0] = '\0';
    mdlResource_loadFromStringList (tempStr, NULL, MESSAGELISTID_ListBox,
				    messageNumber);
    vsprintf (stringP, tempStr, ap);

    va_end (ap);
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Hook Functions							|
|                                                                       |
+----------------------------------------------------------------------*/ 
/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_setSelectionList				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_setSelectionList
(
void
)
    {
    int		 iRow;
    RawItemHdr	*listP, *mListP;
    StringList	*strListP, *mStrListP;
    int		 nSelections;
    Spoint2d	*selections;
    Spoint2d	*curSelectionP;

    if (!listboxGlobs.selectionListDiP || !listboxGlobs.selectionListDiP->rawItemP)
	return;
    listP = listboxGlobs.selectionListDiP->rawItemP;
    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (!strListP)
	return;

    if (!listboxGlobs.multiListDiP || !listboxGlobs.multiListDiP->rawItemP)
	return;

    mListP = listboxGlobs.multiListDiP->rawItemP;
    mStrListP = mdlDialog_listBoxGetStrListP (mListP);
    if (!mStrListP)
	return;

    mdlDialog_listBoxGetSelections (&nSelections, &selections, mListP);

    /* empty the string list */
    mdlStringList_deleteMember (strListP, 0, -1);
    if (nSelections > 0)
	{
	int	iMember;
	char	buffer[40];
	char	*stringP;

	mdlStringList_insertMember (NULL, strListP, -1, nSelections*6);

	for (iRow=0, curSelectionP=selections;
	     iRow<nSelections;
	     iRow++, curSelectionP++)
	    {
	    iMember = iRow*6;

	    mdlStringList_setMember (strListP, iMember++, "{", NULL);
	    sprintf (buffer, "%d,", curSelectionP->y);
	    mdlStringList_setMember (strListP, iMember++, buffer, NULL);
	    sprintf (buffer, "%d ", curSelectionP->x);
	    mdlStringList_setMember (strListP, iMember++, buffer, NULL);
	    mdlStringList_setMember (strListP, iMember++, "}", NULL);

	    mdlStringList_getMember (&stringP, NULL, mStrListP,
				     curSelectionP->y * listboxGlobs.nColumns);
	    mdlStringList_setMember (strListP, iMember++, stringP, NULL);

	    if (listboxGlobs.nColumns > 1)
		{
		mdlStringList_getMember (&stringP, NULL, mStrListP,
					 curSelectionP->y * listboxGlobs.nColumns+1);
		mdlStringList_setMember (strListP, iMember++, stringP, NULL);
		}
	    }
	}

    mdlDialog_listBoxNRowsChanged (listP);
    mdlDialog_listBoxSetTopRowRedraw (listP, 0, TRUE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_trackList					|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_trackList
(
Point2d	*ptP
)
    {
    char    buffer[40];

    if (!listboxGlobs.labelDiP)
	return;

    buffer[0] = '\0';
    if (ptP->y != -1)
	sprintf (buffer, "{%3d, %3d}", ptP->y, ptP->x);

    mdlWindow_cursorTurnOff();
    mdlDialog_itemSetLabel (listboxGlobs.multiSelectDbP, listboxGlobs.labelDiP->itemIndex,
			    buffer);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_multiListInitStrList				|
|                                                                       |
| author        BSI					11/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_multiListInitStrList
(
StringList  *strListP
)
    {
    int	    iString;
    char    fStr1[20], fStr2[20];

    mdlResource_loadFromStringList (fStr1, NULL, MESSAGELISTID_ListBox,
				    MSGID_Right);
    mdlResource_loadFromStringList (fStr2, NULL, MESSAGELISTID_ListBox,
				    MSGID_Left);
    for (iString=0; iString<NMULTILIST_LINES*2; iString++)
	{
	char    buffer[80];

	if (iString & 1)
	    sprintf (buffer, fStr1, iString/2);
	else
	    sprintf (buffer, fStr2, iString/2+'A', iString/2);

	mdlStringList_setMember (strListP, iString, buffer, NULL);
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_multiListInitDisabled				|
|                                                                       |
| author        BSI					11/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_multiListInitDisabled
(
RawItemHdr  *listP
)
    {
    mdlDialog_listBoxEnableCells (listP, 5, 6, -1, -1, FALSE, FALSE);
    mdlDialog_listBoxEnableCells (listP, 12, 12, -1, -1, FALSE, FALSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_multiListBoxHook				|
|                                                                       |
| author        BSI					08/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Public void listmult_multiListBoxHook
(
DialogItemMessage   *dimP
)
    {
    RawItemHdr	*listP = dimP->dialogItemP->rawItemP;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    StringList	*strListP;

	    /* NOTE: nInfoFields is 2 so 2nd infoField can be used
	       to store a ptr to a BSIColorDesc */
	    listP->userDataP =
		strListP = mdlStringList_create (NMULTILIST_LINES*2, 2);
	    if (!strListP)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    mdlDialog_listBoxSetStrListP (listP, strListP, 2);
	    listmult_multiListInitStrList (strListP);
	    listmult_multiListInitDisabled (listP);
	    listboxGlobs.multiListDiP = dimP->dialogItemP;
	    break;
	    }

	case DITEM_MESSAGE_ALLCREATED:
	    listboxGlobs.multiSelectDbP = dimP->db;
	    listboxGlobs.labelDiP =
		mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Label, 0, 0);

	    listboxGlobs.selectionListDiP =
		mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ListBox,
					      LISTBOXID_SelectionList, 0);

	    break;

	case DITEM_MESSAGE_DESTROY:
	    if (listP->userDataP)
		{
		mdlStringList_destroy ((StringList*)listP->userDataP);
		}
	    break;

	case DITEM_MESSAGE_SYNCHRONIZE:
	    {
	    UInt32   attributes;

	    mdlDialog_listBoxGetInfo (&attributes, NULL, NULL, listP);

	    attributes &= ~(LISTATTR_SELBROWSE | LISTATTR_SELSINGLE |
			    LISTATTR_SELMULTI | LISTATTR_SELEXTENDED);
	    switch (listboxGlobs.selectionMode)
		{
		case 0:
		    attributes |= LISTATTR_SELBROWSE;
		    break;

		case 1:
		    attributes |= LISTATTR_SELSINGLE;
		    break;

		case 2:
		    attributes |= LISTATTR_SELMULTI;
		    break;

		case 3:
		    attributes |= LISTATTR_SELEXTENDED;
		    break;
		}

	    mdlDialog_listBoxSetInfo (&attributes, NULL, NULL, TRUE, listP);
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    if (!dimP->u.stateChanged.reallyChanged)
		break;

	    listmult_setSelectionList ();
	    break;

	case DITEM_MESSAGE_BUTTON:
	    if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
		{
		dimP->u.button.motionFunc = (void(*)(void))listmult_trackList;
		}
	    else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		{
		mdlDialog_itemSetLabel (dimP->db, listboxGlobs.labelDiP->itemIndex, "");
		}
	    break;

	case DITEM_MESSAGE_KEYSTROKE:
	    /* if the delete key is pressed, then delete the selected items */
	    if (dimP->u.keystroke.keystroke == VKEY_DELETE)
	    	{
	    	if (SUCCESS == listmult_deleteSelections (listP))
		    {
		    dimP->u.keystroke.hookHandled = TRUE;
		    }
	    	}

	    break;

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_maintainMultiListModifyMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_maintainMultiListModifyMenu
(
DItem_PulldownMenu  *menuP
)
    {
    int			    nSelections;
    RawItemHdr		   *listP;
    DItem_PulldownMenuItem  menuItem;

    if (!listboxGlobs.multiListDiP || !listboxGlobs.multiListDiP->rawItemP)
	return;

    listP = listboxGlobs.multiListDiP->rawItemP;
    mdlDialog_listBoxGetSelections (&nSelections, NULL, listP);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MENUSEARCHID_AddSelection))
        mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MENUSEARCHID_DeleteSelection))
        mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MENUSEARCHID_ToggleAddMode))
        mdlDialog_textPDMItemSetEnabled (&menuItem,
				listboxGlobs.selectionMode == SELECTIONMODE_EXTENDED);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_maintainMultiListSelectMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_maintainMultiListSelectMenu
(
DItem_PulldownMenu  *menuP
)
    {
    DItem_PulldownMenuItem  menuItem;
    BoolInt		    onlyOneElement =
				(listboxGlobs.selectionMode == SELECTIONMODE_BROWSE ||
				 listboxGlobs.selectionMode == SELECTIONMODE_SINGLE);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MENUSEARCHID_SelectEveryOther))
	mdlDialog_textPDMItemSetEnabled (&menuItem, !onlyOneElement);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MENUSEARCHID_SelectAll))
	mdlDialog_textPDMItemSetEnabled (&menuItem, !onlyOneElement);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
				    0, MENUSEARCHID_DeselectAll))
	mdlDialog_textPDMItemSetEnabled (&menuItem,
				listboxGlobs.selectionMode != SELECTIONMODE_BROWSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_maintainMultiListPositionMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_maintainMultiListPositionMenu
(
DItem_PulldownMenu  *menuP
)
    {
    int			    nSelections;
    RawItemHdr		   *listP;
    DItem_PulldownMenuItem  menuItem;

    if (!listboxGlobs.multiListDiP ||
	!listboxGlobs.multiListDiP->rawItemP)
	return;

    listP = listboxGlobs.multiListDiP->rawItemP;
    mdlDialog_listBoxGetSelections (&nSelections, NULL, listP);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL,
			    0, MENUSEARCHID_TopSelection))
        mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		listmult_maintainMultiListLocationMenu			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_maintainMultiListLocationMenu
(
DItem_PulldownMenu  *menuP
)
    {
    int			    nSelections;
    RawItemHdr		   *listP;
    DItem_PulldownMenuItem  menuItem;

    if (!listboxGlobs.multiListDiP || !listboxGlobs.multiListDiP->rawItemP)
	return;

    listP = listboxGlobs.multiListDiP->rawItemP;
    mdlDialog_listBoxGetSelections (&nSelections, NULL, listP);

    if (!mdlDialog_menuBarFindItem (&menuItem, &menuP, NULL, NULL, 0,
				    MENUSEARCHID_LocationSelection))
    mdlDialog_textPDMItemSetEnabled (&menuItem, nSelections > 0);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_multiListInsertItem				|
|                                                                       |
| author        BSI					11/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_multiListInsertItem
(
RawItemHdr  *listP,
StringList  *strListP,
int	     iRow,	    /* => row to insert before */
int	     nDisplayedRows,
int	     oldTopRow,
BoolInt	     appending
)
    {
    long	    iString;
    int	    newTopRow;
    char    buffer[80];

    iString = iRow * listboxGlobs.nColumns;
    mdlStringList_insertMember (&iString, strListP, iString, listboxGlobs.nColumns);

    if (appending)
	listmult_rscSprintf (buffer, MSGID_LeftAfter, iRow-1+'A', iRow-1);
    else
	listmult_rscSprintf (buffer, MSGID_LeftBefore, iRow+'A', iRow);
    mdlStringList_setMember (strListP, iString, buffer, NULL);

    if (listboxGlobs.nColumns > 1)
	{
	if (appending)
	    listmult_rscSprintf (buffer, MSGID_Right, iRow-1);
	else
	    listmult_rscSprintf (buffer, MSGID_Right, iRow);

	mdlStringList_setMember (strListP, iString+1, buffer, NULL);
	}

    mdlDialog_listBoxNRowsChanged (listP);

    newTopRow = iRow - nDisplayedRows/2;
    if (newTopRow < 0)  newTopRow = 0;
    mdlDialog_listBoxSetTopRowRedraw (listP, newTopRow, TRUE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_doMultiListModify				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_doMultiListModify
(
RawItemHdr  *listP,
int	     searchId
)
    {
    int		minRow, maxRow, nDisplayedRows;
    StringList *strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / listboxGlobs.nColumns;
	}

    mdlDialog_listBoxGetDisplayRange (&minRow, &maxRow, NULL, NULL, listP);
    nDisplayedRows = maxRow - minRow + 1;

    switch (searchId)
	{
	case MENUSEARCHID_ResetList:
	    mdlDialog_listBoxSelectCells (listP, 0, -1, 0, -1, FALSE, FALSE);
	    mdlStringList_deleteMember (strListP, 0, -1);
	    mdlStringList_insertMember (NULL, strListP, -1,
					NMULTILIST_LINES * 2);
	    listmult_multiListInitStrList (strListP);

	    mdlDialog_listBoxNRowsChanged (listP);
	    listmult_multiListInitDisabled (listP);

	    mdlDialog_listBoxSetTopRowRedraw (listP, 0, TRUE);
	    break;

	case MENUSEARCHID_RecreateList:
	    mdlStringList_destroy (strListP);
	    listP->userDataP = strListP =
				mdlStringList_create (NMULTILIST_LINES*2, 2);
	    listmult_multiListInitStrList (strListP);
	    mdlDialog_listBoxSetStrListP (listP, strListP, 2);
	    listmult_multiListInitDisabled (listP);
	    mdlDialog_listBoxSetTopRowRedraw (listP, 0, TRUE);

	    /* a stateChanged message won't be generated, fixup list
		manually */
	    listmult_setSelectionList ();
	    break;

	case MENUSEARCHID_AddTop:
	    listmult_multiListInsertItem (listP, strListP, 0, nDisplayedRows,
					  minRow, FALSE);
	    break;

	case MENUSEARCHID_AddBottom:
	    listmult_multiListInsertItem (listP, strListP, nRows,
					  nDisplayedRows, minRow, TRUE);
	    break;

	case MENUSEARCHID_AddSelection:
	    {
	    int		nSelections;
	    Spoint2d	*selections;

	    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
	    if (nSelections > 0 && selections)
		listmult_multiListInsertItem (listP, strListP, selections->y,
					      nDisplayedRows, minRow, FALSE);
	    break;
	    }

	case MENUSEARCHID_DeleteSelection:
	    {
	    listmult_deleteSelections (listP);

	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_doMultiListSelect				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_doMultiListSelect
(
RawItemHdr  *listP,
int	     searchId
)
    {
    StringList	*strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / listboxGlobs.nColumns;
	}

    switch (searchId)
	{
	case MENUSEARCHID_SelectTop:
	    {
	    Spoint2d	selectionPt;

	    selectionPt.x = selectionPt.y = 0;
	    mdlDialog_listBoxSetSelections (listP, 1, &selectionPt, TRUE,
					    TRUE);
	    break;
	    }

	case MENUSEARCHID_SelectBottom:
	    {
	    Point2d	selectionPt;

	    selectionPt.y = nRows - 1;
	    selectionPt.x = 0;
	    mdlDialog_listBoxSetSelectionsX (listP, 1, &selectionPt, TRUE,
					    TRUE);
	    break;
	    }

	case MENUSEARCHID_SelectEveryOther:
	    if ((listboxGlobs.selectionMode == SELECTIONMODE_MULTI) ||
		(listboxGlobs.selectionMode == SELECTIONMODE_EXTENDED))
		{
		int	     iRow;
		int	     nSelectedRows = 0;
		Point2d     *curSelectionP, *selectionsP = NULL;

		selectionsP = (Point2d*)dlmSystem_mdlMalloc ((nRows/2+1)*sizeof(Point2d));
		if (!selectionsP)
		    break;

		for (iRow=0, curSelectionP=selectionsP; iRow<nRows;
			iRow+=2, curSelectionP++, nSelectedRows++)
		     {
		     curSelectionP->y = iRow;
		     curSelectionP->x = 0;
		     }

		mdlDialog_listBoxSetSelectionsX (listP, nSelectedRows,
						selectionsP, TRUE, TRUE);
		free (selectionsP);
		}
	    break;

	case MENUSEARCHID_SelectAll:
	    if ((listboxGlobs.selectionMode == SELECTIONMODE_MULTI) ||
		(listboxGlobs.selectionMode == SELECTIONMODE_EXTENDED))
		{
		mdlDialog_listBoxSelectCells (listP, 0, -1, 0, -1, TRUE,
					      TRUE);
		}
	    break;

	case MENUSEARCHID_DeselectTop:
	    mdlDialog_listBoxSelectCells (listP, 0, 0, 0, -1, FALSE, TRUE);
	    break;

	case MENUSEARCHID_DeselectBottom:
	    mdlDialog_listBoxSelectCells (listP, nRows-1, nRows-1, 0, -1,
					  FALSE, TRUE);
	    break;

	case MENUSEARCHID_DeselectAll:
	    mdlDialog_listBoxSelectCells (listP, 0, -1, 0, -1, FALSE, TRUE);
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_doMultiListPosition				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_doMultiListPosition
(
RawItemHdr  *listP,
int	     searchId
)
    {
    int		minRow, maxRow, nDisplayedRows;
    StringList	*strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / listboxGlobs.nColumns;
	}

    mdlDialog_listBoxGetDisplayRange (&minRow, &maxRow, NULL, NULL, listP);
    nDisplayedRows = maxRow - minRow + 1;

    switch (searchId)
	{
	case MENUSEARCHID_Top1st:
	    mdlDialog_listBoxSetTopRow (listP, 0);
	    break;

	case MENUSEARCHID_BottomLast:
	    {
	    int	newTopRow;

	    newTopRow = nRows - nDisplayedRows;
	    if (newTopRow < 0)
		newTopRow = 0;

	    mdlDialog_listBoxSetTopRow (listP, newTopRow);
	    break;
	    }

	case MENUSEARCHID_TopSelection:
	    {
	    int		nSelections;
	    Spoint2d	*selections;

	    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
	    if (nSelections > 0 && selections)
		mdlDialog_listBoxSetTopRow (listP, selections->y);
	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_doMultiListLocation				|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listmult_doMultiListLocation
(
RawItemHdr  *listP,
int	     searchId
)
    {
    StringList	*strListP;
    int		nRows = 0;

    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (strListP)
	{
	int nMembers;

	nMembers = mdlStringList_size (strListP);
	nRows = nMembers / listboxGlobs.nColumns;
	}

    switch (searchId)
	{
	case MENUSEARCHID_LocationTop:
	    {
	    int newTopRow;

	    mdlDialog_listBoxSetLocationCursor (listP, 0, 0);

	    /* need to get location cursor since first rows may be disabled */
	    mdlDialog_listBoxGetLocationCursor (&newTopRow, NULL, listP);
	    mdlDialog_listBoxSetTopRow (listP, newTopRow);
	    break;
	    }

	case MENUSEARCHID_LocationBottom:
	    {
	    int newTopRow;
	    int	minRow, maxRow, nDisplayedRows;

	    mdlDialog_listBoxSetLocationCursor (listP, nRows-1, 0);

	    /* need to get location cursor since last rows may be disabled */
	    mdlDialog_listBoxGetLocationCursor (&newTopRow, NULL, listP);

	    mdlDialog_listBoxGetDisplayRange (&minRow, &maxRow, NULL, NULL,
					      listP);
	    nDisplayedRows = maxRow - minRow + 1;
	    newTopRow = newTopRow - nDisplayedRows + 1;
	    if (newTopRow < 0)
		newTopRow = 0;

	    mdlDialog_listBoxSetTopRow (listP, newTopRow);
	    break;
	    }

	case MENUSEARCHID_LocationSelection:
	    {
	    int		nSelections;
	    Spoint2d	*selections;

	    mdlDialog_listBoxGetSelections (&nSelections, &selections, listP);
	    if (nSelections > 0 && selections)
		{
		mdlDialog_listBoxSetLocationCursor (listP, selections->y, 0);
		listmult_doMultiListPosition (listP, MENUSEARCHID_TopSelection);
		}
	    break;
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_multiListTestsMenusHook			|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Public void listmult_multiListTestsMenusHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_DRAW:
	    {
	    PulldownMenuMessage	   *pdmmP =
					(PulldownMenuMessage *)dimP->auxInfoP;
	    DItem_PulldownMenu	   *menuP = pdmmP->menuP;

	    if (menuP->type != RTYPE_PulldownMenu)
		break;
	    /* need to switch on menuP->id because multiple menus were attached
	       to this hook function */
	    switch (menuP->id)
		{
		case PULLDOWNMENUID_MultiListModify:
		    listmult_maintainMultiListModifyMenu (menuP);
		    break;

		case PULLDOWNMENUID_MultiListSelect:
		    listmult_maintainMultiListSelectMenu (menuP);
		    break;

		case PULLDOWNMENUID_MultiListPosition:
		    listmult_maintainMultiListPositionMenu (menuP);
		    break;

		case PULLDOWNMENUID_MultiListLocation:
		    listmult_maintainMultiListLocationMenu (menuP);
		    break;
		}

	    break;
	    }

	case DITEM_MESSAGE_BUTTON:
	    {
	    PulldownMenuMessage	   *pdmmP =
					(PulldownMenuMessage *)dimP->auxInfoP;
	    DItem_PulldownMenu	   *menuP;
	    RawItemHdr		   *listP;
	    int			    searchId;

	    if (dimP->u.button.buttonTrans != BUTTONTRANS_UP)
		break;
	    menuP = pdmmP->menuP;

	    if (!listboxGlobs.multiListDiP || !listboxGlobs.multiListDiP->rawItemP)
		break;
	    listP = listboxGlobs.multiListDiP->rawItemP;
	    searchId = pdmmP->u.activateItem.searchId;

	    /* need to switch on menuP->id because multiple menus were
		attached to this hook function */
	    switch (menuP->id)
		{
		case PULLDOWNMENUID_MultiListModify:
		    listmult_doMultiListModify (listP, searchId);
		    break;

		case PULLDOWNMENUID_MultiListSelect:
		    listmult_doMultiListSelect (listP, searchId);
		    break;

		case PULLDOWNMENUID_MultiListPosition:
		    listmult_doMultiListPosition (listP, searchId);
		    break;

		case PULLDOWNMENUID_MultiListLocation:
		    listmult_doMultiListLocation (listP, searchId);
		    break;
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listmult_multiListDialogHook				|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Public void	listmult_multiListDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    listboxGlobs.selectionMode = SELECTIONMODE_BROWSE;
	    listboxGlobs.nColumns = 2;

	    dmP->u.create.interests.nonDataPoints = TRUE;
	    
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

