/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listbox.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listbox.r_v  $
|   $Workfile:   listbox.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:04 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   listbox.r  - Application resource definitions.		    	|
|									|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */

#include "listbox.h"
#include "listbtxt.h"
#include "listbcmd.h"

/*----------------------------------------------------------------------+
|									|
|   Toggle Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Menu Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_PulldownMenuRsc PULLDOWNMENUID_ListBox =
    {
    NOHELP, MHELPTASKIDTOPIC,
    NOHOOK,
    ON, TXT_ApplicationName,
{
{TXT_ListIconMenuItem,  NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	0,
			CMD_LISTBOX_OPEN_LISTICON, OTASKID, ""},

{TXT_SashListBoxMenuItem,	NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	0,
			CMD_LISTBOX_OPEN_SASH, OTASKID, ""},

{TXT_MultiListBoxMenuItem,  	NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	0,
			CMD_LISTBOX_OPEN_MULTILIST, OTASKID, ""},

{TXT_ColoredListBoxMenuItem,  	NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	0,
			CMD_LISTBOX_OPEN_COLORS, OTASKID, ""},

}
    };

