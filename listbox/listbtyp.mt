/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listbtyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listbtyp.mtv  $
|   $Workfile:   listbtyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:06 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   listbtyp.mt								|
|									|
|	Publish dialog box structures				    	|
|									|
+----------------------------------------------------------------------*/
#include    "listbox.h"

publishStructures (listboxglobals);

