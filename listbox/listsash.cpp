/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listsash.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: listsash.cpp,v $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:39:16 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MDL example to dialog box with a sash and 2 list box items	|
|									|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>	    /* MDL Library funcs structures & constants*/
#include    <dlogitem.h>    /* Dialog Box Manager structures & constants*/
#include    <stdio.h>

#include    <dlogman.fdf>   /* dialog box manager function prototypes */
#include    <mswindow.fdf>
#include    <msrsrc.fdf>

#include    "listbox.h"     /* dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
extern ListBoxGlobals listboxGlobs;

/*----------------------------------------------------------------------+
|                                                                       |
|   Hook Functions							|
|                                                                       |
+----------------------------------------------------------------------*/ 

/*----------------------------------------------------------------------+
|									|
|   Sash Example Dialog Routines					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          listsash_adjustSashDialogItems				|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listsash_adjustSashDialogItems
(
DialogBox   *db,
int	     deltaWidth
)
    {
    int		 size, colWidth;
    UInt32      nRows;
    int		 fontHeight = mdlDialog_fontGetCurHeight (db);
    Point2d	 pt;
    BSIRect	 contentRect;
    DialogItem	*diP;
    RawItemHdr	*sashP;
    RawItemHdr	*topListP;
    RawItemHdr	*bottomListP;
    int		 rowHeight;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Sash, SASHID_Example, 0);
    if (NULL == diP)
	return;
    sashP = diP->rawItemP;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ListBox, LISTBOXID_Sash1, 0);
    if (NULL == diP)
	return;
    topListP = diP->rawItemP;

    diP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ListBox, LISTBOXID_Sash2, 0);
    if (NULL == diP)
	return;
    bottomListP = diP->rawItemP;

    mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) db);

    /* Can't hide the items since this removes the focus, so just clear the area */
/*  mdlDialog_itemHide (db, topListP->diP->itemIndex, TRUE);
    mdlDialog_itemHide (db, bottomListP->diP->itemIndex, TRUE); */

    mdlWindow_rectClear ((GuiWindowP) db, &topListP->diP->rect, NULL);
    mdlWindow_rectClear ((GuiWindowP) db, &bottomListP->diP->rect, NULL);

    /* adjust size of column 0 so listBox is horizontally resized */
    mdlDialog_listBoxGetColInfo (&colWidth, NULL, NULL, NULL, topListP, 0);
    if (deltaWidth != 0)
	{
	int listWidth;	    /* size list including scrollBar & bevel */
	int colWidthPixels; /* width of column in pixels */
	int adjustWidth;    /* subtracted from listWidth gives width col in pixels */

	listWidth      = mdlDialog_rectWidth (&topListP->diP->rect);
	colWidthPixels = mdlDialog_toPixels (db, colWidth);
	adjustWidth    = listWidth - colWidthPixels;
	colWidthPixels = (sashP->itemRect.corner.x - fontHeight/2) - adjustWidth;

	colWidth = mdlDialog_toDCoord (db, colWidthPixels);
	mdlDialog_listBoxSetColInfo (&colWidth, NULL, NULL, NULL, topListP,
				     0, FALSE);
	}

    /* adjust horizontal positions of lists so scrollBars line up with sash
       (see Visual Design with OSF/Motif, Kobara, pages 93-94) */

    pt.y = -topListP->itemRect.origin.y;   /* neg num means pixels specified */

    pt.x = -(topListP->itemRect.origin.x +
	    (sashP->itemRect.corner.x - topListP->diP->rect.corner.x));
    mdlDialog_itemMove (db, topListP->diP->itemIndex, &pt, FALSE);

    /* fix vertical size of top List */
    size  = sashP->itemRect.origin.y - topListP->itemRect.origin.y -
		(fontHeight*3/4);
    mdlDialog_listBoxGetHeights (&rowHeight, NULL, NULL, topListP);
    nRows = size / rowHeight;
    mdlDialog_listBoxSetInfo (NULL,  &nRows, NULL, FALSE, topListP);
    mdlDialog_itemDraw (db, topListP->diP->itemIndex);

    /* adjust size of column 0 so listBox is horizontally resized */
    if (deltaWidth != 0)
	{
	mdlDialog_listBoxSetColInfo (&colWidth, NULL, NULL, NULL, bottomListP,
				     0, FALSE);
	}

    /* Move bottom list; neg num means pixels specified */
    pt.y = -(sashP->itemRect.corner.y + fontHeight/2);
    pt.x = -(bottomListP->itemRect.origin.x +
	    (sashP->itemRect.corner.x - bottomListP->diP->rect.corner.x));
    mdlDialog_itemMove (db, bottomListP->diP->itemIndex, &pt, FALSE);

    /* fix size of bottom List */
    size  = contentRect.corner.y + pt.y - (fontHeight*3/4);
    mdlDialog_listBoxGetHeights (&rowHeight, NULL, NULL, bottomListP);
    nRows = size / rowHeight;
    mdlDialog_listBoxSetInfo (NULL,  &nRows, NULL, FALSE, bottomListP);

    mdlDialog_itemDraw (db, bottomListP->diP->itemIndex);

    mdlDialog_itemDraw (db, sashP->diP->itemIndex);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listsash_sashMotionFunc					|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void listsash_sashMotionFunc	/* called each time user moves mouse
					   while dragging sash */
(
MotionFuncArg	*mfaP	/* <> mfaP->pt.y = where sash upper left corner will go */
			/* => mfaP->pt.x = where cursor y position is */
			/* => mfaP->dragging = TRUE if cursor is within window */
)
    {
    if (mfaP->pt.y < listboxGlobs.minSashY)
	mfaP->pt.y = listboxGlobs.minSashY;

    if (mfaP->pt.y > listboxGlobs.maxSashY)
	mfaP->pt.y = listboxGlobs.maxSashY;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listsash_sashHook					|
|                                                                       |
| author        BSI					05/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Public void listsash_sashHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_BUTTON:
	    {
	    if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
		{
		BSIRect	 contentRect;
		int	 fontHeight = mdlDialog_fontGetCurHeight (dimP->db);

		dimP->u.button.motionFunc = (void(*)(void))listsash_sashMotionFunc;

		mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) dimP->db);
		listboxGlobs.minSashY = 5*fontHeight;
		listboxGlobs.maxSashY =
		   mdlDialog_rectHeight (&contentRect) - 5*fontHeight;
		}
	    else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		{
		Sash_ButtonUpAuxInfo *buaiP =
		    (Sash_ButtonUpAuxInfo *) dimP->auxInfoP;

		/* 
		//  use buaiP->newYPos to determine where upperLeft corner
		//  of sash beveled rect will go.  This message is sent after
		//  sash has been erased from old position & moved, but before
		//  it has been drawn 
		*/
		if (buaiP->newYPos != buaiP->oldYPos)
		    listsash_adjustSashDialogItems (dimP->db, 0);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listsash_sashListBoxHook				|
|                                                                       |
| author        BSI					05/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Public void listsash_sashListBoxHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int		 iString;
	    char	 formatStr[20];
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP;

	    strListP = mdlStringList_create (15, 1);
	    if (NULL == strListP)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    mdlResource_loadFromStringList (formatStr, NULL,
					    MESSAGELISTID_ListBox,
					    MSGID_Line);
	    for (iString=0; iString<15; iString++)
		{
		char    buffer[80];

		sprintf (buffer, formatStr, iString);
		mdlStringList_setMember (strListP, iString, buffer, NULL);
		}

	    mdlDialog_listBoxSetStrListP (rihP, strListP, 1);
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    StringList	*strListP;
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;

	    strListP = mdlDialog_listBoxGetStrListP(rihP);
	    if (NULL != strListP)
		{
		mdlStringList_destroy (strListP);
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listsash_sashDialogHook					|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Public void	listsash_sashDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    dmP->u.create.interests.resizes   = TRUE;
	    dmP->u.create.interests.mouses    = TRUE;
	    dmP->u.create.interests.nonDataPoints = TRUE;
	    break;
	    }

	case DIALOG_MESSAGE_INIT:
	    {
	    DialogItem	*diP;

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_Sash,
						SASHID_Example, 0);
	    if (NULL == diP)
		break;

	    listsash_adjustSashDialogItems (dmP->db, 0);
	    break;
	    }

	case DIALOG_MESSAGE_RESIZE:
	    {
	    int	    newWidth, oldWidth;

	    if (CORNER_ALL == dmP->u.resize.whichCorners)
		break;
	    if (1 == mdlDialog_rectHeight (&dmP->u.resize.newContent))
		/* dialog was minimized */
		break;	    

	    newWidth = mdlDialog_rectWidth (&dmP->u.resize.newContent);
	    oldWidth = mdlDialog_rectWidth (&dmP->u.resize.oldContent);

	    listsash_adjustSashDialogItems (dmP->db, newWidth-oldWidth);
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }
