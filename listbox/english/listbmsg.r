/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/english/listbmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/english/listbmsg.r_v  $
|   $Workfile:   listbmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:20 $
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>

#include "listbox.h"

MessageList MESSAGELISTID_ListBox =
    {
      {
      { MSGID_CommandTable,	    "Unable to load command table."},
      { MSGID_DialogBox,	    "Unable to open dialog box."},
      { MSGID_DirSelection,	    "User selected directory, \"%s\""},
      { MSGID_CheckBoxPopulateErr,  "Unable to load check box list box"},
      { MSGID_Line,		    "Line %d" },
      { MSGID_Left,		    "%c Left %d" },
      { MSGID_Right,    	    "Right %d" },
      { MSGID_LeftAfter,	    "%c Left %d After" },
      { MSGID_LeftBefore,	    "%c Left %d Before" },
      }
    };
