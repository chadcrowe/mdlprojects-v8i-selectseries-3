/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/english/listbtxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/english/listbtxt.h_v  $
|   $Workfile:   listbtxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:21 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Static text defines for the dialog box resource labels		|
|									|
+----------------------------------------------------------------------*/
#if !defined (__listbtxtH__)
#define __listbtxtH__

#define TXT_ApplicationName	    	"List Box"

#define TXT_ListIcon		    	"List Box with Icons"
#define TXT_DirectoryItemLabel      	"Directory"

#define	TXT_ListIconMenuItem	    	"List box with ~Icons"
#define TXT_SashListBoxMenuItem     	"~Sash"
#define TXT_ColoredListBoxMenuItem  	"~Color in List Box"
#define TXT_MultiListBoxMenuItem    	"~Multi-Selection List"


/*----------------------------------------------------------------------+
|  Definitions for sash	example					    	|
+----------------------------------------------------------------------*/
#define	TXT_Sash			"Sash"


/*----------------------------------------------------------------------+
|  Definitions for multi-selection list box 				|
+----------------------------------------------------------------------*/
#define TXT_SelectionMode	    	"Selection Mode"
#define TXT_Browse		    	"~Browse"
#define TXT_Single		    	"~Single"
#define TXT_Multiple		    	"~Multiple"
#define TXT_Extended		    	"~Extended"

#define	TXT_Tests			"~Tests"
#define	TXT_Modify			"~Modify"
#define	TXT_Select			"~Select"
#define	TXT_Position			"~Position"
#define	TXT_Location			"~Location"
#define	TXT_Exit			"E~xit"
#define	TXT_ResetList			"~Reset List"
#define	TXT_RecreateList		"Re~create List"
#define	TXT_AddItemAtTop		"Add Item At ~Top"
#define	TXT_AddItemAtBottom		"Add Item At ~Bottom"
#define	TXT_AddBeforeSelection		"Add Before ~Selection"
#define	TXT_DeleteSelection		"~Delete Selection"
#define	TXT_ToggleAddMode		"Toggle ~Add Mode"
#define	TXT_SelectTopItem		"Select ~Top Item"
#define	TXT_SelectBottomItem		"Select ~Bottom Item"
#define	TXT_SelectEveryOther		"Select ~Every Other"
#define	TXT_SelectAll			"~Select All"
#define	TXT_DeselectTopItem		"Deselect Top ~Item"
#define	TXT_DeselectBottomItem		"Dese~lect Bottom Item"
#define	TXT_DeselectAll			"~Deselect All"
#define	TXT_Make1stTop			"Make ~1st Top"
#define	TXT_MakeLastBottom		"Make ~Last Bottom"
#define	TXT_Make1stSelectionTop		"Make 1st Selection ~Top"
#define	TXT_MoveToTopItem		"Move To ~Top Item"
#define	TXT_MoveToBottomItem		"Move To ~Bottom Item"
#define	TXT_MoveToBeginningofSelection	"Move To Beginning of ~Selection"
#define	TXT_CurrentSelection		"~Current Selection:"
#define	TXT_ExampleList			"~Example List:"
#define	TXT_Row				"Row"
#define	TXT_Col				"Col"
#define	TXT_MultiSelectDialogTitle    	"Multi-Selection ListBox"

/*----------------------------------------------------------------------+
|  Definitions for colored list box				    	|
+----------------------------------------------------------------------*/
#define	TXT_ColoredItems	    	"Colored Items"
#define TXT_ColoredToggleLabel      	"List box Background Is ~Colored"

#endif /* __listbtxtH__ */
