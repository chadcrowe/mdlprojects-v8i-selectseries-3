/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listbox.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listbox.h_v  $
|   $Workfile:   listbox.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:01 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   listbox.h								|
|									|
|	Constants & types used in new items dialog example		|
|									|
+----------------------------------------------------------------------*/
#if !defined (__listboxH__)
#define	    __listboxH__

#ifndef __dlogitemH__
#include <dlogitem.h>
#endif

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define BASEID_ListBox			    	20

/*----------------------------------------------------------------------+
|									|
|   Dialog Box ID's							|
|									|
+----------------------------------------------------------------------*/
#define DIALOGID_ListIcon		    	(BASEID_ListBox + 1)
#define DIALOGID_MultiList		    	(BASEID_ListBox + 2)
#define DIALOGID_SashList		    	(BASEID_ListBox + 3)
#define DIALOGID_ColorList		    	(BASEID_ListBox + 4)

/*----------------------------------------------------------------------+
|									|
|   List Box ID's							|
|									|
+----------------------------------------------------------------------*/
#define LISTBOXID_Directories		    	(BASEID_ListBox - 1)
#define LISTBOXID_Choices		    	(BASEID_ListBox - 2)
#define LISTBOXID_Sash1			    	(BASEID_ListBox - 3)
#define LISTBOXID_Sash2			    	(BASEID_ListBox - 4)
#define LISTBOXID_Multi			    	(BASEID_ListBox - 5)
#define LISTBOXID_SelectionList		    	(BASEID_ListBox - 6)
#define LISTBOXID_ColoredList		    	(BASEID_ListBox - 7)
					
/*----------------------------------------------------------------------+
|									|
|   Text ID's								|
|									|
+----------------------------------------------------------------------*/
#define TEXTID_Directory		    	(BASEID_ListBox - 1)

/*----------------------------------------------------------------------+
|									|
|   Sash ID's								|
|									|
+----------------------------------------------------------------------*/
#define SASHID_Example			    	(BASEID_ListBox - 1)
		
/*----------------------------------------------------------------------+
|									|
|   Menu Bar ID's							|
|									|
+----------------------------------------------------------------------*/
#define MENUBARID_MultiList		    	(BASEID_ListBox - 1)
				 
/*----------------------------------------------------------------------+
|									|
|   Synonym ID's							|
|									|
+----------------------------------------------------------------------*/
#define SYNONYMID_MultiList		    	(BASEID_ListBox - 1)
				 
/*----------------------------------------------------------------------+
|									|
|   Pulldown Menu ID's							|
|									|
+----------------------------------------------------------------------*/
#define PULLDOWNMENUID_ListBox		    	(BASEID_ListBox - 1)
#define PULLDOWNMENUID_MultiListTests	    	(BASEID_ListBox - 2)
#define PULLDOWNMENUID_MultiListModify	    	(BASEID_ListBox - 3)
#define PULLDOWNMENUID_MultiListSelect	    	(BASEID_ListBox - 4)
#define PULLDOWNMENUID_MultiListPosition    	(BASEID_ListBox - 5)
#define PULLDOWNMENUID_MultiListLocation    	(BASEID_ListBox - 6)

/*----------------------------------------------------------------------+
|									|
|   Menu Search ID's							|
|									|
+----------------------------------------------------------------------*/
#define MENUSEARCHID_ResetList		1
#define MENUSEARCHID_RecreateList	2
#define MENUSEARCHID_AddTop		3
#define MENUSEARCHID_AddBottom		4
#define MENUSEARCHID_AddSelection	5
#define MENUSEARCHID_DeleteSelection	6
#define MENUSEARCHID_ToggleAddMode	7

#define MENUSEARCHID_SelectTop		1
#define MENUSEARCHID_SelectBottom	2
#define MENUSEARCHID_SelectEveryOther	3
#define MENUSEARCHID_SelectAll		4
#define MENUSEARCHID_DeselectTop	5
#define MENUSEARCHID_DeselectBottom	6
#define MENUSEARCHID_DeselectAll	7

#define MENUSEARCHID_Top1st		1
#define MENUSEARCHID_BottomLast		2
#define MENUSEARCHID_TopSelection	3

#define MENUSEARCHID_LocationTop	1
#define MENUSEARCHID_LocationBottom	2
#define MENUSEARCHID_LocationSelection	3

/*----------------------------------------------------------------------+
|									|
|   Option button ID's						    	|
|									|
+----------------------------------------------------------------------*/
#define OPTIONBTNID_SelectionMode	    	(BASEID_ListBox - 1)

/*----------------------------------------------------------------------+
|									|
|   Toggle button ID's						    	|
|									|
+----------------------------------------------------------------------*/
#define TOGGLEBTNID_Colored 		    	(BASEID_ListBox - 1)

/*----------------------------------------------------------------------+
|									|
|   Message List IDs							|
|									|
+----------------------------------------------------------------------*/
#define MESSAGELISTID_ListBox		    	(BASEID_ListBox - 1)

/*----------------------------------------------------------------------+
|									|
|   Message List Entry Ids						|
|									|
+----------------------------------------------------------------------*/
#define MSGID_CommandTable		    	(BASEID_ListBox + 1)
#define MSGID_DialogBox			    	(BASEID_ListBox + 2)
#define MSGID_DirSelection		    	(BASEID_ListBox + 3)
#define MSGID_CheckBoxPopulateErr	    	(BASEID_ListBox + 4)
#define MSGID_Line			    	(BASEID_ListBox + 5)
#define MSGID_Left			    	(BASEID_ListBox + 6)
#define MSGID_Right			    	(BASEID_ListBox + 7)
#define MSGID_LeftAfter			    	(BASEID_ListBox + 8)
#define MSGID_LeftBefore		    	(BASEID_ListBox + 9)

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKDIALOGID_ListIcon	    		(BASEID_ListBox + 1)
#define HOOKDIALOGID_DialogSash		    	(BASEID_ListBox + 2)
#define HOOKDIALOGID_MultiList		    	(BASEID_ListBox + 3)
#define HOOKDIALOGID_Colors		    	(BASEID_ListBox + 4)

#define HOOKITEMID_ListIcon_FileListDirectory	(BASEID_ListBox - 1)
#define HOOKITEMID_CheckListBox	    		(BASEID_ListBox - 2)
#define HOOKITEMID_ListBoxSash		    	(BASEID_ListBox	- 3)
#define	HOOKITEMID_Sash			    	(BASEID_ListBox	- 4)
#define HOOKITEMID_PDMMultiListTests	    	(BASEID_ListBox	- 5)
#define HOOKITEMID_ListBoxMulti		    	(BASEID_ListBox	- 6)
#define HOOKITEMID_ToggleBtnColor	    	(BASEID_ListBox	- 7)
#define HOOKITEMID_ColoredListBox	    	(BASEID_ListBox	- 8)


/*----------------------------------------------------------------------+
|									|
|   Selection Modes							|
|									|
+----------------------------------------------------------------------*/
#define SELECTIONMODE_BROWSE	0
#define SELECTIONMODE_SINGLE	1
#define SELECTIONMODE_MULTI	2
#define SELECTIONMODE_EXTENDED	3

/*----------------------------------------------------------------------+
|									|
|   Color Descriptor Array Indices					|
|									|
+----------------------------------------------------------------------*/
#define LCOLOR_BACKGROUND	0
#define LCOLOR_TOPSHADOW	1
#define LCOLOR_BOTTOMSHADOW	2
#define LCOLOR_SELECT		3
#define LCOLOR_FGTEXT		4
#define LCOLOR_BGTEXT		5
#define LCOLOR_DGN		6
#define	LCOLOR_LABELTEXT	7

#define NLOCAL_COLORS		8

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
#if !defined (resource)
typedef struct listboxglobals
    {
    int     	 colorToggle;

    int		 minSashY;
    int		 maxSashY;
    int		 selectionMode;

    char	 string[80];
    DialogBox	*multiSelectDbP;
    DialogItem	*labelDiP;		/* label item in multi-select dialog */
    DialogItem	*multiListDiP;
    DialogItem	*selectionListDiP;
    int		 nColumns;		/* # of columns in multiSelect list */

    /* Global color information */
    BSIColorDescr *localColorsP[NLOCAL_COLORS];
    BSIColorPalette *localPalP;
    
    char directoryName[MAXDIRLENGTH];	
    } ListBoxGlobals;
#endif

#endif /* __listboxH__ */