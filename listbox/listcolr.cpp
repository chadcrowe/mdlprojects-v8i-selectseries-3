/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listcolr.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: listcolr.cpp,v $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:39:07 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|	MDL example to color in a list box				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>	    /* MDL Library funcs structures & constants */
#include    <dlogitem.h>    /* Dialog Box Manager structures & constants */
#include    <colrname.h>
#include    <stdio.h>
#include    <keys.h>

#include    <dlogman.fdf>   /* dialog box manager function prototypes */
#include    <mscolor.fdf>   /* color manager function prototypes */
#include    <msrsrc.fdf>   

#include    "listbox.h"     /* basic dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
Private BSIColorDescr *localColorsP[NLOCAL_COLORS];

/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
extern ListBoxGlobals listboxGlobs;

/*----------------------------------------------------------------------+
|									|
|   Utility routines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Name	listcolr_deleteCurrentSelection				|
|                                                                       |
|   Author	BSI                                         2/96        |
|                                                                       |
|   Deletes the selected row from a list box.                           |
+----------------------------------------------------------------------*/
Private int listcolr_deleteCurrentSelection
(
RawItemHdr  *listP	    /*  => List box raw item header pointer	*/
)
    {
    int     	status = SUCCESS;
    int		rowIndex, colIndex;
    BoolInt	found = FALSE;
    StringList	*strListP;

    /* if the raw item header pointer is NULL then return ERROR */
    if (NULL == listP)
    	{
    	return (ERROR);
    	}

    /* if unable to get the string list pointer then return ERROR */
    strListP = mdlDialog_listBoxGetStrListP (listP);
    if (NULL == strListP)
	{
    	return (ERROR);
	}

    rowIndex = -1;
    colIndex = -1;

    /* Find the selected row */
    mdlDialog_listBoxGetNextSelection(&found, &rowIndex, &colIndex, listP);
    if (TRUE == found)
	{

	/* delete the selected row */
	if (SUCCESS == (status=mdlStringList_deleteMember(strListP, rowIndex, 1)))
	    {

	    /*------------------------------------------------------
	    	if deleted row is the last in the string list then
		set the rowIndex so that the row above is selected 
	    ------------------------------------------------------*/
	    if (rowIndex == mdlStringList_size(strListP))
	    	{
		rowIndex--;
	    	}

	    /* select the row below the deleted one */
	    mdlDialog_listBoxSelectCells (listP, rowIndex,
		    	 rowIndex, -1, -1, TRUE, TRUE);

	    /* inform the list box that the string list has changed */
	    mdlDialog_listBoxNRowsChanged (listP);

	    /* redraw the list box */
	    mdlDialog_listBoxDrawContents (listP, -1, -1);

	    }
	}

    return  (status);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          listcolr_scaleColor					|
|                                                                       |
| author        BSI					10/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Private UChar listcolr_scaleColor
(
UChar	color,
double	scaleFactor
)
    {
    double  temp = color * scaleFactor;
    color = (UChar) temp;
    if (color > 255)
	color = 255;

    return (color);
    }

/*----------------------------------------------------------------------+
|									|
| name		listcolr_initializeColorDescriptors			|
|									|
| author	BSI				    11/94		|
|									|
+----------------------------------------------------------------------*/
Public int	listcolr_initializeColorDescriptors
(
BSIColorPalette **palPP,
BSIColorDescr   **colorsPP,	    /* <= color descr array to initialize */
int	    	numColorDescrs      /* => number of colors in the array */
)
    {
    int     	status;
    long	iColor;
    BSIColorDescr **cdPP;
    RGBColorDef	backgroundRgb;
    RGBColorDef	rgb;

    /*-------------------------------------------------------------------
	Create a palette of color descriptors to use for our color
	customized items.
    -------------------------------------------------------------------*/
    if ((status = mdlColorPal_create (palPP, numColorDescrs))
	!= SUCCESS)
	return status;

    /* setup local color array for easy access to color descriptors */
    for (iColor=0, cdPP=colorsPP; iColor < numColorDescrs; iColor++, cdPP++)
	*cdPP = mdlColorPal_getColorDescr (*palPP, iColor);

    /* Use the X color id for Light Blue to set a color descriptor to be used
       as a lt blue background color */
    mdlColorDescr_setByColorId (colorsPP[LCOLOR_BACKGROUND], LIGHT_BLUE, FALSE);
    mdlColorDescr_getRgb (&backgroundRgb, colorsPP[LCOLOR_BACKGROUND]);

    /* Make the select color a little darker than background */
    rgb = backgroundRgb;
    rgb.red   = listcolr_scaleColor (rgb.red, 0.85);
    rgb.green = listcolr_scaleColor (rgb.green, 0.85);
    rgb.blue  = listcolr_scaleColor (rgb.blue, 0.85);
    mdlColorDescr_setByRgb (colorsPP[LCOLOR_SELECT], &rgb, FALSE);

    /* Make the top shadow color a little lighter than the background */
    rgb = backgroundRgb;
    rgb.red   = listcolr_scaleColor (rgb.red, 1.5);
    rgb.green = listcolr_scaleColor (rgb.green, 1.5);
    rgb.blue  = listcolr_scaleColor (rgb.blue, 1.5);
    mdlColorDescr_setByRgb (colorsPP[LCOLOR_TOPSHADOW], &rgb, FALSE);

    /* Make the bottom shadow color alot darker */
    rgb = backgroundRgb;
    rgb.red   = listcolr_scaleColor (rgb.red, 0.5);
    rgb.green = listcolr_scaleColor (rgb.green, 0.5);
    rgb.blue  = listcolr_scaleColor (rgb.blue, 0.5);
    mdlColorDescr_setByRgb (colorsPP[LCOLOR_BOTTOMSHADOW], &rgb, FALSE);

    /* Use some of the standard "menu colors" (Window Manager colors). */
    mdlColorDescr_setByMenuColor (colorsPP[LCOLOR_BGTEXT],  	MGREY_INDEX);
    mdlColorDescr_setByMenuColor (colorsPP[LCOLOR_FGTEXT],  	WHITE_INDEX);
    mdlColorDescr_setByMenuColor (colorsPP[LCOLOR_LABELTEXT], 	RED_INDEX);

    /* Use an element color for one of our descriptors */
    mdlColorDescr_setByElemColorNumber (colorsPP[LCOLOR_DGN], 4);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Hook Functions							|
|                                                                       |
+----------------------------------------------------------------------*/ 
/*----------------------------------------------------------------------+
|                                                                       |
| name          listcolr_colorsDialogHook				|
|                                                                       |
| author        BSI					09/92           |
|                                                                       |
+----------------------------------------------------------------------*/
Public void	listcolr_colorsDialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    dmP->u.create.interests.mouses    = TRUE;
	    dmP->u.create.interests.keystrokes = TRUE;
	    break;
	    }

	case DIALOG_MESSAGE_INIT:
	    {
	    DialogItem	*diP;

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_ToggleButton,
						TOGGLEBTNID_Colored, 0);
	    if (NULL != diP)
		{
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_BACKGROUND,
					 listboxGlobs.localColorsP[LCOLOR_BACKGROUND]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_FOREGROUND,
					 listboxGlobs.localColorsP[LCOLOR_LABELTEXT]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_TOPSHADOW,
					 listboxGlobs.localColorsP[LCOLOR_TOPSHADOW]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_BOTTOMSHADOW,
					 listboxGlobs.localColorsP[LCOLOR_BOTTOMSHADOW]);
		mdlDialog_itemSetColor (dmP->db, diP->itemIndex,
					 DITEM_COLORTYPE_SELECT,
					 listboxGlobs.localColorsP[LCOLOR_SELECT]);
		}

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_ListBox,
						LISTBOXID_ColoredList, 0);
	    if (NULL != diP)
		{
		long	    *infoFieldsP;
		StringList  *strListP;
	    	RawItemHdr  *rihP = diP->rawItemP;

	    	strListP = mdlDialog_listBoxGetStrListP (rihP);

		if (NULL != strListP)
		    {
		    mdlStringList_getMember (NULL, &infoFieldsP, strListP, 1);
		    *(infoFieldsP + 1) = (long) listboxGlobs.localColorsP[LCOLOR_BOTTOMSHADOW];

		    mdlStringList_getMember (NULL, &infoFieldsP, strListP, 2);
		    *(infoFieldsP + 1) = (long) listboxGlobs.localColorsP[LCOLOR_DGN];
		    }

		}
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		listcolr_colorsToggleBtnHook				|
|									|
| author	BSI     				10/92		|
|									|
+----------------------------------------------------------------------*/
Public void listcolr_colorsToggleBtnHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    listboxGlobs.colorToggle = 0;
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    DialogItem	*diP;

	    if (!dimP->u.stateChanged.reallyChanged)
		break;

	    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ListBox,
		    LISTBOXID_ColoredList, 0);
	    if (NULL != diP)
		{
		if (0 != listboxGlobs.colorToggle)
		    {
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_BACKGROUND,
					     listboxGlobs.localColorsP[LCOLOR_BACKGROUND]);
		    mdlDialog_itemDraw (dimP->db, diP->itemIndex);
		    }
		else
		    {
		    mdlDialog_itemSetColor (dimP->db, diP->itemIndex,
					     DITEM_COLORTYPE_BACKGROUND,
					     NULL);
		    mdlDialog_itemDraw (dimP->db, diP->itemIndex);
		    }
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          listcolr_coloredListBoxHook				|
|                                                                       |
| author        BSI					05/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Public void listcolr_coloredListBoxHook
(
DialogItemMessage   *dimP
)
    {

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int		 iString;
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP;
	    char	 formatStr[20];

	    /*
	    //	NOTE: nInfoFields is 2 so 2nd infoField can be used
	    //	to store a ptr to a BSIColorDesc 
	    */
	    strListP = mdlStringList_create (15, 2);
	    if (NULL == strListP)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    mdlResource_loadFromStringList (formatStr, NULL,
					    MESSAGELISTID_ListBox,
					    MSGID_Line);
	    for (iString=0; iString<15; iString++)
		{
		char    buffer[80];

		sprintf (buffer, formatStr, iString);
		mdlStringList_setMember (strListP, iString, buffer, NULL);
		}

	    mdlDialog_listBoxSetStrListP (rihP, strListP, 1);
	    break;
	    }

	case DITEM_MESSAGE_REDIRECT:
	    {

	    /*-----------------------------------------------------------
	    To make global accelerator keys available outside of the command
	    or keyin window, the Dialog Manager sometimes redirects keystrokes  
	    to the command window.  Before doing so, it gives the item and
	    its hook function an opportunity to prohibit the redirect.
	    The redirect will occur if the dialog box in focus does not
	    have a menu bar.
	    In this case, we stop the redirection so the DELETE key can
	    be handled in the list box hook function.
	    -----------------------------------------------------------*/
	    if (dimP->u.keystrokeRedirect.keystroke == VKEY_DELETE)
	    	{
	    	RawItemHdr *listP = dimP->dialogItemP->rawItemP;
	    	
		/* delete the selected row */
	    	if (SUCCESS == listcolr_deleteCurrentSelection (listP))
		    {
		    /* tell the dialog manager that we handled the event */
		    dimP->u.keystrokeRedirect.hookHandled = TRUE;

		    /* tell the dialog manager not to redirect the keystroke */
		    dimP->u.keystrokeRedirect.redirectAllowed = FALSE;
		    }
	    	}

	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP;

	    strListP = mdlDialog_listBoxGetStrListP (rihP);
	    if (NULL != strListP)
		{
		mdlStringList_destroy (strListP);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

