/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listcolr.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   listcolr.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:09 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Colored list box Dialog Example Resources			|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "listbox.h"	/* dialog box example constants & structs */
#include "listbtxt.h"	/* static text definitions */

/*----------------------------------------------------------------------+
|									|
|   Colored Items Example Dialog					|
|									|
+----------------------------------------------------------------------*/
#define XSIZE       	(35*XC)
#define	YSIZE		(11*YC)

#define X1	    	(3*XC)

DialogBoxRsc  DIALOGID_ColorList =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    XSIZE, YSIZE, 
    NOHELP, MHELP, HOOKDIALOGID_Colors, NOPARENTID,
    TXT_ColoredItems,
{
{{X1,GENY(1.5),0,0},ToggleButton,   TOGGLEBTNID_Colored, ON, 0, "", ""},
{{X1,GENY(3),0,0},  ListBox,	    LISTBOXID_ColoredList,    ON, 0, "", ""},


}
    };

/*----------------------------------------------------------------------+
|									|
|   Item Resource Specifications					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   ToggleButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEBTNID_Colored =
    {
    NOCMD, LCMD, NOSYNONYM, 
    NOHELP, LHELPTOPIC, 
    HOOKITEMID_ToggleBtnColor, NOARG, 0x1, NOINVERT,
    TXT_ColoredToggleLabel, 
    "listboxGlobs.colorToggle"
    };

/*----------------------------------------------------------------------+
|									|
|   ListBox Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_ColoredList =
    {
    NOHELP, MHELP, 
    HOOKITEMID_ColoredListBox, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_COLOREDROWS | LISTATTR_SELBROWSE, 
    6, 0, "",
	{
	{24*XC, 60, 0, ""},
	}
    };
