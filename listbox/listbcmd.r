/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listbcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listbcmd.r_v  $
|   $Workfile:   listbcmd.r  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:38:53 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	List box dialog item example application		    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

#include "listbox.h"

/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE		 0
#define CT_MAIN		 1
#define CT_LISTBOX	 2
#define CT_OPEN		 3

/*----------------------------------------------------------------------+
|                                                                       |
| 	Listbox commands						|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_MAIN =
{
    {  1, CT_LISTBOX, INPUT, HID,		"LISTBOX" },
};

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
Table	CT_LISTBOX =
{
    {  1, CT_OPEN,	INHERIT, NONE,		"OPEN" },
};

/*----------------------------------------------------------------------+
|									|
|   									|
|									|
+----------------------------------------------------------------------*/
Table	CT_OPEN =
{
    {  DIALOGID_ListIcon,	CT_NONE,    INHERIT, NONE,    "LISTICON" },
    {  DIALOGID_MultiList,	CT_NONE,    INHERIT, NONE,    "MULTILIST" },
    {  DIALOGID_SashList,	CT_NONE,    INHERIT, NONE,    "SASH" },
    {  DIALOGID_ColorList,	CT_NONE,    INHERIT, NONE,    "COLORS" },
};


/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_LISTBOX        1

DllMdlApp DLLAPP_LISTBOX =
    {
    "LISTBOX", "listbox"          // taskid, dllName
    }
