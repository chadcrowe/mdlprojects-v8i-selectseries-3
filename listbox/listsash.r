/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/listbox/listsash.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/listbox/listsash.r_v  $
|   $Workfile:   listsash.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:18 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Newitems Dialog Example Resources				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */

#include "listbox.h"	/* newitems dialog box example constants & structs */
#include "listbtxt.h"	/* newitems static text definitions */

/*----------------------------------------------------------------------+
|									|
|   Sash Example Dialog							|
|									|
+----------------------------------------------------------------------*/
#define X1	    (2*XC)
#define Y1	    (GENY(1))
#define Y2	    (GENY(5))
#define Y3	    (GENY(6))
#define XSIZE       (30*XC)

DialogBoxRsc  DIALOGID_SashList =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_GROWABLE,
    XSIZE, 20*YC, 
    NOHELP, MHELP, HOOKDIALOGID_DialogSash, NOPARENTID,
    TXT_Sash,
{
{{X1,Y1,0,0},	ListBox,    LISTBOXID_Sash1,    ON, 0, "", ""},
{{ 0,Y2,0,0},	Sash,	    SASHID_Example, 	ON, 0,"",""},
{{X1,Y3,0,0},	ListBox,    LISTBOXID_Sash2,    ON, 0, "", ""},
}
    };

   
/*----------------------------------------------------------------------+
|									|
|   ListBox Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_Sash1 =
    {
    NOHELP, MHELP, 
    HOOKITEMID_ListBoxSash, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_SELSINGLE, 
    6, 0, "",
	{
	{24*XC, 60, 0, ""},
	}
    };

DItem_ListBoxRsc LISTBOXID_Sash2 =
    {
    NOHELP, MHELP, 
    HOOKITEMID_ListBoxSash, NOARG, 
    0, 
    6, 0, "",
	{
	{24*XC, 60, 0, ""},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   Sash Item Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_SashRsc	SASHID_Example =
    {
    NOHELP, MHELP, HOOKITEMID_Sash, NOARG, 5*YC, 5*YC, 0
    }

