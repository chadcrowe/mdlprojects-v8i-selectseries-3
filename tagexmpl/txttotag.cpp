/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/txttotag.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   txttotag.mc  $
|   $Revision: 1.2.16.1 $
|   	$Date: 2013/07/01 20:40:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   txttotag.mc								|
|									|
|   This application allows the user to idetify a text element that is	|
|   to be transformed into tag data, and attached to an identified  	|
|   graphic element.						    	|
|								    	|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <mdl.h>	/* MDL Library funcs structures & constants */
#include <dlogitem.h>	/* Dialog Box Manager structures & constants */
#include <mselems.h>	/* structures that define MicroStation elements */
#include <rscdefs.h>	/* resource mgr structure definitions & constants */
#include <mdlio.h>	/* File I/O definitions and data structs for MDL */
#include <userfnc.h>
#include <dlogids.h>
#include <cmdlist.h>
#include <tcb.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <msmbstr.fdf>

#include <dlogman.fdf>  /* Dialog Box Manager Function Prototypes */
#include <mstagdat.fdf> /* Tag data function prototypes           */
#include <msassoc.fdf>
#include <mselemen.fdf>
#include <mselmdsc.fdf>
#include <msview.fdf>
#include <msstate.fdf>
#include <mslocate.fdf>
#include <msoutput.fdf>
#include <msvar.fdf>
#include <msmisc.fdf>

#include "tagexmpl.h"	/* Contains Dialog Box IDs for this app */
#include "tagexcmd.h"   /* Contains Command numbers for this app */

/*----------------------------------------------------------------------+
|                                                                       |
|   Local Functions                                                     |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Private Global variables                                            |
|                                                                       |
+----------------------------------------------------------------------*/
Private char	    gStringVal[TAG_MAX_DATA_BYTES+1]; /* text elt's string*/
Private ULong       gGraphicPos;    /* file position of graphic element*/
Private TagDef      *gTagDef;       /* Tag definition for user selected tag*/
Private TagSpec     gTagSpec;       /* tag specification */
Private MSElement   gGraphicElm;    /* graphic element to attach to tag*/

/*----------------------------------------------------------------------+
|                                                                       |
|   Local Functions                                                     |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_isTagAttached					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
|   Determines whether a given tag is attached to the currently.	|
|   selected graphic element.					    	|
|   Returns TRUE is tag is already attached.			    	|
|								    	|
+----------------------------------------------------------------------*/
Private BoolInt txttotag_isTagAttached
(
MSWChar	    *tagName,   /* => tag name to check for	    */
MSElement   *elP    	/* => element to check for tag name */
)
    {
    int		    numTags, status;
    BoolInt	    returnStatus = FALSE;
    MSElementDescr  *elmDescrP, *tempP;

    /* Extract all tag elements from the selected element */
    if ((status = mdlTag_getElementTags(
			    &elmDescrP, /* <= linked list of tags   */
			    &numTags, 	/* <= number of tags	    */
			    elP,    	/* => element to get tags for*/
			    MASTERFILE, /* => file number	    */
			    0))     	/* => future arg, always 0  */
			!= SUCCESS)
	{
	mdlOutput_rscPrintf(MSG_ERROR, NULL, MESSAGELISTID_Messages, 
		MESSAGEID_GetTagsErr);
	}
    else
	{
	tempP = elmDescrP;
	while (tempP != NULL)
	    {
	    TagSpec tagSpec;

	    status = mdlTag_extract(
		    	NULL,       	/* <= origin of associated element */
		    	NULL,       	/* <= snap point */
		    	&tagSpec,       /* <= tag specification */
		    	NULL,       	/* <= TRUE for displayable tag */
		    	NULL,       	/* <= value for the tag */
		    	NULL,       	/* <= target element's assocation id */
		    	NULL,       	/* <= offset */
			NULL,		/* <= size for the text */
			NULL,       	/* <= rotation matrix */
			NULL,       	/* <= text parameters */
			&tempP->el,	/* => tag element */
			MASTERFILE);	/* => file number for the tag */
	    if (SUCCESS == status)
	    	{
    	
		/* if target tag name and current tag name are equal */
	    	if (0 == mdlwcscmpi (tagName, tagSpec.tagName))
		    {
		    returnStatus = TRUE;
		    break;
		    }

	    	}

	    tempP = tempP->h.next;
	    }

	/*******************************************************************
	// After successfully calling mdlTag_getElementTags(), MicroStation
	// has allocated memory for a linked list of element descriptors 
	// and has returned a pointer to that list in the first parameter
	// to this function.
	//
	// You need to call mdlElmdscr_freeAll() to deallocate this memory.
	*******************************************************************/
	mdlElmdscr_freeAll(&elmDescrP);
	}

    return  returnStatus;
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_isIntString					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
|   Determines whether a string can be validly converted to an integer.	|
|	Used by the locate filter to ensure the user is locating a text	|
|   element that will be able to be converted to integer data, if indeed|
|   the tag type currently being attached is type "integer".	    	|
|								    	|
+----------------------------------------------------------------------*/
Private BoolInt txttotag_isIntString
(
char *s		    /* => input string to check */
)
    {
    while ('\0' != *s)
    	{
	if (FALSE == isdigit(*s))
	    {
	    return FALSE;
	    }
	    s++;
    	}

    return TRUE;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_isFloatString					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
|   Determines whether a string can be validly converted to a float.	|
|	Used by the locate filter to ensure that the user is locating a |
|   text element that will be able to be converted to float data, if 	|
|   indeed the tag type currently being attached is type "float".   	|
|								    	|
+----------------------------------------------------------------------*/
Private BoolInt txttotag_isFloatString
(
char *s		    /* => input string to check */
)
    {
    int eCount;
    int dotCount;

    eCount = 0;
    dotCount = 0;

    while ('\0' != *s)
	{
	switch(*s)
	    {
	    /* allow for scientific notation */
	    case 'e':
	    case 'E':
	    	{
	    	if (++eCount > 1)
		    {
		    return(FALSE);
		    }
		break;
	    	}
			
	    case '.':
	    	{
	    	
		if (++dotCount > 1)
		    {
		    return(FALSE);
		    }
		break;
	    	}

	    case '+':
	    case '-':
	    	{
		break;
	    	}

	    default:
	    	{
	    	if (TRUE != isdigit(*s))
		    {
		    return(FALSE);
		    }
		break;
	    	}

	    }
	    s++;
	}

	return(TRUE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_acceptText					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
|   The accept function for the text element that is to be changed into |
|   tag data.                                                           |
+----------------------------------------------------------------------*/
Private void txttotag_acceptText
(
Dpoint3d    *point, 	/* => data point entered by user */
int	    view    	/* => view point was in */
)
    {
    int status;     	/* function return code */
    int textJust;     	/* text justification */
    ULong textPos;  	/* file position of the selected text element */
    ElementId tagAssocId;	/* association id on the graphic element */
    UShort tagProps;	/* properties for the new tag element */
    MSElement textElm;	/* text element selected by the user */
    MSElement tagElm;	/* new tag element */
    Dpoint3d textOrigin;/* origin of input text element */
    Dpoint3d tagOffset; /* offset from graphic element to tag element */
    Dpoint3d assocOrigin;/* origin of tag element */
    Dpoint3d userOrigin;    /* text element snap point */
    RotMatrix textRotation; /* rotation matrix for text element */
    MSTextSize	tileSize;   /* tile size for text element */
    TextStyleInfo textStyle;/* font and style from text element */
    TextSizeParam tagSizeParam; /* size for tag's text */
    TextParamWide tagParams; 	/* text parameters for tag element */

    /* get the file position of the text element */
    textPos = mdlElement_getFilePos(FILEPOS_CURRENT, NULL);

    /* read the text element from the design file */
    mdlElement_read(&textElm, MASTERFILE, textPos);

    /* 
    //	extract the origin of the text element 
    //	The text origin is the coordinate of the lower-left corner.
    */
    mdlText_extract (
		    &textOrigin,    	/* <= origin of the text element */
		    &userOrigin,    	/* <= snap point */
		    NULL, 
		    NULL, 
		    NULL, 
		    &textRotation,  	/* <= rotation matrix */
		    &textStyle,     	/* <= font and style */
		    &textJust,      	/* <= text justification */
		    &tileSize,      	/* <= tile size */
		    NULL,	    	/* <= total text size */
		    &textElm);      	/* => text element */

    /* 
    //	If the graphic element does not have an association id then
    //	add an id with mdlAssoc_tagElement
    */
    if (mdlAssoc_isTagged (&tagAssocId, &gGraphicElm) != TRUE)
    	{
	//mdlAssoc_tagElement (&tagAssocId, gGraphicPos, MASTERFILE);
	tagAssocId = mdlElement_getID (&gGraphicElm);
	}

    /* set properties to display the tag */
    tagProps = ~TAG_PROP_DISPOFF;

    /* 
    //	The displayed tag data is to end up located exactly where the
    //	text was located.  mdlTag_create requires the offset of
    //	of the displayed tag data to be with respect to the tagged graphic.
    //	The point used for making the element association is dependent on the
    //	element type, thus the following is done:
    //
    //	1) Create the tag element, supplying NULL for the offset (no offset).
    //
    //	2) Interrogate the created tag element using mdlTag_extract(), determining 
    //	the origin of the base element to which the tag is associated.
    //
    //	3) Compute the difference between the origin of the text element
    //	and the origin of the graphic element.  This is the offset distance
    //	from the graphic element to its associated tag element.
    //
    //	4) Alter the tag element's location using mdlTag_create (input
    //	element == output element) and specifying the computed difference
    //	for the offset.
    */
    memset(&tagSizeParam, 0, sizeof(tagSizeParam));
    tagSizeParam.mode       	=   TXT_BY_TILE_SIZE;
    tagSizeParam.size.width	=   tileSize.width;
    tagSizeParam.size.height	=   tileSize.height;

    memset(&tagParams, 0, sizeof(tagParams));
    tagParams.font	    	= textStyle.font;
    tagParams.just	    	= textJust;
    tagParams.style	    	= textStyle.style;
    tagParams.viewIndependent	= 0;
    //tagParams.slant	    	= tcb->textStyle.slant; /* valid if flags.slant = 1 */
    mdlParams_getActive (&tagParams.slant, ACTIVEPARAM_TEXTSLANT);
    tagParams.characterSpacing	= 0; /* if fixedWidth/interCharSpacing */
    tagParams.underlineSpacing	= 0; /* valid if flags.underline */
    tagParams.flags.slant	= 1;
    
    gTagSpec.set.modelRef = MASTERFILE;

    status = mdlTag_create(
			&tagElm,	/* <= tag element created */
			NULL,		/* => template element */
			&gTagSpec,	/* => specification for tag definition */
			&tagProps,	/* => properties of the tag (see tagdata.h) */
			&(gTagDef->value),/* => value extracted from text element */
			&tagAssocId,	/* => tag id from mdlAssoc_tagElement */
			NULL,       	/* => origin, offset, or NULL */
			&tagSizeParam,	/* => size for text or NULL*/
			&textRotation,	/* => rotation matrix or NULL*/
			&tagParams,     /* => text parameters or NULL*/
			NULL /* => text style name (Not used - always pass NULL) */
			);
    if (SUCCESS != status)
    	{
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Messages, 
			     MESSAGEID_TagCreateErr);
    	return;
    	}

    status = mdlTag_extract(
		    	&assocOrigin,	/* <= origin of associated element */
		    	NULL,       	/* <= snap point */
		    	NULL,       	/* <= tag specification */
		    	NULL,       	/* <= TRUE for displayable tag */
		    	NULL,       	/* <= value for the tag */
		    	NULL,       	/* <= target element's assocation id */
		    	NULL,       	/* <= offset */
			NULL,		/* <= size for the text */
			NULL,       	/* <= rotation matrix */
			NULL,       	/* <= text parameters */
			&tagElm,	/* => tag element */
			MASTERFILE);	/* => file number for the tag */
    if (SUCCESS != status)
    	{
	mdlOutput_rscPrintf(MSG_ERROR, NULL, MESSAGELISTID_Messages, 
		MESSAGEID_TagExtractErr);
    	return;    	
    	}

    /* calculate offset from graphic element to text element */
    /* tag offset = tag origin (text origin) - associated element origin */
    tagOffset.x = userOrigin.x - assocOrigin.x;
    tagOffset.y = userOrigin.y - assocOrigin.y;
	
    status = mdlTag_create(
			&tagElm,/* <= tag element created */
			&tagElm,/* => template element */
			NULL,	/* => specification for tag definition */
			NULL,	/* => properties of the tag (see tagdata.h) */
			NULL,	/* => value extracted from text element */
			NULL,	/* => tag id from mdlAssoc_tagElement */
			&tagOffset, /* => computed location offset */
			NULL,	/* => size for text or NULL*/
			NULL,	/* => rotation matrix or NULL*/
			NULL,   /* => text parameters or NULL*/
			NULL /* => text style name (Not used - always pass NULL) */
			);
    if (SUCCESS != status)
    	{
	mdlOutput_rscPrintf(MSG_ERROR, NULL, MESSAGELISTID_Messages, 
		MESSAGEID_TagCreateErr);
    	return;    	
    	}
	
    /* 
    //  Since the tag data is displayed in the exact same place, delete the
    //  text element
    */
    mdlElement_undoableDelete(&textElm, textPos, TRUE);

    /* write the tag element to design file */
    mdlElement_append(&tagElm);

    /* display the new tag element */
    mdlElement_display(&tagElm, NORMALDRAW);

    /* start element selection command */
    mdlState_startDefaultCommand();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_elmFilter					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
|   The element filter for locating the text element.		    	|
|                                                                       |
+----------------------------------------------------------------------*/
Private int txttotag_elmFilter
(
int	    	preLocate,  	/* => TRUE for prelocate	    */
MSElementUnion 	*elementP,  	/* => element being examined	    */
DgnModelRefP	fileNumber, 	/* => design or reference file      */
unsigned long 	filePosition,	/* => element file position	    */
Point3d     	*pointP,    	/* => point entered by the user     */
int	    	viewNumber  	/* => view containing the point     */
)
    {
    int locateStatus = LOCATE_ELEMENT_ACCEPT;
    int	status;

    /* get the string from the text element */
    status = mdlText_extractString(gStringVal, elementP);
    if (SUCCESS != status)
    	{
    	return LOCATE_ELEMENT_REJECT;
    	}

    /* 
    //	Based upon the supplied tagsetname and tagdefname, get the
    //	description of the tag definition
    */
    status = mdlTag_getTagDef(
	    &gTagDef,		    /* <= definition of the tag	    */
	    gTagSpec.set.setName,   /* => set name containing the tag */
	    gTagSpec.tagName, 	    /* => tag name to get definition for */
	    0,
	    MASTERFILE); 	    /* => file number to search 		    
    if (SUCCESS != status)
    	{
	return LOCATE_ELEMENT_REJECT;
    	}


    /*
    //	Based upon the type of the tag definition selected, make the
    //	value assignment
    */
    switch(gTagDef->value.type)
    	{
	case MS_TAGTYPE_CHAR:
	    {
	    gTagDef->value.val.stringVal = gStringVal;
	    break;
	    }

	case MS_TAGTYPE_SINT:
	    {
	    if (TRUE != txttotag_isIntString(gStringVal))
		{
		mdlOutput_rscPrintf(MSG_ERROR, NULL, 
		    MESSAGELISTID_Messages, MESSAGEID_InvalidTextData);
		locateStatus = LOCATE_ELEMENT_REJECT;
		}
	    else
	    	gTagDef->value.val.shortVal = (short)atoi(gStringVal);
	    break;
	    
	    }

    	case MS_TAGTYPE_LINT:
	    {
	    if (TRUE != txttotag_isIntString(gStringVal))
		{
		mdlOutput_rscPrintf(MSG_ERROR, NULL, 
		    MESSAGELISTID_Messages, MESSAGEID_InvalidTextData);
		locateStatus = LOCATE_ELEMENT_REJECT;
		}
	    else
	    	gTagDef->value.val.longVal = atol(gStringVal);
	    break;
	    
	    }

	case MS_TAGTYPE_DOUBLE:
	    {
	    if (TRUE != txttotag_isFloatString(gStringVal))
		{
		mdlOutput_rscPrintf(MSG_ERROR, NULL, 
		    MESSAGELISTID_Messages, MESSAGEID_InvalidTextData);
		locateStatus = LOCATE_ELEMENT_REJECT;
		}
	    else
	    	gTagDef->value.val.doubleVal = strtod(gStringVal, NULL);
	    break;
	    
	    }

	default:
	    {
	    mdlOutput_rscPrintf(MSG_ERROR, NULL, 
		    MESSAGELISTID_Messages, MESSAGEID_InvalidTextData);
	    locateStatus = LOCATE_ELEMENT_REJECT;
	    break;
	    }

	}

    return locateStatus;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_getText					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
|   Initiates the locate logic for the text element.		    	|
|                                                                       |
+----------------------------------------------------------------------*/
Private void txttotag_getText
(
void		    /* ignore the input arguments */
)
    {
    int elemTypes[1];

    /* Only allow the user to locate text elements */
    elemTypes[0] = TEXT_ELM;

    /* Clear search mask & allow locate of elements in MASTERFILE */
    mdlLocate_noElemNoLocked();

    /* Set search mask to locate text elements */
    mdlLocate_setElemSearchMask(1, elemTypes);


    /* Start modification command to locate and modify graphic element */
    mdlState_startModifyCommand (
			    txttotag_getText,       /*reset func */
			    txttotag_acceptText,    /* datapoint func */
			    NULL,		    /* dynamics func */
			    NULL,		    /* show func */
	                    NULL,		    /* clean func */
			    MESSAGEID_TextToTag,    /* command field message */
			    0,			    /* accept prompt message */
			    FALSE,		    /* use selection sets */
			    0);			    /* pts required for accept*/

    /* set the filter function */
    mdlLocate_setFunction(LOCATE_POSTLOCATE, txttotag_elmFilter);

    /* output a prompt asking user to select a text element */
    mdlOutput_rscPrintf(MSG_PROMPT, NULL, 
	    	MESSAGELISTID_Messages, MESSAGEID_TextElementSelect);

    /* Start search at beginning of file */
    mdlLocate_init();
    }	
	
/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_acceptGraphic					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
|   The accept function for the graphic element to be tagged	    	|
|                                                                       |
+----------------------------------------------------------------------*/
Private void txttotag_acceptGraphic
(
Dpoint3d    *pt,    /* => data point entered by user */
int	    view    /* => view point was in */
)
    {
    int     	lastAction;
    BoolInt 	statusFlag;
    
    /* store the file position of the graphic element */
    gGraphicPos = mdlElement_getFilePos(FILEPOS_CURRENT, NULL);

    /* read the graphic element from the design file */
    mdlElement_read(&gGraphicElm, MASTERFILE, gGraphicPos);

    /* Open the "Tags" modal dialog to select a tag within the set*/
    statusFlag = mdlDialog_openModal(&lastAction, NULL, DIALOGID_Tags);
    if (TRUE != statusFlag)
    	{
    	if (ACTIONBUTTON_OK == lastAction)
	    {
	    /* user hit the OK button, continue processing */

	    /* Get the text element to convert to a tag */
	    txttotag_getText();
	    }
    	else
	    {
	    /* User hit the cancel button, restart the command */
	    mdlState_restartCurrentCommand();    	
	    }
    	}

    }

/*----------------------------------------------------------------------+
|									|
| name          txttotag_placeTagFromText                               |
|									|
| author	BSI     				4/94		|
|									|
|   Begins the locate for the graphic element.			    	|
|									|
+----------------------------------------------------------------------*/
extern "C" void txttotag_placeTagFromText
(
char	*unparsedP	/* => unparsed part of command */
)
//cmdNumber   CMD_PLACE_TAG_FROMTEXT
    {

    /* locate only unlocked elements from the master file */
    mdlLocate_normal();

    /* Start modification command to locate and modify graphic element */
    mdlState_startModifyCommand (
			    txttotag_placeTagFromText,	/*reset func */
			    txttotag_acceptGraphic,  	/* datapoint func */
			    NULL,		    	/* dynamics func */
			    NULL,		    	/* show func */
	                    NULL,		    	/* clean func */
			    MESSAGEID_TextToTag,   	/* command field message */
			    0,			    	/* accept prompt message */
			    FALSE,		    	/* use selection sets */
			    0);			    	/* pts required for accept*/

    /* output a prompt asking user to select a graphic element */
    mdlOutput_rscPrintf(MSG_PROMPT, NULL, 
	    	MESSAGELISTID_Messages, MESSAGEID_GraphicElementSelect);

    /* Start search at beginning of file */
    mdlLocate_init();
    }	

/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_initTagSetList					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Private int	txttotag_initTagSetList
(
StringList	**strListP  	/* <=> string list to populate */
)
    {
    int		errorStatus, numSets;
    
    errorStatus = mdlTag_getSetNames (
		    strListP,		    /* <= string list of set names */
		    &numSets,		    /* <= number of sets found */
		    MASTERFILE,		    /* => file number to search in */
		    0);			    /* => for future use, always 0 */

    if ((SUCCESS != errorStatus) || (NULL == *strListP))
	{

	/* return an empty string list so the list box is properly init'd */
	*strListP = mdlStringList_create (0,1);
	if (NULL == *strListP)
	    errorStatus = ERROR;
	}

    return (errorStatus);	
    
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_getSelectedTagSet				|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Private int	txttotag_getSelectedTagSet
(
MSWChar	*setName  	/* <= currently selected tag set name */
)
    {
    int		errorStatus = ERROR;
    int		rowIndex, colIndex;
    BoolInt	found = FALSE;
    DialogBox	*dbP;
    DialogItem	*diP;
    StringList	*strListP;

    /* Get pointer to tool settings dialog box */
    dbP = mdlDialog_find( DIALOGID_ToolSettings, NULL);
    if (NULL == dbP)
	return	ERROR;

    diP = mdlDialog_itemGetByTypeAndId( 
			    	dbP,	/* dialog box pointer */
    				RTYPE_ListBox,
    				LISTBOXID_TagSets,
    				0 );	/* Starting index */
    if (NULL != diP)
    	{
	strListP = mdlDialog_listBoxGetStrListP(diP->rawItemP);
	if (NULL != strListP)
	    {
	    /* initialize the row and column indices */
	    rowIndex = -1;
	    colIndex = -1;

	    /* Find the selected row */
	    mdlDialog_listBoxGetNextSelection(&found, &rowIndex,
    					     &colIndex, diP->rawItemP);
	    if (TRUE == found)
		{
		char 	*string;

		/* get the name of the tag set from the string list */
		errorStatus = mdlStringList_getMember(&string, NULL, strListP, rowIndex);
		if (SUCCESS == errorStatus)
		    {
		    mbstowcs (setName,string,TAG_NAME_MAX+1);
		    }
		}
	    }
    	}

    return (errorStatus);	
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		txttotag_initTagsList					|
|                                                                       |
| author	BSI				    05/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Private int	txttotag_initTagsList
(
StringList	**strListP  	/* <=> string list to populate */
)
    {
    int		errorStatus;
    int     	numTags;
    TagDef  	*tagDefsP;

    /* Get the currently selected tag set name from tool settings */
    errorStatus = txttotag_getSelectedTagSet(gTagSpec.set.setName);
    if (SUCCESS != errorStatus)
    	{
    	return	errorStatus;
    	}

    /* 
    //	If an error occurs when populating the string list,
    //	then an empty string list is returned so the list box is properly init'd 
    */
    *strListP = mdlStringList_create (0,1);
    if (NULL == *strListP)
	return ERROR;

    /* Retrieve an array of tag definitions for a given set*/
    errorStatus = mdlTag_getSetDef(
		    &tagDefsP,		    /* <= array of tag definitions */
		    &numTags,		    /* <= # of tags in the set */
		    gTagSpec.set.reportName,/* <= report file name for set */
	            gTagSpec.set.setName,   /* => set to get definition for */
		    0,		    /*  => ID of owner of set		    */
		    MASTERFILE);		    /* => file number to search in */
    if (SUCCESS == errorStatus)
	{
	int i;
	long newIndex;
	char    name[TAG_NAME_MAX_BYTES+1];
	for (i = 0; i < numTags; i++)
	    {
	    /* insert a new row into the string list */
	    errorStatus = mdlStringList_insertMember(&newIndex, *strListP, -1, 1);
	    if (SUCCESS != errorStatus)
	    	{
	    	break;
	    	}
	    wcstombs (name,tagDefsP[i].name,TAG_NAME_MAX+1);
	    /* set the new row's contents to the name of the next tag definition*/
	    mdlStringList_setMember(*strListP, newIndex, name, NULL);
	    }
	}

    /* 
    // Free the tag def array and any dynamically allocated contents 
    // mdlTag_freeTagDefArray is void.  It returns no value.
    */
    mdlTag_freeTagDefArray (
	    	tagDefsP,   /* <=> tag definition array */
	    	numTags);   /* => number of tags in the array */

    return (errorStatus);	
    
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          txttotag_tagsetListHook				    	|
|                                                                       |
| author        BSI					05/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Public void txttotag_tagsetListHook
(
DialogItemMessage   *dimP    /* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int     	status;
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP = NULL;

	    status = txttotag_initTagSetList (&strListP);
	    if ((NULL == strListP) || (SUCCESS != status))
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    /* associate the string list to the list box */
	    mdlDialog_listBoxSetStrListP (rihP, strListP, 1);
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP = mdlDialog_listBoxGetStrListP(rihP);

	    if (NULL != strListP)
		{
		mdlStringList_destroy (strListP);
		}
	    break;
	    }

	default:
	    {
	    dimP->msgUnderstood = FALSE;
	    break;
	    }
	}
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          txttotag_tagListHook				    	|
|                                                                       |
| author        BSI					05/95		|
|                                                                       |
+----------------------------------------------------------------------*/
Public void txttotag_tagListHook
(
DialogItemMessage   *dimP    /* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int     	status;
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP = NULL;

	    status = txttotag_initTagsList (&strListP);
	    if ((NULL == strListP) || (SUCCESS != status))
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    /* associate the string list to the list box */
	    mdlDialog_listBoxSetStrListP (rihP, strListP, 1);
	    break;
	    }

	case DITEM_MESSAGE_BUTTON:
	    {
	    DialogItem	*diP;

	    /* If have a double-click then activate the OK */
            if (BUTTONTRANS_UP == dimP->u.button.buttonTrans)
                {
                if ((2 == dimP->u.button.upNumber) && (TRUE == dimP->u.button.clicked))
                    {
                    if (NULL != (diP = mdlDialog_itemGetByTypeAndId
                                (dimP->db, RTYPE_PushButton,
                                PUSHBUTTONID_OK, 0)))
			{
                    	mdlDialog_pushButtonActivate (diP->rawItemP);
			}
                    }
                }

	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*rihP = diP->rawItemP;
	    StringList	*strListP = mdlDialog_listBoxGetStrListP (rihP);

	    if (NULL != strListP)
		{
		mdlStringList_destroy (strListP);
		}
	    break;
	    }

	default:
	    {
	    dimP->msgUnderstood = FALSE;
	    break;
	    }
	}
    }


/*----------------------------------------------------------------------+
|									|
| name		txttotag_dialogHook				    	|
|									|
| author	BSI				10/93			|
|									|
+----------------------------------------------------------------------*/
Public void	txttotag_dialogHook
(
DialogMessage   *dmP
)
    {						
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_ACTIONBUTTON:
	    {
	    int		row	    = -1;
	    int		column	    = -1;
	    int     	errorStatus;
	    char	*stringP    = NULL;
	    BoolInt     found	    = FALSE;
	    DialogItem  *diP	    = NULL;
	    StringList  *strListP   = NULL;

	    /* if the user pressed the cancel button then break */
	    if (ACTIONBUTTON_OK != dmP->u.actionButton.actionType)
		break;

	    /* get a pointer to the tag names list box */
	    if (NULL == (diP = mdlDialog_itemGetByTypeAndId
			    (dmP->db, RTYPE_ListBox,
			    LISTBOXID_Tags, 0)))
	        break;

	    /* get the currently selected row index */
	    mdlDialog_listBoxGetNextSelection
	        (&found, &row, &column, diP->rawItemP);

	    /* if no selection was found then break */
	    if (TRUE != found)
		break;

	    /* get the pointer to the string list associated with the list box */
	    if (NULL == (strListP = mdlDialog_listBoxGetStrListP (diP->rawItemP)))
		break;

	    /* get the tag name using the row index */
	    errorStatus = mdlStringList_getMember (&stringP, NULL, strListP, row);
	    if (SUCCESS == errorStatus)
		{
		MSWChar wString[TAG_NAME_MAX+1];
		BoolInt flag;
		//mbstowcs (wString,stringP,strlen (stringP));
                mdlCnv_converMultibyteToUnicode (stringP,-1,wString,200);

	    	/* check if tag is already attached to the graphic element */
		flag = txttotag_isTagAttached(wString, &gGraphicElm);
		if (TRUE == flag)
		    {
		    mdlOutput_error ("Tag is already attached");
		    mdlOutput_rscPrintf(MSG_ERROR, NULL, 
			    MESSAGELISTID_Messages, MESSAGEID_TagAlreadyAttached);

		    dmP->u.actionButton.abortAction = TRUE;

		    }
		else
		    {
		    /* store tag name for later use */
		    wcscpy(gTagSpec.tagName, wString);
		    }
		}
	    else
		{
		memset (gTagSpec.tagName, 0, sizeof(gTagSpec.tagName));
		}

	    break;
	    }

	default:
	    {
	    dmP->msgUnderstood = FALSE;
	    break;
	    }
    	}
    }

