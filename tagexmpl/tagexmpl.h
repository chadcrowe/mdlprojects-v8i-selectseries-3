/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/tagexmpl.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   tagexmpl.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   tagexmpl.h								|
|									|
|	Constants & types used in tag data example		    	|
|									|
+----------------------------------------------------------------------*/

#if !defined (__tagexmplH__)
#define	    __tagexmplH__

/*----------------------------------------------------------------------+
|									|
|   Dialog ID's								|
|									|
+----------------------------------------------------------------------*/
#define DIALOGID_TagData	    	1
#define DIALOGID_Tags		    	2

/*----------------------------------------------------------------------+
|									|
|   Icon Cmd IDs 							|
|									|
+----------------------------------------------------------------------*/
#define ICONCMDID_ChangeTagDisplay	1
#define ICONCMDID_TextToTag	    	2

/*----------------------------------------------------------------------+
|									|
|   Dialog Hook ID's							|
|									|
+----------------------------------------------------------------------*/
#define	HOOKDIALOGID_Tags	    	1
#define HOOKITEMID_TagSetsList      	2
#define HOOKITEMID_TagsList	    	3

/*----------------------------------------------------------------------+
|									|
|   Toggle ID's								|
|									|
+----------------------------------------------------------------------*/
#define TOGGLEID_Display 		1

/*----------------------------------------------------------------------+
|									|
|   List box ID's							|
|									|
+----------------------------------------------------------------------*/
#define LISTBOXID_TagSets	    	1
#define LISTBOXID_Tags		    	2

/*----------------------------------------------------------------------+
|									|
|   Message list defines						|
|									|
+----------------------------------------------------------------------*/
#define	MESSAGELISTID_Messages		1
#define MESSAGELISTID_TagErrors     	2

/*----------------------------------------------------------------------+
|									|
|   Message List Entry Ids						|
|									|
+----------------------------------------------------------------------*/
#define	MESSAGEID_SelectElement     	1
#define	MESSAGEID_TagDisplay		2
#define	MESSAGEID_TagExtractErr     	3
#define	MESSAGEID_GetTagsErr		4
#define	MESSAGEID_TagCreateErr		5
#define MESSAGEID_TextToTag	    	6
#define MESSAGEID_GraphicElementSelect	7
#define MESSAGEID_TextElementSelect 	8
#define MESSAGEID_InvalidTextData   	9
#define MESSAGEID_TagAlreadyAttached	10
#define MESSAGEID_CmdTableLoadError	11
#define MESSAGEID_ToolBoxOpenError	12
#define MESSAGEID_PaletteOpenError	13
#define MESSAGEID_DescriptionPrompt	14
#define MESSAGEID_CodePrompt	    	15
#define MESSAGEID_MeasurePrompt     	16

/*----------------------------------------------------------------------+
|									|
|   Typedefs for Dialog access strings					|
|									|
+----------------------------------------------------------------------*/
typedef struct dlogboxinfo
{
    int	display;
} DlogBoxInfo;

#endif /* #if !defined (__tagexmplH__) */



