/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/txttotag.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   txttotag.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   txttotag.r -  Tag Example resource definitions			|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include <cmdlist.h>

#include "tagexcmd.h"
#include "tagexmpl.h"
#include "tagextxt.h"

/*----------------------------------------------------------------------+
|									|
|   Dialog Box Resource Specification					|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_Tags = 
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_MODAL,
    32 * XC + 3, 13 * YC + 1,
    NOHELP, MHELP, 
    HOOKDIALOGID_Tags, NOPARENTID, 
    TXT_TagsDialogTitle,
{
{{6*XC+3, 1*YC+11, 0, 6*YC+7}, ListBox, LISTBOXID_Tags, ON, 0, "", ""},
{{2*XC+5, 10*YC+5, BUTTON_STDWIDTH, 0}, PushButton, PUSHBUTTONID_OK, ON, 0, "", ""},
{{17*XC+2, 10*YC+5, BUTTON_STDWIDTH, 0}, PushButton, PUSHBUTTONID_Cancel, ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   ListBox Item Resource                                               |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_Tags =
    {
    NOHELP,
    LHELP,
    HOOKITEMID_TagsList,
    NOARG,
    0,
    5,
    0,
    "",
        {
        {96, 10, ALIGN_LEFT, TXT_Tags},
        }
    };
