/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/tagexmpl.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   tagexmpl.r  $
|   $Revision: 1.3.52.1 $
|   	$Date: 2013/07/01 20:40:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   tagexmpl.r -  Tag Example resource definitions			|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include <cmdlist.h>
#include <pselect.ids>

#include "tagexcmd.h"
#include "tagexmpl.h"
#include "tagextxt.h"

/*----------------------------------------------------------------------+
|									|
|   Dialog Box Resource Specification					|
|									|
+----------------------------------------------------------------------*/

#if defined (MSVERSION) && (MSVERSION >= 0x550)

/*----------------------------------------------------------------------+
|									|
|   Tool Box resources						    	|
|									|
|   A tool box is defined by creating a dialog box resource and tool 	|
|   box resource which have the same identifier.			|
|									|
|   The dialog box resource contains only the ToolBox item.		|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_TagData = 
    {
    DIALOGATTR_TOOLBOXCOMMON,
    0, 0,
    NOHELP, MHELP, 
    NOHOOK, NOPARENTID, 
    "",
{
{{ 0, 0, 0, 0}, ToolBox, DIALOGID_TagData, ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   The following sample toolbox contains 2 icon commands.  	 	|
|   Note that the auxilary info field (the last parameter) is NULL for	|
|   PowerDraft's or MicroStation's icon commands, but contains the  	|
|   string: "owner=\"TAGDISP\"" for the icon command which belongs to	|
|   our application.						    	|
|									|
+----------------------------------------------------------------------*/
DItem_ToolBoxRsc DIALOGID_TagData =
    {
    NOHELP, MHELPTOPIC, NOHOOK, NOARG, 0, TXT_TagToolBoxTitle,
{
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_ChooseElement, ON, 0, "", "owner=\"PSELECT\""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_ChangeTagDisplay, ON, 1, "", "owner=\"TAGEXMPL\""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_TextToTag, ON, 1, "", "owner=\"TAGEXMPL\""},
}
    };

#else
/*----------------------------------------------------------------------+
|									|
|   Icon Cmd Palette							|
|									|
+----------------------------------------------------------------------*/
DItem_IconCmdPaletteRsc DIALOGID_TagData =
    {
    3, 1, 0, NOHELP, MHELP, NOHOOK, NOARG, TXT_DialogTitle,
	{
 	ICONCMDID_ChooseElement,
	ICONCMDID_ChangeTagDisplay,
	ICONCMDID_TextToTag,
	}
    };
#endif /* if defined (MSVERSION) && (MSVERSION >= 0x550) */


/*----------------------------------------------------------------------+
|									|
|  Icon Cmd Resources                                                   |
|									|
+----------------------------------------------------------------------*/
DItem_IconCmdRsc ICONCMDID_ChangeTagDisplay =
    {
    NOHELP, OHELPTASKIDCMD, 0,
    CMD_CHANGE_TAG_DISPLAY, OTASKID, "", "",
	{
#   	if defined (MSVERSION) && (MSVERSION < 0x551)
        {{2*XC, GENY(1), 0, 0}, ToggleButton, TOGGLEID_Display, ON, 0, "", ""},
#   	endif
	}
#   if defined (MSVERSION) && (MSVERSION >= 0x550)
    }
    extendedAttributes
    {
    {
    /* Tool Description/Flyover Help appears in status area */
    {EXTATTR_FLYTEXT, TXT_Flyover_ChangeTagDisplay},     

    /* Tool Tip/Balloon Help appears in yellow text box near icon command */
    {EXTATTR_BALLOON, TXT_Balloon_ChangeTagDisplay},     
    }
    };
#   else
    };
#   endif /* if defined (MSVERSION) && (MSVERSION >= 0x550) */

#if defined (MSVERSION) && (MSVERSION >= 0x551)
CmdItemListRsc CMD_CHANGE_TAG_DISPLAY =
    {{
    {{2*XC, GENY(1), 0, 0}, ToggleButton, TOGGLEID_Display, ON, 0, "", ""},
    }};
#endif

DItem_IconCmdRsc ICONCMDID_TextToTag =
    {
    NOHELP, OHELPTASKIDCMD, 0,
    CMD_PLACE_TAG_FROMTEXT, OTASKID, "", "",
	{
#   	if defined (MSVERSION) && (MSVERSION < 0x551)
        {{2*XC, GENY(2), 0, 0}, ListBox, LISTBOXID_TagSets, ON, 0, "", ""},
#   	endif
	}
#   if defined (MSVERSION) && (MSVERSION >= 0x550)
    }
    extendedAttributes
    {
    {
    /* Tool Description/Flyover Help appears in status area */
    {EXTATTR_FLYTEXT, TXT_Flyover_TextToTag},     

    /* Tool Tip/Balloon Help appears in yellow text box near icon command */
    {EXTATTR_BALLOON, TXT_Balloon_TextToTag},     
    }
    };
#   else
    };
#   endif /* if defined (MSVERSION) && (MSVERSION >= 0x550) */

#if defined (MSVERSION) && (MSVERSION >= 0x551)
CmdItemListRsc CMD_PLACE_TAG_FROMTEXT =
    {{
    {{2*XC, GENY(2), 0, 0}, ListBox, LISTBOXID_TagSets, ON, 0, "", ""},
    }};
#endif
/*----------------------------------------------------------------------+
|									|
|   Toggle Buttons							|
|									|
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEID_Display =
    {
    NOCMD, MCMD, NOSYNONYM, NOHELP, MCMD, NOHOOK, NOARG,
    NOMASK, NOINVERT,
    TXT_DisplayTag,
    "dlogBoxInfo->display"
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   ListBox Item Resource                                               |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_TagSets =
    {
    NOHELP,
    LHELP,
    HOOKITEMID_TagSetsList,
    NOARG,
    0,
    3,
    0,
    "",
        {
        {96, 10, ALIGN_LEFT, TXT_TagSets},
        }
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Small Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdSmallRsc ICONCMDID_ChangeTagDisplay =
    {
    23,    23,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x80, 0x00, 0x01, 0x00, 0x02, 0x02,
        0x04, 0x02, 0x00, 0x10, 0x02, 0x3c, 0x40, 0x01,
        0xce, 0x00, 0x06, 0x06, 0x00, 0x08, 0x04, 0x01,
        0xd0, 0x0b, 0x80, 0x30, 0x30, 0x00, 0x30, 0xc0,
        0x00, 0x33, 0x00, 0x02, 0x24, 0x40, 0x08, 0x78,
        0x40, 0x20, 0x90, 0x40, 0x01, 0xe0, 0x00, 0x01,
        0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 
        }
    };

IconCmdSmallRsc ICONCMDID_TextToTag =
    {
    23,    23,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x3c,
        0x00, 0x10, 0x84, 0x00, 0xd2, 0x34, 0x02, 0x48,
        0x94, 0x09, 0x10, 0x24, 0x10, 0x20, 0x84, 0x20,
        0x42, 0x04, 0x40, 0x48, 0x10, 0x80, 0x5e, 0x41,
        0x00, 0x41, 0x02, 0x00, 0x44, 0x04, 0x00, 0x50,
        0x08, 0x40, 0x40, 0x08, 0x80, 0x00, 0x0b, 0x80,
        0x00, 0x05, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x22,
        0x00, 0x00, 0xc6, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 
        }
    };
/*----------------------------------------------------------------------+
|                                                                       |
|   Large Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdLargeRsc ICONCMDID_ChangeTagDisplay =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x40, 0x00, 0x00, 0x00, 0x80, 0x00, 0x01,
        0x01, 0x00, 0x80, 0x01, 0x00, 0x02, 0x00, 0x01,
        0x1f, 0x88, 0x00, 0x00, 0x61, 0x80, 0x00, 0x01,
        0x81, 0x80, 0x00, 0x02, 0x01, 0x00, 0x00, 0xf4,
        0x02, 0xe0, 0x00, 0x08, 0x04, 0x00, 0x00, 0x18,
        0x18, 0x00, 0x00, 0x18, 0x60, 0x00, 0x01, 0x19,
        0x88, 0x00, 0x04, 0x12, 0x08, 0x00, 0x10, 0x24,
        0x08, 0x00, 0x00, 0x78, 0x00, 0x00, 0x00, 0x90,
        0x00, 0x00, 0x01, 0x20, 0x00, 0x00, 0x01, 0x80,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };


IconCmdLargeRsc ICONCMDID_TextToTag =
    {
    31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "",
        {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x08, 0x0f, 0x80, 0x00,
        0x08, 0x20, 0x80, 0x00, 0x08, 0x80, 0x80, 0x01,
        0xfa, 0x00, 0x80, 0x04, 0x24, 0x0c, 0x80, 0x10,
        0x88, 0x24, 0x80, 0x22, 0x10, 0x48, 0x80, 0x40,
        0x20, 0x10, 0x80, 0x80, 0x20, 0x40, 0x81, 0x00,
        0x21, 0x00, 0x82, 0x00, 0x24, 0x02, 0x04, 0x00,
        0x2f, 0x08, 0x08, 0x00, 0x20, 0x20, 0x10, 0x00,
        0x20, 0x80, 0x20, 0x00, 0x22, 0x00, 0x40, 0x00,
        0x28, 0x00, 0x8c, 0x00, 0x20, 0x01, 0x18, 0x00,
        0x00, 0x02, 0x78, 0x00, 0x00, 0x02, 0x90, 0x00,
        0x00, 0x01, 0x20, 0x00, 0x00, 0x07, 0xe0, 0x00,
        0x00, 0x08, 0x40, 0x00, 0x00, 0x30, 0xc0, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
        }
    };


