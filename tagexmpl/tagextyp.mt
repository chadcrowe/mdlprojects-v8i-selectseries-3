/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/tagextyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   tagextyp.mt - Tag data Dialog Box published structures		|
|									|
+----------------------------------------------------------------------*/
#include "tagexmpl.h"

publishStructures (dlogboxinfo);
