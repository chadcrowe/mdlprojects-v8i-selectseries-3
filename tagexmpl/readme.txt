    	    	    	    TAGEXMPL
			    ========

Example MDL application which demonstrates various mdlTag_xxx functions through the tools:

1) Change tag display 	- changes the display flag in all tags attached to a selected graphic element
			  based on the value of the "Display Tags?" toggle button.

    Tag functions used:

    mdlTag_create()
    mdlTag_extract()
    mdlTag_getElementTags()

   

2) Text to tag      	- converts a selected text element to a tag element and associates the tag element
		    	  to a selected graphic element.  User may choose a tag set and a tag within the set

    Tag functions used:

    mdlTag_create()
    mdlTag_extract()
    mdlTag_freeTagDefArray()
    mdlTag_getElementTags()
    mdlTag_getSetNames()
    mdlTag_getSetDef()
    mdlTag_getTagDef()


3) Create test tag set command
    Keyin: CREATE TAGSET

    This command creates a tag set called test1 with the tag definitions: 
    	a) descript 	- character type
    	b) code     	- integer type
	c) measure  	- real type
 

    Tag functions used:

    mdlTag_createSetDef()