/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/tagdisp.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   tagdisp.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:40:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   tagdisp.mc - Tag display source code.                               |
|              Illustrates basic tag element manipulation concepts.     | 
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <mdl.h>	/* MDL Library funcs structures & constants */
#include <dlogitem.h>	/* Dialog Box Manager structures & constants */
#include <mselems.h>	/* structures that define MicroStation elements */
#include <rscdefs.h>	/* resource mgr structure definitions & constants */
#include <scanner.h>	/* typedefs and defines for design file scanner */
#include <mdlio.h>	/* File I/O definitions and data structs for MDL */
#include <stdlib.h>

#include <dlogman.fdf>  /* Dialog Box Manager Function Prototypes */
#include <mstagdat.fdf> /* Tag data function prototypes           */
#include <mselemen.fdf>
#include <mselmdsc.fdf>
#include <msmisc.fdf>
#include <msoutput.fdf>
#include <mslocate.fdf>
#include <msstate.fdf>
#include <msdgnobj.fdf>


#include "tagexmpl.h"	/* Contains Dialog Box IDs for this app */
#include "tagexcmd.h"   /* Contains Command numbers for this app */

/*----------------------------------------------------------------------+
|                                                                       |
|   Local Function Declarations                                         |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
/*-----------------------------------------------------------------------
    The following variable is referenced in C expression strings used
    by the toggle button item defined in tagdata.r.
-----------------------------------------------------------------------*/
extern DlogBoxInfo     *dlogBoxInfo;

/*----------------------------------------------------------------------+
|									|
|   Tagddisp Code Section  						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Functions for Locate logic  					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name          tagdisp_setElmSearchType		                |
|									|
| author	BSI     				8/94		|
|									|
+----------------------------------------------------------------------*/
Private void tagdisp_setElmSearchType 
(
void
)
    {
    //mdlLocate_normal ();
    mdlLocate_allowLocked  ();
    }

/*----------------------------------------------------------------------+
|									|
| name          tagdisp_changeDisplay				    	|
|									|
| author	BSI     				8/94		|
|									|
+----------------------------------------------------------------------*/
Private int tagdisp_changeDisplay
(
MSElementDescr *tagElmDescrP,
DgnModelRefP    modelRefP
)
    {
    UShort 	tagProps;
    int 	status = SUCCESS;
    MSElement 	newTagElm;
    TagValue 	value;
    BoolInt 	displayable, createTag;

    createTag = FALSE;

    /*
    //	Extract the tag specification, tag value, and displayable flag. 
    //	I have extracted the tag value because I want to check the data
    //	type of the tag to determine whether or not it should be 
    //	displayed.  For example, I don't want to display binary data.
    //
    //	If I did not extract the value here, I could have also called
    //	mdlTag_getTagDef() to get the tag's type from the tag definition.
    */
    status =  mdlTag_extract(
		    	NULL,       	/* <= origin of associated element */
		    	NULL,       	/* <= snap point */
		    	NULL,       	/* <= tag specification */
		    	&displayable,   /* <= TRUE for displayable tag */
		    	&value,       	/* <= value for the tag */
		    	NULL,       	/* <= target element's assocation id */
		    	NULL,       	/* <= offset */
			NULL,		/* <= size for the text */
			NULL,       	/* <= rotation matrix */
			NULL,       	/* <= text parameters */
			&tagElmDescrP->el, /* => tag element */
			modelRefP);/* => file number for the tag */

	if (SUCCESS != status )	
	{
	mdlOutput_rscPrintf(MSG_ERROR, NULL, MESSAGELISTID_Messages, 
		MESSAGEID_TagExtractErr);
	return ERROR;
	}

    /*  
    //	Note:
    //	The only tag property that is stored with the tag element is
    //	the display flag.  Therefore, there is no need to retrieve the
    //	existing property mask for the tag definition using
    //	mdlTag_getTagDef() before modifying the tag properties.
    //	When I recreate the tag using mdlTag_create(), I can simply
    //	pass the display flag for the property parameter.
    */

    /*
    //	If the tag is currently displayed and the user wants to
    //	turn the display OFF then (display toggle is OFF)
    */
    if  (mdlModelRef_isActiveModel (modelRefP) )
    	{
    	
    if ((TRUE == displayable) && (0 == dlogBoxInfo->display))
	{
    	/* Turn OFF the display of this tag */
        tagProps = TAG_PROP_DISPOFF;

	createTag = TRUE;
	}

    /*
    //	Else if the tag is NOT currently displayed and the user
    //	wants to turn ON the display then (display toggle is ON) 
    */
    else if ((FALSE == displayable) && (-1 == dlogBoxInfo->display))
	{

    	/* Turn ON the display of the flag */
        tagProps = ~TAG_PROP_DISPOFF;

	createTag = TRUE;
	}

    /*******************************************************************
    //
    //  When the value parameter is returned from mdlTag_extract(),
    //  MicroStation has allocated memory for the stringVal and binaryVal
    //  members of the TagValue structure.  You are responsible for
    //  freeing this memory.
    //
    ********************************************************************/
    switch (value.type)
	{
	case MS_TAGTYPE_CHAR:
	    {
	    /* Free the memory allocated by mdlTag_extract() */
  	    free(value.val.stringVal);

	    break;
	    }
        case MS_TAGTYPE_BINARY:
	    {

	    /* Free the memory allocated by mdlTag_extract() */
  	    free(value.val.binaryVal);

	    /*
	    //	Do not change the display flag for the binary type since
	    //	the data is not readable anyway 
	    */
	    createTag = FALSE;

	    break;
	    }

	default:
	    break;
	}


    if (TRUE == createTag)
	{
    	status = mdlTag_create(
		&newTagElm,		/* output area for new tag element */
		&tagElmDescrP->el, 	/* template element */
		NULL,			/* specification for tag definition */
		&tagProps,		/* display flag */
		NULL,			/* value extracted from text element */
	 	NULL,			/* tag id from mdlAssoc_tagElement */
		NULL,			/* computed location offset */
		NULL,			/* rest are special text properties */
		NULL,			/* ...				    */
		NULL,
		NULL
			);
	if (SUCCESS != status)
    	    {
	    mdlOutput_rscPrintf(MSG_ERROR, NULL, MESSAGELISTID_Messages, 
				    MESSAGEID_TagCreateErr);
	    }
	else
	    {

	    /*******************************************************************
	    //	The file position of the tag element is contained in the
	    //	userData1 member in the element descriptor header.
	    //
	    //	For example:
	    //
	    //	MSElementDescr *tagElmDescrP;
	    //
	    //	tagElmDescrP->h.userData1  contains the file position of the tag
	    //
	    //	This file position is needed since I intend to modify the tag 
	    //	element and rewrite the element to the design file at the file
	    //	position contained in tagElmDescrP->h.userData1
	    //
	    //********************************************************************/

	    mdlElement_rewrite(&newTagElm, &tagElmDescrP->el, tagElmDescrP->h.userData1);

	    mdlElement_display(&tagElmDescrP->el, ERASE);

	    mdlElement_display(&newTagElm, NORMALDRAW);
	    }
	} /* if (TRUE == createTag) */
    }//if active model

    return status;

    }


/*----------------------------------------------------------------------+
|									|
| name          tagdisp_modElm                                          |
|									|
| author	BSI     				8/94		|
|									|
+----------------------------------------------------------------------*/
Private int     tagdisp_modElm
(
MSElementUnion  *elP,
void *params,                 /* => user parameter */
DgnModelRefP modelRef,        /* => model to hold current elem */
MSElementDescr *elmDscrP,     /* => element descr for elem */
MSElementDescr **newDscrPP    /* <= if replacing entire descr*/
)		
    {
    int		    numTags, status;
    MSElementDescr  *elmDescrP, *tempP, *prevP;

    /* Extract all tag elements from the selected element */
    if ((status = mdlTag_getElementTags( 
			    &elmDescrP, /* <= linked list of tags   */
			    &numTags, 	/* <= number of tags	    */
			    elP,    	/* => element to get tags for*/
			    modelRef, /* => file number	    */
			    0))     	/* => future arg, always 0  */
			!= SUCCESS)
	{
	mdlOutput_rscPrintf(MSG_ERROR, NULL, MESSAGELISTID_Messages, 
		MESSAGEID_GetTagsErr);
	}
    else
	{
	tempP = elmDescrP;
	while (tempP != NULL)
	    {

	    /* change the display of this tag if necessary */
	    tagdisp_changeDisplay(tempP, modelRef);


	    /*******************************************************************
	    //	After successfully calling mdlTag_getElementTags(), MicroStation
	    //	has allocated memory for a linked list of element descriptors 
	    //	and has returned a pointer to that list in the first parameter
	    //	to this function.
	    //
	    //	You need to call mdlElmdscr_freeAll() to deallocate this memory.
	    *******************************************************************/
	
	    prevP = tempP;
	    tempP = tempP->h.next;
	    mdlElmdscr_freeAll(&prevP);
	    }
	}

/*  Do not change the select element */
    return  MODIFY_STATUS_NOCHANGE;
    }

/*----------------------------------------------------------------------+
|									|
| name          tagdisp_acceptElm                                       |
|                                                                       |
| author	BSI     				8/94		|
|									|
+----------------------------------------------------------------------*/
Private void     tagdisp_acceptElm
(
Dpoint3d    *ptP,
int         view
)
    {
    ULong	    filePos;
    DgnModelRefP    currFile;

/*-----------------------------------------------------------------------
    The located element was accepted using a data point so get the
    accepted element and call mdlModify_elementMulti to change the
    display of any tags associated to this element.
-----------------------------------------------------------------------*/
    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFile);

    mdlModify_elementMulti (currFile, filePos, MODIFY_REQUEST_HEADERS,
                            MODIFY_ORIG, tagdisp_modElm, NULL, TRUE);

/*-----------------------------------------------------------------------
    Restart the locate logic
-----------------------------------------------------------------------*/
    mdlLocate_restart (FALSE);
    }

/*----------------------------------------------------------------------+
|									|
|   Command Functions							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name          tagdisp_changeDisplayCmd                                |
|									|
| author	BSI     				8/94		|
|									|
+----------------------------------------------------------------------*/
extern "C" void tagdisp_changeDisplayCmd
(
char	*unparsedP	/* => unparsed part of command */
)
//cmdNumber   CMD_CHANGE_TAG_DISPLAY
    {

    /* Setup search mask with the type of elements we are looking for */
    tagdisp_setElmSearchType();

    mdlState_startModifyCommand (tagdisp_changeDisplayCmd, /*reset func */
			    tagdisp_acceptElm,  /* datapoint func */
			    NULL,	        /* dynamics func */
			    NULL,	        /* show func */
	                    NULL,	        /* clean func */
			    MESSAGEID_TagDisplay, /* command field message */
			    0,		    	/* accept prompt message */
			    TRUE,	        /* use selection sets */
			    0);	       		/* pts required for accept */

    /* output a prompt asking user to select a graphic element */
    mdlOutput_rscPrintf(MSG_PROMPT, NULL, 
	    	MESSAGELISTID_Messages, MESSAGEID_SelectElement);

    /* Start search at beginning of file */
    mdlLocate_init ();
    }

