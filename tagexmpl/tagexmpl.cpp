/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/tagexmpl.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   tagexmpl.mc  $
|   $Revision: 1.2.16.1 $
|   	$Date: 2013/07/01 20:40:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   tagexmpl.mc - Tag example source code.				|
|              Illustrates basic tag data concepts.		    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <mdl.h>	/* MDL Library funcs structures & constants */
#include <dlogitem.h>	/* Dialog Box Manager structures & constants*/
#include <cmdlist.h>	/* MicroStation command numbers		    */
#include <cexpr.h>	/* C Expression structures & constants      */
#include <rscdefs.h>	/* resource mgr structure definitions & constants */
#include <stdlib.h>
#include <mselems.h>
#include <string.h>
#include <userfnc.h>	/* asynchronous function definitions	    */

#include <dlogman.fdf>  /* Dialog Box Manager Function Prototypes   */
#include <mstagdat.fdf> /* Tag data function prototypes           */
#include <mssystem.fdf>	/* mdlSystem_xxx function prototypes	    */
#include <msparse.fdf>
#include <msrsrc.fdf>
#include <mscexpr.fdf>
#include <msoutput.fdf>
#include <msstate.fdf>
#include <mstagdat.fdf>
#include <dlmsys.fdf>

#include "tagexmpl.h"	/* Contains Dialog Box IDs for this app     */
#include "tagexcmd.h"	/* Commands for this application     */

#include "txttotag.fdf" /* Function definitions for dialog hooks    */

/*----------------------------------------------------------------------+
|                                                                       |
|   Local Function Declarations                                         |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/

/*-----------------------------------------------------------------------
    The following variable is referenced in C expression strings used
    by the toggle button item defined in tagexmpl.r.
-----------------------------------------------------------------------*/
Public	DlogBoxInfo     *dlogBoxInfo = NULL;

/*----------------------------------------------------------------------+
|                                                                       |
| name          tagexmpl_getTagErrorText                                |
|                                                                       |
| Get the textual message for a tag error number                        |
|                                                                       |
| author        BSI                                     08/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int	tagexmpl_getTagErrorText
(
int		errorNumber,	/* => tag function error number */
char		*errorMsg	/* <= text message for error number */
)
    {
    /* 
    	Find the text string matching the error number.  
	Note: Since the tag errors are negative numbers in mdlerrs.h,
	negate errorNumber to make it a positive number.  The
	error numbers are also negated in the MessageList structure.
    */
    if (SUCCESS != mdlResource_loadFromStringList (errorMsg, NULL, 
	    	MESSAGELISTID_TagErrors, -errorNumber))
    	{
    	strcpy(errorMsg, "Unknown error number");
	return	ERROR;
	}

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          tagexmpl_createTagDef                                   |
|                                                                       |
| Fill in a TagDef structure with appropriate values. This can be used  |
| when creating or modifying an existing tag set.                       |
|                                                                       |
| author        BSI                                     08/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int	tagexmpl_createTagDef
(
TagDef		*tagDefP,     /* <= tag definition */
MSWChar		*name,        /* => tag name */
MSWChar		*prompt,      /* => tag prompt or NULL */
UShort		*propsMaskP,  /* => props mask or NULL */
MSWChar		*styleName,   /* => text style name or NULL */
UShort		type,         /* => tag type, MS_TAGTYPE_CHAR, etc */
char		*stringVal,   /* => default string value or NULL */
long		longVal,      /* => default long value or NULL */
double		doubleVal,    /* => default double value or NULL */
int		shortVal      /* => default short value or NULL */
)
    {
    /* tagDefP cannot be null */
    if (NULL == tagDefP)
    	{
        return ERROR;
    	}

    /* initialize the tagDef structure */
    memset (tagDefP, 0, sizeof (TagDef));

    /* a tag set name is required */
    if (NULL == name)
    	{
        return ERROR;
    	}
    else
    	{
        wcscpy (tagDefP->name, name);
    	}

    /* type is required - a default should perhaps be character string */
    if (0 == type)
    	{
        return ERROR;
    	}
    else
    	{
        tagDefP->value.type = type;
    	}

    /* use sent prompt value or set to NULL */
    if (NULL != prompt)
    	{
        wcscpy (tagDefP->prompt, prompt);
    	}

    /* use sent propsMask value or set to NULL */
    if (propsMaskP)
    	{
        memcpy (&tagDefP->propsMask, propsMaskP, sizeof(UShort));
    	}

    /* use sent sytleName value or set to NULL */
    if (NULL != styleName)
    	{
        wcscpy (tagDefP->styleName, styleName);
    	}

    /* test and set possible default value sent by calling function */
    /* this has to be coordinated with type */
    /* if type is character string we have to allocate space */          
    if (NULL != stringVal)
    	{
        tagDefP->value.val.stringVal = (char*)dlmSystem_mdlCalloc (1, TAG_MAX_DATA_BYTES);
        strcpy (tagDefP->value.val.stringVal, stringVal);
    	}
    else if (NULL != (void *)longVal)
    	{
        tagDefP->value.val.longVal = longVal;
    	}
    else if (MS_TAGTYPE_DOUBLE == type)
    	{
        tagDefP->value.val.doubleVal = doubleVal;
    	}
    else if (NULL != (void *)shortVal)
    	{
        tagDefP->value.val.shortVal = (short)shortVal;
    	}

    
    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          tagexmpl_createTagSet                                   |
|                                                                       |
| Create a tag set definition					    	|
|                                                                       |
| author        BSI                                     08/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void tagexmpl_createTagSet
(
char *unparsedP
) 
//cmdNumber CMD_CREATE_TAGSET 
{
    int     	status;
    int     	numTags;
    TagDef	*tagDefA;
    MSWChar	reportName[TAG_MAX_RPT_NAME];
    MSWChar    	setName[TAG_SET_NAME_MAX];
    MSWChar	prompt[TAG_PROMPT_MAX];
    char	errorMsg[80];

    /* initialize tag set name */
    wcscpy (setName, 	L"test1");

    /* initialize report name */
    wcscpy (reportName, L"testrep1");

    /* set the number of tag definitions within the tag set */
    numTags = 3;

    tagDefA = (TagDef*)dlmSystem_mdlCalloc (numTags, sizeof (TagDef));
     
    /*----------------------------------------------------------+ 
    	Call tagexmpl_createTagDef to fill in the tag definition 
	structure for each of the tag definitions that will be
	part of the new tag set.
    +----------------------------------------------------------*/
    /* get prompt from the message list */
    mdlResource_loadFromStringList ((char*)prompt, NULL, 
	    	MESSAGELISTID_Messages, MESSAGEID_DescriptionPrompt);
    /* create a character type tag definition */
    tagexmpl_createTagDef ( &tagDefA[0], /* <= tag definition structure to fill */
			    L"descript",  /* => name of tag definition */
			    prompt, 	 /* => prompt for the tag */
			    NULL, 	 /* => properties mask */
			    NULL, 	 /* => style name (not used) */
			    MS_TAGTYPE_CHAR, /* => type of tag */
			    "hello", 	 /* => string value for tag */
			    NULL, 	 /* => long value for tag */
			    NULL, 	 /* => double value for tag */
			    NULL);	 /* => short value for tag */

    /* get prompt from the message list */
    mdlResource_loadFromStringList ((char*)prompt, NULL, 
	    	MESSAGELISTID_Messages, MESSAGEID_CodePrompt);
    /* create an integer type tag definition */
    tagexmpl_createTagDef ( &tagDefA[1], /* <= tag definition structure to fill */
			    L"code",  	 /* => name of tag definition */
			    prompt, 	 /* => prompt for the tag */
			    NULL, 	 /* => properties mask */
			    NULL, 	 /* => style name (not used) */
			    MS_TAGTYPE_LINT, /* => type of tag */
			    NULL, 	 /* => string value for tag */
			    123, 	 /* => long value for tag */
			    NULL, 	 /* => double value for tag */
			    NULL);	 /* => short value for tag */

    /* get prompt from the message list */
    mdlResource_loadFromStringList ((char*)prompt, NULL, 
	    	MESSAGELISTID_Messages, MESSAGEID_MeasurePrompt);
    /* create a double type tag definition */
    tagexmpl_createTagDef ( &tagDefA[2], /* <= tag definition structure to fill */
			    L"measure",   /* => name of tag definition */
			    prompt, 	 /* => prompt for the tag */
			    NULL, 	 /* => properties mask */
			    NULL, 	 /* => style name (not used) */
			    MS_TAGTYPE_DOUBLE, /* => type of tag */
			    NULL, 	 /* => string value for tag */
			    NULL, 	 /* => long value for tag */
			    3.4, 	 /* => double value for tag */
			    NULL);	 /* => short value for tag */

    /* create the tag set */
    status = mdlTag_createSetDef (  setName, /* => name of set to create */
				    reportName, /* => name of report file */ 
    				    0,
				    tagDefA, /* => array of tag definitions */
				    numTags, /* => number of tags in set */
				    MASTERFILE);	     /* => for future use, always pass 0 */
    if (status != SUCCESS)
	{
	tagexmpl_getTagErrorText(status, errorMsg);

	mdlDialog_dmsgsPrint (errorMsg);

	return;
	}

    /* free the memory that this application allocated */
    mdlTag_freeTagDefArray (tagDefA, /* <=> tag definition array */
			    numTags);/* => number of tags in the array */

    return;
}

/*----------------------------------------------------------------------+
|									|
| name		tagexmpl_unloadFunction					|
|									|
| author	BSI 					06/95		|
|									|
+----------------------------------------------------------------------*/
Private int	tagexmpl_unloadFunction
(
int	    	unloadType  	/* => SYSTEM_TERMINATED_SHUTDOWN */
)
    {

    /* free the memory that was allocated in main */
    if (NULL != dlogBoxInfo)
    	{
    	dlmSystem_mdlFree (dlogBoxInfo);
    	}

    /*
    //	If unloadType is negative, MDL ignores the return value.  If
    //	unloadType is positive, the function can return a non-zero value
    //	to abort the unload.
    */
    return (0);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          main                                                    |
|                                                                       |
| author        BSI                                     8/94            |
|                                                                       |
+----------------------------------------------------------------------*/
Private DialogHookInfo uHooks[]=
    {
    {HOOKDIALOGID_Tags,		    	(PFDialogHook)txttotag_dialogHook},
    {HOOKITEMID_TagSetsList,	    	(PFDialogHook)txttotag_tagsetListHook},
    {HOOKITEMID_TagsList,	    	(PFDialogHook)txttotag_tagListHook},
    };

extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    SymbolSet	    *setP;      /* a ptr to a "C expression symbol set" */
    RscFileHandle   rscFileH;   /* a resource file handle */

/*-----------------------------------------------------------------------
    mdlResource_openFile:

    This function opens a resource file, thus making its contents
    available to the application.  In this case, we need to open
    TAGEXMPL.MA as a resource file so that we have access to the the
    string lists for our messages.					       
-----------------------------------------------------------------------*/
    if (SUCCESS != mdlResource_openFile (&rscFileH, NULL, 0))
    	{
	/* unload the application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
    	}

/*-----------------------------------------------------------------------
    mdlParse_loadCommandTable:
 
    This function is going to load the entire command tree into memory.
    The NULL argument means to use the same resource file that the
    application was from.
-----------------------------------------------------------------------*/
    Private MdlCommandNumber  commandNumbers [] =
    {
    {tagdisp_changeDisplayCmd,CMD_CHANGE_TAG_DISPLAY},
    {txttotag_placeTagFromText ,CMD_PLACE_TAG_FROMTEXT},
    {tagexmpl_createTagSet,CMD_CREATE_TAGSET},
    0,
    };
    
    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);


    if (NULL == mdlParse_loadCommandTable (NULL))
    	{
    	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Messages, 
			    	MESSAGEID_CmdTableLoadError);
    	
	/* unload this application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
    	}

/*-----------------------------------------------------------------------
    To make the Dialog Box Manager aware of the function pointers
    that we have equated to our hook ids we need to publish the
    DialogHookInfo structure, uHooks.
-----------------------------------------------------------------------*/
    mdlDialog_hookPublish (sizeof (uHooks)/sizeof (DialogHookInfo), uHooks);

    mdlState_registerStringIds (MESSAGELISTID_Messages, MESSAGELISTID_Messages);

/*-----------------------------------------------------------------------
    To setup variables evaluated within C expression strings:

    Since we are using a pointer to a structure we need to allocate
    memory for it.  Because this symbol will be used in a Dialog Box
    we specify VISIBILITY_DIALOG_BOX.  Then we publish the pointer to
    our structure so that the Dialog Box Manager becomes aware of it.
-----------------------------------------------------------------------*/
    dlogBoxInfo = (DlogBoxInfo*)dlmSystem_mdlCalloc (1, sizeof (DlogBoxInfo));
    setP=mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);
    mdlDialog_publishComplexPtr (setP, "dlogboxinfo", "dlogBoxInfo", &dlogBoxInfo);

/*-----------------------------------------------------------------------
    Setup initial values for dialog box items before opening it.
    Otherwise values for those items with access strings would be
    undefined and would most likely display junk.
-----------------------------------------------------------------------*/

    /* set display tag flag to ON */
    dlogBoxInfo->display = -1;

    /* publish our unload asynchronous function */
    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM, tagexmpl_unloadFunction);

#   if defined (MSVERSION) && (MSVERSION >= 0x550)
    /* open the toolbox */
    if (NULL == mdlDialog_open (NULL, DIALOGID_TagData))
    	{
    	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Messages, 
			    	MESSAGEID_ToolBoxOpenError);
    	
	/* unload this application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
    	}
#   else
    /* open the tool palette */
    if (NULL == mdlDialog_openPalette (NULL, NULL, DIALOGID_TagData))
    	{
    	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Messages, 
			    	MESSAGEID_PaletteOpenError);
    	
	/* unload this application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
    	}
#   endif

    return  SUCCESS;
    }
