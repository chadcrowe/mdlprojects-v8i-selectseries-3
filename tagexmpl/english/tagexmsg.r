/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/english/tagexmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   tagexmsg.r  $
|   $Revision: 1.3.74.1 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <mdlerrs.h>

#include "tagexmpl.h"

/*----------------------------------------------------------------------+
|									|
|   Messages List Resource Definition					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   General Messages & Prompts message list				|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_Messages =
{
    {
    {MESSAGEID_SelectElement, 		"Accept/Reject (select next input)"},
    {MESSAGEID_TagDisplay, 		"Change Tag Display"},
    {MESSAGEID_TagExtractErr,		"Unable to get tag information"},
    {MESSAGEID_GetTagsErr,  		"No tag elements attached"},
    {MESSAGEID_TagCreateErr,		"Cannot create tag element"},
    {MESSAGEID_TextToTag,		"Change Text To Tag"},
    {MESSAGEID_GraphicElementSelect, 	"Select graphic element"},
    {MESSAGEID_TextElementSelect,    	"Select text element"},
    {MESSAGEID_InvalidTextData,    	"Data type mismatch"},
    {MESSAGEID_TagAlreadyAttached,    	"Tag is already attached"},
    {MESSAGEID_CmdTableLoadError,    	"Unable to load command table"},
    {MESSAGEID_ToolBoxOpenError,    	"Unable to open tool box"},
    {MESSAGEID_PaletteOpenError,    	"Unable to open tool palette"},
    {MESSAGEID_DescriptionPrompt,    	"Description ?"},
    {MESSAGEID_CodePrompt,	    	"Code = ?"},
    {MESSAGEID_MeasurePrompt,    	"Measure = ?"},
    }
};

/*----------------------------------------------------------------------+
|									|
|   Tag errors message list					    	|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_TagErrors =
{
    {
    /*
	Note: Since the tag errors are negative numbers in mdlerrs.h,
	and the message list structure will only accept unsigned long
	values for message identifiers, we have to negate the MDLERR_ 
	values.
	
    */
    {-MDLERR_ATTRUNDEFTYPE,	    	"Undefined tag type"},
    {-MDLERR_ATTRSETNAMELONG,	    	"Tag set name is too long"},
    {-MDLERR_ATTRSETNOTFOUND,	    	"Tag set not found"},
    {-MDLERR_ATTRNOTINSET,	    	"Tag not found in tag set"},
    {-MDLERR_ATTRSETPREVDEFINED,	"Tag set already exists"},
    {-MDLERR_ATTRSETTOOBIG,	    	"Tag set is too large"},
    {-MDLERR_ATTRBADRPTFILE,	    	"Invalid tag report file"},
    {-MDLERR_ATTRNOTARGET,	    	"Association id does not point to valid element"},
    {-MDLERR_ATTRPREVDEFINED,	    	"Tag definition already exists"},
    }
};
