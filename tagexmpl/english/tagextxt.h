/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/tagexmpl/english/tagextxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   tagextxt.h - language specific definitions 			    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__tagextxtH__)
#define __tagextxtH__

/*----------------------------------------------------------------------+
|																								|
|   Defines																					|
|																								|
+----------------------------------------------------------------------*/
#define TXT_DialogTitle		    	"Tags"
#define TXT_DisplayTag		    	"~Display Tags?"

#define TXT_TagToolBoxTitle     	"Tag Data"

#define TXT_Change		    	"Change"

#define TXT_Flyover_ChangeTagDisplay	"Change the display flag of a tag"
#define TXT_Balloon_ChangeTagDisplay	"Change Tag Display"

#define TXT_Flyover_TextToTag       	"Change text to tag data"
#define TXT_Balloon_TextToTag       	"Text To Tag"

#define TXT_TagSets		    	"Tag Sets:"
#define TXT_Tags		    	"Select tag name:"
#define TXT_TagsDialogTitle	    	"Tags"


#endif /* #if !defined (__tagextxtH__) */
