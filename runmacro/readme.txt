			    RUNMACRO
			    ========
This example runs a list of macros at MicroStation startup and/or
at design file open time.  

Steps to utilize this example program:
======================================
1) Build RUNMACRO using BMAKE (see the MDL Programmer's Guide for
instructions about BMAKE setup & execution)

2) Set the following in a configuration file or in MicroStation's configuration
variables dialog box.

    a) if you want BASIC macros run at MicroStation startup then set:
MS_INITAPPS > runmacro      # Load RUNMACRO as a MS_INITAPPS
MS_INITMACROS = macro1.ba   # Macros to be run after MicroStation is started
			    # and before a design file is opened

    b) if you want BASIC macros run after a design file is opened then set:
MS_DGNAPPS > runmacro       # Load RUNMACRO as a MS_DGNAPPS
MS_DGNMACROS = macro1.ba    # Macros to be run after a design file is opened 
MS_DGNMACROS > macro2.bas


Notes:
======
a) MS_MACRO would be used to find the macros.

b) RUNMACRO as a MS_INITAPPS will ignore any command line parameters
sent to MicroStation and open the MicroStation Manager dialog box after 
all macros have been completed.

c) RUNMACRO can be run as both a MS_INITAPPS and a MS_DGNAPPS

d) Any number of macros may be included in either the MS_INITMACROS or
MS_DGNMACROS configuration variables.  Simply separate the macro names
with a semi-colon when using MicroStation's Configuration Variables
dialog box, or use the ">" sign as shown above in 2b).
BASIC macro names may have no extension, ".BA" extension, or ".BAS" extension.

e) MS_INITMACROS are run before a design file is opened.  Therefore the macro must
not access any design file objects or attempt to do anything which requires an
open design file.

f) This example starts the graphics environment using mdlSystem_enterGraphics, 
however, this MDL application may be modified to not start the graphics 
environment.  In this case, the application must not perform any BASIC function
which requires graphics.  Opening a dialog box is an example of a function which
requires the graphics environment.


