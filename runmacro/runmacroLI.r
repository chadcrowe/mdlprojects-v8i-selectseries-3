/*----------------------------------------------------------------------+
|                                                                       |
|   $Copyright: (c) 2008 Bentley Systems, Incorporated. All rights reserved. $
|                                                                       |
| Limited permission is hereby granted to reproduce and modify this     |
| copyrighted material provided that the resulting code is used only in |
| conjunction with Bentley Systems products under the terms of the      |
| license agreement provided therein, and that this notice is retained  |
| in its entirety in any such reproduction or modification.             |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Logfile:   J:/mdl/examples/database/giscmd.r_v  $
|   $Workfile:   giscmd.r  $
|   $Revision: 7.1 $
|   $Date: 2008/12/19 14:20:05 $
|                                                                       |
+----------------------------------------------------------------------*/
#include        <rscdefs.h>
#include        <cmdclass.h>

/*-----------------------------------------------------------------------
 Setup for native code only MDL app
-----------------------------------------------------------------------*/
#define  DLLAPP_PRIMARY     1

DllMdlApp   DLLAPP_PRIMARY =
    {
    "RUNMACRO", "runmacro"
    }

