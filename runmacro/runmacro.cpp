/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/runmacro/runmacro.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   runmacro.mc  $
|   $Revision: 1.7.32.1 $
|   	$Date: 2013/07/01 20:40:52 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   runmacro.mc  - Queues command to run BASIC macros 		    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <cmdlist.h>
#include <dlogitem.h>
#include <rscdefs.h>
#include <userfnc.h>
#include <stdlib.h>
#include <string.h>

#include <msinput.fdf>
#include <msrsrc.fdf>
#include <msstrngl.fdf>
#include <mssystem.fdf>

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Local function definitions						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
static char ustnTaskId [] = "USTN";
/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   External variables							|
|									|
+----------------------------------------------------------------------*/
#if defined (MSVERSION) && (MSVERSION < 0x551)
/*    Note: this function was introduced with MicroStation V5.5.1.x */
/*----------------------------------------------------------------------+
|									|
| name		mdlSystem_createListFromCfgVarValue			|
|									|
| author	BSI 					08/95		|
|									|
+----------------------------------------------------------------------*/
Private int mdlSystem_createListFromCfgVarValue
(                                       /* <= # of values, -1 if error */
StringList      **valueStrListPP,       /* <= */ 
char            *macroValueP            /* => */ 
)
    {
    int         count=0;
    long        index;
    char        *p, *pNext, *pHead;
    StringList  *strListP;

    /* ensure there is a value to parse */ 
    if (NULL == macroValueP)
    	return -1;

    /* check for a value of "" */
    if (!*macroValueP)
    	return 0;

    /* create a string list to hold the results */ 
    if (NULL == (strListP = mdlStringList_create (0, 1)))
    	return -1;

    /* copy string because it will be altered */ 
    p = pHead = dlmSystem_mdlCalloc (1, strlen (macroValueP) + 1);
    strcpy (p, macroValueP);

    do
    	{
    	if (p && (pNext = strchr (p, PATH_SEPARATOR_CHAR)))
	    {
	    *pNext = '\0';
	    pNext++;
	    }

    	if (p != NULL)
	    {
	    if (SUCCESS == mdlStringList_insertMember (&index, strListP, -1, 1))
	    	{
	    	mdlStringList_setMember (strListP, index, p, NULL); 
	    	count++;
	    	}
	    }

    } while (p = pNext);

    dlmSystem_mdlFree (pHead);

    /* pass back the string list if they want it, otherwise free it */ 
    if ((valueStrListPP != NULL) && (count > 0))
    	*valueStrListPP = strListP;
    else
    	mdlStringList_destroy (strListP);

    return count;
    }
#endif /* if defined (MSVERSION) && (MSVERSION < 0x551) */

/*----------------------------------------------------------------------+
|									|
| name		runMacro_runByCfgVar					|
|									|
| author	BSI 					08/95		|
|									|
+----------------------------------------------------------------------*/
Private void	runMacro_runByCfgVar
(
char	    	*cfgVarNameP	/* => "MS_INITMACROS" or "MS_DGNMACROS" */
)
    {
    char    	*macroListP;

#if defined (MSVERSION) && (MSVERSION < 0x551) /* MicroStation PowerDraft */
    char    variableWrapper[MAXNAMELENGTH];

    sprintf (variableWrapper, "$(%s)", cfgVarNameP);

    macroListP = mdlSystem_expandCfgVar (variableWrapper);

#else /* MicroStation V5.5 */
    /* get list of macros to run */
    macroListP = mdlSystem_getExpandedCfgVar (cfgVarNameP);
#endif

    if (NULL != macroListP)
    	{
	StringList  *strListP;
	int	    count;

	/* parse list into individual entries */
    	count = mdlSystem_createListFromCfgVarValue (&strListP, macroListP);

	if (count > 0)
	    {
	    int     i;
	    char    *macroNameP;

	    for (i=0; i<count; i++)
	    	{
		mdlStringList_getMember (&macroNameP, NULL, strListP, i);

		mdlInput_sendCommand (CMD_MACRO, macroNameP, INPUTQ_EOQ, NULL, 0);
	    	}
	    
	    /* free string list created by createListFromCfgVarValue */
	    mdlStringList_destroy (strListP);
	    }

    	/* free buffer created by mdlSystem_getExpandedCfgVar */    	
        mdlSystem_freeCfgVarBuffer(macroListP);
    	}
    }

/*----------------------------------------------------------------------+
|									|
| name		runMacro_newDesignFile					|
|									|
| author	BSI 					08/95		|
|									|
+----------------------------------------------------------------------*/
Private void	runMacro_newDesignFile
(
char		*fileNameP,
int		reasonFlag
)
    {
    if (SYSTEM_NEWFILE_COMPLETE == reasonFlag)
    	{
	/* basic macros to be run just after design file is opened */
    	runMacro_runByCfgVar ("MS_DGNMACROS");
    	}
    }

/*----------------------------------------------------------------------+
|									|
| name		runmacro_reloadFunc      				|
|									|
| author	BSI     				8/95		|
|									|
+----------------------------------------------------------------------*/
Private void  runmacro_reloadFunc
(
int		argc,
char		*argv[]
)
    {
    /*
    // Note:
    //	If RUNMACRO is a MS_INITAPPS and a MS_DGNAPPS, the reload function
    //	is called when loading RUNMACRO as a MS_DGNAPPS.   Therefore,
    //  this function needs to check how the application was loaded
    //	or reloaded.
    */
    /* if this application is running as a MS_INITAPPS */
    if (0 == strcmp (argv [1], "MS_INITAPPS"))
    	{

	/* queue commands to run startup Basic macros */
    	runMacro_runByCfgVar ("MS_INITMACROS");

    	/* queue a command to open MicroStation Manager */
    	mdlInput_sendCommand
              (CMD_DIALOG_OPENFILE, "STARTUP", INPUTQ_EOQ, ustnTaskId, 0);
	}
    else /* the application is a MS_DGNAPPS or started from MS command line */
    	{
    	/* hook new design file events */
    	mdlSystem_setFunction (SYSTEM_NEW_DESIGN_FILE, runMacro_newDesignFile);
    	}
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI 					08/95		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT int MdlMain
(
int		argc,
char		*argv[]
)
    {
    RscFileHandle   resourceFileH;

    /* open our resource file */
    mdlResource_openFile (&resourceFileH, NULL, RSC_READONLY);

    /* if this application is running as a MS_INITAPPS */
    if (0 == strcmp (argv [1], "MS_INITAPPS"))
    	{

	/* start the graphics environment */
	mdlSystem_enterGraphics();

	/* queue commands to run startup Basic macros */
   	runMacro_runByCfgVar ("MS_INITMACROS");

	/* queue a command to open MicroStation Manager */
    	mdlInput_sendCommand
              (CMD_DIALOG_OPENFILE, "STARTUP", INPUTQ_EOQ, ustnTaskId, 0);

	/* publish a reload function pointer for the reload program event */
        mdlSystem_setFunction(SYSTEM_RELOAD_PROGRAM, runmacro_reloadFunc);

    	}
    else
    	{
    	/* hook new design file events */
    	mdlSystem_setFunction (SYSTEM_NEW_DESIGN_FILE, runMacro_newDesignFile);
    	}

    return  SUCCESS;
    }



