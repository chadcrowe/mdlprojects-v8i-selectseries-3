
function OnFinish(selProj, selObj)
{
	try
	{
		var strProjectPath = wizard.FindSymbol('PROJECT_PATH');
		var strProjectName = wizard.FindSymbol('PROJECT_NAME');
//debugger;		
        var strSafeProjectName = CreateSafeName(strProjectName);
        wizard.AddSymbol("SAFE_PROJECT_NAME", strSafeProjectName);
		var strSafeProjectNameUC = strSafeProjectName.toUpperCase();

		wizard.AddSymbol("SAFE_PROJECT_NAME_UPPERCASE", strSafeProjectNameUC);

		var strModalDialog = wizard.FindSymbol ("MFC_MODAL_DIALOG");
		var strNonModalDialog = wizard.FindSymbol ("MFC_NON_MODAL_DIALOG");
		var strDockingDialog = wizard.FindSymbol ("MFC_DOCKING_DIALOG");
		var strToolDialog = wizard.FindSymbol ("MFC_TOOLSETTINGS_DIALOG");
		var strMixedDialog = wizard.FindSymbol ("MFC_MIXED_DIALOG");
		
		if ((strModalDialog) || (strNonModalDialog)|| (strDockingDialog)|| (strToolDialog ) || (strMixedDialog))
		    wizard.AddSymbol("DIALOG",true,false);
		else
		    wizard.AddSymbol ("NODIALOG",true,true);
		    
		    
		selProj = CreateCustomProject(strProjectName, strProjectPath);
		AddConfig(selProj, strProjectName);
		AddFilters(selProj);

		var InfFile = CreateCustomInfFile();
		AddFilesToCustomProj(selProj, strProjectName, strProjectPath, InfFile);
		PchSettings(selProj);
		InfFile.Delete();

		selProj.Object.Save();
	}
	catch(e)
	{
		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function CreateCustomProject(strProjectName, strProjectPath)
{
	try
	{
		var strProjTemplatePath = wizard.FindSymbol('PROJECT_TEMPLATE_PATH');
		var strProjTemplate = '';
		strProjTemplate = strProjTemplatePath + '\\default.vcproj';

		var Solution = dte.Solution;
		var strSolutionName = "";
		if (wizard.FindSymbol("CLOSE_SOLUTION"))
		{
			Solution.Close();
			strSolutionName = wizard.FindSymbol("VS_SOLUTION_NAME");
			if (strSolutionName.length)
			{
				var strSolutionPath = strProjectPath.substr(0, strProjectPath.length - strProjectName.length);
				Solution.Create(strSolutionPath, strSolutionName);
			}
		}

		var strProjectNameWithExt = '';
		strProjectNameWithExt = strProjectName + '.vcproj';
		//debugger;
		var oTarget = wizard.FindSymbol("TARGET");
		var prj;
		if (wizard.FindSymbol("WIZARD_TYPE") == vsWizardAddSubProject)  // vsWizardAddSubProject
		{
		    var prjItem = oTarget.AddFromTemplate(strProjTemplate,strProjectNameWithExt);
			prj = prjItem.SubProject;
		}
		else
		{
		    prj = oTarget.AddFromTemplate(strProjTemplate, strProjectPath, strProjectNameWithExt);
		    //prj = oTarget.AddFromTemplate(strProjectTemplate, strProjectNameWithExt);
		}
		return prj;
	}
	catch(e)
	{
		throw e;
	}
}

function AddFilters(proj)
{
	try
	{
		// Add the folders to your project
		var strSrcFilter = wizard.FindSymbol('SOURCE_FILTER');
		var group = proj.Object.AddFilter('Source Files');
		group.Filter = strSrcFilter;
	}
	catch(e)
	{
		throw e;
	}
}

function AddConfig(proj, strProjectName)
{
	try
	{
		var config = proj.Object.Configurations('Debug');
		config.IntermediateDirectory = 'Debug';
		config.OutputDirectory = 'Debug';

		var CLTool = config.Tools('VCCLCompilerTool');
		// TODO: Add compiler settings

		var LinkTool = config.Tools('VCLinkerTool');
		// TODO: Add linker settings

		config = proj.Object.Configurations('Release');
		config.IntermediateDirectory = 'Release';
		config.OutputDirectory = 'Release';

		var CLTool = config.Tools('VCCLCompilerTool');
		// TODO: Add compiler settings

		var LinkTool = config.Tools('VCLinkerTool');
		// TODO: Add linker settings
	}
	catch(e)
	{
		throw e;
	}
}

function PchSettings(proj)
{
	// TODO: specify pch settings
}

function DelFile(fso, strWizTempFile)
{
	try
	{
		if (fso.FileExists(strWizTempFile))
		{
			var tmpFile = fso.GetFile(strWizTempFile);
			tmpFile.Delete();
		}
	}
	catch(e)
	{
		throw e;
	}
}

function CreateCustomInfFile() {
//debugger;

	try
	{
	    var fso, TemplatesFolder, TemplateFiles, strTemplate;
	    fso = new ActiveXObject('Scripting.FileSystemObject');
		var TemporaryFolder = 2;
		var tfolder = fso.GetSpecialFolder(TemporaryFolder);
		var strTempFolder = tfolder.Path;   //tfolder.Drive + '\\' + tfolder.Name;

		var strWizTempFile = strTempFolder + "\\" + fso.GetTempName();

		var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');
		var strInfFile = strTemplatePath + '\\Templates.inf';
		var bCopyOnly = false;
		var strWizTempFileContents = wizard.RenderTemplateToString(strInfFile)
		//this does not work in VS2008 or 2010?
		//wizard.RenderTemplate(strInfFile, strWizTempFile);//, bCopyOnly, true);

        var oStream = fso.CreateTextFile(strWizTempFile, true);
        oStream.Write(strWizTempFileContents);
        oStream.Close();

        return fso.GetFile(strWizTempFile);
	}
	catch(e)
	{
		throw e;
	}
}
function ProcessMFCDialogs (strFileNameIn,strFileNameWithPath)
{
    strFileNameWithPath="mfc" + "\\"+ strProjectName + strFileNameIn;
}
function GetTargetName(strName, strProjectName)
{
	try
	{
		// TODO: set the name of the rendered file based on the template filename
		var strTarget = strName;
        var strFileNameWithPath;
        
		if (strName == 'readme.txt')
			strTarget = 'ReadMe.txt';

		if (strName == 'sample.txt')
			strTarget = 'Sample.txt';
	//the core application		
		if (strName == 'root.cpp') 
		    strTarget = strProjectName + strName.substr(4);
		if (strName == 'root.h') 
		    strTarget = strProjectName + strName.substr(4);
		if (strName == 'root.r') 
		    strTarget = strProjectName + strName.substr(4);
		if (strName == 'rootCmd.r') 
		    strTarget = strProjectName + strName.substr(4);
		if (strName == 'rootdlg.r') 
		    strTarget = strProjectName + strName.substr(4);
		if (strName == 'root.mke') 
		    strTarget = strProjectName + strName.substr(4);
//process dialog stuff	
       // debugger;	    
	    if ((strName =="mfc\\toolsettings.cpp") || (strName=="mfc\\toolsettings.h"))
	        {
	        
	        //ProcessMFCDialogs(strName,strTarget)
	        strTarget="mfc" + "\\"+ strProjectName+strName.substr(4);
	        }
	     if ((strName =='mfc\\dockable.cpp') || (strName=='mfc\\dockable.h'))
	        {
	        //ProcessMFCDialogs(strName,strTarget)
	        strTarget="mfc" + "\\"+ strProjectName+strName.substr(4);
	        } 
	     if ((strName =='mfc\\hosted.cpp') || (strName=='mfc\\hosted.h'))
	        {
	       // ProcessMFCDialogs(strName,strTarget)
	        strTarget="mfc" + "\\"+ strProjectName+strName.substr(4);
	        } 
	     if ((strName =='mfc\\modal.cpp') || (strName=='mfc\\modal.h'))
	        {
	        //ProcessMFCDialogs(strName,strTarget)
	        strTarget="mfc" + "\\"+ strProjectName+strName.substr(4);
	        } 
	     if ((strName =='mfc\\modeless.cpp') || (strName=='mfc\\modeless.h'))
	        {
	        //ProcessMFCDialogs(strName,strTarget)
	        strTarget="mfc" + "\\"+ strProjectName+strName.substr(4);
	        }
	     if ((strName =='mfc\\mfcdll.cpp') || (strName=='mfc\\mfcdll.h'))
	        {
	        //ProcessMFCDialogs(strName,strTarget)
	        strTarget="mfc" + "\\"+ strProjectName+"MFC"+strName.substr(10);
	        }
	     if (strName=="mfc\\mfc.rc")
	        strTarget = "mfc" + "\\"+ strProjectName+"MFC"+strName.substr(7);
	        
	     if (strName=="mfc\\res\\mfc.rc2")
	        strTarget = "mfc"+"\\"+"res"+"\\"+strProjectName+"MFC"+strName.substr(11);
	        
	     if (strName=='mfc\\nodialog.cpp')
	        strTarget= "mfc" + "\\"+ strProjectName + "Code.cpp";
	      //process the english subdir
	       if ((strName=='english\\roottext.h')||(strName=='english\\rootmsg.r'))
	        strTarget= "english" + "\\"+ strProjectName + strName.substr(12);  
	        
         //if ((strName=='mfc\\stdAfx.cpp')||(strName=='mfc\\stdAfx.h'))
         
		return strTarget; 
	}
	catch(e)
	{
		throw e;
	}
}

function AddFilesToCustomProj(proj, strProjectName, strProjectPath, InfFile)
{
	try
	{
		var projItems = proj.ProjectItems

		var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');

		var strTpl = '';
		var strName = '';

		var strTextStream = InfFile.OpenAsTextStream(1, -2);
		while (!strTextStream.AtEndOfStream)
		{
			strTpl = strTextStream.ReadLine();
			if (strTpl != '')
			{
				strName = strTpl;
				var strTarget = GetTargetName(strName, strProjectName);
				var strTemplate = strTemplatePath + '\\' + strTpl;
				var strFile = strProjectPath + '\\' + strTarget;

				var bCopyOnly = false;  //"true" will only copy the file from strTemplate to strTarget without rendering/adding to the project
				var strExt = strName.substr(strName.lastIndexOf("."));
				if(strExt==".bmp" || strExt==".ico" || strExt==".gif" || strExt==".rtf" || strExt==".css")
					bCopyOnly = true;
				wizard.RenderTemplate(strTemplate, strFile, bCopyOnly);
				proj.Object.AddFile(strFile);
			}
		}
		strTextStream.Close();
	}
	catch(e)
	{
		throw e;
	}
}
