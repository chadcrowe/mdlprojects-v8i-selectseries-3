/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/rootdlg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogids.h>
#include    <rscdefs.h>

#include    "[!output SAFE_PROJECT_NAME].h"
#include    "[!output SAFE_PROJECT_NAME]cmd.h"
#include    <[!output SAFE_PROJECT_NAME]text.h>

[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
#define     BW  30*XC

#define     XSIZE 80*XC
#define     YSIZE 27*YC

DialogBoxRsc        DIALOGID_MDLDialog =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_GROWABLE | DIALOGATTR_AUTOUNLOADAPP,
    XSIZE,YSIZE,NOHELP,MHELP,
    HOOKDIALOGID_MDLDialog, NOPARENTID, 
    TXT_DialogTitle,
    {
[!if MFC_MODAL_DIALOG]
    {{XC,D_ROW(4), BW, 0},	PushButton,	    PUSHBUTTONID_ModalDialog,     ON,  0, "", ""},
[!endif]
[!if MFC_NON_MODAL_DIALOG]
    {{XC,D_ROW(6), BW, 0},	PushButton,	    PUSHBUTTONID_ModelessDialog,     ON,  0, "", ""},
[!endif]
[!if MFC_MIXED_DIALOG]
    {{XC,D_ROW(8), BW, 0},	PushButton,	    PUSHBUTTONID_HostedDialog,     ON,  0, "", ""},
[!endif]
[!if MFC_DOCKING_DIALOG]
    {{XC,D_ROW(10), BW, 0},	PushButton,	    PUSHBUTTONID_DockableDialog,     ON,  0, "", ""},
[!endif]
[!if MFC_TOOLSETTINGS_DIALOG]
    {{XC,D_ROW(12), BW, 0},	PushButton,	    PUSHBUTTONID_ToolSettingsDialog,     ON,  0, "", ""},
[!endif]
    }
    };

#undef  YSIZE
#undef  XSIZE
[!endif]
[!if MFC_MIXED_DIALOG]
#define     XSIZE 40*XC
#define     YSIZE 20*YC

DialogBoxRsc        DIALOGID_HostedDialog =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_GROWABLE,
    XSIZE,YSIZE,NOHELP,MHELP,
    HOOKDIALOGID_HostedDialog, NOPARENTID, 
    TXT_HostedDialogTitle,
    {	
    {{XC,D_ROW(2), BW, 0},	PushButton,         PUSHBUTTONID_MDLButton, ON,  0, "", ""},
    {{0, D_ROW(4), 0, 0},	Separator,          1, ON,  SEPARATOR_HORIZONTAL, "", ""},
    }
    };

#undef  YSIZE
#undef  XSIZE
[!endif]

/*----------------------------------------------------------------------+
|                                                                       |
|    Push Button Items                                                  |
|                                                                       |
+----------------------------------------------------------------------*/
[!if MFC_MODAL_DIALOG]
DItem_PushButtonRsc PUSHBUTTONID_ModalDialog =
    {
    DEFAULT_BUTTON,
    NOHELP, LHELPTOPIC,
    NOHOOK, NOARG, CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_MODALDIALOG, LCMD,
    "",
    "Modal Dialog"
    };
[!endif]

[!if MFC_NON_MODAL_DIALOG]
DItem_PushButtonRsc PUSHBUTTONID_ModelessDialog =
    {
    DEFAULT_BUTTON,
    NOHELP, LHELPTOPIC,
    NOHOOK, NOARG, CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_MODELESSDIALOG, LCMD,
    "",
    "Modeless Dialog"
    };
[!endif]

[!if MFC_MIXED_DIALOG]
DItem_PushButtonRsc PUSHBUTTONID_HostedDialog =
    {
    DEFAULT_BUTTON,
    NOHELP, LHELPTOPIC,
    NOHOOK, NOARG, CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_HOSTEDDIALOG, LCMD,
    "",
    "Hosted Dialog"
    };
DItem_PushButtonRsc PUSHBUTTONID_MDLButton =
    {
    DEFAULT_BUTTON,
    NOHELP, LHELPTOPIC,
    NOHOOK, NOARG, CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_MDLBUTTON, LCMD,
    "",
    "MDL Push Button"
    };
[!endif]

[!if MFC_DOCKING_DIALOG]
DItem_PushButtonRsc PUSHBUTTONID_DockableDialog =
    {
    DEFAULT_BUTTON,
    NOHELP, LHELPTOPIC,
    NOHOOK, NOARG, CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_DOCKABLEDIALOG, LCMD,
    "",
    "Dockable Dialog"
    };
[!endif]

[!if MFC_TOOLSETTINGS_DIALOG]
DItem_PushButtonRsc PUSHBUTTONID_ToolSettingsDialog =
    {
    DEFAULT_BUTTON,
    NOHELP, LHELPTOPIC,
    NOHOOK, NOARG, CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_TOOLSETTINGSDIALOG, LCMD,
    "",
    "ToolSettings Dialog"
    };
[!endif]

