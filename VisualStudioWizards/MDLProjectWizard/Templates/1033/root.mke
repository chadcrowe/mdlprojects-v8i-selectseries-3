#--------------------------------------------------------------------------------------
#
#     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/root.mke,v $
#
#  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
#
#--------------------------------------------------------------------------------------
appName		= [!output SAFE_PROJECT_NAME]
sAppName	= [!output SAFE_PROJECT_NAME]

baseDir         = $(_MakeFilePath)
langSpec	= $(baseDir)english/

[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
guiApp = 1
MFC_WIN_APP=1 
NO_LEAN_AND_MEAN=1
[!endif]

%include mdl.mki

#----------------------------------------------------------------------
# Define macros for files included in our link and resource merge
#----------------------------------------------------------------------
appRscs         = $(o)$(appName).rsc \
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
                  $(o)$(appName)dlg.rsc \
[!endif]
                  $(o)$(appName)msg.rsc \
                  $(o)$(sAppName)cmd.rsc

#----------------------------------------------------------------------
# Create needed output directories if they don't exist
#----------------------------------------------------------------------
$(o)$(tstdir)			: $(o)$(tstdir)

#-----------------------------------------------------------------------
#	Define constants specific to this example
#-----------------------------------------------------------------------
#-----------------------------------------------------------------------
#	Generate non-language resource files
#-----------------------------------------------------------------------
$(baseDir)$(appName)cmd.h       : $(baseDir)$(appName)cmd.r

$(o)$(appName)cmd.rsc		: $(baseDir)$(appName)cmd.r

$(o)$(appName).rsc              :$(baseDir)$(appName).r

[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
$(o)$(appName)dlg.rsc              :$(baseDir)$(appName)dlg.r
[!endif]

#-----------------------------------------------------------------------
#	Generate language resource files
#-----------------------------------------------------------------------
$(o)$(appName)msg.rsc              :$(langSpec)$(appName)msg.r

#-----------------------------------------------------------------------
#  Build Native Code 
#-----------------------------------------------------------------------
dlmObjs = \
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
[!if MFC_MODAL_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]ModalDlg$(oext) \
[!endif]
[!if MFC_NON_MODAL_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]ModelessDlg$(oext) \
[!endif]
[!if MFC_MIXED_DIALOG ]
$(o)[!output SAFE_PROJECT_NAME]HostedDlg$(oext) \
[!endif]
[!if MFC_DOCKING_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]DockableDlg$(oext) \
[!endif]
[!if MFC_TOOLSETTINGS_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]ToolSettingsDlg$(oext) \
[!endif]
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
$(o)StdAfx$(oext) \
$(o)[!output SAFE_PROJECT_NAME]MFC.res \
[!else]
$(o)[!output SAFE_PROJECT_NAME]Code$(oext) \
[!endif]
$(o)[!output SAFE_PROJECT_NAME]MFC$(oext) \
[!endif]
$(o)$(appName)$(oext) 

MSJ_SKIP_SIGNRSCS   = 1
DLM_NAME            = $(appName)
DLM_DEST            = $(mdlapps)
DLM_SYM_NAME        = $(appName)sym
DLM_RESL_NAME       = $(appName)res
DLM_OBJECT_DEST     = $(o)
DLM_LIBDEF_SRC      = $(baseDir)
DLM_OBJECT_FILES    = $(dlmObjs)
DLM_LIBRARY_FILES   = $(dlmLibs)
DLM_NO_DLS          = 1		    # use DLLEXPORT instead
DLM_NO_DEF          = 1
DLM_NOENTRY         = 1
DLM_NO_DELAYLOAD    = 1		
DLM_LIBRARY_FILES 	= $(mdlLibs)dgnfileio.lib \
                $(mdlLibs)toolsubs.lib \
                $(mdlLibs)ditemlib.lib \
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
                $(mdlLibs)mdllib.lib \
		        $(mdlLibs)nativewindow.lib \
        		$(mdlLibs)nativewinmfc.lib	
[!else]
		        $(mdlLibs)mdllib.lib 
[!endif]

#------------------------------------------------
#	Compile the source files for the DLM
#------------------------------------------------
%include dlmcomp.mki

[!if MFC_MODAL_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]ModalDlg$(oext):	$(baseDir)mfc/[!output SAFE_PROJECT_NAME]Modal.cpp \
                                $(baseDir)mfc/[!output SAFE_PROJECT_NAME]Modal.h
                                
[!endif]

[!if MFC_NON_MODAL_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]ModelessDlg$(oext):	$(baseDir)mfc/[!output SAFE_PROJECT_NAME]Modeless.cpp \
                                $(baseDir)mfc/[!output SAFE_PROJECT_NAME]Modeless.h
                                
[!endif]

[!if MFC_MIXED_DIALOG ]
$(o)[!output SAFE_PROJECT_NAME]HostedDlg$(oext):	$(baseDir)mfc/[!output SAFE_PROJECT_NAME]Hosted.cpp \
                                $(baseDir)mfc/[!output SAFE_PROJECT_NAME]Hosted.h
                                
[!endif]

[!if MFC_DOCKING_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]DockableDlg$(oext):	$(baseDir)mfc/[!output SAFE_PROJECT_NAME]Dockable.cpp \
                                $(baseDir)mfc/[!output SAFE_PROJECT_NAME]Dockable.h

[!endif]

[!if MFC_TOOLSETTINGS_DIALOG]
$(o)[!output SAFE_PROJECT_NAME]ToolSettingsDlg$(oext):	$(baseDir)mfc/[!output SAFE_PROJECT_NAME]ToolSettings.cpp \
                                    $(baseDir)mfc/[!output SAFE_PROJECT_NAME]ToolSettings.h

[!endif]

[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
$(o)StdAfx$(oext)           : $(baseDir)mfc/StdAfx.cpp

$(o)[!output SAFE_PROJECT_NAME]MFC.res	: 	$(baseDir)mfc/[!output SAFE_PROJECT_NAME]MFC.rc 

$(o)[!output SAFE_PROJECT_NAME]MFC$(oext)	: $(baseDir)mfc/[!output SAFE_PROJECT_NAME]MFC.cpp \
							$(baseDir)mfc/[!output SAFE_PROJECT_NAME]MFC.h     

[!endif]

$(o)$(appName)$(oext)   : $(baseDir)$(appName).cpp

%include dlmlink.mki

#-----------------------------------------------------------------------
#	Merge Objects into one file
#-----------------------------------------------------------------------
$(o)$(sAppName).mi        : $(appRscs)
	$(msg)	
	>$(o)make.opt
	-o$@
	$(appRscs)
	<
	$(RLibCmd) @$(o)make.opt
	~time

#----------------------------------------------------------------------
# complete construction of the .ma
#----------------------------------------------------------------------
# %include $(sAppName)rsc.mki
#

appRscs =   \
         $(o)$(sAppName).mi

$(mdlapps)$(appName).ma		: $(appRscs)
	$(msg)
	> $(o)make.opt
	-o$@
	$(appRscs)
	<
	$(RLibCmd) @$(o)make.opt
	~time


