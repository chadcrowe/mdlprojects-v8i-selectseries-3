/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/rootCmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

/*----------------------------------------------------------------------+
|									|
|   Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	    CT_NONE	    0
#define	    CT_MAIN	    1
#define     CT_ACTION	    2
#define     CT_ACTYPE        3
#define     CT_DIALOG       4

/*----------------------------------------------------------------------+
|                                                                       |
|   Application command syntax   					|
|                                                                       |
+----------------------------------------------------------------------*/
Table CT_MAIN =
{ 
    { 1, CT_ACTION, PLACEMENT, REQ,	"[!output SAFE_PROJECT_NAME_UPPERCASE]" }, 
}

Table CT_ACTION =
{ 
    { 1, CT_ACTYPE,       INHERIT,	NONE,      "ACTION" },
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]    
    { 2, CT_DIALOG,       INHERIT,  NONE,       "RUN"},
[!endif]
}

Table CT_ACTYPE =
{ 
    { 1, CT_NONE,       INHERIT,	NONE,      "DIALOG" },
[!if PRIMITIVE_COMMAND]
    { 2, CT_NONE,       INHERIT,	NONE,      "PLACE" },
[!endif]
[!if LOCATE_COMMAND]
    { 3, CT_NONE,       INHERIT,	NONE,      "LOCATE" },
[!endif]
[!if SCAN_COMMAND]
    { 4, CT_NONE,       INHERIT,	NONE,      "SCAN" },
[!endif]  
}

[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
Table CT_DIALOG =
{
    { 1, CT_NONE,       INHERIT,	NONE,      "MFCTEST" },
[!if MFC_MODAL_DIALOG]
    { 2, CT_NONE,       INHERIT,	NONE,      "MODALDIALOG" },
[!endif]
[!if MFC_NON_MODAL_DIALOG]
    { 3, CT_NONE,       INHERIT,	NONE,      "MODELESSDIALOG" },
[!endif]
[!if MFC_MIXED_DIALOG]
    { 4, CT_NONE,       INHERIT,	NONE,      "HOSTEDDIALOG" },
    { 5, CT_NONE,       INHERIT,	NONE,      "MDLBUTTON" },
[!endif]
[!if MFC_DOCKING_DIALOG]
    { 6, CT_NONE,       INHERIT,	NONE,      "DOCKABLEDIALOG" },
[!endif]
[!if MFC_TOOLSETTINGS_DIALOG]
    { 7, CT_NONE,       INHERIT,	NONE,      "TOOLSETTINGSDIALOG" },
[!endif]
}
[!endif]