/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/MODAL.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#include "stdafx.h"
#include "[!output SAFE_PROJECT_NAME]MFC.h"
#include "[!output SAFE_PROJECT_NAME]Modal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern "C"
    {
    /*----------------------------------------------*/
    // Entry points for the MDL application
    /*----------------------------------------------*/
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_OpenModalDialog   					*					    	
* @Description Construct the MFC dialog.
* @return 				    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) int [!output SAFE_PROJECT_NAME]Mfc_OpenModalDialog( void )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	[!output SAFE_PROJECT_NAME]ModalDlg *pModalDlg;

	pModalDlg = new [!output SAFE_PROJECT_NAME]ModalDlg();
	pModalDlg->ShowModal();
	delete pModalDlg;

	return true;
	}
    }

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]ModalDlg dialog

[!output SAFE_PROJECT_NAME]ModalDlg::[!output SAFE_PROJECT_NAME]ModalDlg()
	: CBModalDialog([!output SAFE_PROJECT_NAME]ModalDlg::IDD)
    {
    //{{AFX_DATA_INIT([!output SAFE_PROJECT_NAME]ModalDlg)
	// NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

UINT [!output SAFE_PROJECT_NAME]ModalDlg::ShowModal()
    {
    int nRet = CBModalDialog::DoModal();

    switch(nRet)
	{
	case -1:
    	    // Modal Dialog could not be created
    	    break;
	case IDABORT:
    	    // TODO: Handle Abort Message
    	    break;
	case IDOK:
    	    // TODO: Handle OK Message
    	    break;
	case IDCANCEL:
    	    // TODO: Handle Cancel Message
    	    break;
	default:
    	    break;
	}

    return TRUE;
    }

void [!output SAFE_PROJECT_NAME]ModalDlg::DoDataExchange(CDataExchange* pDX)
    {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP([!output SAFE_PROJECT_NAME]ModalDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

BEGIN_MESSAGE_MAP([!output SAFE_PROJECT_NAME]ModalDlg, CBModalDialog)
    //{{AFX_MSG_MAP([!output SAFE_PROJECT_NAME]ModalDlg)
	// NOTE: the ClassWizard will add message map macros here
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]ModalDlg message handlers

BOOL [!output SAFE_PROJECT_NAME]ModalDlg::OnInitDialog()
    {
    CBModalDialog::OnInitDialog();

    // TODO: Add extra initialization here

    return TRUE;
    }
