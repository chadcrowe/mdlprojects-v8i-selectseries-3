/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/DOCKABLE.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

// [!output SAFE_PROJECT_NAME]Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "[!output SAFE_PROJECT_NAME]MFC.h"
#include "[!output SAFE_PROJECT_NAME]Dockable.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Pointer to created dockable dialog
[!output SAFE_PROJECT_NAME]DockableDlg	*pDockable;

extern "C"
    {
    /*----------------------------------------------*/
    // Entry points for the MDL application
    /*----------------------------------------------*/
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_OpenDockableDialog   					*					    	
* @Description Construct the MFC dialog.
* @return 				    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) [!output SAFE_PROJECT_NAME]DockableDlg    *[!output SAFE_PROJECT_NAME]Mfc_OpenDockableDialog( void )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (NULL == pDockable)
	    {
	    pDockable= new [!output SAFE_PROJECT_NAME]DockableDlg("[!output PROJECT_NAME]");
	    pDockable->Create();
	    }

	return pDockable;
	}
    }

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]DockableDlg dialog

[!output SAFE_PROJECT_NAME]DockableDlg::[!output SAFE_PROJECT_NAME]DockableDlg(char *pTitle)
: CBDockableDialog([!output SAFE_PROJECT_NAME]DockableDlg::IDD, pTitle)
    {
    //{{AFX_DATA_INIT([!output SAFE_PROJECT_NAME]DockableDlg)
	// NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

void [!output SAFE_PROJECT_NAME]DockableDlg::DoDataExchange(CDataExchange* pDX)
    {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP([!output SAFE_PROJECT_NAME]DockableDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

BEGIN_MESSAGE_MAP([!output SAFE_PROJECT_NAME]DockableDlg, CBDockableDialog)
    //{{AFX_MSG_MAP([!output SAFE_PROJECT_NAME]Dlg)
	// NOTE: the ClassWizard will add message map macros here
	ON_WM_DESTROY()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]DockableDlg message handlers

void [!output SAFE_PROJECT_NAME]DockableDlg::OnDestroy()
    {
    CBDockableDialog::OnDestroy();

    // TODO: Add your message handler code here

    pDockable = NULL;
    }
