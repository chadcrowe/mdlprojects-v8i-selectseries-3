/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/resource.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by [!output SAFE_PROJECT_NAME]MFC.rc
//
#define IDD_[!output SAFE_PROJECT_NAME]DLG_MODAL	    2000
#define IDD_[!output SAFE_PROJECT_NAME]DLG_MODELESS	    2001
#define IDD_[!output SAFE_PROJECT_NAME]DLG_HOSTED	    2002
#define IDD_[!output SAFE_PROJECT_NAME]DLG_DOCKABLE	    2003
#define IDD_[!output SAFE_PROJECT_NAME]DLG_TOOLSETTINGS   2004

#define IDC_BUTTON1	2000
#define IDC_BUTTON2	2001
#define IDC_TEXT	2002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE	2000
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		2003
#define _APS_NEXT_SYMED_VALUE		2005
#endif
#endif
