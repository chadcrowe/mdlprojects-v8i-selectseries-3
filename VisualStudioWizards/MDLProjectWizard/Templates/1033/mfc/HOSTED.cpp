/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/HOSTED.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#include "stdafx.h"
#include "[!output SAFE_PROJECT_NAME]MFC.h"
#include "[!output SAFE_PROJECT_NAME]Hosted.h"

// required for mdlDialog_closeCommandQueue
#include <msdialog.fdf>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Pointer to created hosted dialog
[!output SAFE_PROJECT_NAME]HostedDlg	*pHosted;

extern "C"
    {
    /*----------------------------------------------*/
    // Entry points for the MDL application
    /*----------------------------------------------*/
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_OpenHostedContent   					*					    	
* @Description Construct the MFC portion of the dialog.
* @return 				    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) [!output SAFE_PROJECT_NAME]HostedDlg    *[!output SAFE_PROJECT_NAME]Mfc_OpenHostedContent( MSWindow *pParent )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (NULL == pHosted)
	    {
	    pHosted = new [!output SAFE_PROJECT_NAME]HostedDlg( pParent );
	    pHosted->Create();
	    }

	return pHosted;
	}
    }
/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]HostedDlg dialog

[!output SAFE_PROJECT_NAME]HostedDlg::[!output SAFE_PROJECT_NAME]HostedDlg(MSWindow *pParent)
	: CBHostedDialog([!output SAFE_PROJECT_NAME]HostedDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT([!output SAFE_PROJECT_NAME]Dlg)
	// NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

BOOL [!output SAFE_PROJECT_NAME]HostedDlg::GetAttachPoint( Point2d *pAttachPoint )
    {
    /* Any offset may be applied here */
    pAttachPoint->x = 0;
    pAttachPoint->y = 60;
    return TRUE;
    }

void [!output SAFE_PROJECT_NAME]HostedDlg::DoDataExchange(CDataExchange* pDX)
    {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP([!output SAFE_PROJECT_NAME]HostedDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

BEGIN_MESSAGE_MAP([!output SAFE_PROJECT_NAME]HostedDlg, CBHostedDialog)
    //{{AFX_MSG_MAP([!output SAFE_PROJECT_NAME]HostedDlg)
	// NOTE: the ClassWizard will add message map macros here
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON1, OnMFCPushButton)
	ON_BN_CLICKED(IDC_BUTTON2, OnCloseButton)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]HostedDlg message handlers

void [!output SAFE_PROJECT_NAME]HostedDlg::OnDestroy() 
    {
    CBHostedDialog::OnDestroy();

    // TODO: Add your message handler code here

    pHosted = NULL;
    }

void [!output SAFE_PROJECT_NAME]HostedDlg::OnMFCPushButton() 
    {
    // TODO: Add your control notification handler code here

    AfxMessageBox ("An MFC Push Button was pushed");
    mdlNativeWindow_onKillFocus (m_pParent, 0);
    }

void [!output SAFE_PROJECT_NAME]HostedDlg::OnCloseButton()
    {
    // TODO: add your control notification handler code here

    mdlDialog_closeCommandQueue( m_pParent );
    [!output SAFE_PROJECT_NAME]HostedDlg::OnDestroy();
    }
