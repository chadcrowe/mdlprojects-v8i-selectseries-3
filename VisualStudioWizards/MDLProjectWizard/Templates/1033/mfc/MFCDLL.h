/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/MFCDLL.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#if !defined(AFX_[!output SAFE_PROJECT_NAME]MFC_H__92C09663_D6F1_42E6_B9F2_03A52110B912__INCLUDED_)
#define AFX_[!output SAFE_PROJECT_NAME]MFC_H__92C09663_D6F1_42E6_B9F2_03A52110B912__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// C[!output SAFE_PROJECT_NAME]MFCApp
// See [!output SAFE_PROJECT_NAME]MFC.cpp for the implementation of this class
//

class C[!output SAFE_PROJECT_NAME]MFCApp : public CWinApp
    {
    public:
	C[!output SAFE_PROJECT_NAME]MFCApp();

    // Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C[!output SAFE_PROJECT_NAME]MFCApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(C[!output SAFE_PROJECT_NAME]MFCApp)
	    // NOTE - the ClassWizard will add and remove member functions here.
	    //    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
    };


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_[!output SAFE_PROJECT_NAME]MFC_H__92C09663_D6F1_42E6_B9F2_03A52110B912__INCLUDED_)
