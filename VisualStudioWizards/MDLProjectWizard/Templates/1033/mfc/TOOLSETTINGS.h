/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/TOOLSETTINGS.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#if !defined(AFX_[!output SAFE_PROJECT_NAME]TOOLSETTINGSDLG_H__9A19FBD0_EDCB_4033_AC0B_BE8EE3CD5127__INCLUDED_)
#define AFX_[!output SAFE_PROJECT_NAME]TOOLSETTINGSDLG_H__9A19FBD0_EDCB_4033_AC0B_BE8EE3CD5127__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// [!output SAFE_PROJECT_NAME]ToolSettingsDlg.h : header file
//

#include "resource.h"

// Bentley defines & includes
#include <msmfc.h>

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]ToolSettingsDlg dialog

class [!output SAFE_PROJECT_NAME]ToolSettingsDlg : public CBToolSettingsDialog
    {
    // Construction
    public:
	[!output SAFE_PROJECT_NAME]ToolSettingsDlg();   // standard constructor

    // Dialog Data
	//{{AFX_DATA([!output SAFE_PROJECT_NAME]ToolSettingsDlg)
	enum { IDD = IDD_[!output SAFE_PROJECT_NAME]DLG_TOOLSETTINGS };
	    // NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


    // Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL([!output SAFE_PROJECT_NAME]ToolSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

    // Implementation
    protected:

	// Generated message map functions
	//{{AFX_MSG([!output SAFE_PROJECT_NAME]ToolSettingsDlg)
	    // NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
    };

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_[!output SAFE_PROJECT_NAME]TOOLSETTINGSDLG_H__9A19FBD0_EDCB_4033_AC0B_BE8EE3CD5127__INCLUDED_)
