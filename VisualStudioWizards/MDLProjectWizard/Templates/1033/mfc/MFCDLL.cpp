/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/MFCDLL.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#include "stdafx.h"
#include "[!output SAFE_PROJECT_NAME]MFC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Add for MicroStation
#include <msnativewindow.h>

//
//	Note!
//
//  If this DLL is dynamically linked against the MFC
//  DLLs, any functions exported from this DLL which
//  call into MFC must have the AFX_MANAGE_STATE macro
//  added at the very beginning of the function.
//
//  For example:
//
//  extern "C" BOOL PASCAL EXPORT ExportedFunction()
//  {
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	// normal function body here
//	}
//
//	It is very important that this macro appear in each
//	function, prior to any calls into MFC.  This means that
//	it must appear as the first statement within the 
//	function, even before any object variable declarations
//	as their constructors may generate calls into the MFC
//	DLL.
//
//	Please see MFC Technical Notes 33 and 58 for additional
//	details.
//

/////////////////////////////////////////////////////////////////////////////
// C[!output SAFE_PROJECT_NAME]MFCApp

BEGIN_MESSAGE_MAP(C[!output SAFE_PROJECT_NAME]MFCApp, CWinApp)
    //{{AFX_MSG_MAP(C[!output SAFE_PROJECT_NAME]MFCApp)
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//    DO NOT EDIT what you see in these blocks of generated code!
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// C[!output SAFE_PROJECT_NAME]MFCApp construction

C[!output SAFE_PROJECT_NAME]MFCApp::C[!output SAFE_PROJECT_NAME]MFCApp()
    {
    // TODO: add construction code here,
    // Place all significant initialization in InitInstance
    }

/////////////////////////////////////////////////////////////////////////////
// The one and only C[!output SAFE_PROJECT_NAME]MFCApp object

C[!output SAFE_PROJECT_NAME]MFCApp theApp;
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]MFCApp::InitInstance   					*					    	
* @Description Called when the dll is instantiated.
* @return 				    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
BOOL C[!output SAFE_PROJECT_NAME]MFCApp::InitInstance()
    {
    // Enable ActiveX Controls
    AfxEnableControlContainer(NULL);

    // Add for MicroStation
    mdlNativeWindow_initialize("[!output PROJECT_NAME]");
    return TRUE;
    }
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]MFCApp::ExitInstance   					*					    	
* @Description Called when the dll is being unloaded.
* @return 				    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
int C[!output SAFE_PROJECT_NAME]MFCApp::ExitInstance()
    {	
    return CWinApp::ExitInstance();
    }
