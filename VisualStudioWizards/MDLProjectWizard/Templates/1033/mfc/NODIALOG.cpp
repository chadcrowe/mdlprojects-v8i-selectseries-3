/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/NODIALOG.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern "C"
    {
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_NativeCode   					*					    	
* @Description a native code function to base calls to.
* @return 	.			    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) void [!output SAFE_PROJECT_NAME]Mfc_NativeCode( void )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	AfxMessageBox( "This is a Native Code Call to AfxMessageBox()" );

	return;
	}
    }
