/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/TOOLSETTINGS.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/

#include "stdafx.h"
#include "[!output SAFE_PROJECT_NAME]MFC.h"
#include "[!output SAFE_PROJECT_NAME]ToolSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern "C"
    {
    /*----------------------------------------------*/
    // Entry points for the MDL application
    /*----------------------------------------------*/
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_OpenToolSettingsContent   					*					    	
* 
* @return 	A pointer to the toolsettings dialog.			    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) [!output SAFE_PROJECT_NAME]ToolSettingsDlg	*[!output SAFE_PROJECT_NAME]Mfc_OpenToolSettingsContent( void )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	[!output SAFE_PROJECT_NAME]ToolSettingsDlg *pModelessBox;

	pModelessBox = new [!output SAFE_PROJECT_NAME]ToolSettingsDlg();
	pModelessBox->Create();

	return pModelessBox;
	}
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_CloseToolSettinsContent   					*					    	
* @param    pTS the pointer to the toolsettings.
* @return 	if the pTS is NULL then false.			    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) StatusInt [!output SAFE_PROJECT_NAME]Mfc_CloseToolSettingsContent( [!output SAFE_PROJECT_NAME]ToolSettingsDlg *pTS )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (NULL == pTS)
	    return false;

	return pTS->CloseContent();
	}
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_GetEditBoxText  					*					    	
* @param    *pTS The reference to the toolsettings content.
* @return 	a char * of the character string on the dialog.			    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) char const * [!output SAFE_PROJECT_NAME]Mfc_GetEditBoxText( [!output SAFE_PROJECT_NAME]ToolSettingsDlg *pTS )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( NULL == pTS )
	    return NULL;

	CString text;
	pTS->GetDlgItemText( IDC_TEXT, text );
	return (LPCSTR)text;
	}
    }

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]ToolSettingsDlg dialog
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]ToolSettingsDlg::[!output SAFE_PROJECT_NAME]ToolSettingsDlg   					*					    	
* @param    [!output SAFE_PROJECT_NAME]ToolSettingsDlg::IDD The idd of the dialog.
* @return 	nothing			    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
[!output SAFE_PROJECT_NAME]ToolSettingsDlg::[!output SAFE_PROJECT_NAME]ToolSettingsDlg()
	: CBToolSettingsDialog([!output SAFE_PROJECT_NAME]ToolSettingsDlg::IDD)
    {
    //{{AFX_DATA_INIT([!output SAFE_PROJECT_NAME]ToolSettingsDlg)
	// NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]ToolSettingsDlg::DoDataExchange   					*					    	
* @param    pDX the CDataExchange
* @return 	nothing			    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
void [!output SAFE_PROJECT_NAME]ToolSettingsDlg::DoDataExchange(CDataExchange* pDX)
    {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP([!output SAFE_PROJECT_NAME]ToolSettingsDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]ToolSettingsDlg   					*					    	
* @param    
* @return 	nothing			    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
BEGIN_MESSAGE_MAP([!output SAFE_PROJECT_NAME]ToolSettingsDlg, CBToolSettingsDialog)
    //{{AFX_MSG_MAP([!output SAFE_PROJECT_NAME]ToolSettingsDlg)
	// NOTE: the ClassWizard will add message map macros here
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]ToolSettingsDlg message handlers
