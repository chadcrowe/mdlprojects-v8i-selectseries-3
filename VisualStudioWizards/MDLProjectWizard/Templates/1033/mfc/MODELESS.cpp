/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/mfc/MODELESS.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include "stdafx.h"
#include "[!output SAFE_PROJECT_NAME]MFC.h"
#include "[!output SAFE_PROJECT_NAME]Modeless.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern "C"
    {
    /*----------------------------------------------*/
    // Entry points for the MDL application
    /*----------------------------------------------*/
/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod [!output SAFE_PROJECT_NAME]Mfc_OpenModelessDialog   					*					    	
* @Description Construct the MFC dialog.
* @return 				    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
    __declspec(dllexport) int [!output SAFE_PROJECT_NAME]Mfc_OpenModelessDialog( void )
	{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	[!output SAFE_PROJECT_NAME]ModelessDlg *pModelessDlg;

	pModelessDlg = new [!output SAFE_PROJECT_NAME]ModelessDlg("[!output PROJECT_NAME]");
	pModelessDlg->Create();

	return true;
	}
    }

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]ModelessDlg dialog

[!output SAFE_PROJECT_NAME]ModelessDlg::[!output SAFE_PROJECT_NAME]ModelessDlg(char *pTitle, UINT nID)
	: CBFramedModelessDialog(nID, pTitle)
    {
    //{{AFX_DATA_INIT([!output SAFE_PROJECT_NAME]ModelessDlg)
	// NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

void [!output SAFE_PROJECT_NAME]ModelessDlg::DoDataExchange(CDataExchange* pDX)
    {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP([!output SAFE_PROJECT_NAME]ModelessDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

BEGIN_MESSAGE_MAP([!output SAFE_PROJECT_NAME]ModelessDlg, CBFramedModelessDialog)
    //{{AFX_MSG_MAP([!output SAFE_PROJECT_NAME]ModelessDlg)
	// NOTE: the ClassWizard will add message map macros here
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// [!output SAFE_PROJECT_NAME]ModelessDlg message handlers

