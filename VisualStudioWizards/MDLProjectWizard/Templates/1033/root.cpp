/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MDLProjectWizard/Templates/1033/root.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   [!output SAFE_PROJECT_NAME]  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   [!output SAFE_PROJECT_NAME] - [!output SAFE_PROJECT_NAME] source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <mselems.h>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <msstrlst.h>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>

#include    <msrmgr.h>
#include    <mssystem.fdf>
#include    <msparse.fdf>

#include	<toolsubs.h>

//scan code
#include	<elementref.h>
//locate code
#include	<msdependency.fdf>
#include	<msassoc.fdf>
#include	<msmisc.fdf>
#include	<mslocate.fdf>
#include	<msstate.fdf>
#include	<msoutput.fdf>

//place command
#include	<mstmatrx.fdf>
char    *g_str;
[!if MFC_TOOLSETTINGS_DIALOG]
void    *pTS;
[!endif]

#include "[!output SAFE_PROJECT_NAME]Cmd.h"
#include "[!output SAFE_PROJECT_NAME].h"

[!if MFC_MODAL_DIALOG]
extern "C"    int [!output SAFE_PROJECT_NAME]Mfc_OpenModalDialog( void );    
[!endif]
[!if MFC_NON_MODAL_DIALOG]
extern "C"    int [!output SAFE_PROJECT_NAME]Mfc_OpenModelessDialog( void );    
[!endif]
[!if MFC_MIXED_DIALOG]
extern "C"    void *[!output SAFE_PROJECT_NAME]Mfc_OpenHostedContent( DialogBox * );
[!endif]
[!if MFC_DOCKING_DIALOG]
extern "C"    int [!output SAFE_PROJECT_NAME]Mfc_OpenDockableDialog( void );
[!endif]
[!if MFC_TOOLSETTINGS_DIALOG]
extern "C"    void *[!output SAFE_PROJECT_NAME]Mfc_OpenToolSettingsContent( void );
extern "C"    int [!output SAFE_PROJECT_NAME]Mfc_CloseToolSettingsContent( void * );
extern "C"    char const * [!output SAFE_PROJECT_NAME]Mfc_GetEditBoxText( void * );
[!endif]

/*---------------------------------------------------------------------------------**//**
* @description  [!output SAFE_PROJECT_NAME]_mdlDialogHook
* @param 	dmP      The DialogMessage 
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void	[!output SAFE_PROJECT_NAME]_mdlDialogHook
(
DialogMessage   *dmP
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	    {
	    case DIALOG_MESSAGE_CREATE:
	        {
	        dmP->u.create.interests.windowMoving= TRUE;
	        dmP->u.create.interests.resizes     = TRUE;
	        dmP->u.create.interests.updates     = TRUE;
	        dmP->u.create.interests.dialogFocuses = TRUE;

	    // initialize Globals
	    
	        break;
	        }
	    case DIALOG_MESSAGE_WINDOWMOVING:
	        {
	        dmP->u.windowMoving.handled = FALSE;

            /* Don't process if only moving dialog box */
            if (dmP->u.windowMoving.whichCorners == CORNER_ALL ||
	    	    dmP->u.windowMoving.whichCorners == CORNER_ALL_RESIZED)
                break;
	        dmP->u.windowMoving.handled = TRUE;
	        break;
	        }
	    case DIALOG_MESSAGE_INIT:
	        {
	    /* Fall through */
	        }
	    case DIALOG_MESSAGE_UPDATE:
	        {
	    // resize items
	        break;
	        }

	    case DIALOG_MESSAGE_RESIZE:
	        {
            /* Don't process if only moving dialog box */
            if (dmP->u.resize.whichCorners == CORNER_ALL)
                break;
	       
	        dmP->u.resize.forceCompleteRedraw = TRUE;
	        break;
	        }
	    case DIALOG_MESSAGE_FOCUSIN:
	        {
	        break;
	        }
	    case DIALOG_MESSAGE_DESTROY:
	        {
	        // save globals
	        break;
	        }
	    default:
	        dmP->msgUnderstood = FALSE;
	        break;
	    }
    }
[!if MFC_MIXED_DIALOG ]
/*---------------------------------------------------------------------------------**//**
* @description  [!output SAFE_PROJECT_NAME]_HostedDialogHook
* @param 	dmP      The DialogMessage 
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void	[!output SAFE_PROJECT_NAME]_HostedDialogHook
(
DialogMessage   *dmP
)
    {

    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    dmP->u.create.interests.createds    = TRUE;
	    dmP->u.create.interests.windowMoving= TRUE;
	    dmP->u.create.interests.resizes     = TRUE;
	    dmP->u.create.interests.updates     = TRUE;
	    dmP->u.create.interests.dialogFocuses = TRUE;

	    // initialize Globals
	    
	    break;
	    }
	    
	case DIALOG_MESSAGE_CREATED:
	    {
	    [!output SAFE_PROJECT_NAME]Mfc_OpenHostedContent (dmP->db);
	    
	    break;
	    }
	    
	case DIALOG_MESSAGE_INIT:
	    {
	    
	    /* Fall through */
	    }
	    
	case DIALOG_MESSAGE_UPDATE:
	    {
		
	    // resize items

	    break;
	    }
	    
	case DIALOG_MESSAGE_WINDOWMOVING:
	    {
	    dmP->u.windowMoving.handled = FALSE;

            /* Don't process if only moving dialog box */
            if (dmP->u.windowMoving.whichCorners == CORNER_ALL ||
	    	dmP->u.windowMoving.whichCorners == CORNER_ALL_RESIZED)
                break;
		    
	    dmP->u.windowMoving.handled = TRUE;

	    break;
	    }
	    
	case DIALOG_MESSAGE_RESIZE:
	    {
            /* Don't process if only moving dialog box */
            if (dmP->u.resize.whichCorners == CORNER_ALL)
                break;
	    
	    dmP->u.resize.forceCompleteRedraw = TRUE;
		
	    break;
	    }

	case DIALOG_MESSAGE_FOCUSIN:
	    {
	    break;
	    }

	case DIALOG_MESSAGE_DESTROY:
	    {
	    // save globals
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }
[!endif]
[!if MFC_MODAL_DIALOG ]

/*---------------------------------------------------------------------------------**//**
* @description  Opens a native modal dialog box
* @param 	unparsed
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" /*DLLEXPORT*/Public void [!output SAFE_PROJECT_NAME]_runModalDialog
(
char            *unparsed
)
    {
    [!output SAFE_PROJECT_NAME]Mfc_OpenModalDialog();
    }
[!endif]
[!if MFC_NON_MODAL_DIALOG]
/*---------------------------------------------------------------------------------**//**
* @description  Opens a native modeless dialog box
* @param 	unparsed
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void [!output SAFE_PROJECT_NAME]_runModelessDialog
(
char            *unparsed
)
    {
    [!output SAFE_PROJECT_NAME]Mfc_OpenModelessDialog();
    }
[!endif]
[!if MFC_MIXED_DIALOG ]
/*---------------------------------------------------------------------------------**//**
* @description  Opens an MDL dialog box with some native content
* @param 	unparsed
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void [!output SAFE_PROJECT_NAME]_runHostedDialog
(
char            *unparsed
)
    {
    DialogBox   *dbP;

    if (NULL != (dbP=mdlDialog_find (DIALOGID_HostedDialog,NULL)))
        mdlDialog_show (dbP);
    else
        mdlDialog_open (NULL, DIALOGID_HostedDialog);
    }

/*---------------------------------------------------------------------------------**//**
* @description  Runs something when an MDL PushButton is pushed
* @param 	unparsed
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void [!output SAFE_PROJECT_NAME]_runMDLButton
(
char            *unparsed
)
    {
    printf ("MDL Push Button pressed\n");
    }
[!endif]
[!if MFC_DOCKING_DIALOG]
/*---------------------------------------------------------------------------------**//**
* @description  Opens a native dockable dialog box
* @param 	unparsed
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void [!output SAFE_PROJECT_NAME]_runDockableDialog
(
char            *unparsed
)
    {
    [!output SAFE_PROJECT_NAME]Mfc_OpenDockableDialog();
    }
[!endif]
[!if MFC_TOOLSETTINGS_DIALOG]
/*---------------------------------------------------------------------------------**//**
* @description  Function called when cleaning up the command
* @param 	bEnteringViewCmd TRUE if entering a view command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public int [!output SAFE_PROJECT_NAME]_ToolSettingsCleanup
(
void
)
    {
    [!output SAFE_PROJECT_NAME]Mfc_CloseToolSettingsContent(pTS);
    
    return  SUCCESS;
    }
    
/*---------------------------------------------------------------------------------**//**
* @description  Function called when resetting out of a command
* @param 	bEnteringViewCmd TRUE if entering a view command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public int [!output SAFE_PROJECT_NAME]_ToolSettingsReset
(
void
)
    {
    mdlState_startDefaultCommand ();
    
    return  SUCCESS;
    }
/*---------------------------------------------------------------------------------**//**
* @description  [!output SAFE_PROJECT_NAME]_runToolSettingsDialog
* @param 	unparsed
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void [!output SAFE_PROJECT_NAME]_runToolSettingsDialog
(
char            *unparsed
)
    {
    
    // TODO: start your primitive here
//    placeDate_start(unparsed);
    mdlState_setFunction (STATE_RESET, [!output SAFE_PROJECT_NAME]_ToolSettingsReset);
    mdlState_setFunction (STATE_COMMAND_CLEANUP, [!output SAFE_PROJECT_NAME]_ToolSettingsCleanup);
    pTS = [!output SAFE_PROJECT_NAME]Mfc_OpenToolSettingsContent();
    }
[!endif]
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
/*---------------------------------------------------------------------------------**//**
* @description  [!output SAFE_PROJECT_NAME]_openMainDialog
* @param 	unparsed
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void [!output SAFE_PROJECT_NAME]_openMainDialog
(
char            *unparsed
)
    {
    DialogBox   *dbP;

    if (NULL != (dbP=mdlDialog_find (DIALOGID_MDLDialog,NULL)))
        mdlDialog_show (dbP);
    else
        mdlDialog_open (NULL, DIALOGID_MDLDialog);
    }
[!endif]
[!if SCAN_COMMAND]
/*---------------------------------------------------------------------------------**//**
* @description  elemRefCallback
* @param 	*pRef	a temporary pointer to the element reference that meets the scan criteria.
* @param 	*pArgs	the user args passed to the scan call back
* @param 	*pScanCriteria	the scan criteria used to scan.
* @return	SUCCESS
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
DLLEXPORT    int	elemRefCallback
(
ElementRef      pRef,
void            *pArgs,
ScanCriteria    *scP
)
    {
    long        elmType = elementRef_getElemType (pRef);

    printf ("found element of type %ld \n",elmType);
    
    return  SUCCESS;
    }
/*---------------------------------------------------------------------------------**//**
* @description  [!output SAFE_PROJECT_NAME]ScanCmd
* @param 	unparsed      The unparsed parameter passed in to the command.
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void [!output SAFE_PROJECT_NAME]ScanCmd
(
char    *unparsed
)
{
    ScanCriteria    *scP = mdlScanCriteria_create ();
    mdlScanCriteria_setReturnType (scP, MSSCANCRIT_ITERATE_ELMREF ,FALSE,FALSE);
    mdlScanCriteria_setElemRefCallback (scP,elemRefCallback,NULL);
    mdlScanCriteria_setDrawnElements (scP);
    mdlScanCriteria_setModel (scP,MASTERFILE);  //we are hard coded to active model only right now.
    mdlScanCriteria_scan (scP,NULL,NULL,NULL);
    mdlScanCriteria_free (scP);
    return;

}
[!endif]
[!if LOCATE_COMMAND]
/*----------------------------------------------------------------------+*//**
* Process the selection of elements.                                    *
*                                                                       *
* @bsimethod modElm                                                     *                                               
* @param    *elP,       <=> element to be modified                      *
* @param    *params,      => user parameter                             *
* @param    pModelRef,        => Model ref for current element          *
* @param  *elmDscrP,    => element descr for element                    *
* @param  **newDscrPP   <= if replacing entire descr                    *
* @return       MODIFY_STATUS_ see documentation on mdlModify functions *
*                                                                       *
* Author:   BSI                                 02/01           *
*                                                                       *
+----------------------------------------------------------------------*/
Public  int modElmInfo
(
MSElementUnion  *elP,       /* <=> element to be modified       */
void            *params,    /*  => user parameter               */
DgnModelRefP    pModelRef,    /*  => Model ref for current element      */
MSElementDescr  *elmDscrP,  /*  => element descr for element    */
MSElementDescr  **newDscrPP /*  <= if replacing entire descr    */
)
    {
    ElementID   pDepenedents[10];
    MSElement   localElement;
    UInt32      filePos;
    int         status;
    int         numDeps;
    int         i;

    numDeps =  mdlDependency_getNumOfDependents (mdlElement_getID (elP),pModelRef);

    status = mdlDependency_getDependents (pDepenedents,mdlElement_getID (elP),pModelRef,10);
    for (i=0;i<numDeps ;++i    )
        {
        mdlAssoc_getElement (&localElement,&filePos,pDepenedents[i],pModelRef);
        mdlElement_display (&localElement,HILITE);
        }

    return  MODIFY_STATUS_NOCHANGE;
    }

/*----------------------------------------------------------------------+*//**
* Process Fence Selection                                               *
* @bsimethod fenceModElm                                                * 
* @param        arg     user arguments to pass into this function.      *
* @return       SUCCESS                          *
*                                                                       *
* Author:   BSI                                 02/01           *
*                                                                       *
+----------------------------------------------------------------------*/
Public  int fenceModElmInfo
(
void      *arg
)
    {
    ULong           filePos;
    DgnModelRefP    currFileP;
    MSElement       el;

    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFileP);

    mdlModify_elementSingle (currFileP, filePos, MODIFY_REQUEST_NOHEADERS,
                            MODIFY_COPY, modElmInfo, NULL, 0L);
    mdlElement_read (&el,currFileP,filePos);
 
    return SUCCESS;
    }
/*---------------------------------------------------------------------------------**//**
* @description  the function that is called when the user hits a data point to confirm the selected element(s)
* @param 	pntP      The data point that the user picked to accept the element location.
* @param 	view      The view number that the selected point is in.
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
Private void     acceptElm
(
Dpoint3d    *pntP,
int         view
)
    {
    UInt32          filePos;
    DgnModelRefP    refModelP;
    
    /*------------------------------------------------------------------
    | The located element was accepted using a data point so get the
    | accepted element and reference file info.  Our testLocate_ElmFilter
function
    | has already verified that the element is not already in the active
model.
    +------------------------------------------------------------------*/


    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &refModelP);

    mdlModify_elementSingle (refModelP, filePos, MODIFY_REQUEST_NOHEADERS,
                            MODIFY_COPY, modElmInfo, NULL, 0L);


    /* Restart the locate logic */
    mdlLocate_restart (FALSE);
    }
/*----------------------------------------------------------------------+*//**
The located element was accepted using a data point so get the
accepted element and call mdlModify_elementMulti to process.
*                                                                           *
* @bsimethod acceptElmInfo                                                  *                                               *
* @param        nameOfParam     !!!what this parameter means            *
*                                                                       *
* Author:   BSI                                 02/01           *
*                                                                       *
+----------------------------------------------------------------------*/
Public  void acceptElmInfo
(
Dpoint3d    *pntP,
int         view
)
    {
    ULong           filePos;
    DgnModelRefP    currFileP;
    /*------------------------------------------------------------------
    | The located element was accepted using a data point so get the
    | accepted element and call mdlModify_elementMulti to change it's
    | symbology.
    +------------------------------------------------------------------*/
    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFileP);
    mdlModify_elementMulti (currFileP, filePos, MODIFY_REQUEST_HEADERS,
                            MODIFY_ORIG, modElmInfo, NULL, TRUE);

    /* Restart the locate logic */
    mdlLocate_restart (FALSE);
    }

/*----------------------------------------------------------------------+*//**
* Set the search type mask to lines, text, and shapes only
                                                                            *
* @bsimethod setElmSearchTypeInfo                                           *                                               *
*                                                                       *
* Author:   BSI                                 02/01           *
*                                                                       *
+----------------------------------------------------------------------*/
Public  void setElmSearchTypeInfo
(
void
)
    {//this will only look for lines, text, and shapes.
    static int  searchType[] = {LINE_ELM, TEXT_ELM,SHAPE_ELM};

    /* Clear search criteria */
    /* Alternate ways of setting the search criteria. */
    //mdlLocate_noElemAllowLocked ();
    //mdlLocate_allowLocked ();
    /* Set search mask to look for text and line elements */
    mdlLocate_setElemSearchMask (sizeof(searchType)/sizeof(int), searchType);
    }

/*----------------------------------------------------------------------+*//**
* The function called before the element is actually accepted.          *
* the function can reject the selection                                 *
* @bsimethod myapp_preLocateInfo                                        *                                               
* @param        action     the operation being performed by MicroStation             *
* @param        pElement     the element under consideration             *
* @param        modelRef     the modelRef for the element             *
* @param        filePosition     the filePos of the element             *
* @param        pPoint     user entered point in world coordinates. 
                           Sometimes (like in fence and selection set processing) pPoint can be NULL.             *
* @param        viewNumber     the view in which the element was located             *
* @param        hitPath     the HitPath for the element. Sometimes (like in fence and selection set processing) hitPath can be NULL.            *
* @param        rejectReason     when rejecting the element, fill in with the reason for the rejection. 
                                This string will be presented to the user to explain why the element can't be picked.
                                The buffer is 256 characters. Note: rejectReason can be NULL, in which case no rejection reason can be provided.             *
* @return       LOCATE_FILTER_STATUS_ Reject or Neutral                 *
*                                                                       *
* Author:                                   12/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  LocateFilterStatus  [!output SAFE_PROJECT_NAME]_preLocateInfo
(
LOCATE_Action       action , 
MSElement*          pElement , 
DgnModelRefP        modelRef , 
ULong               filePosition , 
DPoint3d*           pPoint , 
int                 viewNumber , 
HitPathP            hitPath , 
char*               rejectReason 
)
    {
    if (GLOBAL_LOCATE_IDENTIFY != action && GLOBAL_LOCATE_AUTOLOCATE != action)
        return  LOCATE_FILTER_STATUS_Neutral;

    /* see if element is already active model- if yes,we must reject*/
    if (mdlModelRef_isActiveModel(modelRef)!=TRUE )
        {
        if  (NULL != rejectReason)
            sprintf(rejectReason,"Element is in a reference model");
        
        return LOCATE_FILTER_STATUS_Reject;
        }
    return LOCATE_FILTER_STATUS_Neutral;
    }
/*---------------------------------------------------------------------------------**//**
* @description  [!output SAFE_PROJECT_NAME]LocateCmd
* @param 	unparsed      The unparsed arguments to the command. 
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void [!output SAFE_PROJECT_NAME]LocateCmd
(
char    *unparsed
)
    {
    /* Setup search mask with the type of elements we are looking for */
    setElmSearchTypeInfo();
    /* If the user wants to use a fence operation */
    if (0 != tcb->msToolSettings.general.useFence)
        {
        if (tcb->fence > 0)
            {
            mdlState_startFenceCommand(fenceModElmInfo, /* content func */
                           NULL,                    /* outline func */
                           NULL,                    /* data point func */
                           [!output SAFE_PROJECT_NAME]LocateCmd,   /* reset func */
                           0,    /* prompt message */
                           0, /* command message */
                           FENCE_NO_CLIP);          /* clipping mode */
            }
        else
            {
            tcb->msToolSettings.general.useFence = FALSE;
            }
        }
    else
        {

        mdlState_startModifyCommand ([!output SAFE_PROJECT_NAME]LocateCmd, /* reset func */
                           acceptElm,    /* datapoint func */
                           NULL,         /* dynamics func */
                           NULL,         /* show func */
                           NULL,         /* clean func */
                           0, /* command field message */
                           0,      /* prompt message */
                           TRUE,         /* use selection sets */
                           0);           /* points required for accept */
        }
    mdlOutput_rscPrintf (MSG_PROMPT, 0, 0,
			0);


    /* Start search at beginning of file */
    mdlAutoLocate_enable (TRUE,TRUE,TRUE);
    mdlAccuSnap_enableLocate (TRUE);
    mdlAccuSnap_enableSnap (TRUE);

    mdlLocate_init ();
    mdlLocate_setFunction (LOCATE_POSTLOCATE, [!output SAFE_PROJECT_NAME]_preLocateInfo);

        

    }
[!endif]
[!if PRIMITIVE_COMMAND]
/*----------------------------------------------------------------------+*//**
* Creates the text element	    
* @bsimethod createText   				
* @param 	pntP     Data point to create the text	
* @param 	view     view to create the text	
* @param 	elP     The element to create	    	
* @return 	SUCCESS 			    	
*							
* Author:   BSI 				06/03 	
*							
+----------------------------------------------------------------------*/
MdlPublic  int createText
(
Dpoint3d    *pntP, /*Data point to create the text*/
int         view,  /*view to create the text*/
MSElement   *elP   /*The element to create*/
)
    {
    /* Temporarily create the text element in the element passed in */
    mdlText_create (elP, NULL, g_str, NULL, NULL, NULL, NULL, NULL);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+*//**
* acts as a layer betweeen the createText and the calling functions for dynamics    
* @bsimethod generateText   					
* @param 	pntP     Data point to create the text	
* @param 	view     view to create the text	
*								
* Author:   BSI 				06/03 		
*								
+----------------------------------------------------------------------*/
Public  void generateText
(
DPoint3d        *pntP, /*Data point to create the text*/
int             view,   /*view to create the text*/
int             drawMode /* TEMPDRAW OR TEMPERASE */
)
    {
    Transform   t;
    MSElementDescr  *edP = NULL;
    MSElement       tempEl;
    DPoint3d        origin;

    mdlTMatrix_getIdentity (&t);
    mdlTMatrix_setTranslation (&t,pntP);
    
    createText (&origin, view, &tempEl);
    
    mdlElmdscr_new (&edP,NULL,&tempEl);
    mdlElmdscr_transform (edP, &t);
    mdlElmdscr_freeAll (&edP);    
    }
/*----------------------------------------------------------------------+*//**
* 							      
* @bsimethod [!output SAFE_PROJECT_NAME]_done   				      
*							      
* Author:   BSI 				06/03 	      
*							      
+----------------------------------------------------------------------*/
Public  void [!output SAFE_PROJECT_NAME]_done
(
void
)
    {
    /*-----------------------------------------------------------------
    | Start the default command, after you have placed the text element
    | for the date stamp.  This is a single shot tool.
    +-----------------------------------------------------------------*/
    dlmSystem_mdlFree (g_str);
    mdlState_startDefaultCommand ();
    }

/*----------------------------------------------------------------------+*//**
* 							      
* @bsimethod [!output SAFE_PROJECT_NAME]_atPoint   			      
* @param 	pntP     Data point to create the text	
*							      
* Author:   BSI 				06/03 	      
*							      
+----------------------------------------------------------------------*/
Public  void [!output SAFE_PROJECT_NAME]_atPoint
(
Dpoint3d        *pntP
)
    {
    MSElement   el;
    Transform   t;
    MSElementDescr  *edP = NULL;
    mdlTMatrix_getIdentity (&t);
    mdlTMatrix_setTranslation (&t,pntP);
    
    /*-----------------------------------------------------------------
    | As the location for our date stamp can be specified by a single
    | datapoint use the dynamics function to create our text element
    | in dgnBuf at it's final location.
    +-----------------------------------------------------------------*/
    createText (pntP, tcb->lstvw, &el);

    mdlElmdscr_new (&edP,NULL,dgnBuf);
    mdlElmdscr_transform (edP, &t);
  /* Display the new text element and add it to the design file */
    mdlElmdscr_display (edP, ACTIVEMODEL, NORMALDRAW);
    mdlElmdscr_add (edP);  
    mdlElmdscr_freeAll (&edP);
    /* Call our cleanup function terminating the primitive command */
    [!output SAFE_PROJECT_NAME]_done ();
    }

/*----------------------------------------------------------------------+*//**
* 							      
* @bsimethod [!output SAFE_PROJECT_NAME]PlaceCmd   				      
* @param 	unparsedP     Contination string that started this command
*							      
* Author:   BSI 				06/03 	      
*							      
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT void [!output SAFE_PROJECT_NAME]PlaceCmd
(
char    *unparsed
)
    {
    if (unparsed)
        g_str = dlmSystem_strdup (unparsed);
    else
        g_str = dlmSystem_strdup("test");

    mdlState_startPrimitive ([!output SAFE_PROJECT_NAME]_atPoint, [!output SAFE_PROJECT_NAME]_done, 
			    0, 0);

    /*-----------------------------------------------------------------
    | Start dynamics to display elements when one more datapoint will 
    | give enough information to write an element to the design file.
    | For our text element, we can start dynamics right away.
    +-----------------------------------------------------------------*/
    mdlState_setFunction (STATE_COMPLEX_DYNAMICS, generateText);
    }
[!endif]
/*---------------------------------------------------------------------------------**//**
* @description  [!output SAFE_PROJECT_NAME]_mdlCommand
* @param 	unparsed      The unparsed information sent to the command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void [!output SAFE_PROJECT_NAME]_mdlCommand 
(
char * unparsed
)
    {
	printf ("[!output SAFE_PROJECT_NAME]_mdlCommand\n");

	mdlDialog_dmsgsPrint("[!output SAFE_PROJECT_NAME]_mdlCommand\n");  

    }
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
typedef void (*Handler)(char *);

Private DialogHookInfo uHooks[] =
    {
    {HOOKDIALOGID_MDLDialog,  (PFDialogHook)[!output SAFE_PROJECT_NAME]_mdlDialogHook},
[!if MFC_MIXED_DIALOG]
	{HOOKDIALOGID_HostedDialog,  (PFDialogHook)[!output SAFE_PROJECT_NAME]_HostedDialogHook},
[!endif]
    };
[!endif]
/*---------------------------------------------------------------------------------**//**
* @description  MdlMain
* @param 	argc      The number of command line parameters sent to the application.
* @param 	argv[]    The array of strings sent to the application on the command line.
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
	RscFileHandle   rfHandle;

    mdlResource_openFile (&rfHandle,NULL,RSC_READONLY);

    // Map command name to function (usage: MDL COMMAND COMPEXPORT)
    static  MdlCommandName cmdNames[] = 
    {
        {[!output SAFE_PROJECT_NAME]_mdlCommand, "[!output SAFE_PROJECT_NAME]_mdlCommand"  },
[!if PRIMITIVE_COMMAND]
        {[!output SAFE_PROJECT_NAME]PlaceCmd, "[!output SAFE_PROJECT_NAME]PlaceCmd"  },
[!endif]
[!if LOCATE_COMMAND]
        {[!output SAFE_PROJECT_NAME]LocateCmd, "[!output SAFE_PROJECT_NAME]LocateCmd"  },
[!endif]
[!if SCAN_COMMAND]
        {[!output SAFE_PROJECT_NAME]ScanCmd, "[!output SAFE_PROJECT_NAME]ScanCmd"  },
[!endif]
        0,
    };

    mdlSystem_registerCommandNames (cmdNames);

    // Map key-in to function
    static MdlCommandNumber cmdNumbers[] =
    {
		{[!output SAFE_PROJECT_NAME]_mdlCommand,  CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_ACTION_DIALOG },
[!if PRIMITIVE_COMMAND]
		{[!output SAFE_PROJECT_NAME]PlaceCmd,  CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_ACTION_PLACE },
[!endif]
[!if LOCATE_COMMAND]
		{[!output SAFE_PROJECT_NAME]LocateCmd,  CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_ACTION_LOCATE },
[!endif]
[!if SCAN_COMMAND]
		{[!output SAFE_PROJECT_NAME]ScanCmd,  CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_ACTION_SCAN },
[!endif]
[!if MFC_MODAL_DIALOG || MFC_NON_MODAL_DIALOG || MFC_MIXED_DIALOG || MFC_DOCKING_DIALOG || MFC_TOOLSETTINGS_DIALOG]
        {[!output SAFE_PROJECT_NAME]_openMainDialog,	CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_MFCTEST},
[!endif]
[!if MFC_MODAL_DIALOG]
        {[!output SAFE_PROJECT_NAME]_runModalDialog,	CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_MODALDIALOG},
[!endif]
[!if MFC_NON_MODAL_DIALOG]
	{[!output SAFE_PROJECT_NAME]_runModelessDialog,	CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_MODELESSDIALOG},
[!endif]
[!if MFC_MIXED_DIALOG]
	{[!output SAFE_PROJECT_NAME]_runHostedDialog,	CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_HOSTEDDIALOG},
   	{[!output SAFE_PROJECT_NAME]_runMDLButton,		CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_MDLBUTTON},
[!endif]
[!if MFC_DOCKING_DIALOG]
	{[!output SAFE_PROJECT_NAME]_runDockableDialog,	CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_DOCKABLEDIALOG},
[!endif]
[!if MFC_TOOLSETTINGS_DIALOG]
	{[!output SAFE_PROJECT_NAME]_runToolSettingsDialog,	CMD_[!output SAFE_PROJECT_NAME_UPPERCASE]_RUN_TOOLSETTINGSDIALOG},
[!endif]
        0,
    };

    mdlSystem_registerCommandNumbers (cmdNumbers);

    mdlParse_loadCommandTable (NULL);

 	return SUCCESS;
    }
