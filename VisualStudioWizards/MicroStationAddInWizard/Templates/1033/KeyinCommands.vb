'/*--------------------------------------------------------------------------------------+
'|
'|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/KeyinCommands.vb,v $
'|    $RCSfile: KeyinCommands.vb,v $
'|   $Revision: 1.1 $
'|
'|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
'|
'+--------------------------------------------------------------------------------------*/

Imports Bentley.Interop.MicroStationDGN

Namespace [!output SAFE_PROJECT_NAME]

Public Class KeyinCommands

Public Shared Sub [!output SAFE_PROJECT_NAME]Command (ByVal unparsed as String)
'insert code here
End Sub
[!if CHECKBOX_PALCEMENT]
Public Shared Sub [!output SAFE_PROJECT_NAME]PlacementCommand (ByVal unparsed as String)
      [!output SAFE_PROJECT_NAME]PlacementCmd.StartPlacementCommand ([!output SAFE_PROJECT_NAME].GetApp)
End Sub
[!endif]
    
[!if CHECKBOX_LOCATE]
Public Shared Sub [!output SAFE_PROJECT_NAME]LocateCommand (ByVal unparsed as String)
    [!output SAFE_PROJECT_NAME]PlacementCmd.StartPlacementCommand ([!output SAFE_PROJECT_NAME].GetApp)
End Sub
    [!endif]    

end class
end namespace