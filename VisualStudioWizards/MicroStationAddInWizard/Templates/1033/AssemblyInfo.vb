'----------------------------------------------------------------------
'
'     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/AssemblyInfo.vb,v $
'
'  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
'
'----------------------------------------------------------------------
Imports System.Reflection
Imports System.Runtime.CompilerServices

'
' General Information about an Assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an Assembly.
'
<Assembly: AssemblyTitle("")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyConfiguration("")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: AssemblyCulture("")> 

'
' Version information for an Assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Revision and Build Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 

'
' In order to sign your Assembly you must specify a key to use. Refer to the 
' Microsoft .NET Framework documentation for more information on Assembly signing.
'
' Use the attributes below to control which key is used for signing. 
'
' Notes: 
'   (*) If no key is specified, the Assembly is not signed.
'   (*) KeyName refers to a key that has been installed in the Crypto Service
'       Provider (CSP) on your machine. KeyFile refers to a file which contains
'       a key.
'   (*) If the KeyFile and the KeyName values are both specified, the 
'       following processing occurs:
'       (1) If the KeyName can be found in the CSP, that key is used.
'       (2) If the KeyName does not exist and the KeyFile does exist, the key 
'           in the KeyFile is installed into the CSP and used.
'   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
'       When specifying the KeyFile, the location of the KeyFile should be
'       relative to the project output directory which is
'       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
'       located in the project directory, you would specify the AssemblyKeyFile 
'       attribute as <Assembly: AssemblyKeyFile("..\\..\\mykey.snk")>
'   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
'       documentation for more information on this.
'
<Assembly: AssemblyDelaySign(false)>
<Assembly: AssemblyKeyFile("")>
<Assembly: AssemblyKeyName("")>
