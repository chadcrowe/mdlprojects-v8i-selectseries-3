/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/KeyinCommands.cpp,v $
|    $RCSfile: KeyinCommands.cpp,v $
|   $Revision: 1.1 $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#using <Bentley.General.1.0.dll>
#using <mscorlib.dll>
#using <System.dll>
#using <System.Drawing.dll>
#using <System.Windows.Forms.dll>
#using <Bentley.MicroStation.dll>
#using <ustation.dll>
#using <Bentley.Windowing.dll>
#using <Bentley.MicroStation.Interfaces.1.0.dll>
#using <Microsoft.ApplicationBlocks.ExceptionManagement.1.0.dll>
#using <Bentley.Interop.MicroStationDGN.dll>
#using <Bentley.MicroStation.WinForms.Docking.dll>


namespace [!output SAFE_PROJECT_NAME]
{
//for future use
} //end namespace [!output SAFE_PROJECT_NAME]