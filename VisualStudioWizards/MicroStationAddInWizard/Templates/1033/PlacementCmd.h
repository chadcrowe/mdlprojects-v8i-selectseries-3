#pragma once
/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/PlacementCmd.h,v $
|    $RCSfile: PlacementCmd.h,v $
|   $Revision: 1.1 $
|       $Date: 2011/08/09 13:12:41 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

namespace [!output SAFE_PROJECT_NAME]
    {
    ref class [!output SAFE_PROJECT_NAME]PlacementCmd:public Bentley::Interop::MicroStationDGN::IPrimitiveCommandEvents
    {
   
    public:
        Bentley::Interop::MicroStationDGN::Application ^m_pApp;

        [!output SAFE_PROJECT_NAME]PlacementCmd(Bentley::MicroStation::AddIn ^pAddIn);

        virtual void DataPoint (Bentley::Interop::MicroStationDGN::Point3d %pPoint,Bentley::Interop::MicroStationDGN::View ^pView);
        virtual void Reset  ();
        virtual void Start ();
        virtual void Stop ();
        virtual void Cleanup();
        virtual void Keyin (System::String ^unparsedP);
        virtual void Dynamics (Bentley::Interop::MicroStationDGN::Point3d %pPoint,Bentley::Interop::MicroStationDGN::View ^pView,Bentley::Interop::MicroStationDGN::MsdDrawingMode drawMode);
        static void StartPrimitiveCommand(Bentley::MicroStation::AddIn ^pAddIn);
    public:
        ~[!output SAFE_PROJECT_NAME]PlacementCmd(void);
    private :
    };
    }
