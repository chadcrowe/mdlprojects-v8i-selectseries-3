'/*--------------------------------------------------------------------------------------+
'|
'|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/form.vb,v $
'|    $RCSfile: form.vb,v $
'|   $Revision: 1.1 $
'|
'|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
'|
'+--------------------------------------------------------------------------------------*/

Imports Bentley.MicroStation.Winforms
Imports Bentley.Interop.MicroStationDGN
Imports Bentley.MicroStation
Imports Bentley.Windowing
Imports System.Windows.Forms

Namespace [!output SAFE_PROJECT_NAME]

    Public Class [!output SAFE_PROJECT_NAME]form
  'comment out the winforms.adapter to allow this to load in designer mode.
        Inherits Bentley.MicroStation.WinForms.Adapter
        'Inherits System.Windows.Forms.Form
        Private Shared s_App As Bentley.Interop.MicroStationDGN.Application
        Private Shared s_Current As [!output SAFE_PROJECT_NAME]form

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call
            s_Current = Me
        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
            s_Current = Nothing
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.SuspendLayout()
            '
            '[!output SAFE_PROJECT_NAME]Form
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(292, 268)
            Me.Name = "[!output SAFE_PROJECT_NAME]Form"
            Me.Text = "[!output SAFE_PROJECT_NAME]Form"
            Me.ResumeLayout(False)

        End Sub
#End Region
       
    End Class
End Namespace