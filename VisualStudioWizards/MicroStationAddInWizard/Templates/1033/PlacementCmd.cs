/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/PlacementCmd.cs,v $
|    $RCSfile: PlacementCmd.cs,v $
|   $Revision: 1.1 $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#region "System Namespaces"
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using SRI=System.Runtime.InteropServices;
#endregion 

#region "Bentley Namespaces"
using BMW = Bentley.MicroStation.WinForms;
using BMI = Bentley.MicroStation.InteropServices;
using BCOM = Bentley.Interop.MicroStationDGN;
using BM = Bentley.MicroStation;
#endregion

namespace [!output SAFE_PROJECT_NAME]
{
	/// <summary>
	/// </summary>
	internal class [!output SAFE_PROJECT_NAME]PlacementCmd : BCOM.IPrimitiveCommandEvents
	{

		#region "Private Variables"

		// These will get used over and over, so just get them once.
		private Bentley.MicroStation.AddIn  m_AddIn;
		private BCOM.Application m_App;
		 [!if CHECKBOX_WINFORM]
		//the form....
		[!output SAFE_PROJECT_NAME]form myForm;
		[!endif]

		#endregion

		#region "Constructors"
		
		private [!output SAFE_PROJECT_NAME]PlacementCmd()
		{
			//  Make sure only the IDE uses this.
		}
		
		internal [!output SAFE_PROJECT_NAME]PlacementCmd(Bentley.MicroStation.AddIn addIn)
		{
			//Initialize class variables
			m_AddIn = addIn;
			m_App = [!output SAFE_PROJECT_NAME].ComApp;
			

			//  Set the controls to the values from active settings.
			BCOM.Settings settings = m_App.ActiveSettings;
    
			//  Initialize to active settings
		}
		
		#endregion

		/// <summary>
		/// Starts the primitive command to place the Route
		/// </summary>
		internal static void StartPlacementCommand (Bentley.MicroStation.AddIn addIn)
		{
			//These are needed because it is a static method
			Bentley.MicroStation.AddIn pAddIn = addIn;
			BCOM.Application pApp = [!output SAFE_PROJECT_NAME].ComApp;

			//Create a PlaceRouteCommand object
			[!output SAFE_PROJECT_NAME]PlacementCmd command = new [!output SAFE_PROJECT_NAME]PlacementCmd (pAddIn);

			BCOM.CommandState commandState = pApp.CommandState;
			

			pApp.CommandState.StartPrimitive (command, false);

			// Record the name that is saved in the Undo buffer and shown as the prompt.
			pApp.CommandState.CommandName = "Placement Command";

			//Optional Start Dynamics b/c we are ready to show elements 
			//pApp.CommandState.StartDynamics();
		}



		/* --------------------------------------------------------------
		 * The goal of this command is to:
		 * 
		 * ---------------------------------------------------------------------*/
		#region "IPrimitiveCommandEvents"
		public void Start()
		{
			//Instantiate the Form and Tell Microstation it is a tool settings
            [!if CHECKBOX_WINFORM]
			myForm = new [!output SAFE_PROJECT_NAME]form(m_AddIn);
			myForm.AttachToToolSettings (m_AddIn);
            [!endif]

			//Show Prompts etc.
			m_App.ShowCommand("Place First Point");
			m_App.ShowPrompt("Enter Point");

			//Enables Accusnap for this command if the user has it enabled in Microstation
			m_App.CommandState.EnableAccuSnap();

		}


		public void DataPoint(ref BCOM.Point3d Point, BCOM.View View)
		{

			/*--------------------------------------------------------
			 * ------------------------------------------------------*/
		}
	
		
		public void Keyin(string Keyin)
		{
			//Do Nothing
			;
		}

			
		public void Dynamics(ref BCOM.Point3d Point, BCOM.View View, BCOM.MsdDrawingMode DrawMode)
		{
			/*--------------------------------------------------------
			 *  
			 * -------------------------------------------------------*/
		}


		public void Reset()
		{
         [!if CHECKBOX_WINFORM]
            try
            {
                myForm.DetachFromMicroStation();
                myForm.Close();
                myForm.Dispose();
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("no window" + e.Message);
            }
        [!endif]
			m_App.CommandState.StartDefaultCommand();
		}


		public void Cleanup()
		{

         [!if CHECKBOX_WINFORM]
            try
            {
                myForm.DetachFromMicroStation();
                myForm.Dispose();
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message + "cleanup window failed");
            }

            myForm = null;
		[!endif]	
		}

		#endregion


	}
}