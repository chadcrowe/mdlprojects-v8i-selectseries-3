/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/KeyinCommands.cs,v $
|    $RCSfile: KeyinCommands.cs,v $
|   $Revision: 1.1 $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

using System;

using BCOM=Bentley.Interop.MicroStationDGN;
using BMI=Bentley.MicroStation.InteropServices;

namespace [!output SAFE_PROJECT_NAME]
{
/// <summary>Class used for running key-ins.  The key-ins
/// XML file commands.xml provides the class name and the method names.
/// </summary>
internal class KeyinCommands
{
    
public static void [!output SAFE_PROJECT_NAME]Command (System.String unparsed)
    {

    }

[!if CHECKBOX_PALCEMENT]
public static void [!output SAFE_PROJECT_NAME]PlacementCommand (System.String unparsed)
    {
      [!output SAFE_PROJECT_NAME]PlacementCmd.StartPlacementCommand ([!output SAFE_PROJECT_NAME].MyAddin); 
    }
[!endif]
    
}  // End of KeyinCommands

}  // End of the namespace
