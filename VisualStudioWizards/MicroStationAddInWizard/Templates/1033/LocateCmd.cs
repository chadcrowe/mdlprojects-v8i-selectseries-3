/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/LocateCmd.cs,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
using System;
#region "System Namespaces"
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using SRI=System.Runtime.InteropServices;
#endregion 

#region "Bentley Namespaces"
using BMW = Bentley.MicroStation.WinForms;
using BMI = Bentley.MicroStation.InteropServices;
using BCOM = Bentley.Interop.MicroStationDGN;
using BM = Bentley.MicroStation;
#endregion

namespace [!output SAFE_PROJECT_NAME]
{
    /// <summary>
    /// </summary>
    internal class [!output SAFE_PROJECT_NAME]LocateCmd : BCOM.ILocateCommandEvents
       {

        #region "private variables"
        // These will get used over and over, so just get them once.
        private Bentley.MicroStation.AddIn m_AddIn;
        private BCOM.Application m_App;
        [!if CHECKBOX_WINFORM]
        //the form....
        [!output SAFE_PROJECT_NAME]form myForm;
        [!endif]

        #endregion

        internal static void StartLocateCommand(BM.AddIn pAddIn)
        {
            //These are needed because it is a static method
            Bentley.MicroStation.AddIn m_AddIn = pAddIn;
			BCOM.Application m_App = [!output SAFE_PROJECT_NAME].ComApp;
            [!output SAFE_PROJECT_NAME]LocateCmd command = new [!output SAFE_PROJECT_NAME]LocateCmd(m_AddIn);
            BCOM.CommandState commandState = m_App.CommandState;
            commandState.StartLocate(command);
        }

        #region "constructors"
        
        private [!output SAFE_PROJECT_NAME]LocateCmd() { }
        public [!output SAFE_PROJECT_NAME]LocateCmd(BM.AddIn pAddIn)
        {
            m_App = [!output SAFE_PROJECT_NAME].ComApp;
            m_AddIn = pAddIn;
        }
        
        #endregion

        #region "ILocateCommandEvents"
        public void Start(){}
        public void Accept(BCOM.Element pElement, ref BCOM.Point3d pPoint, BCOM.View iView){}
        public void Cleanup (){}
        public void Dynamics(ref BCOM.Point3d pPoint,BCOM.View iView,BCOM.MsdDrawingMode drawMode) {}
        public void LocateFailed () {}
        public void LocateFilter (BCOM.Element pElement,ref BCOM.Point3d pPoint,ref bool accept){}
        public void LocateReset () { }
        #endregion ILocateCommandEvents
    }
}
