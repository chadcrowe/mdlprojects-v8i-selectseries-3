========================================================================
    MicroStationAddInWizard : "[!output PROJECT_NAME]" Project Overview
========================================================================

MicroStationAddInWizard has created this "[!output PROJECT_NAME]" project for you as a starting point.

This file contains a summary of what you will find in each of the files that make up your project.

[!output PROJECT_NAME] is the main application file this file will have an extension of cpp, cs, or
vb depending on the type of source code selected.

[!output PROJECT_NAME]form is a sample form sorce code page.

Keyincommands is the class for implementing the command methods.

commands.xml is the command table XML file.

[!output PROJECT_NAME].mke is the make file for building the application in the MicroStationV8 XM Edition
Developer shell.

/////////////////////////////////////////////////////////////////////////////
Other notes:

/////////////////////////////////////////////////////////////////////////////
