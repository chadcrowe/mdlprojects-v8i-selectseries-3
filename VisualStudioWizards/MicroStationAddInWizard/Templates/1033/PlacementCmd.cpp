/*--------------------------------------------------------------------------------------+
|
|    $RCSfile: PlacementCmd.cpp,v $
|   $Revision: 1.1 $
|       $Date: 2011/08/09 13:12:41 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

#include "[!output SAFE_PROJECT_NAME]PlacementCmd.h"

namespace BCOM=Bentley::Interop::MicroStationDGN;
namespace BM=Bentley::MicroStation;
namespace BMI=Bentley::MicroStation::InteropServices;

namespace [!output SAFE_PROJECT_NAME]
{

    [!output SAFE_PROJECT_NAME]PlacementCmd::[!output SAFE_PROJECT_NAME]PlacementCmd(Bentley::MicroStation::AddIn ^pAddIn )
        {
            m_pApp = Bentley::MicroStation::InteropServices::Utilities::ComApp;
        }
    [!output SAFE_PROJECT_NAME]PlacementCmd::~[!output SAFE_PROJECT_NAME]PlacementCmd(void){}

    void [!output SAFE_PROJECT_NAME]PlacementCmd::Start(){}
    void [!output SAFE_PROJECT_NAME]PlacementCmd::Stop(){}
    void [!output SAFE_PROJECT_NAME]PlacementCmd::Reset(){}
    void [!output SAFE_PROJECT_NAME]PlacementCmd::Cleanup(){}
    void [!output SAFE_PROJECT_NAME]PlacementCmd::DataPoint(BCOM::Point3d %pPoint,BCOM::View ^pView){}
    void [!output SAFE_PROJECT_NAME]PlacementCmd::Keyin(System::String ^unparsedP){};
    void [!output SAFE_PROJECT_NAME]PlacementCmd::Dynamics (BCOM::Point3d %pPoint, BCOM::View ^pView, BCOM::MsdDrawingMode drawMode){}
    void [!output SAFE_PROJECT_NAME]PlacementCmd::StartPrimitiveCommand(Bentley::MicroStation::AddIn  ^pAddIn)
                {
                BCOM::Application ^pApp = Bentley::MicroStation::InteropServices::Utilities::ComApp;
                [!output SAFE_PROJECT_NAME]PlacementCmd    ^command = gcnew [!output SAFE_PROJECT_NAME]PlacementCmd(pAddIn);
                pApp->CommandState->StartPrimitive (command,false);
                
                }

}
