'/*--------------------------------------------------------------------------------------+
'|
'|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/LocateCmd.vb,v $
'|    $RCSfile: LocateCmd.vb,v $
'|   $Revision: 1.1 $
'|       $Date: 2011/08/09 13:12:41 $
'|
'|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
'|
'+--------------------------------------------------------------------------------------*/
Imports BMW = Bentley.MicroStation.WinForms
Imports BMI = Bentley.MicroStation.InteropServices
Imports BCOM = Bentley.Interop.MicroStationDGN
Imports SRI = System.Runtime.InteropServices
Imports SysDbg = System.Diagnostics.Debug
Imports System
Imports BM = Bentley.MicroStation

Namespace [!output SAFE_PROJECT_NAME]
    Public Class [!output SAFE_PROJECT_NAME]LocateCmd
        Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents

#region "Private Variables"

		'// These will get used over and over, so just get them once.
        Private m_AddIn As BM.AddIn
        Private m_App As BCOM.Application
		 [!if CHECKBOX_WINFORM]
		'//the form....
		dim myForm as [!output SAFE_PROJECT_NAME]form
		[!endif]

#end region

    Public Shared Sub StartLocateCommand(ByRef pAddIn As Bentley.MicroStation.AddIn)
        Dim pApp As Bentley.Interop.MicroStationDGN.Application
        pApp = [!output SAFE_PROJECT_NAME].GetComApp
        Dim command As [!output SAFE_PROJECT_NAME]LocateCmd
        command = New [!output SAFE_PROJECT_NAME]LocateCmd(pAddIn)
        pApp.CommandState.StartLocate(command)
    End Sub
#region "Constructors"
		
        Private Sub New()
            '//  Make sure only the IDE uses this.
        End Sub

        Sub New(ByRef addIn As Bentley.MicroStation.AddIn)

            '//Initialize class variables
            m_AddIn = addIn
            m_App = [!output SAFE_PROJECT_NAME].GetComApp
        End Sub		
#end region 

    Public Sub Accept(ByVal Element As Bentley.Interop.MicroStationDGN.Element, ByRef Point As Bentley.Interop.MicroStationDGN.Point3d, ByVal View As Bentley.Interop.MicroStationDGN.View) _
    Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents.Accept

    End Sub

    Public Sub Cleanup() _ 
    Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents.Cleanup
        'unload form here
         [!if CHECKBOX_WINFORM]
            Try
                myForm.DetachFromMicroStation()
                myForm.Dispose()
            Catch e As NullReferenceException
                SysDbg.Write(e.Message + "cleanup window failed")
            End Try
            myForm = Nothing
        [!endif]	
    End Sub

    Public Sub Dynamics(ByRef Point As Bentley.Interop.MicroStationDGN.Point3d, ByVal View As Bentley.Interop.MicroStationDGN.View, ByVal DrawMode As Bentley.Interop.MicroStationDGN.MsdDrawingMode) _ 
    Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents.Dynamics

    End Sub

    Public Sub LocateFailed() _ 
    Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents.LocateFailed

    End Sub

    Public Sub LocateFilter(ByVal Element As Bentley.Interop.MicroStationDGN.Element, ByRef Point As Bentley.Interop.MicroStationDGN.Point3d, ByRef Accepted As Boolean) _ 
    Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents.LocateFilter

    End Sub

    Public Sub LocateReset() _ 
    Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents.LocateReset

    End Sub

    Public Sub Start() _
    Implements Bentley.Interop.MicroStationDGN.ILocateCommandEvents.Start
        'open form here
'//Instantiate the Form and Tell Microstation it is a tool settings
            [!if CHECKBOX_WINFORM]
			set myForm = new [!output SAFE_PROJECT_NAME]form()
			myForm.AttachToToolSettings (m_AddIn)
            [!endif]    
    End Sub
End Class
end namespace 