/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/form.h,v $
|    $RCSfile: form.h,v $
|   $Revision: 1.1 $
|       $Date: 2011/08/09 13:12:41 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#pragma once
#using <mscorlib.dll>
#using <System.Windows.Forms.dll>
#using <System.Drawing.dll>
#using <System.dll>
#using <ustation.dll>
#using <Bentley.MicroStation.dll>
#using <Bentley.Interop.MicroStationDGN.dll>
#using <Bentley.PropertyManager.dll>
#using <Bentley.Windowing.dll>

using System::Windows::Forms::Form;
using System::Console;
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Drawing;

namespace [!output SAFE_PROJECT_NAME]
{
/// <summary>
/// Summary for SampleDialog
///
/// WARNING: If you change the name of this class, you will need to change the
///          'Resource File Name' property for the managed resource compiler tool
///          associated with all .resx files this class depends on.  Otherwise,
///          the designers will not be able to interact properly with localized
///          resources associated with this form.
/// </summary>
/*=================================================================================**//**
* this is the class that derives from the Bentley::MicroStation::WinForms::Adapter base class.  
* this will allow the dialog to be controlled within the MicroStation windowing environment.
* @bsiclass                                                     Mark.Anderson   12/2004
+===============+===============+===============+===============+===============+======*/
    public ref class [!output SAFE_PROJECT_NAME]form : public Bentley::MicroStation::WinForms::Adapter
    {
    public:
/*---------------------------------------------------------------------------------**//**
* This method will initialize the dialog and is automatically generated by the form designer.
* @bsimethod                                                    Mark.Anderson    07/04
+---------------+---------------+---------------+---------------+---------------+------*/
        [!output SAFE_PROJECT_NAME]form(void)
        {
            InitializeComponent();
        }


    protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~[!output SAFE_PROJECT_NAME]form()
		{
			if (components)
			{
				delete components;
			}
		}

    private:

/// <summary>
/// Required designer variable.
/// </summary>
        System::ComponentModel::Container^ components;

/// <summary>
/// Required method for Designer support - do not modify
/// the contents of this method with the code editor.
/// </summary>
/*---------------------------------------------------------------------------------**//**
* This method will initialize the components on the dialog,  This method is automatically
* generated by the form designer.
* @bsimethod                                                    Mark.Anderson    07/04
+---------------+---------------+---------------+---------------+---------------+------*/
        void InitializeComponent(void)
        {
            this->SuspendLayout();
//
// SampleDialog
//
            this->AutoScaleBaseSize = System::Drawing::Size(5, 13);
            this->ClientSize = System::Drawing::Size(292, 266);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
            this->Name = L"[!output SAFE_PROJECT_NAME]";
            this->RightToLeft = System::Windows::Forms::RightToLeft::No;
            this->ShowInTaskbar = false;
            this->Text = L"[!output SAFE_PROJECT_NAME]";
            this->TopMost = true;
            this->ResumeLayout(false);
            //this->OnHostDockedExtentEvent += new Bentley::MicroStation::WinForms::Adapter::OnHostDockedExtentEventHandler (this,SampleDialog::DockedExtentCallback);

        }
    };
}