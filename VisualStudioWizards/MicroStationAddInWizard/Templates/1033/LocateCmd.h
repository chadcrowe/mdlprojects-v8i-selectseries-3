/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/LocateCmd.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#pragma once


namespace BCOM=Bentley::Interop::MicroStationDGN;
namespace BM=Bentley::MicroStation;
namespace [!output SAFE_PROJECT_NAME]
    {
public ref class [!output SAFE_PROJECT_NAME]LocateCmd:public BCOM::ILocateCommandEvents
    {
    public:
        [!output SAFE_PROJECT_NAME]LocateCmd(BM::AddIn ^pAddIn);
        virtual void Accept(BCOM::Element ^pElement,BCOM::Point3d %pPoint, BCOM::View ^pView);
        virtual void LocateFailed (void);
        virtual void LocateFilter (BCOM::Element ^pElement,BCOM::Point3d   %pPoint, bool %accept);
        virtual void LocateReset (void);
        virtual void Cleanup (void);
        virtual void Start (void);
        virtual void Dynamics (BCOM::Point3d %pPoint, BCOM::View ^pView,BCOM::MsdDrawingMode drawMode);
        static void StartLocateCommand (BM::AddIn ^pAddIn);
    public:
        ~[!output SAFE_PROJECT_NAME]LocateCmd(void);
    };
    }