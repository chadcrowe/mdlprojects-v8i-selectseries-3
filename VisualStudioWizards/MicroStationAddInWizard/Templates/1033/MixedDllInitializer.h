/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/MixedDllInitializer.h,v $
|    $RCSfile: MixedDllInitializer.h,v $
|   $Revision: 1.1 $
|       $Date: 2011/08/09 13:12:41 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#pragma once

#pragma unmanaged
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#pragma managed

#include <_vcclrit.h>

#using <mscorlib.dll>

#if defined (INITIALIZER_ClassVisibilityPublic)
    #define INITIALIZER_ClassVisibility public
#else
    #define INITIALIZER_ClassVisibility
#endif

#if !defined (INITIALIZER_ClassName)
    #define INITIALIZER_ClassName MixedDllInitializer
#endif

#if !defined (INITIALIZER_NamespaceBegin)
    #define INITIALIZER_NamespaceBegin namespace BSI {
#endif

#if !defined (INITIALIZER_NamespaceEnd)
    #define INITIALIZER_NamespaceEnd }
#endif

INITIALIZER_NamespaceBegin
    /*=================================================================================**//**
    * @bsiclass                                                     BernMcCarty    10/04

    Enables manual invocation of the CRT code that initializes the static/global data within
    a "mixed .dll". 

    This is to work around the infamous "Mixed DLL Loading Problem"  of .NET 1.x.  Web search
    that phrase for more information.  This problem is fixed in .NET 2.x, but all 
    .NET 1.x Mixed DLLs must apply this workaround.  The workaround involves finding a way
    to ensure that the below minitialize method is invoked before the assembly is otherwise
    used within the appdomain.  Technically, their should be balanced calls to minitialize,
    although it is unclear how necessary that really is in many cases.
    +===============+===============+===============+===============+===============+======*/
    INITIALIZER_ClassVisibility __gc class INITIALIZER_ClassName
        {
        public: static BOOL Initialize()
            {
            BOOL retval = TRUE;
            try
                {
                retval =  __crt_dll_initialize();
                }
            catch(System::Exception* e)
                {
                System::Console::WriteLine(e->Message);
                retval = FALSE;
                }
            return retval;
            }

        public: static BOOL Terminate()
            {
            BOOL retval = TRUE;
            try
                {
                retval = __crt_dll_terminate();
                }
            catch(System::Exception* e)
                {
                System::Console::WriteLine(e->Message);
                retval = FALSE;
                }
            return retval;
            }
        };
INITIALIZER_NamespaceEnd

