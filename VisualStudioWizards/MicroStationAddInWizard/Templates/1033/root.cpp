/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/root.cpp,v $
|    $RCSfile: root.cpp,v $
|   $Revision: 1.1 $
|       $Date: 2011/08/09 13:12:41 $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

#include "StdAfx.h"

#using <Bentley.General.1.0.dll>
#using <mscorlib.dll>
#using <System.dll>
#using <System.Drawing.dll>
#using <System.Windows.Forms.dll>
#using <Bentley.MicroStation.dll>
#using <ustation.dll>
#using <Bentley.Windowing.dll>
#using <Bentley.MicroStation.Interfaces.1.0.dll>
#using <Microsoft.ApplicationBlocks.ExceptionManagement.1.0.dll>
#using <Bentley.Interop.MicroStationDGN.dll>
#using <Bentley.MicroStation.WinForms.Docking.dll>
[!if CHECKBOX_WINFORM]
#include "[!output SAFE_PROJECT_NAME]form.h"
[!endif]
[!if CHECKBOX_PLACEMENT]
#include "[!output SAFE_PROJECT_NAME]PlacementCmd.h"
[!endif]
[!if CHECKBOX_LOCATE]
#include "[!output SAFE_PROJECT_NAME]LocateCmd.h"
[!endif]
using namespace System::Runtime::InteropServices;
namespace BIM = Bentley::Interop::MicroStationDGN;
namespace SRI = System::Runtime::InteropServices;

namespace [!output SAFE_PROJECT_NAME]
    {
[!if CHECKBOX_KEYINS]
    [Bentley::MicroStation::AddInAttribute(KeyinTree = L"[!output SAFE_PROJECT_NAME].commands.xml", MdlTaskID=L"[!output SAFE_PROJECT_NAME]")]
[!else]
    [Bentley::MicroStation::AddInAttribute(MdlTaskID=L"[!output SAFE_PROJECT_NAME]")]
[!endif]
/*=================================================================================**//**
* this is the class that derives from the AddIn base class.  this will allow the
* application to load through the MDL loader.
* @bsiclass                                                     
+===============+===============+===============+===============+===============+======*/
    public ref class [!output SAFE_PROJECT_NAME] : public Bentley::MicroStation::AddIn
    {
        public:static
            Bentley::Interop::MicroStationDGN::Application       ^m_COMApp;

        internal:static Bentley::MicroStation::AddIn       ^s_App;
/*---------------------------------------------------------------------------------**//**
* the constructor for the AddIn subclass.  this is where applications need to load any
* resources that will be used in the code such as commandtables.
* @bsimethod                                                    
+---------------+---------------+---------------+---------------+---------------+------*/
        public: [!output SAFE_PROJECT_NAME](System::IntPtr mdlDesc):  Bentley::MicroStation::AddIn(mdlDesc)
        {
           m_COMApp = Bentley::MicroStation::InteropServices::Utilities::ComApp;          
           s_App = this;
        } 

/*---------------------------------------------------------------------------------**//**
* A static method used for getting a reference to the com host application
* @bsimethod                                                   
+---------------+---------------+---------------+---------------+---------------+------*/
        static Bentley::Interop::MicroStationDGN::Application  ^GetCOMApp () 
                {
                return m_COMApp;
                }

/*---------------------------------------------------------------------------------**//**
* A static method used for getting a reference to the host addin.
* @bsimethod                                                    
+---------------+---------------+---------------+---------------+---------------+------*/
       static Bentley::MicroStation::AddIn  ^GetApp () 
                {
                return s_App;
                }
/*---------------------------------------------------------------------------------**//**
* this is the public entry point for the underlying application
* @bsimethod                                                    
+---------------+---------------+---------------+---------------+---------------+------*/
       public: virtual int Run (array<System::String^>^ commandLine ) override
        {
            [!output SAFE_PROJECT_NAME]::s_App = this;
            return 0;
        } 
    };
[!if CHECKBOX_KEYINS]

/*=================================================================================**//**
* This class handles the Keyins from the user.  It then invokes the methods as needed
* in the CommandHandler class.
* @bsiclass                                                     
+===============+===============+===============+===============+===============+======*/

    public ref class KeyinCommands
    {

/*---------------------------------------------------------------------------------**//**
* The method to handle the keyin " Open".  If the dialog is not
* open it will be opened.
* @bsimethod                                                    
+---------------+---------------+---------------+---------------+---------------+------*/
        public:static void [!output SAFE_PROJECT_NAME]Command (System::String^ unparsed)
        {
               Bentley::Interop::MicroStationDGN::Application^ comMstn = [!output SAFE_PROJECT_NAME]::m_COMApp;
        }
[!if CHECKBOX_PLACEMENT]
/*---------------------------------------------------------------------------------**//**
* The method to handle the keyin "PlaceCmd".  If the dialog is not
* open it will be opened.
* @bsimethod                                                    
+---------------+---------------+---------------+---------------+---------------+------*/
        static void [!output SAFE_PROJECT_NAME]PrimitiveCommand (System::String^ unparsed)
        {
        [!output SAFE_PROJECT_NAME]PlacementCmd::StartPrimitiveCommand([!output SAFE_PROJECT_NAME]::GetApp());
        }
[!endif]

[!if CHECKBOX_LOCATE]
/*---------------------------------------------------------------------------------**//**
* The method to handle the keyin "LocateCmd".  If the dialog is not
* open it will be opened.
* @bsimethod                                                        
+---------------+---------------+---------------+---------------+---------------+------*/
        static void [!output SAFE_PROJECT_NAME]LocateCommand (System::String^ unparsed)
        {
        [!output SAFE_PROJECT_NAME]LocateCmd::StartLocateCommand ([!output SAFE_PROJECT_NAME]::GetApp());
        }
[!endif]
[!endif]
     };
}//end namespace [!output SAFE_PROJECT_NAME] 
