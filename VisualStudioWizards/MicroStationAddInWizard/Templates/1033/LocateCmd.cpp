/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/LocateCmd.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#using <Bentley.General.1.0.dll>
#using <mscorlib.dll>
#using <System.dll>
#using <System.Drawing.dll>
#using <System.Windows.Forms.dll>
#using <Bentley.MicroStation.dll>
#using <ustation.dll>
#using <Bentley.Windowing.dll>
#using <Bentley.MicroStation.Interfaces.1.0.dll>
#using <Microsoft.ApplicationBlocks.ExceptionManagement.1.0.dll>
#using <Bentley.Interop.MicroStationDGN.dll>
#using <Bentley.MicroStation.WinForms.Docking.dll>
#include "[!output SAFE_PROJECT_NAME]LocateCmd.h"

//using namespace BCOM=Bentley::Interop::MicroStationDGN;
//using namespace BM=Bentley::MicroStation;

namespace [!output SAFE_PROJECT_NAME]
    {

[!output SAFE_PROJECT_NAME]LocateCmd::[!output SAFE_PROJECT_NAME]LocateCmd(BM::AddIn  ^pAddin)
    {
    }

[!output SAFE_PROJECT_NAME]LocateCmd::~[!output SAFE_PROJECT_NAME]LocateCmd(void)
    {
    }
void [!output SAFE_PROJECT_NAME]LocateCmd::Accept (Bentley::Interop::MicroStationDGN::Element ^pElement,Bentley::Interop::MicroStationDGN::Point3d %pPoint,Bentley::Interop::MicroStationDGN::View ^pView){}
void [!output SAFE_PROJECT_NAME]LocateCmd::LocateFailed (void){}
void [!output SAFE_PROJECT_NAME]LocateCmd::LocateFilter (BCOM::Element ^pElement,BCOM::Point3d   %pPoint, bool %accept){}
void [!output SAFE_PROJECT_NAME]LocateCmd::LocateReset (void){}
void [!output SAFE_PROJECT_NAME]LocateCmd::Cleanup (void){}
void [!output SAFE_PROJECT_NAME]LocateCmd::Start (void){}
void [!output SAFE_PROJECT_NAME]LocateCmd::Dynamics (BCOM::Point3d %pPoint, BCOM::View ^pView,BCOM::MsdDrawingMode drawMode){}
void [!output SAFE_PROJECT_NAME]LocateCmd::StartLocateCommand (BM::AddIn ^pAddIn)
    {
    BCOM::Application ^pApp = Bentley::MicroStation::InteropServices::Utilities::ComApp;
    [!output SAFE_PROJECT_NAME]LocateCmd   ^command = gcnew [!output SAFE_PROJECT_NAME]LocateCmd(pAddIn);
    pApp->CommandState->StartLocate (command);
    }
    }