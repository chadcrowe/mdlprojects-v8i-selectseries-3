'/*--------------------------------------------------------------------------------------+
'|
'|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/root.vb,v $
'|    $RCSfile: root.vb,v $
'|   $Revision: 1.1 $
'|       $Date: 2011/08/09 13:12:41 $
'|
'|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
'|
'+--------------------------------------------------------------------------------------*/
Imports Bentley.MicroStation.WinForms
Imports Bentley.MicroStation.InteropServices
Imports Bentley.Interop.MicroStationDGN
Imports System.Runtime.InteropServices
Imports Bentley.MicroStation

Namespace [!output SAFE_PROJECT_NAME]
[!if CHECKBOX_KEYINS]
    <Bentley.MicroStation.AddInAttribute(KeyinTree:="[!output SAFE_PROJECT_NAME].commands.xml", MdlTaskId:="[!output SAFE_PROJECT_NAME]")> _
[!else]
    <Bentley.MicroStation.AddInAttribute(MdlTaskId:="ProjReviewVB")> _
[!endif]
    Public Class [!output SAFE_PROJECT_NAME]
        Inherits Bentley.MicroStation.AddIn
        Private Shared s_App As [!output SAFE_PROJECT_NAME]

        Sub New(ByVal mdlDesc As System.IntPtr)
            MyBase.New(mdlDesc)
            s_App = Me
        End Sub
'/*-------------------------------------------------------------------------------------+
'|
'|     Function to get the addin application
'|
'|
'+--------------------------------------------------------------------------------------*/
        Public Shared Function GetApp() As [!output SAFE_PROJECT_NAME]
            GetApp = s_App
        End Function
'/*-------------------------------------------------------------------------------------+
'|
'|     Function to get the COM application hosting this application
'|
'|
'+--------------------------------------------------------------------------------------*/
        Public Shared Function GetComApp() As Bentley.Interop.MicroStationDGN.Application
            GetComApp = Bentley.MicroStation.InteropServices.Utilities.ComApp
        End Function
'/*-------------------------------------------------------------------------------------+
'|
'|     The run entry point for this addin
'|
'|
'+--------------------------------------------------------------------------------------*/
        Protected Overrides Function run(ByVal cmdLine() As String) As Integer
            run = 0
        End Function
'/*-------------------------------------------------------------------------------------+
'|
'|     The main entry point for this addin necessary for compiler
'|
'|
'+--------------------------------------------------------------------------------------*/
  Public Shared Sub main()
            'only here as a place holder for compiling within IDE.
        End Sub
    End Class
End Namespace