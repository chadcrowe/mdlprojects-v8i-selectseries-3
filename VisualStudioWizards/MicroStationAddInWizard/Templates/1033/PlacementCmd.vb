'/*--------------------------------------------------------------------------------------+
'|
'|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/PlacementCmd.vb,v $
'|    $RCSfile: PlacementCmd.vb,v $
'|   $Revision: 1.1 $
'|       $Date: 2011/08/09 13:12:41 $
'|
'|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
'|
'+--------------------------------------------------------------------------------------*/
Imports BMW = Bentley.MicroStation.WinForms
Imports BMI = Bentley.MicroStation.InteropServices
Imports BCOM = Bentley.Interop.MicroStationDGN
Imports SRI = System.Runtime.InteropServices
Imports SysDbg = System.Diagnostics.Debug
Imports System
Imports BM = Bentley.MicroStation

Namespace [!output SAFE_PROJECT_NAME]
    Public Class [!output SAFE_PROJECT_NAME]PlacementCmd
        Implements Bentley.Interop.MicroStationDGN.IPrimitiveCommandEvents

#region "Private Variables"

		'// These will get used over and over, so just get them once.
        Private m_AddIn As BM.AddIn
        Private m_App As BCOM.Application
		 [!if CHECKBOX_WINFORM]
		'//the form....
		dim myForm as [!output SAFE_PROJECT_NAME]form
		[!endif]

#end region

#region "Constructors"
		
        Private Sub New()
            '//  Make sure only the IDE uses this.
        End Sub

        Sub New(ByRef addIn As Bentley.MicroStation.AddIn)

            '//Initialize class variables
            m_AddIn = addIn
            m_App = [!output SAFE_PROJECT_NAME].GetComApp
        End Sub		
#end region 
        Public Shared Sub StartPlacementCommand(ByRef addIn As Bentley.MicroStation.AddIn)
            '//These are needed because it is a static method
            Dim mAddIn As BM.AddIn
            mAddIn = addIn
            Dim app As BCOM.Application

            app = [!output SAFE_PROJECT_NAME].GetComApp

            '//Create a Command object
            Dim command As [!output SAFE_PROJECT_NAME]PlacementCmd
            command = New [!output SAFE_PROJECT_NAME]PlacementCmd(mAddIn)

            Dim commandState As BCOM.CommandState
            commandState = app.CommandState


            app.CommandState.StartPrimitive(command, False)

            '// Record the name that is saved in the Undo buffer and shown as the prompt.
            app.CommandState.CommandName = "Placement Command"

            '//Optional Start Dynamics b/c we are ready to show elements 
            '//app.CommandState.StartDynamics()
        End Sub
		'/* --------------------------------------------------------------
		' * The goal of this command is to:
		' * 
		' * ---------------------------------------------------------------------*/
		#region "IPrimitiveCommandEvents"
        Public Sub Start() _
        Implements Bentley.Interop.MicroStationDGN.IPrimitiveCommandEvents.Start
            '//Instantiate the Form and Tell Microstation it is a tool settings
            myForm = New [!output SAFE_PROJECT_NAME]form
            myForm.AttachToToolSettings(m_AddIn)

            '//Show Prompts etc.
            m_App.ShowCommand("Place First Point")
            m_App.ShowPrompt("Enter Point")

            '//Enables Accusnap for this command if the user has it enabled in Microstation
            m_App.CommandState.EnableAccuSnap()

        End Sub

        Public Sub DataPoint(ByRef Point As BCOM.Point3d, ByVal View As BCOM.View) _
        Implements Bentley.Interop.MicroStationDGN.IPrimitiveCommandEvents.DataPoint

            '/*--------------------------------------------------------
            ' * ------------------------------------------------------*/
        End Sub


        Public Sub Keyin(ByVal Keyin As String) _
        Implements Bentley.Interop.MicroStationDGN.IPrimitiveCommandEvents.Keyin
            '//Do Nothing

        End Sub


        Public Sub Dynamics(ByRef Point As BCOM.Point3d, ByVal View As BCOM.View, ByVal DrawMode As BCOM.MsdDrawingMode) _
            Implements Bentley.Interop.MicroStationDGN.IPrimitiveCommandEvents.Dynamics
            '/*--------------------------------------------------------
            ' *  
            ' * -------------------------------------------------------*/
        End Sub

        Public Sub Reset() Implements Bentley.Interop.MicroStationDGN.IPrimitiveCommandEvents.Reset
         [!if CHECKBOX_WINFORM]
            Try

                myForm.DetachFromMicroStation()
                myForm.Close()
                myForm.Dispose()

            Catch e As NullReferenceException

                SysDbg.Write("no window" + e.Message)
            End Try
         [!endif]
            m_App.CommandState.StartDefaultCommand()
        End Sub


        Sub Cleanup() Implements Bentley.Interop.MicroStationDGN.IPrimitiveCommandEvents.Cleanup
        [!if CHECKBOX_WINFORM]
            Try
                myForm.DetachFromMicroStation()
                myForm.Dispose()
            Catch e As NullReferenceException
                SysDbg.Write(e.Message + "cleanup window failed")
            End Try
            myForm = Nothing
        [!endif]	
        End Sub

		#end region

    End Class
End Namespace