#pragma once
/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MicroStationAddInWizard/Templates/1033/root.h,v $
|    $RCSfile: root.h,v $
|   $Revision: 1.1 $
|       $Date: 2011/08/09 13:12:41 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2011 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

class [!output SAFE_PROJECT_NAME]
{
public:
    [!output SAFE_PROJECT_NAME](void);
    ~[!output SAFE_PROJECT_NAME](void);
};
