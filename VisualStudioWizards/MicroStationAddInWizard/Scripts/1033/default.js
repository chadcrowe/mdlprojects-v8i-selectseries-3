
function OnFinish(selProj, selObj)
{
	try
	{
		var strProjectPath = wizard.FindSymbol('PROJECT_PATH');
		var strProjectName = wizard.FindSymbol('PROJECT_NAME');
		var strSafeProjectName = CreateSafeName (strProjectName);
		wizard.AddSymbol ("SAFE_PROJECT_NAME", strSafeProjectName);
		var strSafeProjectNameUC  = strSafeProjectName.toUpperCase();
		wizard.AddSymbol ("SAFE_PROJECT_NAME_UPPERCASE",strSafeProjectNameUC);

		var strProjectUsesCPP = wizard.FindSymbol ('RADIO_LANGUAGE_CPP');
		var strProjectUsesCSharp = wizard.FindSymbol ('RADIO_LANGUAGE_CSHARP');
		var strProjectUsesVBNet = wizard.FindSymbol ('RADIO_LANGUAGE_VBNET');
	
		var strKeyinsUsed = wizard.FindSymbol ('CHECKBOX_KEYINS');
		var strWinformsUsed = wizard.FindSymbol ('CHECKBOX_WINFORM');
		
		var strPlacementCmdUsed = wizard.FindSymbol ('CHECKBOX_PLACEMENT');
		var strLocateCmdUsed = wizard.FindSymbol ('CHECKBOX_LOCATE');

		selProj = CreateCustomProject(strProjectName, strProjectPath);
		if (wizard.FindSymbol ('RADIO_LANGUAGE_CPP') )
		{
		    AddConfig(selProj, strProjectName);
		    AddFilters(selProj);
		//SetupFilters (selProj);
		}
		else
		    AddFolders (selProj);
		
		
//debugger;
		var InfFile = CreateCustomInfFile();
		AddFilesToCustomProj(selProj, strProjectName, strProjectPath, InfFile);
		PchSettings(selProj);
		InfFile.Delete();
		
        if (wizard.FindSymbol ('RADIO_LANGUAGE_CPP') )
		    selProj.Object.Save();
        else   
            selProj.Save();
	}
	catch(e)
	{
		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function CreateCustomProject(strProjectName, strProjectPath)
{
	try
	{
	var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');
	
		var strProjTemplatePath = wizard.FindSymbol('PROJECT_TEMPLATE_PATH');
		var strProjTemplate = '';
//debugger;
		//what is being created that will determine the default project to use:
		if (wizard.FindSymbol ('RADIO_LANGUAGE_CPP'))
		    strProjTemplate = strTemplatePath + '\\default.vcproj';
		    
        if (wizard.FindSymbol ('RADIO_LANGUAGE_CSHARP'))
            strProjTemplate = strTemplatePath + '\\default.csproj';
            
        if (wizard.FindSymbol ('RADIO_LANGUAGE_VBNET'))
            strProjTemplate = strTemplatePath + '\\default.vbproj';
        
		var Solution = dte.Solution;
		var strSolutionName = "";
		if (wizard.FindSymbol("CLOSE_SOLUTION"))
		{
			Solution.Close();
			strSolutionName = wizard.FindSymbol("VS_SOLUTION_NAME");
			if (strSolutionName.length)
			{
				var strSolutionPath = strProjectPath.substr(0, strProjectPath.length - strProjectName.length);
				Solution.Create(strSolutionPath, strSolutionName);
			}
		}

		var strProjectNameWithExt = '';
		
		if (wizard.FindSymbol ('RADIO_LANGUAGE_CPP') )
		    strProjectNameWithExt = strProjectName + '.vcproj';
		    
        if (wizard.FindSymbol ('RADIO_LANGUAGE_CSHARP'))
            strProjectNameWithExt = strProjectName + '.csproj';
            
        if (wizard.FindSymbol ('RADIO_LANGUAGE_VBNET'))
            strProjectNameWithExt = strProjectName + '.vbproj';
            
		var oTarget = wizard.FindSymbol("TARGET");
		
		var prj;
		
		//wizard.RenderTemplate(strTemplate, strProjectNameWithExt, true);

		if (wizard.FindSymbol("WIZARD_TYPE") == vsWizardAddSubProject)  // vsWizardAddSubProject
		{
			var prjItem = oTarget.AddFromTemplate(strProjTemplate, strProjectNameWithExt);
			prj = prjItem.SubProject;
		}
		else
		{
			prj = oTarget.AddFromTemplate(strProjTemplate, strProjectPath, strProjectNameWithExt);
		}
	
		return prj;
	}
	catch(e)
	{
		throw e;
	}
}
/*****************************************************************************
  The following section contains functions that are used by CSharp Projects
  and CSharp Additems. If you like to add a new function that is CSharp
  specific, please add it beyond this point of this file.

                            - CSHARP SECTION -
******************************************************************************/

/******************************************************************************
     Description: Creates a C# project
  strProjectName: Project Name
  strProjectPath: The path that the project will be created in
 strTemplateFile: Project template file e.g. "defualt.csproj"
******************************************************************************/
function CreateCSharpProject(strProjectName, strProjectPath, strTemplateFile)
{
	try
	{
		// Make sure user sees ui.
		dte.SuppressUI = false;
		var strProjTemplatePath = wizard.FindSymbol("PROJECT_TEMPLATE_PATH") + "\\";
		var strProjTemplate = strProjTemplatePath + strTemplateFile; 
		var Solution = dte.Solution;
		var strSolutionName = "";
		if (wizard.FindSymbol("CLOSE_SOLUTION"))
		{
			Solution.Close();
			strSolutionName = wizard.FindSymbol("VS_SOLUTION_NAME");
			if (strSolutionName.length)
			{

				var strSolutionPath = strProjectPath.substr(0, strProjectPath.length - strProjectName.length);
				Solution.Create(strSolutionPath, strSolutionName);
			}
		}

		strProjectNameWithExt = strProjectName + ".csproj";

		var oTarget = wizard.FindSymbol("TARGET");
		var oPrj;
//debugger;
//	wizard.RenderTemplate(strTemplate, strProjectNameWithExt, true);

		if (wizard.FindSymbol("WIZARD_TYPE") == vsWizardAddSubProject)  // vsWizardAddSubProject
		{
            var nPos = strProjectPath.search("http://");
            var prjItem
            if(nPos == 0)
                prjItem = oTarget.AddFromTemplate(strProjTemplate, strProjectPath + "/" + strProjectNameWithExt);    
            else
			    prjItem = oTarget.AddFromTemplate(strProjTemplate, strProjectPath + "\\" + strProjectNameWithExt);
			oPrj = prjItem.SubProject;
		}
		else
		{
			oPrj = oTarget.AddFromTemplate(strProjTemplate, strProjectPath, strProjectNameWithExt);
		}
		var strNameSpace = "";
		strNameSpace = oPrj.Properties("RootNamespace").Value;
		wizard.AddSymbol("SAFE_NAMESPACE_NAME",  strNameSpace);

		return oPrj;
	}
	catch(e)
	{
		// propagate all errors back to the caller
		throw e;
	}
}
/******************************************************************************
    Description: Adds a designer file to the project.
          oProj: Project object
 strProjectName: Project name
 strProjectPath: Project path
strDesignerFile: Designer file name
    AddItemFile: Wether the wizard is invoked from the Add Item Dialog or not
******************************************************************************/
function AddDesignerFileToCSharpWebProject(oProj, strProjectName, strProjectPath, strDesignerFile, AddItemFile)
{
	dte.SuppressUI = false;
	var projItems;
	if(AddItemFile)
		projItems = oProj;
	else
		projItems = oProj.ProjectItems;

	var strTemplatePath = wizard.FindSymbol("TEMPLATES_PATH");

	var strTpl = "";
	var strName = "";

	if (strDesignerFile != "")
	{
		strName = strDesignerFile;
		var strTarget;
		if(!AddItemFile)
		{
			strTarget = GetCSharpTargetName(strName, strProjectName);
		}
		else
		{
			strTarget = wizard.FindSymbol("ITEM_NAME");
		}
		var strClassName = strTarget.split(".");
		wizard.AddSymbol("SAFE_CLASS_NAME", strClassName[0]);
		wizard.AddSymbol("SAFE_ITEM_NAME", strClassName[0]);

		var strTemplate = strTemplatePath + "\\" + strDesignerFile;
		var projfile = projItems.AddFromTemplate(strTemplate, strTarget);
		if(projfile)
			SetFileProperties(projfile, strName);

		var bOpen = false;
		if(AddItemFile)
			bOpen = true;
		else if (DoOpenFile(strTarget))
			bOpen = true;

		if(bOpen)
		{
			var window = projfile.Open(vsViewKindPrimary);
			if(window)
				window.visible = true;
		}
	}
}



function AddFilters(oProj)
{
	try
	{
//debugger;
		// Add the folders to your project
		var L_strSource_Text = "Source Files";
		var group = oProj.Object.AddFilter(L_strSource_Text);
		group.Filter = "cpp;c;cxx;cs;def;odl;idl;hpj;bat;asm;asmx";
		group.UniqueIdentifier = "{4FC737F1-C7A5-4376-A066-2A32D752A2FF}";

		var L_strHeader_Text = "Header Files";
		group = oProj.Object.AddFilter(L_strHeader_Text);
		group.Filter = "h;hpp;hxx;hm;inl;inc;xsd";
		group.UniqueIdentifier = "{93995380-89BD-4b04-88EB-625FBE52EBFB}";

		var L_strResources_Text = "Resource Files";
		group = oProj.Object.AddFilter(L_strResources_Text);
		group.Filter = "rc;ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe;resx";
		group.UniqueIdentifier = "{67DA6AB6-F800-4c08-8B7A-83BB121AAD01}";
	}
	catch(e)
	{
		throw e;
	}
}
function AddFolders(oProj)
{
	//for the future
}

function AddConfig(proj, strProjectName)
{
	try
	{
		var config = proj.Object.Configurations('Debug');
		config.IntermediateDirectory = 'Debug';
		config.OutputDirectory = 'Debug';

		var CLTool = config.Tools('VCCLCompilerTool');
		// TODO: Add compiler settings

		var LinkTool = config.Tools('VCLinkerTool');
		// TODO: Add linker settings

		config = proj.Object.Configurations('Release');
		config.IntermediateDirectory = 'Release';
		config.OutputDirectory = 'Release';

		var CLTool = config.Tools('VCCLCompilerTool');
		// TODO: Add compiler settings

		var LinkTool = config.Tools('VCLinkerTool');
		// TODO: Add linker settings
	}
	catch(e)
	{
		throw e;
	}
}

function PchSettings(proj)
{
	// TODO: specify pch settings
}

function DelFile(fso, strWizTempFile)
{
	try
	{
		if (fso.FileExists(strWizTempFile))
		{
			var tmpFile = fso.GetFile(strWizTempFile);
			tmpFile.Delete();
		}
	}
	catch(e)
	{
		throw e;
	}
}

function CreateCustomInfFile() {
//    debugger;
	try
	{
		var fso, TemplatesFolder, TemplateFiles, strTemplate;
		fso = new ActiveXObject('Scripting.FileSystemObject');

		var TemporaryFolder = 2;
		var tfolder = fso.GetSpecialFolder(TemporaryFolder);
		var strTempFolder = tfolder.Drive + '\\' + tfolder.Name;

		var strWizTempFile = tfolder.path + "\\wizard\\" + fso.GetTempName();

		var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');
		var strInfFile = strTemplatePath + '\\Templates.inf';
		wizard.RenderTemplate(strInfFile, strWizTempFile);

		var WizTempFile = fso.GetFile(strWizTempFile);
		return WizTempFile;
	}
	catch(e)
	{
		throw e;
	}
}

function GetTargetName(strName, strProjectName)
{
	try
	{
		// TODO: set the name of the rendered file based on the template filename
		var strTarget = strName;

		if (strName == 'readme.txt')
			strTarget = 'ReadMe.txt';

		if (strName == 'sample.txt')
			strTarget = 'Sample.txt';
			
        if (strName == 'root.cpp')
            strTarget = strProjectName + strName.substr(4);
            
        if (strName == 'root.h')
            strTarget = strProjectName + strName.substr(4);
                
        if (strName == 'root.cs')
            strTarget = strProjectName + strName.substr(4);
            
        if (strName == 'root.vb')
            strTarget = strProjectName + strName.substr(4);  
            
        if (strName == 'form.cpp')
            strTarget = strProjectName + strName;  
            
        if (strName == 'form.h')
            strTarget = strProjectName + strName;    
            
        if (strName == 'form.cs')
            strTarget = strProjectName + strName;  
            
        if (strName == 'form.vb')
            strTarget = strProjectName + strName;    
        
        if (strName == 'CPPAddin.mke') 
            strTarget = strProjectName + strName.substr(8);  

        if (strName == 'CSharpAddIn.mke') 
            strTarget = strProjectName + strName.substr(11);  

        if (strName == 'VBAddIn.mke') 
            strTarget = strProjectName + strName.substr(7);  
        
        if (strName == 'form.resx')
            strTarget = strProjectName+strName;
            
        if (strName == 'PlacementCmd.cs')
            strTarget = strProjectName + strName;
            
        if (strName == 'LocateCmd.cs')
            strTarget = strProjectName + strName;
            
        if (strName == 'PlacementCmd.vb')
            strTarget = strProjectName + strName;
            
        if (strName == 'LocateCmd.vb')
            strTarget = strProjectName + strName;
            
        if (strName == 'PlacementCmd.cpp')
            strTarget = strProjectName + strName;
            
        if (strName == 'PlacementCmd.h')
            strTarget = strProjectName + strName;
            
        if (strName == 'LocateCmd.cpp')
            strTarget = strProjectName + strName; 
            
        if (strName == 'LocateCmd.h')
            strTarget = strProjectName + strName;   
            
		return strTarget; 
	}
	catch(e)
	{
		throw e;
	}
}

function AddFilesToCustomProj(proj, strProjectName, strProjectPath, InfFile)
{
	try
	{
		var projItems = proj.ProjectItems

		var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');

		var strTpl = '';
		var strName = '';

		var strTextStream = InfFile.OpenAsTextStream(1, -2);
		while (!strTextStream.AtEndOfStream)
		{
			strTpl = strTextStream.ReadLine();
			if (strTpl != '')
			{
				strName = strTpl;
				var strTarget = GetTargetName(strName, strProjectName);
				var strTemplate = strTemplatePath + '\\' + strTpl;
				var strFile = strProjectPath + '\\' + strTarget;

				var bCopyOnly = false;  //"true" will only copy the file from strTemplate to strTarget without rendering/adding to the project
				var strExt = strName.substr(strName.lastIndexOf("."));
				if(strExt==".bmp" || strExt==".ico" || strExt==".gif" || strExt==".rtf" || strExt==".css")
					bCopyOnly = true;
				wizard.RenderTemplate(strTemplate, strFile, bCopyOnly);
				
				proj.ProjectItems.AddFromFile(strFile);
				
			}
		}
		strTextStream.Close();
	}
	catch(e)
	{
		throw e;
	}
}
