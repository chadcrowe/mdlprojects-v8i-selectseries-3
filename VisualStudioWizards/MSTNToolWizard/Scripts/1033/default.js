
function OnFinish(selProj, selObj)
{
	try
	{
		var strProjectPath = wizard.FindSymbol('PROJECT_PATH');
		var strProjectName = wizard.FindSymbol('PROJECT_NAME');
		
		var strSafeProjectName = CreateSafeName (strProjectName);        
        wizard.AddSymbol ("SAFE_PROJECT_NAME", strSafeProjectName);
		var strSafeProjectNameUC  = strSafeProjectName.toUpperCase();
		wizard.AddSymbol ("SAFE_PROJECT_NAME_UPPERCASE",strSafeProjectNameUC);

		selProj = CreateCustomProject(strProjectName, strProjectPath);
		AddConfig(selProj, strProjectName);
		AddFilters(selProj);

		var InfFile = CreateCustomInfFile();
		AddFilesToCustomProj(selProj, strProjectName, strProjectPath, InfFile);
		PchSettings(selProj);
		InfFile.Delete();

		selProj.Object.Save();
	}
	catch(e)
	{
		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function CreateCustomProject(strProjectName, strProjectPath)
{
	try
	{
		var strProjTemplatePath = wizard.FindSymbol('PROJECT_TEMPLATE_PATH');
		var strProjTemplate = '';
		strProjTemplate = strProjTemplatePath + '\\default.vcproj';

		var Solution = dte.Solution;
		var strSolutionName = "";
		if (wizard.FindSymbol("CLOSE_SOLUTION"))
		{
			Solution.Close();
			strSolutionName = wizard.FindSymbol("VS_SOLUTION_NAME");
			if (strSolutionName.length)
			{
				var strSolutionPath = strProjectPath.substr(0, strProjectPath.length - strProjectName.length);
				Solution.Create(strSolutionPath, strSolutionName);
			}
		}

		var strProjectNameWithExt = '';
		strProjectNameWithExt = strProjectName + '.vcproj';

		var oTarget = wizard.FindSymbol("TARGET");
		var prj;
		if (wizard.FindSymbol("WIZARD_TYPE") == vsWizardAddSubProject)  // vsWizardAddSubProject
		{
			var prjItem = oTarget.AddFromTemplate(strProjTemplate, strProjectNameWithExt);
			prj = prjItem.SubProject;
		}
		else
		{
			prj = oTarget.AddFromTemplate(strProjTemplate, strProjectPath, strProjectNameWithExt);
		}
		return prj;
	}
	catch(e)
	{
		throw e;
	}
}

function AddFilters(oProj)
{
	try
	{
//debugger;
		// Add the folders to your project
		var L_strSource_Text = "Source Files";
		var group = oProj.Object.AddFilter(L_strSource_Text);
		group.Filter = "cpp;c;cxx;cs;def;odl;idl;hpj;bat;asm;asmx";
		group.UniqueIdentifier = "{4FC737F1-C7A5-4376-A066-2A32D752A2FF}";

		var L_strHeader_Text = "Header Files";
		group = oProj.Object.AddFilter(L_strHeader_Text);
		group.Filter = "h;hpp;hxx;hm;inl;inc;xsd";
		group.UniqueIdentifier = "{93995380-89BD-4b04-88EB-625FBE52EBFB}";

		var L_strResources_Text = "Resource Files";
		group = oProj.Object.AddFilter(L_strResources_Text);
		group.Filter = "rc;ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe;resx";
		group.UniqueIdentifier = "{67DA6AB6-F800-4c08-8B7A-83BB121AAD01}";
	}
	catch(e)
	{
		throw e;
	}
}
function AddFolders(oProj)
{
	//for the future
}


function AddConfig(proj, strProjectName)
{
	try
	{
		var config = proj.Object.Configurations('Debug');
		config.IntermediateDirectory = '$(ConfigurationName)';
		config.OutputDirectory = '$(ConfigurationName)';

		var CLTool = config.Tools('VCCLCompilerTool');
		// TODO: Add compiler settings

		var LinkTool = config.Tools('VCLinkerTool');
		// TODO: Add linker settings

		config = proj.Object.Configurations('Release');
		config.IntermediateDirectory = '$(ConfigurationName)';
		config.OutputDirectory = '$(ConfigurationName)';

		var CLTool = config.Tools('VCCLCompilerTool');
		// TODO: Add compiler settings

		var LinkTool = config.Tools('VCLinkerTool');
		// TODO: Add linker settings
	}
	catch(e)
	{
		throw e;
	}
}

function PchSettings(proj)
{
	// TODO: specify pch settings
}

function DelFile(fso, strWizTempFile)
{
	try
	{
		if (fso.FileExists(strWizTempFile))
		{
			var tmpFile = fso.GetFile(strWizTempFile);
			tmpFile.Delete();
		}
	}
	catch(e)
	{
		throw e;
	}
}

function CreateCustomInfFile()
{
	try
	{
		var fso, TemplatesFolder, TemplateFiles, strTemplate;
		fso = new ActiveXObject('Scripting.FileSystemObject');

		var TemporaryFolder = 2;
		var tfolder = fso.GetSpecialFolder(TemporaryFolder);
		var strTempFolder = tfolder.Drive + '\\' + tfolder.Name;

		var strWizTempFile = strTempFolder + "\\" + fso.GetTempName();

		var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');
		var strInfFile = strTemplatePath + '\\Templates.inf';
		wizard.RenderTemplate(strInfFile, strWizTempFile);

		var WizTempFile = fso.GetFile(strWizTempFile);
		return WizTempFile;
	}
	catch(e)
	{
		throw e;
	}
}

function GetTargetName(strName, strProjectName)
{
	try
	{
		// TODO: set the name of the rendered file based on the template filename
		var strTarget = strName;

		if (strName == 'readme.txt')
			strTarget = 'ReadMe.txt';
        else if (strName == 'sample.txt')
			strTarget = 'Sample.txt';
		else if ((strName=='ToolText.h')||(strName=='ToolMsg.r'))
	        strTarget= "english" + "\\"+ strProjectName + strName; 
	    else if (strName == 'Tool.mke')
	        strTarget = strProjectName + '.mke' 
		else 
		    strTarget = strProjectName + strName;
		    
//        if (strName == 'Tool.h')
//            strTarget = strProjectName + strName;
//        if (strName == 'Tool.cpp')
//            strTarget = strProjectName + strName;
//        if (strName == 'ModifyTool.h')
//            strTarget = strProjectName + strName;
//        if (strName == 'ModifyTool.cpp')
//            strTarget = strProjectName + strName;
//        if (strName == 'PrimitiveTool.h')
//            strTarget = strProjectName + strName;
//        if (strName == 'PrimitiveTool.cpp')
//            strTarget = strProjectName + strName;
//        if (strName == 'ViewTool.h')
//            strTarget = strProjectName + strName;
//        if (strName == 'ViewTool.cpp')
//            strTarget = strProjectName + strName;
//        if (strName == 'Tool.mke')
//            strTarget = strProjectName + strName;
//        if (strName == 'Tool.r')
//            strTarget = strProjectName + strName;
        
        return strTarget; 
	}
	catch(e)
	{
		throw e;
	}
}

function AddFilesToCustomProj(proj, strProjectName, strProjectPath, InfFile)
{
	try
	{
		var projItems = proj.ProjectItems

		var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH');

		var strTpl = '';
		var strName = '';

		var strTextStream = InfFile.OpenAsTextStream(1, -2);
		while (!strTextStream.AtEndOfStream)
		{
			strTpl = strTextStream.ReadLine();
			if (strTpl != '')
			{
				strName = strTpl;
				var strTarget = GetTargetName(strName, strProjectName);
				var strTemplate = strTemplatePath + '\\' + strTpl;
				var strFile = strProjectPath + '\\' + strTarget;

				var bCopyOnly = false;  //"true" will only copy the file from strTemplate to strTarget without rendering/adding to the project
				var strExt = strName.substr(strName.lastIndexOf("."));
				if(strExt==".bmp" || strExt==".ico" || strExt==".gif" || strExt==".rtf" || strExt==".css")
					bCopyOnly = true;
				wizard.RenderTemplate(strTemplate, strFile, bCopyOnly);
				proj.Object.AddFile(strFile);
			}
		}
		strTextStream.Close();
	}
	catch(e)
	{
		throw e;
	}
}
