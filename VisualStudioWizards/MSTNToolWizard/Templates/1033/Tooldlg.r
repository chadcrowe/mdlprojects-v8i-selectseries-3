/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/vault/VisualStudioWizards/MSTNToolWizard/Templates/1033/Tooldlg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   toolExample  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:41:57 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   toolExample - toolExample source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogids.h>
#include    <rscdefs.h>

#include    "[!output SAFE_PROJECT_NAME]Tool.h"
#include    "[!output SAFE_PROJECT_NAME]ToolCmd.h"
#include    <[!output SAFE_PROJECT_NAME]ToolText.h>


/*----------------------------------------------------------------------+
|                                                                       |
|    Push Button Items                                                  |
|                                                                       |
+----------------------------------------------------------------------*/
