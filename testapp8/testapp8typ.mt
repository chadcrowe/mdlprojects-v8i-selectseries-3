/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp8/testapp8typ.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp8/testapp8typ.mtv  $
|   $Workfile:   testapp8typ.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	testapp8 Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "testapp8.h"

publishStructures (testapp8globals);

    
