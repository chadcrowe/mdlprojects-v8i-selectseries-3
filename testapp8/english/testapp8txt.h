/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp8/english/testapp8txt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp8/english/testapp8txt.h_v  $
|   $Workfile:   testapp8txt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Static text defines for the testapp8 application dialog resources	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__testapp8txtH__)
#define	__testapp8txtH__

#define	TXT_testapp8DialogBox			"testapp8 Dialog Box"
#define	TXT_testapp8ModalDialogBox			"testapp8 Modal Dialog Box"
#define	TXT_Parameter1				"Parameter 1:"
#define	TXT_OpenModal				"Open Modal"
#define	TXT_IncrementParameter1			"Increment parameter 1?"
#define TXT_Value1				"Value 1"
#define TXT_Value2				"Value 2"
#define TXT_Value3				"Value 3"

#endif
