/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp8/english/testapp8msg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp8/english/testapp8msg.r_v  $
|   $Workfile:   testapp8msg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	testapp8 application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "testapp8.h"	/* testapp8 dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_testapp8Errors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_testapp8Dialog,   "Unable to open testapp8 dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
