#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

template <typename T>
string NumberToString(T Number)
{
	ostringstream ss;
	ss << Number;
	return ss.str();
}

void writeToFile(int number)
{
	ofstream myfile;
	myfile.open("C++ Text.txt", ios::app);
	myfile << NumberToString(number);
	myfile.close();
}

void writeToFile(string s)
{
	ofstream myfile;
	myfile.open("C++ Text.txt", ios::app);
	myfile << s;
	myfile.close();
}
int main
(){
	writeToFile(77);
	writeToFile("Chad");
	return 0;
}