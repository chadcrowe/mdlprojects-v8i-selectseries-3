/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/filemain.c,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/filemain.c_v  $
|   $Workfile:   filemain.c  $
|   $Revision: 7.4.52.1 $
|   	$Date: 2013/07/01 20:37:38 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   filemain.c -- The main file in the DLM in the fileio example.       |
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include	<stdio.h>
#include	<stdlib.h>

#if defined (winNT)
#   include	<io.h>
#endif

#include	<basedefs.h>
#include	<mssystem.fdf>
#include	<dlmsys.fdf>
#include	<userfnc.h>
#include        <mslocate.fdf>
#include	"fileio.h"

extern int toolSubsystem_printf (const char *, ...);

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
#define MAX_FILE_ENTRIES	    20

/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
typedef struct openHook
    {
    void	*mdlDescP;
    ULong	 offset;
    } OpenHook;

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
/*  fileList is used to associated file handles with MDL descriptors */
Private FileOwner    fileList [MAX_FILE_ENTRIES];
Private OpenHook    *openHooksP;
Private int	     sizeHookList;
Private void	    *dlmID;

/*======================================================================+
|									|
|   Private Utility Routines						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		closeApplicationFiles   				|
|									|
| author	BSI     			12/91			|
|									|
|	This closes all of the files that were opened or created        |
|	by the specified application.   				|
|									|
+----------------------------------------------------------------------*/
Private void closeApplicationFiles
(
void	*mdlDescP
)
    {
    FileOwner   *entryP;

    for (entryP = fileList; entryP < &fileList [MAX_FILE_ENTRIES]; entryP++)
	{/* If the MDL descriptor matches then close the file */
	if (entryP->mdlDescP == mdlDescP)
	    {
	    close (entryP->handle);
	    entryP->mdlDescP = NULL;		/*  Mark this entry as free */
	    }
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		clearOpenFilter						|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Private void clearOpenFilter
(
void	*mdlDescP
)
    {
    int		 hookNumber;
    OpenHook	*entryP;

    /* Clear all of the hooks that belong to the specified application */
    for (entryP = openHooksP, hookNumber = 0; hookNumber < sizeHookList;
						    hookNumber++, entryP++)
	{
	if (entryP->mdlDescP == mdlDescP)
	    {
	    entryP->mdlDescP = NULL;
	    entryP->offset = 0;
	    }
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		setOpenFilter						|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Private int setOpenFilter
(
void	*mdlDescP,
ULong	 offset
)
    {
    int		 hookNumber;
    OpenHook	*entryP;

    /*  Find a free entry, i.e., one with mdlDescP NULL */
    for (entryP = openHooksP, hookNumber = 0; hookNumber < sizeHookList;
						hookNumber++, entryP++)
	{
	if (entryP->mdlDescP == NULL)
	    {
	    entryP->mdlDescP = mdlDescP;
	    entryP->offset = offset;
	    return SUCCESS;
	    }
	}

    /*  None found, allocate a new one */
    sizeHookList++;
    openHooksP = realloc (openHooksP, (sizeHookList * sizeof (*openHooksP)));

    if (openHooksP == NULL)
	{
	sizeHookList = 0;
	return ERROR;
	}
	
    entryP = openHooksP + sizeHookList - 1;
    
    entryP->mdlDescP = mdlDescP;
    entryP->offset = offset;
    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		mdlUnloadHook              				|
|									|
| author	BSI     			12/91			|
|									|
|	This is called by MicroStation whenever an MDL application      |
|	is unloaded.  The function dlmSystem_setFunction was used to    |
|	designate this as an MDL unload hook.   			|
|									|
|	The input parameter mdlDescP may or may not be the same 	|
|	as currMdlDesc. An mdlUnloadHook must never use currMdlDesc.    |
|									|
+----------------------------------------------------------------------*/
Private int mdlUnloadHook
(
void	*mdlDescP
)
    {
    /*  Close all of this application's files that were opened
	with open, creat, and sopen. */
    closeApplicationFiles (mdlDescP);

    /*  Remove all of this applications open/create hooks */
    clearOpenFilter (mdlDescP);

    return SUCCESS;
    }

/*======================================================================+
|									|
|   Major Public Code Section						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		callOpenHooks						|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Public void callOpenHooks
(
char	*fileNameP,
int	 operation,
int	 mode
)
    {
    int		 hookNumber;
    OpenHook	*entryP;

    for (entryP = openHooksP, hookNumber = 0; hookNumber < sizeHookList;
					    hookNumber++, entryP++)
	{
	if (entryP->mdlDescP != NULL)
	    {
	    dlmSystem_callMdlFunction (entryP->mdlDescP, (MdlFunctionP) entryP->offset,
				  fileNameP, mode, operation);
	    }
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		fileio_setFunction					|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Public int fileio_setFunction
(
int	 funcType,
ULong	 offset		    /*  => MDL application's function pointer */
)
    {
    switch (funcType)
	{
	case 1:  /*  Set a hook on file open */
	    if (offset)
		return setOpenFilter (mdlSystem_getCurrMdlDesc (), offset);
	    else
		{/*  NULL means clear the hook */
		clearOpenFilter (mdlSystem_getCurrMdlDesc ());
		return SUCCESS;
		}
	}

    return ERROR;
    }

/*----------------------------------------------------------------------+
|									|
| name		findFreeFileEntry       				|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Public FileOwner *findFreeFileEntry (void)
    {
    FileOwner   *entryP;

    for (entryP = fileList; entryP < &fileList [MAX_FILE_ENTRIES]; entryP++)
	{
	if (entryP->mdlDescP == NULL)
	    return entryP;
	}

    return NULL;
    }

/*----------------------------------------------------------------------+
|									|
| name		freeFileEntry						|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Public int freeFileEntry	 /* <= SUCCESS if the entry was found */
(
void	    *mdlDescP,	    /* <= MDL app that has the file open */
int	     handle
)
    {
    FileOwner   *entryP;

    for (entryP = fileList; entryP < &fileList [MAX_FILE_ENTRIES]; entryP++)
	{
	if ((entryP->mdlDescP == mdlDescP) &&
		(entryP->handle == handle))
	    {
	    entryP->mdlDescP = NULL;	/*  Mark the entry as free */
	    return SUCCESS;
	    }
	}

    /* The current application did not really have this file open. */
    return -1;
    }

/*----------------------------------------------------------------------+
|									|
| name		initialize              				|
|									|
| author	BSI     			12/91			|
|									|
|	MicroStation calls the initialization function immediately      |
|	after loading the DLM. If the DLM returns a non-zero value,     |
|	MicroStation aborts the load. If the application is going to    |
|	return a non-zero value, it should also display a message       |
|	using dlmSystem_displayError.   				|
|									|
|	The parameter localDlmID is a pointer to this DLM's descriptor. |
|	All of the dlm..._setFunction calls require this as a parameter.|
|									|
|	The version parameter is taken from the DLS file.  The DLM      |
|	can use this to determine if it is compatible with the MDL      |
|	application.    						|
|									|
+----------------------------------------------------------------------*/
Public int initialize
(
char	    *filenameP,
char	    *taskIDP,
void	    *localDlmID,    /*  Uniquely identifies the DLM */
ULong	     version	    /*  Value is taken from the DLM Spec Source */
)
    {
    static  int calledBefore;

    if (version > 0x500)
	{
	dlmSystem_displayError ("fileio lib: version must be 5.0.0 or less.");
	/*  Reject the load request */
	return -1;
	}

    if (calledBefore)
	return SUCCESS;

    calledBefore = 1;

    dlmID = localDlmID;

    /*  Set up a filter to be called when an application exits. */
    dlmSystem_setFunction (DLM_SYSTEM_MDL_UNLOAD, localDlmID,
			    mdlUnloadHook);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		fileio_chmod, fileio_read, fileio_write, fileio_lseek   |
|									|
| author	BSI     			12/91			|
|									|
|									|
+----------------------------------------------------------------------*/
#if !defined (macintosh)
Public int fileio_chmod
(
char	*pathP,
int	 mode
)
    {
    return chmod (pathP, mode);
    }
#endif

Public int fileio_read
(
int	 handle,
void	*buffP,
int	 count
)
    {
    return read (handle, buffP, count);
    }

Public int fileio_write
(
int	 handle,
void	*buffP,
int	 count
)
    {
    return write (handle, buffP, count);
    }


Public int fileio_lseek
(
int	 handle,
int 	 offset,
int	 whence
)
    {
    return lseek (handle, offset, whence);
    }


Public int userLocate_globalFilter
(
LOCATE_Action       action , 
MSElement*          selElm , 
DgnModelRefP        modelRef , 
ULong               filePosition , 
DPoint3d*           pPoint , 
int                 viewNumber , 
HitPathP            hitPath , 
char*               rejectReason  
)
    {
    printf ("in userLocate_globalFilter, activity = %d, filePos = %d \n",
	    action,filePosition);

    return  LOCATE_FILTER_STATUS_Neutral;
    }



