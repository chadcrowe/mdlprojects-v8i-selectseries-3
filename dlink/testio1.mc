/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/testio1.mc,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   testio1.mc  $
|   $Revision: 1.3.76.1 $
|   	$Date: 2013/07/01 20:37:49 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	DLINK Example MDL Driver				    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>

#include    "mdliolib.h"
#include    "fil1cmd.h"

#include    <msparse.fdf>
#include    <mselems.h>
#include    <msrsrc.fdf>

#include    <mslocate.fdf>
#include    <userfnc.h>

/*----------------------------------------------------------------------+
|									|
|   Function Declarations						|
|									|
+----------------------------------------------------------------------*/
nativeCode int open (char *, int, int);
nativeCode int write (int, void *, int);
nativeCode int creat (char *, int);
nativeCode int close (int);
nativeCode int fileio_setFunction (int, ULong);


/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
Private char test1 [] = "this is the first string to write.";

/*======================================================================+
|									|
|   Major Public Code Section						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		tryall  						|
|									|
| description   Try all of the functions added by the DLM fileio.       |
|									|
| author	BSI					12/31		|
|									|
+----------------------------------------------------------------------*/
Private void tryall
(
char *unparsed
) 
cmdNumber CMD_TESTIO1_TRYALL
    {
    int	    fd1;

    if ((fd1 = open ("--gargbage", O_RDONLY, 0)) == -1)
	printf ("test passed: unable to open '--garbage' \n");

    /*  Create a file and dump data to it. */
    if ((fd1 = creat ("temp1", 0666)) < 0)
	{
	printf ("test failed: Could not create temp1");
	return;
	}

    if (write (fd1, test1, sizeof (test1)) != sizeof (test1))
	{
	printf ("the first write failed");
	close (fd1);
	return;
	}

    /*  Close the file */
    close (fd1);

    /*  Reopen the file for read only, read it,
	and display the contents.   */
    if ((fd1 = open ("temp1", O_RDONLY, 0)) < 0)
	{
	printf ("the first open failed");
	return;
	}
    
    close (fd1);
    }

/*----------------------------------------------------------------------+
|									|
| name		openHook						|
|									|
| description   Test the openHook capability. This is installed by the  |
|		installHook command. It is called whenever this 	|
|		application or testio2 opens, creates, or dups a file.  |
|									|
|									|
| author	BSI					12/31		|
|									|
+----------------------------------------------------------------------*/
Private void openHook
(
char	*filenameP,
int	 mode,
int	 operation
)
    {
    printf ("openHook: name = '%s', mode = %#X, operation = %d.\n",
	filenameP, mode, operation);
    }

/*----------------------------------------------------------------------+
|									|
| name		installHook, removeHook 				|
|									|
| author	BSI					12/31		|
|									|
+----------------------------------------------------------------------*/
Private void installHook
(
char *unparsed
) 
cmdNumber CMD_TESTIO1_INSTALLHOOK
    {
    fileio_setFunction (1, (ULong)openHook);
    }

Private void removeHook
(
char *unparsed
) 
cmdNumber CMD_TESTIO1_REMOVEHOOK
    {
    fileio_setFunction (1, 0);
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI					06/95		|
|									|
+----------------------------------------------------------------------*/
Public void	main
(
int 	argc,
char   *argv[]
)
    {
    unsigned long   rscFileH;

    mdlResource_openFile (&rscFileH, NULL, 0);
    mdlParse_loadCommandTable (NULL);

    }

