/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/fileio.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/fileio.r_v  $
|   $Workfile:   fileio.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:37 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Macintosh resource definition for DLM portion		    	|
|									|
+----------------------------------------------------------------------*/
#include "SysTypes.r"
#include "CodeFragmentTypes.r"

resource 'cfrg' (0) {
   {
      kPowerPC,
      kFullLib,
	  kNoVersionNum,kNoVersionNum,
	  0,0,
          kIsLib,
          kOnDiskFlat,kZeroOffset,kWholeFork,
	  "FileIO"
   }
};
