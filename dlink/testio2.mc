/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/testio2.mc,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   testio2.mc  $
|   $Revision: 1.4.34.1 $
|   	$Date: 2013/07/01 20:37:50 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   testio2.mc -- source code for one of the 2 MDL applications used    |
|		  in the fileio dynamic loading example.		|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>

#include    "mdliolib.h"
#include    "fil2cmd.h"

#include    <msparse.fdf>
#include    <msrsrc.fdf>

/*----------------------------------------------------------------------+
|									|
|   Function Declarations						|
|									|
+----------------------------------------------------------------------*/
nativeCode int open (char *, int, int);
nativeCode int creat (char *, int);
nativeCode int close (int);
nativeCode int write (int, void *, int);

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
Private char test1 [] = "testio2: here is my message.";

/*======================================================================+
|									|
|   Major Public Code Section						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		onefile         					|
|									|
| description   This function does a few simple test with a file.       |
|		Run this after the testio1 has the open file user hook  |
|		installed.						|
|									|
| author	BSI					12/31		|
|									|
+----------------------------------------------------------------------*/
Private void onefile
(
char *unparsed
) 
cmdNumber CMD_TESTIO2_ONEFILE
    {
    int	    fd1;

    /*  Create a file and dump data to it. */
    if ((fd1 = creat ("temp1", 0666)) < 0)
	{
	printf ("Could not create temp1");
	return;
	}

    if (write (fd1, test1, sizeof (test1)) != sizeof (test1))
	{
	printf ("testio2: the first write failed");
	close (fd1);
	return;
	}

    /*  Close the file */
    close (fd1);

    /*  Reopen the file for read only, read it,
	and display the contents.   */
    if ((fd1 = open ("temp1", O_RDONLY, 0)) < 0)
	{
	printf ("the first open failed");
	return;
	}

    /*  Verify that we can read it. */
    close (fd1);
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI					06/95		|
|									|
+----------------------------------------------------------------------*/
Public void	main
(
int 	argc,
char   *argv[]
)
    {
    unsigned long   rscFileH;

    mdlResource_openFile (&rscFileH, NULL, 0);
    mdlParse_loadCommandTable (NULL);
    }

