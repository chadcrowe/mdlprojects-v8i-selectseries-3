/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/fileio.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/fileio.h_v  $
|   $Workfile:   fileio.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:34 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   fileio.h - Example header file  					|
|									|
+----------------------------------------------------------------------*/
#ifndef __fileioH__
#define __fileioH__

/*----------------------------------------------------------------------+
|									|
|   Header File Dependencies						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Defines								|
|									|
+----------------------------------------------------------------------*/

#define	    FILEIO_OPEN	    0
#define	    FILEIO_CREAT    1
#define	    FILEIO_DUP	    2

#if defined (sparc) || defined (hp700)
#define	    dlmSystem_setFunction	dynaload_setFunction
#define	    dlmSystem_displayError	dynaload_displayError
#define	    dlmSystem_callMdlFunction	ustnmdl_callFunction
#endif    

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/

typedef struct fileOwner
    {
    void    *mdlDescP;
    int	     handle;
    } FileOwner;


#endif
