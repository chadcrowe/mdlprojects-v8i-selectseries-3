/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/fileresl.c,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/fileresl.c_v  $
|   $Workfile:   fileresl.c  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:41 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   fileresl.c  -- This file is linked with the standard C library      |
|		   to create a relocatable object file that has the     |
|		   standard C functions that the fileio DLM adds to     |
|		   MicroStation. It does not do any work. It just       |
|		   has references to force the linker to include	|
|		   the desired modules. 				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/

#include    "mdl.h"
    
/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/

typedef int (*funcP)();
	    
/*----------------------------------------------------------------------+
|									|
|   Function Declarations						|
|									|
+----------------------------------------------------------------------*/
    
int	    open (), read (), write (), creat (), chmod (), dup (), 
	    lseek (), close ();
	    
#if defined (unix)
int	    fcntl (),  ioctl (), getmsg (), putmsg (), poll (), umask ();
#endif	    

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
	
/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
	    
funcP funclist [] = {open, read, write, creat, chmod, 
		     dup, lseek, close,
#if defined (unix)
		     fcntl,  ioctl, getmsg, putmsg, poll, umask
#endif	    
		     };

