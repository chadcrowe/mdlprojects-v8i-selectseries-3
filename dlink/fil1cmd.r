/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/fil1cmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/fil1cmd.r_v  $
|   $Workfile:   fil1cmd.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:29 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	DLINK Testio1 example application command table			|
|									|
+----------------------------------------------------------------------*/
#include "rscdefs.h"
#include "cmdclass.h"

#define	    CT_NONE	    0
#define	    CT_MAIN	    1
#define	    CT_TESTIO       2

Table	CT_MAIN =
{
    { 1,  CT_TESTIO,	PLACEMENT,   REQ,	"TESTIO1"     },
};

Table	CT_TESTIO =
{
    { 1,  CT_NONE,	INHERIT,     NONE,	"TRYALL"    },
    { 2,  CT_NONE,	INHERIT,     NONE,	"INSTALLHOOK" },
    { 3,  CT_NONE,	INHERIT,     NONE,	"REMOVEHOOK" },
};

