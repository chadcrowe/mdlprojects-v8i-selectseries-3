/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/fileopen.c,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/fileopen.c_v  $
|   $Workfile:   fileopen.c  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   fileopen.c -- Open Part of the fileio dynamic loading example.	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#if defined (winNT)
#   include	<io.h>
#endif

#include	<basedefs.h>
#include	<mssystem.fdf>

#include	"fileio.h"
    
/*----------------------------------------------------------------------+
|									|
|   Function Declarations						|
|									|
+----------------------------------------------------------------------*/
#if !defined (winNT)
extern	int	    open (), creat (), dup ();
#endif
extern	FileOwner  *findFreeFileEntry ();
extern	void	    callOpenHooks ();

/*======================================================================+
|									|
|   Major Public Code Section						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		fileio_open    						|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Public int fileio_open
(
char	*pathP,
int	 oflag,
int	 mode
)
    {
    FileOwner	*entryP;

    /*  The FileOwner file list is used to keep track of what 
	application opened or created a specific file.   */
    if ((entryP = findFreeFileEntry ()) == NULL)
	return -1;

    /*  Now try to open the file by calling the standard open */
    if ((entryP->handle = open (pathP, oflag, mode)) >= 0)
	{
	/* The open call was successful. Call all of the open hooks */
	callOpenHooks (pathP, FILEIO_OPEN, mode);
	
	/* Record the owner of the file. */
	entryP->mdlDescP = mdlSystem_getCurrMdlDesc ();
	
	return entryP->handle;
	}

    /*  Even though findFreeFileEntry returned a pointer to a fileOwner
	entry, we do not have to free it.  It is not considered allocated 
	if the mdlDescP field is NULL.
    */
    return -1;
    }

/*----------------------------------------------------------------------+
|									|
| name		fileio_creat						|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Public int fileio_creat
(
char	*pathP,
int	 mode
)
    {
    FileOwner	*entryP;

    if ((entryP = findFreeFileEntry ()) == NULL)
	return -1;

    if ((entryP->handle = creat (pathP, mode)) >= 0)
	{
	callOpenHooks (pathP, FILEIO_CREAT, mode);
	entryP->mdlDescP = mdlSystem_getCurrMdlDesc ();
	return entryP->handle;
	}

    /*  The fileOwner entry is considered free if the mdlDescP field
	is NULL. It is not necessary to return it.
    */
    return -1;
    }

/*----------------------------------------------------------------------+
|									|
| name		fileio_dup						|
|									|
| author	BSI     			12/91   	|
|									|
+----------------------------------------------------------------------*/
Public int fileio_dup
(
int	fd
)    
    {
    FileOwner	*entryP;

    if ((entryP = findFreeFileEntry ()) == NULL)
	return -1;

    if ((entryP->handle = dup (fd)) >= 0)
	{
	callOpenHooks (NULL, FILEIO_DUP, fd);
	entryP->mdlDescP = mdlSystem_getCurrMdlDesc ();
	return entryP->handle;
	}

    /*  The fileOwner entry is considered free if the mdlDescP field
	is NULL. It is not necessary to return it.
    */
    return -1;
    }

