/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/fileclos.c,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/fileclos.c_v  $
|   $Workfile:   fileclos.c  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:31 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   fileclos.c -- this contains the close function used in the  	|
|		  fileio example.       				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#if defined (winNT)
#   include	<io.h>
#endif

#include	<basedefs.h>
#include	<mssystem.fdf>

#include	"fileio.h"
    
/*----------------------------------------------------------------------+
|									|
|   Function Declarations						|
|									|
+----------------------------------------------------------------------*/
#if !defined (winNT)
extern 	int	close ();
#endif
extern 	int	freeFileEntry ();

/*======================================================================+
|									|
|   Major Public Code Section						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		fileio_close            				|
|									|
| author	BSI     			12/91			|
|									|
+----------------------------------------------------------------------*/
Public int fileio_close
(
int	handle
)
    {
    /*  Remove the entry from the file list and then close the file */
    if (freeFileEntry (mdlSystem_getCurrMdlDesc (), handle) == SUCCESS)
	return close (handle);
	
    /* The current application did not really have this file open. */
    return -1;
    }
