/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/mdliolib.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/mdliolib.h_v  $
|   $Workfile:   mdliolib.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:42 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|									|
|	This header file contains definitions needed by MDL programs    |
|	that use the fileio DLM.					|
|									|
+----------------------------------------------------------------------*/
#ifndef __mdliolibH__
#define __mdliolibH__

/*----------------------------------------------------------------------+
|									|
|   Header File Dependencies						|
|									|
+----------------------------------------------------------------------*/

#if !defined (__basedefsH__)
#include "basedefs.h"
#endif

/*----------------------------------------------------------------------+
|									|
|   Defines								|
|									|
+----------------------------------------------------------------------*/

#if defined (hp700) || defined (clipper) || defined (pm386) || \
    defined (sparc) || defined (sgimips) || defined (winNT) || defined (rs6000) || \
    defined (os2)   || defined (macintosh)
#define O_RDONLY	    0
#else
#error O_RDONLY is not defined for this platform
#endif

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/

#endif
