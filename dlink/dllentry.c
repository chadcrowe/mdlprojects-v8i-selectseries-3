/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/dllentry.c,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/dllentry.c_v  $
|   $Workfile:   dllentry.c  $
|   $Revision: 1.3.62.1 $
|   	$Date: 2013/07/01 20:37:23 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   dllentry.c -- Provides the required Windows Entry point for a DLL   |
|		  This could be used with any DLM (DLL) 		|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <windows.h>

/*----------------------------------------------------------------------+
|                                                                       |
| name          LibMain - main entry point to a DLL                     |
|                                                                       |
| author        BSI                         			11/91           |
|                                                                       |
+----------------------------------------------------------------------*/
INT  APIENTRY	LibMain
(
HANDLE	hInst, 
ULONG	reasonCalled,
LPVOID	lpReserved
)
    {
    return 1;
    }
