/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/dlink/fil2cmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/dlink/fil2cmd.r_v  $
|   $Workfile:   fil2cmd.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:37:30 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	DLINK Testio2 example application command table			|
|									|
+----------------------------------------------------------------------*/
#include "rscdefs.h"
#include "cmdclass.h"

#define	    CT_NONE	    0
#define	    CT_MAIN	    1
#define	    CT_TESTIO       2

Table	CT_MAIN =
{
    { 1,  CT_TESTIO,	PLACEMENT,   REQ,	"TESTIO2"     },
};

Table	CT_TESTIO =
{
    { 1,  CT_NONE,	INHERIT,     NONE,	"ONEFILE"    },
};

