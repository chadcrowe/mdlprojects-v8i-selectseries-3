/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/adrwdemo/adrwdmt.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/adrwdemo/adrwdmt.mtv  $
|   $Workfile:   adrwdmt.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:37 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	ADRWDEMO Published Structures					|
|									|
+----------------------------------------------------------------------*/
#include    "adrwdemo.h"

publishStructures (adrwdemoinfo);
