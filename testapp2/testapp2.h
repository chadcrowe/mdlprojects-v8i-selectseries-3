/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp2/testapp2.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp2/testapp2.h_v  $
|   $Workfile:   testapp2.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in testapp2 dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __testapp2H__
#define	    __testapp2H__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_testapp2		1   /* dialog id for testapp2 Dialog */
#define DIALOGID_testapp2Modal	2   /* dialog id for testapp2 Modal Dialog */

#define OPTIONBUTTONID_testapp2	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_testapp2		1   /* id for "parameter 1" text item */
#define TOGGLEID_testapp2		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_testapp2		1   /* id for synonym resource */

#define MESSAGELISTID_testapp2Errors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_testapp2Dialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_testapp2	1   /* id for toggle item hook func */
#define HOOKDIALOGID_testapp2		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct testapp2globals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } testapp2Globals;

#endif
