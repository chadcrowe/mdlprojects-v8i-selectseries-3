/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/sclrottc/english/srtcmsgs.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/sclrottc/english/srtcmsgs.r_v  $
|   $Workfile:   srtcmsgs.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:54 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Message Resources					    	|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>

#include "sclrottc.h"

MessageList MESSAGELISTID_SclTxCel = 
{
    {
    { 0, "Identify element" },
    { 1, "Accept/Reject" },
    { 2, "Scale All" },
    { 3, "Scale Single" },
    { 4, "Scale Fence" },
    { 5, "Rotate All" },
    { 6, "Rotate Single" },
    { 7, "Rotate Fence" },
    { 8, "%d Text %d Node %d Cell elements changed"},
    { 9, "Unable to load command table." },
    { 10, "Unable to load SclRotTC" },
    }
};

