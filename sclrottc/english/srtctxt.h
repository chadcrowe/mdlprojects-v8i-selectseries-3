/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/sclrottc/english/srtctxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/sclrottc/english/srtctxt.h_v  $
|   $Workfile:   srtctxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:54 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Static text defines for resources			    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__srtctxtH__)
#define __srtctxtH__

#define	TXT_RotateBy			    	"Rotate By"
#define	TXT_RotateText			    	"Rotate Text"
#define	TXT_RotateTextNode		    	"Rotate Text Node"
#define	TXT_RotateCell			    	"Rotate Cell"
#define	TXT_ScaleBy			    	"Scale By"
#define	TXT_ScaleText			    	"Scale Text"
#define	TXT_ScaleTextNode		    	"Scale Text Node"
#define	TXT_ScaleCell			    	"Scale Cell"
#define	TXT_OriginRotateTextCells	    	"Origin Rotate Text & Cells"
#define	TXT_OriginScaleTextCells	    	"Origin Scale Text & Cells"
#define	TXT_ScaleRotateCellsText	    	"Scale/Rotate Cells & Text"
#define	TXT_TESTFrame			    	"TEST Frame"
#define	TXT_ScaleTextCells		    	"Scale Text & Cells"
#define	TXT_SCLTXCEL			    	"SCLTXCEL"
#define	TXT_SCLROTTC			    	"SCLROTTC"
#define	TXT_All				    	"All"
#define	TXT_Single				"Single"
#define	TXT_Fence			    	"Fence"
#define	TXT_ActiveScaleby3Points	    	"Active Scale by 3 Points"
#define	TXT_ActiveAngleby2Points	    	"Active Angle by 2 Points"
#define	TXT_Undo			    	"Undo"

#endif /* !defined (__srtctxtH__) */
