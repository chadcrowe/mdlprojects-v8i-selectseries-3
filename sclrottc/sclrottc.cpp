/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/sclrottc/sclrottc.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   sclrottc.mc  $						|
|   $Revision: 1.1.32.1 $							|
|   	$Date: 2013/07/01 20:40:53 $				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Scale/Rotate Text & Cell Example Application		    	|
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <tcb.h>
#include    <mselems.h>
#include    <global.h>
#include    <scanner.h>
#include    <msinputq.h>
#include    <userfnc.h>
#include    <mdl.h>
#include    <cexpr.h>
#include    <cmdlist.h>
#include    <cmdclass.h>
#include    <rscdefs.h>
#include    <dlogitem.h>
#include    <dlogman.fdf>
#include    <dlogids.h>
#include    <mslstyle.h>
#include    <stdlib.h>
#include    <string.h>

#include    <mscell.fdf>
#include    <mssystem.fdf>
#include    <msmisc.fdf>
#include    <msselect.fdf>
#include    <msscan.fdf>
#include    <msoutput.fdf>
#include    <mstmatrx.fdf>
#include    <msrmatrx.fdf>
#include    <mscurrtr.fdf>
#include    <mselemen.fdf>
#include    <msstate.fdf>
#include    <mscexpr.fdf>
#include    <msparse.fdf>
#include    <mslocate.fdf>
#include    <msrsrc.fdf>
#include    <msdgnobj.fdf>
#include    <mselmdsc.fdf>
#include    <msvar.fdf>
#include    <toolsubs.h>

#include    "sclrottc.h"
#include    "srtccmd.h"

/*----------------------------------------------------------------------+
|                                                                       |
|   Private Global variables                                            |
|                                                                       |
+----------------------------------------------------------------------*/
Private SclTxCelInfo	SCTinfo;
Private	int     	commandName;
Private	ULong		dialogType;
Private	int		got_origin, txt, Ntxt, cell;
Private	BoolInt		scale;

/*----------------------------------------------------------------------+
|                                                                       |
|   Local function declarations                                         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void Single_doChange
(
int msg
);

/*----------------------------------------------------------------------+
|                                                                       |
| name       Fence_setSearchType                                        |
|                                                                       |
+----------------------------------------------------------------------*/
Private void  Fence_setSearchType
(
void
)
    {
    	static int	searchType[3];
    	int		count=0;

/*  mdlLocate_clearElemSearchMask (sizeof(searchType)/sizeof(int), searchType);*/

/*************************************************************************
  When the fence command is processed it does not seem to clear the search
  mask.  This is the reason for setting the search type to NULL.  Even the
  clear element search mask did not resolve this problem.  But the search
  routine seemed to work fine when single element processing occurred.
*************************************************************************/

    searchType[0] = 0;
    searchType[1] = 0;
    searchType[2] = 0;

    if (SCTinfo.sText == -1)
    	searchType[count++] = TEXT_ELM;
    if (SCTinfo.sNode == -1)
    	searchType[count++] = TEXT_NODE_ELM;
    if (SCTinfo.sCell == -1)
    	searchType[count++] = CELL_HEADER_ELM;

    /* --- set search criteria to find nothing and use Master file only --- */
    mdlLocate_noElemNoLocked ();

    /* --- now add specified elements to list --- */
    mdlLocate_setElemSearchMask (sizeof (searchType)/sizeof (int), searchType);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       singlelocate_ElmFilter                                     |
|                                                                       |
+----------------------------------------------------------------------*/
Private int  singlelocate_ElmFilter
(
LOCATE_Action       action , 
MSElement*          selElm , 
DgnModelRefP        modelRef , 
ULong               filePosition , 
DPoint3d*           pPoint , 
int                 viewNumber , 
HitPathP            hitPath , 
char*               rejectReason  
)
    {
	UInt32          filePos;
	DgnModelRefP	currFile = MASTERFILE;
	MSElementUnion	headElm;
	char		reason[]="can only work for cells and text";

    printf ("hit locate filter at %ld \n",action);

	strcpy (rejectReason,reason);

	/* during prelocate we passed the element that was originally located,
	   this could be any part of a cell. so you do not have to locate the
	   origin to accept the element the same comes true with text nodes
	   although the later problem is not as prevalent */

    if (selElm->hdr.ehdr.complex == 1)		/* is a complex element */
    	{
    	filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFile);
#if defined (MSVERSION) && (MSVERSION >= 0x550)
        mdlElement_read (&headElm, currFile, filePos);
#else
        mdlElement_read ((UShort *)&headElm, currFile, filePos);
#endif
	if ((headElm.hdr.ehdr.type == 7 && SCTinfo.sNode == -1) ||
	    (headElm.hdr.ehdr.type == 2 && SCTinfo.sCell == -1))
	    return LOCATE_ELEMENT_ACCEPT;
	}
	else if (selElm->hdr.ehdr.type == 17 && SCTinfo.sText == -1) 
	    return LOCATE_ELEMENT_ACCEPT;
	else
	    return LOCATE_ELEMENT_REJECT;

    return  LOCATE_ELEMENT_NEUTRAL;
    }

/*----------------------------------------------------------------------+*//**
* !!!Describe Function Completely!!!                                                                        *
*                                                                       *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   mark.anderson                               11/03           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  int buildAlignTMatrix
(
RotMatrix       *rMatrixP,
Transform       *tMatrixP
)
    {
    mdlTMatrix_getIdentity (tMatrixP);
    mdlRMatrix_invert (rMatrixP,rMatrixP);
    mdlTMatrix_rotateByRMatrix (tMatrixP,tMatrixP,rMatrixP);
    mdlRMatrix_fromView (rMatrixP,tcb->lstvw,FALSE);//align to last active view
    mdlRMatrix_invert (rMatrixP,rMatrixP);
    mdlTMatrix_rotateByAngles (tMatrixP,tMatrixP,0.0,0.0,tcb->actangle*fc_piover180);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       CellText_Change                                            |
|                                                                       |
+----------------------------------------------------------------------*/
Private int	CellText_Change
(
MSElementDescr	*edP
)
    {
    	int		    Elm_type, status=ERROR;
    	Dpoint3d	    origin;
	static Transform    OrigtMatrix;
	//MSElementDescr      *edP=NULL;
        RotMatrix           rMatrix;

    if(!got_origin)	/* got_origin not TRUE   */
    	{
    	mdlTMatrix_getIdentity (&OrigtMatrix);
	if(scale)
	    mdlTMatrix_scale (&OrigtMatrix, &OrigtMatrix, SCTinfo.sFact,
			      SCTinfo.sFact, SCTinfo.sFact);
	else
	    mdlTMatrix_rotateByAngles(&OrigtMatrix, &OrigtMatrix, 0.0, 0.0,
				      SCTinfo.sRot*fc_piover180);

	Elm_type = mdlElement_getType (&edP->el);	/* --- check element type --- */
        switch (Elm_type)
	    {
	    case TEXT_ELM:
	    	txt++;      	/* increment the text counter */
	    	mdlText_extract (&origin, NULL, NULL, NULL, NULL, &rMatrix, NULL,
			    	 NULL, NULL, NULL, &edP->el);
                buildAlignTMatrix (&rMatrix,&OrigtMatrix);
	    	break;
	    case TEXT_NODE_ELM:
	        Ntxt++; 	/* increment the text node counter */
		mdlTextNode_extract (&origin, NULL, NULL, NULL, NULL, NULL, &edP->el, edP);
	    	break;
	    case CELL_HEADER_ELM:
	        cell++;		/* increment the cell counter */
	    	mdlCell_extract (&origin, NULL, NULL, NULL, NULL, 0, &edP->el);
	    	break;
	    default:
	    	break;
	    }
    	got_origin = TRUE;
    	}
    if (got_origin) /* if true process element */
        {
        if  (TEXT_ELM == Elm_type)
            {
            mdlCurrTrans_begin ();
            mdlCurrTrans_identity ();
            mdlCurrTrans_rotateByView (tcb->lstvw);
	    	/* set TMatrix to the origin */
            mdlTMatrix_setOrigin (&OrigtMatrix, &origin);

            //status = mdlElement_transform (elP, elP, &OrigtMatrix);
            status = mdlElmdscr_transform (edP,&OrigtMatrix);
            mdlCurrTrans_end ();
            }
        else
            {
            mdlTMatrix_setOrigin (&OrigtMatrix, &origin);
//            status = mdlElement_transform (elP, elP, &OrigtMatrix);           
            status = mdlElmdscr_transform (edP,&OrigtMatrix);

            }
        return status;
        }
    else
        return ERROR;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       scaleElm													|
|                                                                       |
+----------------------------------------------------------------------*/
Private int	scaleElm
(
MSElementUnion	*elP,
void            *params,
DgnModelRefP    modelRefP,
MSElementDescr  *elmDscrP,
MSElementDescr  **newDescrPP,
ModifyElementSource elemSource
)
    {
    int 	status;
    
    mdlElmdscr_duplicate (newDescrPP,elmDscrP);
    
    status = CellText_Change (*newDescrPP);

    mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_SclTxCel, 8,
			 txt, Ntxt, cell);
    /* replace the element in the design file. The scale was the only 
       changes made to the element, this should not change the size */
    return  (status ? 0 : MODIFY_STATUS_REPLACEDSCR);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       All_acceptChange                                           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void All_acceptChange
(
void
)
    {
    	ULong	    elemAddr[50], eofPos, filePos ;
    	int	    scanWords, status, i, numAddr;
    	Scanlist    scanList;

    mdlScan_initScanlist (&scanList);
    mdlScan_noRangeCheck (&scanList);

    scanList.scantype	    = ELEMTYPE | NESTCELL;
    scanList.extendedType   = FILEPOS;

    if (SCTinfo.sText == -1)
	scanList.typmask[1] = TMSK1_TEXT;
    if (SCTinfo.sNode == -1)
	scanList.typmask[0] = TMSK0_TEXT_NODE;
    if (SCTinfo.sCell == -1)
	scanList.typmask[0] |= TMSK0_CELL_HEADER;

    eofPos  = mdlElement_getFilePos (FILEPOS_EOF, NULL);
    mdlScan_initialize (0, &scanList);

    /* loop through all elements in file */
    do
    	{
	scanWords = sizeof(elemAddr)/sizeof(short);
	status	  = mdlScan_file (elemAddr, &scanWords, sizeof(elemAddr), &filePos);
	numAddr   = scanWords / sizeof(short);

	for (i=0; i<numAddr; i++)
	    {
	    if (elemAddr[i] >= eofPos)
	    	break;
	    mdlModify_elementSingle (0, elemAddr[i], MODIFY_REQUEST_HEADERS,
				     MODIFY_ORIG, scaleElm, NULL, 0L);
	    /* after processing all components of a element set got_origin=FALSE */
	    got_origin = FALSE;
	    }
    	} while (status == BUFF_FULL);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       All_doChange                                               |
|                                                                       |
+----------------------------------------------------------------------*/
Private void All_doChange
(
int msg
)
    {
    mdlState_startPrimitive (All_acceptChange, All_doChange, msg, 1);
    /*  <datapoint> call All_acceptChange */
    /*  <reset> call this function  */
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       Single_acceptChange                                        |
|                                                                       |
+----------------------------------------------------------------------*/
Private void  Single_acceptChange
(
void
)
    {
    	UInt32       filePos, *filePositions;
    	DgnModelRefP    currFile = MASTERFILE;
    	int	    i, numSelected;
    	DgnModelRefP *fileNum;
	MSElement   selElm;

    if (mdlSelect_isActive())	/* if selection set is active */
    	{
	/* populate filePostion with an array that contain each element
	   in the file selection set.  numSelected contains the number
	   of elements in the selection set*/

	mdlSelect_returnPositions (&filePositions, &fileNum, &numSelected);
        for (i = 0; i < numSelected; i++)
	    {
	    if (!mdlModelRef_isActiveModel (fileNum[i]))
		continue;

	    /* read the element at file position */
#if defined (MSVERSION) && (MSVERSION >= 0x550)
            mdlElement_read (&selElm, currFile, filePositions[i]);
#else
            mdlElement_read ((UShort *)&selElm, currFile, filePositions[i]);
#endif
	    /* check the header of each element and make sure it is one of
	       the types to be modified */
	    if ((selElm.ehdr.type == 17 && SCTinfo.sText == -1) || 
	    	(selElm.ehdr.type == 7  && SCTinfo.sNode == -1) ||
	    	(selElm.ehdr.type == 2  && SCTinfo.sCell == -1))
	    	{
		/* return the file postion after element has been scaled */
    		filePos = mdlModify_elementSingle (currFile, filePositions[i],
						   MODIFY_REQUEST_HEADERS,
						   MODIFY_ORIG, scaleElm,
						   NULL, 0L);
	    	/* read the element at file position */
#if defined (MSVERSION) && (MSVERSION >= 0x550)
	    	mdlElement_read (&selElm, currFile, filePos);
#else
	    	mdlElement_read ((UShort *)&selElm, currFile, filePos);
#endif
	    	
	    	got_origin = FALSE;
		}
	    }
    	free(filePositions);
    	free(fileNum);
    	}
    else
    	{
	/* find the file position of the element last located */
	filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFile);
    	mdlModify_elementSingle (currFile, filePos, MODIFY_REQUEST_HEADERS,
				    MODIFY_ORIG, scaleElm, NULL, 0L);
	got_origin = FALSE;
    	}

    Single_doChange(0);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       Single_doChange                                            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void Single_doChange
(
int msg
)
    {
    /*  locate all unlocked & displayable elements in the master file */
    mdlLocate_normal();
    got_origin = FALSE;
    mdlState_startModifyCommand (Single_doChange, Single_acceptChange, NULL,
			    	 NULL, NULL, msg, 1, TRUE, 1);
    mdlLocate_setFunction (LOCATE_POSTLOCATE, singlelocate_ElmFilter);
    /* set the internal locate pointers to start at beginning of file */
    mdlLocate_init();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       Fence_acceptChange                                         |
|                                                                       |
+----------------------------------------------------------------------*/
Private int	Fence_acceptChange
(
void
)
    {
    	ULong   filePos;

    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, NULL);
    mdlModify_elementSingle (0, filePos, MODIFY_REQUEST_HEADERS,
			     MODIFY_ORIG, scaleElm, NULL, 0L);
    got_origin = FALSE;
    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name       Fence_doChange                                             |
|                                                                       |
+----------------------------------------------------------------------*/
Private void Fence_doChange
(
int msg
)
    {
    Fence_setSearchType();
    got_origin = FALSE;
    mdlState_startFenceCommand (Fence_acceptChange, NULL, NULL,
				Fence_doChange, msg, 1, FENCE_NO_CLIP);
    }

/*----------------------------------------------------------------------+
|                                                                       |
|       Command Handling routines                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          ORIGIN_SCALE						|
|                                                                       |
+----------------------------------------------------------------------*/
void    origin_scale
(
char*   unparsedP
)
//cmdNumber       CMD_ORIGIN_SCALE
    {
    scale=TRUE;
    txt = Ntxt = cell = 0;	/* set cell, text and textNode counters to 0 */
    if(SCTinfo.sType == 0)      /* 0=all 1=single 2=fence */
	All_doChange(2);
    else if(SCTinfo.sType == 1) /* single */
        Single_doChange(3);
    else		    	/* fence  */
        Fence_doChange(4);
    }
/*----------------------------------------------------------------------+
|                                                                       |
| name          ORIGIN_ROTATE                                           |
|                                                                       |
+----------------------------------------------------------------------*/
void    origin_rotate
(
char*   unparsedP
)
//cmdNumber       CMD_ORIGIN_ROTATE
{
    scale=FALSE;
    txt = Ntxt = cell = 0;		/* set cell, text and textNode counters to 0 */
    if(SCTinfo.sType == 0)      /* 0=all 1=single 2=fence */
	All_doChange(5);
    else if(SCTinfo.sType == 1) /* single */
        Single_doChange(6);
    else						/* fence  */
        Fence_doChange(7);
}

/*----------------------------------------------------------------------+
|                                                                       |
|       Dialog Box Hooks                                                |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          scltxcel_dialogHook                                     |
|                                                                       |
+----------------------------------------------------------------------*/
Private  void   scltxcel_dialogHook
(
DialogMessage   *dmP
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_DESTROY:
	    mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
	    break;

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          ProcessType   Hook Function                             |
|                                                                       |
+----------------------------------------------------------------------*/
Private void ProcessType
(
DialogItemMessage *dimP
)
    {
    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
    	{
        case DITEM_MESSAGE_BUTTON:	    
	    Fence_setSearchType();
	    break;

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          fenceButton   Hook Function				|
|                                                                       |
+----------------------------------------------------------------------*/
Private void fenceButton
(
DialogItemMessage *dimP
)
    {
    	DialogItem  *diP;
	int	    status;

    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
    	{
        case DITEM_MESSAGE_CREATE:
        case DITEM_MESSAGE_INIT:
        case DITEM_MESSAGE_BUTTON:
        case DITEM_MESSAGE_SETSTATE:
	case DITEM_MESSAGE_STATECHANGED:
	    /* enable the fence lock modes */
	    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_OptionButton,
		    OPTIONBUTTONID_LockFenceMode, NULL);
	    status = mdlDialog_itemSetEnabledState (dimP->db, diP->itemIndex,
	    	    TRUE, FALSE);
	    if (!tcb->fence)		/* no fence is defined */
	    	{
	    	if (SCTinfo.sType == 2) /* if option button set to fence then set to single */
	    	SCTinfo.sType = 1;
	    		/* disable the fence option button so it may not be selected */
	    	status = mdlDialog_optionButtonSetEnabled (dimP->dialogItemP->rawItemP,
							   2, FALSE);
		}
	    else    /* there is a fence defined enable the fence option button
		       so it may be selected */
	    	status = mdlDialog_optionButtonSetEnabled (dimP->dialogItemP->rawItemP,
							   2, TRUE);
	    if (SCTinfo.sType != 2) /* option button not set to fence disable
				       the fence lock mode option button */
	    	{
   		diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_OptionButton,
						    OPTIONBUTTONID_LockFenceMode,
						    NULL);
	    	status = mdlDialog_itemSetEnabledState (dimP->db, diP->itemIndex,
						    	FALSE, FALSE);
	    	}
	    if (tcb->current_command == CMD_ORIGIN_ROTATE)
	        origin_rotate("");
	    if (tcb->current_command == CMD_ORIGIN_SCALE)
	        origin_scale("");
	    break;
    	default:
	    dimP->msgUnderstood = FALSE;
	    break;
    	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          main                                                    |
|                                                                       |
+----------------------------------------------------------------------*/
Private DialogHookInfo uHooks[] =
    {
    {HOOKID_SclTxCel,			(PFDialogHook)scltxcel_dialogHook},
    {HOOKITEMID_ToggleProcessType,	(PFDialogHook)ProcessType},
    {HOOKITEMID_FenceProcessing,	(PFDialogHook)fenceButton}
    };

extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
        SymbolSet 		*setP;
    	RscFileHandle	rscFileH;
        DPoint3d            tPoint;

    /* open the resource file that we came out of */
    if (mdlResource_openFile (&rscFileH, NULL, 0) != SUCCESS)
	{
    	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
			    	  mdlSystem_getCurrTaskID(), TRUE);
	return (ERROR);
	}

    Private MdlCommandNumber  commandNumbers [] =
    {
    {origin_rotate,CMD_ORIGIN_ROTATE},
    {origin_scale,CMD_ORIGIN_SCALE},
    0,
    };
    
    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);


    /* load the command table */
    if (mdlParse_loadCommandTable (NULL) == NULL)
	{
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_SclTxCel, 9);
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
			    	  mdlSystem_getCurrTaskID(), TRUE);
	return (ERROR);
	}

    /* set up the variables we are going to set from dialog boxes */
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, FALSE);
    mdlDialog_publishComplexVariable (setP, "scltxcelinfo", "SCTinfo", &SCTinfo);

    /* publish our hooks */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    SCTinfo.sText = -1;
    SCTinfo.sNode = -1;
    SCTinfo.sCell = -1;
    SCTinfo.sType = 1;
    mdlParams_getActive (&tPoint,ACTIVEPARAM_SCALE);
    SCTinfo.sFact = tPoint.x;
    mdlParams_getActive (&SCTinfo.sRot,ACTIVEPARAM_ANGLE);
    //SCTinfo.sRot = tcb->actangle;

    /* start the dialog box */
    mdlState_registerStringIds(MESSAGELISTID_SclTxCel,MESSAGELISTID_SclTxCel);
    if (NULL == mdlDialog_open (NULL, DIALOGID_SclRotTCFrame))
	{
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_SclTxCel, 10);
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
			    	  mdlSystem_getCurrTaskID(), TRUE);
	return (ERROR);
        }
    return (SUCCESS);
    }
