/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/sclrottc/sclrottc.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   sclrottc.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:53 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Scale/Rotate Text/Cell Example Resources		    	|								|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|																		|
|   Include Files   													|
|																		|
+----------------------------------------------------------------------*/
#include <cmdlist.h>
#include <dlogbox.h>
#include <dlogids.h>

#include "sclrottc.h"
#include "srtccmd.h"
#include "srtctxt.h"

/*----------------------------------------------------------------------+
|																		|
|   Local Defines														|
|																		|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|																		|
|   prof Dialog Box    						        					|
|																		|
+----------------------------------------------------------------------*/
#define X1		2 * XC
#define X2		12 * XC
#define X3		16 * XC

#define Y1		GENY(1)
#define Y2		GENY(2.2)
#define Y3		GENY(3.4)
#define Y4		GENY(4.6)
#define Y5		GENY(6)
#define Y6		GENY(4)

#define BW1     10 * XC

DItem_IconCmdRsc ICONCMDID_RotTxCel =
    {
    NOHELP, MHELP, 0,
    CMD_ORIGIN_ROTATE, OTASKID, "", "",
	{
#   	if defined (MSVERSION) && (MSVERSION < 0x551)
	{{X2,Y4,BW1,0},   Text,	       TEXTID_Rot,          ON, 0, TXT_RotateBy, ""},
	{{X1,Y1,0,0},	  ToggleButton,TOGGLEID_Text,       ON, 0, TXT_RotateText, ""},
	{{X1,Y2,0,0},	  ToggleButton,TOGGLEID_Node,       ON, 0, TXT_RotateTextNode, ""},
	{{X1,Y3,0,0},	  ToggleButton,TOGGLEID_Cell,       ON, 0, TXT_RotateCell, ""},
	{{X3,Y5, 0, 0},   OptionButton,OPTIONBUTTONID_LockFenceMode,ON, 0, "\0", ""},
	{{X1, Y5, BW1, 0},OptionButton,OPTIONBUTTONID_PType,ON, 0, "", ""},
#   	endif
	}
#if defined (MSVERSION) && (MSVERSION >= 0x550)
    }
    extendedAttributes
    {{
    {EXTATTR_FLYTEXT, TXT_OriginRotateTextCells}, 
    {EXTATTR_BALLOON, TXT_OriginRotateTextCells}, 
    }
#endif
    };
#if defined (MSVERSION) && (MSVERSION >= 0x551)
CmdItemListRsc CMD_ORIGIN_ROTATE =
    {{
    {{X2,Y4,BW1,0},   Text,	   TEXTID_Rot,          ON, 0, TXT_RotateBy, ""},
    {{X1,Y1,0,0},     ToggleButton,TOGGLEID_Text,       ON, 0, TXT_RotateText, ""},
    {{X1,Y2,0,0},     ToggleButton,TOGGLEID_Node,       ON, 0, TXT_RotateTextNode, ""},
    {{X1,Y3,0,0},     ToggleButton,TOGGLEID_Cell,       ON, 0, TXT_RotateCell, ""},
    {{X3,Y5, 0, 0},   OptionButton,OPTIONBUTTONID_LockFenceMode,ON, 0, "\0", ""},
    {{X1, Y5, BW1, 0},OptionButton,OPTIONBUTTONID_PType,ON, 0, "", ""},
    }};
#endif

DItem_IconCmdRsc ICONCMDID_SclTxCel =
    {
    NOHELP, MHELP, 0,
    CMD_ORIGIN_SCALE, OTASKID, "", "",
	{
#   	if defined (MSVERSION) && (MSVERSION < 0x551)
	{{X2,Y4,BW1,0},   Text,	       TEXTID_Fact,	    ON, 0, TXT_ScaleBy, ""},
	{{X1,Y1,0,0},	  ToggleButton,TOGGLEID_Text,       ON, 0, TXT_ScaleText, ""},
	{{X1,Y2,0,0},	  ToggleButton,TOGGLEID_Node,       ON, 0, TXT_ScaleTextNode, ""},
	{{X1,Y3,0,0},	  ToggleButton,TOGGLEID_Cell,       ON, 0, TXT_ScaleCell, ""},
	{{X3,Y5, 0, 0},   OptionButton,OPTIONBUTTONID_LockFenceMode,ON, 0, "\0", ""},
	{{X1, Y5, BW1, 0},OptionButton,OPTIONBUTTONID_PType,ON, 0, "", ""},
#   	endif
	}

#if defined (MSVERSION) && (MSVERSION >= 0x550)
    }
    extendedAttributes
    {{
    {EXTATTR_FLYTEXT, TXT_OriginScaleTextCells}, 
    {EXTATTR_BALLOON, TXT_OriginScaleTextCells}, 
    }
#endif
    };
#if defined (MSVERSION) && (MSVERSION >= 0x551)
CmdItemListRsc CMD_ORIGIN_SCALE =
    {{
    {{X2,Y4,BW1,0},   Text,	   TEXTID_Fact,	    ON, 0, TXT_ScaleBy, ""},
    {{X1,Y1,0,0},     ToggleButton,TOGGLEID_Text,   ON, 0, TXT_ScaleText, ""},
    {{X1,Y2,0,0},     ToggleButton,TOGGLEID_Node,   ON, 0, TXT_ScaleTextNode, ""},
    {{X1,Y3,0,0},     ToggleButton,TOGGLEID_Cell,   ON, 0, TXT_ScaleCell, ""},
    {{X3,Y5, 0, 0},   OptionButton,OPTIONBUTTONID_LockFenceMode,ON, 0, "\0", ""},
    {{X1, Y5, BW1, 0},OptionButton,OPTIONBUTTONID_PType,ON, 0, "", ""},
    }};
#endif

#undef  X1
#undef  X2
#undef  X3

#undef  Y1
#undef  Y2
#undef  Y3
#undef  Y4
#undef  Y5

#undef  BW1


#if defined (MSVERSION) && (MSVERSION < 0x550)
DialogBoxRsc DIALOGID_SclRotTCFrame =
{
    DIALOGATTR_DEFAULT | DIALOGATTR_NORIGHTICONS,
    XC, YC, NOHELP, MHELP, HOOKID_SclTxCel, NOPARENTID,
    TXT_ScaleRotateCellsText,
	{
	{{0, 0, 0, 0}, IconCmdFrame, ICONCMDFRAMEID_SclRotTC, ON, 0, "", ""},
	}
};

DItem_IconCmdFrameRsc ICONCMDFRAMEID_SclRotTC =
{
    1, 2, NOHELP, MHELP, TXT_TESTFrame,
	{
	{IconCmdPaletteX,	    	ICONCMDPALETTEID_SclTxCel},
	{IconCmd,			ICONCMDID_Undolst},
	}
};

DItem_IconCmdPaletteXRsc	ICONCMDPALETTEID_SclTxCel =
{
    4, 1, 0, NOHELP, MHELP, NOHOOK, NOARG, 0, TXT_ScaleTextCells,
	{
	{ICONCMDID_RotTxCel,			"SCLROTTC"},
	{ICONCMDID_SclTxCel,			"SCLROTTC"},
 	{ICONCMDID_Scl3pt,			"SCLROTTC"},
 	{ICONCMDID_A2pt,			"SCLROTTC"},
	}
};

#else /* Using tool box definitions */

DialogBoxRsc DIALOGID_SclRotTCFrame =
{
    DIALOGATTR_DEFAULT  | DIALOGATTR_NORIGHTICONS | DIALOGATTR_AUTOOPEN | 
    DIALOGATTR_DOCKABLE | DIALOGATTR_BOTHVIRTUAL,
    XC, YC, NOHELP, MHELP, HOOKID_SclTxCel, NOPARENTID, 
    TXT_SCLTXCEL,
	{
	{{0, 0, 0, 0}, IconCmdFrameX, ICONCMDFRAMEID_SclRotTC, ON, 0, "", ""},
	}
};

DItem_IconCmdFrameXRsc ICONCMDFRAMEID_SclRotTC =
    {
    1, 2, NOHELP, MHELP, 0, TXT_SCLROTTC,
	{
	{IconCmd,		ICONCMDID_Undolst,		"SCLROTTC"},
	{ToolBox,		TOOLBOXID_SclTxCel,		"SCLROTTC"},
 	{IconCmd,		ICONCMDID_Scl3pt,		"SCLROTTC"},
 	{IconCmd,		ICONCMDID_A2pt,			"SCLROTTC"},
	}
    };

DialogBoxRsc TOOLBOXID_SclTxCel =
	{
    DIALOGATTR_TOOLBOXCOMMON, 0, 0,
    NOHELP, MHELP, 
    NOHOOK, NOPARENTID, 
    TXT_SCLTXCEL,
	{
	{{0, 0, 0, 0}, ToolBox, TOOLBOXID_SclTxCel, ON, 0, "", ""},
	}
};

DItem_ToolBoxRsc	TOOLBOXID_SclTxCel =
{
    NOHELP, MHELP, NOHOOK, NOARG, 0, TXT_ScaleTextCells,
	{
	{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_RotTxCel, 	ON, 0, "", "owner=\"SCLROTTC\""},
	{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_SclTxCel, 	ON, 0, "", "owner=\"SCLROTTC\""},
 	{{ 0, 0, 0, 0},	IconCmd, ICONCMDID_Scl3pt,	ON, 0, "", "owner=\"SCLROTTC\""},
 	{{ 0, 0, 0, 0},	IconCmd, ICONCMDID_A2pt,	ON, 0, "", "owner=\"SCLROTTC\""},
	}
};

#endif

/*----------------------------------------------------------------------+
|																		|
|   OptionButton Item Resources											|
|																		|
+-----------------------------------------------------------------------*/
DItem_OptionButtonRsc  OPTIONBUTTONID_PType =
{
    NOSYNONYM, NOHELP, MHELP, HOOKITEMID_FenceProcessing, OPTNBTNATTR_NEWSTYLE,
    "",
    "SCTinfo.sType",
	{
        {NOTYPE, NOICON, NOCMD, MCMD, 0, NOMASK, ON, TXT_All},
        {NOTYPE, NOICON, NOCMD, MCMD, 1, NOMASK, ON, TXT_Single},
        {NOTYPE, NOICON, NOCMD, MCMD, 2, NOMASK, ON, TXT_Fence},
    }
};

/*----------------------------------------------------------------------+
|																		|
|   Toggle Button														|
|																		|
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEID_Text =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_ToggleProcessType,
    NOARG, NOMASK, NOINVERT,
    "",
    "SCTinfo.sText"
    };
DItem_ToggleButtonRsc TOGGLEID_Node =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_ToggleProcessType,
    NOARG, NOMASK, NOINVERT,
    "",
    "SCTinfo.sNode"
    };
DItem_ToggleButtonRsc TOGGLEID_Cell =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_ToggleProcessType,
    NOARG, NOMASK, NOINVERT,
    "",
    "SCTinfo.sCell"
    };

/*----------------------------------------------------------------------+
|																		|
|   Text Item Resources     											|
|																		|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_Fact = 
{
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 
    7, "%-f", "%lf", "0", "", NOMASK, NOCONCAT,
    "", "SCTinfo.sFact"
};
DItem_TextRsc TEXTID_Rot = 
{
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 
    7, "%-f", "%lf", "-360.0", "360.0", NOMASK, NOCONCAT,
    "", "SCTinfo.sRot"
};

/*----------------------------------------------------------------------+
|																		|
|  Icon Cmd Resources                                                   |
|																		|
+----------------------------------------------------------------------*/
IconCmdSmallRsc ICONCMDID_SclTxCel =
    {
    23, 23, FORMAT_MONOBITMAP, BLACK_INDEX,
    "scltxcel",
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x07, 0xff, 0xff, 0xc8, 0x00,
	0x00, 0x90, 0x00, 0x01, 0x20, 0x00, 0x02, 0x44,
	0xcc, 0xc4, 0x88, 0x00, 0x09, 0x00, 0x41, 0x12,
	0x21, 0xc0, 0x24, 0x01, 0x04, 0x48, 0x80, 0x00,
	0x91, 0x33, 0x31, 0x20, 0x00, 0x02, 0x40, 0x00,
	0x04, 0x80, 0x00, 0x09, 0xff, 0xff, 0xf0, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 
	}
    };

IconCmdLargeRsc ICONCMDID_SclTxCel =
    {
    31, 31, FORMAT_MONOBITMAP, BLACK_INDEX,
    "scltxcel",
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f,
	0xff, 0xff, 0xfe, 0x10, 0x00, 0x00, 0x04, 0x20,
	0x00, 0x00, 0x08, 0x40, 0x00, 0x00, 0x10, 0x85,
	0x99, 0x94, 0x21, 0x08, 0x00, 0x08, 0x42, 0x10,
	0x00, 0x00, 0x84, 0x00, 0x20, 0x01, 0x08, 0x40,
	0x40, 0x42, 0x10, 0x03, 0xe0, 0x04, 0x21, 0x01,
	0x01, 0x08, 0x40, 0x02, 0x00, 0x10, 0x84, 0x00,
	0x04, 0x21, 0x00, 0x00, 0x00, 0x42, 0x16, 0x66,
	0x50, 0x84, 0x00, 0x00, 0x01, 0x08, 0x00, 0x00,
	0x02, 0x10, 0x00, 0x00, 0x04, 0x20, 0x00, 0x00,
	0x08, 0x7f, 0xff, 0xff, 0xf0, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 
	}
    };

IconCmdSmallRsc ICONCMDID_RotTxCel =
    {
    23, 23, FORMAT_MONOBITMAP, BLACK_INDEX,
    "rottxcel",
	{
	0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x01, 0x40,
	0x00, 0x04, 0x40, 0x00, 0x10, 0x40, 0x00, 0x40,
	0x40, 0x01, 0x01, 0x00, 0x04, 0x04, 0x00, 0x04,
	0x10, 0x00, 0x04, 0x4f, 0x00, 0x05, 0x1c, 0x00,
	0x04, 0x38, 0x00, 0x00, 0x48, 0x00, 0x00, 0x10,
	0x00, 0x00, 0x20, 0x00, 0x0f, 0xf8, 0x00, 0x10,
	0x10, 0x00, 0x20, 0x20, 0x00, 0x40, 0x40, 0x00,
	0x80, 0x80, 0x01, 0x01, 0x00, 0x03, 0xfe, 0x00,
	0x00, 0x00, 0x00, 
	}
    };

IconCmdLargeRsc ICONCMDID_RotTxCel =
    {
    31, 31, FORMAT_MONOBITMAP, BLACK_INDEX,
    "rottxcel",
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x08, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00,
	0x00, 0x88, 0x00, 0x00, 0x02, 0x08, 0x00, 0x00,
	0x08, 0x08, 0x00, 0x00, 0x20, 0x08, 0x00, 0x00,
	0x80, 0x20, 0x00, 0x02, 0x00, 0x80, 0x00, 0x08,
	0x02, 0x00, 0x00, 0x08, 0x08, 0x00, 0x00, 0x08,
	0x23, 0xe0, 0x00, 0x08, 0x87, 0x00, 0x00, 0x0a,
	0x0e, 0x00, 0x00, 0x08, 0x12, 0x00, 0x00, 0x00,
	0x22, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
	0x08, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x1f, 0xfc, 0x00, 0x00, 0x20,
	0x08, 0x00, 0x00, 0x40, 0x10, 0x00, 0x00, 0x80,
	0x20, 0x00, 0x01, 0x00, 0x40, 0x00, 0x02, 0x00,
	0x80, 0x00, 0x07, 0xff, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 
	}
    };

DItem_IconCmdRsc	ICONCMDID_Scl3pt =
    {
    NOHELP, OHELPTASKIDCMD, 0,
    CMD_ACTIVE_SCALE_DISTANCE, MTASKID , "",
    "",
	{	
	}   
#if defined (MSVERSION) && (MSVERSION >= 0x550)
    }
    extendedAttributes
    {{
    {EXTATTR_FLYTEXT, TXT_ActiveScaleby3Points}, 
    {EXTATTR_BALLOON, TXT_ActiveScaleby3Points},
    }
#endif
    };
#if defined (MSVERSION) && (MSVERSION >= 0x551)
CmdItemListRsc CMD_ACTIVE_SCALE_DISTANCE =
    {{
    /* For future popdown items */
    }};
#endif

/* ----- scl3pt ------ */
IconCmdSmallRsc ICONCMDID_Scl3pt =
    {
    23, 23, FORMAT_MONOBITMAP, BLACK_INDEX,
    "scl3pt",
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x40, 0x1f,
	0xf3, 0x80, 0x00, 0x04, 0x00, 0x00, 0x0e, 0x01,
	0x00, 0x00, 0x06, 0x00, 0x00, 0x04, 0x00, 0x00,
	0x08, 0x00, 0x00, 0x38, 0x00, 0x0e, 0x00, 0x00,
	0x04, 0x7f, 0xff, 0x98, 0x00, 0x00, 0x10, 0x00,
	0x00, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 
	}
    };
/* ----- scl3pt ------ */
IconCmdLargeRsc ICONCMDID_Scl3pt =
    {
    31, 31, FORMAT_MONOBITMAP, BLACK_INDEX,
    "scl3pt",
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0xe0, 0x00, 0x00, 0x00, 0x40, 0x00, 0xff,
	0xf3, 0x80, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
	0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00,
	0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x80, 0x00,
	0x00, 0x01, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x07, 0x0f, 0xff, 0xff,
	0xc2, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00,
	0x08, 0x00, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 
	}
    };
DItem_IconCmdRsc	ICONCMDID_A2pt =
    {
    NOHELP, OHELPTASKIDCMD, 0,
    CMD_ACTIVE_ANGLE_PT2, MTASKID , "",
    "",
	{	
	}   
#if defined (MSVERSION) && (MSVERSION >= 0x550)
    }
    extendedAttributes
    {{
    {EXTATTR_FLYTEXT, TXT_ActiveAngleby2Points}, 
    {EXTATTR_BALLOON, TXT_ActiveAngleby2Points},
    }
#endif
    };
#if defined (MSVERSION) && (MSVERSION >= 0x551)
CmdItemListRsc CMD_ACTIVE_ANGLE_PT2 =
    {{
    /* For future popdown items */
    }};
#endif

/* ----- A2pt ------ */
IconCmdSmallRsc ICONCMDID_A2pt =
    {
    23, 23, FORMAT_MONOBITMAP, BLACK_INDEX,
    "A2pt",
	{
	0x00, 0x00, 0x00, 0x00, 0x03, 0x80, 0x00, 0x01,
	0x00, 0x00, 0x0e, 0x00, 0x00, 0x10, 0x00, 0x00,
	0x38, 0x00, 0x00, 0x04, 0x00, 0x00, 0x30, 0x00,
	0x00, 0x80, 0x00, 0x02, 0x00, 0x00, 0x18, 0x01,
	0x00, 0x40, 0x06, 0x03, 0x00, 0x04, 0x08, 0x00,
	0x08, 0x20, 0x00, 0x39, 0x80, 0x00, 0x04, 0x00,
	0x00, 0x30, 0x00, 0x00, 0x80, 0x00, 0x00, 0xe7,
	0x39, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 
	}
    };

/* ----- A2pt ------ */
IconCmdLargeRsc ICONCMDID_A2pt =
    {
    31, 31, FORMAT_MONOBITMAP, BLACK_INDEX,
    "A2pt",
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4e, 0x00,
	0x00, 0x01, 0x04, 0x00, 0x00, 0x0c, 0x38, 0x00,
	0x00, 0x20, 0x40, 0x00, 0x00, 0x80, 0xe0, 0x00,
	0x06, 0x00, 0x00, 0x00, 0x10, 0x00, 0x02, 0x00,
	0x40, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x08, 0x08,
	0x00, 0x00, 0x10, 0x20, 0x00, 0x00, 0x71, 0x80,
	0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x10, 0x00,
	0x00, 0x00, 0xc0, 0x00, 0x00, 0x02, 0x00, 0x00,
	0x00, 0x07, 0x8f, 0x1e, 0x3c, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 
	}
    };

DItem_IconCmdRsc	ICONCMDID_Undolst =
    {
    NOHELP, OHELPTASKIDCMD, 0,
    CMD_UNDO, MTASKID , "",
    "",
	{	
	}   
#if defined (MSVERSION) && (MSVERSION >= 0x550)
    }
    extendedAttributes
    {{
    {EXTATTR_FLYTEXT, TXT_Undo}, 
    {EXTATTR_BALLOON, TXT_Undo},
    }
#endif
    };
#if defined (MSVERSION) && (MSVERSION >= 0x551)
CmdItemListRsc CMD_UNDO =
    {{
    /* For future popdown items */
    }};
#endif

/* ----- Undolst ------ */
IconCmdSmallRsc ICONCMDID_Undolst =
    {
    23, 23, FORMAT_MONOBITMAP, BLACK_INDEX,
    "Undolst",
	{
	0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0xe0,
	0x00, 0x03, 0xff, 0x00, 0x0f, 0xfe, 0x00, 0x0f,
	0xfe, 0x00, 0x0e, 0x1c, 0x00, 0x0c, 0x1c, 0x00,
	0x00, 0x38, 0x00, 0x00, 0xe0, 0x7f, 0xff, 0xc0,
	0xff, 0xff, 0x01, 0xff, 0xfc, 0x00, 0x00, 0x00,
	0x12, 0x97, 0x18, 0x25, 0xa9, 0x48, 0x4b, 0x52,
	0x90, 0x95, 0xa5, 0x21, 0x2b, 0x4a, 0x42, 0x56,
	0x94, 0x87, 0xa5, 0xc6, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 
	}
    };

/* ----- Undolst ------ */
IconCmdLargeRsc ICONCMDID_Undolst =
    {
    31, 31, FORMAT_MONOBITMAP, BLACK_INDEX,
    "Undolst",
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00,
	0x3f, 0xff, 0x80, 0x00, 0x7f, 0xff, 0x80, 0x00,
	0x70, 0x03, 0x80, 0x00, 0x60, 0x03, 0x80, 0x00,
	0x40, 0x03, 0x00, 0x00, 0x00, 0x06, 0x25, 0x2e,
	0x30, 0x0c, 0x4b, 0x52, 0x90, 0x38, 0x96, 0xa5,
	0x27, 0xe1, 0x2b, 0x4a, 0x4f, 0x82, 0x56, 0x94,
	0x80, 0x04, 0xad, 0x29, 0x00, 0x0f, 0x4b, 0x8c,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 
	}
	};

