/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/sclrottc/sclrottc.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/sclrottc/sclrottc.h_v  $
|   $Workfile:   sclrottc.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:53 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Scale/Rotate Text & Cell Header File			    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__sclrottcH__)
#define __sclrottcH__

#include    <basetype.h>
#include    <rscdefs.h>

#define		DIALOGID_SclRotTCFrame      	1

#define		TOOLBOXID_SclTxCel	    	2
#define		TOOLBOXID_RotTxCel	    	3

#define		TEXTID_Fact		    	1
#define		TEXTID_Rot		    	2

#define     	TOGGLEID_Text		    	1
#define     	TOGGLEID_Node			2
#define     	TOGGLEID_Cell		    	3

#define		OPTIONBUTTONID_PType	    	1

#define		ICONCMDFRAMEID_SclRotTC     	2

#define		ICONCMDPALETTEID_SclTxCel	3

#define		ICONCMDID_SclTxCel	    	1
#define		ICONCMDID_Scl3pt	    	2
#define		ICONCMDID_RotTxCel	    	3
#define		ICONCMDID_A2pt		    	4
#define		ICONCMDID_Undolst	    	5

#define     	HOOKID_SclTxCel		    	1
#define		HOOKITEMID_ToggleProcessType  	2
#define		HOOKITEMID_FenceProcessing  	3

#define     	STRINGID_Errors		    	1

#define     	MESSAGELISTID_SclTxCel      	1

typedef struct scltxcelinfo
    {
    int		sText;
    int		sCell;
    int		sNode;
    int		sType;
    double  	sRot;
    double  	sFact;
    } SclTxCelInfo;

#endif /* !defined (__sclrottcH__) */
