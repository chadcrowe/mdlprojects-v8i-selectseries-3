/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/sclrottc/srtctyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/sclrottc/srtctyp.mtv  $
|   $Workfile:   srtctyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:53 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Scale/Rotate Text & Scale Published Structures		    	|
|									|
+----------------------------------------------------------------------*/
#include    "sclrottc.h"
    
publishStructures (scltxcelinfo);
