/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/TextMod/TextMod.cs,v $
|    $RCSfile: TextMod.cs,v $
|   $Revision: 1.4 $
|       $Date: 2006/03/17 21:01:07 $
|     $Author: BernMcCarty $
|
|  $Copyright: (c) 2006 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
using System;
using System.Windows.Forms;
using BMW=Bentley.MicroStation.WinForms;
using BMI=Bentley.MicroStation.InteropServices;
using BCOM=Bentley.Interop.MicroStationDGN;

namespace TextMod
{

/// <summary>When loading an AddIn MicroStation looks for a class
/// derived from AddIn.</summary>
[Bentley.MicroStation.AddInAttribute
    (KeyinTree="TextMod.commands.xml", MdlTaskID="TextModEx")]
public sealed class TextMod : Bentley.MicroStation.AddIn
{
static TextMod s_App = null;

/// <summary>Private constructor required for all AddIn classes derived from 
/// Bentley.MicroStation.AddIn.</summary>
private TextMod
(
System.IntPtr mdlDesc
) :
base(mdlDesc)
    {
    s_App = this;
    }

internal static TextMod Instance
    {
    get { return s_App; }
    }

protected override int Run
(
System.String[] commandLine
)
    {
    return 0;
    }

/// <summary>Uses the Primary Interop Assembly to get a list of
/// elements to process.</summary>
internal static BCOM.ElementEnumerator GetElementEnumerator ()
    {
    BCOM.Application    app = BMI.Utilities.ComApp;
    
    if (app.ActiveDesignFile.Fence.IsDefined)
        return app.ActiveDesignFile.Fence.GetContents (false, false);
        
    if (app.ActiveModelReference.AnyElementsSelected)
        {
        BCOM.ElementEnumerator ee = app.ActiveModelReference.GetSelectedElements ();
        app.ActiveModelReference.UnselectAllElements ();
        return ee;
        }
        
    DialogResult result = MessageBox.Show (new BMI.MicroStationWin32 (), 
            "Process all elements in active model?", "Element Transform Example", MessageBoxButtons.YesNo);

    if (result == DialogResult.OK)
        {
        BCOM.ElementScanCriteria sc = new BCOM.ElementScanCriteriaClass ();
        sc.ExcludeAllTypes ();
        sc.IncludeType (BCOM.MsdElementType.Text);
        
        return app.ActiveModelReference.GraphicalElementCache.Scan (null);
        }
        
    return null;                    
    }
    
}  // End of TextMod class

/// <summary>Class used for running key-ins.  The key-ins
/// XML file provides the name of the class and the methods.
/// </summary>
public class KeyinCommands
{
/// <summary>Runs the change case command. </summary>
public static void ChangeToLower (System.String unparsed)
    {
    BCOM.ElementEnumerator ee = TextMod.GetElementEnumerator ();
    
    if (ee == null)
        return;

    BCOM.Application    app = BMI.Utilities.ComApp;
    //  Associate this with Undo
    app.CommandState.StartPrimitive (new Dummy (), false);
    app.CommandState.CommandName = "TextMod Lower";
    
    while (ee.MoveNext ())
        {
        BCOM.TextElement ele = ee.Current as BCOM.TextElement;
        if (ele != null)
            {
            string  text = ele.Text;
            ele.Text = text.ToLower ();
            //  It is redrawn automatically.
            ele.Rewrite ();
            }
        }        
        
    app.CommandState.StartDefaultCommand ();
    }

/// <summary>Runs the change case command. </summary>
public static void ChangeToUpper (System.String unparsed)
    {
    BCOM.ElementEnumerator ee = TextMod.GetElementEnumerator ();
    
    if (ee == null)
        return;
        
    BCOM.Application    app = BMI.Utilities.ComApp;        
    //  Associate this with Undo
    app.CommandState.StartPrimitive (new Dummy (), false);
    app.CommandState.CommandName = "TextMod Upper";
    
    while (ee.MoveNext ())
        {
        BCOM.TextElement ele = ee.Current as BCOM.TextElement;
        if (ele != null)
            {
            string  text = ele.Text;
            ele.Text = text.ToUpper ();
            ele.Rewrite ();
            }
        }
        
    app.CommandState.StartDefaultCommand ();
    }

/// <summary>Displays the form used to run the 
/// text transform commands</summary>
public static void TransformText (System.String unparsed)
    {
    Transform   transformForm = new Transform ();
    
    transformForm.AttachAsTopLevelForm (TextMod.Instance, true);
    transformForm.Show ();
    }


}  // End of KeyinCommands

}  // End of the namespace
