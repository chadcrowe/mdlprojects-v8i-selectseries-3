The files in this project are:

TextMod.cs - Contains the AddIn startup code and the class KeyinCommands
that handles the key-ins.

Transform.cs - The implementation of a command and an example of a form
attached via AttachAsTopLevelForm.

StandardPostBuild.bat - a batch file that can be used to execute standard 
a post-build event.  For the batch file to work completely MS must be defined
in the environment to point to your MicroStation install location (where
ustation.exe resides).  MS should NOT included a trailing '\' character.
Invoke StandardPostBuild.bat with the post-build event like this:

   call StandardPostBuild.bat $(TargetPath) VALIDATEADDIN
   
TextMod.mke - a make file for building the project.

commands.xml - the keyin commands for this AddIn.

