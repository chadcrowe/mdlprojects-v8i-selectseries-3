/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/TextMod/Transform.cs,v $
|    $RCSfile: Transform.cs,v $
|   $Revision: 1.1 $
|       $Date: 2005/04/07 16:23:56 $
|
|  $Copyright: (c) 2005 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using BMW=Bentley.MicroStation.WinForms;
using BMI=Bentley.MicroStation.InteropServices;
using BCOM=Bentley.Interop.MicroStationDGN;

namespace TextMod
{
/// <summary>WinForms used in MicroStation should
/// derive from Bentley.MicroStationDGN.WinForms.Adapter.
/// </summary>
public class Transform : BMW.Adapter
{
private System.Windows.Forms.CheckBox scaleCheck;
private System.Windows.Forms.CheckBox angleCheck;
private System.Windows.Forms.TextBox scaleValue;
private System.Windows.Forms.TextBox angleValue;
private System.Windows.Forms.Button transformButton;
private System.Windows.Forms.Button exitButton;

private System.ComponentModel.Container components = null;

public Transform()
    {
    //
    // Required for Windows Form Designer support
    //
    InitializeComponent();
    }

/// <summary>
/// Clean up any resources being used.
/// </summary>
protected override void Dispose( bool disposing )
    {
    if( disposing )
        {
        if(components != null)
            {
            components.Dispose();
            }
        }
    base.Dispose( disposing );
}

#region Windows Form Designer generated code
/// <summary>
/// Required method for Designer support - do not modify
/// the contents of this method with the code editor.
/// </summary>
private void InitializeComponent()
    {
    this.scaleCheck = new System.Windows.Forms.CheckBox();
    this.angleCheck = new System.Windows.Forms.CheckBox();
    this.scaleValue = new System.Windows.Forms.TextBox();
    this.angleValue = new System.Windows.Forms.TextBox();
    this.transformButton = new System.Windows.Forms.Button();
    this.exitButton = new System.Windows.Forms.Button();
    this.SuspendLayout();
    // 
    // scaleCheck
    // 
    this.scaleCheck.Location = new System.Drawing.Point(16, 24);
    this.scaleCheck.Name = "scaleCheck";
    this.scaleCheck.Size = new System.Drawing.Size(60, 24);
    this.scaleCheck.TabIndex = 0;
    this.scaleCheck.Text = "Scale";
    // 
    // angleCheck
    // 
    this.angleCheck.Location = new System.Drawing.Point(16, 56);
    this.angleCheck.Name = "angleCheck";
    this.angleCheck.Size = new System.Drawing.Size(60, 24);
    this.angleCheck.TabIndex = 2;
    this.angleCheck.Text = "Angle";
    // 
    // scaleValue
    // 
    this.scaleValue.Location = new System.Drawing.Point(72, 24);
    this.scaleValue.Name = "scaleValue";
    this.scaleValue.TabIndex = 1;
    this.scaleValue.Text = "1";
    // 
    // angleValue
    // 
    this.angleValue.Location = new System.Drawing.Point(72, 56);
    this.angleValue.Name = "angleValue";
    this.angleValue.TabIndex = 3;
    this.angleValue.Text = "0";
    // 
    // transformButton
    // 
    this.transformButton.Location = new System.Drawing.Point(15, 104);
    this.transformButton.Name = "transformButton";
    this.transformButton.Size = new System.Drawing.Size(75, 24);
    this.transformButton.TabIndex = 4;
    this.transformButton.Text = "&Transform";
    this.transformButton.Click += new System.EventHandler(this.transformButton_Click);
    // 
    // exitButton
    // 
    this.exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
    this.exitButton.Location = new System.Drawing.Point(96, 104);
    this.exitButton.Name = "exitButton";
    this.exitButton.Size = new System.Drawing.Size(75, 24);
    this.exitButton.TabIndex = 6;
    this.exitButton.Text = "E&xit";
    this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
    // 
    // Transform
    // 
    this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
    this.CancelButton = this.exitButton;
    this.ClientSize = new System.Drawing.Size(192, 149);
    this.Controls.Add(this.exitButton);
    this.Controls.Add(this.transformButton);
    this.Controls.Add(this.angleValue);
    this.Controls.Add(this.scaleValue);
    this.Controls.Add(this.angleCheck);
    this.Controls.Add(this.scaleCheck);
    this.Name = "Transform";
    this.Text = "Text Transform Example";
    this.ResumeLayout(false);

}

#endregion

/// <summary>The user requested the transform operation.  
/// </summary>
private void transformButton_Click(object sender, System.EventArgs e)
    {
    BCOM.Application    app = BMI.Utilities.ComApp;
    double  rotation = 0;
    double  scale = 0;
    bool    rotate = angleCheck.Checked;
    bool    doScale = scaleCheck.Checked;
    
    if (!doScale && !rotate)
        {
        //  Specify a parent window to force the window to stay on top of 
        //  MicroStation's application window.
        MessageBox.Show (new BMI.MicroStationWin32 (), "Scale and Angle are both disabled");
        return;
        }

    try
        {
        if (doScale)
            scale = System.Convert.ToDouble (scaleValue.Text);
        if (rotate)
            {
            rotation = System.Convert.ToDouble (angleValue.Text);
            rotation = app.Radians (rotation);
            }
        }
    catch (System.Exception)
        {
        MessageBox.Show (new BMI.MicroStationWin32 (), "Invalid scale or angle.");
        
        }
    
    BCOM.ElementEnumerator ee = TextMod.GetElementEnumerator ();
    if (ee == null)
        return;
        
    app.CommandState.StartPrimitive (new Dummy (), false);
    app.CommandState.CommandName = "TextMod Transform";
    
    while (ee.MoveNext ())
        {
        BCOM.TextElement    ele = ee.Current as BCOM.TextElement;
        if (ele != null)
            {
            BCOM.Point3d    origin = ele.get_Origin ();
            
            if (doScale)
                ele.ScaleUniform (ref origin, scale);

            if (rotate)
                ele.RotateAboutZ (ref origin, rotation);
            
            //  There is no need to draw it.  It will be updated automatically
            ele.Rewrite ();                
            }
        }
        
    app.CommandState.StartDefaultCommand ();
    }

/// <summary>Close the form. 
/// </summary>
private void exitButton_Click(object sender, System.EventArgs e)
    {
    this.Close ();    
    }


    }  // End of class Transform
    
/// <summary>
/// This is dummy class.  None of the commands require event handlers, but
/// we want to call this a primitive command for the sake of marking the position
/// in the Undo buffer.
/// </summary>
class Dummy : BCOM.IPrimitiveCommandEvents
{
#region IPrimitiveCommandEvents Members

public void Start()
{
    // TODO:  Add Dummy.Start implementation
}

public void Reset()
{
    // TODO:  Add Dummy.Reset implementation
}

public void DataPoint(ref Bentley.Interop.MicroStationDGN.Point3d Point, Bentley.Interop.MicroStationDGN.View View)
{
    // TODO:  Add Dummy.DataPoint implementation
}

public void Keyin(string Keyin)
{
    // TODO:  Add Dummy.Keyin implementation
}

public void Dynamics(ref Bentley.Interop.MicroStationDGN.Point3d Point, Bentley.Interop.MicroStationDGN.View View, Bentley.Interop.MicroStationDGN.MsdDrawingMode DrawMode)
{
    // TODO:  Add Dummy.Dynamics implementation
}

public void Cleanup()
{
    // TODO:  Add Dummy.Cleanup implementation
}

#endregion

}

}
