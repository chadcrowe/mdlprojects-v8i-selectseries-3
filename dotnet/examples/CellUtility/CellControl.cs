/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/CellUtility/step4/CellControl.cs,v $
|    $RCSfile: CellControl.cs,v $
|   $Revision: 1.1 $
|       $Date: 2006/06/07 13:13:57 $
|
|  $Copyright: (c) 2006 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using SRI=System.Runtime.InteropServices;

using BM=Bentley.MicroStation;
using BMW=Bentley.MicroStation.WinForms;
using BMI=Bentley.MicroStation.InteropServices;
using BCOM=Bentley.Interop.MicroStationDGN;

namespace CellUtility
{
/// <summary>Main form for CellUtility AddIn example.
/// </summary>
internal class CellControl : BMW.Adapter, BCOM.IModelActivateEvents
{
private static CellControl              s_current;
private Bentley.Windowing.WindowContent  m_windowContent;

private Bentley.MicroStation.AddIn                        m_addIn;

private System.Windows.Forms.Panel      bottomPanel;

private const int                       bottomPanelHeight       = 104;
private const int                       bottomPanelOffset       = 2;
private const int                       maxCellPreviewHeight    = 256;
private const int                       cellPreviewOffset       = 4;

//  Items contained in control panel
private System.Windows.Forms.CheckBox   showSharedCells;
private System.Windows.Forms.CheckBox   displayAllCellsInPath;
private System.Windows.Forms.CheckBox   useFilter;
private System.Windows.Forms.TextBox    filterValue;
private System.Windows.Forms.TextBox    attachedLibrary;

//  Cells list at top of form
private System.Windows.Forms.ListBox    cellsList;

//  Display of currently selected cell
private System.Windows.Forms.PictureBox cellPreview;

// Menus

//  Attached library context menu
private System.Windows.Forms.ContextMenu attachContextMenu;
private System.Windows.Forms.MenuItem   attachLibraryMenuItem;
private System.Windows.Forms.MenuItem   detachLibraryMenuItem;
private System.Windows.Forms.MenuItem   copyMenuItem;

//  Individual cell context menu
private System.Windows.Forms.ContextMenu cellContextMenu;
private System.Windows.Forms.MenuItem   deleteCellMenuItem;
private System.Windows.Forms.MenuItem   editCellMenuItem;
private System.Windows.Forms.MenuItem   placeCellMenuItem;

/// <summary>Find the available cell definitions and add
/// the names to the list box.</summary>
private void InterpretCellLibraryData ()
    {
    if (this.DesignMode)
        return;

    cellsList.Items.Clear ();
    attachedLibrary.Text = "(No library attached)";
    try
        {
        bool    includeSharedCells = showSharedCells.Checked;
        bool    includePath = displayAllCellsInPath.Checked;
        BCOM.CellLibrary cellLibrary = AddInMain.ComApp.AttachedCellLibrary;

        if (cellLibrary ==  null)
            return;
            
        System.Text.RegularExpressions.Regex    regex = null;
        
        if (useFilter.Checked)
            {
            regex = new System.Text.RegularExpressions.Regex (filterValue.Text,
                                System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            }
        
        attachedLibrary.Text = cellLibrary.FullName;
        
        BCOM.CellInformationEnumerator cellEnumerator = 
                        AddInMain.ComApp.GetCellInformationEnumerator (includeSharedCells, includePath);
        
        while (cellEnumerator.MoveNext ())
            {
            BCOM.CellInformation   cellInfo = cellEnumerator.Current;
            string  cellName = cellInfo.Name;
            
            if (regex == null || regex.IsMatch (cellName, 0))
                cellsList.Items.Add (cellName);
            }
        }
    catch (System.Exception e)
        {
        AddInMain.ComApp.MessageCenter.AddMessage (e.Message, e.ToString (), BCOM.MsdMessageCenterPriority.Error, false);
        return;
        }                    
    }

internal static void UpdateCellLibraryData ()
    {
    if (null == s_current)
        return;
        
    s_current.InterpretCellLibraryData ();
    }

/// <summary>The Visual Studio IDE requires a constructor without arguments.</summary>
private CellControl()
    {
    System.Diagnostics.Debug.Assert (this.DesignMode, "Do not use the default constructor");
    InitializeComponent();
    }
    
/// <summary>Constructor</summary>
internal CellControl(Bentley.MicroStation.AddIn addIn)
    {
    m_addIn     = addIn;
    InitializeComponent();

    //  Set up events to handle resizing of form; closing of form
    this.SizeChanged += new EventHandler(CellControl_SizeChanged);
    this.Closed += new EventHandler(CellControl_Closed);

    InterpretCellLibraryData ();
    attachedLibrary.ContextMenu     = attachContextMenu;
    cellsList.ContextMenu           = cellContextMenu;
    
    AddInMain.ComApp.AddModelActivateEventsHandler (this);
    }

/// <summary>
/// Clean up any resources being used.
/// </summary>
protected override void Dispose( bool disposing )
    {
    base.Dispose( disposing );
    }

#region Windows Form Designer generated code
/// <summary>
/// Required method for Designer support - do not modify
/// the contents of this method with the code editor.
/// </summary>
private void InitializeComponent()
    {
    this.bottomPanel = new System.Windows.Forms.Panel();
    this.attachedLibrary = new System.Windows.Forms.TextBox();
    this.filterValue = new System.Windows.Forms.TextBox();
    this.useFilter = new System.Windows.Forms.CheckBox();
    this.displayAllCellsInPath = new System.Windows.Forms.CheckBox();
    this.showSharedCells = new System.Windows.Forms.CheckBox();
    this.cellsList = new System.Windows.Forms.ListBox();
    this.attachContextMenu = new System.Windows.Forms.ContextMenu();
    this.attachLibraryMenuItem = new System.Windows.Forms.MenuItem();
    this.detachLibraryMenuItem = new System.Windows.Forms.MenuItem();
    this.copyMenuItem = new System.Windows.Forms.MenuItem();
    this.cellContextMenu = new System.Windows.Forms.ContextMenu();
    this.placeCellMenuItem = new System.Windows.Forms.MenuItem();
    this.editCellMenuItem = new System.Windows.Forms.MenuItem();
    this.deleteCellMenuItem = new System.Windows.Forms.MenuItem();
    this.cellPreview = new System.Windows.Forms.PictureBox();
    this.bottomPanel.SuspendLayout();
    this.SuspendLayout();
    // 
    // bottomPanel
    // 
    this.bottomPanel.Anchor = System.Windows.Forms.AnchorStyles.Left;
    this.bottomPanel.Controls.Add(this.attachedLibrary);
    this.bottomPanel.Controls.Add(this.filterValue);
    this.bottomPanel.Controls.Add(this.useFilter);
    this.bottomPanel.Controls.Add(this.displayAllCellsInPath);
    this.bottomPanel.Controls.Add(this.showSharedCells);
    this.bottomPanel.Location = new System.Drawing.Point(8, 408);
    this.bottomPanel.Name = "bottomPanel";
    this.bottomPanel.Size = new System.Drawing.Size(272, 106);
    this.bottomPanel.TabIndex = 0;
    // 
    // attachedLibrary
    // 
    this.attachedLibrary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
    this.attachedLibrary.Location = new System.Drawing.Point(0, 81);
    this.attachedLibrary.Name = "attachedLibrary";
    this.attachedLibrary.ReadOnly = true;
    this.attachedLibrary.Size = new System.Drawing.Size(256, 20);
    this.attachedLibrary.TabIndex = 4;
    this.attachedLibrary.Text = "";
    // 
    // filterValue
    // 
    this.filterValue.Location = new System.Drawing.Point(88, 48);
    this.filterValue.Name = "filterValue";
    this.filterValue.Size = new System.Drawing.Size(176, 20);
    this.filterValue.TabIndex = 3;
    this.filterValue.Text = ".*";
    this.filterValue.Leave += new System.EventHandler(this.formatChanged);
    // 
    // useFilter
    // 
    this.useFilter.Location = new System.Drawing.Point(0, 48);
    this.useFilter.Name = "useFilter";
    this.useFilter.Size = new System.Drawing.Size(72, 16);
    this.useFilter.TabIndex = 2;
    this.useFilter.Text = "Use Filter";
    this.useFilter.CheckedChanged += new System.EventHandler(this.formatChanged);
    // 
    // displayAllCellsInPath
    // 
    this.displayAllCellsInPath.Location = new System.Drawing.Point(0, 28);
    this.displayAllCellsInPath.Name = "displayAllCellsInPath";
    this.displayAllCellsInPath.Size = new System.Drawing.Size(154, 16);
    this.displayAllCellsInPath.TabIndex = 1;
    this.displayAllCellsInPath.Text = "Display All Cells in Path";
    this.displayAllCellsInPath.CheckedChanged += new System.EventHandler(this.formatChanged);
    // 
    // showSharedCells
    // 
    this.showSharedCells.Location = new System.Drawing.Point(0, 8);
    this.showSharedCells.Name = "showSharedCells";
    this.showSharedCells.Size = new System.Drawing.Size(134, 16);
    this.showSharedCells.TabIndex = 0;
    this.showSharedCells.Text = "Use Shared Cells";
    this.showSharedCells.CheckedChanged += new System.EventHandler(this.formatChanged);
    // 
    // cellsList
    // 
    this.cellsList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
        | System.Windows.Forms.AnchorStyles.Right)));
    this.cellsList.Location = new System.Drawing.Point(8, 8);
    this.cellsList.Name = "cellsList";
    this.cellsList.Size = new System.Drawing.Size(270, 134);
    this.cellsList.TabIndex = 1;
    this.cellsList.DoubleClick += new System.EventHandler(this.cellsList_DoubleClick);
    this.cellsList.SelectedIndexChanged += new System.EventHandler(this.cellsList_SelectedIndexChanged);
    // 
    // attachContextMenu
    // 
    this.attachContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                                                                                      this.attachLibraryMenuItem,
                                                                                      this.detachLibraryMenuItem,
                                                                                      this.copyMenuItem});
    // 
    // attachLibraryMenuItem
    // 
    this.attachLibraryMenuItem.Index = 0;
    this.attachLibraryMenuItem.Text = "Attach library...";
    this.attachLibraryMenuItem.Click += new System.EventHandler(this.attachLibraryMenuItem_Click);
    // 
    // detachLibraryMenuItem
    // 
    this.detachLibraryMenuItem.Index = 1;
    this.detachLibraryMenuItem.Text = "Detach library";
    this.detachLibraryMenuItem.Click += new System.EventHandler(this.detachLibraryMenuItem_Click);
    // 
    // copyMenuItem
    // 
    this.copyMenuItem.Index = 2;
    this.copyMenuItem.Text = "Copy";
    this.copyMenuItem.Click += new System.EventHandler(this.copyMenuItem_Click);
    // 
    // cellContextMenu
    // 
    this.cellContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                                                                                    this.placeCellMenuItem,
                                                                                    this.editCellMenuItem,
                                                                                    this.deleteCellMenuItem});
    // 
    // placeCellMenuItem
    // 
    this.placeCellMenuItem.Index = 0;
    this.placeCellMenuItem.Text = "Place Cell";
    this.placeCellMenuItem.Click += new System.EventHandler(this.placeCellMenuItem_Click);
    // 
    // editCellMenuItem
    // 
    this.editCellMenuItem.Index = 1;
    this.editCellMenuItem.Text = "Edit Cell";
    this.editCellMenuItem.Click += new System.EventHandler(this.editCellMenuItem_Click);
    // 
    // deleteCellMenuItem
    // 
    this.deleteCellMenuItem.Index = 2;
    this.deleteCellMenuItem.Text = "Delete Cell";
    this.deleteCellMenuItem.Click += new System.EventHandler(this.deleteCellMenuItem_Click);
    // 
    // cellPreview
    // 
    this.cellPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
    this.cellPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
    this.cellPreview.Location = new System.Drawing.Point(8, 160);
    this.cellPreview.Name = "cellPreview";
    this.cellPreview.Size = new System.Drawing.Size(270, 225);
    this.cellPreview.TabIndex = 2;
    this.cellPreview.TabStop = false;
    // 
    // CellControl
    // 
    this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
    this.ClientSize = new System.Drawing.Size(290, 525);
    this.Controls.Add(this.cellsList);
    this.Controls.Add(this.cellPreview);
    this.Controls.Add(this.bottomPanel);
    this.Name = "CellControl";
    this.Text = "CellControl";
    this.bottomPanel.ResumeLayout(false);
    this.ResumeLayout(false);

}
    
#endregion
/// <summary>
/// Show the form if it is not already displayed
/// </summary>
internal static void ShowForm (Bentley.MicroStation.AddIn addIn)
    {
    if (null != s_current)
        return;

    s_current = new CellControl (addIn);
    s_current.AttachAsTopLevelForm (addIn, true);
    
    s_current.AutoOpen = true;
    s_current.AutoOpenKeyin = "mdl load CellUtility";
    
    s_current.NETDockable = true;
    Bentley.Windowing.WindowManager    windowManager = 
                Bentley.Windowing.WindowManager.GetForMicroStation ();
    s_current.m_windowContent = 
        windowManager.DockPanel (s_current, s_current.Name, s_current.Name, 
        Bentley.Windowing.DockLocation.Floating);

    s_current.m_windowContent.CanDockHorizontally = false; // limit to left and right docking
    }

/// <summary>
/// Close the form if it is currently displayed
/// </summary>
internal static void CloseForm ()
    {
    if (s_current == null)
        return;
    
    s_current.m_windowContent.Close();

    s_current = null;
    }

/// <summary>
/// Set forms sizes. Keep this separate from InitializeComponent. If you move
/// this into InitializeComponent then
/// -- the IDE may remove this
/// -- the IDE may not be able to display the form making it impossible to use
///    IDE's designer
/// </summary>
private void SetFormSizes ()
    {
    if (this.DesignMode)
        {
        System.Diagnostics.Debug.Assert (!this.DesignMode, "Do not use SetFormSizes in design mode.");
        return;
        }

    int     cellPreviewHeight = this.Width - 4;

    if (cellPreviewHeight > maxCellPreviewHeight)
        cellPreviewHeight = maxCellPreviewHeight;
        
    int     minimumBottomPanel = (cellPreviewHeight + cellPreviewOffset + 50);
    int     bottomPanelTop = this.Height - bottomPanelHeight - bottomPanelOffset;
    
    if (bottomPanelTop < minimumBottomPanel)
        bottomPanelTop = minimumBottomPanel;
        
    int     cellPreviewTop = bottomPanelTop - cellPreviewHeight - cellPreviewOffset;

    this.cellPreview.Top     = cellPreviewTop;
    this.cellPreview.Height  = cellPreviewHeight;

    this.bottomPanel.Top    = bottomPanelTop;
    this.cellPreview.Top    = cellPreviewTop;
    this.cellsList.Height   = cellPreviewTop - 4;
    
    int     newWidth = this.Width - 16;
    this.cellsList.Width = newWidth;
    this.bottomPanel.Width = newWidth;
    this.attachedLibrary.Width = newWidth;
    }

/// <summary>
/// Adjust to controls when the form changes size
/// </summary>
private void CellControl_SizeChanged(object sender, EventArgs e)
    {
    SetFormSizes ();
    }

/// <summary>Handle the standard Closed event
/// </summary>
private void CellControl_Closed(object sender, EventArgs e)
    {   
    if (s_current != null)
        s_current.Dispose (true);
        
    s_current = null;
    }

/// <summary>Respond to the user selecting a choice from right-click menu
/// </summary>
private void attachLibraryMenuItem_Click(object sender, System.EventArgs e)
    {
    OpenFileDialog  dialog = new OpenFileDialog ();
    dialog.Filter   = "Cell Files (*.cel)|*.cel|Design Files (*.dgn)|*.dgn|All Files (*.*)|*.*";
    //  If this just called ShowDialog (), it would be possible for MicroStation to
    //  cover the dialog.
    if (dialog.ShowDialog (new BMI.MicroStationWin32 ()) == DialogResult.OK)
        {
        AddInMain.ComApp.AttachCellLibrary (dialog.FileName, BCOM.MsdConversionMode.Always);
        InterpretCellLibraryData ();
        }
        
    }

/// <summary>By adding our own menu, we eliminated the standard 
/// behavior. Restore the standard behavior for Copy
/// </summary>
private void copyMenuItem_Click(object sender, System.EventArgs e)
    {
    attachedLibrary.Copy ();
    }

/// <summary>
/// Respond to request to remove the library.
/// </summary>
private void detachLibraryMenuItem_Click(object sender, System.EventArgs args)
    {
    try
        {
        AddInMain.ComApp.DetachCellLibrary ();
        InterpretCellLibraryData ();
        }
    catch (System.Exception e)
        {
        AddInMain.ComApp.MessageCenter.AddMessage (e.Message, e.ToString (), BCOM.MsdMessageCenterPriority.Error, false);
        }        
    }

private void placeCellMenuItem_Click(object sender, System.EventArgs e)
    {
    string  cellName = (string)cellsList.SelectedItem;

    PlaceCell.StartPlacementCommand (m_addIn, cellName);
    }

private void editCellMenuItem_Click(object sender, System.EventArgs e)
    {
    BCOM.CellLibrary cellLibrary = AddInMain.ComApp.AttachedCellLibrary;
    if (cellLibrary ==  null)
        return;
            
    string designFileName = cellLibrary.FullName;
    string  cellName = (string)cellsList.SelectedItem;
    //  Easy way to get pointer to Unicode string
    IntPtr  modelName = SRI.Marshal.StringToBSTR (cellName);

    Imports.mdlSystem_newDesignFileAndModel (designFileName, modelName);
    SRI.Marshal.FreeBSTR (modelName);
    }

/// <summary>
/// Respond to request to remove the library.
/// </summary>
private void deleteCellMenuItem_Click(object sender, System.EventArgs e)
    {
    BCOM.CellLibrary cellLibrary = AddInMain.ComApp.AttachedCellLibrary;
    if (cellLibrary ==  null)
        return;
            
    string  cellName = (string)cellsList.SelectedItem;

    Imports.mdlCell_deleteInLibrary (cellName);
    InterpretCellLibraryData ();
    }

/// <summary>Common function for responding to any event that
/// can alter what is displayed in the cells list box.
/// </summary>
private void formatChanged(object sender, System.EventArgs e)
    {
    InterpretCellLibraryData ();
    }

/// <summary>Respond to the double click by starting a command.
/// </summary>
private void cellsList_DoubleClick(object sender, System.EventArgs e)
    {
    string  cellName = (string)cellsList.SelectedItem;
    PlaceCell.StartPlacementCommand (m_addIn, cellName);
    }

private void cellsList_SelectedIndexChanged(object sender, System.EventArgs e)
    {
    BCOM.Application    app = AddInMain.ComApp;
    string  cellName = (string)cellsList.SelectedItem;
    BCOM.CellElement    cellElement = null;
    BCOM.Point3d        zeroOrigin = app.Point3dZero ();
    
    int     size = cellPreview.Height;
    if (size > cellPreview.Width)
        size = cellPreview.Width;
        
    cellPreview.Image = null;
    // Create the element that the dynamics and data point methods will use.
    cellElement = app.CreateCellElement3 (cellName, ref zeroOrigin, true);
    
    System.IntPtr   metafileHandle = new IntPtr(cellElement.DrawToEnhancedMetafile (size, size, true));
    System.Drawing.Imaging.Metafile metafile = new System.Drawing.Imaging.Metafile 
                                                                (metafileHandle, true);
    cellPreview.Image = metafile;
    }

#region IModelActivateEvents Members

public void BeforeActivate(Bentley.Interop.MicroStationDGN.ModelReference TheModel)
    {
    }

public void AfterActivate(Bentley.Interop.MicroStationDGN.ModelReference TheModel)
    {
    InterpretCellLibraryData ();
    }

#endregion
}   // End of class CellControl
}   // End of namespace CellUtility
