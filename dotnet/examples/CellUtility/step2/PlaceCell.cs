/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/CellUtility/step2/PlaceCell.cs,v $
|    $RCSfile: PlaceCell.cs,v $
|   $Revision: 1.1 $
|       $Date: 2006/06/07 13:13:57 $
|
|  $Copyright: (c) 2006 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
//  System namespaces
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using SRI=System.Runtime.InteropServices;

//  Bentley namespaces
using BMW=Bentley.MicroStation.WinForms;
using BMI=Bentley.MicroStation.InteropServices;
using BCOM=Bentley.Interop.MicroStationDGN;

namespace CellUtility
{
/// <summary>The PlaceCell class implements a Place Cell command and
/// implements a form that is embedded in MicroStation's
/// Tool Settings dialog. The command uses this form to collect 
/// scaling and rotation arguments.
/// 
/// </summary>
internal class PlaceCell : 
                BMW.Adapter,        //  For the form embedded in Tool Settings
                BCOM.IPrimitiveCommandEvents  //  To implement primitive command
{
private BCOM.CellElement                    m_cellElement;
private Bentley.MicroStation.AddIn          m_addIn;
private static bool                         s_scaleLocked;

private System.Windows.Forms.Label          xScale;
private System.Windows.Forms.Label          yScale;
private System.Windows.Forms.Label          labelRotation;
private System.Windows.Forms.TextBox        xScaleValue;
private System.Windows.Forms.TextBox        yScaleValue;
private System.Windows.Forms.TextBox        rotationValue;
private System.Windows.Forms.CheckBox       scaleLocked;
private System.Windows.Forms.Label          labelLocked;

/// <summary>
/// Required designer variable.
/// </summary>
private System.ComponentModel.Container components = null;

/// <summary>The IDE requires the constructor with no arguments.</summary>
private PlaceCell()
    {
    //  Make sure only the IDE uses this.
    System.Diagnostics.Debug.Assert (this.DesignMode == true);
    InitializeComponent();
    }

/// <summary>Constructor</summary>
internal PlaceCell(Bentley.MicroStation.AddIn addIn, BCOM.CellElement cellElement)
    {
    //  Invoke the IDE-generated code
    InitializeComponent();

    m_addIn       = addIn;
    m_cellElement = cellElement;
    
    //  Set the controls to the values from active settings.
    BCOM.Application   app = AddInMain.ComApp;
    BCOM.Settings      settings = app.ActiveSettings;
    
    //  Initialize to active settings
    BCOM.Point3d       scale = settings.get_Scale ();
    xScaleValue.Text = scale.X.ToString ();
    yScaleValue.Text = scale.Y.ToString ();
    
    double      aa = settings.Angle;
    rotationValue.Text = app.Degrees (aa).ToString ();

    scaleLocked.CheckState = s_scaleLocked ? CheckState.Checked : CheckState.Unchecked;
    ProcessToolSettingsControls ();
    }
   
/// <summary>Get the transform to change the cell
/// </summary>
private void GetTransform (out BCOM.Transform3d transform, ref BCOM.Point3d origin)
    {
    BCOM.Application    app = AddInMain.ComApp;
    BCOM.Settings       settings = app.ActiveSettings;
    double              angle = settings.Angle;
    BCOM.Point3d        scale = settings.get_Scale ();
    
    BCOM.Matrix3d       matrix = app.Matrix3dFromScaleFactors (scale.X, scale.Y, scale.Z);
    BCOM.Matrix3d       rotMatrix = app.Matrix3dFromAxisAndRotationAngle (2, angle);
    
    matrix = app.Matrix3dFromMatrix3dTimesMatrix3d (ref rotMatrix, ref matrix);
    transform = app.Transform3dFromMatrix3dPoint3d (ref matrix, ref origin);    
    }
    
/// <summary>Clean up any resources being used. 
/// </summary>
protected override void Dispose( bool disposing )
    {
    if( disposing )
        {
        //  This is an explicit call to Dispose. It is not
        //  the result of the object being garbage collected.
        if(components != null)
            {
            components.Dispose();
            }

        if (null != m_cellElement)
            {
            //  Without this, the system puts the COM object on the finalizer
            //  thread, the finalizer thread invokes the interop finalizer,
            //  and it marshalls the COM operation back to the main thread.
            SRI.Marshal.ReleaseComObject (m_cellElement);
            m_cellElement = null;
            }
        }

    base.Dispose( disposing );
    }

#region Windows Form Designer generated code
/// <summary>
/// Required method for Designer support - do not modify
/// the contents of this method with the code editor.
/// </summary>
private void InitializeComponent()
    {
    this.xScale = new System.Windows.Forms.Label();
    this.yScale = new System.Windows.Forms.Label();
    this.labelRotation = new System.Windows.Forms.Label();
    this.xScaleValue = new System.Windows.Forms.TextBox();
    this.yScaleValue = new System.Windows.Forms.TextBox();
    this.rotationValue = new System.Windows.Forms.TextBox();
    this.scaleLocked = new System.Windows.Forms.CheckBox();
    this.labelLocked = new System.Windows.Forms.Label();
    this.SuspendLayout();
    // 
    // xScale
    // 
    this.xScale.Location = new System.Drawing.Point(16, 8);
    this.xScale.Name = "xScale";
    this.xScale.Size = new System.Drawing.Size(72, 16);
    this.xScale.TabIndex = 0;
    this.xScale.Text = "&X Scale";
    // 
    // yScale
    // 
    this.yScale.Location = new System.Drawing.Point(16, 40);
    this.yScale.Name = "yScale";
    this.yScale.Size = new System.Drawing.Size(72, 16);
    this.yScale.TabIndex = 2;
    this.yScale.Text = "&Y Scale";
    // 
    // Rotation
    // 
    this.labelRotation.Location = new System.Drawing.Point(16, 104);
    this.labelRotation.Name = "Rotation";
    this.labelRotation.Size = new System.Drawing.Size(60, 16);
    this.labelRotation.TabIndex = 6;
    this.labelRotation.Text = "&Rotation";
    // 
    // xScaleValue
    // 
    this.xScaleValue.Location = new System.Drawing.Point(96, 8);
    this.xScaleValue.Name = "xScaleValue";
    this.xScaleValue.Size = new System.Drawing.Size(60, 20);
    this.xScaleValue.TabIndex = 1;
    this.xScaleValue.Text = "1";
    this.xScaleValue.TextChanged += new System.EventHandler(this.settings_TextChanged);
    // 
    // yScaleValue
    // 
    this.yScaleValue.Location = new System.Drawing.Point(96, 40);
    this.yScaleValue.Name = "yScaleValue";
    this.yScaleValue.Size = new System.Drawing.Size(60, 20);
    this.yScaleValue.TabIndex = 3;
    this.yScaleValue.Text = "1";
    this.yScaleValue.TextChanged += new System.EventHandler(this.settings_TextChanged);
    // 
    // rotationValue
    // 
    this.rotationValue.Location = new System.Drawing.Point(96, 104);
    this.rotationValue.Name = "rotationValue";
    this.rotationValue.Size = new System.Drawing.Size(60, 20);
    this.rotationValue.TabIndex = 7;
    this.rotationValue.Text = "0";
    this.rotationValue.TextChanged += new System.EventHandler(this.settings_TextChanged);
    // 
    // scaleLocked
    // 
    this.scaleLocked.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
    this.scaleLocked.Location = new System.Drawing.Point(96, 72);
    this.scaleLocked.Name = "scaleLocked";
    this.scaleLocked.Size = new System.Drawing.Size(16, 24);
    this.scaleLocked.TabIndex = 5;
    this.scaleLocked.CheckStateChanged += new System.EventHandler(this.scaleLocked_CheckStateChanged);
    // 
    // labelLocked
    // 
    this.labelLocked.Location = new System.Drawing.Point(16, 72);
    this.labelLocked.Name = "labelLocked";
    this.labelLocked.Size = new System.Drawing.Size(56, 23);
    this.labelLocked.TabIndex = 4;
    this.labelLocked.Text = "&Locked?";
    // 
    // PlaceCell
    // 
    this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
    this.ClientSize = new System.Drawing.Size(176, 133);
    this.Controls.Add(this.labelLocked);
    this.Controls.Add(this.scaleLocked);
    this.Controls.Add(this.rotationValue);
    this.Controls.Add(this.yScaleValue);
    this.Controls.Add(this.xScaleValue);
    this.Controls.Add(this.labelRotation);
    this.Controls.Add(this.yScale);
    this.Controls.Add(this.xScale);
    this.Name = "PlaceCell";
    this.Text = "PlaceCell";
    this.ResumeLayout(false);

}
#endregion

/// <summary>If possible. convert the text to a double.
/// Otherwise, use the default value
/// </summary>
private double ConvertToDouble (string text, double defaultValue)
    {
    try
        {
        return System.Convert.ToDouble (text);
        }
    catch (System.FormatException)
        {
        }
    
    return defaultValue;        
    }
    
#region IPrimitiveCommandEvents Members

public void Start()
    {
    }

public void Reset()
    {
    }

/// <summary>Handle IPrimitiveCommandEvents.DataPoint
/// </summary>
public void DataPoint(ref BCOM.Point3d Point, BCOM.View View)
    {
    if (m_cellElement == null)
        return;
        
    BCOM.Transform3d transform;
    
    GetTransform (out transform, ref Point);            
    m_cellElement.Transform (ref transform);
    
    BCOM.Application   app = AddInMain.ComApp;
    app.ActiveModelReference.AddElement (m_cellElement);
    
    transform = app.Transform3dInverse (ref transform);
    m_cellElement.Transform (ref transform);
    }

public void Keyin(string Keyin)
    {
    }

/// <summary>Handle IPrimitiveCommandEvents.Dynamics
/// </summary>
public void Dynamics(ref BCOM.Point3d Point, BCOM.View View, BCOM.MsdDrawingMode DrawMode)
    {
    BCOM.Application   app = AddInMain.ComApp;
    if (m_cellElement == null)
        return;
    
    BCOM.Transform3d transform;
    
    GetTransform (out transform, ref Point);    
    
    m_cellElement.Transform (ref transform);
    m_cellElement.Redraw (DrawMode);
    
    transform = app.Transform3dInverse (ref transform);
    m_cellElement.Transform (ref transform);
    }

/// <summary>End of the command; Call DetachFromMicroStation
/// to release the Tool Settings dialog.
/// /// </summary>
public void Cleanup()
    {
    //  After DetachFromMicroStation returns , the form this class implements
    //  is no longer embedded in MicroStation's Tool Settings dialog.
    DetachFromMicroStation (); 

    if (m_cellElement != null)
        {
        //  Done strictly for performance
        System.Runtime.InteropServices.Marshal.ReleaseComObject (m_cellElement);
        m_cellElement = null;  //  We released it so make sure nothing else can use it.
        }
    }

#endregion

/// <summary>
/// Starts the primitive command to place the cell
/// </summary>
internal static void StartPlacementCommand (Bentley.MicroStation.AddIn addIn, string name)
    {
    BCOM.Application    app = AddInMain.ComApp;
    BCOM.CommandState   commandState = app.CommandState;
    BCOM.CellElement    cellElement = null;
    BCOM.Point3d        zeroOrigin = app.Point3dZero ();
    
    try {
        // Create the element that the dynamics and data point methods will use.
        cellElement = app.CreateCellElement3 (name, ref zeroOrigin, true);
        }
    catch (System.Exception e) 
        { 
        MessageBox.Show (new BMI.MicroStationWin32 (), e.Message);
        return;
        }
        
    PlaceCell           command = new PlaceCell (addIn, cellElement);

    app.CommandState.StartPrimitive (command, false);

    //  Record the name that is saved in the Undo buffer and
    //  shown as the prompt.
    app.CommandState.CommandName = "Place Cell Example";
    
    //   Set the Form Text -- this will appear as the Tool Settings title.
    command.Text = "Place Cell '" + name + "'";
    
    //  Embed the form in MicroStation's Tool Settings dialog
    command.AttachToToolSettings (addIn);    

    commandState.StartDynamics ();
    }

private void ProcessToolSettingsControls ()
    {
    BCOM.Application    app = AddInMain.ComApp;
    BCOM.Settings       settings = app.ActiveSettings;
    double              defaultAngle = app.Degrees (settings.Angle);
    
    settings.Angle = app.Radians (ConvertToDouble (rotationValue.Text, defaultAngle));

    BCOM.Point3d    activeScale = settings.get_Scale ();
    activeScale.X = ConvertToDouble (xScaleValue.Text, activeScale.X);

    if (s_scaleLocked)
        {
        activeScale.Y = activeScale.X;
        yScaleValue.Text = xScaleValue.Text;
        }
    else    
        activeScale.Y = ConvertToDouble (yScaleValue.Text, activeScale.Y);
        
    settings.set_Scale  (ref activeScale);
    }

/// <summary>The xScaleValue, yScaleValue, and scaleLocked
/// TextBox controls all use settings_TextChanged as 
/// their TextChanged event handler. If the value of any 
/// of these changes, the system calls this event handler. 
/// It computes the new values and saves them all in the 
/// active settings.
/// </summary>
private void settings_TextChanged(object sender, System.EventArgs e)
    {
    ProcessToolSettingsControls ();
    }

private void scaleLocked_CheckStateChanged(object sender, System.EventArgs e)
    {
    s_scaleLocked = scaleLocked.CheckState == CheckState.Checked;
    yScaleValue.Enabled = !s_scaleLocked;
    yScale.Enabled = !s_scaleLocked;
    ProcessToolSettingsControls ();
    }

}   // End of class PlaceCell
}   // End of namespace
