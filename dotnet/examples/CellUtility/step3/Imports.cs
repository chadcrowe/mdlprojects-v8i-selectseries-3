/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/CellUtility/step3/Imports.cs,v $
|    $RCSfile: Imports.cs,v $
|   $Revision: 1.1 $
|       $Date: 2006/06/07 13:13:57 $
|
|  $Copyright: (c) 2006 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
using System;

namespace CellUtility
{
using   SRI = System.Runtime.InteropServices;

/// <summary>When loading an AddIn MicroStation looks for a class
/// derived from AddIn.</summary>
internal sealed class Imports
{
[SRI.DllImport("STDMDLBLTIN.DLL", CharSet=SRI.CharSet.Ansi, 
            CallingConvention=SRI.CallingConvention.StdCall)]
internal static extern int mdlSystem_newDesignFileAndModel (
                                String designFile,              //  Really Ansi
                                IntPtr modelName                //  Unicode
                                );

[SRI.DllImport("STDMDLBLTIN.DLL", CharSet=SRI.CharSet.Unicode, 
            CallingConvention=SRI.CallingConvention.StdCall)]
internal static extern int mdlCell_deleteInLibrary (string cellName);

}   // End of Class Imports
}   // End of Namespace CellUtility

