/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/CellUtility/step3/KeyinCommands.cs,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
using System;
//  using System.Windows.Forms;

//  using BMW=Bentley.MicroStation.WinForms;
using BCOM=Bentley.Interop.MicroStationDGN;
using BMI=Bentley.MicroStation.InteropServices;

namespace CellUtility
{
/// <summary>Class used for running key-ins.  The key-ins
/// XML file commands.xml provides the class name and the method names.
/// </summary>
internal class KeyinCommands
{

public static void LibraryAttach (System.String unparsed)
    {
    BCOM.Application    app = AddInMain.ComApp;
    
    try
        {
        app.AttachCellLibrary (unparsed, BCOM.MsdConversionMode.Always);
        CellControl.UpdateCellLibraryData ();
        }
    catch (System.Exception e)
        {
        app.MessageCenter.AddMessage ("Attaching failed: " + e.Message, e.ToString (), 
                                        BCOM.MsdMessageCenterPriority.Error, false);
        }        
    }

public static void LibraryDetach (System.String unparsed)
    {
    BCOM.Application    app = AddInMain.ComApp;

    try
        {
        app.DetachCellLibrary ();
        CellControl.UpdateCellLibraryData ();
        }
    catch (System.Exception e)
        {
        app.MessageCenter.AddMessage ("Detaching failed" + e.Message, e.ToString (), 
                                        BCOM.MsdMessageCenterPriority.Error, false);
        }        
    }
    
public static void PlaceCellCommand (System.String unparsed)
    {
    BCOM.Application    app = AddInMain.ComApp;

    PlaceCell.StartPlacementCommand (AddInMain.MyAddin, unparsed);
    }
    
}  // End of KeyinCommands

}  // End of the namespace
