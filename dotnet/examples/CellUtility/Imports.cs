/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/CellUtility/step4/Imports.cs,v $
|    $RCSfile: Imports.cs,v $
|   $Revision: 1.1 $
|       $Date: 2006/06/07 13:13:58 $
|
|  $Copyright: (c) 2006 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
using System;

namespace CellUtility
{
using   SRI = System.Runtime.InteropServices;

/// <summary>When loading an AddIn MicroStation looks for a class
/// derived from AddIn.</summary>
internal sealed class Imports
{

internal const int SYSTEM_NEW_DESIGN_FILE = 1;
internal delegate void HandleDesignFileEvent (string filename, int state);

[SRI.DllImport("USTATION.DLL", EntryPoint="mdlSystem_setFunction",
            CallingConvention=SRI.CallingConvention.Cdecl)]
internal static extern System.IntPtr mdlSystem_setNewDesignFileFunction (
                int functionType, HandleDesignFileEvent eventDelegate);

[SRI.DllImport("USTATION.DLL", CharSet=SRI.CharSet.Ansi, 
            CallingConvention=SRI.CallingConvention.Cdecl)]
internal static extern System.IntPtr mdlSystem_getCurrMdlDesc ();

[SRI.DllImport("STDMDLBLTIN.DLL", CharSet=SRI.CharSet.Ansi, 
            CallingConvention=SRI.CallingConvention.StdCall)]
internal static extern int mdlSystem_newDesignFileAndModel (
                                String designFile,              //  Really Ansi
                                IntPtr modelName                //  Unicode
                                );

[SRI.DllImport("STDMDLBLTIN.DLL", CharSet=SRI.CharSet.Unicode, 
            CallingConvention=SRI.CallingConvention.StdCall)]
internal static extern int mdlCell_deleteInLibrary (string cellName);

}   // End of Class Imports
}   // End of Namespace CellUtility

