/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/CellUtility/step4/Events.cs,v $
|    $RCSfile: Events.cs,v $
|   $Revision: 1.2 $
|       $Date: 2006/08/31 19:47:47 $
|
|  $Copyright: (c) 2006 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
// #define MDL_NEWDESIGNFILE
#define MANAGED_NEWDESIGNFILE
using System;

namespace CellUtility
{
using   SRI = System.Runtime.InteropServices;
using   SWF = System.Windows.Forms;
using   BMI=Bentley.MicroStation.InteropServices;

/// <summary>
/// Shows how to add MDL and .NET event handlers. See CellControl
/// for an example of how to add an IModelActivateEvents event
/// handler. IModelActivateEvents is an event interface in the 
/// MicroStationDGN COM object model.
/// /// </summary>
internal sealed class Events
{
//  Keep a local reference to the event handler delegate object. 
//  If unmanaged code holds the only reference, it will be 
//  garbage collected.
#if MDL_NEWDESIGNFILE
private static Imports.HandleDesignFileEvent    s_designFileEventHandler;
#elif MANAGED_NEWDESIGNFILE
private static Bentley.MicroStation.AddIn.NewDesignFileEventHandler
                                                s_managedEventHandler;
#endif

private static void HandleMdlDesignFileEvent 
(
string      filename, 
int         state
)
    {
    string  display = 
        System.String.Format 
            ("HandleMdlDesignFileEvent: file = '{0}' state = {1}",
                    filename, state);

    SWF.MessageBox.Show (new BMI.MicroStationWin32 (), display);
    }

private static void MyAddin_NewDesignFileEvent
(
Bentley.MicroStation.AddIn sender, 
Bentley.MicroStation.AddIn.NewDesignFileEventArgs eventArgs
)
    {
    string  display = 
        System.String.Format 
            ("MyAddin_NewDesignFileEvent: sender = {0}, file = '{1}' state = {2}",
                    sender.ToString (), eventArgs.Name, eventArgs.WhenCode.ToString ());

    SWF.MessageBox.Show (new BMI.MicroStationWin32 (), display);
    }

///<summary>Prior to using any mdl..._setFunction 
///function we must be certain the current MDL descriptor is
///belongs to our add-in. This is essential because the 
///mdl...setFunction functions save the function pointer in the
///MDL descriptor. This is guaranteed to be true if called from 
///directly or indirectly from AddInMain.Run or from a key-in.  
///</summary>
private static void VerifyMdlDescriptor ()
    {
    System.IntPtr   myMdlDesc = AddInMain.MyAddin.GetMdlDescriptor ();
    System.IntPtr   currMdlDesc = Imports.mdlSystem_getCurrMdlDesc ();

    System.Diagnostics.Debug.Assert (myMdlDesc == currMdlDesc);
    }

internal static void SetEventHandlers ()
    {
#if MDL_NEWDESIGNFILE
    VerifyMdlDescriptor ();

    //  Create the delegate and save it in a static so it won't be garbage collected.
    s_designFileEventHandler = new Imports.HandleDesignFileEvent (HandleMdlDesignFileEvent);
    Imports.mdlSystem_setNewDesignFileFunction (Imports.SYSTEM_NEW_DESIGN_FILE, s_designFileEventHandler);
#elif MANAGED_NEWDESIGNFILE
    s_managedEventHandler = 
            new Bentley.MicroStation.AddIn.NewDesignFileEventHandler(MyAddin_NewDesignFileEvent);

    AddInMain.MyAddin.NewDesignFileEvent += s_managedEventHandler;
#endif
    }
    
internal static void ClearEventHandlers ()
    {
#if MDL_NEWDESIGNFILE
    VerifyMdlDescriptor ();

    if (null == s_designFileEventHandler)
        return; //  already cleared.

    Imports.mdlSystem_setNewDesignFileFunction (Imports.SYSTEM_NEW_DESIGN_FILE, null);
    s_designFileEventHandler = null;
#endif
    
#if MANAGED_NEWDESIGNFILE
    if (null == s_managedEventHandler)
        return; //  already cleared.

    AddInMain.MyAddin.NewDesignFileEvent -= s_managedEventHandler;
    s_managedEventHandler = null;
#endif
    }

}   // End of Class Imports
}   // End of Namespace CellUtility



