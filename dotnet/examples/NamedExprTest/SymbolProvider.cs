/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/NamedExprTest/SymbolProvider.cs,v $
|    $RCSfile: SymbolProvider.cs,v $
|   $Revision: 1.1 $
|       $Date: 2008/06/02 15:30:32 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2008 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
namespace   NamedExprTest
{
using       System;
using       System.Reflection;

using   ECE   = Bentley.ECObjects.Expressions;
using   BIM   = Bentley.Internal.MicroStation;
using   BMA   = Bentley.MicroStation.Application;
using   SCS   = System.Collections.Specialized;
using   AddIn = Bentley.MicroStation.AddIn;

/*====================================================================================**/
///
/// <summary>SymbolProvider is a class that publishes symbols that can be
/// used in named expression and evaluated by the ECEvaluator.</summary>
///
/// <author>Bentley</author>                                     <date>05/2008</date>
/*==============+===============+===============+===============+===============+======*/
public class    SymbolProvider : ECE.IECSymbolProvider
{
/*------------------------------------------------------------------------------------**/
/// <summary>Default constructor.</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
/*--------------+---------------+---------------+---------------+---------------+------*/
public  SymbolProvider
(
)
    {
    }

/*------------------------------------------------------------------------------------**/
/// <summary>Document the symbols for namespace Weather.</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
/*--------------+---------------+---------------+---------------+---------------+------*/
private void DocumentWeatherSymbols 
(
System.Xml.XmlTextWriter writer
)
    {
    // AverageTemp
    writer.WriteStartElement ("Symbol");
     writer.WriteElementString ("Set", "Weather");
     writer.WriteElementString ("Name", "AverageTemp");
     writer.WriteElementString ("Returns", "Double");
     writer.WriteElementString ("Description", "Returns Average of two temperatures");

     writer.WriteStartElement ("Parameter");
      writer.WriteElementString ("Name", "temp1");
      writer.WriteElementString ("Type", "Double");
      writer.WriteElementString ("Description", "temperature");
     writer.WriteEndElement (); // Parameter

     writer.WriteStartElement ("Parameter");
      writer.WriteElementString ("Name", "temp2");
      writer.WriteElementString ("Type", "Double");
      writer.WriteElementString ("Description", "temperature");
     writer.WriteEndElement (); // Parameter
    writer.WriteEndElement (); // Symbol

    // CelsiusTemp
    writer.WriteStartElement ("Symbol");
     writer.WriteElementString ("Set", "Weather");
     writer.WriteElementString ("Name", "CelsiusTemp");
     writer.WriteElementString ("Returns", "Double");
     writer.WriteElementString ("Description", "Returns temperature in Celsius");

     writer.WriteStartElement ("Parameter");
      writer.WriteElementString ("Name", "temperature");
      writer.WriteElementString ("Type", "Double");
      writer.WriteElementString ("Description", "fahrenheit temperature to convert");
     writer.WriteEndElement (); // Parameter
    writer.WriteEndElement (); // Symbol

    // HighTemp
    writer.WriteStartElement ("Symbol");
     writer.WriteElementString ("Set", "Weather");
     writer.WriteElementString ("Name", "HighTemp");
     writer.WriteElementString ("Returns", "Double");
     writer.WriteElementString ("Description", "Returns high temperature for month in fahrenheit");
    writer.WriteEndElement (); // Symbol

    // LowTemp
    writer.WriteStartElement ("Symbol");
     writer.WriteElementString ("Set", "Weather");
     writer.WriteElementString ("Name", "LowTemp");
     writer.WriteElementString ("Returns", "Double");
     writer.WriteElementString ("Description", "Returns low temperature for month in fahrenheit");
    writer.WriteEndElement (); // Symbol
    }

#region IECSymbolProvider Members
/*------------------------------------------------------------------------------------**/
/// <summary>Readonly name property of the property enabler. This string should not
/// be localized.</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
/*--------------+---------------+---------------+---------------+---------------+------*/
public virtual string Name
    {
    get
        {
        return @"NamedExprTest.SymbolProvider";
        }
    }

/*------------------------------------------------------------------------------------**/
/// <summary>Document the provided symbols. This documentation is used to generate 
/// HTML report of symbols and information to populate expression editor right-click 
/// menu.</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
/*--------------+---------------+---------------+---------------+---------------+------*/
public virtual void DocumentSymbols
(
System.Xml.XmlTextWriter writer
)
    {
    Assembly thisAssembly = Assembly.GetExecutingAssembly ();

    writer.WriteStartElement ("SymbolProvider");
    writer.WriteAttributeString ("name", System.IO.Path.GetFileName (thisAssembly.Location));
    DocumentWeatherSymbols (writer);
    writer.WriteEndElement (); //SymbolProvider
    }

/*---------------------------------------------------------------------------------**//**
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
public static System.Double   Celsius_Temp (System.Double fahrenheit)
    {
    return (5.0/9.0)*(fahrenheit-32.0);    
    }

/*---------------------------------------------------------------------------------**//**
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
public static System.Double   Average_Temp (System.Double low, System.Double high)
    {
    return (low+high)/2.0;    
    }

/*---------------------------------------------------------------------------------**//**
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
public static System.Double Low_Temp
    {
    get 
        {
        return 32.0;
        }
    }

/*---------------------------------------------------------------------------------**//**
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
public static System.Double High_Temp
    {
    get 
        {
        return 94.0;
        }
    }

/*----------------------------------------------------------------------------------*//**
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
private static void   PublishWeatherGlobals 
(
ref ECE.ECContext    context
)
    {
    ECE.ECContext  currentContext = new ECE.ECContext ();
    
    //  Make a namespace symbol String and add it to the current
    //  context. Make the namespace symbol point to the methods 
    //  context.
    ECE.NamespaceSymbol weatherNamespace = new ECE.NamespaceSymbol ("Weather", currentContext);
    context.AddSymbol (weatherNamespace);

    //  Now create a symbol that refers to a static method and add 
    //  it to the methodsContext
    ECE.TypeIdentifier  localType = new ECE.TypeIdentifier (typeof (SymbolProvider));

    currentContext.AddSymbol (new ECE.StaticMethodSymbol ("AverageTemp", localType, "Average_Temp"));
    currentContext.AddSymbol (new ECE.StaticMethodSymbol ("CelsiusTemp", localType, "Celsius_Temp"));
    currentContext.AddSymbol (new ECE.ValueSymbol ("HighTemp",  High_Temp));
    currentContext.AddSymbol (new ECE.ValueSymbol ("LowTemp",   Low_Temp));
    }

/*------------------------------------------------------------------------------------**/
/// <summary>Publishes symbols for the contect object</summary>
/// <remarks>If contextObjects is null then only global symbols are published.</remarks>
/// <author>Bentley</author>                                     <date>05/2008</date>
/*--------------+---------------+---------------+---------------+---------------+------*/
public virtual void PublishSymbols 
(
ref ECE.ECContext    context,
System.Object        contextObject, // typically BIME::Element
SCS.StringCollection requiredSymbolSets
)
    {
    if (null == requiredSymbolSets || requiredSymbolSets.Contains ("Weather"))
        PublishWeatherGlobals (ref context);
    }

/*------------------------------------------------------------------------------------**/
/// <summary>Free/Release any temporary objects that may have been created during
/// the call to publish symbols.</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
/*--------------+---------------+---------------+---------------+---------------+------*/
public virtual void SymbolUsageComplete
(
SCS.StringCollection contextStrings
)
    {
    // no temporary objects created so there is nothing to do
    }

#endregion

}  // SymbolProvider
}  // namespace NamedExprTest

                                                       
