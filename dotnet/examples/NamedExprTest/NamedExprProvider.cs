/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/NamedExprTest/NamedExprProvider.cs,v $
|    $RCSfile: NamedExprProvider.cs,v $
|   $Revision: 1.1 $
|       $Date: 2008/06/02 15:30:32 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2008 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
namespace   NamedExprTest
{
using System;
using SCS = System.Collections.Specialized;
using BIM = Bentley.Internal.MicroStation;

/*====================================================================================**/
///
/// <summary>Named Expression Provider class. As an alternative to storing named 
/// expressions in dgnlibs, applications can also provide named expressions by
/// implementing and registering a INamedExpressionProvider based class.</summary>
///
/// <author>Bentley</author>                                     <date>05/2008</date>
/*==============+===============+===============+===============+===============+======*/
public class NamedExpressionProvider : BIM.INamedExpressionProvider
    {
    /*------------------------------------------------------------------------------------**/
    /// <summary>Gets the name of the expressionProvider</summary>
    /// <author>Bentley</author>                                     <date>05/2008</date>
    /*--------------+---------------+---------------+---------------+---------------+------*/
    public System.String      Name
        {
        get
            {
            return "NamedExprTest.NamedExpressionProvider";
            }
        }

    /*------------------------------------------------------------------------------------**/
    /// <summary>Gets the NamedExpression object by name. Returns null is no 
    /// expression is found</summary>
    /// <author>Bentley</author>                                     <date>05/2008</date>
    /*--------------+---------------+---------------+---------------+---------------+------*/
    public BIM.NamedExpression GetExpressionByName 
    (
    System.String name
    )
        {
        SCS.StringCollection requiredSymbolSets = new SCS.StringCollection ();
        requiredSymbolSets.Add ("Weather");

        if (name.Equals ("Avg_Temp", StringComparison.CurrentCultureIgnoreCase))
            {
            return new BIM.NamedExpression ("Avg_Temp", "Average Temperature", "Weather.AverageTemp(Weather.HighTemp,Weather.LowTemp)", requiredSymbolSets);
            }
       if (name.Equals ("High_Celsius", StringComparison.CurrentCultureIgnoreCase))
            {
            return new BIM.NamedExpression ("High_Celsius", "High Temp in Celsius", "Weather.CelsiusTemp(Weather.HighTemp)", requiredSymbolSets);
            }

        return null;
        }

    /*------------------------------------------------------------------------------------**/
    /// <summary>Gets a list of NamedExpression. </summary>
    /// <author>Bentley</author>                                     <date>05/2008</date>
    /*--------------+---------------+---------------+---------------+---------------+------*/
    public SCS.StringCollection GetAvailableExpressionList 
    (
    System.String keywordFilter
    )
        {
        // in this case ignore the keyword filter and always return all provided expressions
        SCS.StringCollection availableExpressions = new SCS.StringCollection ();

        availableExpressions.Add ("Avg_Temp");
        availableExpressions.Add ("High_Celsius");

        return availableExpressions;
        }
    }
}  // namespace NamedExprTest
