/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/dotnet/examples/NamedExprTest/NamedExprApp.cs,v $
|    $RCSfile: NamedExprApp.cs,v $
|   $Revision: 1.1 $
|       $Date: 2008/06/02 15:30:32 $
|     $Author: Mark.Anderson $
|
|  $Copyright: (c) 2008 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;
using Bentley.MicroStation.WinForms;
using System.Runtime.InteropServices;

namespace NamedExprTest
{
using   ECE = Bentley.ECObjects.Expressions;
using   BIM = Bentley.Internal.MicroStation;
using   BMA = Bentley.MicroStation.Application;
using   SCS = System.Collections.Specialized;

[Bentley.MicroStation.AddInAttribute(
    MdlTaskID="NamedExprApp",
    KeyinTree="NamedExprTest.commands.xml.Deflate")]
public sealed class NamedExprApp : Bentley.MicroStation.AddIn
{
private BIM.INamedExpressionProvider m_ExpressionProvider;
private ECE.IECSymbolProvider        m_WeatherSymbolProvider;
private static NamedExprApp          s_NamedExprApp;

/*---------------------------------------------------------------------------------**//**
/// <summary>Constructor</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
private NamedExprApp
(
System.IntPtr mdlDesc
) :
base(mdlDesc)
    {
    }

/*---------------------------------------------------------------------------------**//**
/// <summary>The AddIn loader invokes this after loading the AddIn</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
protected override int Run
(
System.String[] commandLine
)
    {
    s_NamedExprApp = this;

    // register Named Expression Provider
    BIM.NamedExpressionManager.AddExpressionProvider (m_ExpressionProvider = new NamedExprTest.NamedExpressionProvider());
    
    // register Symbol Provider for Expression evaulation
    BIM.NamedExpressionManager.ExpressionEvaluator.SymbolManager.AddSymbolsProvider (m_WeatherSymbolProvider = new NamedExprTest.SymbolProvider ());

    return 0;
    }

/*------------------------------------------------------------------------------------**/
/// <summary>Gets single NamedExprApp instance</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
/*--------------+---------------+---------------+---------------+---------------+------*/
internal static NamedExprApp   Instance
    {
    get
        {
        return s_NamedExprApp;
        }
    }

/*---------------------------------------------------------------------------------**//**
/// <summary>Handles MDL LOAD requests after the application has been loaded.
/// </summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
private void HandleReloadEvent(Bentley.MicroStation.AddIn sender, ReloadEventArgs eventArgs)
    {
    }

/*---------------------------------------------------------------------------------**//**
/// <summary>Handles MDL UNLOAD requests</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
private void HandleUnloadEvent(Bentley.MicroStation.AddIn sender, UnloadingEventArgs eventArgs)
    {
    /* By default, un-forced unloads are always aborted by the AddIn base class. */
    }
   
/*--------------------------------------------------------------------------------------*/
/*------------------------Key-in Command Entry Points-----------------------------------*/
/*--------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------**//**
/// <summary>Handles the NAMEDEXPRTEST RUNTEST key-in</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
public static void RunTest (string unparsed)
    {
    BIM.NamedExpression ne; 
    if (null != (ne = BIM.NamedExpressionManager.GetExpressionByName ("Avg_Temp")))
        {
        System.Object exprResult = BIM.NamedExpressionManager.EvaluateExpression (ne.Expression, null, ne.RequiredSymbolSets);
        if (null != exprResult)
            {
            string msg = String.Format ("{0}={1}", ne.Expression, exprResult.ToString());
            BMA.MessageCenter.ShowInfoMessage (exprResult.ToString(), msg, false);
            }
        }

    if (null != (ne = BIM.NamedExpressionManager.GetExpressionByName ("High_Celsius")))
        {
        System.Object exprResult = BIM.NamedExpressionManager.EvaluateExpression (ne.Expression, null, ne.RequiredSymbolSets);
        if (null != exprResult)
            {
            string msg = String.Format ("{0}={1}", ne.Expression, exprResult.ToString());
            BMA.MessageCenter.ShowInfoMessage (exprResult.ToString(), msg, false);
            }
        }
    }

/*---------------------------------------------------------------------------------**//**
/// <summary>Handles the NAMEDEXPRTEST DUMPSYMBOLS key-in</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
public static void DumpSymbols (string unparsed)
    {
    string xmlFilename = System.IO.Path.Combine (System.Environment.GetEnvironmentVariable("TEMP"), "NameExprApp.xml");
    XmlTextWriter   xwriter = new XmlTextWriter (xmlFilename, System.Text.Encoding.Unicode);

    Instance.m_WeatherSymbolProvider.DocumentSymbols (xwriter);
    xwriter.Close ();

    // force XML document to show in Shells registered XML handler
    System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo (xmlFilename);
    p.UseShellExecute = true;
    System.Diagnostics.Process process = new System.Diagnostics.Process ();
    process.StartInfo = p;
    process.Start ();
    }

/*---------------------------------------------------------------------------------**//**
/// <summary>Handles the NAMEDEXPRTEST DUMPEXPRESSIONS key-in</summary>
/// <author>Bentley</author>                                     <date>05/2008</date>
+---------------+---------------+---------------+---------------+---------------+------*/
public static void DumpExpressions (string unparsed)
    {
    SCS.StringCollection exprList = Instance.m_ExpressionProvider.GetAvailableExpressionList (null);
    int numExpr = exprList.Count;
    string shortMsg = String.Format ("{0} Named Expression provided by NamedExprApp", numExpr);
    string longMsg = String.Empty;
    if (exprList.Count > 0)
        {
        longMsg = "============== Application Provided Named Expressions ============\n";

        foreach (string exprEntry in exprList)
            longMsg += String.Format ("{0}\n", exprEntry);

        longMsg += "======== End of Application Provided Named Expressions ==========\n";
        }
    else
        {
        longMsg = shortMsg;
        }

    BMA.MessageCenter.ShowInfoMessage (shortMsg, longMsg, false);
    }

            
}  //  End of ExpressionsApp
}  // End of NamedExprTest namespace


