/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/circuitcomp.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include <cmdlist.h>
#include <keys.h>

#include "circuitcomp.h"
#include "circuittxt.h"
#include "circuitcmd.h"

DialogBoxRsc DIALOGID_CircuitData =
    {
    DIALOGATTR_DEFAULT,
    49*XC, 20*YC,
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_CircuitCompData,
    {
    {{  1*XC, GENY(1),	   23*XC, 7.75*YC }, GroupBox, 0, ON, 0, TXT_GBOX_CIRCUITDATA, ""},
    {{ 12*XC, GENY(2), 	   10*XC, 0}, Text, TEXTID_DataProjectName, ON, 0, "", ""},
    {{ 12*XC, GENY(3.25),  10*XC, 0}, Text, TEXTID_DataProjectDate, ON, 0, "", ""},
    {{ 12*XC, GENY(4.5),   10*XC, 0}, Text, TEXTID_DataProjectNumber, ON, 0, "", ""},
    {{ 12*XC, GENY(5.75),  10*XC, 0}, Text, TEXTID_DataCircuitID, ON, 0, "", ""},
    {{ 25*XC, GENY(1),	   23*XC, 7.75*YC }, GroupBox, 0, ON, 0, TXT_GBOX_BATTERYDATA, ""},
    {{ 36*XC, GENY(2),     10*XC, 0}, Text, TEXTID_DataVoltage, ON, 0, "", ""},
    {{ 36*XC, GENY(3.25),  10*XC, 0}, Text, TEXTID_DataCells, ON, 0, "", ""},
    {{ 36*XC, GENY(4.50),  10*XC, 0}, Text, TEXTID_DataAmps, ON, 0, "", ""},
    {{ 36*XC, GENY(5.75),  10*XC, 0}, Text, TEXTID_DataStyle, ON, 0, "", ""},
    {{  1*XC, GENY(8), 	   23*XC, 7.75*YC}, GroupBox, 0, ON, 0, TXT_GBOX_LIGHTDATA, ""},
    {{ 12*XC, GENY(9), 	   10*XC, 0}, Text, TEXTID_DataWatts, ON, 0, "", ""},
    {{ 12*XC, GENY(10.25), 10*XC, 0}, Text, TEXTID_DataID, ON, 0, "", ""},
    {{ 12*XC, GENY(11.5),  10*XC, 0}, Text, TEXTID_DataLifeHrs, ON, 0, "", ""},
    {{ 12*XC, GENY(12.75), 10*XC, 0}, Text, TEXTID_DataColor, ON, 0, "", ""},
    {{ 25*XC, GENY(8),	   23*XC, 7.75*YC}, GroupBox, 0, ON, 0, TXT_GBOX_GROUPBOX, ""},
    {{ 36*XC, GENY(9), 	   10*XC, 0}, Text, TEXTID_DataGauge, ON, 0, "", ""},
    {{ 36*XC, GENY(10.25), 10*XC, 0}, Text, TEXTID_DataWireColor, ON, 0, "", ""},
    {{ 13*XC, GENY(15), BUTTON_STDWIDTH,0},PushButton, PUSHBUTTONID_OK, ON, 0, "", ""},
    {{ 28*XC, GENY(15), BUTTON_STDWIDTH,0}, PushButton, PUSHBUTTONID_Cancel, ON, 0, "",""},
    }
    };

DialogBoxRsc DIALOGID_BatteryEdit =
    {
    DIALOGATTR_MODAL|DIALOGATTR_ALWAYSSETSTATE,
    25*XC, 10*YC,
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_TITLE_EDITBATTERY,
    {
    {{12*XC, GENY(1),10*XC,0},Text, TEXTID_CircuitIDBattery, ON,0,"",""},
    {{12*XC, GENY(2),10*XC,0},Text, TEXTID_Voltage,	ON,0,"",""},
    {{12*XC, GENY(3),10*XC,0},Text, TEXTID_Cells, ON,0,"",""},
    {{12*XC, GENY(4),10*XC,0},Text, TEXTID_Amps, ON,0,"",""},
    {{12*XC, GENY(5),10*XC,0},Text, TEXTID_Style, ON,0,"",""},
    {{2*XC,  GENY(6.5), BUTTON_STDWIDTH,0 },PushButton, PUSHBUTTONID_OK, ON, 0, "", ""},
    {{15*XC, GENY(6.5), BUTTON_STDWIDTH,0}, PushButton, PUSHBUTTONID_Cancel, ON, 0, "",""},
    }
    }

DialogBoxRsc DIALOGID_LightEdit =
    {
    DIALOGATTR_MODAL|DIALOGATTR_ALWAYSSETSTATE,
    25*XC, 10*YC,
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_TITLE_EDITLIGHT,
    {
    {{12*XC, GENY(1),10*XC,0},Text, TEXTID_CircuitIDLight,   ON,0,"",""},
    {{12*XC, GENY(2),10*XC,0},Text, TEXTID_Watts, ON,0,"",""},
    {{12*XC, GENY(3),10*XC,0},Text, TEXTID_ID, ON,0,"",""},
    {{12*XC, GENY(4),10*XC,0},Text, TEXTID_LifeHrs,	ON,0,"",""},
    {{12*XC, GENY(5),10*XC,0},Text, TEXTID_Color, ON,0,"",""},
    {{2*XC,  GENY(6.5), BUTTON_STDWIDTH,0 },PushButton, PUSHBUTTONID_OK, ON, 0, "", ""},
    {{15*XC, GENY(6.5), BUTTON_STDWIDTH,0}, PushButton, PUSHBUTTONID_Cancel, ON, 0, "",""},
    }
    }

DialogBoxRsc DIALOGID_WireEdit =
    {
    DIALOGATTR_MODAL|DIALOGATTR_ALWAYSSETSTATE,
    25*XC, 7.5*YC,
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_TITLE_EDITWIRE,
    {
    {{12*XC, GENY(1),10*XC,0},Text, TEXTID_CircuitIDWire, ON,0,"",""},
    {{12*XC, GENY(2),10*XC,0},Text, TEXTID_Gauge, ON,0,"",""},
    {{12*XC, GENY(3),10*XC,0},Text, TEXTID_WireColor, ON,0,"",""},
    {{2*XC,  GENY(4.5), BUTTON_STDWIDTH,0 },PushButton, PUSHBUTTONID_OK, ON, 0, "", ""},
    {{15*XC, GENY(4.5), BUTTON_STDWIDTH,0}, PushButton, PUSHBUTTONID_Cancel, ON, 0, "",""},
    }
    }

DialogBoxRsc TOOLBOXID_CircuitCommands =
    {
    DIALOGATTR_TOOLBOXCOMMON,
    0, 0,
    NOHELP, MHELP, 
    NOHOOK, NOPARENTID,
    TXT_TITLE_CIRCUITCOMPONENTS,
    {
    {{0, 0, 0, 0}, ToolBox, TOOLBOXID_CircuitCommands, ON, 0, "", ""},
    }
    };


DItem_ToolBoxRsc	TOOLBOXID_CircuitCommands =
    {
    NOHELP, MHELP, NOHOOK, NOARG, 0, "Circuit Comp",
    {
    {{ 0, 0, 0, 0}, IconCmd, ICONCMDID_PLACEBATTERY,	ON, 0, "", ""},
    {{ 0, 0, 0, 0}, IconCmd, ICONCMDID_PLACELIGHT,  	ON, 0, "", ""},
    {{ 0, 0, 0, 0}, IconCmd, ICONCMDID_PLACEWIRE,   	ON, 0, "", ""},
    {{ 0, 0, 0, 0}, IconCmd, ICONCMDID_EDITCOMPONENT,   ON, 0, "", ""},
    {{ 0, 0, 0, 0}, IconCmd, ICONCMDID_SETTINGS,        ON, 0, "", ""},
    }
    };



DItem_IconCmdRsc    ICONCMDID_PLACEBATTERY =
    {
NOHELP, MHELP,0,CMD_CIRCUIT_PLACE_BATTERY, OTASKID, "BATTRY","",
    {
    }
    }    
    extendedAttributes
	{
	    {
	    {EXTATTR_FLYTEXT, TXT_FLYOVER_PLACEBATTERY},
	    {EXTATTR_BALLOON, TXT_BALLOON_PLACEBATTERY},
	}
    };

CmdItemListRsc CMD_CIRCUIT_PLACE_BATTERY = 
    {
    	{
	{{2*XC, GENY(1),10*XC,0},Text, TEXTID_CircuitIDBattery, ON,0,"",""},
	{{2*XC, GENY(2),10*XC,0},Text, TEXTID_Voltage,	    ON,0,"",""},
	{{2*XC, GENY(3),10*XC,0},Text, TEXTID_Cells,	    ON,0,"",""},
	{{2*XC, GENY(4),10*XC,0},Text, TEXTID_Amps, 	    ON,0,"",""},
	{{2*XC, GENY(5),10*XC,0},Text, TEXTID_Style,	    ON,0,"",""},
        }
    };

DItem_IconCmdRsc    ICONCMDID_PLACELIGHT =
    {
    NOHELP, MHELP,0,CMD_CIRCUIT_PLACE_LIGHT, OTASKID, "LITE","",
    {
    }
    }    
    extendedAttributes
        {
	{
	{EXTATTR_FLYTEXT, TXT_FLYOVER_PLACELIGHT},
	{EXTATTR_BALLOON, TXT_BALLOON_PLACELIGHT},
    	}
    };

CmdItemListRsc CMD_CIRCUIT_PLACE_LIGHT = 
    {
    	{
	{{2*XC, GENY(1),10*XC,0},Text, TEXTID_CircuitIDLight,   ON,0,"",""},
	{{2*XC, GENY(2),10*XC,0},Text, TEXTID_Watts,	    	ON,0,"",""},
	{{2*XC, GENY(3),10*XC,0},Text, TEXTID_ID,	    	ON,0,"",""},
	{{2*XC, GENY(4),10*XC,0},Text, TEXTID_LifeHrs,	   	ON,0,"",""},
	{{2*XC, GENY(5),10*XC,0},Text, TEXTID_Color,	    	ON,0,"",""},
        }
    };

DItem_IconCmdRsc    ICONCMDID_PLACEWIRE =
    {
    NOHELP, MHELP,0,CMD_CIRCUIT_PLACE_WIRE, OTASKID, "","",
    {
    }
    }    
    extendedAttributes
    {
    	{
	{EXTATTR_FLYTEXT, TXT_FLYOVER_PLACEWIRE},
	{EXTATTR_BALLOON, TXT_BALLOON_PLACEWIRE},
    	}
    };

CmdItemListRsc CMD_CIRCUIT_PLACE_WIRE = 
    {
    	{
	{{2*XC, GENY(1),10*XC,0},Text, TEXTID_CircuitIDWire,    ON,0,"",""},
	{{2*XC, GENY(2),10*XC,0},Text, TEXTID_Gauge,	    	ON,0,"",""},
	{{2*XC, GENY(3),10*XC,0},Text, TEXTID_WireColor,	ON,0,"",""},
        }
    };

DItem_IconCmdRsc    ICONCMDID_EDITCOMPONENT =
    {
    NOHELP, MHELP,0,CMD_CIRCUIT_EDIT_COMPONENT, OTASKID, "","",
    {
    }
    }    
    extendedAttributes
    {
    	{
	{EXTATTR_FLYTEXT, TXT_FLYOVER_EDITCOMP},
	{EXTATTR_BALLOON, TXT_BALLOON_EDITCOMP},
    	}
    };

CmdItemListRsc CMD_CIRCUIT_EDIT_COMPONENT = 
    {
    	{
        }
    };

DItem_IconCmdRsc    ICONCMDID_SETTINGS =
    {
    NOHELP, MHELP,0,CMD_CIRCUIT_DIALOG_SETTINGS, OTASKID, "","",
    {
    }
    }    
    extendedAttributes
    {
    	{
	{EXTATTR_FLYTEXT, TXT_FLYOVER_SETTINGS},
	{EXTATTR_BALLOON, TXT_BALLOON_SETTINGS},
    	}
    };

CmdItemListRsc CMD_CIRCUIT_DIALOG_SETTINGS = 
    {
    	{
        }
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Text Item resources                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Circuit Data Items  						|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_DataCircuitID = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_CIRCUITID, "dlogData.circuitID"
};

DItem_TextRsc TEXTID_DataProjectName = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "", "",
"","",NOMASK, TEXT_NOCONCAT, TXT_PROJECT, "dlogData.projectName"
};

DItem_TextRsc TEXTID_DataProjectDate = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "", "",
"","",NOMASK, TEXT_NOCONCAT, TXT_DATE, "dlogData.projectDate"
};

DItem_TextRsc TEXTID_DataProjectNumber = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_NUMBER, "dlogData.projectNumber"
};

DItem_TextRsc TEXTID_DataVoltage = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%lf", "%lf",
"0.0","",NOMASK, TEXT_NOCONCAT, TXT_VOLTAGE, "dlogData.batteryDefaults.voltage"
};

DItem_TextRsc TEXTID_DataCells = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_CELLS,"dlogData.batteryDefaults.cells"
};

DItem_TextRsc TEXTID_DataAmps = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%lf", "%lf",
"0.0","",NOMASK, TEXT_NOCONCAT, TXT_AMPS, "dlogData.batteryDefaults.amps"
};

DItem_TextRsc TEXTID_DataStyle = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_STYLE,"dlogData.batteryDefaults.style"
};

DItem_TextRsc TEXTID_DataGauge = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_GAUGE,"dlogData.wireDefaults.gauge"
};


DItem_TextRsc TEXTID_DataWireColor = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_COLORCODE,"dlogData.wireDefaults.colorCode"
};

DItem_TextRsc TEXTID_DataWatts = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_WATTS,"dlogData.lightDefaults.watts"
};

DItem_TextRsc TEXTID_DataID = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_MFG_ID,"dlogData.lightDefaults.ID"
};

DItem_TextRsc TEXTID_DataLifeHrs = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_LIFEHRS,"dlogData.lightDefaults.lifeHrs"
};

DItem_TextRsc TEXTID_DataColor = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_COLOR,"dlogData.lightDefaults.color"
};

/*----------------------------------------------------------------------+
|                                                                       |
|   Wire Command Items                                 			|
|                                                                       |
+----------------------------------------------------------------------*/

DItem_TextRsc TEXTID_CircuitIDWire = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_CIRCUITID, "dlogWire.circuitID"
};

DItem_TextRsc TEXTID_Gauge = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_GAUGE, "dlogWire.gauge"
};


DItem_TextRsc TEXTID_WireColor = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_COLORCODE, "dlogWire.colorCode"
};


/*----------------------------------------------------------------------+
|                                                                       |
|   Battery Command Items                              			|
|                                                                       |
+----------------------------------------------------------------------*/

DItem_TextRsc TEXTID_CircuitIDBattery = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_CIRCUITID, "dlogBattery.circuitID"
};

DItem_TextRsc TEXTID_Voltage = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%lf", "%lf",
"0.0","",NOMASK, TEXT_NOCONCAT, TXT_VOLTAGE,"dlogBattery.voltage"
};

DItem_TextRsc TEXTID_Cells = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_CELLS, "dlogBattery.cells"
};

DItem_TextRsc TEXTID_Amps = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%lf", "%lf",
"0.0","",NOMASK, TEXT_NOCONCAT, TXT_AMPS, "dlogBattery.amps"
};

DItem_TextRsc TEXTID_Style = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_STYLE, "dlogBattery.style"
};

/*----------------------------------------------------------------------+
|                                                                       |
|   Light Command Items                                 		|
|                                                                       |
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_CircuitIDLight = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
	"","",NOMASK, TEXT_NOCONCAT, TXT_CIRCUITID,"dlogLight.circuitID"
};

DItem_TextRsc TEXTID_Watts = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_WATTS, "dlogLight.watts"
};

DItem_TextRsc TEXTID_ID = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_MFG_ID, "dlogLight.ID"
};

DItem_TextRsc TEXTID_LifeHrs = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_LIFEHRS, "dlogLight.lifeHrs"
};

DItem_TextRsc TEXTID_Color = 
{
NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 32, "%ld", "%ld",
"","",NOMASK, TEXT_NOCONCAT, TXT_COLOR, "dlogLight.color"
};


/*----------------------------------------------------------------------+
|                                                                       |
|   Small Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdSmallRsc ICONCMDID_PLACEBATTERY =
{
23,    23,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "Battery",
   	{
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,
    11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,
    11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,
    11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,
    11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    }
};

/*----------------------------------------------------------------------+
|                                                                       |
|   Large Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdLargeRsc ICONCMDID_PLACEBATTERY =
{
31,    31,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "Battery",
    {
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    }
};


/*----------------------------------------------------------------------+
|                                                                       |
|   Small Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdSmallRsc ICONCMDID_PLACELIGHT =
    {
    23,    23,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "LIGHT",
    	{
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11, 0, 0,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11, 0, 0, 6, 6, 0, 0,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11, 0, 6, 6, 6, 6, 6,17, 0,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11, 0, 6, 6, 6, 6, 6, 6,17,17, 0,11,11,11,11,11,
        11,11,11,11,11,11,11,11, 0, 6, 6, 6, 6, 6, 6, 6,17, 0,11,11,11,11,11,
        11,11,11,11,11,11,11, 0, 6, 6, 0, 6, 6, 6, 0, 6,17,17, 0,11,11,11,11,
        11,11,11,11,11,11,11, 0, 6, 6, 0, 0, 6, 0, 0, 6,17,17, 0,11,11,11,11,
        11,11,11,11,11,11,11, 0, 6, 6, 0, 6, 0, 6, 0, 6,17,17, 0,11,11,11,11,
        11,11,11,11,11,11,11,11, 0, 6, 0, 6, 6, 6, 0, 6,17, 0,11,11,11,11,11,
        11,11,11,11,11,11,11,11, 0, 6, 6, 0, 6, 0, 6,17,17, 0,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11, 0, 6, 6, 0, 6, 6,17, 0,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11, 0, 0, 6, 6, 0, 0,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11, 0, 6, 6, 0,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11, 0, 6, 6, 0,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,
        11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,
        11,11,11, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,
        11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,
        11,11,11, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,
        11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    	}
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Large Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdLargeRsc ICONCMDID_PLACELIGHT =
{
31,    31,    FORMAT_MONOBITMAP,    BLACK_INDEX, "LIGHT",
    {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x0f, 0x80, 0x00, 0x00, 0x60, 0xc0, 0x00,
    0x01, 0x00, 0x40, 0x00, 0x02, 0x00, 0x80, 0x00,
    0x08, 0x00, 0x80, 0x00, 0x10, 0x01, 0x00, 0x00,
    0x53, 0x52, 0x00, 0x00, 0xbf, 0xe2, 0x00, 0x01,
    0x3b, 0x44, 0x00, 0x02, 0x40, 0x90, 0x00, 0x02,
    0x81, 0x20, 0x00, 0x06, 0x85, 0x80, 0x00, 0x05,
    0xfb, 0x00, 0x00, 0x06, 0x18, 0x00, 0x00, 0x0c,
    0x30, 0x00, 0x00, 0x08, 0x40, 0x00, 0x00, 0x10,
    0x80, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x42,
    0x00, 0x00, 0x00, 0xf4, 0x00, 0x00, 0x01, 0x38,
    0x00, 0x00, 0x03, 0x90, 0x00, 0x00, 0x07, 0xa0,
    0x00, 0x00, 0x08, 0xc0, 0x00, 0x00, 0x1e, 0x80,
    0x00, 0x00, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 
    }
};

/*----------------------------------------------------------------------+
|                                                                       |
|   Small Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdSmallRsc ICONCMDID_PLACEWIRE =
    {
    23,    23,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "Wire",
    	{
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    	}
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Large Icon Item Resource                                  		|
|                                                                       |
+----------------------------------------------------------------------*/
IconCmdLargeRsc ICONCMDID_PLACEWIRE =
{
31,    31,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "Wire",
    	{
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11, 0, 0, 01,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
    	}
};

IconCmdSmallRsc ICONCMDID_EDITCOMPONENT =
    {
    23, 23, ICONFORMAT_FIXEDCOLORS, BLACK_INDEX, "EdtCompS",
	{
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,15,15,15,15,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,15,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,15,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,15,15,15,11,11,11,15,15,15,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,15,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,15,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,15,15,15,15,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0,11,11,11,11,11,11,
        11,11,11,11,11,10,10,10,10,10,11,11,11, 0, 0, 0, 0, 0,11,11,11,11,11,
        11,11,11,11,11,10,11,11,11,10,11,11,11,11,11, 0,11,11,11,11,11,11,11,
        11,11,11,11,11,10,11,11,11,10,11,11,11,11,11, 0,11,11,11,11,11,11,11,
        11,11,11,10,10,10,11,11,11,10,10,10,11,11,11, 0,11,11,11,11,11,11,11,
        11,11,11,11,11,10,11,11,11,10,11,11,11, 0, 0, 0,11,11,11,11,11,11,11,
        11,11,11,11,11,10,11,11,11,10,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	}
    };

IconCmdLargeRsc ICONCMDID_EditText =
    {
    31, 31, ICONFORMAT_FIXEDCOLORS, BLACK_INDEX, "EdtCompL",
	{
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,15,15,15,15,15,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,11,15,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,11,15,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,15,15,11,11,11,11,15,15,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,15,15,11,11,11,11,15,15,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,11,15,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,11,11,11,11,15,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,15,15,15,15,15,15,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,10,10,10,10,10,10,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,10,11,11,11,11,10,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,10,11,11,11,11,10,11,11,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,10,10,10,11,11,11,11,10,10,10,11,11,11,11, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,10,10,10,11,11,11,11,10,10,10,11, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,10,11,11,11,11,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,10,11,11,11,11,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,10,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	}
    };

IconCmdSmallRsc ICONCMDID_SETTINGS =
    {
    23,    23,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "SettingS",
        {
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11, 0,17,17,17, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11, 0, 0,17, 6, 6, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6,17,17,17,17,17,17,17,17, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 0, 0, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 0, 6, 6, 0, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 0, 0, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 0, 6, 6, 0, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 0, 0, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,10,11,11,
        11,11,11,11,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        }
    };

IconCmdLargeRsc ICONCMDID_SETTINGS =
    {
    31,    31,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "SettingL",
        {
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11, 0, 0, 0, 0, 0, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11, 0,17,17,17,17,17,17, 0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11, 0, 0,17, 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6,17,17,17,17,17,17,17,17,17,17,17,17,17, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0,17, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,10,11,11,
        11,11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,10,11,11,
        11,11,11,11,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        }
    };
