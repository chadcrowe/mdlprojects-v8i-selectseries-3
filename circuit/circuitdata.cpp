/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/circuitdata.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   circuitcomp.mc - CircuitComp source code                            |
|              Illustrates basic MDL concepts and functionality for     |
|              use with the introductory JMDL class.                     |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <mselems.h>
#include    <scanner.h>
#include    <cexpr.h>
#include    <rscdefs.h>
#include    <msbnrypo.fdf>
#include	<time.h>
#include    <stdarg.h>
#include    <string.h>
#include    <stdlib.h>

#include    <msrsrc.fdf>
#include    <msdialog.fdf>
#include    <msoutput.fdf>
#include    <mselemen.fdf>
#include    <msscan.fdf>
#include    <msfile.fdf>
#include    <mssystem.fdf>
#include    <mscnv.fdf>
#include    <mdllib.fdf>

#include    "circuitcomp.h"
#include    "circuitcmd.h"

/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
/* The following macros evaluate to function calls for the BIG ENDIAN  */
/* machines (680x0, Sun, etc) and to nothing for other systems.  They  */
/* are used to swap the byte order of two-byte integers to accommodate */
/* the byte ordering of these machines.	    	    	    	       */

#if defined (BIG_ENDIAN)
#   define SWAP_BYTE(arg1)  	      mdlCnv_swapByteArray(arg1, 1)
#   define SWAP_BYTE_ARRAY(arg1,arg2) mdlCnv_swapByteArray(arg1,arg2)
#else
#   define SWAP_BYTE(arg1)
#   define SWAP_BYTE_ARRAY(arg1,arg2)
#endif

/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
extern CircuitInfo      dlogData;
/* change CircuitInfoElement to MSElement*/

/*======================================================================+
|									|
|   Minor Code Section - These routines are specific to an application, |
|   	    	    	 but can be used with very little modification  |
|   	    	    	 for many cases                        	    	|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          circuitdata_createApplicationHeader - create application|
|			header common to all application elements	|
|                                                                       |
| author        BentleySystems                                    6/99  |
|                                                                       |
+----------------------------------------------------------------------*/
Public void     circuitdata_createApplicationHeader
(
MSElement  *elP,   /* <= element to set up header info */
int		     fileFormatDataSize,			/* => words in application data */
int		     signature  	/* => assigned signature value */
)
    {
//    short   *signatureP;

    /* ------------------------------------------------------------------
       Set up the required portions of the header :
       Set type to the MicroStation application element.
       Set level to the assigned level.
       Set words to the words to follow in our element.
       Set range to the entire design cube.
       ------------------------------------------------------------------ */
    elP->ehdr.type    = MICROSTATION_ELM;
    elP->ehdr.level   = APPLICATION_LEVEL;
    elP->ehdr.elementSize = (offsetof (ApplicationElm, appData) + fileFormatDataSize + 1)/2;

/*
    elP->ehdr.words   = HDR_WORDS_AFTER_WTF + words;
    elP->ehdr.xlow    = elP->ehdr.ylow  = elP->ehdr.zlow  =
					mdlCnv_toScanFormat (MINI4);
    elP->ehdr.xhigh   = elP->ehdr.yhigh = elP->ehdr.zhigh =
					mdlCnv_toScanFormat (MAXI4);
  */
    /* ------------------------------------------------------------------
       We set the index to attributes in the display header portion
       of the element. In this case we have no attribute data on the
       element, so there is a simple relationship between words to follow
       and the attribute index.
       ------------------------------------------------------------------ */
    elP->ehdr.attrOffset  = elP->ehdr.elementSize;

/*    elP->dhdr.attindx = elP->ehdr.words - WORDS_BTWN_WTF_ATTRINDX;*/

    /* the rest of the header information is already correctly set to zero */

    /* Set the signature word in the element. We need to swap the signature */
    /* word because the scanner operates on the first 20 words of the       */
    /* element. When the scanner returns this to us, it will be correctly   */
    /* aligned and swapped. */

    elP->applicationElm.signatureWord = SIGNATUREID_CIRCUITPROJECT;
    SWAP_BYTE (&elP->applicationElm.signatureWord);

/*    signatureP = ((short *)elP) + 18;
    *signatureP = SIGNATUREID_CIRCUITPROJECT;
    SWAP_BYTE (signatureP);*/

    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          circuitdata_findApplicationElement - finds application	|
|		element in design file				|
|                                                                       |
| author        BentleySystems                                     6/99 |
|                                                                       |
+----------------------------------------------------------------------*/
Public int  	circuitdata_findApplicationElement
(
MSElement  *elP,    	    /* <= file format element */
ULong		    *filePosP,	    /* <= file position */
int		    signature,	    /* => our assigned signature */
ULong		    ddbId	    /* => Data def. resource used for conversion. */
)
    {
    int     	scanSize;
    Scanlist	scanList;
//    short       *signatureP;
    ULong       nextFilePos, scanBuf[10];
    MSElement	el;

    memset (elP, 0, sizeof (MSElement));

    /* zero scanlist and scanbuf */
    memset (&scanBuf[0], 0, sizeof(scanBuf));
    memset (&scanList, 0, sizeof (Scanlist));

    /* initialize the scanList */
    mdlScan_initScanlist (&scanList);
    mdlScan_noRangeCheck (&scanList);

    /* set up scan for our application element, to return its file position */
    scanList.scantype   = ELEMTYPE | LEVELS | ONEELEM;
    scanList.extendedType = FILEPOS;
    scanList.typmask[4] = TMSK4_MICROSTATION_ELM; /* scan for type 66 only */
    scanList.levmask[1] = LMSK1_MSAPPINFO_LEVEL;  /* scan for level 20 only */

    /* use mdlScan_initialize, mdlScan_file to find element */
    mdlScan_initialize (0, &scanList);

    mdlScan_file (scanBuf, &scanSize, sizeof (scanBuf), &nextFilePos);

    if (scanSize > 0)
    	{
    	mdlElement_read(&el, mdlModelRef_getActive(), scanBuf[0]);

        /*-----------------------------------------------------------
         Hard code offset in element to signature to avoid
         platform-specific padding that comes from using member name.
        -----------------------------------------------------------*/
	/*signatureP = ((short *)&el) + 18;
	SWAP_BYTE (signatureP);*/
	
//	*signatureP = el.applicationElm.signatureWord;
//	SWAP_BYTE (&signature);

	if (el.applicationElm.signatureWord == signature)
	    {
	    /* Copy element header and signature word. */
	    memcpy (elP, &el, sizeof(MSElement));

	    *filePosP = scanBuf[0];
	    return SUCCESS;
	    }
	}
	
    /* if we did not find element, return error */
    memset (elP, 0, sizeof (MSElement));
    return ERROR;
    }

/*======================================================================+
|																		|
|   Private Utility Routines - These routines are very specific to  	|
|   	    	    	       our particular example          	    	|
|																		|
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          circuitdata_retrieveCircuitInfo 						|
|                                                                       |
| author        BentleySystems                                     6/99 |
|                                                                       |
+----------------------------------------------------------------------*/
Public int  	circuitdata_retrieveCircuitInfo
(
CircuitInfo	*appData,       /* <= pointer to data for storage */
ULong		ddbId
)
    {
    ULong	filePos = 0L;
    int		status; /*size = -1*/
    MSElement 	el;

    status = circuitdata_findApplicationElement (&el, &filePos, SIGNATUREID_CIRCUITPROJECT, ddbId);

    if (status == SUCCESS)
    	{
//	short   *fileDataP = ((short *) &el) + 19;
	/* Convert the rest of the element to internal format. 
    	if (SUCCESS != mdlCnv_bufferFromFileFormat (NULL, &size,
    						    (byte *) appData, NULL,
						    (byte *) fileDataP, ddbId))*/
		/* Convert the rest of the element to internal format. */
	if (SUCCESS != mdlCnv_bufferFromFileFormat (NULL, NULL,
				    (byte *) appData, NULL,
				    (byte *) el.applicationElm.appData, ddbId, NULL))

	    {
	    return ERROR;
	    }
	}

    return status;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          circuitdata_storeApplicationData - store application 	|
|				data in file				|
| author        BentleySystems                                     6/99 |
|                                                                       |
+----------------------------------------------------------------------*/
Public int  	circuitdata_storeApplicationData
(
CircuitInfo  	*appDataP, 	/* => Data to be converted and saved. */
ULong		 ddbId		/* => RscId for data conv. rules. */
)
    {
    MSElement	oldEl, newEl;
    ULong		filePos;
//    int			status;
    int 		bExists;
    int			size;
    int			fileFormatDataSize;
//    short		*fileDataP;
//    CircuitInfo		oldci, returnci;

    // find existing data, if any
    bExists = (SUCCESS == circuitdata_findApplicationElement (&oldEl, &filePos, SIGNATUREID_CIRCUITPROJECT, ddbId));

    // create our application element
    memset (&newEl, 0, sizeof (MSElement));
    
    mdlCnv_calcFileSizeFromDataDef (&fileFormatDataSize, NULL, (byte *) appDataP, ddbId, NULL);

    circuitdata_createApplicationHeader (&newEl, fileFormatDataSize, SIGNATUREID_CIRCUITPROJECT);

    /* Convert the data portion of the element to file format. */
    /* Convert the data portion of the element. */
//    fileDataP = ((short *) &newEl) + 19;
    if (SUCCESS != mdlCnv_bufferToFileFormat (&size, (byte *) newEl.applicationElm.appData, NULL, (byte *)appDataP, ddbId, NULL))
    	return ERROR;

    /* add/rewrite the element */
    if (bExists)
    	filePos = mdlElement_rewrite ((MSElement *)&newEl, (MSElement *)&oldEl, filePos);
    else
    	filePos = mdlElement_add((MSElement *)&newEl);

    /* check return from mdlElement_add or mdlElement_rewrite. If     
       file position is 0, a problem occured with the write, and      
       mdlErrno has the error number in it. Otherwise return SUCCESS. */
    if (filePos == 0L)
    	return ERROR;
    else
    	return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Name	applyDrawingData					|
|                                                                       |
|   Author	BentleySystems				   3/93		|
|                                                                       |
+----------------------------------------------------------------------*/
 Private void	applyDrawingData
 (
 char       	*unparsedP
 )
    {
    /* Write application data to design file. */
    if (SUCCESS != circuitdata_storeApplicationData (&dlogData, DATADEFID_CIRCUITDATA))
    	{
        mdlOutput_rscPrintf (MSG_ERROR, 0, MESSAGELISTID_Msgs, MSGID_DataNotSaved);
    	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Name	openDrawingDataDialog					|
|                                                                       |
|   Author	BentleySystems				    3/93	|
|                                                                       |
+----------------------------------------------------------------------*/
Private void    openDrawingDataDialog
(
void
)
    {
    int last;
    if (FALSE == mdlDialog_openModal (&last, NULL, DIALOGID_CircuitData))
    	{
    	if (last == ACTIONBUTTON_OK)
    	    {
	    applyDrawingData(NULL);
	    }
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Name	circuitdata_setDefaults						|
|                                                                       |
|   Author	BentleySystems				   3/93         |
|                                                                       |
+----------------------------------------------------------------------*/
 void    circuitdata_setDefaults 
(
)
    {
    time_t t = time(NULL);
    struct tm *pTM = localtime(&t);
	
    memset(&dlogData, 0, sizeof(CircuitInfo));
    dlogData.circuitID=1;
    mdlResource_loadFromStringList(dlogData.projectName, NULL, MESSAGELISTID_Msgs, MSGID_defaultProjectName);
    dlogData.projectNumber = 1;
    strftime(dlogData.projectDate, 15, "%m-%d-%Y", pTM);

    dlogData.batteryDefaults.typeID    = DATADEFID_CIRCUITLINKAGE_BATTERY;
    dlogData.batteryDefaults.circuitID = dlogData.circuitID;
    dlogData.batteryDefaults.voltage   = 1.5;
    dlogData.batteryDefaults.cells     = 1;
    dlogData.batteryDefaults.amps      = 0.5;
    dlogData.batteryDefaults.style     = 0;
	
    dlogData.wireDefaults.typeID       = DATADEFID_CIRCUITLINKAGE_WIRE;
    dlogData.wireDefaults.circuitID    = dlogData.circuitID;
    dlogData.wireDefaults.gauge        = 3;
    dlogData.wireDefaults.colorCode    = 5;
    
    dlogData.lightDefaults.typeID      = DATADEFID_CIRCUITLINKAGE_LIGHT;
    dlogData.lightDefaults.circuitID   = dlogData.circuitID;
    dlogData.lightDefaults.lifeHrs     = 400;
    dlogData.lightDefaults.ID 	       = 10032;
    dlogData.lightDefaults.color       = 4;
    dlogData.lightDefaults.watts       = 60;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Name	findDrawingData						|
|                                                                       |
|   Author	BentleySystems				   3/93         |
|                                                                       |
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT void    findDrawingData 
(
char	        *unparsedP
)
//cmdNumber   CMD_DIALOG_SETTINGS
    {
//    int     status;
    if (SUCCESS != circuitdata_retrieveCircuitInfo (&dlogData, DATADEFID_CIRCUITDATA))
        {
	circuitdata_setDefaults();
        }

    openDrawingDataDialog();
    }
        
