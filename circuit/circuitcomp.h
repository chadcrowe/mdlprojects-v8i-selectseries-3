/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/circuitcomp.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

#include <mselems.h>

/* These following masks must be 'OR'ed with typemask[4] */
#if !defined (TMSK4_MICROSTATION_ELM)
#    define TMSK4_MICROSTATION_ELM  ELMBITMSK (MICROSTATION_ELM)    
#endif

#define APPL_DATA_WORDS				70 /* sizeof structure + signatureWord + pad */
#define HDR_WORDS_AFTER_WTF			17 /*not valid for V8 */
#define WORDS_BTWN_WTF_ATTRINDX			14 /*not valid for V8 */
#define LMSK1_MSAPPINFO_LEVEL		ELMBITMSK(MSAPPINFO_LEVEL)

#define MAXI4						0x7fffffffL
#define MINI4						0x80000000L
#define APPLICATION_LEVEL			20

#define SIGNATUREID_CIRCUITPROJECT		900
#define DEPENDENCYAPPID_CircuitConnection       900

/*  appValues */
#define TYPE_CONNECTION				0170
#define TYPE_MAX				1 //this is set for how many dependencies are managed in this app

#define DATADEFID_CIRCUITLINKAGE_BATTERY	1
#define DATADEFID_CIRCUITLINKAGE_WIRE		2
#define DATADEFID_CIRCUITLINKAGE_LIGHT		3
#define DATADEFID_CIRCUITDATA			4

#define TOOLBOXID_CircuitCommands		1

#define DIALOGID_CircuitData			2
#define DIALOGID_BatteryEdit			3
#define DIALOGID_LightEdit			4
#define DIALOGID_WireEdit			5



#define ICONCMDFRAMEID_CircuitComp		1

#define ICONCMDID_PLACEBATTERY			1
#define ICONCMDID_PLACEWIRE			2
#define ICONCMDID_PLACELIGHT			3
#define ICONCMDID_EDITCOMPONENT 		4
#define ICONCMDID_SETTINGS 			5

#define TEXTID_Voltage				1
#define TEXTID_Cells				2
#define TEXTID_Amps				3
#define TEXTID_Style				4

#define TEXTID_Gauge				5
#define TEXTID_Length				6
#define TEXTID_WireColor			7
#define TEXTID_CircuitID			8

#define TEXTID_Watts				9
#define TEXTID_ID				10
#define TEXTID_LifeHrs				11
#define TEXTID_Color				12

#define TEXTID_CircuitIDBattery			13
#define TEXTID_CircuitIDWire			14
#define TEXTID_CircuitIDLight			15

#define TEXTID_DataCircuitID			16
#define TEXTID_DataProjectName			17
#define TEXTID_DataProjectDate			18
#define TEXTID_DataProjectNumber		19
#define TEXTID_DataVoltage			20
#define TEXTID_DataCells			21
#define TEXTID_DataAmps				22
#define TEXTID_DataStyle			23
#define TEXTID_DataGauge			24
#define TEXTID_DataLength			25
#define TEXTID_DataWireColor			26
#define TEXTID_DataWatts			27
#define TEXTID_DataID				28
#define TEXTID_DataLifeHrs			29
#define TEXTID_DataColor			30

#define PUSHBUTTONID_ApplyData			1

#define MESSAGELISTID_Commands			1
#define MESSAGELISTID_Prompts			2
#define MESSAGELISTID_Msgs			3

#define COMMANDID_PlaceBattery			1
#define COMMANDID_PlaceLight			2
#define COMMANDID_PlaceWire			3
#define COMMANDID_QueryCircuitElement		4
#define COMMANDID_DrawingData			5

#define PROMPTID_EnterFirst			1
#define PROMPTID_EnterNext 			2
#define PROMPTID_EnterPoint 			3
#define PROMPTID_IdentifyElt			4

#define MSGID_LoadCmdTbl			1
#define MSGID_BatteryCellName			2
#define MSGID_LightCellName			3
#define MSGID_FoundBattery			4
#define MSGID_FoundLight			5
#define MSGID_FoundWire				6
#define MSGID_NoDrawingData			7
#define MSGID_FoundDrawingData			8
#define MSGID_defaultProjectName		9
#define MSGID_defaultDrawingName		10
#define MSGID_defaultDrawingDate		11
#define MSGID_NotCircuitElement			12
#define MSGID_CellNotFound			13
#define MSGID_DataNotSaved			14


typedef struct batteryinfo
    {
    int			typeID;
    int			circuitID;	
    double		voltage;
    double		amps;
    int			cells;
    int			style;
    }BatteryInfo;

typedef struct wireinfo
    {
    int			typeID;
    int			circuitID;
    int			gauge;
    int			colorCode;	
    }WireInfo;

typedef struct lightinfo
    {
    int			typeID;
    int			circuitID;	
    int			watts;
    int 		ID;
    int 		lifeHrs;
    int 		color;
    }LightInfo;

typedef struct  batteryLinkage
    {
    LinkageHeader	linkHdr;
    BatteryInfo		info;
    } BatteryLinkage;

typedef struct  wireLinkage
    {
    LinkageHeader	linkHdr;
    WireInfo		info;
    } WireLinkage;

typedef struct  lightLinkage
    {
    LinkageHeader	linkHdr;
    LightInfo		info;
    } LightLinkage;

typedef struct circuitinfo
    {
    char		projectName[16];
    char		projectDate[16];
    int			projectNumber;
    int			circuitID;
    WireInfo		wireDefaults;
    BatteryInfo 	batteryDefaults;
    LightInfo		lightDefaults;
    } CircuitInfo;

#if !defined (resource)
/*typedef struct applelement
    {
    Elm_hdr		ehdr;
    Disp_hdr		dhdr;
    short		signatureWord;
    CircuitInfo		appData;
    } CircuitInfoElement;*/


#endif
