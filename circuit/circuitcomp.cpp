/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/circuitcomp.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   circuitcomp.mc - CircuitComp source code                            |
|              Illustrates basic MDL concepts and functionality for     |
|              use with the introductory JMDL class.                    |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <stdarg.h>
#include <mdl.h>        /* MDL Library funcs structures & constants */
#include <tcb.h>        /* MicroStation terminal control block */
#include <global.h>     /* structure definitions for global data areas */
#include <dlogitem.h>   /* Dialog Box Manager structures & constants */
#include <cmdlist.h>    /* MicroStation command numbers */
#include <cexpr.h>      /* C Expression structures & constants */
#include <userfnc.h>    /* definitions for accessing MDL user functions */
#include <mselems.h>    /* structures that define MicroStation elements */
#include <rscdefs.h>    /* resource mgr structure definitions & constants */
#include <toolset.h>    /* MicroStation tool set definitions */
#include <dlogids.h>    
#include <string.h>
#include <msvar.fdf>
#include <dlogman.fdf>
#include <mssystem.fdf>
#include <msoutput.fdf>
#include <msrsrc.fdf>
#include <mscexpr.fdf>
#include <msstate.fdf>
#include <mselemen.fdf>
#include <msmisc.fdf>
#include <msparse.fdf>
#include <mslocate.fdf>
#include <mswindow.fdf>
#include <mdllib.fdf>
#include <msrmatrx.fdf>
#include <mstmatrx.fdf>
#include <mslinkge.fdf>
#include <mselmdsc.fdf>
#include <mscell.fdf>
#include <mscnv.fdf>
#include <msdgnobj.fdf>
#include <msmodel.fdf>

#include "circuitcomp.h"
#include "circuitcmd.h"
#include "circuitcomp.fdf"

// published parameters
BatteryInfo     dlogBattery;
WireInfo	dlogWire;
LightInfo       dlogLight;
CircuitInfo     dlogData;

DPoint3d	wirePts[MAX_VERTICES];
int		nVertices;
MSElementDescr  *s_dynamicEdP = NULL;


/*======================================================================+
|                                                                       |
|   UserData extraction functions					|
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          extractLightData                                        |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int     extractLightData
(
MSElement       *elP,
LightInfo       *lightInfoP
)
    {
    void		*nextLink = NULL;
    LightLinkage	uData;

    nextLink = mdlLinkage_extractFromElement (&uData, elP, SIGNATUREID_CIRCUITPROJECT,
					      DATADEFID_CIRCUITLINKAGE_LIGHT, NULL, NULL, NULL);

    memcpy (lightInfoP, &uData.info,sizeof (LightInfo));
    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          extractWireData						|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int      extractWireData
(
MSElement       *elP,
WireInfo	*wireInfoP
)
    {
    void	*nextLink = NULL;
    WireLinkage uData;

    nextLink = mdlLinkage_extractFromElement (&uData, elP, SIGNATUREID_CIRCUITPROJECT,
					      DATADEFID_CIRCUITLINKAGE_WIRE, NULL, NULL, NULL);

    memcpy (wireInfoP, &uData.info,sizeof (WireInfo));
    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          extractBatteryData                                      |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int      extractBatteryData
(
MSElement       *elP,
BatteryInfo     *batteryInfoP
)
    {
    void		*nextLink = NULL;
    BatteryLinkage	uData;

    nextLink = mdlLinkage_extractFromElement (&uData, elP, SIGNATUREID_CIRCUITPROJECT,
					      DATADEFID_CIRCUITLINKAGE_BATTERY, NULL, NULL, NULL);

    memcpy (batteryInfoP, &uData.info,sizeof (BatteryInfo));
    return SUCCESS;
    }

/*======================================================================+
|                                                                       |
|   Place Battery state and dynamic Functions				|
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name		updateLinkage						|
|                                                                       |
| author	BSI							|
|									|
+----------------------------------------------------------------------*/
Private int 	updateLinkage
(
MSElement       *elP,
int		dataDefID,
void		*data,
BoolInt		deleteFirst
)
    {
    int 		status;
    LinkageHeader	tLinkHdr;
/* the Linkage Haeder structure is now 
    info	    	
    remote	
    modified
    user    	
    wdExponent
    wdMantissa		       */
    
    memset(&tLinkHdr, 0 , sizeof(LinkageHeader));
    tLinkHdr.info	=0;
    tLinkHdr.remote	=0;
    tLinkHdr.modified	=0;
    tLinkHdr.user	=1;

    tLinkHdr.primaryID = SIGNATUREID_CIRCUITPROJECT;
		
    if (deleteFirst)
    	mdlLinkage_deleteFromElement (elP, SIGNATUREID_CIRCUITPROJECT, dataDefID ,NULL, NULL, NULL);

    status = mdlLinkage_appendToElement(elP, &tLinkHdr, data, dataDefID, NULL);

    return (status == SUCCESS) ?MODIFY_STATUS_REPLACE:MODIFY_STATUS_ABORT;	
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          circuitcomp_addLinkage                                  |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Public int      circuitcomp_addLinkage
(
MSElement       *elP,   /* element to add ink onto*/
void		*dataP    	/* link data to add */
)
    {
    int		     status = ERROR;
    int		    *typeID = (int*)dataP;

    switch (*typeID)
        {
        case DATADEFID_CIRCUITLINKAGE_BATTERY:
	    status = updateLinkage(elP, DATADEFID_CIRCUITLINKAGE_BATTERY, &dlogBattery, FALSE);
	    break;

	case DATADEFID_CIRCUITLINKAGE_LIGHT:
	    status = updateLinkage(elP, DATADEFID_CIRCUITLINKAGE_LIGHT, &dlogLight, FALSE);
	    break;

	    case DATADEFID_CIRCUITLINKAGE_WIRE:
	    status = updateLinkage(elP, DATADEFID_CIRCUITLINKAGE_WIRE, &dlogWire, FALSE);
    	    break;
	}

    return status;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name		circuitcomp_updateLinkage				|
|                                                                       |
| author	BSI							|
|									|
+----------------------------------------------------------------------*/
Public int	circuitcomp_updateLinkage
(
MSElement	*elP,
void		*dataP
)
    {
    int			status = ERROR;
    int			*typeID = (int*)dataP;

    switch (*typeID)
    	{
	case DATADEFID_CIRCUITLINKAGE_BATTERY:
	    status = updateLinkage(elP, DATADEFID_CIRCUITLINKAGE_BATTERY, &dlogBattery, TRUE);
	    break;

	case DATADEFID_CIRCUITLINKAGE_LIGHT:
	    status = updateLinkage(elP, DATADEFID_CIRCUITLINKAGE_LIGHT, &dlogLight, TRUE);
	    break;

	case DATADEFID_CIRCUITLINKAGE_WIRE:
	    status = updateLinkage(elP, DATADEFID_CIRCUITLINKAGE_WIRE, &dlogWire, TRUE);
	    break;
    	}

    return status;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          getTransform						|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private Transform *getTransform
(
Transform   *tMatrixP,
DPoint3d    *pt,
int	    view
)
    {
    RotMatrix   rMatrix;

    memset(tMatrixP, 0, sizeof(Transform));

    mdlRMatrix_fromView(&rMatrix, view, TRUE);
    mdlRMatrix_invert(&rMatrix, &rMatrix);

    mdlTMatrix_fromRMatrix(tMatrixP, &rMatrix);
    mdlTMatrix_scale (tMatrixP, tMatrixP, tcb->xactscle, tcb->yactscle, tcb->zactscle);
    mdlTMatrix_setTranslation(tMatrixP, pt);

    return tMatrixP;
    }

/*======================================================================+
|                                                                       |
|   Common cell placement functions					|
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          cleanUp                                                 |
|                                                                       |
| author        BSI                                                     |
|                                                                       |
+----------------------------------------------------------------------*/
Private void  cleanUp
(
)
    {
    if  (NULL!=s_dynamicEdP)
        {
        mdlElmdscr_freeAll (&s_dynamicEdP);
        }

    mdlState_startDefaultCommand();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          circuitCellDynamics										|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
#if defined (OLD_CODE)
Private void    circuitCellDynamics 
(
DPoint3d	*pt,
int		view
)
    {
    Transform tMatrix;
   
    getTransform(&tMatrix, pt, view);
    mdlElement_transform(dgnBuf, dgnBuf, &tMatrix);
    }
#else
Private void circuitCellDynamics
(
DPoint3d        *pt,
int             view,
int             drawMode
)
{
     Transform          tMatrix;
     MSElementDescrP    tmpEdP;

     if (TEMPDRAW != drawMode)
         return;
 
     mdlElmdscr_duplicate (&tmpEdP, s_dynamicEdP);
     getTransform(&tMatrix, pt, view);
     mdlElmdscr_transform (tmpEdP, &tMatrix);
     mdlElmdscr_display (tmpEdP, ACTIVEMODEL, drawMode);
     mdlElmdscr_freeAll (&tmpEdP);

}
#endif
/*----------------------------------------------------------------------+
|                                                                       |
| name          constructCellDescriptor									|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void 	constructCellDescriptor
(
DPoint3d 	*pt,
int		view,
char 		*cellName,
int		typeID
)
    {
    Transform	    tMatrix;
    MSElementDescr  *dupdscrP = NULL, *cellDescrP = NULL;
    MSWChar	    uniCellName[MAX_CELLNAME_LENGTH];

    getTransform(&tMatrix, pt, view);
//cellName
    mdlCnv_convertMultibyteToUnicode (cellName, -1,uniCellName,MAX_CELLNAME_LENGTH);
 
    if (SUCCESS != mdlCell_getElmDscr (&cellDescrP, NULL, NULL, NULL, TRUE,
				    NULL, NULL, 0, 0,FALSE,uniCellName,NULL))
				    /*cellName,NULL,FORMAT_MULTIBYTE))*/
	{
	mdlOutput_rscPrintf (MSG_MESSAGE, 0, MESSAGELISTID_Msgs, MSGID_CellNotFound);
	return;
	}
	
    // duplicate and transform the cell descriptor
    mdlElmdscr_duplicate(&dupdscrP, cellDescrP);
    mdlElmdscr_transform(dupdscrP, &tMatrix);

    // display battery cell
    mdlElmdscr_display(dupdscrP, MASTERFILE, NORMALDRAW);

    // add our linkage
    mdlModify_elementDescr2 (&dupdscrP, MASTERFILE, MODIFY_REQUEST_HEADERS, circuitcomp_addLinkage, (void *)&typeID, 0L);

    // add to design file
    mdlElmdscr_add(dupdscrP);

    // clean up our descriptors
    if (cellDescrP)
    	mdlElmdscr_freeAll (&cellDescrP);

    if (dupdscrP)
    	mdlElmdscr_freeAll (&dupdscrP);
    }

/*======================================================================+
|                                                                       |
|   Place Battery state functions					|
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          dataPointBattery					|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    dataPointBattery 
(
DPoint3d	*pt,
int		view
)
    {
    int			typeID;
    char		buffer[10];

    mdlResource_loadFromStringList(buffer, NULL, MESSAGELISTID_Msgs, MSGID_BatteryCellName);

    // set up typeID
    dlogBattery.typeID = DATADEFID_CIRCUITLINKAGE_BATTERY;
    typeID = dlogBattery.typeID;

    // construct battery cell
    constructCellDescriptor(pt, view, buffer, typeID);

    // clean up
    mdlState_setFunction (STATE_RESET, cleanUp);
    mdlState_setFunction (STATE_COMMAND_CLEANUP, NULL);

    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          placeBattery											|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Public  void placeBattery
(
char		    *unparsed
)
//cmdNumber	    CMD_PLACE_BATTERY
    {
    int 	     	status;
    char		cellName[7];
    MSWChar		uniCellName[MAX_CELLNAME_LENGTH];
		    
    // restore defaults
    DialogBox *dbP = mdlDialog_find(DIALOGID_ToolSettings, NULL);
    if (dbP)
    	{
    	memcpy(&dlogBattery, &dlogData.batteryDefaults, sizeof(BatteryInfo));
    	mdlDialog_itemsSynch(dbP);
    	}
				
    mdlResource_loadFromStringList(cellName, NULL, MESSAGELISTID_Msgs, MSGID_BatteryCellName);
    mdlState_startPrimitive (dataPointBattery, placeBattery, COMMANDID_PlaceBattery, PROMPTID_EnterPoint);

    // set up dynamic cell

    mdlCnv_convertMultibyteToUnicode (cellName, -1,uniCellName,MAX_CELLNAME_LENGTH);
     			   
    status = mdlCell_getElmDscr (&s_dynamicEdP, NULL, NULL, NULL, TRUE,
			         NULL, NULL, 0, 0, FALSE,uniCellName,NULL);
    if (status == SUCCESS)
#if defined (OLD_CODE)

        {
        mdlDynamic_setElmDescr (s_dynamicEdP);
    	mdlState_dynamicUpdate (circuitCellDynamics, TRUE);
    	}
#else
    {
    mdlState_setFunction (STATE_COMPLEX_DYNAMICS, circuitCellDynamics);
    mdlState_setFunction (STATE_COMMAND_CLEANUP, cleanUp);
    }
#endif

    }

/*======================================================================+
|                                                                       |
|   Place Battery state functions										|
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          dataPointLight											|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    dataPointLight 
(
DPoint3d	 *pt,
int			 view
)
    {
    int			typeID;
    char		buffer[10];

    mdlResource_loadFromStringList(buffer, NULL, MESSAGELISTID_Msgs, MSGID_LightCellName);

    // set up typeID
    dlogLight.typeID = DATADEFID_CIRCUITLINKAGE_LIGHT;
    typeID = dlogLight.typeID;

    // construct battery cell
    constructCellDescriptor(pt, view, buffer, typeID);

    // clean up
    mdlState_setFunction (STATE_RESET, cleanUp);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          placeLight												|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Public  void placeLight
(
char		    *unparsed
)
//cmdNumber	   CMD_PLACE_LIGHT
    {
    char		cellName[7];
    int 	     	status;
    MSElementDescr	*cellDP;
    MSWChar		uniCellName[MAX_CELLNAME_LENGTH];
		    
    // restore defaults
    DialogBox *dbP = mdlDialog_find(DIALOGID_ToolSettings, NULL);
    if (dbP)
    	{
    	memcpy(&dlogLight, &dlogData.lightDefaults, sizeof(LightInfo));
    	mdlDialog_itemsSynch(dbP);
    	}

    mdlResource_loadFromStringList(cellName, NULL, MESSAGELISTID_Msgs, MSGID_LightCellName);
    mdlState_startPrimitive (dataPointLight, placeLight, COMMANDID_PlaceLight, PROMPTID_EnterPoint);
    
    mdlCnv_convertMultibyteToUnicode (cellName, -1,uniCellName,MAX_CELLNAME_LENGTH);

    // set up dynamic cell
    status = mdlCell_getElmDscr (&cellDP, NULL, NULL, NULL, TRUE,
			         NULL, NULL, 0, 0, FALSE, uniCellName,NULL);
    

    if (status == SUCCESS)
        {
        mdlDynamic_setElmDescr (cellDP);
    	mdlState_dynamicUpdate (circuitCellDynamics, TRUE);
    	}

    return;
    }

/*======================================================================+
|                                                                       |
|   Place Wire state and dynamic Functions								|
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          createWire                                              |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private       int     createWire
(
MSElement       *elP,
DPoint3d	*points,
int		nPoints
)
    {
    int status;

    status = mdlLineString_create (elP, NULL, points, nPoints);

    return status;
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          wireDynamics						|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
void		wireDynamics
(	    
DPoint3d	*pntP,
int              view
)
    {
    if (nVertices < MAX_VERTICES)
    	wirePts[nVertices] = *pntP;

    createWire(dgnBuf, wirePts, (nVertices+1 >= MAX_VERTICES) ? MAX_VERTICES : nVertices+1);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          placeWire_done                                          |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    placeWire_done
(
void
)
    {
    MSElementUnion  el;
    
    /* Create line in local storage based on the last datapoint */
    if (SUCCESS == createWire (&el, wirePts, nVertices))
    	{
        /* Display the created line segment and add it to the design file */
        mdlElement_display (&el, NORMALDRAW);
        circuitcomp_addLinkage (&el,&dlogWire.typeID);
        dlogWire.typeID = DATADEFID_CIRCUITLINKAGE_WIRE;
        mdlElement_add (&el);
    	}

    /*---------------------------------------------------------------
    | Restart the place wire command.  If single-shot is enabled then
    | the default command will be activated instead of place wire.
    +--------------------------------------------------------------*/
    mdlState_restartCurrentCommand ();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          placeWire_firstPoint                                    | 
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    placeWire_firstPoint
(
Dpoint3d        *pntP,
int             view
)
    {
    /* Add this point to our vertex list */
    if (nVertices < MAX_VERTICES)
    	{
    	wirePts[nVertices] = *pntP;
    	nVertices++;
    	}
	
    if (nVertices == 1)
    	{
        /*------------------------------------------------------------------
        | Start dynamics to display elements as soon as you have enough
        | information to tell something about them, for a line this means
        | that we need at least one point. 
        +------------------------------------------------------------------*/
    	mdlState_dynamicUpdate (wireDynamics, FALSE);

         /* Output prompt message requesting the next point */
        mdlOutput_rscPrintf (MSG_PROMPT, 0, MESSAGELISTID_Prompts, PROMPTID_EnterNext);
    	}
    else if	(nVertices == 2)
    	{
    	/*--------------------------------------------------------------------
	| mdlState_setFunction:
	| 
	| This function is called because at this point we need 
	| to set up a reset function.  Now that the user has placed
	| at least one line segment, we no longer want the reset
	| function to be placeWire_start().  The call below to the 
	| function sets up the reset event for the function placeWire_done.
	| When the user hits a reset, placeLine_done is called by MicroStation
	| and placeWire_done finishes placement and performs cleanup 
	| for this command.      
	+---------------------------------------------------------------------*/
    	mdlState_setFunction (STATE_RESET, placeWire_done);
	}
		
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          placeWire_start                                         |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
 void    placeWire_start
(
char            *unparsedP
)
//cmdNumber       CMD_PLACE_WIRE
    {								
    // restore defaults
    DialogBox *dbP = mdlDialog_find(DIALOGID_ToolSettings, NULL);
    if (dbP)
    	{
    	memcpy(&dlogWire, &dlogData.wireDefaults, sizeof(WireInfo));
    	mdlDialog_itemsSynch(dbP);
    	}
    	
    // make sure we start from zero
    nVertices = 0;

    // begin the placement command
    mdlState_startPrimitive (placeWire_firstPoint, placeWire_start, 
			     COMMANDID_PlaceWire, PROMPTID_EnterFirst);
    }

/*======================================================================+
|                                                                       |
|   Query state functions						|
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          identifyElement                                         |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Public int      identifyElement 
(
MSElement       *elP
)
    {
//    int		     typeID = 0;
    void	    *nextLink = NULL;
    BatteryLinkage   uData;
   
    nextLink = mdlLinkage_extractFromElement (&uData, elP,SIGNATUREID_CIRCUITPROJECT,
					      DATADEFID_CIRCUITLINKAGE_BATTERY, NULL, NULL, NULL);
	
    if (nextLink == NULL)
    	nextLink = mdlLinkage_extractFromElement (&uData, elP,SIGNATUREID_CIRCUITPROJECT,
					      DATADEFID_CIRCUITLINKAGE_LIGHT, NULL, NULL, NULL);
		
    if (nextLink == NULL)
    	nextLink = mdlLinkage_extractFromElement (&uData, elP,SIGNATUREID_CIRCUITPROJECT,
					      DATADEFID_CIRCUITLINKAGE_WIRE, NULL, NULL, NULL);

    if (nextLink == NULL)
    	return -1;

    return uData.info.typeID;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          acceptElement                                           |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    acceptElement
(
DPoint3d	*pt,
int		 view
)
    {
    DgnModelRefP currFile;
    int		typeID;
    ULong       filePos;
    WireInfo	wireInfo;
    MSElement	el;
    LightInfo	lightInfo;
    BatteryInfo batteryInfo;
    int		lastAction;
//    int		status;

    /*------------------------------------------------------------------
    | The located element was accepted using a data point so get the
    | accepted element.
    | 
    +------------------------------------------------------------------*/
    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFile);

    mdlElement_read (&el, currFile, filePos);

    typeID = identifyElement (&el);

    switch (typeID)
        {
    	case DATADEFID_CIRCUITLINKAGE_BATTERY:	/*battery*/
	    mdlOutput_rscPrintf (MSG_MESSAGE, 0, MESSAGELISTID_Msgs, MSGID_FoundBattery);

	    // extract data 
	    if (extractBatteryData (&el,&batteryInfo) != SUCCESS)
	    	break;

	    // fill our published data structure
	    memcpy (&dlogBattery,&batteryInfo,sizeof(BatteryInfo));
			
	    // open modification dialog
	    if (!mdlDialog_openModal (&lastAction,NULL, DIALOGID_BatteryEdit))
	        {
	        if  (lastAction == ACTIONBUTTON_OK)
	    	    {
		    // change the element to contain the modified data 
		    mdlModify_elementSingle (currFile, filePos, MODIFY_REQUEST_HEADERS, MODIFY_ORIG, 
		    			     circuitcomp_updateLinkage, (void*)&typeID, 0L);
		    }
	    	}

	    break;

    	case DATADEFID_CIRCUITLINKAGE_WIRE:	/*wire */
	    mdlOutput_rscPrintf (MSG_MESSAGE, 0, MESSAGELISTID_Msgs, MSGID_FoundWire);

	    // extract data 
	    if (extractWireData (&el,&wireInfo)!=SUCCESS)
	    	break;

	    // fill our published data structure
	    memcpy (&dlogWire,&wireInfo,sizeof(WireInfo));
			
	    // open modification dialog
	    if (!mdlDialog_openModal (&lastAction,NULL, DIALOGID_WireEdit))
	        {
	        if (lastAction == ACTIONBUTTON_OK)
	       	    {
		    // change the element to contain the modified data 
		    mdlModify_elementSingle (currFile, filePos, MODIFY_REQUEST_HEADERS, MODIFY_ORIG, 
		    			     circuitcomp_updateLinkage, (void*)&typeID, 0L);
		    }
	    	}
	    break;

    	case DATADEFID_CIRCUITLINKAGE_LIGHT:	/*light */
	    mdlOutput_rscPrintf (MSG_MESSAGE, 0, MESSAGELISTID_Msgs, MSGID_FoundLight);
	    	
	    // extract data 
	    if (extractLightData (&el, &lightInfo) != SUCCESS)
	    	break;

	    // fill our published data structure
    	    memcpy (&dlogLight,&lightInfo,sizeof(LightInfo));

	    // open modification dialog
	    if (!mdlDialog_openModal (&lastAction, NULL, DIALOGID_LightEdit))
	    	{
	    	if (lastAction == ACTIONBUTTON_OK)
	    	    {
	    	    // change the element to contain the modified data 
		    mdlModify_elementSingle (currFile, filePos, MODIFY_REQUEST_HEADERS, MODIFY_ORIG,
		    			     circuitcomp_updateLinkage, (void*)&typeID, 0L);
	    	    }
	    	}

	    break;
    		
    	default : /* unidentified circuit element */
	    mdlOutput_rscPrintf (MSG_MESSAGE, 0, MESSAGELISTID_Msgs, MSGID_NotCircuitElement);
	    break;
        }

    /* Restart the locate logic */
    mdlLocate_restart (FALSE);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          queryElement                                            |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
Public  void queryElement 
(
char	       *unparsed
) 
//cmdNumber       CMD_EDIT_COMPONENT
    {
    mdlState_startModifyCommand (queryElement,acceptElement,NULL, NULL, NULL,
    				 COMMANDID_QueryCircuitElement, PROMPTID_IdentifyElt, FALSE, 0);
    mdlLocate_init ();

    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          openToolbox                                         	|
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
 void    openToolbox
(
char		*unparsedP
)
//cmdNumber       CMD_DIALOG_CIRCUITTOOLS
    {
    mdlDialog_open (NULL, TOOLBOXID_CircuitCommands);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          main			                                |
|                                                                       |
| author        BSI                                     10/93           |
|                                                                       |
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int		 argc,
char		*argv[]
)
    {
    RscFileHandle        rscFH;
    SymbolSet		*setP;

    mdlResource_openFile (&rscFH, NULL, 0);

    MdlCommandNumber  commandNumbers [] =
        {
        {openToolbox,CMD_CIRCUIT_DIALOG_CIRCUITTOOLS},
        {queryElement,CMD_CIRCUIT_EDIT_COMPONENT},
        {placeWire_start,CMD_CIRCUIT_PLACE_WIRE},
        {placeLight,CMD_CIRCUIT_PLACE_LIGHT},
        {placeBattery,CMD_CIRCUIT_PLACE_BATTERY},
        {findDrawingData,CMD_CIRCUIT_DIALOG_SETTINGS},
        0
        };
    
    mdlSystem_registerCommandNumbers(commandNumbers);

    if (mdlParse_loadCommandTable (NULL)==NULL)
	{
	/* Display error message */
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Msgs, MSGID_LoadCmdTbl);

    	/* exit this program and unload it */	
	mdlSystem_exit(	ERROR, /* => status for parent task */
			1);    /* => 1 to unload the program */	
	}

    /*-----------------------------------------------------------------+
    | mdlState_registerStringIds () takes two arguments. The first is the 
    | id of the stringlist holding the Command messages, the second is the id
    | of the stringlist holding the prompt messages.
    +-----------------------------------------------------------------*/
    mdlState_registerStringIds (MESSAGELISTID_Commands, MESSAGELISTID_Prompts);

	/* publish our variables */
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);
    mdlDialog_publishComplexVariable (setP, "batteryinfo",  "dlogBattery", &dlogBattery);
    mdlDialog_publishComplexVariable (setP, "lightinfo",    "dlogLight",   &dlogLight);
    mdlDialog_publishComplexVariable (setP, "wireinfo",     "dlogWire",    &dlogWire);
    mdlDialog_publishComplexVariable (setP, "circuitinfo",  "dlogData",    &dlogData);

    if (SUCCESS != circuitdata_retrieveCircuitInfo (&dlogData, DATADEFID_CIRCUITDATA))
        {
	circuitdata_setDefaults();
        }
    return  SUCCESS;
    }