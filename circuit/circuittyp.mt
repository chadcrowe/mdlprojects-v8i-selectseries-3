/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/circuittyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   circuittyp.mt - published structures				|
|									|
+----------------------------------------------------------------------*/
#pragma packedLittleEndian

#include <datadef.h>

#include "circuitcomp.h"

createDataDef (batteryinfo, DATADEFID_CIRCUITLINKAGE_BATTERY);
createDataDef (wireinfo, DATADEFID_CIRCUITLINKAGE_WIRE);
createDataDef (lightinfo, DATADEFID_CIRCUITLINKAGE_LIGHT);
createDataDef (circuitinfo, DATADEFID_CIRCUITDATA);

/* publishStructures ( geninfo ); */
publishStructures ( batteryinfo );
publishStructures ( wireinfo );
publishStructures ( lightinfo );
publishStructures ( circuitinfo );