/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/english/circuittxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#define TXT_Battery_Cell_Name   "BATTRY"
#define TXT_Light_CellName      "LITE"
#define TXT_CircuitCompData     "Drawing Data"
#define TXT_GBOX_LIGHTDATA      "Light Info"
#define TXT_GBOX_CIRCUITDATA    "General Info"
#define TXT_GBOX_BATTERYDATA    "Battery Info"
#define TXT_GBOX_GROUPBOX       "Wire Info"

#define TXT_DATE		"Date:"
#define TXT_CIRCUITID		"Circuit ID:"
#define TXT_PROJECT		"Project :"
#define TXT_NUMBER 		"Number:"
#define TXT_VOLTAGE 		"Voltage:"
#define TXT_CELLS 		"Cells:"
#define TXT_AMPS 		"Amps:"
#define TXT_STYLE 		"Style:"
#define TXT_GAUGE 		"Gauge:"
#define TXT_COLORCODE 		"Color Code:"
#define TXT_WATTS 		"Watts:"
#define TXT_MFG_ID 		"MFG ID:"
#define TXT_LIFEHRS 		"Life (hrs):"
#define TXT_COLOR 		"Color:"	

#define TXT_TITLE_CIRCUITCOMPONENTS 	"Circuit Tools"
#define TXT_TITLE_EDITWIRE	    	"Edit Wire Properties"
#define TXT_TITLE_EDITLIGHT	    	"Edit Light Properties"
#define TXT_TITLE_EDITBATTERY	    	"Edit Battery Properties"

#define TXT_FLYOVER_PLACEWIRE		"Place Wire"
#define TXT_BALLOON_PLACEWIRE		"Place Wire"

#define TXT_FLYOVER_PLACEBATTERY	"Place Battery"
#define TXT_BALLOON_PLACEBATTERY	"Place Battery"

#define TXT_FLYOVER_PLACELIGHT		"Place Light"
#define TXT_BALLOON_PLACELIGHT		"Place Light"

#define TXT_FLYOVER_EDITCOMP		"Edit Component Settings"
#define TXT_BALLOON_EDITCOMP		"Edit Component Settings"

#define TXT_FLYOVER_SETTINGS		"Edit Circuit Settings"
#define TXT_BALLOON_SETTINGS		"Edit Circuit Settings"
