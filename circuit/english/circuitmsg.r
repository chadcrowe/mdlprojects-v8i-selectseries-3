/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/english/circuitmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|																		|
|   $Workfile:   circuitmsg.r  $
|   $Revision: 1.4.76.1 $
|   	$Date: 2013/07/01 20:36:08 $
|																		|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|																		|
|   myappstr.r - MyApp Message list resource   	    	    			|
|																		|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|																		|
|   Include Files   													|
|																		|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "circuitcomp.h"

/*----------------------------------------------------------------------+
|																		|
|   Messages List Resource Definition									|
|																		|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|								        								|
|   The Message List resource structure is defined as follows:			|
|																		|
|   #if defined (resource)												|
|   	typedef struct __messagelist__									|
|   	    {															|
|   	    ULong	1;   # No. expected infoFields per string.			|
|   	    struct messages												|
|   	    	{														|
|   	    	ULong   infoFields[];									|
|       	char    msg[];												|
|       	} Messages [];												|
|	    } MessageList;													|
|																		|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_Commands =
{
    {
    {COMMANDID_PlaceBattery, 		"Place Battery"},
    {COMMANDID_PlaceLight,		"Place Light"},
    {COMMANDID_PlaceWire,		"Place Wire"},
    {COMMANDID_QueryCircuitElement,	"Query Circuit Element"},
    {COMMANDID_DrawingData,		"Drawing Data"},
    }
};

MessageList MESSAGELISTID_Prompts =
{
    {
    {PROMPTID_EnterFirst, 		"Enter first point"},
    {PROMPTID_EnterNext, 		"Enter next point"},
    {PROMPTID_EnterPoint, 		"Enter data point"},
    {PROMPTID_IdentifyElt,		"Identify element"},
    }

};

MessageList MESSAGELISTID_Msgs =
{
    {
    {MSGID_LoadCmdTbl,			"Unable to load command table"},
    {MSGID_BatteryCellName,		"BATTRY"},	  /* DO NOT TRANSLATE */
    {MSGID_LightCellName,		"LITE"},	  /* DO NOT TRANSLATE */
    {MSGID_FoundBattery,		"Found battery"},
    {MSGID_FoundLight,			"Found Light"	},
    {MSGID_FoundWire,			"Found Wire"	},
    {MSGID_NoDrawingData,		"No Circuit Data"},
    {MSGID_FoundDrawingData,		"Found Circuit Data"},
    {MSGID_defaultProjectName,		"rob"},
    {MSGID_defaultDrawingName,		" "},
    {MSGID_defaultDrawingDate,		" "},
    {MSGID_NotCircuitElement,		"Not a circuit element."},
    {MSGID_CellNotFound,		"Cell not found"},
    {MSGID_DataNotSaved,		"Could not save settings"},
    }
};
