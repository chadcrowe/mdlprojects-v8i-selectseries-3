/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/circuit/circuitcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#pragma suppressREQCmds
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   circuitcmd.r  $
|   $Revision: 1.5.32.1 $
|   $Date: 2013/07/01 20:35:42 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   circuitcmd.r - Command Table hierarchy 	    	    	    	|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

#pragma		suppressREQCmds

/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_CIRCUIT        1

DllMdlApp DLLAPP_CIRCUIT =
    {
    "CIRCUIT", "circuit"          // taskid, dllName
    }



/*----------------------------------------------------------------------+
|									|
|   Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE	    0
#define CT_ROOT     1
#define	CT_MAIN	    2
#define	CT_PLACE    3
#define CT_DIALOG   4
#define CT_EDIT     5


/*----------------------------------------------------------------------+
|									|
|   Table <tableId> =    						|
|   {									|
|      	{ <no.>, <subTableId>, <cmdClass>, <options>, "<cmdWord>" };	|
|   };									|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Circuit commands														|
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|                                                                       |
|   Main (Root) command word table										|
|                                                                       |
+----------------------------------------------------------------------*/
Table   CT_ROOT = 
{
    { 1,  CT_MAIN,	MANIPULATION,	REQ,	    "Circuit" 	},
}
Table	CT_MAIN =
{ 
    { 1,  CT_PLACE,	PLACEMENT,	REQ,	    "Place" 	}, 
    { 2,  CT_EDIT,      MANIPULATION,   NONE,       "Edit"	},
    { 3,  CT_DIALOG,	DIALOGOPEN,	REQ,	    "Dialog" 	}, 
};

/*----------------------------------------------------------------------+
|                                                                       |
|   CMD_PLACE subtable							|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_PLACE =
{
    { 1,  CT_NONE, 	INHERIT, 	NONE, 	    "Battery" 	},
    { 2,  CT_NONE, 	INHERIT, 	NONE, 	    "Light" 	},
    { 3,  CT_NONE, 	INHERIT, 	NONE, 	    "Wire" 	},
};

	
/*----------------------------------------------------------------------+
|                                                                       |
|   CT_DIALOG subtable							|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_DIALOG =
{ 
    { 1,  CT_NONE, 	INHERIT, 	NONE, 	    "CircuitTools" 	}, 
    { 2,  CT_NONE, 	INHERIT, 	NONE, 	    "Settings" 		}, 
};

/*----------------------------------------------------------------------+
|                                                                       |
|   CT_EDIT subtable													|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_EDIT =
{ 
    { 1,  CT_NONE, 	INHERIT, 	NONE, 	    "Component"	}, 
};
