/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevOffByDgn/LevOffByDgntyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevOffByDgn/LevOffByDgntyp.mtv  $
|   $Workfile:   LevOffByDgntyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevOffByDgn Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "LevOffByDgn.h"

publishStructures (LevOffByDgnglobals);

    
