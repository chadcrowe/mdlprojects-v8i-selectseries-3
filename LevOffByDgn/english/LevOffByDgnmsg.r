/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevOffByDgn/english/LevOffByDgnmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevOffByDgn/english/LevOffByDgnmsg.r_v  $
|   $Workfile:   LevOffByDgnmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevOffByDgn application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "LevOffByDgn.h"	/* LevOffByDgn dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_LevOffByDgnErrors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_LevOffByDgnDialog,   "Unable to open LevOffByDgn dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
