/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevOffByDgn/english/LevOffByDgntxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevOffByDgn/english/LevOffByDgntxt.h_v  $
|   $Workfile:   LevOffByDgntxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Static text defines for the LevOffByDgn application dialog resources	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__LevOffByDgntxtH__)
#define	__LevOffByDgntxtH__

#define	TXT_LevOffByDgnDialogBox			"LevOffByDgn Dialog Box"
#define	TXT_LevOffByDgnModalDialogBox			"LevOffByDgn Modal Dialog Box"
#define	TXT_Parameter1				"Parameter 1:"
#define	TXT_OpenModal				"Open Modal"
#define	TXT_IncrementParameter1			"Increment parameter 1?"
#define TXT_Value1				"Value 1"
#define TXT_Value2				"Value 2"
#define TXT_Value3				"Value 3"

#endif
