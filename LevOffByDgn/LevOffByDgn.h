/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevOffByDgn/LevOffByDgn.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevOffByDgn/LevOffByDgn.h_v  $
|   $Workfile:   LevOffByDgn.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in LevOffByDgn dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __LevOffByDgnH__
#define	    __LevOffByDgnH__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_LevOffByDgn		1   /* dialog id for LevOffByDgn Dialog */
#define DIALOGID_LevOffByDgnModal	2   /* dialog id for LevOffByDgn Modal Dialog */

#define OPTIONBUTTONID_LevOffByDgn	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_LevOffByDgn		1   /* id for "parameter 1" text item */
#define TOGGLEID_LevOffByDgn		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_LevOffByDgn		1   /* id for synonym resource */

#define MESSAGELISTID_LevOffByDgnErrors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_LevOffByDgnDialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_LevOffByDgn	1   /* id for toggle item hook func */
#define HOOKDIALOGID_LevOffByDgn		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct LevOffByDgnglobals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } LevOffByDgnGlobals;

#endif
