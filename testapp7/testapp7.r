/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp7/testapp7.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp7/testapp7.r_v  $
|   $Workfile:   testapp7.r  $
|   $Revision: 1.2.32.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	testapp7 Dialog Example Resources					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "testapp7.h"	/* testapp7 dialog box example constants & structs */
#include "testapp7cmd.h"	/* testapp7 dialog box command numbers */
#include "testapp7txt.h"	/* testapp7 dialog box static text defines */

/*----------------------------------------------------------------------+
|									|
|   testapp7 Dialog Box							|
|									|
+----------------------------------------------------------------------*/
#define X1 (14*XC)		/* text & option button x position */
#define X2 (7*XC)		/* push button x position */
#define XW (9*XC)		/* text & option button width */
#define BTN_WIDTH (12*XC)	/* push button width */

DialogBoxRsc DIALOGID_testapp7 =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    25*XC, 7*YC,
    NOHELP, MHELP, HOOKDIALOGID_testapp7, NOPARENTID,
    TXT_testapp7DialogBox,
{
{{X1,GENY(1),XW,0}, Text,	      TEXTID_testapp7, ON, 0, "", ""},
{{X1,GENY(2),XW,0}, OptionButton, OPTIONBUTTONID_testapp7, ON, 0, "", ""},
{{X2,GENY(4),BTN_WIDTH,0}, PushButton, PUSHBUTTONID_OModal, ON, 0,"",""},
}
    };

#undef X1		/* undef symbols so they can be reused */
#undef X2
#undef XW
#undef BTN_WIDTH

/*----------------------------------------------------------------------+
|									|
|   Modal Sub-Dialog Box						|
|   (opened when PUSHBUTTONID_OModal is activated)			|
|									|
+----------------------------------------------------------------------*/
#define X1 (1*XC)		/* toggle button x position */
#define X2 (3*XC)		/* OK button x position */
#define X3 (14*XC)		/* Cancel button x position */

DialogBoxRsc DIALOGID_testapp7Modal =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_MODAL,
    25*XC, 6*YC,
    NOHELP, MHELP, HOOKDIALOGID_testapp7, NOPARENTID,
    TXT_testapp7ModalDialogBox,
{
{{X1,GENY(1),0,0},		 ToggleButton, TOGGLEID_testapp7, ON, 0, "", ""},
{{X2,GENY(3),BUTTON_STDWIDTH,0}, PushButton, PUSHBUTTONID_OK,  ON, 0, "", ""},
{{X3,GENY(3),BUTTON_STDWIDTH,0}, PushButton, PUSHBUTTONID_Cancel, ON,0,"",""},
}
    };

#undef X1		/* undef symbols so they can be reused */
#undef X2
#undef X3

/*----------------------------------------------------------------------+
|									|
|   Item Resource Specifications					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Text Item Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_testapp7 =
    {
    NOCMD, LCMD, SYNONYMID_testapp7, NOHELP, MHELP, NOHOOK, NOARG,
    4, "%ld", "%ld", "1", "3", NOMASK, NOCONCAT,
    TXT_Parameter1,
    "testapp7Globals.parameter1"
    };

/*----------------------------------------------------------------------+
|									|
|   Option Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
DItem_OptionButtonRsc  OPTIONBUTTONID_testapp7 =
    {
    SYNONYMID_testapp7, NOHELP, MHELP, NOHOOK, NOARG,
    TXT_Parameter1,
    "testapp7Globals.parameter1",
	{
	{NOTYPE, NOICON, NOCMD, LCMD, 1, NOMASK, ON, TXT_Value1},
	{NOTYPE, NOICON, NOCMD, LCMD, 2, NOMASK, ON, TXT_Value2},
	{NOTYPE, NOICON, NOCMD, LCMD, 3, NOMASK, ON, TXT_Value3},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   PushButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_OModal =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP, NOHOOK, 0,
    0x01000000, LCMD, "",
    TXT_OpenModal
    };

/*----------------------------------------------------------------------+
|									|
|   Toggle Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEID_testapp7 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP,
    HOOKITEMID_ToggleButton_testapp7, NOARG, NOMASK, NOINVERT,
    TXT_IncrementParameter1,
    "testapp7Globals.parameter2"
    };

/*----------------------------------------------------------------------+
|									|
|   Synonym List Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_SynonymsRsc SYNONYMID_testapp7 =
    {
    	{
	{Text,		TEXTID_testapp7},
	{OptionButton,	OPTIONBUTTONID_testapp7},
	}
    };

