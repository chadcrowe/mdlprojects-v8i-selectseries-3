
#define PI 3.1415926535897932384626433
#include    <mdl.h>
#include    <toolsubs.h>
#include    <basetype.h>
#include    <malloc.h>
#include    <msrmgr.h>
#include    <mstypes.h>
#include    <string.h>
#include    <msparse.fdf>
#include    <msstate.fdf>
#include    <mssystem.fdf>
#include    <dlmsys.fdf>
#include    <mscnv.fdf>
#include    <msfile.fdf>
#include    <msdialog.fdf>
#include    <dlogids.h>
#include    <mswindow.fdf>
#include    <msdgnmodelref.fdf>
#include    <msdgnlib.fdf>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mscell.fdf>
#include    <msmline.fdf>
#include    <mstextstyle.fdf>
#include    <listmodel.fdf>
#include    <changetrack.fdf>
#include    <mselems.h>
#include    <scanner.h>
#include    <userfnc.h>
#include    <cmdlist.h>
#include    <string.h>
#include    <toolsubs.h>
#include    <dlogman.fdf>
#include    <mssystem.fdf>
#include    <mslinkge.fdf>
#include    <msscan.fdf>
#include    <msoutput.fdf>
#include    <msparse.fdf>
#include    <mselemen.fdf>
#include    <msrsrc.fdf>
#include    <mslocate.fdf>
#include    <msstate.fdf>
#include    <msscancrit.fdf>

#include <mstypes.h>
#include <msscancrit.fdf>
#include <elementref.h>

#include <msdgnobj.fdf>

#include    <msdgncache.h>
#include    <ditemlib.fdf>
//#include	<FontManager.h>
#include    <mselementtemplate.fdf>
#include    <msdimstyle.fdf>
#include    <msdim.fdf>
#include    <namedexpr.fdf>
#if !defined (DIM)
#define DIM(a) ((sizeof(a)/sizeof((a)[0])))
#endif


USING_NAMESPACE_BENTLEY_USTN
USING_NAMESPACE_BENTLEY_USTN_ELEMENT
USING_NAMESPACE_TEXT
#include <iostream>
#define LINEAR_TEMPLATE_NAME      L"CreatedGroup\\Created Linear Template"
#define SHAPE_TEMPLATE_NAME       L"CreatedGroup\\Created Shape Template"
#define HATCH_TEMPLATE_NAME       L"CreatedGroup\\Created Hatch Template"
#define AREAPATTERN_TEMPLATE_NAME L"CreatedGroup\\Created AreaPattern Template"
#define CELL_TEMPLATE_NAME        L"CreatedGroup\\Created Cell Template"
#define TEXT_TEMPLATE_NAME        L"CreatedGroup\\Created Text Template"
#define MLINE_TEMPLATE_NAME       L"CreatedGroup\\Created MLine Template"
#define DIMENSION_TEMPLATE_NAME   L"CreatedGroup\\Created Dimension Template"
#define HEADER_TEMPLATE_NAME      L"CreatedGroup\\Header"
#define COMPONENT1_TEMPLATE_NAME  L"CreatedGroup\\Component1"
#define COMPONENT2_TEMPLATE_NAME  L"CreatedGroup\\Component2"
#include <iostream>
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
int ScanForLinearElements(DgnModelRefP modelRef);
int ElementRefScanCallback(ElementRef, void *callbackArg, ScanCriteriaP);
#include <sstream>
#include <msselect.fdf>
#include <msmisc.fdf>
#include <ElemHandle.h>
#include <ElementAgenda.h>
#include <msview.fdf>
#include <msreffil.fdf>
#include <mselmdsc.fdf>
#include <leveltable.fdf>
//__declspec(dllexport) ElementID mdlElement_getID();
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
void IterateLevels();

template <typename T>
string NumberToString(T Number)
{
	ostringstream ss;
	ss << Number;
	return ss.str();
}

void appendToFile(int number)
{
	ofstream myfile;
	myfile.open("List Levels.txt", ios::app);
	myfile << NumberToString(number);
	myfile.close();
}
void appendToFile(MSWChar s[512])
{
	wofstream myfile;
	myfile.open("List Levels.txt", ios::app);
	myfile << s;
	myfile.close();
}
void appendToFile(string s)
{
	ofstream myfile;
	myfile.open("List Levels.txt", ios::app);
	myfile << s;
	myfile.close();
}
void writeToFile(string s)
{
	ofstream myfile;
	myfile.open("List Levels.txt");
	myfile << s;
	myfile.close();
}
extern "C"  DLLEXPORT int   MdlMain
(){
	writeToFile("");
	IterateLevels();
	mdlDialog_cmdNumberQueue(FALSE, CMD_MDL_UNLOAD, mdlSystem_getCurrTaskID(), TRUE);
	return SUCCESS;
}
//Note that LevelID is the wrong type in the MDL Function Reference example
int levels_process(LevelID levelID, void* args){
	MSWChar     levelName[512];
	UInt32      id;
	BoolInt levelDisplay;
	DgnModelRefP *modelRef = NULL;
	modelRef = new DgnModelRefP;
	modelRef = (DgnModelRefP*)args;
	mdlView_getLevelDisplay(&levelDisplay, *modelRef, 0, levelID, VIEW_LEVEL_DISPLAY_TYPE_NORMAL);
	//mdlLevel_getDisplay(&levelDisplay, mdlModelRef_getActive(), levelID);//This is for global display
	appendToFile("Is displayed ");
	if (levelDisplay)
	{
		appendToFile(1);
	}
	else
	{
		appendToFile(0);
	}
	appendToFile(", ");
	mdlLevel_getName(levelName, 512, *modelRef, levelID);
	appendToFile("The level name is ");
	appendToFile(levelName);
	appendToFile(",");
	if (SUCCESS == mdlLevel_getIdFromName(&id, mdlModelRef_getActive(), LEVEL_NULL_ID, levelName)){
		appendToFile(" the id is ");
		appendToFile(id);
	}
	else
	{
		if (SUCCESS == mdlLevel_getIdFromName(&id, mdlLevelLibrary_getModelRef(), LEVEL_NULL_ID, levelName)){
			//printf("the id is %ld in library \n", id);
			appendToFile(" the id is ");
			appendToFile(id);
		}
	}
	appendToFile("\n");
	return SUCCESS;
}

void IterateLevels(){
	LevelIteratorP  iteratorP;
	int             status;

	DgnFileObjP     dgnFile;
	dgnFile = mdlDgnFileObj_getMasterFile();
	DgnModelRefP dgnModelRef = mdlModelRef_getActive();
	ModelRefIteratorP  iterator;
	DgnModelRefP modelRef;

	//This next section will list the levels in the active model reference first
	MSWChar name[512];
	mdlModelRef_getDisplayName(mdlModelRef_getActive(), name, 512, NULL);
	appendToFile(name);
	appendToFile("\n");
	iteratorP = mdlLevelIterator_create(mdlModelRef_getActive());
	status = mdlLevelIterator_setIterateType(iteratorP, LEVEL_ITERATE_TYPE_ALL_LEVELS);
	status = mdlLevelIterator_setIterateLibraryLevels(iteratorP, TRUE, LEVEL_ITERATE_TYPE_ALL_LEVELS);
	//if you want to report the levels in the ref files you need to combine the model iterator into this.
	modelRef = mdlModelRef_getActive();
	mdlLevelIterator_traverse(iteratorP, levels_process, &modelRef);

	//List the levels in the references
	mdlModelRefIterator_create(&iterator, dgnModelRef, MRITERATE_PrimaryChildRefs, 0);
	while (NULL != (modelRef = mdlModelRefIterator_getNext(iterator)))
	{
		appendToFile("\n");
		mdlModelRef_getDisplayName(modelRef, name, 512, NULL);
		appendToFile(name);
		appendToFile("\n");
		iteratorP = mdlLevelIterator_create(modelRef);
		status = mdlLevelIterator_setIterateType(iteratorP, LEVEL_ITERATE_TYPE_ALL_LEVELS);
		status = mdlLevelIterator_setIterateLibraryLevels(iteratorP, TRUE, LEVEL_ITERATE_TYPE_ALL_LEVELS);
		mdlLevelIterator_traverse(iteratorP, levels_process, &modelRef); //send the modelRef address to the function to retrieve
	}

	mdlModelRefIterator_free(&iterator);
	return;
}
