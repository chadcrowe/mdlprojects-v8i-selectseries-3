/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp7/english/testapp7txt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp7/english/testapp7txt.h_v  $
|   $Workfile:   testapp7txt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Static text defines for the testapp7 application dialog resources	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__testapp7txtH__)
#define	__testapp7txtH__

#define	TXT_testapp7DialogBox			"testapp7 Dialog Box"
#define	TXT_testapp7ModalDialogBox			"testapp7 Modal Dialog Box"
#define	TXT_Parameter1				"Parameter 1:"
#define	TXT_OpenModal				"Open Modal"
#define	TXT_IncrementParameter1			"Increment parameter 1?"
#define TXT_Value1				"Value 1"
#define TXT_Value2				"Value 2"
#define TXT_Value3				"Value 3"

#endif
