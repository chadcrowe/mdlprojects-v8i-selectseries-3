/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cellexp/english/CellExpTxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#define         TEXTITEMLABEL_CELLNAME  "Cell Name"
#define         TXT_Tree                "Cells"
#define         TITLETEXT               "Cell Explorer"
#define         OPTIONSMENUTEXT         "Options"
#define         TEXTADDLIB              "~Add Library"
#define         OPENDIALOG              "opendialog"
#define         TREETITLETEXT           "cell library"
#define         TEXTITEMLABEL_CELLDESC  "CellDescrip:"
#define		TXT_CellName            "Cell Name"
#define		TXT_CellDescription	"Description"
#define		TXT_CellType		"Type"
#define		TXT_CellISThreeD	"3d/2d"
#define		TXT_CellIHidden		"Hidden"
#define		TXT_CellILocked		"Locked"
