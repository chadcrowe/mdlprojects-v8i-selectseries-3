/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cellexp/cellexp.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
//#include <fstream.h>
//#include <iostream.h>

#include <string.h>
#define __NORECTANGLE__     /* so we can include windows.h */
#include <windows.h>        /* for GetPrivateProfileString */
#include <math.h>
#undef  __NORECTANGLE__
#include <direct.h>   
#include <mselems.h>
#include <rtypes.h>
#include <system.h>
#include <cexpr.h>
#include <mdl.h>
#include <cmdlist.h>
#include <cmdclass.h>
#include <msstrlst.h >
#include <mdlerrs.h>
#include <msscancrit.h>
#include <stdio.h>
#include <toolsubs.h>
#include <malloc.h>


#include <mscexpr.fdf>
#include <msrsrc.fdf>
#include <msparse.fdf>
#include <msoutput.fdf>
#include <dlmsys.fdf>
#include <msstate.fdf>
#include <msfile.fdf>
#include <mssystem.fdf>
#include "CellExp.h"
#include <ditemlib.fdf>
#include <msdialog.fdf>
#include <listModel.fdf>
#include <mselmdsc.fdf >
#include <msscancrit.fdf>
#include <mswrkdgn.fdf>
#include <mscnv.fdf >
#include <msmodel.fdf >
#include <treemodel.fdf>
#include <mscell.fdf>
#include <msvar.fdf>   /* contains the extern declaration of the global variables*/
#include <modelindex.fdf>
#include <mswindow.fdf>
#include <msmisc.fdf>
#include <msrmatrx.fdf >
#include <msvec.fdf>
#include <mselemen.fdf >
#include <miscilib.fdf >
#include <msritem.fdf>
#include <msview.fdf>
#include <rdbmslib.fdf>
#include <MicroStationAPI.h>
//#include    <interface/IEvent.h>
//#include    <interface/MSElemHandle.h>
//#include    <interface/MSViewManager.h>

//#include    <interface/IViewContext.h>

#include "CellExpCmd.h"

int      gCurrentContainer;
BoolInt  gDetailList=FALSE;

/*----------------------------------------------------------------------+*//**
* Builds the list model that is used in the list view of the cell libraries                                    *
* @bsimethod BuildListModel                                             *
* @param        pList     IN OUT The list model that is being filled in with data            *
* @param        dbP      IN The dialog box so the tree item can be cross linked with the list box            *
* @return       the number of rows in the listmodel                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  int BuildListModel
(
ListModel       *pList, /*<==> the listModel to populate */
DialogBox       *dbP    /*<== the dialog box that holds the listmodel */ 
)
    {
    int                 count=0;
    DgnIndexIteratorP   pDgnIndexIterator;
    DgnFileObjP         pLibObj;
    MSWChar             wCellName[MAX_CELLNAME_LENGTH];//this should be unicode
    MSWChar             wCellDescription[MAX_CELLDSCR_LENGTH];
    BoolInt             isThreeD,isHidden,isLocked;
    int                 cellType;
    DgnIndexItemP       pDgnIndexItem;
    int                 iStatus,row,col;
    GuiTreeNode         *pNode,*pChildNode;
    GuiTreeCell         *pCell;
    ListRow             *pRow;
    ListCell            *pListCell;
    char                *pString;
    BoolInt             found;

    DialogItem  *pItem = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_Tree,TREEID_CellList,0);
    mdlDialog_treeLastCellClicked (&row,&col,pItem->rawItemP);

    mdlDialog_treeGetNextSelection (&found, &row, &col, pItem->rawItemP);
    pNode = mdlTreeModel_getDisplayRowAtIndex (mdlDialog_treeGetTreeModelP (pItem->rawItemP) ,row);
    pCell = mdlTreeNode_getCellAtIndex (pNode,0);

    iStatus = mdlTreeCell_getStringValue (pCell,&pString);

    iStatus = mdlCell_getLibraryObject (&pLibObj,pString,TRUE);

    pDgnIndexIterator = mdlModelIterator_create (pLibObj);
        
    mdlModelIterator_setAcceptCellsOnly (pDgnIndexIterator,TRUE);

    pChildNode = mdlTreeNode_getFirstChild (pNode);         
        while (pDgnIndexItem = mdlModelIterator_getNext (pDgnIndexIterator))
            {
            double      lastSaved;
            pRow = mdlListRow_create (pList);
            //do the name column
            mdlModelItem_getName (pDgnIndexItem, wCellName, MAX_CELLNAME_LENGTH);
            pListCell = mdlListRow_getCellAtIndex (pRow, CELLNAMECOL);
            mdlListCell_setStringValueW (pListCell,wCellName,TRUE);
            iStatus = mdlListCell_setInfoField (pListCell,0,(long)pChildNode);
            long    infoField;
            iStatus = mdlListCell_getInfoField (pListCell,0,&infoField);
            //do the description column
            mdlModelItem_getDescription (pDgnIndexItem, wCellDescription, MAX_CELLDSCR_LENGTH);
            pListCell = mdlListRow_getCellAtIndex (pRow,CELLDESCCOL);
            mdlListCell_setStringValueW (pListCell,wCellDescription,TRUE);
            mdlModelItem_getData (pDgnIndexItem,&cellType, &isThreeD, &isLocked,&isHidden,&lastSaved);
            cellType = mdlModelItem_getCellType (pDgnIndexItem);
                {
                pListCell = mdlListRow_getCellAtIndex (pRow,CELLTYPECOL);

                switch (cellType)
                    {

                    case CELLLIBTYPE_GRAPHIC:
                        mdlListCell_setStringValueW (pListCell,L"Grph",TRUE);
                        break;
                    case CELLLIBTYPE_POINT:
                        mdlListCell_setStringValueW (pListCell,L"Point",TRUE);
                        break;
                    case CELLLIBTYPE_MENU:
                        mdlListCell_setStringValueW (pListCell,L"Menu",TRUE);
                        break;
                    case CELLLIBTYPE_CBMENU:
                        mdlListCell_setStringValueW (pListCell,L"CBMenu",TRUE);
                        break;
                    case CELLLIBTYPE_MATRIXMENU:
                        mdlListCell_setStringValueW (pListCell,L"Matrix",TRUE);
                        break;
                    default:
                        mdlListCell_setStringValueW (pListCell,L"Undef",TRUE);
                        break;
                    }
                 }
            pListCell = mdlListRow_getCellAtIndex (pRow,CELLTHREEDCOL);
            if (isThreeD)
                mdlListCell_setStringValueW (pListCell,L"3",TRUE);
            else
                mdlListCell_setStringValueW (pListCell,L"2",TRUE);

            pListCell = mdlListRow_getCellAtIndex (pRow,CELLLOCKEDCOL);
            if (isLocked)
                mdlListCell_setStringValueW (pListCell,L"L",TRUE);
            else
                mdlListCell_setStringValueW (pListCell,L"U",TRUE);

            pListCell = mdlListRow_getCellAtIndex (pRow,CELLMODELIDCOL);
            ValueDescr  valDescr;
            valDescr.formatType=FMT_LONG;
            valDescr.value.sLongFormat=mdlModelItem_getModelID (pDgnIndexItem);
            mdlListCell_setValue (pListCell,&valDescr,FALSE);
            if(!isHidden)
                mdlListModel_addRow (pList,pRow);    
            pChildNode = mdlTreeNode_getNextChild (pNode,pChildNode);

            count++;
            }

        mdlModelIterator_free (pDgnIndexIterator);

    return count;
    }

/*----------------------------------------------------------------------+*//**
* Draws the cell if it is a 2d library                                                                        *
* @bsimethod Draw2DCell                                                 *                                               *
* @param        cellP IN the cell to display            *
* @param        dbP IN  the dialog to display the cell on             *
* @param        localRectP IN the rectangle to draw into            *
* @param        nDices  IN is the file 2d or 3d            *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void Draw2DCell
(
MSElementDescr      *cellP,
DialogBox           *dbP,
BSIRect             *localRectP,
ViewFlags           *viewflagsP,
BoolInt             nDices
)
    {
    RotMatrix       rMatrix;
    Dpoint3d        origin, newextent;
    DVector3d       dRangeVec;
    UInt32          colorMap[MAX_CMAPENTRIES];
    byte            colorTable[MAX_CTBLBYTES];
    
    mdlColor_getColorTableByModelRef (colorTable, MASTERFILE);
    mdlColor_matchLongColorMap (colorMap, colorTable, 0, FALSE);

    mdlRMatrix_getIdentity (&rMatrix);

    //mdlRMatrix_rotateRange (&range.org,&range.end,&rMatrix);
    mdlCnv_scanRangeToDRange (&dRangeVec, &cellP->el.hdr.dhdr.range);

    origin = dRangeVec.org;
    origin.z = 0.0;

    mdlVec_subtractPoint (&newextent,&dRangeVec.end,&origin);

    newextent.z = fc_1000;
    if (newextent.x < fc_1) newextent.x = fc_1000;
    if (newextent.y < fc_1) newextent.y = fc_1000;

    mdlDialog_rectInset(localRectP, 2, 2);

    mdlWindow_clipRectSet ((MSWindow *)dbP, localRectP);

    mdlElmdscr_extendedDisplayToWindow ((MSWindow *)dbP, localRectP,viewflagsP ,cellP, &rMatrix,
                &origin, &newextent, nDices, -1,colorMap,FALSE,NULL);

    mdlWindow_clipRectSet ((MSWindow *)dbP, NULL);

    }


/*----------------------------------------------------------------------+*//**
* Draws the cell if it is a 3d library                                                                        *
* @bsimethod Draw3DCell                                                 *                                               *
* @param        cellP IN the cell to display            *
* @param        dbP IN  the dialog to display the cell on             *
* @param        localRectP IN the rectangle to draw into            *
* @param        nDices  IN is the file 2d or 3d            *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void Draw3DCell
(
MSElementDescr      *cellP,
DialogBox           *dbP,
BSIRect             *localRectP,
ViewFlags           *viewflagsP,
BoolInt             nDices
)
    {
    BSIRect         localRectArray[4];
    int             deltaX;
    int             deltaY;
    int             winNumber;
    RotMatrix       rMatrixArray[4];
    Dpoint3d        newextent;
    DVector3d       dRangeVec;
   /* MstnColorMap          colorMap[MAX_CMAPENTRIES];
    IViewportP      sampleVp =  Bentley::Ustn::IViewManager::GetManager().GetFirstOpenView();
    if (sampleVp)
        for (int indexColor =0;indexColor<MAX_CMAPENTRIES  ;indexColor++ )
            {
            colorMap[indexColor] = Bentley::Ustn::ISessionMgr::GetMasterDgnFile()->GetColorMap()[indexColor];
            }
     
        colorMap [255] = sampleVp->GetBackgroundColor();
    if (userPrefsP->extFlags.invertBackground)
        colorMap [255] = 0xffffff;
    */

    //set up the views
    mdlView_getStandard (&rMatrixArray[0],STANDARDVIEW_Top);
    mdlView_getStandard (&rMatrixArray[1],STANDARDVIEW_Iso);
    mdlView_getStandard (&rMatrixArray[2],STANDARDVIEW_Front);
    mdlView_getStandard (&rMatrixArray[3],STANDARDVIEW_Right);

    deltaX = localRectP->corner.x - localRectP->origin.x;
    deltaY = localRectP->corner.y - localRectP->origin.y;

    localRectArray[0] = *localRectP;
    localRectArray[0].corner.y = localRectP->origin.y+(deltaY/2);
    localRectArray[0].corner.x = localRectP->origin.x+(deltaX/2);

    localRectArray[1] = *localRectP;
    localRectArray[1].origin.x = localRectP->origin.x+(deltaX/2);
    localRectArray[1].corner.y = localRectP->origin.y+(deltaY/2);

    localRectArray[2] = *localRectP;
    localRectArray[2].origin.y = localRectP->origin.y+(deltaY/2);
    localRectArray[2].corner.x = localRectP->corner.x-(deltaX/2);

    localRectArray[3]  = *localRectP;
    localRectArray[3].origin.x = localRectP->origin.x+(deltaX/2);
    localRectArray[3].origin.y = localRectP->origin.y+(deltaY/2);

    for (winNumber  = 0;winNumber <4  ;winNumber++ )
        {
        mdlCnv_scanRangeToDRange (&dRangeVec, &cellP->el.hdr.dhdr.range);

        mdlRMatrix_multiplyRange (&dRangeVec.org,&dRangeVec.end,&rMatrixArray[winNumber]);
        mdlVec_subtractPoint (&newextent,&dRangeVec.end,&dRangeVec.org);

        mdlRMatrix_unrotatePoint (&dRangeVec.org, &rMatrixArray[winNumber]);

        if (newextent.z< fc_1) newextent.z = fc_1000;
        if (newextent.x < fc_1) newextent.x = fc_1000;
        if (newextent.y < fc_1) newextent.y = fc_1000;


        mdlDialog_rectInset(&localRectArray[winNumber], 2, 2);

        mdlWindow_clipRectSet ((MSWindow *)dbP, localRectP);

        mdlElmdscr_extendedDisplayToWindow ((MSWindow *)dbP, &localRectArray[winNumber],viewflagsP ,cellP, &rMatrixArray[winNumber],
                                            &dRangeVec.org, &newextent, 0, -1,NULL,TRUE,NULL);
        mdlDialog_rectDrawEdge (dbP,&localRectArray[winNumber],TRUE);
        mdlWindow_clipRectSet ((MSWindow *)dbP, NULL);

        }
    }

/*----------------------------------------------------------------------+*//**
* The function draws the cell to a generic item.  
* It needs the dialog box the generic item and the cell to draw                                                                        *
* @bsimethod drawCellDetail                                             *                                               *
* @param        *dbP    IN The dialog box that holds the item to preview            *
* @param        diP     IN The generic item that will be drawn to            *
* @param        cellName  IN    The name of the cell that is to be drawn            *
* @param        is3d    IN is this a 3d cell 
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void drawCellDetail
(
DialogBox       *dbP,
DialogItem      *diP,
MSWChar         *cellName,
BoolInt         is3d
)
    {
    ViewFlags       viewflags;
    MSElementDescr *cellDP = NULL;
    BoolInt         nDices;
    int             status;
    
    status = mdlCell_getElmDscr (&cellDP,NULL,NULL,NULL,FALSE,NULL,0,0,FALSE,TRUE,cellName,NULL);
    
    if (SUCCESS!=status)
        {
        mdlOutput_messageCenter  (MESSAGE_ERROR,"no Preview Available","unable to preview error while getting data",FALSE);
        return;
        }

    mdlElmdscr_validate (cellDP, MASTERFILE);
    
    nDices = mdlModelRef_is3D (MASTERFILE);
    
    memset (&viewflags, 0, sizeof(viewflags));
    viewflags.patterns   = 1;
    viewflags.ed_fields  = 1;
    viewflags.on_off     = 1;
    viewflags.points     = 1;
    viewflags.constructs = 1;
    viewflags.dimens     = 1;

    if (!is3d)
        Draw2DCell (cellDP, dbP, &diP->rect, &viewflags, nDices);
    else
        Draw3DCell (cellDP, dbP, &diP->rect, &viewflags, nDices);
        

    mdlElmdscr_freeAll (&cellDP);
    }
/*----------------------------------------------------------------------+*//**
* builds the Icon view for the cell                                                                        *
* @bsimethod constructIconViews                                        *                                               *
* @param        dbP IN the dialog to display to            *
* @param        ctPanel IN  the container Panel, if it is a sigle cell or the entire library they are seperate panels *
* @param        draw    IN  to draw or not            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private void    constructIconViews
(
DialogBox   *dbP,
DialogItem  *ctPanel,
BoolInt     draw
)
    {
    char                 dummy[2048];
    int                  nCol, nRow;
    int                  gap, buttonSize, halfGap;
    long                 count; 
    Point2d              origin;
    DialogItemRsc       *diRsc=(DialogItemRsc *)(dummy);
    DialogItem          *diP=NULL;
    ListModel           *pListModel;
    int                  attrib;
    int                  containerId;
    BoolInt              bStatus;

    DialogItem *ctPanelDiP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_ContainerPanel, CTPANELID_Detail, 0);

    mdlDialog_containerPanelGetInfo (&attrib,&containerId,NULL,NULL,ctPanelDiP->rawItemP);
    DialogItem *pTemp = ctPanel;
    gap = 2;// userDataP->csPrefs.gapPixels;
    halfGap = gap/2;

    int startIndex = 4;
     /* --- free all items currently drawn --- */
    if (pTemp)
        {
        int childCount =  mdlDialog_rItemChildGetCount (pTemp->rawItemP);
        bStatus = mdlDialog_rItemChildFree (pTemp->rawItemP,0,childCount-1);
        }

    pListModel = mdlListModel_create (7);
    int length = BuildListModel (pListModel,dbP);
    int numCols;
    int hbuttonSize;

    numCols = length > (int)floor (sqrt ((double)pTemp->extent.width))/2? (int)floor (sqrt ((double)pTemp->extent.width))/2:length ;
    hbuttonSize = (pTemp->extent.width/numCols)-halfGap;
    if (numCols<=0) return;
    int vbuttonSize;
    int numRows = length;

    numRows = length >(int) floor(sqrt ((double)pTemp->extent.height))/2?(int) floor(sqrt ((double)pTemp->extent.height))/2:length;

    if (numRows<=0) 
        numRows=1;

    vbuttonSize = (pTemp->extent.height/numRows)-halfGap;

    if (vbuttonSize>hbuttonSize)
        {
        numCols = length/numRows;
        buttonSize = vbuttonSize;
        }
    else
        {
        numRows = length/numCols;
        buttonSize = hbuttonSize;
        }

    /* --- set dialog item resource for generic item --- */
    memset (dummy, 0, sizeof (dummy));
    diRsc->extent.origin.x = 0;
    diRsc->extent.origin.y = 0;
    diRsc->attributes      = ON;
    diRsc->extent.width    = (short) -buttonSize;
    diRsc->extent.height   = (short) -buttonSize;
    diRsc->type            = RTYPE_Generic;
    diRsc->id              = GENERICID_CellIcon;

    /* --- build each generic item count across the row --- */
    for (nCol = 0, count = 0L; nCol < numCols; nCol++)
        {
        origin.x = (pTemp->extent.origin.x+(buttonSize/2))+((buttonSize+gap) * nCol) + gap;
        origin.x = -origin.x;  /* in pixels */
        
        for (nRow = 0; nRow <= numRows; nRow++)
            {
            origin.y = (pTemp->extent.origin.y)+(buttonSize+gap) * nRow;
            origin.y = -origin.y;   /* in pixels */

            /* --- set item argument to generic item count --- */
            if (count<length)
                {
                ListCell *pCell= mdlListModel_getCellAtIndexes (pListModel,count++,CELLMODELIDCOL);
                ValueDescr  val;
                if (pCell)
                    mdlListCell_getValue (pCell,&val);
                else
                    val.value.sLongFormat = -1;
                diRsc->itemArg = val.value.sLongFormat;

                diP=(DialogItem *)dlmSystem_mdlCalloc (1, sizeof (DialogItem));

                mdlDialog_rItemDiPInitialize (diP,dbP,diRsc,&origin);

                diP->itemIndex=count-1;
                diP->rawItemP = mdlDialog_rItemChildLoad (diP,sizeof *diP,pTemp->rawItemP,"X","2",NULL,NULL,NULL,-1,TRUE,FALSE);
                BoolInt aStatus;
                aStatus = mdlDialog_containerPanelItemAdded (ctPanelDiP->rawItemP, diP->rawItemP,draw);
                dlmSystem_mdlFree (diP);
                }
            }
        }
    mdlListModel_destroy (pListModel,TRUE);
    }

/*----------------------------------------------------------------------+*//**
* Handle the resizing of the dialog                                                                        *
* @bsimethod cellExp_adjustVSashDialogItems                                        *                                               *
* @param        db  IN  The dialog that is being resized            *
* @param        forceCTErase    IN wether to force the update of the entire container before      *
* @param        pOldContent IN  the old extents before this resize          *
* @param        refreshItems    IN redraw after the resize            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private void    cellExp_adjustVSashDialogItems

(
DialogBox       *db,
BoolInt         forceCTErase,
BSIRect         *pOldContent,
BoolInt         refreshItems
)
    {
    DialogItem  *sashDiP;
    DialogItem  *treeDiP;
    DialogItem  *ctPanelDiP;
    BoolInt     eraseFirst=TRUE;
    int         attrib;
    int         containerId;
    
    /* Get the 3 main items on the dialog */
    sashDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Sash, SASHID_VDivider, 0);
    treeDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Tree, TREEID_CellList, 0);
    ctPanelDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ContainerPanel, CTPANELID_Detail, 0);

    eraseFirst = forceCTErase;
    mdlDialog_containerPanelGetInfo (&attrib,&containerId,NULL,NULL,ctPanelDiP->rawItemP);
    //handle the resize of the items to make them always fit.
    switch (containerId)
        {
        case CONTAINERID_CellListing:
            {
            DialogItem  *pListBox = mdlDialog_itemGetByTypeAndId (db,RTYPE_ListBox,LISTBOXID_CellListing,0);
            Sextent extents = ctPanelDiP->extent;
        
            extents.origin.y = extents.origin.y+2*XC;
            extents.height = extents.height-(2*XC);
            extents.width = ctPanelDiP->extent.width;

            if (pListBox)
            mdlDialog_itemSetExtent (db,pListBox->itemIndex,&extents,TRUE);
            //eraseFirst = TRUE;
            /* Resize, reposition and draw the items */
            mdlDialog_itemsAdjustFlushWithSash (db, NULL, pOldContent, sashDiP, 
                                                treeDiP, refreshItems, refreshItems,
                                                ctPanelDiP, eraseFirst, refreshItems);
            
            }

            break;
        case CONTAINER_TextInfo:
            {
            DialogItem  *pPreview = mdlDialog_itemGetByTypeAndId (db,RTYPE_Generic,GENERICID_CellPreview,0);
            DialogItem  *pTextDescr = mdlDialog_itemGetByTypeAndId (db,RTYPE_Text,TEXTID_CellDescription,0);
            Sextent     extents = pPreview->extent;
            extents.origin.x = ctPanelDiP->extent.origin.x+(XC);
            extents.origin.y = (pTextDescr->extent.origin.y)+(4*XC);
            extents.width=ctPanelDiP->extent.width-(2*XC);
            extents.height=ctPanelDiP->extent.height-(extents.origin.y)-(2*XC);
            mdlDialog_itemSetExtent (db,pPreview->itemIndex,&extents,FALSE);
            /* Resize, reposition and draw the items */
            mdlDialog_itemsAdjustFlushWithSash (db, NULL, pOldContent, sashDiP, 
                                                treeDiP, refreshItems, refreshItems,
                                                ctPanelDiP, eraseFirst, refreshItems);
            
            }

            break;
        case CONTAINERID_CellIconView:
            {
            DialogItem *pTemp = mdlDialog_itemGetByTypeAndId (db,RTYPE_Container,containerId,0);
            /* Resize, reposition and draw the items */
            mdlDialog_itemsAdjustFlushWithSash (db, NULL, pOldContent, sashDiP, 
                                                treeDiP, refreshItems, refreshItems,
                                                ctPanelDiP, eraseFirst, refreshItems);
            constructIconViews (db,pTemp,FALSE);
            }
            break;
        default:

            break;
        }

    }

/*----------------------------------------------------------------------+*//**
* When a tree node is selcted this starts the rebuilding of the list model on the main panel*
* @bsimethod populateCellListing                                        **
* @param        pNode   IN  the node that is currently selected        *
* @param        dbP IN  the dialog box that holds the items           *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void populateCellListing
(
GuiTreeNode     *pNode,
DialogBox       *dbP
)
    {
    ListModel *pList;
    
    DialogItem  *pItem = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_ListBox,LISTBOXID_CellListing,0);
    
    pList = mdlDialog_listBoxGetListModelP (pItem->rawItemP);
    
    mdlListModel_empty (pList,TRUE);
    
    BuildListModel (pList,dbP);
    
    mdlDialog_listBoxSetListModelP (pItem->rawItemP,pList,CELLLISTMODELCOLS);

    }


/*----------------------------------------------------------------------+*//**
* The function to draw the cell to a generic item.  It loops through the cells
* finds the cell to display then invokes the code to draw.                                                                        *
* @bsimethod drawCellIcon                                               *                                               *
* @param        dbP IN the dialog to draw the Icon on            *
* @param        diP IN the dialog item to draw the Icon on this should be a container           *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void drawCellIcon
(
DialogBox       *dbP,
DialogItem      *diP
)
    {
    int         iStatus,count;
    DgnFileObjP pLibObj;
    int         row,col,found;
    char        *pString;
    BoolInt     is3d;
    DgnIndexItemP   pDgnIndexItem;

    DialogItem  *pItem = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_Tree,TREEID_CellList,0);
    
    mdlDialog_treeLastCellClicked (&row,&col,pItem->rawItemP);

    mdlDialog_treeGetNextSelection (&found, &row, &col, pItem->rawItemP);
    
    GuiTreeNode *pNode = mdlTreeModel_getDisplayRowAtIndex (mdlDialog_treeGetTreeModelP (pItem->rawItemP) ,row);
    
    GuiTreeCell *pCell = mdlTreeNode_getCellAtIndex (pNode,0);

    iStatus = mdlTreeCell_getStringValue (pCell,&pString);

    iStatus = mdlCell_getLibraryObject (&pLibObj,pString,TRUE);

    DgnIndexIteratorP  pDgnIndexIterator = mdlModelIterator_create (pLibObj);
        
    mdlModelIterator_setAcceptCellsOnly (pDgnIndexIterator,TRUE);
    count =0;
    while (pDgnIndexItem = mdlModelIterator_getNext (pDgnIndexIterator))
        {
        if (diP->itemArg == mdlModelItem_getModelID (pDgnIndexItem) )
            {
            MSWChar     cellName[MAX_CELLNAME_LENGTH];

            mdlModelItem_getName (pDgnIndexItem, cellName, MAX_CELLNAME_LENGTH);
            mdlModelItem_getData (pDgnIndexItem,NULL,&is3d,NULL, NULL,NULL);
            drawCellDetail (dbP,diP,cellName,is3d);

            break;
            }
        else
            count++;
        }
    }

/*----------------------------------------------------------------------+*//**
* !!!Describe Function Completely!!!                                                                        *
* @bsimethod drawPreview                                                *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private void    drawPreview
(
DialogBox       *dbP,
DialogItem      *diP
)
    {
    int             row,col;
    BoolInt         found;
    GuiTreeNode    *pNode;
    GuiTreeCell    *pCell;
    MSWChar        *pWString=NULL;
    DialogItem     *pItem;
    ValueDescr      val;

    pItem  = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_Tree,TREEID_CellList,0);
    mdlDialog_treeLastCellClicked (&row,&col,pItem->rawItemP);

    mdlDialog_treeGetNextSelection (&found, &row, &col, pItem->rawItemP);
    pNode = mdlTreeModel_getDisplayRowAtIndex (mdlDialog_treeGetTreeModelP (pItem->rawItemP) ,row);
    pCell = mdlTreeNode_getCellAtIndex (pNode,0);
    mdlTreeCell_getDisplayTextW (pCell,&pWString);
    mdlTreeCell_getValue (pCell,&val);
    CellData    *pCellData=(CellData*)val.value.voidPFormat;

    drawCellDetail (dbP,diP,pWString,pCellData->isThreeD);
    }

/*----------------------------------------------------------------------+*//**
* This function will put the information on the "nodes" of the tree item.  *
* @bsimethod setChildNodes                                              *                                               *
* @param        pModel     The tree model that is being worked on            *
* @param        pRootNode     The current root node            *
* @param        pLibObj     the cell library object            *
* @param        cellLibraryName     The name of the cell library            *
* @return       The number of nodes that are added.                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  int setChildNodes
(
GuiTreeModel    *pModel,
GuiTreeNode     *pRootNode,
DgnFileObjP      pLibObj,
char            *cellLibraryName
)
    {
    int     count=0;
    DgnIndexIteratorP   pDgnIndexIterator;
    MSWChar             wCellName[MAX_CELLNAME_LENGTH];//this should be unicode
    BoolInt             isThreeD;
    DgnIndexItemP       pDgnIndexItem;
    CellData            *pCellData;

    pDgnIndexIterator = mdlModelIterator_create (pLibObj);
    
    mdlModelIterator_setAcceptCellsOnly (pDgnIndexIterator,TRUE);

        
    while (pDgnIndexItem = mdlModelIterator_getNext (pDgnIndexIterator))
        {
        GuiTreeNode     *pNode;
        GuiTreeCell     *pCell;
        MSWChar         wCellDescription[MAX_CELLDSCR_LENGTH];
        int             cellType;
        int             isLocked,isHidden;
        double          lastSaved;
        ValueDescr      val;

        pNode   = mdlTreeNode_create (pModel,FALSE);
        //do the name column
        mdlModelItem_getName (pDgnIndexItem, wCellName, MAX_CELLNAME_LENGTH);
        pCell = mdlTreeNode_getCellAtIndex (pNode,0);
        mdlTreeCell_setDisplayTextW (pCell,wCellName);

        //do the description column
        mdlModelItem_getDescription (pDgnIndexItem, wCellDescription, MAX_CELLDSCR_LENGTH);
        mdlModelItem_getData (pDgnIndexItem,&cellType, &isThreeD, &isLocked,&isHidden,&lastSaved);
        //populate the node info!   This is stored in the node.
        pCellData = (CellData*)dlmSystem_mdlMalloc (sizeof *pCellData);
        pCellData->cellType=cellType;
        pCellData->isHidden=isHidden;
        pCellData->isThreeD=isThreeD;
        pCellData->isLocked=isLocked;
        wcscpy (pCellData->description,wCellDescription);
        wcscpy (pCellData->name,wCellName);
        strcpy (pCellData->LibraryName,cellLibraryName);

        val.formatType=FMT_VOIDP;
        val.value.voidPFormat=pCellData;
        mdlTreeCell_setValue (pCell,&val,FALSE);
        if (!pCellData->isHidden)
            mdlTreeNode_addChild (pRootNode,pNode);
#if defined (debug)
        printf ("the cellname is '%S' and the description is '%S' \n", wCellName, wCellDescription);
#endif  
        count++;
        }

    mdlModelIterator_free (pDgnIndexIterator);

    return count;
    }

/*----------------------------------------------------------------------+*//**
* This function visits each node of the tree and in this context frees the memory for each item. *
* This is a recursive function.
* @bsimethod cellExp_visitNodes                                    *                                               *
* @param        pNode     The node that is being visited.            *
* @return       Always returns success                          *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  int cellExp_visitNodes
(
GuiTreeNode    *pNode
)
    {
    StatusInt       status = SUCCESS;
    int             iChildCnt, index;
    GuiTreeNode     *pChildNode;
    GuiTreeCell     *pCell;
    ValueDescr      val;

    /* Process the Tree Node */
    /* ... Do whatever you want with pNode ... */
   
    /* Traverse thru all descendents */
    iChildCnt = mdlTreeNode_getChildCount (pNode);
    for (index=0; index < iChildCnt; index++)
            {
            pChildNode = mdlTreeNode_getChildAtIndex (pNode, index);
	    
	    pCell = mdlTreeNode_getCellAtIndex (pChildNode,0);

	     //get the pointer in the cell
	    mdlTreeCell_getValue (pCell,&val);
	    //free the resources
	    if ((FMT_VOIDP == val.formatType)&& (0!=val.value.voidPFormat))
		dlmSystem_mdlFree (val.value.voidPFormat);

            if (NULL != pChildNode && mdlTreeNode_isParent (pChildNode))
                {
                if (SUCCESS != (status = cellExp_visitNodes (pChildNode)))
                            break;
                }
            }

    return status;
    }

/*----------------------------------------------------------------------+*//**
* populates the treemodel.  This is used by the hook function CREATE state.*
* @bsimethod cellExp_populateTree                                  *                                               *
* @return       The TreeModel that holds all the cells.                 *
*                                                                       *
* Author:   MarkAnderson                                10/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  GuiTreeModel* cellExp_populateTree
(
void
)
    {
    GuiTreeModel   *pTreeModel;
    GuiTreeNode    *pNode;
    GuiTreeCell    *pCell;
    DgnFileObjP     pLibObj;
    int             iStatus;
    GuiTreeNode    *pRootNode;

    pTreeModel = mdlTreeModel_create (1);

    pRootNode = mdlTreeModel_getRootNode (pTreeModel);

    //set up the active Cell library first!
    iStatus = mdlCell_getLibraryObject (&pLibObj,tcb->celfilenm,TRUE);
    if (SUCCESS==iStatus)
        {
        pNode = mdlTreeNode_create (pTreeModel,TRUE);
        pCell = mdlTreeNode_getCellAtIndex (pNode,0);
        mdlTreeNode_setCollapsedIcon (pNode, ICONID_VolumeFolder, RTYPE_Icon, NULL);
        mdlTreeNode_setExpandedIcon (pNode, ICONID_OpenVolumeFolder, RTYPE_Icon, NULL);
        mdlTreeNode_setExpandedSelectedIcon (pNode, ICONID_CurrentVolumeFolder, RTYPE_Icon, NULL);
        mdlTreeCell_setStringValue (pCell,tcb->celfilenm,TRUE);
        mdlTreeNode_addChild (pRootNode,pNode);
        setChildNodes (pTreeModel,pNode,pLibObj,tcb->celfilenm);
        }
//this is left commented out since this loop can significantly slow the application
//if you want to try this then set MS_CELLLIST to a small set of libraries.
#if defined (FOUNDMYMIND)
    {
     char cellLibName[MAXFILENAME];
    //process the libraries in the celllist now
    char *celllist = mdlSystem_getExpandedCfgVar ("MS_CELLLIST");
    cellLibName = strtok (celllist,";");
    while   (cellLibName)
        {
        iStatus = mdlCell_getLibraryObject (&pLibObj,cellLibName,TRUE);

        if (SUCCESS == iStatus)
            {
            pNode = mdlTreeNode_create (pTreeModel,TRUE);
            pCell = mdlTreeNode_getCellAtIndex (pNode,0);
            mdlTreeNode_setCollapsedIcon (pNode, ICONID_VolumeFolder, RTYPE_Icon, NULL);
            mdlTreeNode_setExpandedIcon (pNode, ICONID_OpenVolumeFolder, RTYPE_Icon, NULL);
            mdlTreeNode_setExpandedSelectedIcon (pNode, ICONID_CurrentVolumeFolder, RTYPE_Icon, NULL);
            mdlTreeCell_setStringValue (pCell,cellLibName,TRUE);
            mdlTreeNode_addChild (pRootNode,pNode);
            setChildNodes (pTreeModel,pNode,pLibObj,cellLibName);
            }

        cellLibName = strtok (NULL,";");
        }
    dlmSystem_mdlFree (celllist);
    }
#endif

    return pTreeModel;
    }

/*----------------------------------------------------------------------+*//**
* The callback function that is called when the tree item is interacted with.                                                                        *
* @bsimethod treeHook                                                   *                                               *
* @param        dimP     The message structure that is sent to the dialog item.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void treeHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_INIT:
            {
            GuiTreeModel   *pTreeModel = cellExp_populateTree ();
            mdlDialog_treeSetTreeModelP (dimP->dialogItemP->rawItemP,pTreeModel);
            BoolInt bStatus = mdlDialog_treeSetContainer (dimP->dialogItemP->rawItemP,gCurrentContainer,mdlSystem_getCurrMdlDesc(),FALSE);
            }
        break;
        case DITEM_MESSAGE_BUTTON:
            {
            if ((BUTTONTRANS_UP==dimP->u.button.buttonTrans)&&(dimP->u.button.upNumber==1))
                {
                int         row,col;
                mdlDialog_treeLastCellClicked (&row,&col,dimP->dialogItemP->rawItemP);
                if (row >=0)
                    {
                    BoolInt     found;
                    GuiTreeNode    *pNode;
                    MSWChar     *pWString=NULL;
                    ValueDescr  val;
                    mdlDialog_treeGetNextSelection (&found, &row, &col, dimP->dialogItemP->rawItemP);
                    pNode = mdlTreeModel_getDisplayRowAtIndex (mdlDialog_treeGetTreeModelP (dimP->dialogItemP->rawItemP) ,row);
                    GuiTreeCell    *pCell = mdlTreeNode_getCellAtIndex (pNode,0);
                    mdlTreeCell_getValue (pCell, &val);
                    mdlTreeCell_getDisplayTextW (pCell,&pWString);
                    if (pWString)
                        {
                        BoolInt     changed;
                        CellData    *pCellData=(CellData*)val.value.voidPFormat;
                        DialogItem  *pItem  = mdlDialog_itemGetByTypeAndId (dimP->db,RTYPE_Text,TEXTID_CellName,0);
                        mdlDialog_itemSetStringValueW (&changed,pWString,dimP->db,pItem->itemIndex);
                        mdlDialog_itemSynch (dimP->db,pItem->itemIndex);
                        pItem  = mdlDialog_itemGetByTypeAndId (dimP->db,RTYPE_Text,TEXTID_CellDescription,0);

                        mdlDialog_itemSetStringValueW (&changed, pCellData->description,dimP->db,pItem->itemIndex);
                        mdlDialog_itemSynch (dimP->db,pItem->itemIndex);
                        mdlParams_setActive ((void*)pWString,ACTIVEPARAM_WCHAR_CELLNAME);
                        }
                    }
                }
               else if((BUTTONTRANS_UP==dimP->u.button.buttonTrans)&&(dimP->u.button.upNumber==2))
                {
                mdlDialog_cmdNumberQueue (FALSE,CMD_PLACE_CELL_ICON,"",TRUE);
                }
            }
            break;

        case DITEM_MESSAGE_STATECHANGED:
            {
            int             row=-1, col=-1;
            BoolInt         found,bChanged=FALSE;
            GuiTreeNode     *pNode;
            int             containerID;

            GuiTreeModel *pModel = mdlDialog_treeGetTreeModelP (dimP->dialogItemP->rawItemP);

            DialogItem *ctPanelDiP = mdlDialog_itemGetByTypeAndId (dimP->db , RTYPE_ContainerPanel, CTPANELID_Detail, 0);

            mdlDialog_containerPanelGetInfo (NULL,&containerID,NULL,NULL,ctPanelDiP->rawItemP);
            mdlDialog_treeLastCellClicked (&row, &col, dimP->dialogItemP->rawItemP);
            if  (row != INVALID_ITEM && col != INVALID_ITEM)
                {
                row = -1;
                col = -1;

                /* Single selection */
                mdlDialog_treeGetNextSelection (&found, &row, &col, dimP->dialogItemP->rawItemP);
                
                if  (found)
                    {
                    if (NULL != (pNode = mdlTreeModel_getDisplayRowAtIndex (pModel, row)))
                        {
                        ValueDescr  valueDescr;
                
                        mdlTreeNode_getValue (pNode, &valueDescr);
                        if (mdlTreeNode_isLeaf (pNode))
                            {
                            mdlDialog_treeSetContainer (dimP->dialogItemP->rawItemP,CONTAINER_TextInfo,mdlSystem_getCurrMdlDesc(),FALSE);
                            gCurrentContainer = CONTAINER_TextInfo;
                            bChanged = TRUE;
                            }
                        else
                            {
                            if (gDetailList)
                                {
                                mdlDialog_treeSetContainer (dimP->dialogItemP->rawItemP,CONTAINERID_CellListing,mdlSystem_getCurrMdlDesc(),FALSE);
                                populateCellListing (pNode,dimP->db);
                                gCurrentContainer = CONTAINERID_CellListing;
                                }
                            else
                                {
                                mdlDialog_treeSetContainer (dimP->dialogItemP->rawItemP,CONTAINERID_CellIconView,mdlSystem_getCurrMdlDesc(),FALSE);
                                DialogItem *pContainer = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Container, CONTAINERID_CellIconView, 0);
                                gCurrentContainer = CONTAINERID_CellIconView;
                                }
                            bChanged = TRUE;
                            }
                        }
                    if  (bChanged)
                        {
                        /* Redraw 3 main items in dialog (Tree, Sash, CTPanel) */
           //             MdlWin32InvalidateWindow (dimP->db);
                        cellExp_adjustVSashDialogItems (dimP->db,TRUE,NULL,TRUE);
                        }
                    else
                        {
                        DialogItem *ctPDiP;
                        /* Redraw the ContainerPanel */
                        ctPDiP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ContainerPanel, CTPANELID_Detail, 0);

                        if (ctPDiP)
                            mdlDialog_itemDrawEx (dimP->db, ctPDiP->itemIndex, TRUE);
                        
                        }
                    }
                }
            }
            break;

        case DITEM_MESSAGE_DESTROY:
            {
            GuiTreeModel   *pTreeModel = mdlDialog_treeGetTreeModelP (dimP->dialogItemP->rawItemP);
            //need to free the structure
            
            cellExp_visitNodes (mdlTreeModel_getRootNode (pTreeModel));

            mdlTreeModel_destroy (pTreeModel,TRUE);
            }
            break;
        default:
            dimP->msgUnderstood= FALSE;
            break;
        }
    }
/*----------------------------------------------------------------------+*//**
* This dummy function is left as an example of reacting to the sash item moving.                                                                        *
* @bsimethod populateCellListing                                        *                                               *
* @param        mfaP     Information passed to this function as the sash is moved.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private void cellExp_vSashMotionFunc   /* called each time user moves mouse
                                           while dragging sash */
(
MotionFuncArg   *mfaP   /* <> mfaP->pt.x = where sash upper left corner will go */
                        /* => mfaP->pt.y = where cursor x position is */
                        /* => mfaP->dragging = TRUE if cursor is within window */
)
    {
    /*if (mfaP->pt.x < MYInfo.minSashX)
        mfaP->pt.x = MYInfo.minSashX;

    if (mfaP->pt.x > MYInfo.maxSashX)
        mfaP->pt.x = MYInfo.maxSashX;*/
    }


/*----------------------------------------------------------------------+*//**
* The callback function for the sash item                                                                       *
* @bsimethod treeHook                                                   *                                               *
* @param        dimP     The structure passed to the dialog item handler.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void dividerHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_BUTTON:
            {
            if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
                {
                BSIRect  contentRect;
                int      fontHeight = mdlDialog_fontGetCurHeight (dimP->db);

                mdlWindow_contentRectGetLocal (&contentRect,(MSWindow *) dimP->db);
                }
            else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
                {
                Sash_ButtonUpAuxInfo *buaiP =
                    (Sash_ButtonUpAuxInfo *) dimP->auxInfoP;

                /* use buaiP->newXPos to determine where upperLeft corner
                   of sash beveled rect will go.  This message is sent after
                   sash has been erased from old position & moved, but before
                   it has been drawn */
                if (buaiP->newXPos != buaiP->oldXPos)
                    cellExp_adjustVSashDialogItems (dimP->db, TRUE, NULL, TRUE);
                }

            break;
            }

        default:
            dimP->msgUnderstood = FALSE;
            break;
        }


    return ;
    }
/*----------------------------------------------------------------------+*//**
* A callback for the container panel                                                                        *
* @bsimethod treeHook                                                   *                                               *
* @param        dimP     the structure that the dialog manager passes to the hook function.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void ctpanelHook
(
DialogItemMessage   *dimP
)
    {
    return ;
    }
/*----------------------------------------------------------------------+*//**
* 
q                                                                        *
* @bsimethod treeHook                                                   *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void textItemHook
(
DialogItemMessage   *dimP
)
    {
    return ;
    }


/*----------------------------------------------------------------------+*//**
* !!!Describe Function Completely!!!                                                                        *
* @bsimethod textItemDescriptonHook                                     *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void textItemDescriptonHook
(
DialogItemMessage       *dimP
)
    {
    return ;
    }

/*----------------------------------------------------------------------+*//**
* The callback function for the Generic item.  This will draw the graphics on the dialog item.                                                                        *
* @bsimethod genericPreviewItemHook                                     *                                               *
* @param        dimP     the structure that is passed to the dialog item handler.            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void genericPreviewItemHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood=TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_DRAW:
            {
            mdlDialog_rectFill(dimP->db, &dimP->dialogItemP->rect,BLACK_INDEX);

        /*draw the generic item */
            drawPreview (dimP->db,dimP->dialogItemP);
            break;
            }
        default :
        dimP->msgUnderstood = FALSE;
        break;
        }
    return ;
    }

/*----------------------------------------------------------------------+*//**
* the callback function for the dialog.                                                                        *
* @bsimethod dialogHook                                                 *                                               *
* @param        dmP     The structure passed to the dialog handler.            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void dialogHook
(
DialogMessage       *dmP
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
        {
        case DIALOG_MESSAGE_CREATE:
            {
            dmP->u.create.interests.windowMoving= TRUE;
            dmP->u.create.interests.resizes     = TRUE;
            dmP->u.create.interests.updates     = TRUE;
            break;
            }

        case DIALOG_MESSAGE_INIT:
        case DIALOG_MESSAGE_UPDATE:
            {
            DialogItem  *diP;

            diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_Sash,
                                                SASHID_VDivider, 0);
            if (!diP)
                break;

            cellExp_adjustVSashDialogItems (dmP->db, FALSE, NULL, TRUE);
            break;
            }

        case DIALOG_MESSAGE_WINDOWMOVING:
            {
            int         minSize = (10*XC);

            /* Don't process if only moving dialog box */
            if (dmP->u.windowMoving.whichCorners == CORNER_ALL ||
                dmP->u.windowMoving.whichCorners == CORNER_ALL_RESIZED)
                break;

            /* Minimum size for dialog */
            if (dmP->u.windowMoving.newWidth  < minSize)
                dmP->u.windowMoving.newWidth  = minSize;
            if (dmP->u.windowMoving.newHeight < minSize)
                dmP->u.windowMoving.newHeight = minSize;

            dmP->u.windowMoving.handled     = TRUE;

            break;
            }

        case DIALOG_MESSAGE_RESIZE:
            {
            BSIRect     oldContent;

            /* Don't process if only moving dialog box */
            if (dmP->u.resize.whichCorners == CORNER_ALL)
                break;

            mdlWindow_pointToLocal (&oldContent.origin, (MSWindow *)dmP->db,
                                    &dmP->u.resize.oldContent.origin);
            mdlWindow_pointToLocal (&oldContent.corner, (MSWindow *)dmP->db,
                                    &dmP->u.resize.oldContent.corner);

            cellExp_adjustVSashDialogItems (dmP->db, TRUE, &oldContent, 
                (dmP->u.resize.oldContent.origin.x != dmP->u.resize.newContent.origin.x ||
                 dmP->u.resize.oldContent.origin.y != dmP->u.resize.newContent.origin.y));
            break;
            }

        default:
            dmP->msgUnderstood = FALSE;
            break;
        }
    }


/*----------------------------------------------------------------------+*//**
* The callback function for the cell listmodel                                                                        *
* @bsimethod CellListlingHook                                           *                                               *
* @param        dimP     The structure passed to the dialog item handler            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void CellListlingHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood=TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_CREATE:
            {
            ListColumn  *pColumn;
            ListModel   *pListModel = mdlListModel_create (CELLLISTMODELCOLS);

            if  (NULL != (pColumn = mdlListModel_getColumnAtIndex (pListModel, 0)))
                mdlListColumn_setInfoFieldCount (pColumn, 1);

            int     numRows = BuildListModel (pListModel,dimP->db);
            mdlDialog_listBoxSetListModelP (dimP->dialogItemP->rawItemP,pListModel,CELLLISTMODELCOLS);
            }
            break;
        case DITEM_MESSAGE_DESTROY:
            {
            mdlListModel_destroy (mdlDialog_listBoxGetListModelP (dimP->dialogItemP->rawItemP),TRUE);
            }
            break;
        
        case DITEM_MESSAGE_BUTTON:
            {
            if ((BUTTONTRANS_UP==dimP->u.button.buttonTrans)&&(dimP->u.button.upNumber==1))
                {
                int         row,col;
                mdlDialog_listBoxLastCellClicked (&row,&col,dimP->dialogItemP->rawItemP);
                if (row >=0)
                    {
                    BoolInt     found;
                    ListRow    *pRow;
                    MSWChar     *pWString=NULL;
                    ValueDescr  val;
                    mdlDialog_listBoxGetNextSelection (&found, &row, &col, dimP->dialogItemP->rawItemP);
                    pRow = mdlListModel_getRowAtIndex (mdlDialog_listBoxGetListModelP (dimP->dialogItemP->rawItemP) ,row);
                    ListCell    *pCell = mdlListRow_getCellAtIndex (pRow,0);
                    mdlListCell_getValue (pCell, &val);
                    mdlListCell_getDisplayTextW (pCell,&pWString);
                    if (pWString)
                        {
                        ListCell *pCell;
                        mdlParams_setActive ((void*)pWString,ACTIVEPARAM_WCHAR_CELLNAME);
                        if (NULL != (pCell = mdlListModel_getCellAtIndexes (mdlDialog_listBoxGetListModelP (dimP->dialogItemP->rawItemP), row, 0)))
                            {
                            long        infoField;

                    /* Get the infoField from the cell, which is the GuiTreeNode ptr */
                            if (SUCCESS == mdlListCell_getInfoField (pCell, 0, &infoField))
                                {
                                GuiTreeNode *pNode = (GuiTreeNode *) infoField;

                        /* Expand or collapse the node */
                                if  (mdlTreeNode_isExpanded (pNode))
                                    {
                                    mdlTreeNode_collapse (pNode, FALSE);
                                    }
                                else if  (mdlTreeNode_isParent (pNode))
                                    {
                                    mdlTreeNode_makeDisplayable (pNode);
                                    mdlTreeNode_expand (pNode);
                                    }
                                else 
                                    {
                                    mdlTreeNode_makeDisplayable (pNode);
                                    }
                        
                                DialogItem *treeDiP = mdlDialog_itemGetByTypeAndId (dimP->db,RTYPE_Tree,TREEID_CellList,0);

                        /* Update the Tree */
                                mdlDialog_treeModelUpdated (treeDiP->rawItemP, TRUE);

                                row = mdlTreeModel_getDisplayRowIndex (mdlDialog_treeGetTreeModelP (treeDiP->rawItemP) , pNode);
                                if (row >= 0)
                                    mdlDialog_treeSelectCells (treeDiP->rawItemP, row, row, 0, 0, TRUE, TRUE);
                                }
                            }
                        }
                    }
                }
            else if((BUTTONTRANS_UP==dimP->u.button.buttonTrans)&&(dimP->u.button.upNumber==2))
                {
                int         row,col;
                mdlDialog_listBoxLastCellClicked (&row,&col,dimP->dialogItemP->rawItemP);
                if (row > 0 )
                    mdlDialog_cmdNumberQueue (FALSE,CMD_PLACE_CELL_ICON,"",TRUE);
                }
            
            }
            break;

        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    return ;
    }


/*----------------------------------------------------------------------+*//**
* The icon view of the cells                                                                        *
* @bsimethod genericIconViewHook                                        *                                               *
* @param        dimP     The structure that is passed to the dialog item            *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void genericIconViewHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood=TRUE;
    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_DRAW:
            {
            mdlDialog_rectFill(dimP->db, &dimP->dialogItemP->rect, 
                                BLACK_INDEX);
            drawCellIcon(dimP->db,dimP->dialogItemP );
            }
            break;
        case DITEM_MESSAGE_BUTTON:
            {
            /*mdlModelRef_
            mdlParams_setActive ();*/
            }
            break;
        default:
            dimP->msgUnderstood = FALSE;
            break;
        }

    return ;
    }

/*----------------------------------------------------------------------+*//**
* !!!Describe Function Completely!!!                                                                        *
* @bsimethod iconContainerHook                                          *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  void iconContainerHook
(
DialogItemMessage       *dimP
)
    {
    return ;
    }

/*----------------------------------------------------------------------+*//**
* The command entry point for the dialog.                                                                        *
* @bsimethod openDialogFunction                                         *                                               *
* @param        unparsed     unused unparsed argument to the command            *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT void openDialogFunction
(
char        *unparsed
)
    {
    mdlDialog_open (NULL, DIALOGID_CellExp);

    return ;
    }

/*----------------------------------------------------------------------+*//**
* Command function to set the type of view in the container.                                                                 *
* @bsimethod setListView                                                *                                               *
* @param        unparsed     on to show list of the cells off to show preview graphic            *
*                                                                       *
* Author:   MarkAnderson                                04/02           *
*                                                                       *
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  void setListView
(
char        *unparsed
)
    {
    BoolInt     bChanged= FALSE;
    DialogBox   *dbP = mdlDialog_find (DIALOGID_CellExp, NULL);

    DialogItem  *pTree = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_Tree, TREEID_CellList ,0);

    if ((*unparsed)&& (0== strcmpi (unparsed,"on")))
        {
        gDetailList=TRUE;
        if (pTree)
            {
            mdlDialog_treeSetContainer (pTree->rawItemP,CONTAINERID_CellListing,mdlSystem_getCurrMdlDesc(),FALSE);
            populateCellListing (NULL,dbP);
            bChanged = TRUE;
            }
        gCurrentContainer = CONTAINERID_CellListing;

        }
    else
        {
        gDetailList = FALSE;
        if (pTree)
            {
            mdlDialog_treeSetContainer (pTree->rawItemP,CONTAINERID_CellIconView,mdlSystem_getCurrMdlDesc(),FALSE);
            bChanged = TRUE;
            }
        gCurrentContainer = CONTAINERID_CellIconView;

        }

        if  (bChanged)
            {
            /* Redraw 3 main items in dialog (Tree, Sash, CTPanel) */
            //MdlWin32InvalidateWindow (dbP);

            cellExp_adjustVSashDialogItems (dbP, TRUE, NULL, TRUE);
            }
        else
            {
            DialogItem *ctPDiP;
                        /* Redraw the ContainerPanel */

            ctPDiP = mdlDialog_itemGetByTypeAndId (dbP,RTYPE_ContainerPanel, CTPANELID_Detail, 0);
            if (ctPDiP)
                mdlDialog_itemDrawEx (dbP, ctPDiP->itemIndex, TRUE);
                
            }

    return ;
    }
//because the dialog hook call back function now has a typedef
#if defined (MSVERSION) && (MSVERSION >= 0x890)
#define Handler PFDialogHook
#else
typedef void (*Handler)();
#endif

Private DialogHookInfo uHooks[] = 
    {
    {HOOKID_Dialog,(Handler)dialogHook  },
    {HOOKITEMID_Tree, (Handler)treeHook         },
    {HOOKITEMID_Sash_VDivider, (Handler)dividerHook     },
    {HOOKITEMID_CTPanel, (Handler)ctpanelHook   },
    {HOOKITEMID_CellName, (Handler)textItemHook         },
    {HOOKITEMID_CellDescription, (Handler)textItemDescriptonHook        },
    {HOOKITEMID_GenericCellPreview, (Handler)genericPreviewItemHook     },
    {HOOKITEMID_CellListing, (Handler)CellListlingHook  },
    {HOOKITEMID_GenericCellIconView,(Handler)genericIconViewHook        },
    {HOOKITEMID_CellIconContainer,(Handler)iconContainerHook    },
    };

/*----------------------------------------------------------------------+*//**
* The main entry point for the native code MDL application.
* This is different from the main or initialize from other application environments.                                                                        *
* @bsimethod MdlMain                                                    *                                               *
* @param        argc     The count of args passed to the application            *
* @param        argv[]     The array of args passed to this application as char arrays            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   MarkAnderson                                03/02           *
*                                                                       *
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
    RscFileHandle   rfHandle;

    mdlResource_openFile (&rfHandle,NULL,RSC_READONLY);
    
    static  MdlCommandName cmdNames[] = 
        {
        {openDialogFunction,"OPENDIALOG"  },
        0,
        };

    mdlSystem_registerCommandNames (cmdNames);

    static MdlCommandNumber cmdNumbers[] =
        {
        {openDialogFunction,CMD_CELLEXP_OPEN },
        {setListView,CMD_CELLEXP_LISTTOGGLE },
        0,
        };
    gCurrentContainer = CONTAINERID_CellListing;

    mdlSystem_registerCommandNumbers (cmdNumbers);
    
    mdlParse_loadCommandTable (NULL);

    mdlDialog_hookPublish (sizeof uHooks/sizeof DialogHookInfo,uHooks);

    return SUCCESS;
    }

