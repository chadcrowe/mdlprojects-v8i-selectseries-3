/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cellexp/CellExp.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#include <rscdefs.h>


#define     DLLAPPID 1

/* associate app with dll */
DllMdlApp DLLAPPID = 
    {
    "CellExp", "CellExp"
    }

