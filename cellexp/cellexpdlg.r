/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cellexp/CellExpDlg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogids.h>

#include    "CellExp.h"
#include    "CellExpTxt.h"

#define     XSIZE 80*XC
#define     YSIZE 25*YC
#define     SX  15*XC

DialogBoxRsc        DIALOGID_CellExp =
    {
    DIALOGATTR_DEFAULT|DIALOGATTR_GROWABLE,XSIZE,YSIZE,NOHELP,MHELP,
    HOOKID_Dialog,NOPARENTID,TITLETEXT,
    {	
    {{0,D_ROW(2),0,0},	Tree,	    TREEID_CellList,     ON,  0, "", ""},
    {{SX,0,0,0},Sash,	    SASHID_VDivider,	    ON,  0, "", ""},
    {{0,0,0,0},	ContainerPanel, CTPANELID_Detail,   ON,  0, "", ""},
    }
    };

#undef  YSIZE
#undef  XSIZE
#undef  SX

DItem_TreeRsc   TREEID_CellList =
    {
    NOHELP,MHELP,HOOKITEMID_Tree, 0,TREEATTR_NOSHOWROOT|TREEATTR_NOROOTHANDLE|
    TREEATTR_NOKEYSEARCH|TREEATTR_HORIZSCROLLBAR|TREEATTR_DOUBLECLICKEXPANDS,
    20, CTPANELID_Detail,TREETITLETEXT,
	{
	{0, 25*XC, 256, 0, TXT_Tree},
	}
    };

DItem_SashRsc      SASHID_VDivider = 
    {
    NOHELP, MHELP, 
    HOOKITEMID_Sash_VDivider, 0, 
    10*XC, 10*XC, 
    SASHATTR_VERTICAL | SASHATTR_ALLGRAB | SASHATTR_SOLIDTRACK | 
    SASHATTR_WIDE | SASHATTR_SAVEPOSITION | SASHATTR_NOENDS
    };

DItem_ContainerPanelRsc    CTPANELID_Detail = 
    {
    NOHELP, MHELP, 
    HOOKITEMID_CTPanel, 0, 
    CTPANELATTR_BOUNDINGBOX,
    CONTAINERID_CellIconView, 0,
    ""
    };

DItem_ContainerRsc         CONTAINER_TextInfo = 
    {
    NOCMD, LCMD, NOHELP, MHELP, NOHOOK, NOARG, 0,
    DILISTID_SimpleInfo    
    };
#define     CBX 20*XC

DialogItemListRsc DILISTID_SimpleInfo =
    {
    {
    {{CBX-1.3*XC,D_ROW(2),0,0}, Text, TEXTID_CellName, ON, 0, "", ""},
    {{CBX,D_ROW(4),0,0}, Text, TEXTID_CellDescription, ON, 0, "", ""},
    {{CBX-10*XC,D_ROW(6),30*XC,30*XC}, Generic, GENERICID_CellPreview, ON, 0, "", ""},

    }
    };    

DItem_TextRsc   TEXTID_CellName = 
    {
    NOCMD,MCMD,NOSYNONYM,NOHELP,MHELP,HOOKITEMID_CellName,NOARG,
    25,"%S","%S","","",NOMASK,TEXT_READONLY,TEXTITEMLABEL_CELLNAME,""
    };

DItem_TextRsc   TEXTID_CellDescription = 
    {
    NOCMD,MCMD,NOSYNONYM,NOHELP,MHELP,HOOKITEMID_CellDescription,NOARG,
    25,"%S","%S","","",NOMASK,TEXT_READONLY,TEXTITEMLABEL_CELLDESC,""
    };
DItem_GenericRsc    GENERICID_CellPreview = 
    {
    NOHELP,MHELP,HOOKITEMID_GenericCellPreview,NOARG
    };


DItem_ContainerRsc      CONTAINERID_CellListing =
    {
    NOCMD,MCMD,NOHELP,MHELP,NOHOOK,NOARG,0,
    DILISTID_CellListing
    };

DialogItemListRsc   DILISTID_CellListing =
    {
    {
    {{1*XC,D_ROW(2),46*XC,22*YC}, ListBox, LISTBOXID_CellListing, ON, 0, "", ""},
    }
    };

DItem_ListBoxRsc    LISTBOXID_CellListing =
    {
    NOHELP,MHELP,HOOKITEMID_CellListing,NOARG,LISTATTR_SORTCOLUMNS|LISTATTR_HORIZSCROLLBAR|LISTATTR_GRID|LISTATTR_RESIZABLECOLUMNS|LISTATTR_COLHEADINGBORDERS,
    15,0,"",
    	{
        {12*XC,512,ALIGN_LEFT,TXT_CellName},
        {20*XC,512,ALIGN_LEFT,TXT_CellDescription},
        {8*XC,512,ALIGN_LEFT,TXT_CellType},
        {8*XC,512,ALIGN_LEFT,TXT_CellISThreeD},
        {8*XC,512,ALIGN_LEFT,TXT_CellILocked},
    	}
    };

DItem_ContainerRsc      CONTAINERID_CellIconView =
    {
    NOCMD,MCMD,NOHELP,MHELP,HOOKITEMID_CellIconContainer,NOARG,0,DILISTID_CellIconView
    };

DialogItemListRsc   DILISTID_CellIconView = 
    {
    {
    //{{1*XC,D_ROW(2),23,23 },Generic,GENERICID_CellIcon,ON,0,"",""},
    }
    };
DItem_GenericRsc    GENERICID_CellIcon = 
    {
    NOHELP,MHELP,HOOKITEMID_GenericCellIconView,NOARG
    };