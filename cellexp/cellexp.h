/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/cellexp/CellExp.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/

#define     DIALOGID_CellExp       1

#define     TREEID_CellList		1
#define     LISTBOXID_CellListing       2

#define     CELLLISTMODELCOLS           7

#define     CELLNAMECOL                 0
#define     CELLDESCCOL                 1
#define     CELLTYPECOL                 2
#define     CELLTHREEDCOL               3
#define     CELLLOCKEDCOL               4
#define     CELLHIDDENCOL               5
#define     CELLMODELIDCOL              6

#define     SASHID_VDivider		1

#define     CTPANELID_Detail		1

#define     CONTAINER_TextInfo		1
#define     CONTAINERID_CellListing     2
#define     CONTAINERID_CellIconView    3

#define     DILISTID_SimpleInfo		1
#define     DILISTID_CellListing        2
#define     DILISTID_CellIconView       3

#define     TEXTID_CellName		1
#define     TEXTID_CellDescription      2

#define     HOOKITEMID_Tree		1
#define     HOOKITEMID_Sash_VDivider	2
#define     HOOKITEMID_CTPanel		3
#define     HOOKITEMID_CellName		4
#define     HOOKID_Dialog               5
#define     HOOKITEMID_GenericCellPreview   6
#define     HOOKITEMID_CellDescription  7
#define     HOOKITEMID_CellListing      8
#define     HOOKITEMID_GenericCellIconView  9
#define     HOOKITEMID_CellIconContainer    10

#define     GENERICID_CellPreview       1
#define     GENERICID_CellIcon          2

typedef struct  celldata 
    {
    char        LibraryName[512];
    MSWChar     name[512];
    MSWChar     description[512];
    int         cellType;
    BoolInt     isThreeD;
    BoolInt     isLocked;
    BoolInt     isHidden;
    }CellData;