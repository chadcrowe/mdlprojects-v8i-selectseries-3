/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/smgrapi/english/sapimsgs.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/smgrapi/english/sapimsgs.r_v  $
|   $Workfile:   sapimsgs.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:55 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Settings Manager API Example Application Strings	    	|
|									|
+----------------------------------------------------------------------*/
#include    <rscdefs.h>
#include    <cmdclass.h>

#include    "smgrapi.h"

MessageList MSGLISTID_Status =
{
    {
    { STATUSID_CreatingFile, 	"Creating file - %s" },
    { STATUSID_SettingFileName,	"Setting file name" },
    { STATUSID_MLineAdded,    	"Multiline Style added" },
    { STATUSID_SettingsTitle, 	"Settings Data" },
    { STATUSID_UpdatingStyle, 	"Updating multiline style" },
    { STATUSID_ReadingStyle,  	"Reading multiline style" },
    { STATUSID_LoadingStyle,  	"Loading Multiline Style" },
    }
};

MessageList MSGLISTID_Error =
{
    {
    { ERRORID_SettingsEnvVar,	"MS_SETTINGS not defined" },
    { ERRORID_CreateRsc,     	"Unable to create resource file" },
    { ERRORID_OpenRsc,      	"Unable to open resource file" },
    { ERRORID_OpenSettings,   	"Unable to open settings file" },
    { ERRORID_NoName,       	"No style name provided" },
    { ERRORID_CreateMLine,    	"Unable to create multiline style" },
    { ERRORID_AddStyle,       	"Unable to add multiline style" },
    { ERRORID_UpdateStyle,    	"Unable to update multiline style" },
    { ERRORID_ReadStyle,      	"Unable to read multiline style" },
    { ERRORID_LoadStyle,      	"Unable to load multiline style" },
    }
};
