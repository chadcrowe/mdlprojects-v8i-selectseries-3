/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/smgrapi/smgrapi.mc,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   smgrapi.mc  $
|   $Revision: 1.6.52.1 $
|   	$Date: 2013/07/01 20:40:54 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Settings Manager API Example Application		    	|
|									|
|   	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -   	|
|									|
|   This example application demonstrates the use of the Settings   	|
|   Manager API capabilities for reading, writing, creating, and    	|
|   updatings settings files.					    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <msdefs.h>
#include <rscdefs.h>
#include <cmdlist.h>
#include <stdarg.h>
#include <string.h>	    
#include <settings.h>	    /* new settings info */

#include <setmlib.fdf>	    /* settings manager function prototypes */
#include <msoutput.fdf>
#include <mssystem.fdf>
#include <msparse.fdf>
#include <msrsrc.fdf>
#include <msfile.fdf>
#include <msmisc.fdf>
#include <msinput.fdf>

#include "smgrapi.h"
#include "sapicmd.h"

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
Private StyleUnion gStyle;

/*----------------------------------------------------------------------+
|									|
|   Local Macros							|
|									|
+----------------------------------------------------------------------*/

/*======================================================================+
|									|
|   Private Utility Routines						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		smgrapi_printError					|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	smgrapi_printString
(
long	msgNumber,	    /* => Message number of string to output	*/
int 	where,		    /* => Where to write message	    	*/
...
)
    {
    va_list	ap;
    long    	listNumber;

    if (where == MSG_ERROR)
    	mdlUtil_beep (1);

    if (where == MSG_ERROR)
    	listNumber = MSGLISTID_Error;
    else
    	listNumber = MSGLISTID_Status;

    va_start (ap, where);

    mdlOutput_rscvPrintf (where, NULL, listNumber, msgNumber, ap);

    va_end (ap);
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_synchMlineDialog     				|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	smgrapi_synchMlineDialog
(
void
)
    {
    /*	Tell multiline dialog box to synchronize settings data	*/
    mdlInput_sendCommand (CMD_MDL_KEYIN, "MLINESET DIALOG MULTILINE SYNCHRONIZE",
		    	  0, NULL, 0);
    }

/*----------------------------------------------------------------------+
|									|
| name	    	smgrapi_synchDimensionDialogs			    	|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void 	smgrapi_synchDimensionDialogs
(
void
)
    {
    /*	Tell dimensions dialog box to synchronize settings data */
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMATTRIB SYNCHRONIZE",
			  0, NULL, 0);
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMGEOM SYNCHRONIZE",
		    	  0, NULL, 0);
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMPLACE SYNCHRONIZE",
		    	  0, NULL, 0);
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMSYMBOL SYNCHRONIZE",
		    	  0, NULL, 0);
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMTEMPLATE SYNCHRONIZE",
		    	  0, NULL, 0);
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMTERMINATORS SYNCHRONIZE",
		    	  0, NULL, 0);
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMTEXT SYNCHRONIZE",
		    	  0, NULL, 0);
    mdlInput_sendCommand (CMD_MDL_KEYIN, "DIMSET DIALOG DIMTOLERANCE SYNCHRONIZE",
		    	  0, NULL, 0);
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_synchEditDialogs  				|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	smgrapi_synchEditDialogs
(
void
)
    {
    /*	Tell settings manager edit dialog box to synchronize settings data */
    mdlInput_sendCommand (CMD_MDL_KEYIN, "SETMGR SETMGR SYNCHRONIZE EDIT",
		    	  0, NULL, 0);
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_synchSelectDialogs				|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	smgrapi_synchSelectDialogs
(
void
)
    {
    /*	Tell settings manager select dialog box to synchronize settings data */
    mdlInput_sendCommand (CMD_MDL_KEYIN, "SETMGR SETMGR SYNCHRONIZE SELECT",
		    	  0, NULL, 0);
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_getFileName					|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private int	smgrapi_getFileName
(
char 	*path,		/* <= MAXFILELENGTH (full path & filename) */
char 	*file	    	/* <= MAXFILELENGTH (name + ext) */
)
    {
    char    lPath[MAXFILELENGTH];
    char    lName[MAXNAMELENGTH];
    char    lExtension[MAXEXTENSIONLENGTH];
    char   *pathP = NULL;

    /* 	Get name of settings file from configutation variable */
    if (mdlSystem_getCfgVar (lPath, "MS_SETTINGS", MAXFILELENGTH) != SUCCESS)
    	{
	smgrapi_printString (ERRORID_SettingsEnvVar, MSG_ERROR);
	return ERROR;
	}

    /*	Expand configuration variable and get complete path to settings */
    /*	file							    	*/
    pathP = mdlSystem_expandCfgVar (lPath);
    strcpy (lPath, pathP);
    mdlSystem_freeCfgVarBuffer(pathP);

    /*	Parse path into component pieces    */
    mdlFile_parseName (lPath, NULL, NULL, lName, lExtension);

    /*	Now return requested information    */
    if (path)
    	strcpy (path, lPath);

    if (file)
    	sprintf (file, "%s.%s", lName, lExtension);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_openSettingsResource				|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private int  	smgrapi_openSettingsResource
(
RscFileHandle	*rscFh,		/* <=> resource file handle	*/
BoolInt		readWrite,	/*  => TRUE if read/write	*/
char		*rscFile,	/*  => default file name	*/
char		*hdrString,	/*  => header info for new file */
long		*hdrValue	/*  => header info for new file */
)
    {
    char    fileName[MAXFILELENGTH], dir[MAXDIRLENGTH], envvar[64];
    int	    status = ERROR;

    dir[0] = '\0';

    /*	Try to locate the settings file in the normal locations */
    if (mdlSystem_getenv (dir, "MS_SETTINGSDIR", MAXDIRLENGTH) == SUCCESS)
	{
	strcpy (envvar, "MS_SETTINGSDIR");
	if (status = mdlFile_find (fileName, rscFile, envvar, ".stg"))
	    {	/* look in MS_DATA directory */
	    strcpy (envvar, "MS_DATA");
	    if (status = mdlFile_find (fileName, rscFile, envvar, ".stg"))
	    	/* if we still cant find it look in current directory */
	    	status = mdlFile_find (fileName, rscFile, NULL, ".stg");
	    }
	}

    /*	If we found a settings file, open it up for processing	*/
    if (status == SUCCESS)
	{
	if ((mdlResource_openFile (rscFh, fileName, readWrite) != SUCCESS) &&
	    (readWrite == RSC_READWRITE))
	    status = mdlResource_openFile (rscFh, fileName, RSC_READONLY);
	}
    else
	{
	/*  No settings file found - create one for use */
	if (readWrite == RSC_READWRITE)
	    {
	    if (dir[0])		    /* If found environment var earlier */
	    	mdlFile_create (fileName, rscFile, "MS_SETTINGSDIR", "stg");
	    else
	    	mdlFile_create (fileName, rscFile, "MS_DATA", "stg");

	    smgrapi_printString (STATUSID_CreatingFile, MSG_STATUS, rscFile);

	    /* 	Create new resource file */
	    if ((status = mdlResource_createFile (fileName, hdrString,
					    	  *hdrValue)) != SUCCESS)
		{
		smgrapi_printString (ERRORID_CreateRsc, MSG_ERROR);
		return ERROR;
		}

	    /* 	Open the new settings file */
	    if ((status = mdlResource_openFile (rscFh, fileName,
					    	readWrite)) != SUCCESS)
		{
		smgrapi_printString (ERRORID_OpenRsc, MSG_ERROR);
		return ERROR;
		}
	    }
	}

    return  status;
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_openSettingsFile				|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private int 	smgrapi_openSettingsFile
(
RscFileHandle  *rscFhP,
ULong	    	fileAttrMask
)
    {
    char    tmpBuf[MAXFILELENGTH];
    char    file[MAXFILELENGTH];
    long    value;

    /*	Get name of settings manager settings resource file */
    if (smgrapi_getFileName (tmpBuf, file) != SUCCESS)
	{
	/*  None exists - make our own	*/
    	smgrapi_printString (STATUSID_SettingFileName, MSG_STATUS);
	strcpy (file, "settings.stg");
	}

    value = 500L;	    /* release version */
    mdlResource_loadFromStringList (tmpBuf, NULL, MSGLISTID_Status,
				    STATUSID_SettingsTitle);
    if (smgrapi_openSettingsResource (rscFhP, fileAttrMask, file,
				      tmpBuf, &value) != SUCCESS)
	{
    	smgrapi_printString (ERRORID_OpenSettings, MSG_ERROR);
    	return ERROR;
    	}

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_addMultilineStyle				|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	smgrapi_addMultilineStyle
(
char   *unparsed		    /* => STYLE NAME */
)   cmdNumber	CMD_SMGRAPI_ADD
    {
    RscFileHandle   rscFh;
    StyleUnion	    style;
    ULong	    rscID;
    char	    alias[16];

    /*	Get name/alias from command parameters unparsed */
    if (!(unparsed && unparsed[0]))
	{
	smgrapi_printString (ERRORID_NoName, MSG_ERROR);
    	return;
	}

    strcpy (alias, unparsed);

    /* 	Open resource file  */
    if (smgrapi_openSettingsFile (&rscFh, RSC_READWRITE) != SUCCESS)
	{
    	smgrapi_printString (ERRORID_OpenSettings, MSG_ERROR);
    	return;
    	}

    /* 	Create style from tcb	*/
    if (mdlSetmgr_createStyle (&style, RTYPE_Multiline) != SUCCESS)
	{
    	smgrapi_printString (ERRORID_CreateMLine, MSG_ERROR);
    	mdlResource_closeFile (rscFh);
    	return;
    	}

    /*	Determine if new or updated style */
    if (mdlResource_getRscIdByAlias (&rscID, rscFh, RTYPE_Multiline, alias))
	{
	/*  Add new style   */
    	if (mdlSetmgr_addStyle (NULL, &style, rscFh, RTYPE_Multiline,
				alias, "TESTLIB") == SUCCESS)
	    {
	    smgrapi_printString (STATUSID_MLineAdded, MSG_STATUS);
	    /*	Synchronize appropriate dialog/settings boxes	*/
	    smgrapi_synchEditDialogs ();
	    smgrapi_synchSelectDialogs ();
	    }
    	else
	    smgrapi_printString (ERRORID_AddStyle, MSG_ERROR);
	}
    else
    	{
    	/*  Update existing style   */
    	if (mdlSetmgr_updateStyle (NULL, &style, rscFh, RTYPE_Multiline,
				   alias, "TESTLIB") == SUCCESS)
	    smgrapi_printString (STATUSID_UpdatingStyle, MSG_STATUS);
    	else
	    smgrapi_printString (ERRORID_UpdateStyle, MSG_ERROR);
    	}

    mdlResource_closeFile (rscFh);
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_readMultilineStyle				|
|								    	|
|	    	read multiline style from file into global variable 	|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	smgrapi_readMultilineStyle
(
char   *unparsed
)   cmdNumber	CMD_SMGRAPI_READ
    {
    RscFileHandle   rscFh;
    char	    alias[16];

    /* 	Get name/alias from command keyin   */
    if (!(unparsed && unparsed[0]))
	{
	smgrapi_printString (ERRORID_NoName, MSG_ERROR);
    	return;
	}

    strcpy (alias, unparsed);

    /* 	Open resource file  */
    if (smgrapi_openSettingsFile (&rscFh, RSC_READWRITE) != SUCCESS)
	{
    	smgrapi_printString (ERRORID_OpenSettings, MSG_ERROR);
    	return;
    	}

    /*	Read in the desired multiline style */
    if (mdlSetmgr_readStyle (NULL, &gStyle, rscFh, alias,
			     RTYPE_Multiline) == SUCCESS)
	    smgrapi_printString (STATUSID_ReadingStyle, MSG_STATUS);
    	else
	    smgrapi_printString (ERRORID_ReadStyle, MSG_ERROR);

    mdlResource_closeFile (rscFh);
    }

/*----------------------------------------------------------------------+
|									|
| name		smgrapi_loadMultilineStyle				|
|								    	|
|	    	load tcb multiline style from global variable	    	|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	smgrapi_loadMultilineStyle
(
char   *unparsed
)   cmdNumber 	CMD_SMGRAPI_LOAD
    {
    if (mdlSetmgr_loadStyle (&gStyle, RTYPE_Multiline) == SUCCESS)
	{
	smgrapi_printString (STATUSID_LoadingStyle, MSG_STATUS);
	smgrapi_synchMlineDialog();
	}
    else
	smgrapi_printString (ERRORID_LoadStyle, MSG_ERROR);

    return;
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI				    	02/95		|
|									|
+----------------------------------------------------------------------*/
Private void	main
(
int 	argc,
char   *argv[]
)
    {
    RscFileHandle   rfHandle;

    /* 	Initialize the environment  */
    mdlResource_openFile (&rfHandle, NULL, FALSE);
    mdlParse_loadCommandTable (NULL);
    }



