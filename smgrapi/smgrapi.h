/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/smgrapi/smgrapi.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/smgrapi/smgrapi.h_v  $
|   $Workfile:   smgrapi.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:54 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Settings Manager API Example Application Definitions	    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__smgrapiH__)
#define __smgrapiH__

#define	    MSGLISTID_Status	    	1
#define	    MSGLISTID_Error	    	2

#define     STATUSID_CreatingFile   	0
#define     STATUSID_SettingFileName	1
#define     STATUSID_MLineAdded		2
#define     STATUSID_SettingsTitle	3
#define     STATUSID_UpdatingStyle	4
#define     STATUSID_ReadingStyle	5
#define     STATUSID_LoadingStyle	6

#define     ERRORID_SettingsEnvVar  	0
#define     ERRORID_CreateRsc       	1
#define     ERRORID_OpenRsc	    	2
#define     ERRORID_OpenSettings    	3
#define     ERRORID_NoName	    	4
#define     ERRORID_CreateMLine     	5
#define     ERRORID_AddStyle	    	6
#define     ERRORID_UpdateStyle     	7
#define     ERRORID_ReadStyle       	8
#define     ERRORID_LoadStyle       	9

#endif /* !defined	(__smgrapiH__) */
