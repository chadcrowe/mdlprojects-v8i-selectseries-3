/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/smgrapi/sapicmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/smgrapi/sapicmd.r_v  $			
|   $Workfile:   sapicmd.r  $						
|   $Revision: 1.2.76.1 $						    	
|   	$Date: 2013/07/01 20:40:54 $			    	
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Settings Manager API Example Application Command Table      	|
|									|
+----------------------------------------------------------------------*/
#include    <cmdclass.h>
#include    <rscdefs.h>

#define	    CT_NONE 	0
#define	    CT_MAIN 	1
#define	    CT_SMGRAPI	2

Table 	CT_MAIN =
{
    {	1,  CT_SMGRAPI,	    INPUT,   	REQ,	    "SMGRAPI" },
};

Table	CT_SMGRAPI =
{
    {	1,  CT_NONE,	    INHERIT,	NONE,	    "LOAD" },
    {	2,  CT_NONE,	    INHERIT,	NONE,	    "READ" },
    {	3,  CT_NONE,	    INHERIT,	NONE,	    "ADD" },
};
