/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMR/english/LevHideAMRmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMR/english/LevHideAMRmsg.r_v  $
|   $Workfile:   LevHideAMRmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevHideAMR application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "LevHideAMR.h"	/* LevHideAMR dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_LevHideAMRErrors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_LevHideAMRDialog,   "Unable to open LevHideAMR dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
