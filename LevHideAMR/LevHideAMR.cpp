
#define PI 3.1415926535897932384626433
#include    <mdl.h>
#include    <toolsubs.h>
#include    <basetype.h>
#include    <malloc.h>
#include    <msrmgr.h>
#include    <mstypes.h>
#include    <string.h>
#include    <msparse.fdf>
#include    <msstate.fdf>
#include    <mssystem.fdf>
#include    <dlmsys.fdf>
#include    <mscnv.fdf>
#include    <msfile.fdf>
#include    <msdialog.fdf>
#include    <dlogids.h>
#include    <mswindow.fdf>
#include    <msdgnmodelref.fdf>
#include    <msdgnlib.fdf>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mscell.fdf>
#include    <msmline.fdf>
#include    <mstextstyle.fdf>
#include    <listmodel.fdf>
#include    <changetrack.fdf>
#include    <mselems.h>
#include    <scanner.h>
#include    <userfnc.h>
#include    <cmdlist.h>
#include    <string.h>
#include    <toolsubs.h>
#include    <dlogman.fdf>
#include    <mssystem.fdf>
#include    <mslinkge.fdf>
#include    <msscan.fdf>
#include    <msoutput.fdf>
#include    <msparse.fdf>
#include    <mselemen.fdf>
#include    <msrsrc.fdf>
#include    <mslocate.fdf>
#include    <msstate.fdf>
#include    <msscancrit.fdf>

#include <mstypes.h>
#include <msscancrit.fdf>
#include <elementref.h>

#include <msdgnobj.fdf>

#include    <msdgncache.h>
#include    <ditemlib.fdf>
//#include	<FontManager.h>
#include    <mselementtemplate.fdf>
#include    <msdimstyle.fdf>
#include    <msdim.fdf>
#include    <namedexpr.fdf>
#if !defined (DIM)
#define DIM(a) ((sizeof(a)/sizeof((a)[0])))
#endif


USING_NAMESPACE_BENTLEY_USTN
USING_NAMESPACE_BENTLEY_USTN_ELEMENT
USING_NAMESPACE_TEXT
#include <iostream>
#define LINEAR_TEMPLATE_NAME      L"CreatedGroup\\Created Linear Template"
#define SHAPE_TEMPLATE_NAME       L"CreatedGroup\\Created Shape Template"
#define HATCH_TEMPLATE_NAME       L"CreatedGroup\\Created Hatch Template"
#define AREAPATTERN_TEMPLATE_NAME L"CreatedGroup\\Created AreaPattern Template"
#define CELL_TEMPLATE_NAME        L"CreatedGroup\\Created Cell Template"
#define TEXT_TEMPLATE_NAME        L"CreatedGroup\\Created Text Template"
#define MLINE_TEMPLATE_NAME       L"CreatedGroup\\Created MLine Template"
#define DIMENSION_TEMPLATE_NAME   L"CreatedGroup\\Created Dimension Template"
#define HEADER_TEMPLATE_NAME      L"CreatedGroup\\Header"
#define COMPONENT1_TEMPLATE_NAME  L"CreatedGroup\\Component1"
#define COMPONENT2_TEMPLATE_NAME  L"CreatedGroup\\Component2"
#include <iostream>
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
int ScanForLinearElements(DgnModelRefP modelRef);
int ElementRefScanCallback(ElementRef, void *callbackArg, ScanCriteriaP);
#include <sstream>
#include <msselect.fdf>
#include <msmisc.fdf>
#include <ElemHandle.h>
#include <ElementAgenda.h>
#include <msview.fdf>
#include <msreffil.fdf>
#include <mselmdsc.fdf>
#include <leveltable.fdf>
//__declspec(dllexport) ElementID mdlElement_getID();
#include <iostream>
#include <fstream>
#include <sstream>
//#include "boost/lexical_cast.hpp"
using namespace std;
template <typename T>
string NumberToString(T Number)
{
	ostringstream ss;
	ss << Number;
	return ss.str();
}

int stringToNumber(string s)
{
	int num;
	stringstream convert(s);
	convert >> num;
	return num;
}
void appendToFile(int number)
{
	ofstream myfile;
	myfile.open("Turn off Levels.txt", ios::app);
	myfile << NumberToString(number);
	myfile.close();
}
void appendToFile(string s)
{
	ofstream myfile;
	myfile.open("Turn off Levels.txt", ios::app);
	myfile << s;
	myfile.close();
}
wchar_t* convertStringto_wchar_t(string s)
{
	const char * c = s.c_str();
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);
	return wc;
}
extern "C"  DLLEXPORT int   MdlMain
(){
	int worked;
	string line;
	ifstream infile;
	appendToFile("");
	infile.open("Turn off Levels.txt");
	if (infile.is_open()){
		while (getline(infile, line)) // Saves the line in id
		{
			MSWCharP levelName;
			levelName = convertStringto_wchar_t(line);
			DgnModelRefP modelRef = mdlModelRef_getActive();
			LevelID levid;
			mdlLevel_getIdFromName(&levid,modelRef,LEVEL_NULL_ID,levelName);
			delete [] levelName;
			worked = mdlView_setLevelDisplay(modelRef, 0, levid, FALSE);//not working, worked = 0
		}
	}
	infile.close();
	mdlDialog_cmdNumberQueue(FALSE, CMD_MDL_UNLOAD, mdlSystem_getCurrTaskID(), TRUE);
	return SUCCESS;
}
