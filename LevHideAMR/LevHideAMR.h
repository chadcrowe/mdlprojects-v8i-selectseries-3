/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMR/LevHideAMR.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMR/LevHideAMR.h_v  $
|   $Workfile:   LevHideAMR.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in LevHideAMR dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __LevHideAMRH__
#define	    __LevHideAMRH__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_LevHideAMR		1   /* dialog id for LevHideAMR Dialog */
#define DIALOGID_LevHideAMRModal	2   /* dialog id for LevHideAMR Modal Dialog */

#define OPTIONBUTTONID_LevHideAMR	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_LevHideAMR		1   /* id for "parameter 1" text item */
#define TOGGLEID_LevHideAMR		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_LevHideAMR		1   /* id for synonym resource */

#define MESSAGELISTID_LevHideAMRErrors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_LevHideAMRDialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_LevHideAMR	1   /* id for toggle item hook func */
#define HOOKDIALOGID_LevHideAMR		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct LevHideAMRglobals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } LevHideAMRGlobals;

#endif
