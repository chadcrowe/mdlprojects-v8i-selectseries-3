/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMR/LevHideAMRtyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMR/LevHideAMRtyp.mtv  $
|   $Workfile:   LevHideAMRtyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevHideAMR Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "LevHideAMR.h"

publishStructures (LevHideAMRglobals);

    
