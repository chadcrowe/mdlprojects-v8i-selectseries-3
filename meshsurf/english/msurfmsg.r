/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/meshsurf/english/msurfmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/meshsurf/english/msurfmsg.r_v  $
|   $Workfile:   msurfmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:19 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/

#include "rscdefs.h"
#include "cmdclass.h"

MessageList 0 =
{
    {
    {1, "Mesh Surface" },
    }
};

MessageList 1 =
{
    {
    }
};
