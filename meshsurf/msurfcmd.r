/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/meshsurf/msurfcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Command Table for MDL STATION.MC example program			|
|									|
|   $Logfile:   J:/mdl/examples/meshsurf/msurfcmd.r_v  $
|   $Workfile:   msurfcmd.r  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:40:06 $
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

/* Version 100 */
/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_MESHSURF        1

DllMdlApp DLLAPP_MESHSURF =
    {
    "MESHSURF", "meshsurf"          // taskid, dllName
    }



/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define   CT_NONE    	    	      	0
#define   CT_MAIN			1
#define   CT_MESH			2

/*----------------------------------------------------------------------+
|									|
|   Main Change Commands						|
|									|
+----------------------------------------------------------------------*/
Table	CT_MAIN =
    {
    	{ 1, CT_MESH,    PLACEMENT,	REQ,	"MESH" },
    };

/*----------------------------------------------------------------------+
|									|
|	Construct Subtable						|
|									|
+----------------------------------------------------------------------*/
Table	CT_MESH =
    { 
	{ 1, CT_NONE, 	INHERIT,	NONE,	"SURFACE" },
    }; 
