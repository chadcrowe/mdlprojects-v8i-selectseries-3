/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/meshsurf/meshsurf.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/meshsurf/meshsurf.mcv  $
|   $Workfile:   meshsurf.mc  $
|   $Revision: 1.2.16.1 $
|   	$Date: 2013/07/01 20:40:05 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <mdlbspln.h>
#include    <mselems.h>
#include    <rscdefs.h>

#include    "msurfcmd.h"

#include    <msparse.fdf>
#include    <msoutput.fdf>
#include    <msstate.fdf>
#include    <mslocate.fdf>
#include    <mselemen.fdf>
#include    <mswindow.fdf>
#include    <msrsrc.fdf>
#include    <mselmdsc.fdf>
#include    <msbsplin.fdf>
#include    <msview.fdf>
#include	<mssystem.fdf>

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
typedef int (*PFBsplineMesh)
(
DPoint3d *pointP,
DPoint3d *normalP,
DPoint2d *paramP,
int       nU,
int       nV,
void     *userDataP
);

typedef int (*PFBsplineTriangleStrip)
(
Dpoint3d        *pointP,
Dpoint3d        *normalP,
Dpoint2d        *paramP,
int             nPoints,
void            *userDataP
);

typedef int (*PFBsplineStroke)
(
void    *arg1P,
void    *arg2P,
void    *arg3P,
void    *arg4P,
void    *arg5P,
void    *arg6P,
void    *arg7P,
void    *arg8P
);


extern "C" int mdlBspline_nativeMeshSurface
(
PFBsplineMesh       meshFunction,               /* => mesh function */
PFBsplineTriangleStrip triangleFunction,        /* => triangle strip function */
double              tolerance,                  /* => tolerance */
int                 toleranceMode,              /* => tolerance mode */
Transform           *toleranceTransformP,       /* => tolerance transform */
Dpoint3d            *toleranceCameraP,          /* => tolerance camera position */
double              toleranceFocalLength,       /* => tolerance focal length */
Dpoint2d            *parameterScale,            /* => parameter scale */
MSBsplineSurface    *surfaceP,                  /* => surface to mesh */
BoolInt             normalsRequired,            /* => TRUE to return normals */
BoolInt             parametersRequired,         /* => TRUE to return parameters */
void                *userDataP                  /* => user data */
);

/*----------------------------------------------------------------------+
|									|
|   Local function declarations 					|
|									|
+----------------------------------------------------------------------*/

/*ff Major Public Code Section */
/*----------------------------------------------------------------------+
|									|
|   Major Public Code Section						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
| name		meshSurface_meshFunction				|
|									|
| author	BSI				    12/92		|
|									|
+----------------------------------------------------------------------*/
Private int meshSurface_meshFunction
(
Dpoint3d    *pointP,
Dpoint3d    *normalP,
Dpoint2d    *paramP,
int	    nColumns,
int	    nRows,
void	    *userDataP
)    
    {
    int		iRow, iColumn;
    Dpoint3d	shapePoints[5];
    MSElement	element;
    
    for (iRow=0; iRow<nRows-1; iRow++)
	for (iColumn=0; iColumn<nColumns-1; iColumn++)
	    {
	    shapePoints[0] = shapePoints[4] = pointP[iRow * nColumns + iColumn];
	    shapePoints[1] = pointP[iRow * nColumns + iColumn + 1];
	    shapePoints[2] = pointP[(iRow+1) * nColumns + iColumn +  1];
	    shapePoints[3] = pointP[(iRow+1) * nColumns + iColumn];
	    
	    mdlShape_create (&element, NULL, shapePoints, 5, -1);
	    mdlElement_add (&element);
	    mdlElement_display (&element, NORMALDRAW);
	    }

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		meshSurface_triangleFunction				|
|									|
| author	BSI				    12/92		|
|									|
+----------------------------------------------------------------------*/
Private int meshSurface_triangleFunction
(
Dpoint3d    *pointP,
Dpoint3d    *normalP,
Dpoint2d    *paramP,
int	    nPoints,
void	    *userDataP
)    
   {
    int		i;
    Dpoint3d	shapePoints[4];
    MSElement	element;
	
    for (i=2; i<nPoints; i++)
	{
	shapePoints[0] = shapePoints[3] = pointP[i-2];
	shapePoints[1] = pointP[i-1];
	shapePoints[2] = pointP[i];
    
	mdlShape_create (&element, NULL, shapePoints, 4, -1);
	mdlElement_add (&element);
	mdlElement_display (&element, NORMALDRAW);
	}
    return SUCCESS;
    }
    
/*----------------------------------------------------------------------+
|									|
| name		meshSurface_accept					|
|									|
| author	BSI					6/90		|
|									|
+----------------------------------------------------------------------*/
Private void meshSurface_accept
(
Dpoint3d    *pointP,
int	    view
)				    
    {
    DgnModelRefP	currFile;
    double		viewScale, uorTolerance;
    Dpoint3d		delta;
    MSWindow		*windowP;
    BSIRect		contentRect;
    ULong		filePos;
    MSElementDescr	*edP;
    MSBsplineSurface	surface;
	
    /* Set the tolerance to be the size of 5 pixels on the screen */
    windowP = mdlWindow_viewWindowGet (view);
    mdlWindow_contentRectGetGlobal (&contentRect, windowP);
    mdlView_getParameters (NULL, NULL, &delta, NULL, NULL, view);
    
    /* Scale from pixels to uors */
    viewScale = delta.x / (double) (contentRect.corner.x - contentRect.origin.x);
    uorTolerance = 5.0 * viewScale;

    
    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFile);
    if (mdlElmdscr_read (&edP, filePos, currFile, FALSE, NULL))
	{
	if (! mdlBspline_convertToSurface (&surface, edP))
	    {
	    mdlBspline_nativeMeshSurface (meshSurface_meshFunction,
				    meshSurface_triangleFunction,
				    uorTolerance, STROKETOL_ChoordHeight,
				    NULL, NULL, 0.0, NULL, &surface, FALSE, FALSE, NULL);
	    mdlBspline_freeSurface (&surface);
	    }
	mdlElmdscr_freeAll (&edP);
	}


    mdlState_restartCurrentCommand();
    }

/*----------------------------------------------------------------------+
|									|
| name		meshSurface						|
|									|
| author	BSI     				11/90		|
|									|
+----------------------------------------------------------------------*/
Public void	meshSurface
(
char    *unparsedP
)	
//cmdNumber   CMD_MESH_SURFACE
    {
    /* tell MicroStation we want to start a "modification" command */
    mdlLocate_allowLocked ();
    mdlState_startModifyCommand (meshSurface, meshSurface_accept, NULL,
				 NULL, NULL, 1, 0, TRUE, FALSE);

    /* set the internal locate pointers to start at beginning of file */
    mdlLocate_init ();
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI					6/90		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
    RscFileHandle   rfHandle;

    /* Open our file for access to command table */
    mdlResource_openFile (&rfHandle, NULL, FALSE);
    
    /* Load the command table */
    static MdlCommandNumber cmdNumbers[] =
    {
    {meshSurface,CMD_MESH_SURFACE},
    0,
    };

    mdlSystem_registerCommandNumbers (cmdNumbers);

    if (mdlParse_loadCommandTable (NULL) == NULL)
	mdlOutput_error ("Unable to load command table.");

    return  SUCCESS;
    }
