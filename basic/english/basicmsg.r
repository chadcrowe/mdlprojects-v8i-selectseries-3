/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/basic/english/basicmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/basic/english/basicmsg.r_v  $
|   $Workfile:   basicmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Basic application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "basic.h"	/* basic dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_BasicErrors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_BasicDialog,   "Unable to open Basic dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
