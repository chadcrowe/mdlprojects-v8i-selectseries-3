/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/basic/basic.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/basic/basic.h_v  $
|   $Workfile:   basic.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in basic dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __basicH__
#define	    __basicH__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_Basic		1   /* dialog id for Basic Dialog */
#define DIALOGID_BasicModal	2   /* dialog id for Basic Modal Dialog */

#define OPTIONBUTTONID_Basic	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_Basic		1   /* id for "parameter 1" text item */
#define TOGGLEID_Basic		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_Basic		1   /* id for synonym resource */

#define MESSAGELISTID_BasicErrors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_BasicDialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_Basic	1   /* id for toggle item hook func */
#define HOOKDIALOGID_Basic		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct basicglobals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } BasicGlobals;

#endif
