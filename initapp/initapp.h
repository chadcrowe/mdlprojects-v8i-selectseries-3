/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/initapp/initapp.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/initapp/initapp.h_v  $
|   $Workfile:   initapp.h  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:38:46 $
|									|
+----------------------------------------------------------------------*/
#if !defined  (__initappH__)
#define     __initappH__

/*----------------------------------------------------------------------+
|									|
|   Message List IDs							|
|									|
+----------------------------------------------------------------------*/
#define MESSAGELISTID_Messages      1

/*----------------------------------------------------------------------+
|									|
|   Messages								|
|									|
+----------------------------------------------------------------------*/
#define MSGID_LineCountTemplate     1
#define MSGID_LineCountTemplate2    2
#define MSGID_ErrorNeedsDgnFile     3

#endif /* if !defined  (__initappH__) */
