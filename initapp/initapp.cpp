/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/initapp/initapp.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   initapp.mc  $
|   $Revision: 1.2.16.1 $
|   	$Date: 2013/07/01 20:38:45 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   initapp.mc - Example of an initapp program.			        |
|                                                                       |
|   This is an example program to show the usage of an appliation as    |
|   an MS_DGNAPPS and/or MS_INITAPPS and/or USER initiated program.     |
|                                                                       |
|   Examples of a command line swithes are as follows:                  |
|                                                                       |
|   ustation -wainitapp --> Will not work because a design file was not |
|                           passed.                                     |
|                                                                       |
|   ustation -wainitapp -itest.dgn --> will work without                |
|                                      graphics initiated               |
|                                                                       |
|   ustation -wainitapp -itest.dgn -ig --> will work with               |
|                                          graphics initiated           |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>        /* system include files */
#include    <tcb.h>	    
#include    <userfnc.h>
#include    <msinputq.h>
#include    <cmdlist.h>
#include    <cmdclass.h>
#include    <mselems.h>
#include    <scanner.h>
#include    <stdio.h>
#include    <dlogbox.h>
#include    <string.h>
#include    <dlogids.h>
#include    <toolsubs.h>

#include    "initapp.h"

#include    <msfile.fdf>
#include    <msscan.fdf>
#include    <msoutput.fdf>
#include    <msparse.fdf>
#include    <mselemen.fdf>
#include    <msrsrc.fdf>
#include    <mslocate.fdf>
#include    <mssystem.fdf>
#include    <msdialog.fdf>
#include    <msinput.fdf>
#include    <msview.fdf>
#include    <msscancrit.fdf>
#include    <msmodel.fdf>
#include    <msdgnobj.fdf>
#include    <mscnv.fdf>

#define debug 1

/*----------------------------------------------------------------------+
|									|
| name		initapp_scanOffline                     	        |
|									|
| author	BSI     				6/95		|
|									|
+----------------------------------------------------------------------*/
void  initapp_scanOffline 
(
)
    {
    UInt32               elemAddr[50], filePos;
    int                 numLines, numAddr, scanWords;
    int                 scan_status;
    char                lineNum[MAXFILELENGTH], directory[MAXFILELENGTH];
    char	    	device[MAXDEVICELENGTH];
    char	    	dir[MAXDIRLENGTH];
    DgnModelRefP	fileNum;
    MSWChar		displayName [512];
    FILE                *fp;                                                         
    ScanCriteria	*scP;
    long		status;
    char		disName[MAXFILELENGTH];
    char	    	formatTemplate[600];


    /*-----------------------------------------------------+
    |  initialize scan to start from begining of file      |
    +-----------------------------------------------------*/
    /*mdlScan_initScanlist (&scanList);
    mdlScan_setDrawnElements(&scanList);
    mdlScan_noRangeCheck(&scanList); */
    scP = mdlScanCriteria_create ();
    /*-------------------------------------------------+
    |  Build scan levels                               |
    +-------------------------------------------------*/
    /*-------------------------------------------------+
    | Initilize the scanList to select no levels       |
    +-------------------------------------------------*/
    /*for (i=0; i<4; i++)
        scanList.levmask[i] = 0x0000;

    scanList.typmask[0] = TMSK0_LINE;  
    scanList.typmask[1] = 0x0000;
    scanList.typmask[2] = 0x0000;
    scanList.typmask[3] = 0x0000;
    scanList.typmask[4] = 0x0000;
    scanList.typmask[5] = 0x0000;
    scanList.typmask[6] = 0x0000;
    scanList.typmask[7] = 0x0000;

    scanList.scantype = ELEMTYPE;
    scanList.extendedType = FILEPOS;
	    
    filePos = 0L;	       */
    mdlScanCriteria_addSingleElementTypeTest (scP,LINE_ELM);
    //mdlScanCriteria_setDrawnElements (scP);
    mdlScanCriteria_setReturnType (scP,MSSCANCRIT_RETURN_FILEPOS ,FALSE,TRUE );
    mdlScanCriteria_setElementCategory (scP,ELEMENT_CATEGORY_GRAPHICS );
    fileNum = MASTERFILE;
    mdlScanCriteria_setModel (scP,fileNum);
    scanWords = numLines = 0;

//    if ((init_status = mdlScan_initialize (fileNum, &scanList)) == SUCCESS);
      if  (scP)
        {
        /*-------------------------------------+
        | scan, read, find line elements       |
        +-------------------------------------*/
        do
            {
	    scanWords = sizeof (elemAddr)/sizeof (short);

	    scan_status = mdlScanCriteria_scan (scP, elemAddr,&scanWords, &filePos);
//            scan_status = mdlScan_file (elemAddr, &scanWords, sizeof(elemAddr),&filePos);
	    numAddr = scanWords / sizeof(short);
            if (numAddr >= 1)
                { 
                numLines = numLines + numAddr;
                }
            } while ((scan_status == BUFF_FULL)&& (scanWords)) ;

    	status = mdlResource_loadFromStringList(formatTemplate, NULL, 
			MESSAGELISTID_Messages, MSGID_LineCountTemplate);
	mdlModelRef_getDisplayName (fileNum,displayName,sizeof (displayName),NULL);
	mdlCnv_convertUnicodeToMultibyte (displayName,-1,disName,sizeof(disName));
        sprintf(lineNum, formatTemplate,numLines,disName); 
        /*------------------------------------------------+
        |  Create a text file which contains the number of|
        |  of lines and the file opened                   |
        +------------------------------------------------*/
        mdlFile_getcwd(directory, sizeof(directory));

	mdlFile_parseName (directory, device, dir, NULL, NULL);
	mdlFile_buildName (directory, device, dir, "linecnt", "txt");
#if defined (debug)
	printf("Opening output file %s\n", directory);
#endif
        fp = mdlTextFile_open(directory, TEXTFILE_WRITE);

        mdlTextFile_putString(lineNum, fp, TEXTFILE_DEFAULT);
        mdlTextFile_close(fp);
        }
    mdlScanCriteria_free (scP);
    }

/*----------------------------------------------------------------------+
|									|
| name		initapp_scan            			        |
|									|
| author	BSI     				6/95		|
|									|
+----------------------------------------------------------------------*/
void  initapp_scan
(
)
    {
    UInt32               elemAddr[50], filePos;
    int                 numLines, numAddr, scanWords;
    int                 scan_status;
    char                lineNum[MAXFILELENGTH];
    char	    	formatTemplate[80];
    DgnModelRefP	fileNum;
    ScanCriteria	*scP;

    mdlView_setFunction(UPDATE_POST, NULL);

    /*-----------------------------------------------------+
    |  initialize scan to start from begining of file      |
    +-----------------------------------------------------*/
    /*mdlScan_initScanlist (&scanList);
    mdlScan_setDrawnElements(&scanList);
    mdlScan_noRangeCheck(&scanList);*/
    scP = mdlScanCriteria_create ();

    /*-------------------------------------------------+
    |  Build scan levels                               |
    +-------------------------------------------------*/
    /*-------------------------------------------------+
    | Initilize the scanList to select no levels       |
    +------------------------------------------------ */
    /*for (i=0; i<4; i++)
        scanList.levmask[i] = 0x0000;

    scanList.typmask[0] = TMSK0_LINE;  
    scanList.typmask[1] = 0x0000;
    scanList.typmask[2] = 0x0000;
    scanList.typmask[3] = 0x0000;
    scanList.typmask[4] = 0x0000;
    scanList.typmask[5] = 0x0000;
    scanList.typmask[6] = 0x0000;
    scanList.typmask[7] = 0x0000;

    scanList.scantype = ELEMTYPE;
    scanList.extendedType = FILEPOS;  */
	    
    filePos = 0L;
    fileNum = MASTERFILE;
    scanWords = numLines = 0;
    mdlScanCriteria_setReturnType (scP,MSSCANCRIT_RETURN_FILEPOS ,FALSE,TRUE );
    mdlScanCriteria_addSingleElementTypeTest (scP,LINE_ELM);
    //mdlScanCriteria_setDrawnElements(scP);
    mdlScanCriteria_setModel (scP, fileNum);
    mdlScanCriteria_setElementCategory (scP,ELEMENT_CATEGORY_GRAPHICS );
    if (scP)
    //(init_status = mdlScan_initialize (fileNum, &scanList)) == SUCCESS);
        {
        /*-------------------------------------+
        | scan, read, find line elements       |
        +-------------------------------------*/
        do
            {
	    scanWords = sizeof (elemAddr)/sizeof (short);
	    scan_status = mdlScanCriteria_scan (scP, elemAddr,&scanWords, &filePos);

//            scan_status = mdlScan_file (elemAddr, &scanWords, sizeof(elemAddr),&filePos);
	    numAddr = scanWords / sizeof(short);
            if (numAddr >= 1)
                { 
                numLines = numLines + numAddr;
                }
            } while ((scanWords)&&(scan_status == BUFF_FULL));
        /*------------------------------------------------+
        |  Display the number of lines in the design file |
        +------------------------------------------------*/
    	mdlResource_loadFromStringList(formatTemplate, NULL, 
			MESSAGELISTID_Messages, MSGID_LineCountTemplate2);

        sprintf(lineNum, formatTemplate, numLines);
         
        mdlDialog_openMessageBox(DIALOGID_MsgBoxOK, lineNum,
				    MSGBOX_ICON_INFORMATION);
        }
    mdlScanCriteria_free (scP);
    }

/*----------------------------------------------------------------------+
|									|
| name		initapp_reloadFunc      				|
|									|
| author	BSI     				6/95		|
|									|
+----------------------------------------------------------------------*/
Private void  initapp_reloadFunc
(
)
    {
    //mdlInput_sendCommand
    //          (CMD_DIALOG_OPENFILE, "STARTUP", 0, ustnTaskId, 0);
    mdlDialog_cmdNumberQueue (false, CMD_DIALOG_OPENFILE,"",TRUE);
    }

/*----------------------------------------------------------------------+
|									|
| name		initapp_displayMessage      				|
|									|
| author	BSI     				6/95		|
|									|
+----------------------------------------------------------------------*/
Private int  initapp_displayMessage
(
)
    {        
    int    status;
    char   errorString[MAXFILELENGTH];

    mdlResource_loadFromStringList(errorString, NULL, 
			MESSAGELISTID_Messages, MSGID_ErrorNeedsDgnFile);

    status = mdlDialog_openMessageBox (DIALOGID_MsgBoxOKCancel, errorString, 
				    MSGBOX_ICON_INFORMATION);
    if (status == ACTIONBUTTON_OK)
        {
    mdlDialog_cmdNumberQueue (false, CMD_DIALOG_OPENFILE,"STARTUP",TRUE);
       
        }
    if (status == ACTIONBUTTON_CANCEL)
    	{
        exit(0);
    	}

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		main						        |
|									|
| author	BSI     				6/95		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int   argc,		/* => number of args in next array */
char *argv[]		/* => array of cmd line arguments */
)
    {
    static char         limits[] = "-i";
    char                 *tmp1;
    int                 open_status;
    RscFileHandle    	rscFileH;

   // mdlSystem_enterDebug();
    if (SUCCESS != mdlResource_openFile (&rscFileH, NULL, TRUE))
    	{
	/* unload this application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
    	return ERROR;
    	}

    /*----------------------------------------------------+
    |   If the calling application is a "MS_DGNAPPS"      |
    +----------------------------------------------------*/ 
    if (strcmp (argv [1], "MS_DGNAPPS") == 0)
	{ 
        mdlView_setFunction(UPDATE_POST, initapp_scan);
        }

    /*----------------------------------------------------+
    |  If the calling application is a "MS_INITAPP"       |
    +--------------------------------------------------- */
    else if (strcmp (argv [1], "MS_INITAPPS") == 0)
	{
        /*---------------------------------------------------+
        | Checking for a design file as the third parameter  |
        +---------------------------------------------------*/ 
        if (argc < 5)
            {
            mdlSystem_enterGraphics();
            initapp_displayMessage();
            }
        if (argc == 5)
            {
	    char    tmpBuf[MAXFILELENGTH];
            //tmp = strtok(argv[4], limits);
	    strcpy (tmpBuf,&argv[4][2]);
            if ((open_status = mdlSystem_newDesignFile ( tmpBuf )) == SUCCESS)
                {
                initapp_scanOffline();
                }
            else
                {
                mdlSystem_enterGraphics();
                initapp_displayMessage();
                }
            }

        /*-----------------------------------------------------+
        | Checking for the -ig switch in the fourth parameter  |
        +-----------------------------------------------------*/
        if (argc >= 6)
            {
            char    tmpBuf [MAXFILELENGTH];    
            //tmp1 = strtok(argv[5], limits);
	    strcpy (tmpBuf, &argv[4][2]);
            tmp1 = strtok(argv[4], limits);
            mdlSystem_enterGraphics();
            
            if (strcmp (tmp1, "g") == 0)
                {
                mdlSystem_enterGraphics();
                if ((open_status = mdlSystem_newDesignFile ( tmpBuf )) == SUCCESS)
                    {
                    mdlSystem_setFunction(SYSTEM_RELOAD_PROGRAM, initapp_reloadFunc);
                    initapp_scan();
                    }
                }
            else
                {
                open_status = mdlSystem_newDesignFile ( tmpBuf );
                if (SUCCESS==open_status)
                    {
                    initapp_scanOffline();
                    }
                }
            }
        }
        /*----------------------------------------------+
        | If the calling application is a "USER"        |
        +----------------------------------------------*/     
    else if (strcmp (argv [1], "USER") == 0)
        {
         initapp_scan();
        }

    return  SUCCESS;
    }                           
