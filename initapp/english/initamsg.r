/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/initapp/english/initamsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   initamsg.r  - Application's language specific resource definitions.	|
|									|
|   $Workfile:   initamsg.r  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:38:51 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "initapp.h"

/*----------------------------------------------------------------------+
|									|
|   Messages								|
|									|
+----------------------------------------------------------------------*/
MessageList  MESSAGELISTID_Messages =
    {
{

{ MSGID_LineCountTemplate,	"There are %d lines in this design file %s" },
{ MSGID_LineCountTemplate2,	"There are %d lines in this design file" },
{ MSGID_ErrorNeedsDgnFile,	"This application needs a design file." },

}
    };

