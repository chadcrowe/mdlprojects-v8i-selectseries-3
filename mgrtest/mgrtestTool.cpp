/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mgrtest/mgrtestTool.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   mgrtestTool  $
|   $Revision: 
|   	$Date: 
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   mgrtestTool - mgrtestTool source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   mgrtestTool.cpp -- Example of ISession Manager and Session Monitor  |
|   	    	                                                        |
|									|
|   This example application shows how to use ISessionMgr struct        |
|   to find a specific file or open a file with a window selection.     |
|   The application also allows user to add or delete Session Monitor.  |
|   The Session Monitor can display information of the master file and  |
|   active model.                                                       |
|									|
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -					                     	|
|									|
|   IsessionMgr_IsActive - Check each model whether it is active and    |
|       change the active model from the current to the first model     |
|	IsessionMgr_find - Find a specific file and open it as the master   |
|       file if successful 	        |
|	IsessionMgr_openDialog - Prompts user to select a file and open it  |
|       as the master file
|	IsessionMgr_addMon - Add a new Session Monitor                  	|
|	IsessionMgr_delMon - Delete a existing Session Monitor 	            |
|	main - main entry point					                         	|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>
#include    <msrmgr.h>
#include    <mssystem.fdf>
#include    <msparse.fdf>
#include <toolsubs.h>
#include <msoutput.fdf>
#include <msdgnobj.fdf>

//added for new interfaces 
#include <microstationapi.h>
#include <mstypes.h>
#include    <interface\ISessionMgr.h>
#include  <dgnfile\msdgnfile.h>


//place command
char    *g_str;

#include "mgrtestToolCmd.h"
#include "mgrtestTool.h"


USING_NAMESPACE_BENTLEY;
USING_NAMESPACE_BENTLEY_USTN;
USING_NAMESPACE_BENTLEY_USTN_ELEMENT;





/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
/*=================================================================================**//**
* Contrived ExampleSessionMon:
* 
* Print information of the master file when it stared
*
* Print information of models when it is changed
*
* Call IsessionMgr_addMon first to add Session Monotior
*
* @bsiclass
+===============+===============+===============+===============+===============+======*/
#define MAX_LENGTH 512

struct  ExampleSessionMon:SessionMonitor
{

public:
virtual void ExampleSessionMon::_OnMasterFileStart (MSDgnFileR DgnFile) override
    {

		Bentley::WString wName;
		wName = DgnFile.GetName();
		wchar_t message [MAX_LENGTH];

		swprintf(message,L"The master file is %s.\n",wName.c_str());
		mdlOutput_messageCenterW (MESSAGE_INFO, L"Master file changed.", message, false);
		return;
    }



virtual void ExampleSessionMon::_OnModelRefActivated (DgnModelRefR newModelRef, DgnModelRefP oldModelRef) override
	
    {
		MSWChar newModelName [MAX_LENGTH];
		
		mdlModelRef_getModelName (&newModelRef, newModelName);

		char message [MAX_LENGTH];
		
		sprintf(message,"The new model is %S.\n", newModelName);
		mdlOutput_messageCenter (MESSAGE_INFO, "Active model changed.", message, false);
		return;
    }
};

/*---------------------------------------------------------------------------------**//**
* @description  IsessionMgr_IsActive - Check each model whether it is active and    
*       change the active model from the current to the first model   
* @param 	unparsed      The unparsed information sent to the command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void IsessionMgr_IsActive
(
char * unparsed
)
    {
	    ISessionMgrR SessionMgr = ISessionMgr::GetManager();

		MSDgnFileP pFile = SessionMgr.GetMasterDgnFile();
		

		DgnModelRefP       modelP = NULL;
		DgnModelRefP pModel = SessionMgr.GetActiveModel(); 
        MSWChar            modelName [MAX_LENGTH];
		char  message [MAX_LENGTH];
		DgnModelRefListP  pList = NULL;
		mdlModelRefList_create (&pList);
		pList = mdlDgnFileObj_getModelRefList (pFile);
		int countlist = mdlModelRefList_getCount (pList);
	
		int set = 0;
		for (int i = 0; i < countlist; i++)
		{
			modelP = mdlModelRefList_get (pList, i);
			mdlModelRef_getModelName (modelP, modelName);
			if (TRUE == SessionMgr.IsActiveModel(modelP))
			    {
					sprintf(message,"%S is the active model.\n", modelName);

		     		mdlOutput_messageCenter (MESSAGE_INFO, "IsActive model", message, false);
			    }
			else
			    {
					if (set == 0)
					{
					set = 1;
					mdlModelRef_activateAndDisplay (modelP);
					pModel = SessionMgr.GetActiveModel(); 
					sprintf(message,"The active model is changed to %S.\n", modelName);
		     		mdlOutput_messageCenter (MESSAGE_INFO, "Active model changed.", message, false);
					}
					
			    }
		}
		mdlModelRefList_free (&pList);
		return;

    }

/*---------------------------------------------------------------------------------**//**
* @description  IsessionMgr_find - Dind a specific file and open it as the master   
*      file if successful   
* @param 	unparsed      The file name to find
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void IsessionMgr_find
(
char * unparsed
)
    {
		

    	ISessionMgrR SessionMgr = ISessionMgr::GetManager();

		MSDgnFileP pFile = SessionMgr.GetMasterDgnFile();
		if ((unparsed)&&(0!=unparsed))
		{
			
			Bentley::WString     fileName = WString (unparsed);
			wchar_t  message [MAX_LENGTH];
			DgnOpenStatus find;
			MSDocumentPtr DocumentP = SessionMgr.FindDesignFile (find, fileName.GetMSWCharCP(), GRAPHICSFILE_CAD, true);
			DgnOpenStatus Docstatus;

			if (find == SUCCESS)
			    {
			    
				
				Bentley::WString name, modelName;
				name = DocumentP->GetFileName();

				Docstatus = SessionMgr.SwitchToNewFile (*DocumentP,  L"Default" , GRAPHICSFILE_UStn, true, false, false, false);
				pFile = SessionMgr.GetMasterDgnFile();
				
				modelName = SessionMgr.GetActiveDgnCache()->GetModelName();
				
				swprintf(message,L"%s is found. The current active model is %s.\n", fileName.c_str(), modelName.c_str());
		     	mdlOutput_messageCenterW (MESSAGE_INFO, L"File found.", message, false);
			    }
			else
				mdlOutput_messageCenterW (MESSAGE_ERROR, L"Find failed", L"File is not existed.", false); 

		}
		else
		{
			mdlOutput_messageCenterW (MESSAGE_ERROR, L"Find failed", L"Please enter the file name.", false);
		}
		
		return;
    }

/*---------------------------------------------------------------------------------**//**
* @description  IsessionMgr_openDialog - Prompts user to select a file and open it  
*       as the master file
* @param 	unparsed      The unparsed information sent to the command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void IsessionMgr_openDialog
(
char * unparsed
)
    {

	ISessionMgrR SessionMgr = ISessionMgr::GetManager();

	wchar_t  message [MAX_LENGTH];
	DgnOpenStatus status;		
	MSDocumentPtr DocumentP;
	DocumentP = SessionMgr.OpenDgnFileDialog (status);
	DgnOpenStatus Docstatus;
	MSDocumentP pDocument;
	pDocument = SessionMgr.GetMaster();
	if ((DocumentP!=NULL) && (status == SUCCESS))
	    {
			Bentley::WString fileName, modelName;
			fileName = DocumentP->GetFileName();
			
			Docstatus = SessionMgr.SwitchToNewFile (*DocumentP,  L"Default" , GRAPHICSFILE_UStn, true, false, false, false);
			pDocument = SessionMgr.GetMaster();
			if (SUCCESS == Docstatus)
			{
				modelName = SessionMgr.GetActiveDgnCache()->GetModelName();
				swprintf(message,L"The opened file is %s. The current active model is %s.\n", fileName.c_str(), modelName.c_str());
		     	mdlOutput_messageCenterW (MESSAGE_INFO, L"File opened.", message, false);
			}
	    }
	return;
    }

/*---------------------------------------------------------------------------------**//**
* @description  IsessionMgr_addMon - Add a new Session Monitor
* @param 	unparsed      The unparsed information sent to the command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/

static ExampleSessionMon s_mySessionMon;

extern "C" DLLEXPORT void IsessionMgr_addMon
(
char * unparsed
)
    {
	
	ISessionMgrR SessionMgr = ISessionMgr::GetManager();
	SessionMgr.AddSessionMonitor (s_mySessionMon);
	
    }

/*---------------------------------------------------------------------------------**//**
* @description  IsessionMgr_delMon - Delete a new Session Monitor
* @param 	unparsed      The unparsed information sent to the command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void IsessionMgr_delMon
(
char * unparsed
)

    {
	
	ISessionMgrR SessionMgr = ISessionMgr::GetManager();
	SessionMgr.DropSessionMonitor (s_mySessionMon);

    }

/*---------------------------------------------------------------------------------**//**
* @description  MdlMain
* @param 	argc      The number of command line parameters sent to the application.
* @param 	argv[]    The array of strings sent to the application on the command line.
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
	RscFileHandle   rfHandle;

    mdlResource_openFile (&rfHandle,NULL,RSC_READONLY);

    // Map command name to function (usage: MDL COMMAND COMPEXPORT)
    static  MdlCommandName cmdNames[] = 
    {
		{IsessionMgr_delMon, "IsessionMgr_delMon"},
		{IsessionMgr_addMon, "IsessionMgr_addMon"},
		{IsessionMgr_IsActive,"IsessionMgr_isActive"},
		{IsessionMgr_find,"IsessionMgr_find"},
		{IsessionMgr_openDialog,"IsessionMgr_open"},
        0,
    };

    mdlSystem_registerCommandNames (cmdNames);

    // Map key-in to function
    static MdlCommandNumber cmdNumbers[] =
    {
		{IsessionMgr_delMon, CMD_MGRTEST_ACTION_DEL},
		{IsessionMgr_addMon, CMD_MGRTEST_ACTION_ADD},
		{IsessionMgr_IsActive, CMD_MGRTEST_ACTION_ISACTIVE},
		{IsessionMgr_find, CMD_MGRTEST_ACTION_FIND},
		{IsessionMgr_openDialog, CMD_MGRTEST_ACTION_OPEN},
        0,
    };

    mdlSystem_registerCommandNumbers (cmdNumbers);

    mdlParse_loadCommandTable (NULL);

 	return SUCCESS;
    }
