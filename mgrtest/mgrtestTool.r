/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mgrtest/mgrtestTool.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   toolExample  $
|   $Revision: 1.1.12.1 $
|   	$Date: 2013/07/01 20:40:20 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   toolExample - toolExample source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include <rscdefs.h>


#define     DLLAPPID 1

/* associate app with dll */
DllMdlApp DLLAPPID = 
    {
    "mgrtestTool", "mgrtestTool"
    }

