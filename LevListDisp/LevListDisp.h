/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevListDisp/LevListDisp.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevListDisp/LevListDisp.h_v  $
|   $Workfile:   LevListDisp.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in LevListDisp dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __LevListDispH__
#define	    __LevListDispH__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_LevListDisp		1   /* dialog id for LevListDisp Dialog */
#define DIALOGID_LevListDispModal	2   /* dialog id for LevListDisp Modal Dialog */

#define OPTIONBUTTONID_LevListDisp	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_LevListDisp		1   /* id for "parameter 1" text item */
#define TOGGLEID_LevListDisp		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_LevListDisp		1   /* id for synonym resource */

#define MESSAGELISTID_LevListDispErrors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_LevListDispDialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_LevListDisp	1   /* id for toggle item hook func */
#define HOOKDIALOGID_LevListDisp		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct LevListDispglobals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } LevListDispGlobals;

#endif
