/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevListDisp/LevListDispcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevListDisp/LevListDispcmd.r_v  $
|   $Workfile:   LevListDispcmd.r  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	"LevListDisp Dialog Example" Command Table Resources			|
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_LevListDisp        1

DllMdlApp DLLAPP_LevListDisp =
    {
    "LevListDisp", "LevListDisp"          // taskid, dllName
    }


/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE		 0
#define CT_LevListDisp	 1

/*----------------------------------------------------------------------+
|                                                                       |
| 	"LevListDisp dialog example" commands					|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_LevListDisp =
{
    {  1, CT_NONE, INPUT, NONE,		"OPENMODAL" },
};
