/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevListDisp/LevListDisptyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevListDisp/LevListDisptyp.mtv  $
|   $Workfile:   LevListDisptyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevListDisp Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "LevListDisp.h"

publishStructures (LevListDispglobals);

    
