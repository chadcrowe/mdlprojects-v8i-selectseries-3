/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevListDisp/english/LevListDispmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevListDisp/english/LevListDispmsg.r_v  $
|   $Workfile:   LevListDispmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevListDisp application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "LevListDisp.h"	/* LevListDisp dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_LevListDispErrors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_LevListDispDialog,   "Unable to open LevListDisp dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
