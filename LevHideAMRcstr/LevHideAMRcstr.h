/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMRcstr/LevHideAMRcstr.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMRcstr/LevHideAMRcstr.h_v  $
|   $Workfile:   LevHideAMRcstr.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in LevHideAMRcstr dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __LevHideAMRcstrH__
#define	    __LevHideAMRcstrH__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_LevHideAMRcstr		1   /* dialog id for LevHideAMRcstr Dialog */
#define DIALOGID_LevHideAMRcstrModal	2   /* dialog id for LevHideAMRcstr Modal Dialog */

#define OPTIONBUTTONID_LevHideAMRcstr	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_LevHideAMRcstr		1   /* id for "parameter 1" text item */
#define TOGGLEID_LevHideAMRcstr		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_LevHideAMRcstr		1   /* id for synonym resource */

#define MESSAGELISTID_LevHideAMRcstrErrors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_LevHideAMRcstrDialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_LevHideAMRcstr	1   /* id for toggle item hook func */
#define HOOKDIALOGID_LevHideAMRcstr		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct LevHideAMRcstrglobals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } LevHideAMRcstrGlobals;

#endif
