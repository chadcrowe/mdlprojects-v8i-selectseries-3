/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMRcstr/LevHideAMRcstrtyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMRcstr/LevHideAMRcstrtyp.mtv  $
|   $Workfile:   LevHideAMRcstrtyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevHideAMRcstr Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "LevHideAMRcstr.h"

publishStructures (LevHideAMRcstrglobals);

    
