/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMRcstr/english/LevHideAMRcstrmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMRcstr/english/LevHideAMRcstrmsg.r_v  $
|   $Workfile:   LevHideAMRcstrmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	LevHideAMRcstr application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "LevHideAMRcstr.h"	/* LevHideAMRcstr dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_LevHideAMRcstrErrors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_LevHideAMRcstrDialog,   "Unable to open LevHideAMRcstr dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
