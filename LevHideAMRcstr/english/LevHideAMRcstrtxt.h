/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMRcstr/english/LevHideAMRcstrtxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMRcstr/english/LevHideAMRcstrtxt.h_v  $
|   $Workfile:   LevHideAMRcstrtxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Static text defines for the LevHideAMRcstr application dialog resources	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__LevHideAMRcstrtxtH__)
#define	__LevHideAMRcstrtxtH__

#define	TXT_LevHideAMRcstrDialogBox			"LevHideAMRcstr Dialog Box"
#define	TXT_LevHideAMRcstrModalDialogBox			"LevHideAMRcstr Modal Dialog Box"
#define	TXT_Parameter1				"Parameter 1:"
#define	TXT_OpenModal				"Open Modal"
#define	TXT_IncrementParameter1			"Increment parameter 1?"
#define TXT_Value1				"Value 1"
#define TXT_Value2				"Value 2"
#define TXT_Value3				"Value 3"

#endif
