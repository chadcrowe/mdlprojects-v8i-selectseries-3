/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/LevHideAMRcstr/LevHideAMRcstrcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/LevHideAMRcstr/LevHideAMRcstrcmd.r_v  $
|   $Workfile:   LevHideAMRcstrcmd.r  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	"LevHideAMRcstr Dialog Example" Command Table Resources			|
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_LevHideAMRcstr        1

DllMdlApp DLLAPP_LevHideAMRcstr =
    {
    "LevHideAMRcstr", "LevHideAMRcstr"          // taskid, dllName
    }


/*----------------------------------------------------------------------+
|									|
|  Local Defines							|
|									|
+----------------------------------------------------------------------*/
#define	CT_NONE		 0
#define CT_LevHideAMRcstr	 1

/*----------------------------------------------------------------------+
|                                                                       |
| 	"LevHideAMRcstr dialog example" commands					|
|                                                                       |
+----------------------------------------------------------------------*/
Table	CT_LevHideAMRcstr =
{
    {  1, CT_NONE, INPUT, NONE,		"OPENMODAL" },
};
