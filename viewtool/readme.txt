			    VIEWTOOL

Allows for exact sizing of MicroStation's standard views.  View size can be
specified with an absolute pixel size (widthxheight) or by a ratio.  What
this enables you to do is render your view (File->Save As) and create
raster images of an exact size (640x480, 1024x768, etc.).  Normally when
doing a File->Save As you can only specify the width or the height of the
output image...the other value is computed by taking the value you entered
and multiplying it by the current view ratio.  Now you can set what the
current views ratio is so images will be the size you want without having
to muck about trying to stretch views to get the right size.