/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/viewtool/english/viewttxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/viewtool/english/viewttxt.h_v  $
|   $Workfile:   viewttxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:41:04 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   viewctxt.h                                                          |
|                                                                       |
+----------------------------------------------------------------------*/
#if !defined (__viewttxtH__)
#define __viewttxtH__

#define VIEW_DLOG_TEXT		"View Master"
#define VIEW_OPTNBTN_TEXT       "~View:"
#define VIEW_PUSHBTN_TEXT       "~OK"
#define VIEW_TOGGLE_WIDTH_TEXT	"~Width:"
#define VIEW_TOGGLE_HEIGHT_TEXT	"~Height:"
#define VIEW_TOGGLE_RATIO_TEXT	"~Ratio:"

#endif /* #if !defined (__viewttxtH__) */