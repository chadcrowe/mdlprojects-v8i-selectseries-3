/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/viewtool/english/viewtmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/viewtool/english/viewtmsg.r_v  $
|   $Workfile:   viewtmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:41:03 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   viewmsgs.r                                                          |
|                                                                       |
+----------------------------------------------------------------------*/
#include "rscdefs.h"

#include "viewtool.h"
	    
MessageList MESSAGELISTID_Messages =
{
    {
    { 0, "" },
    { MSGID_HRSelected, "Height and Ratio already selected..." },
    { MSGID_WRSelected, "Width and Ratio already selected..." },
    { MSGID_WHSelected, "Width and Height already selected..." },
    { MSGID_DialogOpenError, "Unable to open dialog box" },
    }
};

	    

