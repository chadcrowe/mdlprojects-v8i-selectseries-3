/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/viewtool/viewtool.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/viewtool/viewtool.h_v  $
|   $Workfile:   viewtool.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:41:03 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   viewtool.h								|
|                                                                       |
+----------------------------------------------------------------------*/
#if !defined (__viewtoolH__)
#define __viewtoolH__

#define DIALOGID_ViewTool		1

#define GENERICID_Overview		1

#define OPTIONBUTTONID_View     	1

#define TOGGLEID_Width			1
#define TOGGLEID_Height			2
#define TOGGLEID_Ratio  		3

#define TEXTID_Width			1
#define TEXTID_Height			2
#define TEXTID_Ratio    		3

#define PUSHBUTTONID_Okay		1

#define HOOKDIALOGID_ViewTool		1
#define HOOKITEMID_OK			2
#define HOOKITEMID_View			3
#define HOOKITEMID_Overview	        4
#define HOOKITEMID_WidthToggle	        5
#define HOOKITEMID_HeightToggle	        6
#define HOOKITEMID_RatioToggle	        7
#define HOOKITEMID_WidthText	        8
#define HOOKITEMID_HeightText	        9
#define HOOKITEMID_RatioText	        10

/*----------------------------------------------------------------------+
|									|
|   Message list IDs							|
|									|
+----------------------------------------------------------------------*/
#define	    MESSAGELISTID_Messages	0

/*----------------------------------------------------------------------+
|									|
|   Message IDs							    	|
|									|
+----------------------------------------------------------------------*/
#define     MSGID_HRSelected	    	1
#define     MSGID_WRSelected	    	2
#define     MSGID_WHSelected	    	3
#define	    MSGID_DialogOpenError	4


/*----------------------------------------------------------------------+
|									|
|   Typedefs for Dialog access strings					|
|									|
+----------------------------------------------------------------------*/
typedef struct viewtoolinfo
    {
    int		viewNo;
    int		viewWidth;
    int		viewHeight;
    float	viewRatio;
    } ViewToolInfo;

#endif /* #if !defined (__viewtoolH__) */