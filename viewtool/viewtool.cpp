/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/viewtool/viewtool.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   viewtool.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:41:03 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   viewtool.mc - MDL application to...					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <tcb.h>
#include <scanner.h>
#include <msdefs.h>
#include <rscdefs.h> 
#include <mselems.h>
#include <mdlio.h>	    
#include <mdl.h>
#include <global.h>
#include <dlogitem.h>
#include <dlogbox.h>
#include <dlogman.fdf>
#include <cexpr.h>
#include <cmdlist.h>
#include <userfnc.h>
#include <windmsg.h>
#include <math.h>
#include <toolsubs.h>
#include <msvar.fdf>
#include "viewtool.h"

#include <mssystem.fdf>
#include <msoutput.fdf>
#include <msrsrc.fdf>
#include <msview.fdf>
#include <mselmdsc.fdf>
#include <mselemen.fdf>
#include <msscan.fdf>
#include <mscexpr.fdf>
#include <msview.fdf>
#include <mscnv.fdf>
#include <msscancrit.fdf>

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
extern  MSGraphConfig       graphConfig;
static	ViewToolInfo	    viewToolInfo;
int 	justOpened = FALSE;

/*----------------------------------------------------------------------+
|									|
|   	Local Functions 						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name		viewtool_createOverview 				|
|									|
| author	BSI				    9/93		|
|									|
+----------------------------------------------------------------------*/
Private int     viewtool_createOverview
(
DialogBox       *dbP,
DialogItem      *diP
)
    {
    MSElementDescr  *edP;
    UInt32	    scanBuf[1024], eofPos, filePos, realPos;
    int		    scanWords, status, i, numAddr;
    int		    threeD=TRUE;
    ViewFlags	    viewflags;
    RotMatrix	    rotMatrix;
    Dpoint3d        origin, extent;
    ScanCriteria    *scP;

    mdlView_getParameters (&origin, NULL, &extent, &rotMatrix, NULL,
			     viewToolInfo.viewNo);

    if (tcb->ndices == 2) threeD = FALSE; 

    viewflags.patterns   = TRUE;
    viewflags.on_off     = TRUE;
    viewflags.points     = TRUE;
    viewflags.constructs = TRUE;
    viewflags.dimens     = TRUE;
    viewflags.fast_curve = TRUE;
    viewflags.fast_text  = TRUE;
    viewflags.fast_font  = TRUE;
    viewflags.line_wghts = FALSE;
    viewflags.fast_cell  = FALSE;
    viewflags.text_nodes = FALSE;
    viewflags.ed_fields  = FALSE; 
    viewflags.delay      = FALSE;	 
    viewflags.grid       = FALSE;	 
    viewflags.lev_symb   = FALSE;	 
    viewflags.def        = FALSE;	 

    /*mdlScan_initScanlist (&scanList);
    mdlScan_noRangeCheck (&scanList);
    mdlScan_setDrawnElements (&scanList);
    mdlScan_viewRange (&scanList, viewToolInfo.viewNo, 0);

    scanList.scantype     = ELEMTYPE | NESTCELL;
    scanList.extendedType = FILEPOS; */
    scP = mdlScanCriteria_create ();
    mdlScanCriteria_setReturnType (scP, MSSCANCRIT_RETURN_FILEPOS, FALSE,TRUE);
    mdlScanCriteria_setViewRangeTest (scP, viewToolInfo.viewNo, MASTERFILE);
    mdlScanCriteria_setModel (scP, MASTERFILE);
    mdlScanCriteria_setElementCategory (scP, ELEMENT_CATEGORY_GRAPHICS);
    eofPos  = mdlElement_getFilePos (FILEPOS_EOF, NULL);
    filePos = 0L;
    realPos = 0L;

    scanWords = sizeof (scanBuf)/sizeof (short);

    do
	{
	status	= mdlScanCriteria_scan (scP, scanBuf, &scanWords, &filePos);
	numAddr = scanWords/sizeof(short);

	for (i=0; i<numAddr; i++)
	    {
            if (scanBuf[i] >= eofPos)
		break;

            if (scanBuf[i] < realPos)
                continue;

            if (mdlElmdscr_read (&edP, scanBuf[i], 0, FALSE, &realPos) != 0)
		{
		mdlElmdscr_displayToWindow ((GuiWindowP) dbP, &diP->rect,
					    &viewflags, edP, &rotMatrix,
					    &origin, &extent, threeD, -1);
		mdlElmdscr_freeAll (&edP);
		}
	    }

	} while (status == BUFF_FULL);

    return  SUCCESS;
    }
 
/*----------------------------------------------------------------------+
|									|
| name		viewtool_viewChanged					|
|									|
| author	BSI				    10/93		|
|									|
+----------------------------------------------------------------------*/
Private int	viewtool_viewChanged ()
    {
    BSIRect          clipRect;
    MSWindow	    *vwP;
    DialogBox       *dbP;
    int		    width, height;

    vwP = mdlWindow_viewWindowGet (viewToolInfo.viewNo);
    mdlWindow_contentRectGetLocal (&clipRect, vwP);

    width  = (clipRect.corner.x-clipRect.origin.x)+1;
    height = (clipRect.corner.y-clipRect.origin.y)+1;

    if ((viewToolInfo.viewWidth != width) || (viewToolInfo.viewHeight != height))
        {
        viewToolInfo.viewWidth  = width;
        viewToolInfo.viewHeight = height;
        viewToolInfo.viewRatio  = ((float) width)/height;

        if (dbP = mdlDialog_find (DIALOGID_ViewTool, NULL))
	    mdlDialog_itemsSynch (dbP);

	return TRUE;
        }

    return FALSE;
    }

/*----------------------------------------------------------------------+
|									|
| name		viewtool_updateOverview					|
|									|
| author	BSI				    12/93		|
|									|
+----------------------------------------------------------------------*/
Private void	viewtool_updateOverview
(
int     forceUpdate
)
    {
    DialogBox   *dbP;
    DialogItem  *diP;
    int		noDrawFromResize = TRUE;

    dbP = mdlDialog_find (DIALOGID_ViewTool, NULL);
    diP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_Generic, GENERICID_Overview, 0);

    if (viewtool_viewChanged ())
        {
        BSIRect     clipRect;
        Sextent	    sextent;
        int	    width, height, fontHeight;

	clipRect = diP->rect;
	mdlDialog_rectInset (&clipRect, -2, -2);
	mdlDialog_rectFill (dbP, &clipRect, LGREY_INDEX);

	sextent.origin.x = (short)mdlCnv_roundDoubleToLong(-1*diP->rect.origin.x);
	sextent.origin.y = (short)mdlCnv_roundDoubleToLong(-1*diP->rect.origin.y);
	sextent.height   = (short)mdlCnv_roundDoubleToLong(-1*(diP->rect.corner.y-diP->rect.origin.y+1));
	sextent.width    = (short)mdlCnv_roundDoubleToLong(-1*(diP->rect.corner.y-diP->rect.origin.y+1)*viewToolInfo.viewRatio);

	if ((diP->rect.corner.y-diP->rect.origin.y+1)*viewToolInfo.viewRatio > (diP->rect.corner.x-diP->rect.origin.x))
	    noDrawFromResize = FALSE;

	fontHeight = mdlDialog_fontGetCurHeight (dbP);

	width  = mdlCnv_roundDoubleToLong (diP->rect.origin.x+
	  ((diP->rect.corner.y-diP->rect.origin.y+1)*viewToolInfo.viewRatio)+
			    	(2*(XC*fontHeight/DCOORD_RESOLUTION)));

	height = diP->rect.origin.y+
	  (diP->rect.corner.y-diP->rect.origin.y+1)+(YC*fontHeight/DCOORD_RESOLUTION);

        mdlWindow_extentSet ((GuiWindowP) dbP, width, height);
	mdlDialog_itemSetExtent (dbP, diP->itemIndex, &sextent, FALSE);
	mdlWindow_windowEventsProcessAll ();
	}

    if ((noDrawFromResize) || (forceUpdate))
         mdlDialog_itemDraw (dbP, diP->itemIndex);
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Event Hook Functions                                                |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name		viewtool_newDesignFile					|
|									|
| author	BSI				    9/93		|
|									|
+----------------------------------------------------------------------*/
Private int	viewtool_newDesignFile
(
char	    *filenameP,
int	    state
)
    {
    if ((state == SYSTEM_NEWFILE_COMPLETE) &&
    	(FALSE == mdlView_isActive (viewToolInfo.viewNo)))
	{
    	mdlView_turnOn (viewToolInfo.viewNo);
	justOpened = TRUE;
	}

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		viewtool_viewUpdate					|
|									|
| author	BSI				    9/93		|
|									|
+----------------------------------------------------------------------*/
Private int	viewtool_viewUpdate
(
int		    preUpdate,
int		    eraseMode,
long		    *fileMask,
int		    numRegions,
Asynch_update_view  regions[],
BSIRect             *coverLists [],
int		    numCovers[],
MSDisplayDescr	    *displayDescr []
)
    {
    int     i;

    if (!justOpened)
    	{
	for (i=0; i<numRegions; i++)
	    {
	    if ((regions[i].viewnum == viewToolInfo.viewNo) &&
		(regions[i].screen_corner.x == regions[i].update_corner.x-regions[i].update_origin.x) &&
		(regions[i].screen_corner.y == regions[i].update_corner.y-regions[i].update_origin.y))
		{
		viewtool_updateOverview (FALSE);
		break;
		}
	    }
	}
    justOpened = FALSE;

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		viewtool_windowUpdate					|
|									|
| author	BSI				    9/93		|
|									|
+----------------------------------------------------------------------*/
Private int	viewtool_windowUpdate
(
MSWindow       *windowP,        /* => window receiving update */
int             windowOp,       /* => operation */
BSIRect        *oldGlobalP      /* => global rectangle before operation */
)
    {
    int		viewNumber;

    mdlWindow_isView (&viewNumber, windowP);

    if ((windowOp == WINDOW_MOVEEVENT) &&
	(viewNumber == viewToolInfo.viewNo))
	{
        BSIRect   clipRect;

#if defined (MSVERSION) && (MSVERSION >= 0x551)
	/* get the view rectangle, excluding view scroll bars */
	mdlView_getViewRectangle (&clipRect, windowP, VIEW_INLOCALCOORDS);
#else
	mdlWindow_globalRectGetGlobal (&clipRect, windowP);
#endif

	if ((oldGlobalP->corner.x-oldGlobalP->origin.x != clipRect.corner.x-clipRect.origin.x) ||
	    (oldGlobalP->corner.y-oldGlobalP->origin.y != clipRect.corner.y-clipRect.origin.y))
	    viewtool_updateOverview (FALSE);
	}

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Dialog Hook Functions                                               |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_dlogHook	                                |
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_dlogHook
(
DialogMessage   *dmP
)
    {
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
      	{

	case DIALOG_MESSAGE_CREATE:
	    {
	    /* establish an interest is dialog box resizes */
	    dmP->u.create.interests.resizes = TRUE;
	    break;
	    };

	case DIALOG_MESSAGE_RESIZE:
	    {
	    /* force a complete update of the dialog box */
	    dmP->u.resize.forceCompleteRedraw = TRUE;
	    break;
	    };

	case DIALOG_MESSAGE_DESTROY:
	    {
	    /* unload this application when the dialog box is closed */
	    mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD, 
				    	mdlSystem_getCurrTaskID (), TRUE);
	    break;
	    };

      	default:
	    dmP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|									|
| name		viewtool_overGenericHook				|
|									|
| author	BSI				    9/93		|
|									|
+----------------------------------------------------------------------*/
Private void	viewtool_overGenericHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    dimP->dialogItemP->attributes.acceptsKeystrokes = FALSE;
	    dimP->dialogItemP->attributes.mouseSensitive    = FALSE;
	    break;
	    }

        case DITEM_MESSAGE_DRAW:
	    {
            BSIRect   clipRect;

	    if (FALSE == mdlView_isActive (viewToolInfo.viewNo))
		{
		justOpened = TRUE;
		mdlView_turnOn (viewToolInfo.viewNo);
		}
	    else
		{
		BSIColorDescr   *bgColorP;

		clipRect = dimP->dialogItemP->rect;
		mdlDialog_rectInset (&clipRect, -2, -2);
		mdlDialog_rectDrawBeveled (dimP->db, &clipRect, FALSE, TRUE);

		bgColorP = mdlColorPal_getColorDescr (NULL, 255);
		mdlDialog_rectFillCD (dimP->db, &dimP->dialogItemP->rect, &dimP->dialogItemP->rect, bgColorP, NULL);
		viewtool_createOverview (dimP->db, dimP->dialogItemP);
		}

	    break;
	    }
    
	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_viewOptionHook                                 |
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_viewOptionHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
    	case DITEM_MESSAGE_STATECHANGED:
	    {
	    if (dimP->u.stateChanged.reallyChanged)
		{
		if (FALSE == mdlView_isActive (viewToolInfo.viewNo))
		    {
		    justOpened = TRUE;
		    mdlView_turnOn (viewToolInfo.viewNo);
		    }

		viewtool_updateOverview (TRUE);
		break;
		}
	    }

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_widthToggleHook                                |
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_widthToggleHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
    	case DITEM_MESSAGE_SETSTATE:
	    {
	    ValueUnion value;

	    mdlDialog_itemGetValue (NULL, &value, NULL, dimP->db, dimP->itemIndex, 0);

	    switch (value.sLongFormat)
		{
		case 0:
		    {
		    mdlDialog_itemSetEnabledState (dimP->db, dimP->itemIndex+1, FALSE, FALSE);
		    break;
		    }

		case 1:
		    {
		    DialogItem  *diHP, *diRP;
		    ValueUnion  valueH, valueR;

		    diHP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Height, 0);
		    diRP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Ratio, 0);

		    mdlDialog_itemGetValue (NULL, &valueH, NULL, dimP->db, diHP->itemIndex, 0);
		    mdlDialog_itemGetValue (NULL, &valueR, NULL, dimP->db, diRP->itemIndex, 0);

		    if ((!valueH.sLongFormat) || (!valueR.sLongFormat))
			{
			mdlDialog_itemSetEnabledState (dimP->db, dimP->itemIndex+1, TRUE, FALSE);
			}
		    else
			{
			value.sLongFormat = 0;
			mdlDialog_itemSetValue (NULL, 0, &value, NULL, dimP->db, dimP->itemIndex);
			mdlOutput_rscPrintf (MSG_ERROR, NULL, 
					    MESSAGELISTID_Messages, 
					    MSGID_HRSelected);
			}
		    break;
		    }
		}
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_heightToggleHook                               |
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_heightToggleHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
    	case DITEM_MESSAGE_SETSTATE:
	    {
	    ValueUnion value;

	    mdlDialog_itemGetValue (NULL, &value, NULL, dimP->db, dimP->itemIndex, 0);

	    switch (value.sLongFormat)
		{
		case 0:
		    {
		    mdlDialog_itemSetEnabledState (dimP->db, dimP->itemIndex+1, FALSE, FALSE);
		    break;
		    }
									    
		case 1:
		    {
		    DialogItem  *diWP, *diRP;
		    ValueUnion  valueW, valueR;

		    diWP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Width, 0);
		    diRP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Ratio, 0);

		    mdlDialog_itemGetValue (NULL, &valueW, NULL, dimP->db, diWP->itemIndex, 0);
		    mdlDialog_itemGetValue (NULL, &valueR, NULL, dimP->db, diRP->itemIndex, 0);

		    if ((!valueW.sLongFormat) || (!valueR.sLongFormat))
			{
			mdlDialog_itemSetEnabledState (dimP->db, dimP->itemIndex+1, TRUE, FALSE);
			}
		    else
			{
			value.sLongFormat = 0;
			mdlDialog_itemSetValue (NULL, 0, &value, NULL, dimP->db, dimP->itemIndex);
			mdlOutput_rscPrintf (MSG_ERROR, NULL, 
				    	MESSAGELISTID_Messages, MSGID_WRSelected);
			}
		    break;
		    }
		}

	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_ratioToggleHook                                |
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_ratioToggleHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
    	case DITEM_MESSAGE_SETSTATE:
	    {
	    ValueUnion value;

	    mdlDialog_itemGetValue (NULL, &value, NULL, dimP->db, dimP->itemIndex, 0);

	    switch (value.sLongFormat)
		{
		case 0:
		    {
		    mdlDialog_itemSetEnabledState (dimP->db, dimP->itemIndex+1, FALSE, FALSE);
		    break;
		    }

		case 1:
		    {
		    DialogItem  *diWP, *diHP;
		    ValueUnion  valueW, valueH;

		    diWP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Width, 0);
		    diHP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Height, 0);

		    mdlDialog_itemGetValue (NULL, &valueW, NULL, dimP->db, diWP->itemIndex, 0);
		    mdlDialog_itemGetValue (NULL, &valueH, NULL, dimP->db, diHP->itemIndex, 0);

		    if ((!valueW.sLongFormat) || (!valueH.sLongFormat))
			{
			mdlDialog_itemSetEnabledState (dimP->db, dimP->itemIndex+1, TRUE, FALSE);
			}
		    else
			{
			value.sLongFormat = 0;
			mdlDialog_itemSetValue (NULL, 0, &value, NULL, dimP->db, dimP->itemIndex);
			mdlOutput_rscPrintf (MSG_ERROR, NULL, 
			    	MESSAGELISTID_Messages, MSGID_WHSelected);
			}
		    break;
		    }
		}

	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_widthTextHook					|
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_widthTextHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
      	case DITEM_MESSAGE_FOCUSOUT:
	    {
	    DialogItem  *diP;
	    ValueUnion  value;

	    mdlDialog_itemSetState (NULL, dimP->db, dimP->itemIndex);

	    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Ratio, 0);
	    mdlDialog_itemGetValue (NULL, &value, NULL, dimP->db, diP->itemIndex, 0);

	    if (!value.sLongFormat)
		viewToolInfo.viewRatio  = ((float) viewToolInfo.viewWidth)/viewToolInfo.viewHeight;
	    else
		viewToolInfo.viewHeight = mdlCnv_roundDoubleToLong (viewToolInfo.viewWidth / viewToolInfo.viewRatio);

	    mdlDialog_itemsSynch (dimP->db);
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_heightTextHook					|
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_heightTextHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
      	case DITEM_MESSAGE_FOCUSOUT:
	    {
	    DialogItem  *diP;
	    ValueUnion  value;

	    mdlDialog_itemSetState (NULL, dimP->db, dimP->itemIndex);

	    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Ratio, 0);
	    mdlDialog_itemGetValue (NULL, &value, NULL, dimP->db, diP->itemIndex, 0);

	    if (!value.sLongFormat)
		viewToolInfo.viewRatio = ((float) viewToolInfo.viewWidth)/viewToolInfo.viewHeight;
	    else
		viewToolInfo.viewWidth = mdlCnv_roundDoubleToLong (viewToolInfo.viewHeight * viewToolInfo.viewRatio);

	    mdlDialog_itemsSynch (dimP->db);
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_ratioTextHook					|
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_ratioTextHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
      	case DITEM_MESSAGE_FOCUSOUT:
	    {
	    DialogItem  *diP;
	    ValueUnion  value;

	    mdlDialog_itemSetState (NULL, dimP->db, dimP->itemIndex);

	    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_ToggleButton, TOGGLEID_Width, 0);
	    mdlDialog_itemGetValue (NULL, &value, NULL, dimP->db, diP->itemIndex, 0);

	    if (!value.sLongFormat)
	    	{
		viewToolInfo.viewWidth  = mdlCnv_roundDoubleToLong(viewToolInfo.viewHeight * viewToolInfo.viewRatio);
	    	}
	    else
	    	{
		viewToolInfo.viewHeight = mdlCnv_roundDoubleToLong(viewToolInfo.viewWidth / viewToolInfo.viewRatio);
	    	}

	    mdlDialog_itemsSynch (dimP->db);
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          viewtool_okayButtonHook                                 |
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    viewtool_okayButtonHook
(
DialogItemMessage       *dimP
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
      	case DITEM_MESSAGE_BUTTON:
	    {
	    MSWindow    *vwP;

	    if (FALSE == mdlView_isActive (viewToolInfo.viewNo))
	        {
	        justOpened = TRUE;
	        mdlView_turnOn (viewToolInfo.viewNo);
	        }

	    mdlWindow_setFunction (WINDOW_MODIFYEVENTS, NULL);
	    mdlView_setFunction (UPDATE_POST, NULL);

	    vwP = mdlWindow_viewWindowGet (viewToolInfo.viewNo);
	    mdlWindow_extentSet (vwP, viewToolInfo.viewWidth, viewToolInfo.viewHeight);
	    viewtool_updateOverview (TRUE);

	    mdlWindow_setFunction (WINDOW_MODIFYEVENTS, viewtool_windowUpdate);
	    mdlView_setFunction (UPDATE_POST, viewtool_viewUpdate);
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Open Initial Application Dialog                                     |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name		viewtool_openDialog					|
|									|
| author	BSI				    9/93		|
|									|
+----------------------------------------------------------------------*/
Private int	viewtool_openDialog ()
    {
    DialogBox   *dbP;
    DialogItem  *diP;
    int		view;
    SymbolSet	*setP;
    int		fontHeight;

    viewToolInfo.viewNo = 0;

    /* find the first available, active view */
    for (view=0; view<8; view++)
	if (TRUE == mdlView_isActive (view))
	    {
	    viewToolInfo.viewNo = view;
	    break;
	    }

    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);
    mdlDialog_publishComplexVariable (setP, "viewtoolinfo", "viewToolInfo", &viewToolInfo);

    if ((dbP = mdlDialog_create (NULL, NULL, RTYPE_DialogBox, DIALOGID_ViewTool, FALSE)) == NULL)
	{
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Messages,
			     MSGID_DialogOpenError);
	return 1;
	}
	    
    viewtool_updateOverview (FALSE);
    diP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_Generic, GENERICID_Overview, 0);
    fontHeight = mdlDialog_fontGetCurHeight (dbP);

    mdlWindow_extentSet ((GuiWindowP) dbP, diP->rect.corner.x+(2*(XC*fontHeight/DCOORD_RESOLUTION)),
		    	diP->rect.corner.y+(YC*fontHeight/DCOORD_RESOLUTION));
    mdlDialog_show (dbP);

    mdlWindow_setFunction (WINDOW_MODIFYEVENTS, viewtool_windowUpdate);
    mdlView_setFunction (UPDATE_POST, viewtool_viewUpdate);
    mdlSystem_setFunction (SYSTEM_NEW_DESIGN_FILE, viewtool_newDesignFile);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Dialog Hooks Structure                                              |
|                                                                       |
+----------------------------------------------------------------------*/
Private DialogHookInfo uHooks[]=
    {
    {HOOKDIALOGID_ViewTool,          (PFDialogHook)viewtool_dlogHook},
    {HOOKITEMID_OK,		     (PFDialogHook)viewtool_okayButtonHook},
    {HOOKITEMID_Overview,	     (PFDialogHook)viewtool_overGenericHook},
    {HOOKITEMID_View,		     (PFDialogHook)viewtool_viewOptionHook},
    {HOOKITEMID_WidthToggle,	     (PFDialogHook)viewtool_widthToggleHook},
    {HOOKITEMID_HeightToggle,	     (PFDialogHook)viewtool_heightToggleHook},
    {HOOKITEMID_RatioToggle,	     (PFDialogHook)viewtool_ratioToggleHook},
    {HOOKITEMID_WidthText,	     (PFDialogHook)viewtool_widthTextHook},
    {HOOKITEMID_HeightText,	     (PFDialogHook)viewtool_heightTextHook},
    {HOOKITEMID_RatioText,	     (PFDialogHook)viewtool_ratioTextHook},
    };

/*----------------------------------------------------------------------+
|                                                                       |
| name          main                                                    |
|                                                                       |
| author        BSI                                     9/93            |
|                                                                       |
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    RscFileHandle         rscFileH;

    mdlResource_openFile (&rscFileH, NULL, 0);
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    viewtool_openDialog ();

    return SUCCESS;
    }
