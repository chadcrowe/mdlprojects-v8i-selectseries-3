/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/viewtool/viewtool.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/viewtool/viewtool.r_v  $
|   $Workfile:   viewtool.r  $
|   $Revision: 1.3.32.1 $
|   	$Date: 2013/07/01 20:41:03 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   viewtool.r								|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <dlogbox.h>
#include <dlogids.h>

#include "viewtool.h"
#include "viewttxt.h"

/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_VIEWTOOL        1

DllMdlApp DLLAPP_VIEWTOOL =
    {
    "VIEWTOOL", "viewtool"          // taskid, dllName
    }



/*----------------------------------------------------------------------+
|                                                                       |
|   Dialog Box Resource Definitions                                     |
|                                                                       |
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_ViewTool =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE | DIALOGATTR_REQUESTBACKINGSTORE,
    100*XC, 11*YC+6,
    NOHELP, MHELP, HOOKDIALOGID_ViewTool, NOPARENTID,
    VIEW_DLOG_TEXT,
{
{{18*XC+3, 1*YC, 148, 115}, Generic, GENERICID_Overview, ON, 0, "", ""},
{{11*XC,   1*YC, 6*XC,  0}, OptionButton, OPTIONBUTTONID_View, ON, 0, "", ""},
{{1*XC,    3*YC, 0,     0}, ToggleButton, TOGGLEID_Width, ON, 0, "", ""},
{{11*XC,   3*YC, 6*XC,  0}, Text, TEXTID_Width, OFF, 0, "", ""},
{{1*XC,    5*YC, 0,     0}, ToggleButton, TOGGLEID_Height, ON, 0, "", ""},
{{11*XC,   5*YC, 6*XC,  0}, Text, TEXTID_Height, OFF, 0, "", ""},
{{1*XC,    7*YC, 0,     0}, ToggleButton, TOGGLEID_Ratio, ON, 0, "", ""},
{{11*XC,   7*YC, 6*XC,  0}, Text, TEXTID_Ratio, OFF, 0, "", ""},
{{4*XC+2,  9*YC, 10*XC, 0}, PushButton, PUSHBUTTONID_Okay, ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Item Resource Definitions						|
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Option Button Item Resources                                        |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_OptionButtonRsc OPTIONBUTTONID_View =
    {
    NOSYNONYM, NOHELP, LHELP,
    HOOKITEMID_View,
    OPTNBTNATTR_NEWSTYLE | OPTNBTNATTR_DONTADDBUMPWIDTH | NOARG,
    VIEW_OPTNBTN_TEXT, "viewToolInfo.viewNo",
    {
{NOTYPE, NOICON, NOCMD, LCMD, 0, NOMASK, ON, "~1"},
{NOTYPE, NOICON, NOCMD, LCMD, 1, NOMASK, ON, "~2"},
{NOTYPE, NOICON, NOCMD, LCMD, 2, NOMASK, ON, "~3"},
{NOTYPE, NOICON, NOCMD, LCMD, 3, NOMASK, ON, "~4"},
{NOTYPE, NOICON, NOCMD, LCMD, 4, NOMASK, ON, "~5"},
{NOTYPE, NOICON, NOCMD, LCMD, 5, NOMASK, ON, "~6"},
{NOTYPE, NOICON, NOCMD, LCMD, 6, NOMASK, ON, "~7"},
{NOTYPE, NOICON, NOCMD, LCMD, 7, NOMASK, ON, "~8"},
    }
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   PushButton Item Resources                                           |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_Okay =
    {
    DEFAULT_BUTTON, NOHELP, LHELP,
    HOOKITEMID_OK, NOARG, NOCMD, LCMD,
    "", VIEW_PUSHBTN_TEXT
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Text Item Resources                                        		|
|                                                                       |
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_Width =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, LHELP,
    HOOKITEMID_WidthText, NOARG, 5, "%d", "%d", "1", "",
    NOMASK, TEXT_NOCONCAT, "", "viewToolInfo.viewWidth"
    };

DItem_TextRsc TEXTID_Height =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, LHELP,
    HOOKITEMID_HeightText, NOARG, 5, "%d", "%d", "1", "",
    NOMASK, TEXT_NOCONCAT, "", "viewToolInfo.viewHeight"
    };

DItem_TextRsc TEXTID_Ratio =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, LHELP,
    HOOKITEMID_RatioText, NOARG, 5, "%f", "%f", "", "",
    NOMASK, TEXT_NOCONCAT, "", "viewToolInfo.viewRatio"
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Toggle Button Item Resources                                        |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEID_Width =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, LHELP,
    HOOKITEMID_WidthToggle, NOARG, NOMASK, NOINVERT,
    VIEW_TOGGLE_WIDTH_TEXT, ""
    };

DItem_ToggleButtonRsc TOGGLEID_Height =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, LHELP,
    HOOKITEMID_HeightToggle, NOARG, NOMASK, NOINVERT,
    VIEW_TOGGLE_HEIGHT_TEXT, ""
    };

DItem_ToggleButtonRsc TOGGLEID_Ratio =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, LHELP,
    HOOKITEMID_RatioToggle, NOARG, NOMASK, NOINVERT,
    VIEW_TOGGLE_RATIO_TEXT, ""
    };

/*----------------------------------------------------------------------+
|									|
|   Generic Icon Resource 						|
|									|
+----------------------------------------------------------------------*/
DItem_GenericRsc GENERICID_Overview =
    {
    NOHELP, MHELP, HOOKITEMID_Overview, NOARG
    };
