/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v550new/english/v550msg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   v550msg.r  - Application's language specific resource definitions. 	|
|									|
|   $Workfile:   v550msg.r  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:41:00 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "v550new.h"

/*----------------------------------------------------------------------+
|									|
|   Messages								|
|									|
+----------------------------------------------------------------------*/
MessageList  STRINGID_V550NewMessages =
    {
{
{ MSGID_ErrorLoadingCmdTable,	"Error loading command table" },
{ MSGID_YesNoCancelQuestion,	"Use this type of message box if an answer is required " 
    "before proceeding. Pressing <cancel> should return to state before question was asked." },
{ MSGID_OKInformation,      	"Use this message box to display informational messages. " 
    "No choice is required from the user." },
{ MSGID_OKCritical,      	"Use this message box to display severe error conditions. " 
    "No choice is required from the user." },
{ MSGID_OKCancelWarning,      	"Use this type of message box if an answer is required "
    "before proceeding and the default action is potentially dangerous. Example: "
    "overwriting a file." },
{ MSGID_FileOpenDialogTitle,	"File Open Example" },
{ MSGID_FileOpenFilterString,	"*.dgn,MicroStation Design Files [*.dgn],"
    "*.123,Other File Type [*.123],*.xyz,Another File Type [*.xyz]"},
{ MSGID_CustomHint,	    	"This is an example of a custom button." },
{ MSGID_ToolBoxOpenErr,      	"Unable to open the sample tool box. " 
    "No choice is required from the user." },
{ MSGID_RunningPowerDraft,	"Running PowerDraft..." },
{ MSGID_MessagesDirections,	"After closing this dialog, press <D> data "
    "point to keep cycling through message types in the status area." },
{ MSGID_MessagesExample0,	"MSG_MESSAGE" 	},
{ MSGID_MessagesExample1,	"MSG_ERROR" 	},
{ MSGID_MessagesExample2,	"MSG_PROMPT" 	},
{ MSGID_MessagesExample3,	"MSG_STATUS" 	},
{ MSGID_MessagesExample4,	"MSG_COMMAND" 	},
{ MSGID_InvalidVersion,	    	"Invalid MicroStation Version"},
{ MSGID_RunningReview,	    	"Running MicroStation Review..."},
{ MSGID_RunningMicroStation,	"Running MicroStation..."},
{ MSGID_CenterTxtItemMsg,	"Resize dialog vertically to display only the first two items in this dialog box."},
{ MSGID_BottomTxtItemMsg,	"Resize dialog vertically to display all three items in this dialog box."},
{ MSGID_TopTxtItemMsg,      	"This item is always displayed."},
{ MSGID_WindowMovingOpenErr,    "Unable to window moving example dialog. " 
    "No choice is required from the user." },
{ MSGID_CompletedSaveDuringSession, "Save completed during session."},
{ MSGID_BeforeSaveOnExit,      	"Before the save on file close."},
}
    };

