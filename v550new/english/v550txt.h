/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v550new/english/v550txt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   v550txt.h  -- Application's language specific constants.   	    	|
|									|
|   $Workfile:   v550txt.h  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:41:01 $
|									|
+----------------------------------------------------------------------*/
#ifndef __v550txtH__
#define __v550txtH__

/*----------------------------------------------------------------------+
|									|
|   Header File Dependencies						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Defines								|
|									|
+----------------------------------------------------------------------*/
#define ASPECT_V550New		    1.0

#define TXT_V550NewCustomize	    "Custom..."
#define TXT_FileTextLabel	    "F~iles:"
#define TXT_DirectoriesLabel	    "Directories:"
#define TXT_PlaceMyLine		    "Place Line"
#define TXT_Flyover_PlaceMyLine	    "Place Line Flyover Help"
#define TXT_Balloon_PlaceMyLine	    "Place Line Balloon Help"
#define TXT_SmartToolsTitle	    "Smart Tools"
#define TXT_SampleToolBoxTitle	    "Sample"
#define TXT_SampleFrame       	    "Sample Frame"
#define TXT_V550NewAppTitle	    "~Demonstrate"
#define TXT_DemoFileOpen	    "~File Open"
#define TXT_DemoMessageBoxes	    "~Message Boxes"
#define TXT_DemoPrompts		    "~Prompts"
#define TXT_DemoToolBox		    "~Tool Box"
#define TXT_WindowMovingDialogTitle "Window Moving Dialog Example"
#define TXT_DemoWindowMoving	    "~Window Moving"
#define TXT_CenterMlTextItemLabel   "Center multiline text item:"
#define TXT_BottomMlTextItemLabel   "Bottom multiline text item:"
#define TXT_TopTextItemLabel	    "Top text item:"

#endif
