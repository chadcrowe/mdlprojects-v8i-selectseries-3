/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v550new/v550new.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   v550new.mc  $
|   $Revision: 1.1.32.1 $
|       $Date: 2013/07/01 20:40:59 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   v550new.mc  - Application which demonstrates functionality new to   |
|               MicroStation PowerDraft and MicroStation 95     |
|                                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <global.h>
#include <rscdefs.h>
#include <dlogids.h>
#include <dlogitem.h>
#include <deffiles.h>
#include <cmdlist.h>
#include <userfnc.h>
#include <string.h>
#include <tcb.h>
#include <userpref.h>
#include <msinputq.h>
#include <stdlib.h>

#include <dlogman.fdf>
#include <msdialog.fdf>
#include <msoutput.fdf>
#include <msparse.fdf>
#include <msrsrc.fdf>
#include <msstate.fdf>
#include <mssystem.fdf>
#include <mscexpr.fdf>
#include <msinput.fdf>
#include <mselemen.fdf>
#include <dlmsys.fdf>
#include <toolsubs.h>
#include <msvar.fdf>

#include "v550new.h"
#include "v550cmd.h"

/*----------------------------------------------------------------------+
|                                                                       |
|   Local defines                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Local type definitions                                              |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Local function definitions                                          |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Private Global variables                                            |
|                                                                       |
+----------------------------------------------------------------------*/
static DItem_PulldownMenuItem   gAppMenuItem;

int                             newXSize, newYSize;
char                            textItemVariable[128];
Private BoolInt                 needsUpdate = FALSE;
char                            mlTextString [1000];

/*----------------------------------------------------------------------+
|                                                                       |
|   Public Global variables                                             |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   External variables                                                  |
|                                                                       |
+----------------------------------------------------------------------*/

/*======================================================================+
|                                                                       |
|   Private Utility Routines                                            |
|                                                                       |
+======================================================================*/

/*======================================================================+
|                                                                       |
|   CAD Input Journaling Example                                        |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          applMessageReceive                                      |
|                                                                       |
| author        BSI                                     10/95           |
|                                                                       |
| This function processes the application messages which were journaled |
| by the multiline text item hook function during MACRO Record.         |
|                                                                       |
+----------------------------------------------------------------------*/
void applMessageReceive
(
Inputq_element  *queueElementP  /* <=> pointer to queue element */
)
    {
    int         itemIndex;
    DialogBox   *dbP;
    DialogItem  *itemDiP;

    /* find a pointer to the Window Moving dialog box */
    dbP = mdlDialog_find (DIALOGID_WindowMoving, NULL);

    /* if the returned pointer is NULL then return */
    if (NULL == dbP)
        return;

    /* find a pointer to the multiline text item in the dialog box */
    itemDiP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_MultilineText, 
                                        MULTILINETEXTID_CenterItem, 0);
    /* if the returned pointer is NULL then return */
    if (itemDiP == NULL)
        return;

    itemIndex = itemDiP->itemIndex;

    /* if the application message contains the first line of input */
    if (0 == strncmp ("LineOne ", queueElementP->u.fill, strlen("LineOne ")))
        {
        /* clear the contents of the buffer */
        memset (mlTextString, 0, sizeof(mlTextString));

        /* set the global variable to the first line of input */
        sprintf(mlTextString, "%s\0", &queueElementP->u.fill[8]);
        }
    /* else the application message contains the next line of input */
    else
        {
        /* append to the global variable the next line of input */
        sprintf(mlTextString, "%s\n%s\0", mlTextString, &queueElementP->u.fill[9]);
        }

    /* update the text item to show its new value */
    mdlDialog_itemSynch (dbP, itemIndex);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          multiline_itemHook                                      |
|                                                                       |
| author        BSI                                     10/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    multiline_itemHook
(
DialogItemMessage       *dimP   /* => a ptr to a dialog item message */
)
    {

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_CREATE:
            {

            /* clear the contents of the buffer */
            memset (mlTextString, 0, sizeof(mlTextString));

            /* load the contents of the buffer from the resource file */
            if (mdlResource_loadFromStringList
                    (mlTextString, NULL, STRINGID_V550NewMessages, 
                                MSGID_CenterTxtItemMsg) != SUCCESS)
                {
                dimP->u.create.createFailed = TRUE;
                }

            break;
            }


        case DITEM_MESSAGE_GETSTATE:
            {

            /* set the string for the multiline text item */
            dimP->u.value.stringValueP = mlTextString;
            dimP->u.value.maxStringSize = sizeof(mlTextString);

            /* tell the dialog manager that we have handled this event */
            dimP->u.value.hookHandled = TRUE;

            break;
            }

        case DITEM_MESSAGE_SETSTATE:
            {

            /* if the string's value has been changed then update our variable */
            if (0 != strncmp(mlTextString, dimP->u.value.stringValueP, sizeof(mlTextString)-1))
                {
                /* get the string from the multiline text item */

                memset (mlTextString, 0, sizeof(mlTextString));
                strncpy(mlTextString, dimP->u.value.stringValueP, sizeof(mlTextString)-1);

                /*---------------------------------------------------------------- 
                | Tell the dialog manager that the value for the item has
                | changed.  The valueChanged flag must be TRUE for the 
                | DITEM_MESSAGE_JOURNALSTATE message to be sent.
                +-----------------------------------------------------------------*/ 
                dimP->u.value.valueChanged = TRUE;

                /* tell the dialog manager that we have handled this event */
                dimP->u.value.hookHandled = TRUE;
                }

            break;
            }

        case DITEM_MESSAGE_JOURNALSTATE:
            {
            int     status, lineCount, i;
            char    **appMsgStringsPP;
            char    *appMsgString;

            /*------------------------------------------------------- 
            | Journal an application message during MACRO generation.
            | By default, the MACRO generator does not journal any 
            | actions to change the value of a multiline text item.
            | Therefore, this application must journal application
            | messages to change the value of the multiline text item.
            | 
            |   int     mdlSystem_journalAppMessage
            |   (
            |   char    *taskIdP,   => task id string (may be NULL)
            |   char    *msgP       => Message text, may contain newlines
            |   );
            |
            +------------------------------------------------------*/

            /* find the number of text lines in the multiline text item */
            lineCount = mdlText_countBufferStrings(dimP->u.value.stringValueP);
            /* if the text item does not contain any text then */
            if (0 == lineCount)
                {
                /* journal the application message */
                status = mdlSystem_journalAppMessage (mdlSystem_getCurrTaskID(),
                                                    "LineOne ");
                /*-------------------------------------------------
                | Set the hookHandled flag to TRUE to tell the 
                | macro generator that we have handled the event.
                +------------------------------------------------*/
                dimP->u.value.hookHandled = TRUE;

                return;
                }

            /* get an array of strings */
            status = mdlText_getStringArrayFromBuffer (&appMsgStringsPP,
                            dimP->u.value.stringValueP, lineCount);
            if (SUCCESS != status)
                {
                return;
                }
        
            /* allocate application message buffer - add space for message prefix */
            appMsgString = (char *)dlmSystem_mdlCalloc (1, strlen(appMsgStringsPP[0]) + 20);

            /* build the application message string */
            sprintf(appMsgString, "%s%s\0", "LineOne ", appMsgStringsPP[0]);

            /* for each line in the multiline text item */
            for (i = 0; i < lineCount; i++)
                {
                /* free the message buffer's memory area */
                status = mdlSystem_journalAppMessage (mdlSystem_getCurrTaskID(),
                                                    appMsgString);

                /* free the message buffer's memory area */
                dlmSystem_mdlFree (appMsgString);

                /* allocate application message buffer - add space for message prefix */
                appMsgString = (char*)dlmSystem_mdlCalloc (1, strlen(appMsgStringsPP[i+1]) + 20);

                /* build the application message string */
                sprintf(appMsgString, "%s%s\0", "LineNext ", appMsgStringsPP[i+1]);
                }

            /*---------------------------------------------- 
            |   Free the array of strings returned from 
            |   mdlText_getStringArrayFromBuffer 
            +----------------------------------------------*/
            mdlText_freeStringArray (appMsgStringsPP, lineCount);

            /* free the message buffer's memory area */
            dlmSystem_mdlFree (appMsgString);

            /*-------------------------------------------------
            | Set the hookHandled flag to TRUE to tell the 
            | macro generator that we have handled the event.
            +------------------------------------------------*/
            dimP->u.value.hookHandled = TRUE;

            break;
            };

        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    }

/*======================================================================+
|                                                                       |
|   Window Moving Example                                               |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          windowMoving_initTextItems                              |
|                                                                       |
| author        BSI                                 6/95                |
|                                                                       |
+----------------------------------------------------------------------*/
Private int     windowMoving_initTextItems
(
DialogBox       *dbP
)
    {
    int         itemIndex;
    char        message [128];
    BoolInt     flag;
    DialogItem  *topItemDiP, *bottomItemDiP;

    topItemDiP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_Text, 
                                                TEXTID_TopItem, 0);
    bottomItemDiP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_MultilineText, 
                                                MULTILINETEXTID_BottomItem, 0);

    if (topItemDiP == NULL || bottomItemDiP == NULL)
        return ERROR;

    /* set the value in the top text item */
    if (mdlResource_loadFromStringList
            (textItemVariable, NULL, STRINGID_V550NewMessages, MSGID_TopTxtItemMsg) != SUCCESS)
        {
        return  ERROR;
        }

    itemIndex = topItemDiP->itemIndex;

    if (mdlDialog_itemSetValue(&flag, 0, NULL, textItemVariable, dbP, itemIndex) 
                == TRUE)
        return  ERROR;

    /* set the value in the bottom multiline text item */
    if (mdlResource_loadFromStringList
            (message, NULL, STRINGID_V550NewMessages, MSGID_BottomTxtItemMsg) != SUCCESS)
        {
        return  ERROR;
        }

    itemIndex = bottomItemDiP->itemIndex;

    if (mdlDialog_itemSetValue(&flag, 0, NULL, message, dbP, itemIndex) 
                == TRUE)
        return  ERROR;


    return  SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          windowMoving_setEnabledState                            |
|                                                                       |
| author        BSI                                 11/94               |
|                                                                       |
| Enable or disable any items in the dialog which are displayed or not  |
| displayed according to the current dialog dimensions.                 |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    windowMoving_setEnabledState
(
DialogBox       *dbP,
int             newHeight
)
    {
    int         pos1, pos2, pos3;   /* vertical position of top, center, & bottom items*/

    BoolInt     centerEnabled;      /* Are items in center of dialog enabled? */
    BoolInt     bottomEnabled;      /* Are items in bottom of dialog enabled? */

    DialogItem  *centerItemDiP, *topItemDiP, *bottomItemDiP;
    
    /* Get item pointers & return if any can't be found */
    topItemDiP    = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_Text, TEXTID_TopItem, 0);
    centerItemDiP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_MultilineText, 
                                                MULTILINETEXTID_CenterItem, 0);
    bottomItemDiP = mdlDialog_itemGetByTypeAndId (dbP, RTYPE_MultilineText, 
                                                MULTILINETEXTID_BottomItem, 0);
    if (centerItemDiP == NULL || topItemDiP == NULL || bottomItemDiP == NULL)
        return;
    
    /* Calculate allowable size */
    pos1 = topItemDiP->rawItemP->itemRect.corner.y;
    pos2 = centerItemDiP->rawItemP->itemRect.corner.y;
    pos3 = bottomItemDiP->rawItemP->itemRect.corner.y;
    

    /* set flag to enable center & bottom dialog items */
    centerEnabled = bottomEnabled = TRUE;

    /* if bottom item(s) are not shown then disable them */
    if (newHeight < pos3)
        bottomEnabled = FALSE;

    /* if center item(s) are not shown then disable them */
    if (newHeight < pos2)
        centerEnabled = FALSE;

    /* enable or disable center item(s) */
    mdlDialog_itemSetEnabledState (dbP, centerItemDiP->itemIndex, centerEnabled, FALSE);
    
    /* enable or disable bottom item(s) */
    mdlDialog_itemSetEnabledState (dbP, bottomItemDiP->itemIndex, bottomEnabled, FALSE);

    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          windowMoving_calculateDockedExtents                     |
|                                                                       |
| author        BSI                                 06/95               |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    windowMoving_calculateDockedExtents
(
int             *dockWidthP,
int             *dockHeightP,
DialogBox       *dbP,
int             dockPosition
)
    {
    int         nFontIndex, nFontHeight, nFontWidth;
    BoolInt     useDefault=TRUE;

    /* Get the current text size in pixels */
    nFontIndex = mdlDialog_fontIndexGet (dbP);
    mdlDialog_fontGetInfo (NULL, &nFontWidth, NULL, &nFontHeight, dbP, nFontIndex);


    switch (dockPosition)
        {

        case DOCK_TOP:
            {
            DialogItem  *topItemDiP;

            /* get a pointer to the text item in the top portion of our dialog */
            topItemDiP = mdlDialog_itemGetByTypeAndId 
                            (dbP, RTYPE_Text, TEXTID_TopItem, 0);

            /* if the pointer is not NULL */
            if (NULL != topItemDiP)
                {
                /* set the docked height & width according to the size of this text item */

                *dockWidthP  = topItemDiP->rawItemP->itemRect.corner.x + nFontWidth*2;
                *dockHeightP = topItemDiP->rawItemP->itemRect.corner.y + nFontHeight/2;

                /* set flag to use the height & width that we just calculated */
                useDefault   = FALSE;
                }
            break;
            }

        /* we don't allow docking on the left or right. */
        case DOCK_BOTTOM:
        case DOCK_LEFT:
        case DOCK_RIGHT:
        default:
            {
            useDefault = TRUE;
            break;
            }

        }

    /* 
    | if we want to use the default "docked" size then just calculate
    | the dialog size
    */
    if (TRUE == useDefault)
        {
        BSIRect contentRect;

        mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) dbP);

        *dockWidthP  = mdlDialog_rectWidth (&contentRect);
        *dockHeightP = mdlDialog_rectHeight (&contentRect);
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          windowMoving_handleWindowMoving                         |
|                                                                       |
| author        BSI                                 6/95                |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    windowMoving_handleWindowMoving
(
DialogMessage   *dmP
)
    {
    int         pos1, pos2, pos3;
    int         nFontIndex, nFontHeight, nFontWidth;
    int         proposedHeight, oldHeight;
    DialogItem  *centerItemDiP, *topItemDiP, *bottomItemDiP;
    
    /* Get the current text size in pixels */
    nFontIndex = mdlDialog_fontIndexGet (dmP->db);
    mdlDialog_fontGetInfo (NULL, &nFontWidth, NULL, &nFontHeight, dmP->db, nFontIndex);

    /* Get item pointers & return if any can't be found */
    topItemDiP    = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_Text, TEXTID_TopItem, 0);
    centerItemDiP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_MultilineText, MULTILINETEXTID_CenterItem, 0);
    bottomItemDiP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_MultilineText, MULTILINETEXTID_BottomItem, 0);
    if (centerItemDiP == NULL || topItemDiP == NULL || bottomItemDiP == NULL)
        return;
    
    /* Calculate allowable sizes  -- add one to simulate mdlDialog_rectWidth*/
    pos1     = topItemDiP->rawItemP->itemRect.corner.y   + nFontHeight/2+1;
    pos2     = centerItemDiP->rawItemP->itemRect.corner.y     + nFontHeight/2+1;
    pos3     = bottomItemDiP->rawItemP->itemRect.corner.y  + nFontHeight/2+1;
    newXSize = topItemDiP->rawItemP->itemRect.corner.x   + nFontWidth*2+1;

    proposedHeight = dmP->u.windowMoving.newHeight;
    oldHeight      = mdlDialog_rectHeight(&dmP->u.windowMoving.oldContent);

    /* Find the new size of the dialog */
    if (proposedHeight < (pos1 + nFontHeight*2))
        newYSize = pos1;
    else if (proposedHeight < (pos2 + nFontHeight*2))
        newYSize = pos2;
    else
        newYSize = pos3;

    /*-------------------------------------------------------------------
    //  Tell the dialog manager what new size we want. 
    //  Since we allow only three sizes and there is little likelihood
    //  that the user will have picked a size exactly matching one of 
    //  our pre-set heights. Resetting the Height and Width in this 
    //  message takes the place of calling mdlWindow_extentSet (et. al).
    //  The dialog manager will reset the actual size of the dialog if
    //  handled is set to TRUE
    -------------------------------------------------------------------*/

    /* give Dialog Mgr the dialog's new dimensions */
    dmP->u.windowMoving.newWidth    = newXSize;
    dmP->u.windowMoving.newHeight   = newYSize;

    /* tell Dialog Mgr to use our dialog dimensions */
    dmP->u.windowMoving.handled     = TRUE;


    /*------------------------------------------------------------------- 
    //  Redraw dialog items for the following cases:
    //  1) new height is greater than old dialog height
    //
    //  2) dialog is shrunk by any corners
    -------------------------------------------------------------------*/

    if (newYSize > oldHeight)
        {
        needsUpdate = TRUE;
        }
    else if ((newYSize < oldHeight) &&
        ((dmP->u.windowMoving.whichCorners == CORNER_UPPERLEFT) ||
         (dmP->u.windowMoving.whichCorners == CORNER_LOWERLEFT) ||
         (dmP->u.windowMoving.whichCorners == CORNER_LOWERRIGHT) ||
         (dmP->u.windowMoving.whichCorners == CORNER_UPPERRIGHT)))
        {
        needsUpdate = TRUE;
        }
    else
        {
        needsUpdate = FALSE;
        }

    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          windowMoving_dialogHook                                 |
|                                                                       |
| author        BSI                                     6/95            |
|                                                                       |
+----------------------------------------------------------------------*/
Public void windowMoving_dialogHook
(
DialogMessage   *dmP
)
    {

    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
        {
        case DIALOG_MESSAGE_CREATE:
            {
            dmP->u.create.interests.mouses      = TRUE;

            /* We want DIALOG_MESSAGE_RESIZE dialog message */ 
            dmP->u.create.interests.resizes     = TRUE;

            dmP->u.create.interests.updates     = TRUE;

            /* We want DIALOG_MESSAGE_WINDOWMOVING dialog message */ 
            dmP->u.create.interests.windowMoving = TRUE;

            dmP->u.create.createFailed          = FALSE;
            
            break;
            }

        case DIALOG_MESSAGE_INIT:
            {

            /* put values in the multiline text items */
            if (SUCCESS != windowMoving_initTextItems (dmP->db)) 
                dmP->u.init.initFailed = TRUE;

            break;
            }

        case DIALOG_MESSAGE_RESIZE:
            {

            /*
            //  This message is sent to a dialog box hook function AFTER
            //  a move or size event occurs.  At this point, the application
            //  has processed the DIALOG_MESSAGE_WINDOWMOVING message and has
            //  set the size of the dialog box.  We just want to force a redraw
            //  of the dialog.
            */

            /* needsUpdate was set during WindowMoving handling */
            dmP->u.resize.forceCompleteRedraw = needsUpdate;

            needsUpdate = FALSE;
            break;
            }

       case DIALOG_MESSAGE_WINDOWMOVING:
            {

            /*
            //  This message is sent to a dialog box hook function BEFORE
            //  a move or size event occurs.  At this point, the application
            //  has the opportunity to change the size of the dialog box
            //  based on which corner is moving, the chosen size of the dialog, etc.
            */

            /* Don't process if only moving dialog box */
            if (CORNER_ALL          == dmP->u.windowMoving.whichCorners ||
                CORNER_ALL_RESIZED  == dmP->u.windowMoving.whichCorners)
                {
                break;
                }
                
            /* Don't process if minimizing the dialog box */
            if (1 == dmP->u.windowMoving.newHeight)
                break;

            windowMoving_handleWindowMoving (dmP);

            break;
            }

        case DIALOG_MESSAGE_UPDATE:
            {
            /*
            //  Message sent after the dialog manager has drawn the contents
            //  of the dialog box.  At this point we want to enable or disable
            //  our dialog items based on the new size of the dialog box.
            */
            windowMoving_setEnabledState (dmP->db, newYSize);

            break;
            }

        case DIALOG_MESSAGE_DOCKEDEXTENT:
            {
            int     dockWidth, dockHeight;

            /*
            //  The dialog manager sends the DIALOG_MESSAGE_DOCKEDEXTENT
            //  message to a dialog box when the user is attempting to dock
            //  the dialog box on the application window, and the dialog 
            //  manager wants to know the size of the dialog in the docked
            //  position.
            */

            /* calculate the dimensions of the dialog in "docked" position */
            windowMoving_calculateDockedExtents (&dockWidth, &dockHeight, 
                                               dmP->db, dmP->u.dockedExtent.dockPosition);

            /* set the dimensions of the dialog in the "docked" position */
            dmP->u.dockedExtent.dockExtent.x = dockWidth;
            dmP->u.dockedExtent.dockExtent.y = dockHeight;
/*-------------------------------------------------------------------------------**
**                 {                                                             **
**                 static int cnt;                                               **
**                                                                               **
**                 printf ("%5d. %d \n",cnt++,dmP->u.dockedExtent.dockPosition); **
**                 }                                                             **
**-------------------------------------------------------------------------------*/
            /* only allow the dialog to be docked on TOP and BOTTOM of application window */
            //DOCK_BOTTOM == dmP->u.dockedExtent.dockPosition)

            if (DOCK_TOP != dmP->u.dockedExtent.dockPosition) //||
                {
                /* tell Dialog Mgr that we've specified the dimensions of the dialog */
                //dmP->u.dockedExtent.extentFlag = DOCKEXTENT_SPECIFIED;
                dmP->u.dockedExtent.extentFlag = DOCKEXTENT_INVALIDREGION;
                printf ("nodock  %d \n",dmP->u.dockedExtent.dockPosition);

                dmP->u.dockedExtent.hookHandled = TRUE;
                }
            else
                {
                dmP->u.dockedExtent.extentFlag = DOCKEXTENT_SPECIFIED;
                printf ("docked %d \n",dmP->u.dockedExtent.dockPosition);

                //dmP->u.dockedExtent.extentFlag = DOCKEXTENT_INVALIDREGION;
                dmP->u.dockedExtent.hookHandled = TRUE;
                }

            break;
            }

        default:
            dmP->msgUnderstood = FALSE;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          windowMovingExample                                     |
|                                                                       |
| author        BSI                                     06/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    windowMovingExample
(
char            *unparsedP
)
//cmdNumber       CMD_DEMO_WINDOWMOVING
    {
    int         msgResponse;
    char        msgString[128];
    DialogBox   *dbP;

    /*-------------------------------------------------------------------
    //  Open the sample dialog box.  This example works like the "Key-in"
    //  dialog box in MicroStation PowerDraft & V5.5.  The dialog has
    //  three possible sizes.  One of the three possible sizes is chosen
    //  based on the size chosen by the user.
    //  The hook function on the dialog box handles the DIALOG_MESSAGE_WINDOWMOVING
    //  dialog message to determine the new size of the dialog box based on the 
    //  size that the user has chosen.
    //  The sample dialog may also be docked, just as the "Key-in" dialog.
    //  The application handles the DIALOG_MESSAGE_DOCKEDEXTENT dialog message
    //  to force the size of the dialog in the docked position to only included
    //  the first text item in the dialog box.
    //
    //  The sequence of events which occur during a dialog resize are:
    //  1)  Dialog hook gets the DIALOG_MESSAGE_WINDOWMOVING message and sizes
    //      the dialog to one of the possible dialog box sizes as defined by our
    //      application.
    //  2)  Dialog hook gets the DIALOG_MESSAGE_RESIZE message and forces
    //      a complete redraw of the dialog box.
    //  3)  Dialog hook gets the DIALOG_MESSAGE_UPDATE message and enables
    //      and disables items based on the new size of the dialog box.
    -------------------------------------------------------------------*/

    if ((dbP = mdlDialog_open (NULL, DIALOGID_WindowMoving)) == NULL)
        {
        /*--------------------------------------------------------------------
        Critical error message - stop sign icon
        -------------------------------------------------------------------- */
        mdlResource_loadFromStringList 
          (msgString, 0L, STRINGID_V550NewMessages, MSGID_WindowMovingOpenErr);

        /* ACTIONBUTTON_OK is the only possible return value */
        msgResponse = mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, 
                        MSGBOX_ICON_CRITICAL);
        }
    }

/*======================================================================+
|                                                                       |
|   Status Area Messages Example                                        |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          messagesDataPoint                                       |
|                                                                       |
| author        BSI                                     09/94           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    messagesDataPoint
(
Dpoint3d        *pointP,
int             view
)
    {
    static int  pointCounter;

    mdlOutput_rscPrintf (pointCounter%5, NULL, STRINGID_V550NewMessages, 
        MSGID_MessagesExample0 + pointCounter%5);

    pointCounter++;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          messagesExample                                         |
|                                                                       |
| author        BSI                                     09/94           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    messagesExample
(
char            *unparsedP
)
//cmdNumber       CMD_DEMO_MESSAGES
    {
    char        msgString[128];

    mdlResource_loadFromStringList 
        (msgString, 0L, STRINGID_V550NewMessages, MSGID_MessagesDirections);

    mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, MSGBOX_ICON_INFORMATION);

    mdlState_startPrimitive (messagesDataPoint, messagesExample, 0, 0);
    }

/*======================================================================+
|                                                                       |
|   Tool Box Example                                                    |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          toolBoxExample                                          |
|                                                                       |
| author        BSI                                     08/94           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    toolBoxExample
(
char            *unparsedP
)
//cmdNumber       CMD_DEMO_TOOLBOX
    {
    DialogBox   *dbP;
    char        msgString[256];
    int         msgResponse;

    /*  open a dialog box which contains a icon command frame with tool boxes */
    if ((dbP = mdlDialog_open (NULL, DIALOGID_SmartToolFrame)) == NULL)
        {
        /*--------------------------------------------------------------------
        Critical error message - stop sign icon
        -------------------------------------------------------------------- */
        mdlResource_loadFromStringList 
          (msgString, 0L, STRINGID_V550NewMessages, MSGID_ToolBoxOpenErr);

        /* ACTIONBUTTON_OK is the only possible return value */
        msgResponse = mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, 
                        MSGBOX_ICON_CRITICAL);
        }

    /* open a tool box */
    if ((dbP = mdlDialog_open (NULL, TOOLBOXID_SmartTools)) == NULL)
        {
        /*--------------------------------------------------------------------
        Critical error message - stop sign icon
        -------------------------------------------------------------------- */
        mdlResource_loadFromStringList 
          (msgString, 0L, STRINGID_V550NewMessages, MSGID_ToolBoxOpenErr);

        /* ACTIONBUTTON_OK is the only possible return value */
        msgResponse = mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, 
                        MSGBOX_ICON_CRITICAL);
        }
    

    /* see if the tool box was docked last time it was opened */
    if (dbP && (mdlWindow_getDocked ((GuiWindowP) dbP) != 0))
        {
        /* docking requires that the application area be reorganized */
        mdlWindow_organizeApplicationArea ();
        }
    }

/*======================================================================+
|                                                                       |
|   File Open Example                                                   |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          customButtonHook                                        |
|                                                                       |
| author        BSI                                       08/94         |
|                                                                       |
+----------------------------------------------------------------------*/
Public void     customButtonHook
(
DialogItemMessage   *dimP
)                           
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
        {
        case DITEM_MESSAGE_QUEUECOMMAND:
            {
            char    msgString[128];

            mdlResource_loadFromStringList 
                (msgString, 0L, STRINGID_V550NewMessages, MSGID_CustomHint);

            mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, MSGBOX_ICON_INFORMATION);
            }
            break;
    
        default:
            dimP->msgUnderstood = FALSE;
            break;
        }
    }
        
/*----------------------------------------------------------------------+
|                                                                       |
| name          fileOpenExample                                         |
|                                                                       |
| author        BSI                                     08/94           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    fileOpenExample
(
char            *unparsedP
)
//cmdNumber       CMD_DEMO_FILEOPEN
    {
    char            dialogTitle[128], filterString[256],
                    fileName[MAXFILELENGTH];
    FileOpenParams  fileOpenParams;
    FileOpenExtraInfo  extraInfoP;

    mdlResource_loadFromStringList 
        (dialogTitle, 0L, STRINGID_V550NewMessages, MSGID_FileOpenDialogTitle);

    mdlResource_loadFromStringList
        (filterString, 0L, STRINGID_V550NewMessages, MSGID_FileOpenFilterString);

    memset (&fileOpenParams, 0, sizeof(FileOpenParams));

    fileOpenParams.openCreate     = FILELISTATTR_OPEN | 
                                    FILEOPENEXTATTR_CENTERONSCREEN;

//    fileOpenParams.dialogId       = DIALOGID_V550NewFileOpen;
    fileOpenParams.defaultFilterP = "*.dgn;*.dwg;*.dxf";
    fileOpenParams.defaultDirP    = "MS_DEF";
    fileOpenParams.titleP         = dialogTitle;
    fileOpenParams.filterInfoStrP = filterString;
    //fileOpenParams.dialogOwnerMD  = mdlSystem_getCurrMdlDesc ();

    /* Set def file id so that design file history is populated in the menu */
    fileOpenParams.defFileId      = DEFDGNFILE_ID;

    if (mdlDialog_fileOpenExt (fileName, &extraInfoP, &fileOpenParams, 0) == SUCCESS)
        {
        mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, fileName, MSGBOX_ICON_INFORMATION);
        }
    }

/*======================================================================+
|                                                                       |
|   Message Box Example                                                 |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          messageBoxExample                                       |
|                                                                       |
| author        BSI                                     08/94           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    messageBoxExample
(
char            *unparsedP
)
//cmdNumber       CMD_DEMO_MSGBOX
    {
    char        msgString[256];
    int         msgResponse;

    /*  --------------------------------------------------------------------
        Yes/No/Cancel dialog - question mark icon 
        -------------------------------------------------------------------- */
    mdlResource_loadFromStringList 
        (msgString, 0L, STRINGID_V550NewMessages, MSGID_YesNoCancelQuestion);

    msgResponse = mdlDialog_openMessageBox (DIALOGID_MsgBoxYesNoCancel, msgString, 
                        MSGBOX_ICON_QUESTION);

    /* possible return values */
    switch (msgResponse)
        {
        case ACTIONBUTTON_YES:      break;
        case ACTIONBUTTON_NO:       break;
        case ACTIONBUTTON_CANCEL:   break;
        }

    /*  --------------------------------------------------------------------
        Information message - information "i" icon
        -------------------------------------------------------------------- */
    mdlResource_loadFromStringList 
        (msgString, 0L, STRINGID_V550NewMessages, MSGID_OKInformation);

    /* ACTIONBUTTON_OK is the only possible return value */
    msgResponse = mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, 
                        MSGBOX_ICON_INFORMATION);

    /*  --------------------------------------------------------------------
        Critical error message - stop sign icon
        -------------------------------------------------------------------- */
    mdlResource_loadFromStringList 
        (msgString, 0L, STRINGID_V550NewMessages, MSGID_OKCritical);

    /* ACTIONBUTTON_OK is the only possible return value */
    msgResponse = mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, 
                        MSGBOX_ICON_CRITICAL);

    /*  --------------------------------------------------------------------
        OK/Cancel dialog - exclamation point icon 
        -------------------------------------------------------------------- */
    mdlResource_loadFromStringList 
        (msgString, 0L, STRINGID_V550NewMessages, MSGID_OKCancelWarning);

    msgResponse = mdlDialog_openMessageBox (DIALOGID_MsgBoxOKCancel, msgString, 
                        MSGBOX_ICON_WARNING);

    /* possible return values */
    switch (msgResponse)
        {
        case ACTIONBUTTON_OK:      break;
        case ACTIONBUTTON_CANCEL:  break;
        }
    }

/*======================================================================+
|                                                                       |
|   Save Asynch Example                                                 |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          saveAsynchExample                                       |
|                                                                       |
| author        BSI                                     09/94           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    saveAsynchExample
(
BoolInt         afterSave,          /* => TRUE - after save has been completed */
BoolInt         intermediateSave    /* => TRUE - during a session, FALSE - on exit */
)
    {
    if (TRUE == intermediateSave)
        {
        /* if the saving of the file is complete */
        if (TRUE == afterSave)
            {
            /*------------------------------------------------- 
            Save occured during the session.  User chose
            "File->Save" or keyed-in "Save Design" command.
            -------------------------------------------------*/
            mdlOutput_rscPrintf(MSG_STATUS, NULL, STRINGID_V550NewMessages,
                                MSGID_CompletedSaveDuringSession);
            }
        }
    else
        {
        /* if save of the file is not complete */
        if (FALSE == afterSave)
            {
            /* saving changes at close of design file - before save */
            mdlOutput_rscPrintf(MSG_STATUS, NULL, STRINGID_V550NewMessages, 
                                MSGID_BeforeSaveOnExit);
            }
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          unloadFunction                                          |
|                                                                       |
| author        BSI                                     09/94           |
|                                                                       |
+----------------------------------------------------------------------*/
Private int     unloadFunction
(
int             unloadType
)
    {
     /* no need to clean up menus if shutting down */
    if (SYSTEM_TERMINATED_SHUTDOWN == unloadType)
        return (0);

    mdlDialog_menuBarDeleteCmdWinMenu (&gAppMenuItem);
    return (0);
    }

/*----------------------------------------------------------------------+
|                                                                       |
+----------------------------------------------------------------------*/
static DialogHookInfo  uHooks[] =
    {
    {HOOKITEMID_Button_V550NewCustom,   (PFDialogHook)customButtonHook},
    {HOOKID_WindowMoving,               (PFDialogHook)windowMoving_dialogHook},
    {HOOKITEMID_MultilineText,          (PFDialogHook)multiline_itemHook},
    };

/*----------------------------------------------------------------------+
|                                                                       |
| name          main                                                    |
|                                                                       |
| author        BSI                                     8/94            |
|                                                                       |
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    SymbolSet            *setP;
    RscFileHandle   rfHandle;

    mdlResource_openFile (&rfHandle, NULL, RSC_READ);

    /*------------------------------------------------------------------- 
    Must be running MicroStation PowerDraft, MicroStation Version 5.5, or
    MicroStation Review V5.5
    -------------------------------------------------------------------*/
    if (tcb->ustn_version < 0x550)
        {
        /* output an error message */
        mdlOutput_rscPrintf 
            (MSG_STATUS, NULL, STRINGID_V550NewMessages, MSGID_InvalidVersion);

        /* Queue a command to unload this application */
        mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
                                    mdlSystem_getCurrTaskID(), TRUE);

        return ERROR;
        }

    /* running MicroStation PowerDraft? */
    if (SUCCESS == mdlSystem_getCfgVarAtLevel 
                    (NULL, "_MSDRAFT", 0, CFGVAR_LEVEL_PREDEFINED))
        {
        mdlOutput_rscPrintf 
            (MSG_STATUS, NULL, STRINGID_V550NewMessages, MSGID_RunningPowerDraft);
        }
    /* running MicroStation MicroStation Review? */
    else if (SUCCESS == mdlSystem_getCfgVarAtLevel 
                    (NULL, "_MSREVIEW", 0, CFGVAR_LEVEL_PREDEFINED))
        {
        mdlOutput_rscPrintf 
            (MSG_STATUS, NULL, STRINGID_V550NewMessages, MSGID_RunningReview);
        }
    /* running MicroStation? */
    else if (SUCCESS == mdlSystem_getCfgVarAtLevel 
                    (NULL, "_MICROSTATION", 0, CFGVAR_LEVEL_PREDEFINED))
        {
        mdlOutput_rscPrintf 
            (MSG_STATUS, NULL, STRINGID_V550NewMessages, MSGID_RunningMicroStation);
        }

    /* publish the dialog item hooks */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    /* publish a variable for our text item */
    setP=mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);
    mdlDialog_publishBasicArray(setP, mdlCExpression_getType (TYPECODE_CHAR), "textItemVariable", textItemVariable,
                                    sizeof(textItemVariable));

    /* Load our command table */
    Private MdlCommandNumber  commandNumbers [] =
    {
    {messageBoxExample,CMD_DEMO_MSGBOX},
    {fileOpenExample,CMD_DEMO_FILEOPEN},
    {toolBoxExample,CMD_DEMO_TOOLBOX},
    {messagesExample,CMD_DEMO_MESSAGES},
    {windowMovingExample,CMD_DEMO_WINDOWMOVING},
    0,
    };
    
    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);


    if (NULL == mdlParse_loadCommandTable (NULL))
        mdlOutput_rscPrintf (MSG_ERROR, NULL, STRINGID_V550NewMessages, 
                            MSGID_ErrorLoadingCmdTable);

    /* The following asynch is new for PowerDraft */
    mdlSystem_setFunction (SYSTEM_FILE_SAVE,    saveAsynchExample);

    /* want to be notified at unload time to clean up our menu */
    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM, unloadFunction);

    /* add our commands to the application menu */
    mdlDialog_menuBarAddCmdWinMenu (&gAppMenuItem, PULLDOWNMENUID_V550New, TRUE);

    /* want to be notified when we get an APPLICATION_EVENT message */
    mdlInput_setFunction (INPUT_MESSAGE_RECEIVED, applMessageReceive);

    return  SUCCESS;
    }
