/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v550new/v550new.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   v550new.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:59 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   v550new.h  -- Application constants and definitions.    	    	|
|									|
|									|
+----------------------------------------------------------------------*/
#if !defined (__v550newH__)
#define       __v550newH__

/*----------------------------------------------------------------------+
|									|
|   Header File Dependencies						|
|	      								|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Defines								|
|									|
+----------------------------------------------------------------------*/
#define BASEID_V550New     5500

/*----------------------------------------------------------------------+
|									|
|   Dialog Box ID's							|
|									|
+----------------------------------------------------------------------*/
#define DIALOGID_V550NewFileOpen		(BASEID_V550New - 1)
#define DIALOGID_SmartToolFrame		    	(BASEID_V550New - 2)
#define DIALOGID_WindowMoving		    	(BASEID_V550New - 3)

/*----------------------------------------------------------------------+
|									|
|   Tool Box ID's							|
|									|
|	Note that these identifiers are also dialog box identifiers	|
| and therefore must be unique within the group of dialog box ids	|
| for this application.							|
+----------------------------------------------------------------------*/
#define TOOLBOXID_SmartTools			(BASEID_V550New - 4)
#define TOOLBOXID_Sample			(BASEID_V550New - 5)

/*----------------------------------------------------------------------+
|									|
|   Command Frame ID's							|
|									|
+----------------------------------------------------------------------*/
#define ICONCMDFRAMEID_MainSample		(BASEID_V550New - 6)

/*----------------------------------------------------------------------+
|									|
|   Icon Command ID's							|
|									|
+----------------------------------------------------------------------*/
#define ICONCMDID_PlaceMyLine		    	(BASEID_V550New - 1)

/*----------------------------------------------------------------------+
|									|
|   Text ID's								|
|									|
+----------------------------------------------------------------------*/
#define TEXTID_TopItem			    	(BASEID_V550New - 1)

/*----------------------------------------------------------------------+
|									|
|   Multiline Text ID's							|
|									|
+----------------------------------------------------------------------*/
#define MULTILINETEXTID_CenterItem	    	(BASEID_V550New - 1)
#define MULTILINETEXTID_BottomItem	    	(BASEID_V550New - 2)

/*----------------------------------------------------------------------+
|									|
|   Push Button ID's							|
|									|
+----------------------------------------------------------------------*/
#define PUSHBUTTONID_V550NewCustom	    	(BASEID_V550New - 1)

/*----------------------------------------------------------------------+
|									|
|   Option Button ID's							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_Button_V550NewCustom    	(BASEID_V550New - 1)
#define HOOKID_WindowMoving		    	(BASEID_V550New - 2)
#define HOOKITEMID_MultilineText		(BASEID_V550New - 3)

/*----------------------------------------------------------------------+
|									|
|   Hook Arguments 							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   List Box ID's							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Menu Bar ID's							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Pulldown Menu ID's							|
|									|
+----------------------------------------------------------------------*/
#define PULLDOWNMENUID_V550New		    	(BASEID_V550New - 1)

/*----------------------------------------------------------------------+
|									|
|   Menu Search ID's							|
|									|
+----------------------------------------------------------------------*/
#define MENUSEARCHID_DemoFileOpen	    	1
#define MENUSEARCHID_DemoMessageBoxes	    	2
#define MENUSEARCHID_DemoPrompts	    	3
#define MENUSEARCHID_DemoToolBox	    	4
#define MENUSEARCHID_DemoWindowMoving       	5

/*----------------------------------------------------------------------+
|									|
|   Label ID's								|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Message List IDs							|
|									|
+----------------------------------------------------------------------*/
#define STRINGID_V550NewMessages	    	(BASEID_V550New - 1)

/*----------------------------------------------------------------------+
|									|
|   Messages								|
|									|
+----------------------------------------------------------------------*/
#define MSGID_ErrorLoadingCmdTable	    1
#define MSGID_YesNoCancelQuestion	    2
#define MSGID_OKInformation		    3
#define MSGID_OKCritical		    4
#define MSGID_OKCancelWarning		    5
#define MSGID_FileOpenDialogTitle	    6
#define MSGID_FileOpenFilterString	    7
#define MSGID_CustomHint		    8
#define MSGID_ToolBoxOpenErr		    9
#define MSGID_RunningPowerDraft		    10
#define MSGID_MessagesDirections	    11
#define MSGID_MessagesExample0		    12
#define MSGID_MessagesExample1		    13
#define MSGID_MessagesExample2		    14
#define MSGID_MessagesExample3		    15
#define MSGID_MessagesExample4		    16
#define MSGID_InvalidVersion		    17
#define MSGID_RunningReview		    18
#define MSGID_RunningMicroStation	    19
#define MSGID_CenterTxtItemMsg		    20
#define MSGID_BottomTxtItemMsg		    21
#define MSGID_TopTxtItemMsg		    22
#define MSGID_WindowMovingOpenErr	    23
#define MSGID_CompletedSaveDuringSession    24
#define MSGID_BeforeSaveOnExit		    25

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Macros								|
|									|
+----------------------------------------------------------------------*/

#endif	/* !defined (__v550newH__) */
