/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v550new/v550new.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   v550new.r  - Application resource definitions.		    	|
|									|
|   $Workfile:   v550new.r  $
|   $Revision: 1.4.52.1 $
|      	$Date: 2013/07/01 20:40:59 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */
#include <cmdlist.h>
#include <deffiles.h>

#include <drop.ids>
#include <match.ids> 
#include <smrtline.ids> 
#include <pselect.ids>

#include "v550new.h"
#include "v550txt.h"
#include "v550cmd.h"

/*======================================================================+
|									|
|   Dialog Box Resource Specifications					|
|									|
+======================================================================*/
#undef	XC
#define	XC (DCOORD_RESOLUTION/2) * ASPECT_V550New

#define COL_GAP     (2*XC)			/* gap between columns */
#define LST_W	    (15*XC)			/* list box width      */
#define BUT_W	    (BUTTON_STDWIDTH + 2*XC)    /* button width	       */
#define TXT_W	    (18*XC)			/* text field width    */
#define SBR_W       (3*XC)			/* scroll bar width    */
#define ADJ	    (0.35*XC)			/* list x adjustment   */
#define TYX_W	    (12*XC)			/* type extra width    */
#define TYO_W       (TXT_W + TYX_W)		/* type option width   */

#define COL1	    (COL_GAP)				/* column 1 x  */
#define COL2	    (COL1 + LST_W + COL_GAP + SBR_W)    /* column 2 x  */
#define COL3	    (COL2 + LST_W + 10*XC + SBR_W + XC) /* column 3 x  */

#define DLG_W	    (COL3 + BUT_W  + XC)	/* dialog width	       */
#define DRO_X	    (COL2 + TYX_W + 2*XC)	/* drives option x     */
#define DRO_W       (12*XC)			/* drives option width */
#define DIR_W       (TXT_W + 10*XC)		/* dir label width     */

#define LBL_Y       (1.75*YC)			/* label y	       */
#define TXT_Y       (3.00*YC)			/* text y	       */
#define	LST_Y       (TXT_Y + GENY(2))		/* list box y          */
#define OPT_Y       (LST_Y + GENY(11.5))	/* option button y     */
#define BY1	    (TXT_Y)			/* button 1 y origin   */
#define BY2	    (BY1 + 2.5*YC)		/* button 2	       */
#define BY3	    (BY2 + 2.5*YC)		/* button 3	       */
#define BY4	    (BY3 + 2.5*YC)		/* button 4	       */
#define DLG_H	    (OPT_Y + GENY(2))		/* dialog height       */
#define OFF_Y       (2*DLG_H)			/* move off the dialog */

/*----------------------------------------------------------------------+
|									|
|   File Open Dialog Box						|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_V550NewFileOpen =
    {
    DIALOGATTR_FILEOPENCOMMON,
    DLG_W, DLG_H,
    NOHELP, MHELPTOPIC,
    NOHOOK, NOPARENTID,
    "",
{
{{0,0, 0,0},		      MenuBar,	    MENUBARID_GraphOpen, ON, 0, "", ""},
{{COL1,     TXT_Y, TXT_W, 0}, Text,	    TEXTID_FileOpenFileName, ON, TEXT_MASK_LABELABOVE, TXT_FileTextLabel, ""},
{{0,        OFF_Y, TXT_W, 0}, Text,         TEXTID_FileOpenFilter,		   ON, 0, "\0", ""},
#if defined (SF_DIR_OPTBTN_SUPPORT)
{{COL2,     TXT_Y, DIR_W, 0}, OptionButton, OPTIONBUTTONID_FileOpenDirs,	   ON, 0, "", ""},
#else
{{COL2,     TXT_Y, DIR_W, 0}, Text,	    TEXTID_FileOpenDirLabel,		   ON, 0, "",   ""},
#endif
{{COL1-ADJ, LST_Y, 0,     0}, ListBox,      LISTID_FileOpenExtFiles,		   ON, 0, "\0", ""},
{{COL2-ADJ, LST_Y, 0,     0}, ListBox,      LISTID_FileOpenExtDirs,		   ON, 0, "\0", ""},
{{COL3,     BY2,   BUT_W, 0}, PushButton,   PUSHBUTTONID_Cancel,		   ON, 0, "",   ""},
{{COL3,     BY1,   BUT_W, 0}, PushButton,   PUSHBUTTONID_FileListOK,		   ON, 0, "",   ""},
{{COL3,     BY3,   BUT_W, 0}, PushButton,   PUSHBUTTONID_Help,			   OFF, 0, "",  ""},
{{COL1,     OPT_Y, TYO_W, 0}, OptionButton, OPTIONBUTTONID_FileOpenTypeFilters,    ON, 0, "",   ""},
{{DRO_X,    OPT_Y, DRO_W, 0}, ComboBox,     COMBOBOXID_FileListDrivesEx,	   ON, 0, "",   ""},
{{COL2,     LBL_Y, 0,     0}, Label,	    0, ON, ALIGN_LEFT, TXT_DirectoriesLabel,          ""},

/* additional custom items */
{{COL3,     BY4,   BUT_W, 0}, PushButton,   PUSHBUTTONID_V550NewCustom,	   ON, 0, "",  ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   Window Moving Example Dialog Box					|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_WindowMoving =
    {
    DIALOGATTR_DEFAULT  | DIALOGATTR_NORIGHTICONS | DIALOGATTR_AUTOOPEN |
    DIALOGATTR_DOCKABLE | DIALOGATTR_BOTHVIRTUAL  | DIALOGATTR_GROWABLE,
    46 * XC + 2, 16 * YC + 10,
    NOHELP,    MHELP,
    HOOKID_WindowMoving,
    NOPARENTID,
    TXT_WindowMovingDialogTitle,
{
{{2*XC, 7, 27*XC, 0}, Label, 0, ON, 0, TXT_TopTextItemLabel, ""},
{{2*XC, 2*YC, 33*XC, 0}, Text, TEXTID_TopItem, ON, 0, "", ""},
{{2*XC, 4*YC+5, 30*XC, 0}, Label, 0, ON, 0, TXT_CenterMlTextItemLabel, ""},
{{2*XC, 5*YC+8, 30*XC, 5*YC+4}, MLText, MULTILINETEXTID_CenterItem, ON,
		     0, "", ""},
{{2*XC, 10*YC+5,30*XC, 0}, Label, 0, ON, 0, TXT_BottomMlTextItemLabel, ""},
{{2*XC, 11*YC+8,30*XC, 3*YC+9}, MLText, MULTILINETEXTID_BottomItem, ON,
		     0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   Example Command Frame with tool boxes			    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Main Palette Dialog Box - This definition of the palette dialog 	|
|	box is the same as a standard dialog box, except there is no    |
|	location specification for the only element in it, the 		|
|	IconCmdFrameX. 							|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_SmartToolFrame = 
    {
    DIALOGATTR_DEFAULT  | DIALOGATTR_NORIGHTICONS | DIALOGATTR_AUTOOPEN |
    DIALOGATTR_DOCKABLE | DIALOGATTR_BOTHVIRTUAL,
    0, 0,
    NOHELP, MHELP, 
    NOHOOK, NOPARENTID, 
    TXT_SampleFrame,
{
{{0, 0, 0, 0}, IconCmdFrameX, ICONCMDFRAMEID_MainSample, ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   MainSample Icon Cmd Frame - Here, the ToolBoxes 			|
|	are included in the frame.  The dimension of the actual palette |
|	is given here in terms of number of icons.  This one is a 	|
|	1X2 - 1 icons wide by 2 icons tall.  In most cases, these will  |
|	be 2 columns wide by hopefully no more than 10 rows.  The Main  |
|	uStn tool palette is probably the biggest you would ever want   |
|	to make a palette, so that it can all be seen on a Hercules	|
|	or EGA screen.							|
|									|
+----------------------------------------------------------------------*/
DItem_IconCmdFrameXRsc ICONCMDFRAMEID_MainSample =
    {
    2, 2, NOHELP, MHELP, 0, TXT_SampleFrame,
	{
 	{ToolBox,	    TOOLBOXID_Sample,       	"V550NEW"},
 	{ToolBox,	    TOOLBOXID_Polygons,     	MTASKID},
 	{ToolBox,	    TOOLBOXID_Ellipses,		MTASKID},
 	{ToolBox,	    TOOLBOXID_Tags, 		MTASKID},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   Tool Box resources						    	|
|									|
|   A tool box is defined by creating a dialog box resource and tool 	|
|   box resource which have the same identifier.			|
|									|
|   The dialog box resource contains only the ToolBox item.		|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc TOOLBOXID_Sample = 
    {
    DIALOGATTR_TOOLBOXCOMMON,
    0, 0,
    NOHELP, MHELP, 
    NOHOOK, NOPARENTID, 
    "",
{
{{ 0, 0, 0, 0}, ToolBox, TOOLBOXID_Sample, ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   The following sample toolbox contains 4 icon commands.  The first, 	|
|   second, and fourth icon commands are part of PowerDraft or      	|
|   MicroStation, and the third icon command is part of the this    	|
|   application.						    	|
|   Note that the auxilary info field (the last parameter) is NULL for	|
|   PowerDraft's or MicroStation's icon commands, but contains the  	|
|   string: "owner=\"V550NEW\"" for the icon command which belongs to	|
|   our application.						    	|
|									|
+----------------------------------------------------------------------*/
DItem_ToolBoxRsc TOOLBOXID_Sample =
    {
    NOHELP, MHELPTOPIC, NOHOOK, NOARG, 0, TXT_SampleToolBoxTitle,
{
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_ChooseElement, ON, 0, "", "owner=\"PSELECT\""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_50PlaceText,   ON, 0, "", ""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_PlaceMyLine,   ON, 0, "", "owner=\"V550NEW\""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_DeleteElement, ON, 1, "", ""},
}
    };

DialogBoxRsc TOOLBOXID_SmartTools = 
    {
    DIALOGATTR_TOOLBOXCOMMON,
    0, 0,
    NOHELP, MHELP, 
    NOHOOK, NOPARENTID, 
    "",
{
{{ 0, 0, 0, 0}, ToolBox, TOOLBOXID_SmartTools, ON, 0, "", ""},
}
    };

DItem_ToolBoxRsc TOOLBOXID_SmartTools = 
    {
    NOHELP, MHELPTOPIC, NOHOOK, NOARG, 0, TXT_SmartToolsTitle,
{
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_SmartLine,     ON, 0, "", "owner=\"SMRTLINE\""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_DropElement,   ON, 0, "", "owner=\"DROP\""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_MatchElement,  ON, 0, "", "owner=\"MATCH\""},
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_DeleteElement, ON, 1, "", ""},
}
    };

/*======================================================================+
|									|
|   Item Resource Specifications					|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
|   List Box Resources							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Text Item Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_TopItem =
    {
    NOCMD,
    LCMD,
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    30,
    "%s",
    "%s",
    "",
    "",
    NOMASK,
    TEXT_NOCONCAT,
    "",
    "textItemVariable"
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Multiline Text Item Resources                                       |
|                                                                       |
+----------------------------------------------------------------------*/
DItem_MultilineTextRsc MULTILINETEXTID_CenterItem =
    {
    NOSYNONYM,
    NOHELP,
    LHELP,
    HOOKITEMID_MultilineText,
    NOARG,
    MLTEXTATTR_NODISPLAYNLCHAR,
    3,
    ""
    };

DItem_MultilineTextRsc MULTILINETEXTID_BottomItem =
    {
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    MLTEXTATTR_NODISPLAYNLCHAR,
    3,
    ""
    };

/*----------------------------------------------------------------------+
|									|
|   Option Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   PushButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_V550NewCustom =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP,
    HOOKITEMID_Button_V550NewCustom, NOARG, NOCMD, LCMD, "",
    TXT_V550NewCustomize
    };

/*----------------------------------------------------------------------+
|									|
|   Toggle Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Menu Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_PulldownMenuRsc PULLDOWNMENUID_V550New =
    {
    NOHELP, MHELPTASKIDTOPIC,
    NOHOOK,
    ON, TXT_V550NewAppTitle,
{
{TXT_DemoFileOpen,  	NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	MENUSEARCHID_DemoFileOpen,
			CMD_DEMO_FILEOPEN, OTASKID, ""},

{TXT_DemoMessageBoxes, 	NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	MENUSEARCHID_DemoMessageBoxes,
			CMD_DEMO_MSGBOX, OTASKID, ""},

{TXT_DemoPrompts,  	NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	MENUSEARCHID_DemoPrompts,
			CMD_DEMO_MESSAGES, OTASKID, ""},

{TXT_DemoToolBox,  	NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	MENUSEARCHID_DemoToolBox,
			CMD_DEMO_TOOLBOX, OTASKID, ""},

{TXT_DemoWindowMoving,  NOACCEL, ON, NOMARK, 0, NOSUBMENU, 
		    	NOHELP, MHELPTASKIDTOPIC, NOHOOK, 
		    	MENUSEARCHID_DemoWindowMoving,
			CMD_DEMO_WINDOWMOVING, OTASKID, ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   Synonym List Resources						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Icon Cmd Resources                                                  |
|									|
|   Note the use of extended attributes to implement flyover and    	|
|   balloon help.						    	|
|									|
+----------------------------------------------------------------------*/
DItem_IconCmdRsc ICONCMDID_PlaceMyLine =
    {
    NOHELP, OHELPTASKIDCMD, 0,
    CMD_PLACE_LINE, MTASKID, "", "",
	{
	}
    }
    extendedAttributes
    {{

    /* Tool Description/Flyover Help appears in status area */
    {EXTATTR_FLYTEXT, TXT_Flyover_PlaceMyLine},     

    /* Tool Tip/Balloon Help appears in yellow text box near icon command */
    {EXTATTR_BALLOON, TXT_Balloon_PlaceMyLine},     
    }};

/*----------------------------------------------------------------------+
|									|
|  Icon Resources                                                       |
|									|
+----------------------------------------------------------------------*/
IconCmdSmallRsc ICONCMDID_PlaceMyLine =
    {
    23, 23, FORMAT_MONOBITMAP, BLACK_INDEX, TXT_PlaceMyLine,
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x08, 0x00, 0x00, 0x20, 0x00, 0x00, 0x80, 0x00,
	0x02, 0x00, 0x00, 0x08, 0x00, 0x00, 0x20, 0x00,
	0x00, 0x80, 0x00, 0x02, 0x00, 0x00, 0x08, 0x00,
	0x00, 0x20, 0x00, 0x00, 0x80, 0x00, 0x02, 0x00,
	0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00,
	}
    };

IconCmdLargeRsc ICONCMDID_PlaceMyLine =
    {
    31, 31, FORMAT_MONOBITMAP, BLACK_INDEX, TXT_PlaceMyLine,
	{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00,
	0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x80, 0x00,
	0x00, 0x02, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00,
	0x00, 0x20, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00,
	0x02, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00,
	0x20, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x02,
	0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x20,
	0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x02, 0x00,
	0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x20, 0x00,
	0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00,
	}
    };



