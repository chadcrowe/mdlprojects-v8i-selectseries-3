/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/NavSeperate/hilite.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
#include    "hilite.h"

/*----------------------------------------------------------------------+
|									|
|   Local function declarations 					|
|									|
+----------------------------------------------------------------------*/

/*ff Major Public Code Section */
/*----------------------------------------------------------------------+
|									|
|   Major Public Code Section						|
|									|
+----------------------------------------------------------------------*/
Public void HiliteElement (DgnModelRefP modelRefP, ElementID elID)
    {
      
       DisplayPathP path;
       ElementRef elemRef, cmplxHdrElRef;
       ElementHiliteState newHiliteState = HILITED_Normal;
       BoolInt bIsComplex;
       

       // Validate input
       if (!elID) return;
       //not in keeping with the company standard.  should mandate sending in the modelref.
       // Use modelRef provided or active modelref
       if (NULL == modelRefP)
           return;

       // Create elemRef from element id
       elemRef = dgnCache_findElemByID(mdlModelRef_getCache(modelRefP),elID);
	
       // Is elemRef a simple element or part of a complex element?
       bIsComplex = elementRef_isComplexComponent (elemRef);
       
       // Create elemRef display path
       path = mdlDisplayPath_new (elemRef, modelRefP);

       // Assign elemRef to either simple element or component
       if (bIsComplex)
             {
             cmplxHdrElRef = mdlDisplayPath_getElem (path, 0);
             elemRef = cmplxHdrElRef;
             newHiliteState = HILITED_ComponentsOnly;
             }

       // Set hilite mode and update view with new display path
       mdlDisplayPath_drawInViews (path, 0xffff, DRAW_MODE_Hilite, DRAW_PURPOSE_Hilite);
	   UInt32 filePos = mdlDisplayPath_getCursorFilePos (path);
	   mdlDisplayPath_release (path);

       }

/*----------------------------------------------------------------------+*//**
* Process the selection of elements.                                    *
*                                                                       *
* @bsimethod modElm                                                     *                                               
* @param    *elP,       <=> element to be modified                      *
* @param    *params,      => user parameter                             *
* @param    pModelRef,        => Model ref for current element          *
* @param  *elmDscrP,    => element descr for element                    *
* @param  **newDscrPP   <= if replacing entire descr                    *
* @return       MODIFY_STATUS_ see documentation on mdlModify functions *
*                                                                       *
* Author:   BSI                                 02/01           *
*                                                                       *
+----------------------------------------------------------------------*/
Public  int modElmInfo
(
MSElementUnion  *elP,       /* <=> element to be modified       */
void            *params,    /*  => user parameter               */
DgnModelRefP    pModelRef,    /*  => Model ref for current element      */
MSElementDescr  *elmDscrP,  /*  => element descr for element    */
MSElementDescr  **newDscrPP /*  <= if replacing entire descr    */
)
    {
   
    HiliteElement (pModelRef, mdlElement_getID(elP));

    return  MODIFY_STATUS_NOCHANGE;
    }