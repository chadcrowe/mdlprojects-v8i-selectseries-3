/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/NavSeperate/NavSeperate.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   NavSeperate  $
|   $Revision: 1.1.12.1 $
|       $Date: 2013/07/01 20:35:36 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   NavSeperate - NavSeperate source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Function -                                                          |
|                                                                       |
|   NavSeperate.cpp -- Example of element process and coedge hilite     |
|                                                                       |
|                                                                       |
|   This example application shows how to use IElementGraphicsProcessor |
|   to iterate elements and hilite elements with coedges.               |
|   The application allows user to choose either hilite element or      |
|   coedges.                        |
|   It also demonstrate how to report data with different type of       |
|   elements.                       |
|                                                                       |
|       - - - - - - - - - - - - - - - - - - - - - - - - - - - - -       |
|                                                                       |
|   Public Routine Summary -                                                            |
|                                                                       |
|   imodelvisitor_processElements - Check each element whether it has   |
|       coedge, hilite those with coedges                               |
|       NavSeperateLocateCmdSolid - Report edge data in selected element    | 
|       NavSeperateLocateCmdGparray - Report point data in selected         |
|       element                     |
|       main - main entry point                                                                 |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <mselems.h>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <msstrlst.h>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>

#include    <msrmgr.h>
#include    <mssystem.fdf>                                                        
#include    <msparse.fdf>
#include    <interface\ElemHandle.h>
#include <interface\element\DisplayHandler.h> 
#include        <toolsubs.h>

//scan code
#include        <elementref.h>
//locate code
#include        <msdependency.fdf>
#include        <msassoc.fdf>
#include        <msmisc.fdf>
#include        <mslocate.fdf>
#include        <msstate.fdf>
#include        <msoutput.fdf>
#include <mskisolid.h>
#include <mskisolid.fdf>

#include <GPArray.h>
//Locate command
#include        <mstmatrx.fdf>

#include "NavSeperateCmd.h"
#include "NavSeperate.h"
#include "hilite.h"
#include "MyElementGraphicsProcessor.h"
Reporter* g_reporter;

USING_NAMESPACE_BENTLEY
USING_NAMESPACE_BENTLEY_USTN
USING_NAMESPACE_BENTLEY_USTN_ELEMENT

//set this to handle dgn or just i-models  true all false only i-model.
static bool g_bProcessAll = true;


/*----------------------------------------------------------------------+*//**
* Process Fence Selection                                               *
* @bsimethod fenceModElm                                                * 
* @param        arg     user arguments to pass into this function.      *
* @return       SUCCESS                          *
*                                                                       *
* Author:   BSI                                 02/01           *
*                                                                       *
+----------------------------------------------------------------------*/
Public  int fenceModElmInfo
    (
    void      *arg
    )
    {
    UInt32           filePos;
    DgnModelRefP    currFileP;
    MSElementDescr  *edP = NULL;

    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &currFileP);

    UInt32 actPos;
    mdlElmdscr_read (&edP,filePos,currFileP,TRUE,&actPos);

    DisplayPathP path;
    path = mdlLocate_getCurrPath();

    KIBODY* bodyP;
    KIENTITY_LIST *listP= NULL;
    int status;

    status = mdlKISolid_elementToBodyList(&listP,NULL,NULL,NULL,edP,currFileP,filePos,TRUE,TRUE,TRUE);

    for (int i =0;SUCCESS== mdlKISolid_listNthEl (&bodyP,listP,i);++i)
        {
        g_reporter->ReportSolid (L"Solid",bodyP,g_reporter->GetRootNodePtr());
        MSElement       el;
        mdlModify_elementSingle (currFileP, filePos, MODIFY_REQUEST_NOHEADERS,
                MODIFY_ORIG, modElmInfo, NULL, 0L);
        mdlElement_read (&el,currFileP,filePos);
        }

    mdlKISolid_freeBody(bodyP);
    mdlElmdscr_freeAll (&edP);


    return SUCCESS;
}
/*---------------------------------------------------------------------------------**//**
* @description  the function that is called when the user hits a data point to confirm the selected element(s)
* @param        pntP      The data point that the user picked to accept the element location.
* @param        view      The view number that the selected point is in.
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
Private void     acceptElmSolid
    (
    Dpoint3d    *pntP,
    int         view

    )
    {
    UInt32          filePos;
    DgnModelRefP    refModelP;

    /*------------------------------------------------------------------
    | The located element was accepted using a data point so get the
    | accepted element and reference file info.  Our testLocate_ElmFilter
    function
    | has already verified that the element is not already in the active
    model.
    +------------------------------------------------------------------*/

    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &refModelP);

    MSElementDescr* edP = NULL;

    UInt32 actPos;

    mdlElmdscr_read (&edP,filePos,refModelP,TRUE,&actPos);

    KIBODY* bodyP;
    KIENTITY_LIST *listP= NULL;
    KIENTITY_LIST *TransForms=NULL;
    int status;
    int count;

    DisplayPathP path;
    path = mdlLocate_getCurrPath();

    mdlKISolid_listCreate (&listP);
    mdlKISolid_listCreate (&TransForms);
    status = mdlKISolid_elementToBodyList(&listP,NULL,&TransForms,NULL,edP,refModelP,filePos,TRUE,TRUE,TRUE);
    status = mdlKISolid_listCount (&count,listP);
    for (int i =0;SUCCESS== mdlKISolid_listNthEl (&bodyP,listP,i);++i)
        {
        g_reporter->ReportSolid (L"Solid",bodyP,g_reporter->GetRootNodePtr());
        if ((true == g_reporter->m_haveCoedge))
            {
            mdlModify_elementSingle (refModelP, filePos, MODIFY_REQUEST_HEADERS,
                            MODIFY_ORIG, modElmInfo, NULL, TRUE);

            g_reporter->m_haveCoedge = false;
            }
        mdlKISolid_freeBody(bodyP);
    }

    mdlElmdscr_freeAll (&edP);

    mdlKISolid_listDelete (&listP);
    mdlKISolid_listDelete (&TransForms);
    // mdlKISolid_endCurrTrans();
    /* Restart the locate logic */

    mdlLocate_restart (FALSE);
}

/*---------------------------------------------------------------------------------**//**
* @description  the function that is called when the user hits a data point to confirm the selected element(s)
* @param        pntP      The data point that the user picked to accept the element location.
* @param        view      The view number that the selected point is in.
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
Private void     acceptElmGparray
    (
    Dpoint3d    *pntP,
    int         view
    )
    {
    UInt32          filePos;
    DgnModelRefP    refModelP;
    MSElementDescr* edP = NULL;
    UInt32 actPos;
    int fillGpaStatus;

    /*------------------------------------------------------------------
    | The located element was accepted using a data point so get the
    | accepted element and reference file info.  Our testLocate_ElmFilter
    function
    | has already verified that the element is not already in the active
    model.
    +------------------------------------------------------------------*/

    filePos = mdlElement_getFilePos (FILEPOS_CURRENT, &refModelP);

    mdlElmdscr_read (&edP,filePos,refModelP,TRUE,&actPos);
    ElemHandle eh(edP, false, false);

    for (ChildElemIter child(eh,EXPOSECHILDREN_Count, false);child.IsValid();child= child.ToNext())
        {
        Handler& baseHandler = child.GetHandler();
        GPArrayP pArray=GPArray::Grab ();

        if (NULL!=baseHandler.GetDisplayHandler())      
            fillGpaStatus = baseHandler.GetDisplayHandler()->FillGPA (child, pArray); 

        if (fillGpaStatus == SUCCESS)
                g_reporter->ReportGPArray (L"GPA",pArray,g_reporter->GetRootNodePtr());

        pArray->Drop();
        }

    mdlElmdscr_freeAll (&edP);
    /* Restart the locate logic */
    mdlLocate_restart (FALSE);
    }
/*----------------------------------------------------------------------+*//**
* Set the search type mask to lines, text, and shapes only              *
* @bsimethod setElmSearchTypeInfo                                       *
* Author:   BSI                                 02/01                   *
+----------------------------------------------------------------------*/
Public  void setElmSearchTypeInfo(void)
    {//this will only look for lines, text, and shapes.
    static int  searchType[] = {SOLID_ELM, TEXT_ELM,SHAPE_ELM};

        /* Clear search criteria */
        /* Alternate ways of setting the search criteria. */
        //mdlLocate_noElemAllowLocked ();
        //mdlLocate_allowLocked ();
        /* Set search mask to look for text and line elements */
    mdlLocate_setElemSearchMask (sizeof(searchType)/sizeof(int), searchType);
    }

/*---------------------------------------------------------------------------------**//**
* @description  NavSeperateProcessElements
* @param     unparsed      Contination string that started this command. 
*                         "edge" to hilite coedges only, otherwise to hilite whole elemnt
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void NavSeperateProcessElements
    (
    char * unparsed
    )
    {
    Reporter::log_printf (0,"NavSeperateProcessElements\n");

    ISessionMgrR    pSessionMgr=ISessionMgr::GetManager();
    DgnModelRefP pModel = pSessionMgr.GetActiveModel();
    DgnCacheP pCache = pModel->GetDgnCache();
    MSDgnFileP      pFile;
    pFile = pModel->GetDgnFile();
    UInt32 count = pCache->GetElementCount (DGNCACHE_SECTION_GRAPHIC_ELMS);
    DgnElmListIterator iter;
    int elCounter=0;
    WString   rootName("_");
    WString schemaName;

    Reporter  reporter(const_cast<wchar_t*>(rootName.c_str()),pFile,true,false,false,false);
    MyElementGraphicsProcessor pMyProcessor (reporter,reporter.GetRootNodePtr());

    if (0==strcmpi("edge", unparsed))
            pMyProcessor.m_reporter.m_hiliteEdge = true;

    DgnFile::CacheIterator* modelCache = DgnFile::CacheIterator::Create(*pFile);

    //this should be changed to pass the top level models to the next function which will then 
    //traverse the modelref chain.
    if ((unparsed) && (strlen (unparsed)>0))
        schemaName = WString (unparsed);
    else if (g_bProcessAll)
        schemaName = WString ("*");

    ModelRefIteratorP  mrIterator;
    DgnModelRefP       modelP;
    mdlModelRefIterator_create (&mrIterator,pModel,MRITERATE_Root|MRITERATE_PrimaryChildRefs,-1);

    if (g_bProcessAll)
        while(NULL!=(modelP= mdlModelRefIterator_getNext(mrIterator)))
        {
        pCache = mdlModelRef_getCache(modelP);
        reporter.SetCurrentModelRef (modelP);

        if (pCache && (!pCache->IsFilled(DGNCACHE_SECTION_GRAPHIC_ELMS)))
            pCache->FillSections (DGNCACHE_SECTION_GRAPHIC_ELMS);
        if ((mdlModelRef_isActiveModel(modelP)) || ((mdlModelRef_isReference(modelP)) && (mdlModelRef_isDisplayedInView(modelP,ANY_VIEW))))
            for (CacheElemRef elRef = iter.FirstCacheElm(pCache->GetGraphicElms()); NULL != elRef; elRef = iter.NextCacheElm (false, false))
                {
                Reporter::log_printf (0,"element of type %ld, and ID = %I64u modelid = %S\n ",elRef->GetElemType(),elRef->GetElemID(), pCache->GetModelName());
                char dbgBuffer [512];
                sprintf (dbgBuffer,"element of type %ld, and ID = %I64u modelid = %S\n ",elRef->GetElemType(),elRef->GetElemID(), pCache->GetModelName());
                WString debugInfo = WString (dbgBuffer);
                reporter.DebugNode(debugInfo);
                ElemHandle eh = ElemHandle(elRef,pModel);
                BoolInt ishilite = elementRef_isHilited (elRef, pModel);

                if (FALSE==ishilite)
                    Bentley::Ustn::ElementGraphicsOutput::Process(eh,pMyProcessor);

                if ((true == pMyProcessor.m_reporter.m_haveCoedge) && (false == pMyProcessor.m_reporter.m_hiliteEdge))
                    {
                    HiliteElement (modelP, elRef->GetElemID());
                    pMyProcessor.m_reporter.m_haveCoedge = false;
                    }

                elCounter++;
                }
        }
    mdlModelRefIterator_free(&mrIterator);
}

/*----------------------------------------------------------------------+*//**
*                                                             
* @bsimethod NavSeperateLocateCmd                                     
* @param        unparsedP     Contination string that started this command
*                                                             
* Author:   BSI                                 06/03         
*                                                             
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT void NavSeperateLocateCmdSolid
    (
    char    *unparsed
    )
    {
    /* Setup search mask with the type of elements we are looking for */
    setElmSearchTypeInfo();

    MSWChar   nameP[512];
    DgnFileP  pFile = mdlModelRef_getDgnFile (ACTIVEMODEL);

    mdlModelRef_getModelName (ACTIVEMODEL,nameP);
    WString name(L"_");

    //create a new one.
    g_reporter = new Reporter(const_cast<wchar_t*>(name.c_str()),pFile,true,false,false,false);

    /* If the user wants to use a fence operation */
    if (0 != tcb->msToolSettings.general.useFence)
    {
        if (tcb->fence > 0)
            {
            mdlState_startFenceCommand(fenceModElmInfo, /* content func */
                            NULL,                    /* outline func */
                            NULL,                    /* data point func */
                            NavSeperateLocateCmdSolid,   /* reset func */
                            0,    /* prompt message */
                            0, /* command message */
                            FENCE_NO_CLIP);          /* clipping mode */
            }
        else
            {
                    tcb->msToolSettings.general.useFence = FALSE;
            }
    }
    else
        {
        mdlState_startModifyCommand (NavSeperateLocateCmdSolid, /* reset func */
                    acceptElmSolid,    /* datapoint func */
                    NULL,         /* dynamics func */
                    NULL,         /* show func */
                    NULL,         /* clean func */
                    0, /* command field message */
                    0,      /* prompt message */
                    TRUE,         /* use selection sets */
                    0);           /* points required for accept */
        }

    mdlOutput_rscPrintf (MSG_PROMPT, 0, 0,
            0);

    /* Start search at beginning of file */
    mdlAutoLocate_enable (TRUE,TRUE,TRUE);
    mdlAccuSnap_enableLocate (TRUE);
    mdlAccuSnap_enableSnap (TRUE);

    mdlLocate_init ();
}

/*----------------------------------------------------------------------+*//**
*                                                             
* @bsimethod NavSeperateLocateCmd                                     
* @param        unparsedP     Contination string that started this command
*                                                             
* Author:   BSI                                 06/03         
*                                                             
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT void NavSeperateLocateCmdGparray
    (
    char    *unparsed
    )
    {

    /* Setup search mask with the type of elements we are looking for */
    setElmSearchTypeInfo ();

    MSWChar   nameP[512];
    DgnFileP  pFile = mdlModelRef_getDgnFile (ACTIVEMODEL);

    mdlModelRef_getModelName (ACTIVEMODEL,nameP);
    WString name(L"_");
    g_reporter = new Reporter(const_cast<wchar_t*>(name.c_str()),pFile,true,false,false,false);

    /* If the user wants to use a fence operation */
    if (0 != tcb->msToolSettings.general.useFence)
        {
        if (tcb->fence > 0)
            {
            mdlState_startFenceCommand(fenceModElmInfo, /* content func */
                                NULL,                    /* outline func */
                                NULL,                    /* data point func */
                                NavSeperateLocateCmdGparray,   /* reset func */
                                0,    /* prompt message */
                                0, /* command message */
                                FENCE_NO_CLIP);          /* clipping mode */
            }
        else
            {
            tcb->msToolSettings.general.useFence = FALSE;
            }
        }
        else
            {

            mdlState_startModifyCommand (NavSeperateLocateCmdGparray, /* reset func */
                        acceptElmGparray,    /* datapoint func */
                        NULL,         /* dynamics func */
                        NULL,         /* show func */
                        NULL,         /* clean func */
                        0, /* command field message */
                        0,      /* prompt message */
                        TRUE,         /* use selection sets */
                        0);           /* points required for accept */

            }
        mdlOutput_rscPrintf (MSG_PROMPT, 0, 0,0);
        /* Start search at beginning of file */
        mdlAutoLocate_enable (TRUE,TRUE,TRUE);
        mdlAccuSnap_enableLocate (TRUE);
        mdlAccuSnap_enableSnap (TRUE);
        mdlLocate_init ();
}

/*---------------------------------------------------------------------------------**//**
* @description  MdlMain
* @param        argc      The number of command line parameters sent to the application.
* @param        argv[]    The array of strings sent to the application on the command line.
* @bsimethod                                                    BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT  int MdlMain
    (
    int         argc,
    char        *argv[]
    )
    {
    RscFileHandle   rfHandle;

    mdlResource_openFile (&rfHandle,NULL,RSC_READONLY);

    // Map command name to function (usage: MDL COMMAND COMPEXPORT)
    static  MdlCommandName cmdNames[] = 
        {
        {NavSeperateLocateCmdSolid, "NavSeperateLocateCmdSolid"  },
        {NavSeperateLocateCmdGparray, "NavSeperateLocateCmdGparray"  },
        {NavSeperateProcessElements, "NavSeperateprocessElements"},
        0,
        };

        mdlSystem_registerCommandNames (cmdNames);

        // Map key-in to function
    static MdlCommandNumber cmdNumbers[] =
        {
        {NavSeperateLocateCmdSolid,  CMD_NAVSEPERATE_LOCATE_SOLID },
        {NavSeperateLocateCmdGparray, CMD_NAVSEPERATE_LOCATE_GPARRAY},
        {NavSeperateProcessElements, CMD_NAVSEPERATE_PROCESS},
        0,
        };

    mdlSystem_registerCommandNumbers (cmdNumbers);

    mdlParse_loadCommandTable (NULL);

    return SUCCESS;
    }
