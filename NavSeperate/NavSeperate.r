/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/NavSeperate/NavSeperate.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Workfile:   NavSeperate  $
|   $Revision: 1.1.12.1 $
|       $Date: 2013/07/01 20:35:36 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   NavSeperate - NavSeperate source code. |
|                                                                       |
+----------------------------------------------------------------------*/
#include <rscdefs.h>


#define     DLLAPPID 1

/* associate app with dll */
DllMdlApp DLLAPPID = 
    {
    "NavSeperate", "NavSeperate"
    }

