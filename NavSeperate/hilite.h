/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/NavSeperate/hilite.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   hilite.hv  $
|   $Workfile:   hilite.h  $
|   $Revision: 1.1.12.1 $
|   	$Date: 2013/07/01 20:35:36 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <mselems.h>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <msstrlst.h>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>

#include    <msrmgr.h>
#include    <mssystem.fdf>
#include    <msparse.fdf>

#include	<toolsubs.h>

//scan code
#include	<elementref.h>
//locate code
#include	<msdependency.fdf>
#include	<msassoc.fdf>
#include	<msmisc.fdf>
#include	<mslocate.fdf>
#include	<msstate.fdf>
#include	<msoutput.fdf>
#include <mshitpath.h>
#include <msdisplaypath.h>
#include <msdgncache.h>
#include <msdgncache.fdf>
#include    <mskisolid.h>
#include    <mskisolid.fdf>
extern "C" void            elementRef_setHiliteState (ElementRef elRef, DgnModelRefP modelRef, ElementHiliteState newState);

//place command
#include	<mstmatrx.fdf>
/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/

Public void HiliteElement
(
DgnModelRefP modelRefP, 
ElementID elId
);



Public  int modElmInfo
(
MSElementUnion  *elP,       /* <=> element to be modified       */
void            *params,    /*  => user parameter               */
DgnModelRefP    pModelRef,    /*  => Model ref for current element      */
MSElementDescr  *elmDscrP,  /*  => element descr for element    */
MSElementDescr  **newDscrPP /*  <= if replacing entire descr    */
);