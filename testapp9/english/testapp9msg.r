/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp9/english/testapp9msg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp9/english/testapp9msg.r_v  $
|   $Workfile:   testapp9msg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	testapp9 application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "testapp9.h"	/* testapp9 dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_testapp9Errors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_testapp9Dialog,   "Unable to open testapp9 dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
