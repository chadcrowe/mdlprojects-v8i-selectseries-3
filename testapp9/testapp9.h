/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp9/testapp9.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp9/testapp9.h_v  $
|   $Workfile:   testapp9.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in testapp9 dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __testapp9H__
#define	    __testapp9H__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_testapp9		1   /* dialog id for testapp9 Dialog */
#define DIALOGID_testapp9Modal	2   /* dialog id for testapp9 Modal Dialog */

#define OPTIONBUTTONID_testapp9	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_testapp9		1   /* id for "parameter 1" text item */
#define TOGGLEID_testapp9		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_testapp9		1   /* id for synonym resource */

#define MESSAGELISTID_testapp9Errors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_testapp9Dialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_testapp9	1   /* id for toggle item hook func */
#define HOOKDIALOGID_testapp9		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct testapp9globals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } testapp9Globals;

#endif
