/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/pointclouds/pointclouds.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   pointclouds.r  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:40:49 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   pointclouds.r - PointClouds Example Dialog Box resource definitions			|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include <cmdlist.h>
#include <pselect.ids>

#include <keys.h>

#include "pointclouds.h"
#include "pointcloudscmd.h"
#include "pointcloudstxt.h"

/*----------------------------------------------------------------------+
|									|
| Use of predefined MicroStation symbol MSVERSION to compile the correct|
| version of your code.							|
|									|
|   #if defined (MSVERSION) && (MSVERSION >= 0x551)			|
|	code new to MicroStation Version 5.5.1 and higher		|
|   #endif								|
|									|
|   #if defined (MSVERSION) && (MSVERSION >= 0x550)			|
|	code new to MicroStation PowerDraft and higher			|
|   #endif								|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Dialog Box Resource Specification					|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   PointClouds Example Dialog Box							|
|									|
+----------------------------------------------------------------------*/

#define DW  79*XC
#define DH  35*YC

#define TEXTCOL1    18*XC
#define TEXTCOL2    55*XC

#define XCCHANN     XC
#define YCCHANN     6.5
#define XCMETA      XC
#define YCMETA      12
#define XCQUERY     XC
#define YCQUERY     19.5

DialogBoxRsc DIALOGID_PointClouds=
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
    DW, DH,
    NOHELP, MHELP, HOOKDIALOGID_Dlog, NOPARENTID,
    TXT_Main,
        {
        {{TEXTCOL1, GENY(3), 50*XC,  0},                Text, TEXTID_FileName, ON, 0, "", ""},
        {{68*XC,    GENY(3)-4, 10*XC, 0},               PushButton, PUSHBUTTONID_MyAttach, ON, 0, "", ""},
        {{TEXTCOL1, GENY(4.5), 10*XC,  0},              Text, TEXTID_NumberOfPoints, OFF, 0, "", ""},

        {{XCCHANN,  GENY(YCCHANN), DW - 2*XC, 6*YC},    GroupBox, 0, ON, 0, TXT_ChannelsGroupLabel, ""},
        {{TEXTCOL1, GENY(YCCHANN + 1), 10*XC,  0},      Text, TEXTID_HasRGBChannel, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCCHANN + 2), 10*XC,  0},      Text, TEXTID_HasIntensityChannel, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCCHANN + 3), 10*XC,  0},      Text, TEXTID_HasClassificationChannel, OFF, 0, "", ""},

        {{XCMETA,   GENY(YCMETA), DW - 2*XC, 8.5*YC},   GroupBox, 0, ON, 0, TXT_MetaDataGroupLabel, ""},
        {{TEXTCOL1, GENY(YCMETA+1), 40*XC,  0},         Text, TEXTID_SceneName, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCMETA+2), 10*XC,  0},         Text, TEXTID_NumberOfClouds, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCMETA+3), 10*XC,  0},         Text, TEXTID_NumberOfPointsMeta, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCMETA+4), 40*XC,  0},         Text, TEXTID_LowerBound, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCMETA+5), 40*XC,  0},         Text, TEXTID_UpperBound, OFF, 0, "", ""},

        {{XCQUERY,  GENY(YCQUERY), DW - 2*XC, 7*YC},    GroupBox, 0, ON, 0, TXT_QueryGroupLabel, ""},
        {{TEXTCOL1, GENY(YCQUERY+1), 40*XC,  0},        Text, TEXTID_QueryOrigin, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCQUERY+2), 40*XC,  0},        Text, TEXTID_QueryCorner, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCQUERY+3), 10*XC,  0},        Text, TEXTID_NumberOfPointsQuery, OFF, 0, "", ""},
        {{TEXTCOL1, GENY(YCQUERY+4), 10*XC,  0},        Text, TEXTID_NumberOfIterationsQuery, OFF, 0, "", ""},
        }
    }

extendedIntAttributes
    {
        {
        {EXTINTATTR_DLOGATTRS, DIALOGATTRX_NETDOCKABLE}
        }
    };

#undef DW
#undef DH


/*----------------------------------------------------------------------+
|									|
|   Item Resource Specifications					|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|                                                                       |
|   Text Item Resource Definitions					|
|                                                                       |
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_FileName =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   HOOKITEMID_MyFileName, NOARG, 
   512, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_FileNameLabel, 
   "dlogBoxInfo.fileName"
   };

DItem_TextRsc TEXTID_NumberOfPoints =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_NumberOfPointsLabel, 
   "dlogBoxInfo.numberOfPointsStr"
   };

DItem_TextRsc TEXTID_HasRGBChannel =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_HasRGBChannelLabel, 
   "dlogBoxInfo.hasRGBChannelStr"
   };

DItem_TextRsc TEXTID_HasIntensityChannel =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_HasIntensityChannelLabel, 
   "dlogBoxInfo.hasIntensityChannelStr"
   };

DItem_TextRsc TEXTID_HasClassificationChannel =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_HasClassificationChannelLabel, 
   "dlogBoxInfo.hasClassificationChannelStr"
   };

DItem_TextRsc TEXTID_SceneName =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_SceneNameLabel, 
   "dlogBoxInfo.sceneNameStr"
   };

DItem_TextRsc TEXTID_NumberOfClouds =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_NumberOfCloudsLabel, 
   "dlogBoxInfo.numberOfCloudsStr"
   };

DItem_TextRsc TEXTID_NumberOfPointsMeta =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_NumberOfPointsLabel, 
   "dlogBoxInfo.numberOfPointsMetaStr"
   };

DItem_TextRsc TEXTID_LowerBound =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   50, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_LowerBoundLabel, 
   "dlogBoxInfo.lowerBoundStr"
   };

DItem_TextRsc TEXTID_UpperBound =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   50, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_UpperBoundLabel, 
   "dlogBoxInfo.upperBoundStr"
   };

DItem_TextRsc TEXTID_QueryOrigin =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   50, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_QueryOriginLabel, 
   "dlogBoxInfo.queryOriginStr"
   };

DItem_TextRsc TEXTID_QueryCorner =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   50, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_QueryCornerLabel, 
   "dlogBoxInfo.queryCornerStr"
   };

DItem_TextRsc TEXTID_NumberOfPointsQuery =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_NumberOfPointsLabel, 
   "dlogBoxInfo.numberOfPointsQueryStr"
   };

DItem_TextRsc TEXTID_NumberOfIterationsQuery =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, 
   NOHOOK, NOARG, 
   25, "%s", "%s", "", "", NOMASK, NOCONCAT, 
   TXT_NumberOfIterationsLabel, 
   "dlogBoxInfo.numberOfIterationsQueryStr"
   };


/*----------------------------------------------------------------------+
|                                                                       |
|   "File Attach" Push Button Item Resource Definitions				|
|                                                                       |
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_MyAttach =
    {
    DEFAULT_BUTTON, 
    NOHELP, MHELP,
    HOOKITEMID_MyAttach, NOARG, 
    NOCMD, LCMD, "", 
    TXT_FileAttachItemLabel
    };
