/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/pointclouds/english/pointcloudstxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   pointcloudstxt.h  $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:40:50 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    pointcloudstxt.h	English language defines for help example	    	|
|									|
+----------------------------------------------------------------------*/

#if !defined (__pointcloudstxtH__)
#define __pointcloudstxtH__

/*----------------------------------------------------------------------+
|									|
|    Defines								|
|									|
+----------------------------------------------------------------------*/
#define TXT_Main		    	"Point Clouds Example Application"

/* Dialog item labels */
#define TXT_FileNameLabel		"~Point Cloud File:"
#define TXT_FileAttachItemLabel "~Attach File"
#define TXT_NumberOfPointsLabel "Number of Points:"
#define TXT_HasRGBChannelLabel  "RGB:"
#define TXT_HasIntensityChannelLabel        "Intensity:"
#define TXT_HasClassificationChannelLabel   "Classification:"
#define TXT_ChannelsGroupLabel  "Channels"
#define TXT_MetaDataGroupLabel  "MetaData"
#define TXT_NumberOfCloudsLabel "Number of Clouds:"
#define TXT_SceneNameLabel      "Scene Name:"
#define TXT_LowerBoundLabel     "Lower Bound:"
#define TXT_UpperBoundLabel     "Upper Bound:"
#define TXT_QueryGroupLabel     "Query Points"
#define TXT_QueryOriginLabel    "Origin (UORs):"
#define TXT_QueryCornerLabel    "Corner (UORs):"
#define TXT_NumberOfIterationsLabel         "Number of Iterations:"

#endif /* #if !defined (__pointcloudstxtH__) */