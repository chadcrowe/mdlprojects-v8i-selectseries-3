/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/pointclouds/english/pointcloudsstr.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:     $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:40:50 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   pointcloudsstr.r - PointClouds Message list resource   	    	    		|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "pointclouds.h"

/*----------------------------------------------------------------------+
|									|
|   Messages List Resource Definition					|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_Msgs =
{
    {
    {MSGID_LoadCmdTbl, 		"Unable to load command table"},
    {MSGID_DialogOpen, 		"Unable to open main dialog"},
    {MSGID_SureString,		"Are you sure?"},
    {MSGID_CantOpenFile,	"Can't open this file. Please make sure it exists."},
    }
};

