/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/pointclouds/pointclouds.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:     $
|   $Revision: 1.1.16.1 $
|   	$Date: 2013/07/01 20:40:49 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   pointclouds.h - pointclouds Dialog Box constants and types			|
|                                                                       |
+----------------------------------------------------------------------*/

#if !defined (__pointcloudsH__)
#define __pointcloudsH__

/*----------------------------------------------------------------------+
|									|
|   Dialog Box ID's							|
|									|
+----------------------------------------------------------------------*/
#define DIALOGID_PointClouds   1

/*----------------------------------------------------------------------+
|									|
|   Dialog Item ID's							|
|									|
+----------------------------------------------------------------------*/
#define TEXTID_FileName                 2
#define TEXTID_NumberOfPoints           3
#define TEXTID_HasRGBChannel            4
#define TEXTID_HasIntensityChannel      5
#define TEXTID_HasClassificationChannel 6
#define TEXTID_NumberOfClouds           7
#define TEXTID_SceneName                8
#define TEXTID_NumberOfPointsMeta       9
#define TEXTID_LowerBound               10
#define TEXTID_UpperBound               11
#define TEXTID_QueryOrigin              12
#define TEXTID_QueryCorner              13
#define TEXTID_NumberOfPointsQuery      14
#define TEXTID_NumberOfIterationsQuery  15

#define PUSHBUTTONID_MyAttach	        1

/*----------------------------------------------------------------------+
|									|
|   Message list IDs							|
|									|
+----------------------------------------------------------------------*/
#define	MESSAGELISTID_Msgs	    2

/*----------------------------------------------------------------------+
|									|
|   Command IDs - used in the Message list definition for command names	|
|									|
+----------------------------------------------------------------------*/
#define	MSGID_LoadCmdTbl	1
#define	MSGID_DialogOpen	2
#define	MSGID_SureString 	3
#define	MSGID_CantOpenFile  4

/*----------------------------------------------------------------------+
|									|
|   Dialog Hook ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKDIALOGID_Dlog	    1
#define HOOKITEMID_MyFileName   2
#define HOOKITEMID_MyAttach	    3


/*----------------------------------------------------------------------+
|									|
|   Typedefs for Dialog access strings					|
|									|
+----------------------------------------------------------------------*/
typedef struct dlogboxinfo
{
    char      fileName[512];
    char      numberOfPointsStr[32];
    char      hasRGBChannelStr[10];
    char      hasIntensityChannelStr[10];
    char      hasClassificationChannelStr[10];
    char      numberOfCloudsStr[10];
    char      sceneNameStr[512];
    char      numberOfPointsMetaStr[32];
    char      lowerBoundStr[64];
    char      upperBoundStr[64];
    char      queryOriginStr[64];
    char      queryCornerStr[64];
    char      numberOfPointsQueryStr[32];
    char      numberOfIterationsQueryStr[32];
    int	      style;
} DlogBoxInfo;


#endif /* #if !defined (__pointcloudsH__) */
