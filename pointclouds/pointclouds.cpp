/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/pointclouds/pointclouds.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:     $
|   $Revision: 1.2.16.1 $
|   	$Date: 2013/07/01 20:40:49 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   pointclouds.cpp - pointclouds source code.                                       |
|       This application displays a dialog with a field that allows     |
|       to enter a file name. This file must be a point cloud file with |
|       the ".POD" format. After attaching the file, some of its |
|       properties are displayed in the dialog.                         |
|                                                                       |
|       The application illustrates how to use some functions of the    |
|       PointClouds API.                                                |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <cmdlist.h>    /* MicroStation command numbers */
#include <WString.h>

#include "pointclouds.h"    /* Contains Dlog IDs and typedefs for this app */
#include "pointcloudscmd.h" /* Contains Command numbers for this app */

#include <dlogman.fdf>
#include <mssystem.fdf>
#include <msoutput.fdf>
#include <msrsrc.fdf>
#include <mscexpr.fdf>
#include <msstate.fdf>
#include <msparse.fdf>
#include <msvar.fdf>
#include <mscnv.fdf>

#include <pointcloud\pointcloudhandler.h>
#include <pointcloud\pointcloudapi.h>   /* Contains most point cloud API methods */

/*----------------------------------------------------------------------+
|                                                                       |
|   Local Function Declarations                                         |
|                                                                       |
+----------------------------------------------------------------------*/
   
/*----------------------------------------------------------------------+
|                                                                       |
|   Private Global variables                                            |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------
| The following variable is referenced in C expression strings used
| by the text and option button items defined in pointclouds.r.
+----------------------------------------------------------------------*/
Private DlogBoxInfo     dlogBoxInfo;

/*======================================================================+
|                                                                       |
|   Local Functions				    	|
|                                                                       |
+======================================================================*/

/*----------------------------------------------------------------------+
|                                                                       |
| name          QueryPointCloudData                                  |
|                                                                       |
| author        BSI                                     6/10           |
|                                                                       |
+----------------------------------------------------------------------*/
Private  void QueryPointCloudData
(
DialogItemMessage                      *dimP,       /* => a ptr to a dialog item message */
Bentley::Ustn::Element::EditElemHandle  eeh,        /* => point cloud element handle */
UInt64                                  nbPoints    /* => number of points in the cloud */
)
    {
    // This method queries points in the point cloud file 

    // Get range of point cloud element (in UORs)
    MSElementCP element = eeh.GetElementCP ();
    DVector3d   rangeVec;
    mdlCnv_scanRangeToDRange (&rangeVec, &element->hdr.dhdr.range);

    // Create a smaller range to query points
    DVector3d   smallerRange;
    smallerRange.org.x = rangeVec.org.x + (rangeVec.end.x - rangeVec.org.x) * 0.333;
    smallerRange.org.y = rangeVec.org.y + (rangeVec.end.y - rangeVec.org.y) * 0.333;
    smallerRange.org.z = rangeVec.org.z + (rangeVec.end.z - rangeVec.org.z) * 0.333;
    smallerRange.end.x = rangeVec.end.x - (rangeVec.end.x - rangeVec.org.x) * 0.333;
    smallerRange.end.y = rangeVec.end.y - (rangeVec.end.y - rangeVec.org.y) * 0.333;
    smallerRange.end.z = rangeVec.end.z - (rangeVec.end.z - rangeVec.org.z) * 0.333;

    // Queries points within the "smallerRange" bounding box. It is also possible to query points within a sphere
    // using CreateBoundingSphereQuery.
    Bentley::Ustn::PointCloud::PointCloudDataQueryPtr query = 
        Bentley::Ustn::PointCloud::IPointCloudDataQuery::CreateBoundingBoxQuery (eeh, smallerRange.org, smallerRange.end);

    // Set the density of points to query
    query->SetDensity (Bentley::Ustn::PointCloud::IPointCloudDataQuery::QUERY_DENSITY_FULL, 1);
    
    // Set dialog items
    char displayStr[32];
    DialogItem *diP;

    // Origin of the query bounding box (in UORs)
    sprintf (displayStr, "%0.5f, %0.5f, %0.5f", smallerRange.org.x, smallerRange.org.y, smallerRange.org.z);
    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_QueryOrigin, 0);
    mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

    // Corner of the query bounding box (in UORs)
    sprintf (displayStr, "%0.5f, %0.5f, %0.5f", smallerRange.end.x, smallerRange.end.y, smallerRange.end.z);
    diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_QueryCorner, 0);
    mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

    // Set maximum of points to query on one call to GetPoints (minimum (nbPoints / 100, 100000))
    int             maxPoints = ((int) nbPoints / 100 < 100000 ? (int) nbPoints / 100 : 100000);

    // Declare buffers that will contain queried points and attributes
    DPoint3d*       pointsBuffer =  new DPoint3d[maxPoints];
    RGBColorDef*    rgbBuffer = new RGBColorDef[maxPoints];
    short*          intensityBuffer = new short [maxPoints];

    // Query points.
    // Points must be queried by multiple calls to GetPoints, until returned points == 0.
    int pointsRead = 0;
    int nbQueriedPoints = 0;
    int nbIterations = 0;
    do
        {
        pointsRead = query->GetPoints (maxPoints, pointsBuffer, rgbBuffer, intensityBuffer, NULL, 0);
        nbQueriedPoints += pointsRead;
        nbIterations++;

        // Set dialog item: Number of points that were queried
        sprintf (displayStr, "%d", nbQueriedPoints);
        diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_NumberOfPointsQuery, 0);
        mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

        // Set dialog item: Number of iterations required to get all the points
        sprintf (displayStr, "%d", nbIterations);
        diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_NumberOfIterationsQuery, 0);
        mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);
        }
    while (pointsRead > 0);  

    delete [] intensityBuffer;
    delete [] pointsBuffer;
    delete [] rgbBuffer;
    }

/*======================================================================+
|                                                                       |
|   Open Dialog Command Functions				    	|
|                                                                       |
+======================================================================*/

/*----------------------------------------------------------------------+
|                                                                       |
| name          pointclouds_openMyMainDialog                                  |
|                                                                       |
| author        BSI                                     06/10           |
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+*//**
 Open our main dialog box with the function mdlDialog_open.
 The NULL parameter means to look for the Dialog Box resource
 in the file that the application is from.
* 								    	    *
* @bsimethod pointclouds_openMyMainDialog   					*					    	*
*								        *
* Author:   BSI 				02/01 		*
*								        *
+----------------------------------------------------------------------*/
Public  void pointclouds_openMyMainDialog
(
char            *unparsedP
)
//cmdNumber       CMD_OPEN_MYMAINDIALOG
    {

    /*------------------------------------------------------------- 
    | Open our main dialog box with the function mdlDialog_open.
    | The NULL parameter means to look for the Dialog Box resource
    | in the file that the application is from.
    +-------------------------------------------------------------*/
    if (NULL == mdlDialog_open (NULL, DIALOGID_PointClouds))
	{
	/* On error, output a message and unload the application */
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Msgs, 
				    MSGID_DialogOpen);
	mdlDialog_cmdNumQueueExt (FALSE, CMD_MDL_UNLOAD,
			       mdlSystem_getCurrTaskID(), TRUE, TRUE);
	}
    }

/*======================================================================+
|                                                                       |
|   Dialog Hook Functions                                               |
|                                                                       |
+======================================================================*/

/*----------------------------------------------------------------------+
|                                                                       |
| name          pointclouds_dlogHook                                    |
|                                                                       |
| author        BSI                                     5/10            |
|                                                                       |
+----------------------------------------------------------------------*/
Public  void pointclouds_dlogHook
(
DialogMessage   *dmP    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_DESTROY:
	    {
	    /*-----------------------------------------------------------
	    | We want our application to unload if the Close icon is
	    | double-clicked.  
	    +----------------------------------------------------------*/
	    mdlDialog_cmdNumQueueExt (FALSE, CMD_MDL_UNLOAD,
			          mdlSystem_getCurrTaskID(), TRUE, TRUE);
	    mdlState_startDefaultCommand ();
	    break;
	    };
	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          pointclouds_attachHook                                  |
|                                                                       |
| author        BSI                                     5/10            |
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+*//**
* 								    	    *
* @bsimethod pointclouds_attachHook   						*					    	*
*								        *
* Author:   BSI 				5/10 		*
*								        *
+----------------------------------------------------------------------*/
Public  void pointclouds_attachHook
(
DialogItemMessage       *dimP   /* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_QUEUECOMMAND:
	    {
        Bentley::Ustn::Element::EditElemHandle eeh;

        // Attach the point cloud file specified in the dialog box
        if (SUCCESS == Bentley::Ustn::Element::PointCloudHandler::CreateElement(eeh, ACTIVEMODEL, Bentley::WString(dlogBoxInfo.fileName)))
        {
            eeh.AddToModel (ACTIVEMODEL);

            Bentley::Ustn::PointCloud::PointCloudFileQueryPtr pFQ = Bentley::Ustn::PointCloud::IPointCloudFileQuery::CreateFileQuery (eeh);
            
            // Display number of points in the cloud
            UInt64 nbPoints = pFQ->GetNumberOfPoints ();

            char numberOfPointsStr[32];
            sprintf (numberOfPointsStr, "%d", nbPoints);
            DialogItem *diP;
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_NumberOfPoints, 0);
            mdlDialog_itemSetValue (NULL, 0, NULL, numberOfPointsStr, dimP->db, diP->itemIndex);

            // Indicate which channels are contained in the point cloud file
            char hasChannelStr[10];
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_HasRGBChannel, 0);
            if (pFQ->HasRGBChannel())
                sprintf (hasChannelStr, "yes");
            else
                sprintf (hasChannelStr, "no");
            mdlDialog_itemSetValue (NULL, 0, NULL, hasChannelStr, dimP->db, diP->itemIndex);
                
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_HasIntensityChannel, 0);
            if (pFQ->HasIntensityChannel())
                sprintf (hasChannelStr, "yes");
            else
                sprintf (hasChannelStr, "no");
            mdlDialog_itemSetValue (NULL, 0, NULL, hasChannelStr, dimP->db, diP->itemIndex);

            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_HasClassificationChannel, 0);
            if (pFQ->HasClassificationChannel())
                sprintf (hasChannelStr, "yes");
            else
                sprintf (hasChannelStr, "no");
            mdlDialog_itemSetValue (NULL, 0, NULL, hasChannelStr, dimP->db, diP->itemIndex);

            // Get metaData
            DPoint3d lowerBound;
            DPoint3d upperBound;    
            Bentley::WString name;
            UInt32 num_clouds;
            UInt64 num_points;
            pFQ->GetMetaData (name, num_clouds, num_points, &lowerBound, &upperBound);

            // Set dialog items
            char displayStr[512];
            name.ToChar(displayStr, 512);
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_SceneName, 0);
            mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

            sprintf (displayStr, "%d", num_clouds);
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_NumberOfClouds, 0);
            mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

            sprintf (displayStr, "%d", num_points);
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_NumberOfPointsMeta, 0);
            mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

            sprintf (displayStr, "%0.5f, %0.5f, %0.5f", lowerBound.x, lowerBound.y, lowerBound.z);
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_LowerBound, 0);
            mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

            sprintf (displayStr, "%0.5f, %0.5f, %0.5f", upperBound.x, upperBound.y, upperBound.z);
            diP = mdlDialog_itemGetByTypeAndId (dimP->db, RTYPE_Text, TEXTID_UpperBound, 0);
            mdlDialog_itemSetValue (NULL, 0, NULL, displayStr, dimP->db, diP->itemIndex);

            // Queries points of the point cloud
            QueryPointCloudData (dimP, eeh, num_points);
        }
        else
        {
            // Display error message
            int actionButton;
	        char message[80];

            mdlResource_loadFromStringList(message, NULL, MESSAGELISTID_Msgs, MSGID_CantOpenFile);

	        actionButton = mdlDialog_openMessageBox (
	        DIALOGID_MsgBoxOK, 
	        message,		      
	        MSGBOX_ICON_WARNING);
        }

	    mdlState_startDefaultCommand ();
	    break;
	    };
	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*======================================================================+
|                                                                       |
|   System Event Handler routines                                       |
|                                                                       |
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          pointclouds_unloadFunction                                    |
|                                                                       |
| author        BSI                                     06/10           |
|                                                                       |
+----------------------------------------------------------------------*/
Public  int pointclouds_unloadFunction
(
int             unloadType
)
    {
    if (unloadType < 0)
    	{
    	return	SUCCESS;
    	}

    if (mdlDialog_find (DIALOGID_PointClouds, NULL))
	{
	int actionButton;
	char buffer[80];

        mdlResource_loadFromStringList(buffer, NULL, MESSAGELISTID_Msgs, MSGID_SureString);

#if defined (MSVERSION) && (MSVERSION >= 0x550)
    	actionButton = mdlDialog_openMessageBox (
		DIALOGID_MsgBoxYesNo, /* => dialog id of message box */
		buffer,		      /* => message to display */
		MSGBOX_ICON_QUESTION);/* => which icon to show */
	if (ACTIONBUTTON_YES == actionButton)
	    return SUCCESS;
	else
	    return !SUCCESS;
#else
    	actionButton = mdlDialog_openAlert (buffer);
	if (ACTIONBUTTON_OK == actionButton)
	    return SUCCESS;
	else
	    return !SUCCESS;
#endif
	}

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Main                                                                |
|                                                                       |
+----------------------------------------------------------------------*/
Public DialogHookInfo uHooks[]=
    {
    {HOOKDIALOGID_Dlog,             (PFDialogHook)pointclouds_dlogHook},
    {HOOKITEMID_MyAttach,           (PFDialogHook)pointclouds_attachHook},
    };

/*----------------------------------------------------------------------+
|                                                                       |
| name          main                                                    |
|                                                                       |
| author        BSI                                     5/10            |
|                                                                       |
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    RscFileHandle   rscFileH;   /* a resource file handle */
    SymbolSet	    *setP;      /* a ptr to a "C expression symbol set" */

    if (mdlResource_openFile (&rscFileH, NULL, RSC_READ) != SUCCESS)
	{
    	/* exit this program and unload it */	
        mdlSystem_exit(	ERROR, 1);
	}

    // Initializes the pointcloud libraries and registers the element handler
    Bentley::Ustn::Element::PointCloudHandler::Initialize();

    Private MdlCommandNumber  commandNumbers [] =
    {
    { pointclouds_openMyMainDialog,   CMD_OPEN_MYMAINDIALOG},
    0
    };

    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);

    if (mdlParse_loadCommandTable (NULL) == NULL)
	{
    	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Msgs, MSGID_LoadCmdTbl);

    	/* exit this program and unload it */	
    	mdlSystem_exit(	ERROR, 1);
	}

    mdlDialog_hookPublish (sizeof (uHooks)/sizeof (DialogHookInfo), uHooks);
    setP = mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);

    mdlDialog_publishComplexVariable (setP, "dlogboxinfo", "dlogBoxInfo", &dlogBoxInfo);

    dlogBoxInfo.style = 0;
    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM, pointclouds_unloadFunction);
    mdlDialog_sendMessagesOnUnload (mdlSystem_getCurrMdlDesc(),TRUE);
    mdlDialog_cmdNumQueueExt (TRUE, CMD_OPEN_MYMAINDIALOG, "", TRUE, FALSE);
			       
    return (SUCCESS);
    }

