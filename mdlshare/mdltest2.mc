/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdltest2.mc,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   mdltest2.mc  $
|   $Revision: 1.3.52.1 $
|   	$Date: 2013/07/01 20:39:58 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Test MDL Application referencing the shared functions and   	|
|   	variables exported by the mdlshare MDL Shared Library MDL   	|
|   	application.						    	|
|									|
|   	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -   	|
|									|
|   Public Routine Summary -						|
|									|
|   	mdltest2_displayPassword - Command function to display local	|
|				   dialog box using shared hook func-	|
|				   tion from the mdlshare shared lib-	|
|			    	   rary.				|
|   	main - Main application entry point.			    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <msdefs.h>
#include <basedefs.h>
#include <global.h>
#include <dlogitem.h>
#include <dlogman.fdf>
#include <rscdefs.h>

#include "mdlt2cmd.h"
#include "mdlt2ids.h"

#include <msoutput.fdf>
#include <msparse.fdf>
#include <msrsrc.fdf>

/*----------------------------------------------------------------------+
|									|
|   Symbols from the MDL Shared Library we are referencing	    	|
|									|
+----------------------------------------------------------------------*/
extern int  taskCount;
extern void  sharelib_passwordHook (DialogItemMessage	*);
extern void sharelib_setPasswordVarP (char *);

Private char	myPassword[32];

DialogHookInfo	uHooks[] = { {HOOKITEMID_Password2, sharelib_passwordHook} };

/*----------------------------------------------------------------------+
|									|
| name		mdltest2_displayPassword				|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Private void	mdltest2_displayPassword
(
char   *password
) cmdNumber CMD_MDLT2_PASSWORD
    {
    int	    lastAct;

    myPassword[0] = '\0';
    mdlDialog_openModal (&lastAct, NULL, DIALOGID_Password2);
    mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGEID_Msgs, MSGID_S2Password,
		    	 myPassword);
    }

/*----------------------------------------------------------------------+
|									|
| name		main						    	|
|									|
| author	BSI		 			5/95		|
|									|
+----------------------------------------------------------------------*/
Public void main
(
int   argc,		/* => number of args in next array  */
char *argv[]		/* => array of cmd line arguments   */
)
    {
    RscFileHandle   rscFileH;

    /*	Open the resource file that we came out of		    */
    mdlResource_openFile (&rscFileH, NULL, 0);

    /*	Load the command table					    */
    mdlParse_loadCommandTable (NULL);

    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    sharelib_setPasswordVarP (&myPassword[0]);

    /* 
        Output a message - note that the message list resource is
        contained in the shared library 
    */
    mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGEID_Msgs, MSGID_S2TaskCount,
		    	 taskCount);
    }
