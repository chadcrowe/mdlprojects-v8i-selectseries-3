/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlt1cmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdlt1cmd.r_v  $
|   $Workfile:   mdlt1cmd.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:47 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application:			    	|
|	    MDLTEST1 application command resources		    	|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

#define	CT_NONE	    0
#define	CT_MAIN	    1
#define	CT_MDLT1    2

Table	CT_MAIN =
{
    { 1,    CT_MDLT1,       INPUT,  	REQ,    "MDLT1" },
};

Table	CT_MDLT1 =
{
    { 1,    CT_NONE,	    INHERIT,	NONE,	"PASSWORD" },
};

