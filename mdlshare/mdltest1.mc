/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdltest1.mc,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
 /*----------------------------------------------------------------------+
|									|
|   $Workfile:   mdltest1.mc  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:52 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	Test MDL Application referencing the shared functions and   	|
|   	variables exported by the mdlshare MDL Shared Library MDL   	|
|   	application.						    	|
|									|
|   	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -   	|
|									|
|   Public Routine Summary -						|
|									|
|   	mdltest1_displayPassword - Command function to get the password |
|			    	   from the shared library dialog box	|
|   	main - Main entry point					    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <msdefs.h>
#include <basedefs.h>
#include <global.h>
#include <rscdefs.h>

#include "mdlt1cmd.h"
#include "mdlt1ids.h"

#include <msrsrc.fdf>
#include <msparse.fdf>
#include <msoutput.fdf>

/*----------------------------------------------------------------------+
|									|
|   Symbols from the MDL Shared Library we are referencing	    	|
|									|
+----------------------------------------------------------------------*/
extern int  taskCount;
extern int  sharelib_passwordDialog (char *);

/*----------------------------------------------------------------------+
|									|
| name		mdltest1_displayPassword				|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Private void	mdltest1_displayPassword
(
char   *password
) cmdNumber CMD_MDLT1_PASSWORD
    {
    char    savePassword[32];
    int	    returnVal;

    /*	Call into the shared library to display the password dialog */
    /*	and get the returned password and the comparison value	    */
    savePassword[0] = '\0';
    returnVal = sharelib_passwordDialog (savePassword);
    mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGEID_Msgs, MSGID_S1Password,
		    	 returnVal, savePassword);
    }

/*----------------------------------------------------------------------+
|									|
| name		main						    	|
|									|
| author	BSI		 			5/95		|
|									|
+----------------------------------------------------------------------*/
Public void main
(
int   argc,		/* => number of args in next array  */
char *argv[]		/* => array of cmd line arguments   */
)
    {
    RscFileHandle   rscFileH;

    /*	Open the resource file that we came out of (MDLTEST1.MA)    */
    mdlResource_openFile (&rscFileH, NULL, 0);

    /*	Load the command table					    */
    mdlParse_loadCommandTable (NULL);

    /* 
        Output a message - note that the message list resource is
        contained in the shared library 
    */
    mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGEID_Msgs, MSGID_S1TaskCount, taskCount);
    }
