/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdltest2.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdltest2.r_v  $
|   $Workfile:   mdltest2.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:59 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application - MDLTEST application   	|
|   	dialog box resources					    	|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	    
#include <dlogids.h>

#include "mdlt2ids.h"
#include "mdlt2txt.h"

/*----------------------------------------------------------------------+
|									|
|   Password Dialog Box							|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_Password2 =
    {
    DIALOGATTR_MODAL,
    26 * XC,7 * YC,
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_EnterPassword2,
{
{{11 * XC, GENY(2), 11 * XC, 0},    Text, TEXTID_Password2, ON, 0, "", ""},
{{2 * XC, GENY(4), 10 * XC, 0},     PushButton, PUSHBUTTONID_OK, ON, 0, "", ""},
{{14 * XC, GENY(4), 10 * XC, 0},    PushButton, PUSHBUTTONID_Cancel, ON, 0, "", ""},
}
   };

/*----------------------------------------------------------------------+
|									|
|   Password Dialog Item Resource Specifications			|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_Password2 =
   {
   NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_Password2, NOARG,
   25, "%s", "%s", "", "", NOMASK, NOCONCAT,
   TXT_Password2, ""
   };
