/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlt2cmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdlt2cmd.r_v  $
|   $Workfile:   mdlt2cmd.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:49 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application:			    	|
|	    MDLTEST2 application command resources			|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

#define	CT_NONE	    0
#define	CT_MAIN	    1
#define	CT_MDLT2    2

Table	CT_MAIN =
{
    { 1,    CT_MDLT2,       INPUT,  	REQ,    "MDLT2" },
};

Table	CT_MDLT2 =
{
    { 1,    CT_NONE,	    INHERIT,	NONE,	"PASSWORD" },
};

