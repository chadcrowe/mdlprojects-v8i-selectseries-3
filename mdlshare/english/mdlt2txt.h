/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/english/mdlt2txt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/english/mdlt2txt.h_v  $
|   $Workfile:   mdlt2txt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:03 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application - MDLTEST2 text      	|
|   	definitions						    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__mdlt2txtH__)
#define __mdlt2txtH__

#define	TXT_EnterPassword2   	"Enter Password 2"
#define	TXT_Password2	    	"~Password2:"

#endif /* !defined	(__mdlt2txtH__) */
