/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/english/mdlshmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/english/mdlshmsg.r_v  $
|   $Workfile:   mdlshmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:01 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MDL Shared Library Example Application Messages		    	|
|									|
+----------------------------------------------------------------------*/
#include    <rscdefs.h>
#include    <dlogbox.h>

#include    "mdlshids.h"
#include    "mdlt1ids.h"
#include    "mdlt2ids.h"

/*----------------------------------------------------------------------+
|									|
|	   Message Resources						|
|									|
+----------------------------------------------------------------------*/
/* Prompt and Status message */

MessageList MESSAGEID_Msgs =
{
    {
    {	MSGID_PasswordS,   	"Password Dialog: %s" },
    {	MSGID_TaskPasswordLS,	"Set task password: %08lX/%s" },
    {	MSGID_PasswordVarSL,	"Set task password variable ptr: %s/%08lX" },
    {	MSGID_AllocBlock,	"  *  Alloc Task Block: %08lX" },
    {	MSGID_UnloadTask,	"Task unload : %s/%08lX" },
    {	MSGID_Initialize,	"Task initialization : %s" },
    {	MSGID_TaskBlockP,	"     taskBlockP = %08lX" },
    {	MSGID_InitLibrary,	"  *  initializing shared library" },
    {	MSGID_LinkTaskInfo,	"     linking task block into list" },
    {	MSGID_UnloadLibrary,	"Shared Library Unloading" },
    {	MSGID_NoMoreClients,	"All MDL clients terminated - unloading" },
    {	MSGID_S1Password,	"(%d) - Password = %s" },
    {	MSGID_S1TaskCount,	"MDL Task 1 - taskcount = %d" },
    {	MSGID_S2Password,   	"Password = %s" },
    {	MSGID_S2TaskCount,  	"MDL Task 2 - taskcount = %d" },
    }
};
