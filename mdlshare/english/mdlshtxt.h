/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/english/mdlshtxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/english/mdlshtxt.h_v  $
|   $Workfile:   mdlshtxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:02 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application - Shared Library     	|
|   	text definitions					    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__mdlshtxtH__)
#define __mdlshtxtH__

#define	TXT_EnterPassword   	"Enter Password"
#define	TXT_Password	    	"~Password:"

#endif /* !defined	(__mdlshtxtH__) */
