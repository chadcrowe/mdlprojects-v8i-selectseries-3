/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlshare.mc,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   mdlshare.mc  $
|   $Revision: 1.4.52.1 $
|   	$Date: 2013/07/01 20:39:41 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application			    	|
|								    	|
|	    This MDL application also serves as an MDL shared	    	|
|	    library which has functions and variables which can	    	|
|	    be referenced by other MDL applications.		    	|
|									|
|   	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -   	|
|									|
|   Public Routine Summary -						|
|								    	|
|   	mdlshare_passwordHook - Password text item hook function	|
|   	mdlshare_passwordDialog - Password dialog box command function	|
|   	mdlshare_setPassword - Set task password value		    	|
|   	mdlshare_setPasswordVarP - Set pointer to local task access 	|
|				   string for text item password	|
|   	initialize - Main entry point for each MDL application ref- 	|
|		     erencing this shared library		    	|
|   	main - Main application entry point			    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <msdefs.h>
#include <basedefs.h>
#include <global.h>
#include <dlogids.h>
#include <dlogitem.h>
#include <cmdlist.h>
#include <keys.h>
#include <cexpr.h>
#include <userfnc.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "mdlshare.h"
#include "mdlshcmd.h"

#include <dlogman.fdf>
#include <mssystem.fdf>
#include <msrsrc.fdf>
#include <msparse.fdf>
#include <msfile.fdf>

/*----------------------------------------------------------------------+
|									|
|   Public Variables							|
|									|
+----------------------------------------------------------------------*/
Public	int 	taskCount = 0;

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
Private int	    initialized = FALSE;
Private TaskInfo   *taskListP = NULL;
Private int	    pwIndex = 0;
Private char	    localPassword[26];
Private char	    mdlshareTaskId[32];
Private char	    fullPathName[MAXFILELENGTH];
Private TaskInfo    libTaskInfo = {
				    0L, NULL, NULL, NULL, 0, 0
				  };

/*----------------------------------------------------------------------+
|									|
| name		share_printMsg					    	|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Private void	share_printMsg
(
int    strNum,
...
)
    {
    char    msgBuf[128], formatStr[128];
    va_list ap;

    /*	Display indicated message in messages dialog box     */
    mdlResource_loadFromStringList (formatStr, libTaskInfo.rscFileH,
				    MESSAGEID_Msgs, strNum);
    va_start (ap, strNum);
    vsprintf (msgBuf, formatStr, ap);
    va_end (ap);

    mdlDialog_dmsgsPrint (msgBuf);
    }

/*----------------------------------------------------------------------+
|									|
| name		share_getTaskInfoP					|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Private void *share_getTaskInfoP
(
void   *mdlDescrP
)
    {
    TaskInfo   *taskP = taskListP;
    void       *currDesc = mdlSystem_getCurrMdlDesc ();
    void       *mdlDscrP = (mdlDescrP ? mdlDescrP : currDesc);

    /*	Locate the task information block for the task indicated by */
    /*	the MDL Descriptor parameter.				    */
    while (taskP && (taskP->mdlDescrP != mdlDscrP))
    	taskP = taskP->next;

    /*	If didn't find a match, see if it is our own task.	    */
    if (!taskP &&
    	((mdlDescrP == libTaskInfo.mdlDescrP) ||
	 (!mdlDescrP && (libTaskInfo.mdlDescrP == currDesc))))
	return &libTaskInfo;

    return (taskP);
    }


/*----------------------------------------------------------------------+*//**
* !!!Describe Function Completely!!!                                                                        *
*                                                                       *                                               *
* @param        nameOfParam     !!!what this parameter means            *
* @return       !!!what the return value means                          *
*                                                                       *
* Author:   mark.anderson                               10/04           *
*                                                                       *
+----------------------------------------------------------------------*/
Private  int validatePassword
(
DialogBox   *dbP
)
    {


    return TRUE;
    }

/*----------------------------------------------------------------------+
|									|
| name		share_passwordHook				    	|
|									|
| author	BSI 					05/95		|
|									|
+----------------------------------------------------------------------*/
Public void share_passwordHook
(
DialogItemMessage	*dimP
)
    {
    TaskInfo   *taskP = share_getTaskInfoP (NULL);
    char       *pwdP;

    /*	Password Hook function - get proper access string from task */
    /*	block.							    */
    if (taskP)
	pwdP = taskP->passwordP;
    else
	return;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    /*	Initialize our access string			    */
	    strcpy (pwdP, "\0");
	    pwIndex = 0;
	    break;

	case DITEM_MESSAGE_FOCUSIN:
	    /*	Check our string position			    */
	    if (strcmp (pwdP, "\0") == 0)
		pwIndex = 0;
	    break;

	case DITEM_MESSAGE_KEYSTROKE:
	    {
	    /*	Handle the input keystroke for backspae processing  */
	    int keystroke = dimP->u.keystroke.keystroke;
	    switch (keystroke)
		{
		case VKEY_BACKSPACE:
		    if (--pwIndex < 0)
		    	pwIndex = 0;
		    pwdP[pwIndex] = '\0';
	    	    break;

		 default:
		    if ((keystroke > 0x20) && (keystroke < 0x7f) && (pwIndex < 24))
			{
	    	    	pwdP[pwIndex++] = (char)keystroke;
	       	    	dimP->u.keystroke.keystroke = 'X';
	   		}
		    break;
	    	}
	    break;
	    }

	case DITEM_MESSAGE_FOCUSOUT:
	    /*	Mark end of string if leaving			    */
  	    pwdP[pwIndex] = '\0';
	    break;
	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		share_openDialog					|
|									|
| author	BSI				    	06/95		|
|									|
+----------------------------------------------------------------------*/
Private int share_openDialog
(
void
)
    {
    long    	lastAct;

    /*	Open dialog and return value				    */
    mdlDialog_openModal (&lastAct, NULL, DIALOGID_Password);
    return (lastAct);
    }

/*----------------------------------------------------------------------+
|									|
| name		share_passwordDialog					|
|									|
| author	BSI		 			5/95		|
|									|
+----------------------------------------------------------------------*/
Public int share_passwordDialog
(
char   *pwd	    /* <=> Password to compare against/return area  */
) cmdNumber CMD_MDLSHARE_PASSWORD
    {
    TaskInfo   *taskP = share_getTaskInfoP (NULL);
    long    	lastAct;

    if (!taskP)
	return ERROR;

    share_printMsg (MSGID_PasswordS, taskP->taskId);

    /*	Open the dialog box in lib task space and get the password  */
    /*	from the user						    */
    lastAct = mdlDialog_callFunction (libTaskInfo.mdlDescrP,
				      share_openDialog, NULL);

    /*	Have a password - now get the result and return that and    */
    /*	the result to the user via the input pointer value	    */
    if (pwd)
	{
	/*  Caller gave us memory to hold password		    */
	if (*pwd)
	    {
	    /*	Have password to compare against		    */
	    if (strcmp (pwd, localPassword) != 0)
		return 1;
	    else
		return SUCCESS;
	    }
	else
	    {
	    /*	Have return area to place user password into	    */
	    strcpy (pwd, localPassword);

	    /*	If task block has a password compare it to the user */
	    /*	value, otherwise just return			    */
	    if (*taskP->password)
	    	if (strcmp (taskP->password, localPassword) != 0)
		    return 1;
	    	else
		    return SUCCESS;
	    else
	    	return 2;
	    }
    	}
    else
	/*  If task block has a password compare it to the user     */
	/*  value, otherwise just return			    */
	if (*taskP->password)
	    if (strcmp (taskP->password, localPassword) != 0)
	    	return 1;
	    else
	    	return SUCCESS;
	else
	    return 3;
    }

/*----------------------------------------------------------------------+
|									|
| name		share_setPassword					|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Public void	share_setPassword
(
char   *password
) cmdNumber CMD_MDLSHARE_SET_PASSWORD
    {
    TaskInfo   *taskP = share_getTaskInfoP (NULL);

    /*	Set the password the user must type into the dialog box	    */
    if (!taskP)
	return;

    strcpy (taskP->password, password);
    share_printMsg (MSGID_TaskPasswordLS, taskP->taskId, password);
    }

/*----------------------------------------------------------------------+
|									|
| name		share_setPasswordVarP				    	|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Public void	share_setPasswordVarP
(
char   *passwordP
)
    {
    TaskInfo   *taskP = share_getTaskInfoP (NULL);

    /*	Set the password access string for the password text item   */
    if (!taskP)
	return;

    taskP->passwordP = passwordP;
    share_printMsg (MSGID_PasswordVarSL, taskP->taskId, passwordP);
    }

/*----------------------------------------------------------------------+
|									|
| name		share_allocTaskBlock					|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Private void   *share_allocTaskBlock
(
void   *parms
)
    {
    void   *retVal;

    /*	Allocate a task information block			    */
    retVal = (void *)calloc (1, sizeof (TaskInfo));
    share_printMsg (MSGID_AllocBlock, retVal);

    return retVal;
    }

/*----------------------------------------------------------------------+
|									|
| name		share_clientTaskUnload				    	|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Private void	share_clientTaskUnload
(
void	       *mdlDescP,
long	    	exitReason,
char		*taskIdP
)
    {
    TaskInfo   *taskP = share_getTaskInfoP (mdlDescP);

    /*	Free task block for unloading MDL application		    */
    if (taskP)
    	{
    	share_printMsg (MSGID_UnloadTask, taskP->taskId, taskP);
	if (taskP->prev)
	    taskP->prev->next = taskP->next;
	if (taskP->next)
	    taskP->next->prev = taskP->prev;
	if (taskListP == taskP)
	    taskListP = taskP->next;
	free (taskP);
	taskCount--;

	/*  If no more client tasks, unload the library		    */
	if (!taskCount)
	    {
	    taskListP = NULL;
	    }
    	}
    }

/*----------------------------------------------------------------------+
|									|
| name		share_sharedLibraryUnload				|
|									|
| author	BSI					05/95		|
|									|
+----------------------------------------------------------------------*/
Private int	share_sharedLibraryUnload
(
void	       *mdlDescP,
long	    	exitReason,
unsigned char  *taskIdP
)
    {
    TaskInfo   *taskP;

    share_printMsg (MSGID_UnloadLibrary);

    /*	Free all client specific memory if still allocated	    */
    if (taskListP)
	{
	while (taskP = taskListP)
	    share_clientTaskUnload (taskP->mdlDescrP, 0L, taskP->taskId);
	taskListP = NULL;
	}
    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		share_noMoreClientsHook				    	|
|									|
| author	BSI		 			5/95		|
|									|
+----------------------------------------------------------------------*/
Private void 	share_noMoreClients
(
void
)
    {
    share_printMsg (MSGID_NoMoreClients);
    mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_SILENTUNLOAD, libTaskInfo.taskId,
			      TRUE);
    }

/*----------------------------------------------------------------------+
|									|
|   Hook definitions							|
|									|
+----------------------------------------------------------------------*/
DialogHookInfo	uHooks[] = { {HOOKITEMID_Password, share_passwordHook} };

/*----------------------------------------------------------------------+
|									|
| name		initialize					    	|
|									|
| author	BSI		 			5/95		|
|									|
+----------------------------------------------------------------------*/
Public	int initialize
(
char	       *fileNameP,  	/* <= Client MDL Application	    	*/
char	       *taskIdP,    	/* <= Client MDL Task ID	    	*/
void	       *libDescP,	/* <= Shared Library Descriptor pointer	*/
unsigned long	initParameter	/* <= Initialization parameters     	*/
)
    {
    TaskInfo   *taskInfoP, *taskP;

    /*	Initalize the environment for the client application:	    */
    /*	    1)	Allocate a task block for the application in the    */
    /*	    	shared library tasks memory space		    */
    /*	    2)	If not initialized, set up header pointer for task  */
    /*	    	list and set up the asynch function for handling    */
    /*      	all client tasks terminating, otherwise hook new    */
    /*		block into list					    */
    /*	    3)	Initialize task specific data including access to   */
    /*	    	shared library resources and access to the dialog   */
    /*	    	hook functions via the local dialog box interface   */
    share_printMsg (MSGID_Initialize, mdlSystem_getCurrTaskID ());

    /*	Allocate new task block in the shared library's memory	    */
    /*	space							    */
    if (!(taskInfoP = (void *)mdlDialog_callFunction (libTaskInfo.mdlDescrP,
						      share_allocTaskBlock,
						      NULL)))
	return -1;

    share_printMsg (MSGID_TaskBlockP, taskInfoP);

    /*	Increment client task count and connect new task block into */
    /*	out task list.						    */
    taskCount++;
    if (!initialized)
	{
	share_printMsg (MSGID_InitLibrary);
	taskListP = taskInfoP;
	initialized++;

    	/*  Set up asynch function to handle notification of all    */
    	/*  clients being unloaded				    */
    	mdlShare_setFunction (SHARED_LIB_NO_MORE_CLIENTS, libDescP,
			      share_noMoreClients);
	}
    else
    	{
	share_printMsg (MSGID_LinkTaskInfo);
	taskP = taskListP;
    	while (taskP->next)
	    taskP = taskP->next;
	taskP->next = taskInfoP;
	taskInfoP->prev = taskP;
    	}

    /*	Initialize task specific information including access to    */
    /*	the library's resources and dialog item hook function	    */
    strcpy (taskInfoP->taskId, taskIdP);
    taskInfoP->mdlDescrP = mdlSystem_findMdlDesc (taskIdP);
    taskInfoP->passwordP = &localPassword[0];
    
    /*
    The fully qualified path name of this shared library application is
    used to open the resource files which are linked into
    this library.  These resources are available to any application
    which uses this shared library.
    */
    mdlResource_openFile (&taskInfoP->rscFileH, fullPathName, RSC_READ);

    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    return SUCCESS;
    }

/*----------------------------------------------------------------------+
|									|
| name		main						    	|
|									|
| author	BSI		 			5/95		|
|									|
+----------------------------------------------------------------------*/
Public void main
(
int   argc,		/* => number of args in next array  */
char *argv[]		/* => array of cmd line arguments   */
)
    {
    /* Save the fully qualified path name of the shared library
	for use in mdlResource_openFile during initialize()
    */
    strcpy (fullPathName, argv[0]);

    /*	Open the resource file that we came out of		    */
    mdlResource_openFile (&libTaskInfo.rscFileH, NULL, RSC_READ);

    /*	Load the command table					    */
    mdlParse_loadCommandTable (NULL);

    /*	Publish our hook functions				    */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);
    strcpy (libTaskInfo.taskId, mdlSystem_getCurrTaskID ());

    libTaskInfo.mdlDescrP = mdlSystem_getCurrMdlDesc ();
    strcpy (libTaskInfo.password, "mdlshare");
    libTaskInfo.passwordP = &localPassword[0];

    /*	Init client application unload asynch function		    */
    mdlSystem_setFunction (SYSTEM_ALL_MDL_UNLOADS, share_clientTaskUnload);

    /*	Set up our own unload function				    */
    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM,
		    	   share_sharedLibraryUnload);
    }
