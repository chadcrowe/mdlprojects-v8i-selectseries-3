#--------------------------------------------------------------------------------------
#
#     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlshare.mke,v $
#
#  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
#
#--------------------------------------------------------------------------------------
#---------------------------------------------
#	Define constants specific to this example
#---------------------------------------------
baseDir     	= $(_MakeFilePath)
privateInc  	= $(baseDir)

%include mdl.mki

#----------------------------------------------------------------------
# Create needed output directories if they don't exist
#----------------------------------------------------------------------
$(o)$(tstdir)			: $(o)$(tstdir)

$(rscObjects)$(tstdir)		: $(rscObjects)$(tstdir)

$(reqdObjs)$(tstdir)		: $(reqdObjs)$(tstdir)

appName     	= mdlshare
sAppName    	= mdlsh
appName1     	= mdltest1
sAppName1    	= mdlt1
appName2     	= mdltest2
sAppName2    	= mdlt2

mdlshareObjs	= $(o)$(appName).mo $(mdlLibs)ditemlib.dlo
mdlshareRscs	= $(o)$(appName).mp $(o)$(sAppName)cmd.rsc
mdltest1Objs	= $(o)$(appName1).mo $(o)$(appName).dlo
mdltest1Rscs	= $(o)$(appName1).mp $(o)$(sAppName1)cmd.rsc
mdltest2Objs	= $(o)$(appName2).mo $(o)$(appName).dlo \
	    	  $(mdlLibs)ditemlib.dlo
mdltest2Rscs	= $(o)$(appName2).mp $(o)$(sAppName2)cmd.rsc

#---------------------------------------------
#	Rule to generate a dlo from the dls source file
#	    ----------------------------------
#   	The default rule is being overriden to allow the 
#   	definition of a preprocessor symbol to control the
#   	exporting of runtime/symbolic names via the
#   	$(dlsOpts) make symbol
#---------------------------------------------
.dls.dlo:
    $(msg)
    > $(o)temp.cmd
    -o$@ 
    $(dlsOpts)
%if defined (winNT) || defined (rs6000) || defined (macintosh) || defined (os2)
   -w$(moduleDef)
%endif
    $%$*.dls
    <
    $(dlmspecCmd) @$(o)temp.cmd
    ~time

#---------------------------------------------
#	Generate command table include & resource file using rcomp
#---------------------------------------------
$(genSrc)$(sAppName)cmd.h $(o)$(sAppName)cmd.rsc    : $(baseDir)$(sAppName)cmd.r

$(genSrc)$(sAppName1)cmd.h $(o)$(sAppName1)cmd.rsc  : $(baseDir)$(sAppName1)cmd.r

$(genSrc)$(sAppName2)cmd.h $(o)$(sAppName2)cmd.rsc  : $(baseDir)$(sAppName2)cmd.r

#---------------------------------------------
#	Compile the Dynamic Load Specification
#---------------------------------------------
$(o)$(appName).dlo	    : $(baseDir)$(appName).dls

#---------------------------------------------
#	Compile the MDL files
#---------------------------------------------
$(o)$(appName).mo	    : $(baseDir)$(appName).mc $(baseDir)$(sAppName)ids.h

$(o)$(appName1).mo	    : $(baseDir)$(appName1).mc $(baseDir)$(sAppName1)ids.h

$(o)$(appName2).mo	    : $(baseDir)$(appName2).mc $(baseDir)$(sAppName2)ids.h

#---------------------------------------------
#	Link the MDL Applications
#---------------------------------------------
#	Link the shared library making sure to mark the exported
#	files using the -e option
#---------------------------------------------
$(o)$(appName).mp	    : $(mdlshareObjs) $(o)$(appName).dlo
	$(msg)
	> $(o)make.opt
	$(linkOpts) 
	-e$(o)$(appName).dlo
	-a$@
	$(mdlshareObjs)
	<
	$(MLinkCmd) @$(o)make.opt 
	~time

#---------------------------------------------
#	Link the client MDL Applications including the shared library
#   	DLO file
#---------------------------------------------
$(o)$(appName1).mp	    : $(mdlTest1Objs)
	$(msg)
	> $(o)make.opt
	$(linkOpts) 
	$(mdlTest1Objs)
	-a$@
	<
	$(MLinkCmd) @$(o)make.opt 
	~time

$(o)$(appName2).mp	    : $(mdlTest2Objs)
	$(msg)
	> $(o)make.opt
	$(linkOpts) 
	$(mdlTest2Objs)
	-a$@
	<
	$(MLinkCmd) @$(o)make.opt 
	~time

#---------------------------------------------
#	Merge the dialog resources & MDL program file using rlib
#---------------------------------------------
$(reqdObjs)$(appName).mi	    	: $(mdlshareRscs)
	$(msg)
	> $(o)make.opt
	-o$@
	$(mdlshareRscs)
	<
	$(RLibCmd) @$(o)make.opt
	~time

$(mdlapps)$(appName1).ma	    	: $(mdltest1Rscs)
	$(msg)
	> $(o)make.opt
	-o$@
	$(mdltest1Rscs)
	<
	$(RLibCmd) @$(o)make.opt
	~time

$(reqdObjs)$(appName2).mi	    	: $(mdltest2Rscs)
	$(msg)
	> $(o)make.opt
	-o$@
	$(mdltest2Rscs)
	<
	$(RLibCmd) @$(o)make.opt
	~time

#---------------------------------------------
#   Include language specific make steps
#---------------------------------------------
%include $(baseDir)mdlshrsc.mki
