/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlshare.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdlshare.h_v  $
|   $Workfile:   mdlshare.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application: Shared library      	|
|   	header file						    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__mdlshareH__)
#define __mdlshareH__

#if !defined (__mdlshidsH__)
#   include "mdlshids.h"
#endif

typedef struct taskinfo
    {
    ULong   	rscFileH;	    /* Resource file handle	    	*/
#if defined (resource)
    ULong	    	passwordP;  /* Pointer to src task access str 	*/
    ULong	    	mdlDescrP;  /* Pointer to MDL Descriptor    	*/
    ULong	    	next;	    /* Pointer to next Task Block   	*/
    ULong	    	prev;	    /* Pointer to previous Task Block	*/
#else
    char	       *passwordP;
    void	       *mdlDescrP;
    struct taskinfo    *next;
    struct taskinfo    *prev;
#endif
    char    	taskId[12];	    /* Task Identifier		    	*/
    char    	password[26];       /* Task Password		    	*/
    } TaskInfo;

#endif /* !defined	(__mdlshareH__) */
