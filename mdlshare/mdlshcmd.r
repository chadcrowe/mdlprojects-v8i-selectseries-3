/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlshcmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdlshcmd.r_v  $
|   $Workfile:   mdlshcmd.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:44 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application:			    	|
|	    Shared library command resources			    	|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>

#define	CT_NONE	    0
#define	CT_MAIN	    1
#define	CT_MDLSHARE 2
#define	CT_SET	    3

Table	CT_MAIN =
{
    { 1,    CT_MDLSHARE,    INPUT,  	REQ,    "MDLSHARE" },
};

Table	CT_MDLSHARE =
{
    { 1,    CT_NONE,	    INHERIT,	NONE,	"PASSWORD" },
    { 2,    CT_SET,	    INHERIT,	NONE,	"SET" },
};

Table	CT_SET =
{
    { 1,    CT_NONE,	    INHERIT,	NONE,	"PASSWORD" },
};
