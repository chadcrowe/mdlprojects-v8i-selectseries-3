/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlshids.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdlshids.h_v  $
|   $Workfile:   mdlshids.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:45 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application dialog resource      	|
|   	definitions						    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__mdlshidsH__)
#define __mdlshidsH__

#define	DIALOGID_Password	    12345678

#define	TEXTID_Password		    12345678

#define	HOOKITEMID_Password	    12345678

#define	MESSAGEID_Msgs		    12345678

#define MSGID_PasswordS		    0
#define	MSGID_TaskPasswordLS	    1
#define	MSGID_PasswordVarSL	    2
#define	MSGID_AllocBlock	    3
#define	MSGID_UnloadTask	    4
#define	MSGID_Initialize	    5
#define	MSGID_TaskBlockP	    6
#define	MSGID_InitLibrary	    7
#define	MSGID_LinkTaskInfo	    8
#define	MSGID_UnloadLibrary	    9
#define	MSGID_NoMoreClients	    10

#endif /* !defined	(__mdlshidsH__) */
