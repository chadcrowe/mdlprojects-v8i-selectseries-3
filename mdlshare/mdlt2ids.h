/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlt2ids.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdlt2ids.h_v  $
|   $Workfile:   mdlt2ids.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:50 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application dialog resource      	|
|   	definitions						    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__mdlt2idsH__)
#define __mdlt2idsH__

#include    "mdlshids.h"

#define	DIALOGID_Password2	    87654321

#define	TEXTID_Password2    	    87654321

#define	HOOKITEMID_Password2	    87654321

#define	MSGID_S2Password	    2000
#define	MSGID_S2TaskCount	    2001

#endif /* !defined	(__mdlt2idsH__) */
