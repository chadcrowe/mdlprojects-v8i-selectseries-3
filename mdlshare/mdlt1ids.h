/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mdlshare/mdlt1ids.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mdlshare/mdlt1ids.h_v  $
|   $Workfile:   mdlt1ids.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:39:48 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|   	MDL Shared Library Example Application dialog resource      	|
|   	definitions						    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__mdlt1idsH__)
#define __mdlt1idsH__

#include    "mdlshids.h"

#define	MSGID_S1Password    1000
#define	MSGID_S1TaskCount   1001

#endif /* !defined	(__mdlt1idsH__) */
