/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/toolexample/toolExample.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   toolExample  $
|   $Revision: 1.1.36.1 $
|   	$Date: 2013/07/01 20:40:57 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   toolExample - toolExample source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <mselems.h>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <msstrlst.h>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>

#include    <msrmgr.h>
#include    <mssystem.fdf>
#include    <msparse.fdf>

#include <toolsubs.h>
#include <elementref.h>
#include <msdependency.fdf>
#include <msassoc.fdf>
#include <msmisc.fdf>
#include <mslocate.fdf>
#include <msstate.fdf>
#include <msoutput.fdf>
#include <MicroStationAPI.h>
#include <mstypes.h>
#include <mstmatrx.fdf>

char    *g_str;

#include "toolExampleCmd.h"
#include "toolExample.h"
#include "PlacementExampleTool.h"
#include "ModifyExampleTool.h"

USING_NAMESPACE_BENTLEY_USTN;
USING_NAMESPACE_BENTLEY_USTN_ELEMENT;


/*---------------------------------------------------------------------------------**//**
* @description  toolExample_mdlDialogHook
* @param 	dmP      The DialogMessage 
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" Public void	toolExample_mdlDialogHook
(
DialogMessage   *dmP
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	    {
	    case DIALOG_MESSAGE_CREATE:
	        {
	        dmP->u.create.interests.windowMoving= TRUE;
	        dmP->u.create.interests.resizes     = TRUE;
	        dmP->u.create.interests.updates     = TRUE;
	        dmP->u.create.interests.dialogFocuses = TRUE;

	    // initialize Globals
	    
	        break;
	        }
	    case DIALOG_MESSAGE_WINDOWMOVING:
	        {
	        dmP->u.windowMoving.handled = FALSE;

            /* Don't process if only moving dialog box */
            if (dmP->u.windowMoving.whichCorners == CORNER_ALL ||
	    	    dmP->u.windowMoving.whichCorners == CORNER_ALL_RESIZED)
                break;
	        dmP->u.windowMoving.handled = TRUE;
	        break;
	        }
	    case DIALOG_MESSAGE_INIT:
	        {
	    /* Fall through */
	        }
	    case DIALOG_MESSAGE_UPDATE:
	        {
	    // resize items
	        break;
	        }

	    case DIALOG_MESSAGE_RESIZE:
	        {
            /* Don't process if only moving dialog box */
            if (dmP->u.resize.whichCorners == CORNER_ALL)
                break;
	       
	        dmP->u.resize.forceCompleteRedraw = TRUE;
	        break;
	        }
	    case DIALOG_MESSAGE_FOCUSIN:
	        {
	        break;
	        }
	    case DIALOG_MESSAGE_DESTROY:
	        {
	        // save globals
	        break;
	        }
	    default:
	        dmP->msgUnderstood = FALSE;
	        break;
	    }
    }


 
/*----------------------------------------------------------------------+*//**
* 							      
* @bsimethod toolExamplePlaceCmd   				      
* @param 	unparsedP     Contination string that started this command
*							      
* Author:   BSI 				06/03 	      
*							      
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT void toolExamplePlaceCmd
(
char    *unparsed
)
    {

    PlacementExampleTool *pTool = new PlacementExampleTool(CMD_TOOLEXAMPLE_ACTION_PLACE,0);

    if (unparsed)
        pTool->m_str = dlmSystem_strdup (unparsed);
    else
        pTool->m_str = dlmSystem_strdup("test");

    pTool->MyInstallTool();
   
    /*-----------------------------------------------------------------
    | Start dynamics to display elements when one more datapoint will 
    | give enough information to write an element to the design file.
    | For our text element, we can start dynamics right away.
    +-----------------------------------------------------------------*/
    //mdlState_setFunction (STATE_COMPLEX_DYNAMICS, generateText);
    }
/*---------------------------------------------------------------------------------**//**
* @description  toolExample_mdlCommand
* @param 	unparsed      The unparsed information sent to the command
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT void toolExample_mdlCommand 
(
char * unparsed
)
    {
	ModifyExampleTool *pTool = new ModifyExampleTool ();

    pTool->MyInstallTool();
    }
/*---------------------------------------------------------------------------------**//**
* @description  MdlMain
* @param 	argc      The number of command line parameters sent to the application.
* @param 	argv[]    The array of strings sent to the application on the command line.
* @bsimethod 							BSI             06/03
+---------------+---------------+---------------+---------------+---------------+------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
	RscFileHandle   rfHandle;

    mdlResource_openFile (&rfHandle,NULL,RSC_READONLY);

    // Map command name to function (usage: MDL COMMAND COMPEXPORT)
    static  MdlCommandName cmdNames[] = 
    {
        {toolExample_mdlCommand, "toolExample_mdlCommand"  },
        {toolExamplePlaceCmd, "toolExamplePlaceCmd"  },
        0,
    };

    mdlSystem_registerCommandNames (cmdNames);

    // Map key-in to function
    static MdlCommandNumber cmdNumbers[] =
    {
		{toolExample_mdlCommand,  CMD_TOOLEXAMPLE_ACTION_DIALOG },
		{toolExamplePlaceCmd,  CMD_TOOLEXAMPLE_ACTION_PLACE },
        0,
    };

    mdlSystem_registerCommandNumbers (cmdNumbers);

    mdlParse_loadCommandTable (NULL);

 	return SUCCESS;
    }
