/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/toolexample/ModifyExampleTool.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   toolExample  $
|   $Revision: 1.1.36.1 $
|   	$Date: 2013/07/01 20:40:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   toolExample - toolExample source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <WString.h>
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <mselems.h>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <msstrlst.h>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>

#include    <msrmgr.h>
#include    <mssystem.fdf>
#include    <msparse.fdf>
#include    <RefCounted.h>
#include    <toolsubs.h>

#include <elementref.h>
#include <msdependency.fdf>
#include <msassoc.fdf>
#include <msmisc.fdf>
#include <mslocate.fdf>
#include <msstate.fdf>
#include <msoutput.fdf>
#include <mstypes.h>
#include <mstmatrx.fdf>
#include <MicroStationAPI.h>

//#pragma once
#include "toolExampleCmd.h"
USING_NAMESPACE_BENTLEY;
USING_NAMESPACE_BENTLEY_USTN;
USING_NAMESPACE_BENTLEY_USTN_ELEMENT;

class ModifyExampleTool :MstnElementSetTool
    {
    private:
        int                 m_noMotionCount; // number of times "noMotion" has been called since last motion

    public:

        virtual bool        WantAccuSnap () {return false;}
        virtual bool        NeedPointForDynamics () {return SOURCE_Pick == GetElemSource ();} // Can check GetHitSource to detect EditAction
        virtual bool        OnPostLocate (HitPathCP path, char *cantAcceptReason) override;
        virtual bool        OnModelNoMotion (MstnButtonEventCP) override;
        virtual StatusInt   OnElementModify (EditElemHandleR elHandle) override;
        virtual void        OnRestartCommand () override;
        virtual void        OnPostInstall () override;
        virtual EditElemHandleP BuildLocateAgenda (HitPathCP path, MstnButtonEventCP ev) override;
        virtual bool        OnInstall () override;
        virtual void        MyInstallTool();
    };
