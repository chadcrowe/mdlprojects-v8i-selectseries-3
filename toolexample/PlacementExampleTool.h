/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/toolexample/PlacementExampleTool.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   toolExample  $
|   $Revision: 1.1.36.1 $
|   	$Date: 2013/07/01 20:40:57 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   toolExample - toolExample source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include    <mdl.h>
#include    <stdio.h>
#include    <string.h>
#include    <malloc.h>
#include    <mselemen.fdf>
#include    <mselmdsc.fdf>
#include    <mslinkge.fdf>
#include    <msscancrit.fdf>
#include    <mstagdat.fdf>
#include    <mselems.h>
#include    <mscell.fdf>
#include    <leveltable.fdf>
#include    <mslstyle.fdf>
#include    <msstrlst.h>
#include    <mscnv.fdf>
#include    <msdgnobj.fdf>
#include    <msmodel.fdf>
#include    <msview.fdf>
#include    <msviewinfo.fdf>
#include    <msvar.fdf>
#include    <dlmsys.fdf>
#include    <msdialog.fdf>

#include    <msrmgr.h>
#include    <mssystem.fdf>
#include    <msparse.fdf>

#include <toolsubs.h>

#include <elementref.h>
#include <msdependency.fdf>
#include <msassoc.fdf>
#include <msmisc.fdf>
#include <mslocate.fdf>
#include <msstate.fdf>
#include <msoutput.fdf>
#include <mstypes.h>
#include <MicroStationAPI.h>
#include <mstmatrx.fdf>

#include "toolExampleCmd.h"
USING_NAMESPACE_BENTLEY_USTN;
USING_NAMESPACE_BENTLEY_USTN_ELEMENT;

/*=================================================================================**//**
* Simple placement tool to create a text element by 1 point.
* @bsiclass
+===============+===============+===============+===============+===============+======*/
class PlacementExampleTool:MstnPrimitiveTool
    {
    protected:
        DPoint3d m_origin;
        bool     m_haveOrigin;
    public:
        char     *m_str;

        PlacementExampleTool (int cmdNumber, int cmdName):MstnPrimitiveTool ( cmdNumber,  cmdName,0){m_haveOrigin=true;}
        virtual bool OnInstall () override;
        virtual void OnPostInstall() override;
        virtual void OnCleanup() override;
        virtual void OnRestartCommand() override;

        virtual void OnComplexDynamics(MstnButtonEventCP ev);
        virtual bool OnResetButton (MstnButtonEventCP ev) override {OnReinitialize (); return true;}
        virtual bool OnDataButton (MstnButtonEventCP ev) override;

        virtual bool CreateElement (EditElemHandleR eeh, DPoint3dR origin,MstnButtonEventCP ev);
        //created this method since the InstallTool is protected and only accessable with in the class.
        virtual void MyInstallTool();
    };