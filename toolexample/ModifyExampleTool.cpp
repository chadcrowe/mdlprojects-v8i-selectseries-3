/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/toolexample/ModifyExampleTool.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   toolExample  $
|   $Revision: 1.1.36.1 $
|   	$Date: 2013/07/01 20:40:56 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   toolExample - toolExample source code. |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/

#include "ModifyExampleTool.h"

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
bool            ModifyExampleTool::OnPostLocate (HitPathCP path, char *cantAcceptReason)
    {
    if (!__super::OnPostLocate (path, cantAcceptReason))
        return false;

    // Only allow elements of our type to be located...
    ElemHandle  eh (mdlDisplayPath_getElem ((DisplayPathP) path, 0), mdlDisplayPath_getPathRoot ((DisplayPathP) path));

    return true;
    }

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
StatusInt       ModifyExampleTool::OnElementModify (EditElemHandleR eeh)
    {

    return SUCCESS;
    }
/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
bool            ModifyExampleTool::OnModelNoMotion (MstnButtonEvent const *ev) 
  {
  //  DisplayInfoBalloon (ev);
    return true;
  }
/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
void            ModifyExampleTool::OnRestartCommand ()
    {
    ModifyExampleTool* newTool = new ModifyExampleTool ();

    newTool->InstallTool ();
    }
/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
EditElemHandleP  ModifyExampleTool::BuildLocateAgenda (HitPathCP path, MstnButtonEventCP ev)
    {
    // Here we have both the new agenda entry and the current hit path if needed...
    EditElemHandleP elHandle = __super::BuildLocateAgenda (path, ev);

    return  elHandle;
    }
/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
void            ModifyExampleTool::OnPostInstall ()
    {
    __super::OnPostInstall ();
    mdlAccuDraw_setEnabledState (false); // Don't enable AccuDraw w/Dynamics...
    }

/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
bool            ModifyExampleTool::OnInstall ()
    {
    if (!__super::OnInstall ())
        return false;

    SetCmdNumber (0);      // For toolsettings/undo string...
    SetCmdName (0, 0);  // For command prompt...

    return true;
    }
/*---------------------------------------------------------------------------------**//**
* @bsimethod
+---------------+---------------+---------------+---------------+---------------+------*/
void    ModifyExampleTool::MyInstallTool()
    {
    InstallTool();
    }

