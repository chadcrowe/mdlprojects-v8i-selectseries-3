/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/treexmpl/treexmpl.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: treexmpl.h,v $
|   $Revision: 1.5.52.1 $
|	$Date: 2013/07/01 20:40:58 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   treexmpl.h								|
|									|
+----------------------------------------------------------------------*/
#if !defined (__treexmplH__)
#define __treexmplH__

#include    <basetype.h>

/*----------------------------------------------------------------------+
|									|
|   Dialog ID's								|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_treexmpl		1

/*----------------------------------------------------------------------+
|									|
|   Item ID's								|
|									|
+----------------------------------------------------------------------*/
#define	TREEID_TreeExample		1

#define	LISTBOXID_SingleColumn		1
#define	LISTBOXID_MultiColumn		2

#define	SASHID_VExample			1

#define	CTPANELID_Detail		1

#define	DILISTID_SingleColumn		1
#define	DILISTID_MultiColumn		2
#define	DILISTID_Tabs			3
#define DILISTID_Simple			4

#define	CONTAINERID_SingleColumn	1
#define	CONTAINERID_MultiColumn		2
#define	CONTAINERID_Tabs		3
#define CONTAINERID_Simple		4

#define COMBOBOXID_Test1		1
#define COMBOBOXID_Tree                 2

#define RBLISTID_Options1		1
#define RBUTTONID_Option1		1
#define RBUTTONID_Option2		2
#define RBUTTONID_Option3		3

#define SCALEID_Range1			1
#define SCALEID_Range2			2

#define SPINBOXID_Test1			1

#define TPLISTID_ONE			1
#define TABPAGEID_General		1
#define TABPAGEID_Specific1		2
#define TABPAGEID_Specific2		3

#define TEXTID_Range1			1
#define TEXTID_Range2			2

#define TOGGLEID_Demo1			1
#define TOGGLEID_Demo2			2
#define TOGGLEID_Demo3			3

/*----------------------------------------------------------------------+
|									|
|   Icon ID's								|
|									|
+----------------------------------------------------------------------*/
#define	ICONID_Music			1
#define	ICONID_Container		2

/*----------------------------------------------------------------------+
|									|
|   Hook ID's								|
|									|
+----------------------------------------------------------------------*/
#define	HOOKDIALOGID_treexmpl		1
#define	HOOKITEMID_Tree			2
#define	HOOKITEMID_SingleListBox	3
#define	HOOKITEMID_MultiListBox		4
#define	HOOKITEMID_Sash_VExample	5
#define	HOOKITEMID_ComboBoxes		6
#define HOOKITEMID_Simple		7
#define HOOKITEMID_CTPanel		8
#define	HOOKITEMID_TreeComboBox		9

/*----------------------------------------------------------------------+
|									|
|   Synonym ID's							|
|									|
+----------------------------------------------------------------------*/
#define SYNONYMID_ComboBox		1
#define SYNONYMID_Range1		2
#define SYNONYMID_Range2		3
#define SYNONYMID_RadioButtons1		4

/*----------------------------------------------------------------------+
|									|
|   Message list defines						|
|									|
+----------------------------------------------------------------------*/
#define	MESSAGELISTID_Messages		1

/*----------------------------------------------------------------------+
|									|
|   Message List Entry Ids						|
|									|
+----------------------------------------------------------------------*/
#define	MESSAGEID_DialogOpenError   	1
#define	MESSAGEID_ResourceLoadError	2


#if !defined (resource)
typedef	struct treexmplinfo
    {
    int		    containerId;
    void	    *pSingleLM;
    void	    *pMultiLM;
    void	    *pSingleLBRiP;
    void	    *pMultiLBRiP;
    void	    *pTreeModel;
    int		    minSashX;
    int		    maxSashX;
    double	    range1;
    int		    range2;
    double	    comboBoxValue;
    int		    radioButtonValue;
    double	    spinBoxValue;
//    long            treeComboBoxValue;
    MSWChar         treeComboBoxValue[256];

    unsigned long   toggle1:1;
    unsigned long   toggle2:1;
    unsigned long   toggle3:1;
    unsigned long   resBits:29;

    } TreeXmplInfo;
#endif

#endif
