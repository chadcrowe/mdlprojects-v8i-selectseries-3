/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/treexmpl/english/treextxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   treextxt.h - language specific definitions 			    	|
|									|
+----------------------------------------------------------------------*/
#if !defined (__treextxtH__)
#define __treextxtH__

/*----------------------------------------------------------------------+
|																		|
|   Defines																|
|		   																|
+----------------------------------------------------------------------*/
#define TXT_DialogTitle		    "Tree Example"
#define TXT_Work		    "Work"
#define TXT_WorkId		    "Id"
#define TXT_Year		    "Year"
#define TXT_Tree		    "Tree"
#define TXT_General		    "General"
#define TXT_Specific1		    "Specific-1"
#define TXT_Specific2		    "Specific-2"
#define TXT_ComboBox1		    "ComboBox 1"
#define TXT_Range1		    "Range 1"
#define TXT_Range2		    "Range 2"
#define TXT_RadioButtons1	    "Radio Buttons"
#define TXT_RadioButton1	    "Radio Button 1"
#define TXT_RadioButton2	    "Radio Button 2"
#define TXT_RadioButton3	    "Radio Button 3"
#define TXT_SpinBox1		    "Spin Box 1"
#define TXT_Toggle1		    "Toggle 1"
#define TXT_Toggle2		    "Toggle 2"
#define TXT_Toggle3		    "Toggle 3"
#define TXT_TreeComboBox            "Tree Combo"

#endif /* #if !defined (__treextxtH__) */
