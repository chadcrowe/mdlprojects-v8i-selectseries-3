/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/treexmpl/english/treexmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: treexmsg.r,v $
|   $Revision: 1.2.78.1 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "treexmpl.h"

/*----------------------------------------------------------------------+
|									|
|   Messages List Resource Definition					|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_Messages =
{
    {
    {MESSAGEID_DialogOpenError,		"Unable to create/open Dialog treexmpl"},
    {MESSAGEID_ResourceLoadError,	"Resource could not be loaded"},
    }
};

