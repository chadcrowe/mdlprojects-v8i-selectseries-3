/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/treexmpl/treexmpl.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: treexmpl.cpp,v $
|   $Revision: 1.1.32.1 $
|	$Date: 2013/07/01 20:40:58 $
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Include Files							|
|									|
+----------------------------------------------------------------------*/

#include    <mdl.h>

#include    <stdlib.h>
#include    <stdio.h>
#include    <string.h>

#include    <cmdlist.h>	    /* MicroStation command numbers */
#include    <dlogids.h>
#include    <dlogitem.h>
#include    <mdlerrs.h>

#include    <ditemlib.fdf>
#include    <dlogman.fdf>
#include    <mscexpr.fdf>
#include    <mscnv.fdf>
#include    <msdialog.fdf>
#include    <msoutput.fdf>
#include    <msritem.fdf>
#include    <msrsrc.fdf>
#include    <msstate.fdf>
#include    <mssystem.fdf>
#include    <toolsubs.h>
#include    <listmodel.fdf>
#include    <treemodel.fdf>

#include    "treexmpl.h"

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
TreeXmplInfo    MYInfo;

typedef struct  mytreedata
    {
    MSWChar	*pwDisplayText;
    BoolInt     bAllowsChildren;
    int		containerId;
    struct mytreedata   **children;
    struct mymusicdata  *musicdata;
    int		numMusic;
    } MyTreeData;

typedef struct  mymusicdata
    {
    MSWChar	*pwWork;
    char	*pWorkId;
    int		year;
    } MyMusicData;

/* Different presentations within a ContainerPanel */
MyTreeData  Simple = 
	    {L"Simple", FALSE, CONTAINERID_Simple, NULL, NULL};
MyTreeData  Table = 
	    {L"Table", FALSE, CONTAINERID_MultiColumn, NULL, NULL};
MyTreeData  Tabs = 
	    {L"Tabs", FALSE, CONTAINERID_Tabs, NULL, NULL};
MyTreeData  *aContainerPanel[] =
	    {&Simple, &Table, &Tabs, NULL};
MyTreeData  Containers = 
	    {L"Containers", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aContainerPanel, NULL};

/* --- Hierarchy of data nodes --- */

/* Composers */
MyMusicData aByrd[] =
	    {
	    {L"Great Service", "", 1575},
	    {L"Sacred Motets", "", 1589},
	    {L"Masses in 3, 4 & 5 Parts", "", 1593},
	    };
MyTreeData  Byrd = 
	    {L"Byrd, William", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aByrd, (sizeof(aByrd)/sizeof(MyMusicData))};

MyMusicData aMonteverdi[] =
	    {
	    {L"La Favola d' Orfeo", "", 1607},
	    {L"Vespro della Beata Vergine ", "", 1610},
	    {L"L'Incoronazione di Poppea ", "", 1642},
	    };
MyTreeData  Monteverdi = 
	    {L"Monteverdi, Claudio", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aMonteverdi, (sizeof(aMonteverdi)/sizeof(MyMusicData))};

MyMusicData aPalestrina[] =
	    {
	    {L"29 Motets for 5 Voices from Cantico Canticorum", "", 1590},
	    {L"Mass \"Papae Marcelli\"", "", 1591},
	    {L"Mass \"Hodie Christus natus est\"", "", 1592},
	    };
MyTreeData  Palestrina = 
	    {L"Palestrina, Giovanni Pierluigi da", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aPalestrina, (sizeof(aPalestrina)/sizeof(MyMusicData))};


MyMusicData aBach[] =
	    {
	    {L"Mass in B minor", "BWV 232", 1720},
	    {L"\"Goldberg\" Variations", "BWV 988", 1730},
	    {L"6 \"Brandenburg\" Concertos", "BWV 1046-1051", 1740},
	    {L"The Art Of Fugue", "BWV 1080", 1747},
	    };
MyTreeData  Bach = 
	    {L"Bach, Johann Sebastian", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aBach, (sizeof(aBach)/sizeof(MyMusicData))};

MyMusicData aHandel[] =
	    {
	    {L"6 Concerti Grossi", "Op. 3, HWV 312-317", 1720},
	    {L"Messiah", "", 1728},
	    {L"3 Suites \"Water Music\"", "HWV 348-350", 1747},
	    };
MyTreeData  Handel = 
	    {L"Handel, George Frideric", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aHandel, (sizeof(aHandel)/sizeof(MyMusicData))};

MyMusicData aVivaldi[] =
	    {
	    {L"The Four Seasons", "Op. 8", 1720},
	    {L"Gloria", "RV 589", 1728},
	    {L"Concertos (for violin, bassoon, cello, oboe, flute, etc.)", "", 1747},
	    };
MyTreeData  Vivaldi = 
	    {L"Vivaldi, Antonio", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aVivaldi, (sizeof(aVivaldi)/sizeof(MyMusicData))};

MyMusicData aBeethoven[] =
	    {
	    {L"Fidelio", "Op. 72c", 1814},
	    {L"Missa Solemnis in D Major", "Op. 123", 1822},
	    {L"Symphony #9 \"Choral\"", "Op. 125", 1824},
	    };
MyTreeData  Beethoven = 
	    {L"Beethoven, Ludwig van", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aBeethoven, (sizeof(aBeethoven)/sizeof(MyMusicData))};

MyMusicData aHaydn[] =
	    {
	    {L"\"Lord Nelson\" Mass", "Ho. XXII:11", 1798},
	    {L"The Creation (\"Die Schöpfung\")", "HO. XXI:2", 1798},
	    {L"The Seasons (\"Die Jahreszeiten\")", " XXI:3  ", 1801},
	    };
MyTreeData  Haydn = 
	    {L"Haydn, Franz Joseph", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aHaydn, (sizeof(aHaydn)/sizeof(MyMusicData))};

MyMusicData aMozart[] =
	    {
	    {L"Marriage of Figaro (\"Le nozze di Figaro\")", "K. 492", 1785},
	    {L"Don Giovanni", "K.537", 1788},
	    {L"Cosi von Tutte", "K. 588", 1790},
	    {L"Magic Flute (\"Die Zauberflöte\")", "K. 620", 1791},
	    };
MyTreeData  Mozart = 
	    {L"Mozart, Wolfgang Amadeus", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aMozart, (sizeof(aMozart)/sizeof(MyMusicData))};

MyMusicData aBrahms[] =
	    {
	    {L"Requiem (\"Ein Deutsches Requiem\")", " Op. 45", 1860},
	    {L"4 Symphonies", "Op. 68, 78, 90, 98", 1860},
	    {L"Piano Concerto #2", "Op. 83", 1860},
	    {L"Violin Concerto", "Op. 83", 1860},
	    };
MyTreeData  Brahms = 
	    {L"Brahms, Johannes", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aBrahms, (sizeof(aBrahms)/sizeof(MyMusicData))};

MyMusicData aDvorak[] =
	    {
	    {L"Stabat Mater", "Op. 58", 1877},
	    {L"Symphony #9 (\"New World\")", "Op. 95", 1893},
	    };
MyTreeData  Dvorak = 
	    {L"Dvorak, Antonin", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aDvorak, (sizeof(aDvorak)/sizeof(MyMusicData))};

MyMusicData aTchaikovsky[] =
	    {
	    {L"\"Swan Lake\" Ballet", "Op. 20", 1893},
	    {L"Piano Concerto #1", "Op. 23", 1893},
	    {L"Violin Concerto", "Op. 35", 1893},
	    {L"\"Nutcracker\" Ballet", "Op. 74", 1893},
	    {L"Symphony #6 (\"Pathetique\")", "Op. 95", 1893},
	    };
MyTreeData  Tchaikovsky = 
	    {L"Tchaikovsky, Petr Ilyitch", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aTchaikovsky, (sizeof(aTchaikovsky)/sizeof(MyMusicData))};


MyMusicData aBartok[] =
	    {
	    {L"Miraculous Mandarin", "", 1918},
	    {L"Cantata Profana ", "", 1930},
	    {L"Concerto for Orchestra", "", 1942},
	    };
MyTreeData  Bartok = 
	    {L"Bartok, Bela", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aBartok, (sizeof(aBartok)/sizeof(MyMusicData))};

MyMusicData aCopland[] =
	    {
	    {L"Fanfare for the Common Man", "", 1942},
	    {L"\"Rodeo\" Ballet", "", 1942},
	    {L"\"Appalachian Spring\" Ballet", "", 1943},
	    {L"Symphony #3", "", 1946},
	    };
MyTreeData  Copland = 
	    {L"Copland, Aaron", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aCopland, (sizeof(aCopland)/sizeof(MyMusicData))};

MyMusicData aStravinsky[] =
	    {
	    {L"\"Firebird\" Ballet", "", 1910},
	    {L"\"Petruska\" Ballet", "", 1911},
	    {L"\"Rite of Spring\" Ballet", "", 1912},
	    };
MyTreeData  Stravinsky = 
	    {L"Stravinsky, Igor", FALSE, CONTAINERID_MultiColumn, NULL, 
		(MyMusicData *) aStravinsky, (sizeof(aStravinsky)/sizeof(MyMusicData))};

/* Renaissance musical periods */
MyTreeData  *aRenaissance[] =
	    {&Byrd, &Monteverdi, &Palestrina, NULL};
MyTreeData  Renaissance = 
	    {L"Renaissance", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aRenaissance, NULL};
MyTreeData  *aBaroque[] =
	    {&Bach, &Handel, &Vivaldi, NULL};
MyTreeData  Baroque = 
	    {L"Baroque", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aBaroque, NULL};
MyTreeData  *aClassical[] =
	    {&Beethoven, &Haydn, &Mozart, NULL};
MyTreeData  Classical = 
	    {L"Classical", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aClassical, NULL};
MyTreeData  *aRomantic[] =
	    {&Brahms, &Dvorak, &Tchaikovsky, NULL};
MyTreeData  Romantic = 
	    {L"Romantic", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aRomantic, NULL};
MyTreeData  *aContemporary[] =
	    {&Bartok, &Copland, &Stravinsky, NULL};
MyTreeData  Contemporary = 
	    {L"Contemporary", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aContemporary, NULL};

/* Music node */
MyTreeData  *aMusic[] =
	    {&Renaissance, &Baroque, &Classical, &Romantic, &Contemporary, NULL};
MyTreeData  Music = 
	    {L"Classical Music", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aMusic, NULL};

/* Root node */
MyTreeData  *aRoot[] =
	    {&Containers, &Music, NULL};
MyTreeData  Root = 
	    {L"Presentations", TRUE, CONTAINERID_SingleColumn, (MyTreeData **) aRoot, NULL};

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_adjustVSashDialogItems				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_adjustVSashDialogItems
(
DialogBox       *db,
BoolInt		forceCTErase,
BSIRect		*pOldContent,
BoolInt		refreshItems
)
    {
    DialogItem	*sashDiP;
    DialogItem	*treeDiP;
    DialogItem	*ctPanelDiP;
    BoolInt     eraseFirst;
    
    /* Get the 3 main items on the dialog */
    sashDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Sash, SASHID_VExample, 0);
    treeDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_Tree, TREEID_TreeExample, 0);
    ctPanelDiP = mdlDialog_itemGetByTypeAndId (db, RTYPE_ContainerPanel, CTPANELID_Detail, 0);

    /* Only erase-first if forced or if non- ALIGN_PARENT container */
    eraseFirst = forceCTErase || (MYInfo.containerId != CONTAINERID_SingleColumn &&
				  MYInfo.containerId != CONTAINERID_MultiColumn);

    /* Resize, reposition and draw the items */
    mdlDialog_itemsAdjustFlushWithSash (db, NULL, pOldContent, sashDiP, 
					treeDiP, refreshItems, refreshItems,
					ctPanelDiP, eraseFirst, refreshItems);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_nodeLoadChildren				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_nodeLoadChildren
(
GuiTreeModel    *pModel,
GuiTreeNode     *pParentNode
)
    {
    GuiTreeNode     *pNode;
    MyTreeData      *pMyData;
    MyTreeData      **pChildren;
    MyTreeData      *pChild;
    ValueDescr      valueDescr;

    mdlTreeNode_getValue (pParentNode, &valueDescr);
    pMyData = (MyTreeData *) valueDescr.value.voidPFormat;
    pChildren  = pMyData->children;

    pChild = *pChildren;
    while (pChild != NULL)
	{
	pNode = mdlTreeNode_create (pModel, pChild->bAllowsChildren);
	mdlTreeNode_setDisplayTextW (pNode, pChild->pwDisplayText);
	mdlTreeNode_setEditor (pNode, 0, 0, NULL, 1, 1);

	valueDescr.formatType = FMT_VOIDP;
	valueDescr.value.voidPFormat = pChild;
	mdlTreeNode_setValue (pNode, &valueDescr, FALSE);

	if  (pChild->bAllowsChildren)
	    {
	    mdlTreeNode_setCollapsedIcon (pNode, ICONID_SubDirectoryFolder, RTYPE_Icon, NULL);
	    mdlTreeNode_setExpandedIcon (pNode, ICONID_OpenDirectoryFolder, RTYPE_Icon, NULL);
	    mdlTreeNode_setExpandedSelectedIcon (pNode, ICONID_CurrentDirectoryFolder, RTYPE_Icon, NULL);
	    }
	else if (pMyData == &Containers)
	    {
	    mdlTreeNode_setLeafIcon (pNode, ICONID_Container, RTYPE_Icon, mdlSystem_getCurrMdlDesc());
	    }
	else
	    {
	    mdlTreeNode_setLeafIcon (pNode, ICONID_Music, RTYPE_Icon, mdlSystem_getCurrMdlDesc());
	    }

	mdlTreeNode_addChild (pParentNode, pNode);

	pChildren++;
	pChild = *pChildren;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_showChildrenInList				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_showChildrenInList
(
GuiTreeModel    *pModel,
GuiTreeNode     *pParentNode
)
    {
    ListRow	*pRow;
    ListCell	*pCell;
    GuiTreeNode *pChildNode;
    int	iconRscId=0;    
    int	iconRscType=0;
    void	*iconOwnerMD=0;
    MSWChar	*pwDisplayText;

    /* Empty the ListModel of its current rows */
    mdlListModel_empty ((ListModel*)MYInfo.pSingleLM, TRUE);

    /* Loop thru the children of this tree node */
    pChildNode = mdlTreeNode_getFirstChild (pParentNode);
    while (pChildNode)
	{
	pRow = mdlListRow_create ((ListModel*)MYInfo.pSingleLM);
	if  (pRow != NULL)
	    {
	    pCell = mdlListRow_getCellAtIndex (pRow, 0);
	    if  (pCell != NULL)
	    	{
		/* Copy current icon from tree node to list row */
		mdlTreeNode_getCurrentIcon (pChildNode, &iconRscId, &iconRscType, &iconOwnerMD);
		mdlListCell_setIcon (pCell, iconRscId, iconRscType, iconOwnerMD);

		/* Copy display text from tree node to list row */
		mdlTreeNode_getDisplayTextW (pChildNode, &pwDisplayText);
		mdlListCell_setDisplayTextW (pCell, pwDisplayText);

		/* Set infoField in list row to the GuiTreeNode ptr */
		mdlListCell_setInfoField (pCell, 0, (long) pChildNode);

		mdlListModel_addRow ((ListModel*)MYInfo.pSingleLM, pRow);
	    	}
	    else
		break;
	    }
	else
	    break;
	
	pChildNode = mdlTreeNode_getNextChild (pParentNode, pChildNode);
	}

    /* Update the data model in the listbox */
    if  (MYInfo.pSingleLBRiP != NULL)
    	{
	mdlDialog_listBoxNRowsChangedRedraw ((RawItemHdr*)MYInfo.pSingleLBRiP, FALSE);
    	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_showNodeDetail     				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_showNodeDetail
(
GuiTreeModel    *pModel,
GuiTreeNode     *pLeafNode
)
    {
    MyTreeData      *pMyData;
    MyMusicData     *pMusic;
    int		    numMusic;
    ValueDescr      valueDescr;
    ListRow	    *pRow;
    ListCell	    *pCell;
    char	    buffer[256];
    int		    i;

    /* Empty the ListModel of its current rows */
    mdlListModel_empty ((ListModel *)MYInfo.pMultiLM, TRUE);

    mdlTreeNode_getValue (pLeafNode, &valueDescr);

    pMyData     = (MyTreeData *) valueDescr.value.voidPFormat;
    pMusic      = pMyData->musicdata;
    numMusic    = pMyData->numMusic;

    if  (pMusic)
    	{
	for (i=0; i<numMusic; i++)
	    {
	    pRow = mdlListRow_create ((ListModel *)MYInfo.pMultiLM);
	    if	(pRow)
		{
		pCell = mdlListRow_getCellAtIndex (pRow, 0);
		mdlListCell_setStringValueW (pCell, pMusic->pwWork, TRUE);

		pCell = mdlListRow_getCellAtIndex (pRow, 1);
		mdlListCell_setStringValue (pCell, pMusic->pWorkId, TRUE);

		pCell = mdlListRow_getCellAtIndex (pRow, 2);
		sprintf (buffer, "%d", pMusic->year);
		mdlListCell_setStringValue (pCell, buffer, TRUE);

		/* Append row to end of list */
		mdlListModel_addRow ((ListModel *)MYInfo.pMultiLM, pRow);
		}

	    pMusic++;
	    }
    	}
    else    /* Fill with demo data */
    	{
	for (i=0; i<=20; i++)
	    {
	    pRow = mdlListRow_create ((ListModel *)MYInfo.pMultiLM);
	    if	(pRow)
		{
		pCell = mdlListRow_getCellAtIndex (pRow, 0);
		sprintf (buffer, "Work %d", i);
		mdlListCell_setDisplayText (pCell, buffer);
		mdlListCell_setLongValue (pCell, i);

		pCell = mdlListRow_getCellAtIndex (pRow, 1);
		sprintf (buffer, "Opus %d", i);
		mdlListCell_setDisplayText (pCell, buffer);
		mdlListCell_setLongValue (pCell, i);

		pCell = mdlListRow_getCellAtIndex (pRow, 2);
		sprintf (buffer, "%d", 1900+i);
		mdlListCell_setDisplayText (pCell, buffer);
		mdlListCell_setLongValue (pCell, i);

		/* Append row to end of list */
		mdlListModel_addRow ((ListModel *)MYInfo.pMultiLM, pRow);
		}
	    }
	}

    if  (MYInfo.pMultiLBRiP != NULL)
    	{
	mdlDialog_listBoxNRowsChangedRedraw ((RawItemHdr*)MYInfo.pMultiLBRiP, FALSE);
    	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_treeHook					|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_treeHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    GuiTreeModel    *pModel;
    DialogItem      *diP = dimP->dialogItemP;
    RawItemHdr      *riP = diP->rawItemP;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    GuiTreeNode     *pRoot;
	    ValueDescr      valueDescr;

	    /* Create TreeModel with 1 column */
	    pModel = mdlTreeModel_create (1);   
	    
	    /* Get root node from the TreeModel */
	    pRoot = mdlTreeModel_getRootNode (pModel);
	    mdlTreeNode_setDisplayTextW (pRoot, Root.pwDisplayText);

	    valueDescr.formatType = FMT_VOIDP;
	    valueDescr.value.voidPFormat = &Root;
	    mdlTreeNode_setValue (pRoot, &valueDescr, FALSE);

	    mdlTreeNode_setCollapsedIcon (pRoot, ICONID_VolumeFolder, RTYPE_Icon, NULL);
	    mdlTreeNode_setExpandedIcon (pRoot, ICONID_OpenVolumeFolder, RTYPE_Icon, NULL);
	    mdlTreeNode_setExpandedSelectedIcon (pRoot, ICONID_CurrentVolumeFolder, RTYPE_Icon, NULL);

	    /* Load the root's children; other parent nodes will load their */
	    /* children and expand during the STATECHANGING message */
	    treexmpl_nodeLoadChildren (pModel, pRoot);

	    /* Start with the root expanded */
	    mdlTreeNode_expand (pRoot);

	    mdlDialog_treeSetTreeModelP (riP, pModel);

	    MYInfo.pTreeModel = pModel;

	    break;
	    }

	case DITEM_MESSAGE_STATECHANGING:
	    {
	    GuiTreeNode     *pNode;
	    int		    row;

	    if  (dimP->u.stateChanging.changeType == STATECHANGING_TYPE_NODEEXPANDING)
	    	{
		pModel = mdlDialog_treeGetTreeModelP (riP);

		row = dimP->u.stateChanging.row;

		/* Get this parent node & if not already populated, fill it */
		if (NULL != (pNode = mdlTreeModel_getDisplayRowAtIndex (pModel, row)) &&
		    !mdlTreeNode_isParentPopulated (pNode))
		    {
		    treexmpl_nodeLoadChildren (pModel, pNode);
		    }
		}

	    dimP->u.stateChanging.response = STATECHANGING_RESPONSE_ALLOW;
	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
	    int		    row=-1, col=-1;
	    BoolInt	    found;
	    GuiTreeNode     *pNode;

	    pModel = mdlDialog_treeGetTreeModelP (riP);

	    mdlDialog_treeLastCellClicked (&row, &col, riP);
	    if  (row != INVALID_ITEM && col != INVALID_ITEM)
	    	{
		row = -1;
		col = -1;

		/* Single selection */
		//mdlDialog_treeGetNextSelection (&found, &row, &col, riP);
		/* Multi-selection */
		found = !(mdlDialog_treeGetLocationCursor (&row, &col, riP));
		
		if  (found)
		    {
		    if (NULL != (pNode = mdlTreeModel_getDisplayRowAtIndex (pModel, row)))
			{
			MyTreeData  *pMyData;
			ValueDescr  valueDescr;
			BoolInt     bChanged=FALSE;

			mdlTreeNode_getValue (pNode, &valueDescr);
			pMyData = (MyTreeData *) valueDescr.value.voidPFormat;

			/* Display the correct Container */
			if (pMyData->containerId > 0 &&
			    pMyData->containerId != MYInfo.containerId)
			    {
			    MYInfo.containerId = pMyData->containerId;
			    mdlDialog_treeSetContainer (riP, MYInfo.containerId,
							    mdlSystem_getCurrMdlDesc (),
							    FALSE);
			    bChanged = TRUE;
			    }

			/* If showing hierarchy, display children or detail */
			if (MYInfo.containerId == CONTAINERID_SingleColumn ||
			    MYInfo.containerId == CONTAINERID_MultiColumn)
			    {
			    DialogItem      *ctPDiP;

			    if  (mdlTreeNode_isParent (pNode))
				{
				if (!mdlTreeNode_isParentPopulated (pNode))
				    treexmpl_nodeLoadChildren (pModel, pNode);

				treexmpl_showChildrenInList (pModel, pNode);
				}
			    else if  (mdlTreeNode_isLeaf (pNode))
			    	{
				treexmpl_showNodeDetail (pModel, pNode);
			    	}

			    if  (bChanged)
			    	{
				/* Redraw 3 main items in dialog (Tree, Sash, CTPanel) */
				treexmpl_adjustVSashDialogItems (dimP->db, TRUE, NULL, FALSE);
			    	}
			    else
			    	{
				/* Redraw the ContainerPanel */
				ctPDiP = mdlDialog_itemGetByTypeAndId (dimP->db,
						RTYPE_ContainerPanel, CTPANELID_Detail, 0);
				if (ctPDiP)
				    mdlDialog_itemDrawEx (dimP->db, ctPDiP->itemIndex, FALSE);
				}
			    }
			else
			    {
			    /* Redraw 3 main items in dialog (Tree, Sash, CTPanel) */
			    treexmpl_adjustVSashDialogItems (dimP->db, FALSE, NULL, FALSE);
			    }
			}
		    }
	    	}
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    if  (NULL != (pModel = mdlDialog_treeGetTreeModelP (riP)))
	    	{
		mdlTreeModel_destroy (pModel, TRUE);
	    	}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_vSashMotionFunc				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_vSashMotionFunc	/* called each time user moves mouse
					   while dragging sash */
(
MotionFuncArg	*mfaP	/* <> mfaP->pt.x = where sash upper left corner will go */
			/* => mfaP->pt.y = where cursor x position is */
			/* => mfaP->dragging = TRUE if cursor is within window */
)
    {
    if (mfaP->pt.x < MYInfo.minSashX)
	mfaP->pt.x = MYInfo.minSashX;

    if (mfaP->pt.x > MYInfo.maxSashX)
	mfaP->pt.x = MYInfo.maxSashX;
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_vSashHook					|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_vSashHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_BUTTON:
	    {
	    if (dimP->u.button.buttonTrans == BUTTONTRANS_DOWN)
		{
		BSIRect	 contentRect;
		int	 fontHeight = mdlDialog_fontGetCurHeight (dimP->db);

		dimP->u.button.motionFunc = (void(*)(void))treexmpl_vSashMotionFunc;

		mdlWindow_contentRectGetLocal (&contentRect, (GuiWindowP) dimP->db);
		MYInfo.minSashX = 5*fontHeight;
		MYInfo.maxSashX =
		    mdlDialog_rectWidth (&contentRect) -
		    5*fontHeight;
		}
	    else if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
		{
		Sash_ButtonUpAuxInfo *buaiP =
		    (Sash_ButtonUpAuxInfo *) dimP->auxInfoP;

		/* use buaiP->newXPos to determine where upperLeft corner
		   of sash beveled rect will go.  This message is sent after
		   sash has been erased from old position & moved, but before
		   it has been drawn */
		if (buaiP->newXPos != buaiP->oldXPos)
		    treexmpl_adjustVSashDialogItems (dimP->db, FALSE, NULL, TRUE);
		}

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_listBoxSingleHook				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_listBoxSingleHook
(
DialogItemMessage   *dimP
)
    {
    DialogItem	*diP = dimP->dialogItemP;
    RawItemHdr	*riP = diP->rawItemP;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    ListColumn  *pColumn;

	    MYInfo.pSingleLBRiP = riP;

	    /* Create ListModel with 1 column */
	    MYInfo.pSingleLM = mdlListModel_create (1);

	    /* Set up for each cell in column 0 to have 1 infoField to save GuiTreeNode */
	    if  (NULL != (pColumn = mdlListModel_getColumnAtIndex ((ListModel *)MYInfo.pSingleLM, 0)))
	    	{
		mdlListColumn_setInfoFieldCount (pColumn, 1);
	    	}

	    if (!MYInfo.pSingleLM)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    mdlDialog_listBoxSetListModelP (riP, (ListModel *)MYInfo.pSingleLM, 1);

	    break;
	    }

	case DITEM_MESSAGE_BUTTON:
	    {
	    DialogItem      *treeDiP;
	    int		    row = -1, col = -1;
	    ListCell	    *pCell;
	    GuiTreeNode     *pNode;

	    /* Double-click */
	    if ((dimP->u.button.upNumber == 2) && dimP->u.button.clicked != NULL)
	    	{
		treeDiP = mdlDialog_itemGetByTypeAndId (dimP->db, 
				RTYPE_Tree, TREEID_TreeExample, 0);
		if (!treeDiP)
		    break;

		mdlDialog_listBoxLastCellClicked (&row, &col, riP);
		if  (row == INVALID_ITEM || col == INVALID_ITEM)
		    break;

		/* Get the row cell clicked */
		if (NULL != (pCell = mdlListModel_getCellAtIndexes ((ListModel *)MYInfo.pSingleLM, row, 0)))
		    {
		    long	infoField;

		    /* Get the infoField from the cell, which is the GuiTreeNode ptr */
		    if (SUCCESS == mdlListCell_getInfoField (pCell, 0, &infoField))
			{
			pNode = (GuiTreeNode *) infoField;

			/* Expand or collapse the node */
			if  (mdlTreeNode_isExpanded (pNode))
			    {
			    mdlTreeNode_collapse (pNode, FALSE);
			    }
			else if  (mdlTreeNode_isParent (pNode))
			    {
			    if (!mdlTreeNode_isParentPopulated (pNode) && MYInfo.pTreeModel)
				treexmpl_nodeLoadChildren ((GuiTreeModel *)MYInfo.pTreeModel, pNode);

			    mdlTreeNode_makeDisplayable (pNode);
			    mdlTreeNode_expand (pNode);
			    }
			else 
			    {
			    mdlTreeNode_makeDisplayable (pNode);
			    }

			/* Update the Tree */
			mdlDialog_treeModelUpdated (treeDiP->rawItemP, TRUE);

			row = mdlTreeModel_getDisplayRowIndex ((GuiTreeModel *)MYInfo.pTreeModel, pNode);
			if (row >= 0)
			    mdlDialog_treeSelectCells (treeDiP->rawItemP, row, row, 0, 0, TRUE, TRUE);
			}
		    }
		}

	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    if (MYInfo.pSingleLM != NULL)
		{
		mdlListModel_destroy ((ListModel *)MYInfo.pSingleLM, TRUE);
		MYInfo.pSingleLM = NULL;
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_listBoxMultiHook				|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void treexmpl_listBoxMultiHook
(
DialogItemMessage   *dimP
)
    {
    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    DialogItem	*diP = dimP->dialogItemP;
	    RawItemHdr	*riP = diP->rawItemP;

	    MYInfo.pMultiLBRiP = riP;

	    /* Create ListModel with 3 columns */
	    MYInfo.pMultiLM = mdlListModel_create (3);

	    if (!MYInfo.pMultiLM)
		{
		dimP->u.create.createFailed = TRUE;
		return;
		}

	    mdlDialog_listBoxSetListModelP (riP, (ListModel *)MYInfo.pMultiLM, 3);

	    /* Hide column 1 */
	    {
	    UInt32   attributes = 0; //HIDDEN;
	    mdlDialog_listBoxSetColInfo (NULL, NULL, &attributes, NULL, riP, 1, FALSE);
	    }
	    
#if 0	    
	    /* Filter Row testing */
	    {
	    ListRow     *pRow;
	    ListCell    *pCell;
	    int		i;
	    MSWChar     wsFilterTableName[256];
	    ULong       ulFilterId;
	    BoolInt     bRedraw;

	    pRow = mdlListRow_create (MYInfo.pMultiLM);
	    for (i=0; i<3; i++)
		{
		pCell = mdlListRow_getCellAtIndex (pRow, i);
		mdlListCell_setEditor (pCell, RTYPE_ComboBox, COMBOBOXID_ElementStyleIcons, NULL, 0, 1);
		}
	    mdlListModel_setFilterRow ((ListModel *)MYInfo.pMultiLM, pRow);
	    
	    mdlCnv_convertMultibyteToUnicode ("abcde", -1,
					     wsFilterTableName, sizeof(wsFilterTableName));
	    ulFilterId = 1;
	    bRedraw = TRUE;
	    mdlDialog_listBoxSetFilter (riP, TRUE, wsFilterTableName, ulFilterId, TRUE);
	    }
#endif	    

	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    if (MYInfo.pMultiLM != NULL)
		{
		mdlListModel_destroy ((ListModel *)MYInfo.pMultiLM, TRUE);
		MYInfo.pMultiLM = NULL;
		}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	}
    }

/*----------------------------------------------------------------------+
|									|
| name		treexmpl_comboBoxHook   				|
|									|
| author        BSI					07/2000         |
|									|
+----------------------------------------------------------------------*/
Private void    treexmpl_comboBoxHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    RawItemHdr		*riP;
    ListModel		*pListModel;

    dimP->msgUnderstood = TRUE;

    riP = dimP->dialogItemP->rawItemP;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int			iTest;
	    char		buffer[20];
	    char		label[50];

	    sprintf (label, "Label w/ %c symbol:", 176);
	    mdlDialog_itemSetLabel (dimP->db, dimP->itemIndex, label);

	    pListModel = mdlListModel_create (1);
	    if (pListModel)
		{
		for (iTest=0; iTest<=20; iTest++)
		    {
		    sprintf (buffer, "%d", iTest*5);
		    mdlListModel_insertString (pListModel, buffer, iTest);
		    }

		mdlDialog_comboBoxSetListModelP (riP, pListModel);
		}

	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    if  (NULL != (pListModel = mdlDialog_comboBoxGetListModelP (riP)))
	    	{
		mdlListModel_destroy (pListModel, TRUE);
	    	}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }
    
/*----------------------------------------------------------------------+
|									|
| name		treexmpl_comboBoxChildTreeHook                          |
|									|
| author        BSI					09/04           |
|									|
+----------------------------------------------------------------------*/
Private void    treexmpl_comboBoxChildTreeHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    RawItemHdr		*riP;
    GuiTreeModel        *pTreeModel;

    dimP->msgUnderstood = TRUE;

    riP = dimP->dialogItemP->rawItemP;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_STATECHANGING:
	    {
	    GuiTreeNode     *pNode;
	    int		    row;

	    if  (dimP->u.stateChanging.changeType == STATECHANGING_TYPE_NODEEXPANDING)
	    	{
		pTreeModel = mdlDialog_treeGetTreeModelP (riP);

		row = dimP->u.stateChanging.row;

		/* Get this parent node & if not already populated, fill it */
		if (NULL != (pNode = mdlTreeModel_getDisplayRowAtIndex (pTreeModel, row)) &&
		    !mdlTreeNode_isParentPopulated (pNode))
		    {
		    treexmpl_nodeLoadChildren (pTreeModel, pNode);
		    }
		}

	    dimP->u.stateChanging.response = STATECHANGING_RESPONSE_ALLOW;
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }
    
/*----------------------------------------------------------------------+
|									|
| name		treexmpl_treeComboBoxHook   				|
|									|
| author        BSI					09/04           |
|									|
+----------------------------------------------------------------------*/
Private void    treexmpl_treeComboBoxHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    RawItemHdr		*riP;
    GuiTreeModel        *pTreeModel;

    dimP->msgUnderstood = TRUE;

    riP = dimP->dialogItemP->rawItemP;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    GuiTreeNode     *pRoot;
	    ValueDescr      valueDescr;
	    RawItemHdr      *treeRiP;

	    /* Create TreeModel with 1 column */
	    pTreeModel = mdlTreeModel_create (1);   
	    
	    /* Get root node from the TreeModel */
	    pRoot = mdlTreeModel_getRootNode (pTreeModel);
	    mdlTreeNode_setDisplayTextW (pRoot, Root.pwDisplayText);

	    valueDescr.formatType = FMT_VOIDP;
	    valueDescr.value.voidPFormat = &Root;
	    mdlTreeNode_setValue (pRoot, &valueDescr, FALSE);

	    mdlTreeNode_setCollapsedIcon (pRoot, ICONID_VolumeFolder, RTYPE_Icon, NULL);
	    mdlTreeNode_setExpandedIcon (pRoot, ICONID_OpenVolumeFolder, RTYPE_Icon, NULL);
	    mdlTreeNode_setExpandedSelectedIcon (pRoot, ICONID_CurrentVolumeFolder, RTYPE_Icon, NULL);

	    /* Load the root's children; other parent nodes will load their */
	    /* children and expand during the STATECHANGING message */
	    treexmpl_nodeLoadChildren (pTreeModel, pRoot);

	    /* Start with the root expanded */
	    mdlTreeNode_expand (pRoot);

	    mdlDialog_comboBoxSetTreeModelP (riP, pTreeModel);

            /* Setup a hook for the Tree item */	    
	    if (NULL != (treeRiP = mdlDialog_comboBoxGetTreeP (riP)))
                {
		mdlDialog_hookItemSetByDescr (treeRiP, /*(void *)*/ treexmpl_comboBoxChildTreeHook, NULL);
		}

	    break;
	    }

	case DITEM_MESSAGE_STATECHANGED:
	    {
#if defined (WHEN_TREECOMBOBOXVALUE_IS_A_LONG)	    
            MyTreeData      *pMyData;
	    
	    pMyData = (MyTreeData *) MYInfo.treeComboBoxValue;
	    if (NULL != pMyData)
                printf ("New value of TreeComboBox: %S\n", pMyData->pwDisplayText);
#else
            printf ("New value of TreeComboBox: %S\n", MYInfo.treeComboBoxValue);
#endif		
	    
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    if  (NULL != (pTreeModel = mdlDialog_comboBoxGetTreeModelP (riP)))
	    	{
		mdlTreeModel_destroy (pTreeModel, TRUE);
	    	}
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		treexmpl_simpleContainerHook   				|
|									|
| author        BSI					07/2000         |
|									|
+----------------------------------------------------------------------*/
Private void    treexmpl_simpleContainerHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    RawItemHdr		*riP;

    dimP->msgUnderstood = TRUE;

    riP = dimP->dialogItemP->rawItemP;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    //printf ("simpleContainerHook demo: CREATE called\n");
	    break;
	    }

	case DITEM_MESSAGE_DESTROY:
	    {
	    //printf ("simpleContainerHook demo: DESTROY called\n");
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|									|
| name		treexmpl_ctPanelHook	       				|
|									|
| author        BSI					07/2000         |
|									|
+----------------------------------------------------------------------*/
Private void    treexmpl_ctPanelHook
(
DialogItemMessage   *dimP	/* => a ptr to a dialog item message */
)
    {
    RawItemHdr		*riP;

    dimP->msgUnderstood = TRUE;

    riP = dimP->dialogItemP->rawItemP;

    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_CREATE:
	    {
	    int     containerId;

	    mdlDialog_containerPanelGetInfo (NULL, &containerId, NULL, NULL, riP);

	    MYInfo.containerId = containerId;

	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
        }
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_dialogHook					|
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private void	treexmpl_dialogHook
(
DialogMessage	*dmP	    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_CREATE:
	    {
	    dmP->u.create.interests.windowMoving= TRUE;
	    dmP->u.create.interests.resizes     = TRUE;
	    dmP->u.create.interests.updates     = TRUE;
	    break;
	    }

	case DIALOG_MESSAGE_INIT:
	case DIALOG_MESSAGE_UPDATE:
	    {
	    DialogItem	*diP;

	    diP = mdlDialog_itemGetByTypeAndId (dmP->db, RTYPE_Sash,
						SASHID_VExample, 0);
	    if (!diP)
		break;

	    treexmpl_adjustVSashDialogItems (dmP->db, FALSE, NULL, TRUE);
	    break;
	    }

	case DIALOG_MESSAGE_WINDOWMOVING:
	    {
	    int		minSize = (10*XC);

            /* Don't process if only moving dialog box */
            if (dmP->u.windowMoving.whichCorners == CORNER_ALL ||
	    	dmP->u.windowMoving.whichCorners == CORNER_ALL_RESIZED)
                break;

	    /* Minimum size for dialog */
	    if (dmP->u.windowMoving.newWidth  < minSize)
		dmP->u.windowMoving.newWidth  = minSize;
	    if (dmP->u.windowMoving.newHeight < minSize)
		dmP->u.windowMoving.newHeight = minSize;

	    dmP->u.windowMoving.handled     = TRUE;

	    break;
	    }

	case DIALOG_MESSAGE_RESIZE:
	    {
	    BSIRect     oldContent;

            /* Don't process if only moving dialog box */
            if (dmP->u.resize.whichCorners == CORNER_ALL)
                break;

	    mdlWindow_pointToLocal (&oldContent.origin, (GuiWindowP) dmP->db,
				    &dmP->u.resize.oldContent.origin);
	    mdlWindow_pointToLocal (&oldContent.corner, (GuiWindowP) dmP->db,
				    &dmP->u.resize.oldContent.corner);

	    treexmpl_adjustVSashDialogItems (dmP->db, FALSE, &oldContent, 
		(dmP->u.resize.oldContent.origin.x != dmP->u.resize.newContent.origin.x ||
		 dmP->u.resize.oldContent.origin.y != dmP->u.resize.newContent.origin.y));
	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          treexmpl_unloadFunction                                 |
|                                                                       |
| author        BSI					07/2000         |
|                                                                       |
+----------------------------------------------------------------------*/
Private int     treexmpl_unloadFunction
(
int     unloadType
)
    {
    return SUCCESS;     /* Allow unload */
    }


Private	DialogHookInfo uHooks[]	=
{
 {HOOKDIALOGID_treexmpl,	    (PFDialogHook)treexmpl_dialogHook},
 {HOOKITEMID_Tree,		    (PFDialogHook)treexmpl_treeHook},
 {HOOKITEMID_Sash_VExample,	    (PFDialogHook)treexmpl_vSashHook},
 {HOOKITEMID_SingleListBox,	    (PFDialogHook)treexmpl_listBoxSingleHook},
 {HOOKITEMID_MultiListBox,	    (PFDialogHook)treexmpl_listBoxMultiHook},
 {HOOKITEMID_ComboBoxes,	    (PFDialogHook)treexmpl_comboBoxHook},
 {HOOKITEMID_Simple,    	    (PFDialogHook)treexmpl_simpleContainerHook},
 {HOOKITEMID_CTPanel,    	    (PFDialogHook)treexmpl_ctPanelHook},
 {HOOKITEMID_TreeComboBox,          (PFDialogHook)treexmpl_treeComboBoxHook},
};

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author        BSI					07/2000         |
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    SymbolSet	   *setP;	/* a ptr to a "C expression symbol set"	*/
    RscFileHandle   rscFileH;	/* a resource file handle */

    /*-----------------------------------------------------------------------
    mdlResource_openFile:

    This function opens a resource file, thus making its contents
    available to the application.  In this case, we need to open
    treexmpl.ma as a resource file so that we have access to the the
    string lists for our messages.
    -----------------------------------------------------------------------*/
    if (SUCCESS	!= mdlResource_openFile	(&rscFileH, NULL, 0))
	{
	mdlOutput_rscPrintf (MSG_ERROR,	NULL, MESSAGELISTID_Messages,
			     MESSAGEID_ResourceLoadError);

	/* unload the application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				  mdlSystem_getCurrTaskID (), TRUE);
	}

    /* --- Set up function to get called at unload time --- */
    mdlSystem_setFunction (SYSTEM_UNLOAD_PROGRAM, treexmpl_unloadFunction);

    /*-----------------------------------------------------------------------
    To make the Dialog Box Manager aware of the function pointers
    that we have equated to our hook ids we need to publish the
    DialogHookInfo structure, uHooks.
    -----------------------------------------------------------------------*/
    mdlDialog_hookPublish (sizeof(uHooks) / sizeof(DialogHookInfo), uHooks);

    mdlState_registerStringIds (MESSAGELISTID_Messages,	MESSAGELISTID_Messages);

    /* set up the variables we are going to set	from dialog boxes */
    setP = mdlCExpression_initializeSet	(VISIBILITY_DIALOG_BOX,	0, FALSE);
    memset (&MYInfo, 0, sizeof (TreeXmplInfo));
    mdlDialog_publishComplexVariable (setP, "treexmplinfo", "MYInfo", &MYInfo);

    /* open the	dialogbox	*/
    if (NULL ==	mdlDialog_open (NULL, DIALOGID_treexmpl))
	{
	mdlOutput_rscPrintf (MSG_ERROR,	NULL, MESSAGELISTID_Messages,
			     MESSAGEID_DialogOpenError);

	/* unload this application */
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				  mdlSystem_getCurrTaskID (), TRUE);
	}
    return  SUCCESS;
    }				/* Finish main function	*/

