/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/treexmpl/treexmpl.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   treexmpl.r -  Tree Gui Item Example resource definitions		|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include <cmdlist.h>

#include "treexmpl.h"
#include "treextxt.h"


/*----------------------------------------------------------------------+
| 
|  Register Application and DLL
| 
+----------------------------------------------------------------------*/
#define  DLLAPP_TREEXMPL        1

DllMdlApp DLLAPP_TREEXMPL =
    {
    "TREEXMPL", "treexmpl"          // taskid, dllName
    }



#define XSIZE       (75*XC)
#define YSIZE       (75*YC)

#define SX	    (20*XC)
#define GBX	    (3*XC)
#define TGX	    (3*XC)
#define SPX	    (3*XC)
#define RBX	    (4*XC)
#define CBX	    (12*XC)
#define TX	    (9*XC)
#define SCX	    (16*XC)

#define Y1	    (1*YC)

#define SW	    (14*XC)		/* width of Scale items */
#define TW	    (5*XC)		/* width of text items */
#define CW	    (14*XC)		/* width of ComboBox items */

#define GBW	    (17*XC)

/*----------------------------------------------------------------------+
|									|
|   Dialog Box Resource Specification					|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_treexmpl = 
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_GROWABLE | DIALOGATTR_MOTIONTOITEMS |
    DIALOGATTR_AUTOUNLOADAPP,
    XSIZE, YSIZE,
    NOHELP, MHELP, HOOKDIALOGID_treexmpl, NOPARENTID,
    TXT_DialogTitle,
	{
	{{0,0,0,0},	Tree,	    TREEID_TreeExample,     ON,  0, "", ""},
	{{SX,0,0,0},	Sash,	    SASHID_VExample,	    ON,  0, "", ""},
	{{0,0,0,0},	ContainerPanel, CTPANELID_Detail,   ON,  0, "", ""},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   TabPage Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_TabPageListRsc  TPLISTID_ONE =
    {
    (60*XC), 0,
    NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,
    TABATTR_DEFAULT | TABATTR_RELATIVEPOSITIONS,
    "",
    {
    {{0,0,0,0}, TabPage, TABPAGEID_General,    ON, 0,"",""},
    {{0,0,0,0}, TabPage, TABPAGEID_Specific1,  ON, 0,"",""},
    {{0,0,0,0}, TabPage, TABPAGEID_Specific2,  ON, 0,"",""},
    }
    };

DItem_TabPageRsc  TABPAGEID_General =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,
    TABATTR_DEFAULT, 0, 0,
    TXT_General,
    {
    {{CBX,D_ROW(1.5),CW,0}, ComboBox, COMBOBOXID_Test1, ON, 0, "", ""},
    {{GBX,D_ROW(4)-YC,GBW,D_ROW(4.5)}, GroupBox, 0, ON, 0, TXT_RadioButtons1, ""},
    {{RBX,D_ROW(4),0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
    {{RBX,D_ROW(5),0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
    {{RBX,D_ROW(6),0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},
    }
    };

DItem_TabPageRsc  TABPAGEID_Specific1 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,
    TABATTR_DEFAULT, 0, 0,
    TXT_Specific1,
    {
    {{TX,Y1,TW,0}, Text,   TEXTID_Range1,  ON, 0, "", ""},
    {{SCX,Y1,SW,0}, Scale,  SCALEID_Range1, ON, 0, "", ""},
    {{TGX,D_ROW(3), 0, 0}, ToggleButton, TOGGLEID_Demo1,    ON, 0, "", ""},
    {{TGX,D_ROW(4), 0, 0}, ToggleButton, TOGGLEID_Demo2,    ON, 0, "", ""},
    {{TGX,D_ROW(5), 0, 0}, ToggleButton, TOGGLEID_Demo3,    ON, 0, "", ""},
    }
    };

DItem_TabPageRsc  TABPAGEID_Specific2 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG, 
    TABATTR_DEFAULT,
    0, 0,
    TXT_Specific2,
    {
    {{TX,Y1,TW,0}, Text,   TEXTID_Range2,  ON, 0, "", ""},
    {{SCX,Y1,SW,0}, Scale,  SCALEID_Range2, ON, 0, "", ""},
    {{SPX,D_ROW(4),TW*2.5,1.5*YC}, SpinBox, SPINBOXID_Test1, ON, 0, "", ""},
    {{CBX,D_ROW(6),CW,0}, ComboBox, COMBOBOXID_Tree, ON, 0, "", ""},
    }
    };
    
/*----------------------------------------------------------------------+
|									|
|   Tree Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_TreeRsc TREEID_TreeExample =
    {
    NOHELP, MHELP, 
    HOOKITEMID_Tree, NOARG, 
    TREEATTR_DYNAMICSCROLL | TREEATTR_LINESDIM | TREEATTR_LINESDOTTED | 
    TREEATTR_NOROOTHANDLE | TREEATTR_NOSHOWROOT | TREEATTR_DOUBLECLICKEXPANDS |
    TREEATTR_EDITABLE | TREEATTR_FOCUSOUTLOOK,
    6, CTPANELID_Detail, "",
	{
	{0, 256*XC, 256, 0, TXT_Tree},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   Sash Item Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_SashRsc	SASHID_VExample =
    {
    NOHELP, MHELP, 
    HOOKITEMID_Sash_VExample, 0, 
    10*XC, 10*XC, 
    SASHATTR_VERTICAL | SASHATTR_ALLGRAB | SASHATTR_SOLIDTRACK | 
    SASHATTR_WIDE | SASHATTR_SAVEPOSITION | SASHATTR_NOENDS
    };

/*----------------------------------------------------------------------+
|									|
|   ContainerPanel Item Resources					|
|									|
+----------------------------------------------------------------------*/
DItem_ContainerPanelRsc	CTPANELID_Detail =
    {
    NOHELP, MHELP, 
    HOOKITEMID_CTPanel, 0, 
    0,
    CONTAINERID_SingleColumn, 0,
    ""
    };

/*----------------------------------------------------------------------+
|									|
|   DialogItemList Resources						|
|									|
+----------------------------------------------------------------------*/

DialogItemListRsc DILISTID_SingleColumn =
    {
    {
    {{0,0,0,0},	ListBox,    LISTBOXID_SingleColumn,     ON, ALIGN_PARENT, "", ""},
    }
    };    

DItem_ContainerRsc CONTAINERID_SingleColumn = 
    {
    NOCMD, LCMD, NOHELP, MHELP, NOHOOK, NOARG, 0,
    DILISTID_SingleColumn
    };

DialogItemListRsc DILISTID_MultiColumn =
    {
    {
    {{0,0,0,0},	ListBox,    LISTBOXID_MultiColumn,   ON, ALIGN_PARENT, "", ""},
    }
    };    

DItem_ContainerRsc CONTAINERID_MultiColumn = 
    {
    NOCMD, LCMD, NOHELP, MHELP, NOHOOK, NOARG, 0,
    DILISTID_MultiColumn
    };

DialogItemListRsc DILISTID_Tabs =
    {
    {
    {{XC,XC,0,0},   TabPageList,    TPLISTID_ONE,   ON, 0, "", ""},
    }
    };    

DItem_ContainerRsc CONTAINERID_Tabs = 
    {
    NOCMD, LCMD, NOHELP, MHELP, NOHOOK, NOARG, 0,
    DILISTID_Tabs
    };

DialogItemListRsc DILISTID_Simple =
    {
    {
    {{CBX,D_ROW(1.5),CW,0}, ComboBox, COMBOBOXID_Test1, ON, 0, "", ""},
    {{GBX,D_ROW(4)-YC,GBW,D_ROW(4.5)}, GroupBox, 0, ON, 0, TXT_RadioButtons1, ""},
    {{RBX,D_ROW(4),0,0}, RadioButton,  RBUTTONID_Option1, ON, 0, "", ""},
    {{RBX,D_ROW(5),0,0}, RadioButton,  RBUTTONID_Option2, ON, 0, "", ""},
    {{RBX,D_ROW(6),0,0}, RadioButton,  RBUTTONID_Option3, ON, 0, "", ""},
    }
    };    

DItem_ContainerRsc CONTAINERID_Simple = 
    {
    NOCMD, LCMD, NOHELP, MHELP, HOOKITEMID_Simple, NOARG, 0,
    DILISTID_Simple
    };

/*----------------------------------------------------------------------+
|									|
|   ListBox Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_SingleColumn =
    {
    NOHELP, MHELP, 
    HOOKITEMID_SingleListBox, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_SELBROWSE | LISTATTR_HORIZSCROLLBAR | 
    LISTATTR_NOTRAVERSAL | LISTATTR_DRAWPREFIXICON,
    6, 0, "",
	{
	{256*XC, 256, 0, ""},
	}
    }
    extendedIntAttributes
    {
	{
	{EXTINTATTR_ITEMATTRS, LISTATTRX_FOCUSOUTLOOK}
	}
    };

DItem_ListBoxRsc LISTBOXID_MultiColumn =
    {
    NOHELP, MHELP, 
    HOOKITEMID_MultiListBox, NOARG, 
    LISTATTR_DYNAMICSCROLL | LISTATTR_SELBROWSE | LISTATTR_HORIZSCROLLBAR | 
    LISTATTR_COLHEADINGBORDERS | LISTATTR_SORTCOLUMNS | LISTATTR_SAVECOLUMNINFO |
    LISTATTR_NEWCOLHEADORIGIN | LISTATTR_NOTRAVERSAL | LISTATTR_DRAWPREFIXICON |
    LISTATTR_EDITABLE,
    6, 0, "",
	{
	{30*XC, 256, 0, TXT_Work},
	{7*XC,   50, 0, TXT_WorkId},
	{7*XC,    7, 0, TXT_Year},
	}
    }
    extendedIntAttributes
    {
	{
	{EXTINTATTR_ITEMATTRS, LISTATTRX_FOCUSOUTLOOK}
	}
    };

/*----------------------------------------------------------------------+
|									|
|   ComboBox Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_ComboBoxRsc COMBOBOXID_Test1 =
    {
    NOCMD, LCMD, 
    NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_ComboBoxes, NOARG,
    12, "%.0lf", "%lf", "0.0", "100.0", NOMASK,
    0, 6, 4, 0, 0, 
    COMBOATTR_AUTOADDNEWSTRINGS, 
    TXT_ComboBox1,
    "MYInfo.comboBoxValue",
	{
	{0, 12, 0, ""},
	}
    }
    extendedWideAttributes
    {
	{
	{EXTWATTR_LABEL, L"MSWChar"}
	}
    };

DItem_ComboBoxRsc COMBOBOXID_Tree =
    {
    NOCMD, LCMD, 
    NOSYNONYM, NOHELP, MHELP, 
    HOOKITEMID_TreeComboBox, NOARG,
    256, "", "", "", "", NOMASK,
    0, 15, 0, 40*XC, 0, 
    COMBOATTR_READONLY | COMBOATTR_TREE | COMBOATTR_DRAWPREFIXICON /*| COMBOATTR_USEMODELVALUE*/ /*| COMBOATTR_TREEPARENTSELECTABLE*/, 
    TXT_TreeComboBox,
    "MYInfo.treeComboBoxValue",
	{
	{0, 256, 0, ""},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   RadioButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_RadioButtonListRsc RBLISTID_Options1 =
    {
    {
    RBUTTONID_Option1,
    RBUTTONID_Option2,
    RBUTTONID_Option3,
    }
    };

DItem_RadioButtonRsc RBUTTONID_Option1 = 
    {
    NOCMD, LCMD, SYNONYMID_RadioButtons1, 
    NOHELP, MHELP, NOHOOK, NOARG,
    1, 0x00ff, RBLISTID_Options1,
    TXT_RadioButton1,
    "MYInfo.radioButtonValue"
    };

DItem_RadioButtonRsc RBUTTONID_Option2 = 
    {
    NOCMD, LCMD, SYNONYMID_RadioButtons1, 
    NOHELP, MHELP, NOHOOK, NOARG,
    2, 0x00ff, RBLISTID_Options1,
    TXT_RadioButton2,
    "MYInfo.radioButtonValue"
    };

DItem_RadioButtonRsc RBUTTONID_Option3 = 
    {
    NOCMD, LCMD, SYNONYMID_RadioButtons1, 
    NOHELP, MHELP, NOHOOK, NOARG,
    3, 0x00ff, RBLISTID_Options1,
    TXT_RadioButton3,
    "MYInfo.radioButtonValue"
    };

/*----------------------------------------------------------------------+
|									|
|   Scale Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_ScaleRsc SCALEID_Range1 =
    {
    NOCMD, LCMD, SYNONYMID_Range1, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    0.0, 100.0, 1.0, 10.0, 
    TEXTID_Range1, 
    0, "%.0lf", 
    "",
    "MYInfo.range1",
    "0", "100"
    };
    
DItem_ScaleRsc SCALEID_Range2 =
    {
    NOCMD, LCMD, SYNONYMID_Range2, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    0.0, 100.0, 1.0, 10.0, 
    TEXTID_Range2, 
    SCALE_HASARROWS, "%.0lf", 
    "",
    "MYInfo.range2",	    /* note range 2 is int and this still works */
    "0", "100"
    };

/*----------------------------------------------------------------------+
|									|
|   SpinBox Item Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_SpinBoxRsc SPINBOXID_Test1 =
    {
    NOCMD, LCMD, NOSYNONYM, NOHELP, MHELP, 
    NOHOOK, NOARG,
    8, "%.1lf in", "%lf",  
    1.0, 100.0, 0.5,
    NOMASK, SPINATTR_LABELABOVE, 
    TXT_SpinBox1,
    "MYInfo.spinBoxValue"
    };
    
/*----------------------------------------------------------------------+
|									|
|   Text Item Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_Range1 = 
    {
    NOCMD, LCMD, SYNONYMID_Range1, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    5, "%.0lf", "%lf", "0", "100", NOMASK, 0, 
    TXT_Range1,
    "MYInfo.range1"
    };

DItem_TextRsc TEXTID_Range2 = 
    {
    NOCMD, LCMD, SYNONYMID_Range2, NOHELP, MHELP, 
    NOHOOK, NOARG, 
    5, "%ld", "%ld", "0", "100", NOMASK, 0, 
    TXT_Range2,
    "MYInfo.range2"
    };

/*----------------------------------------------------------------------+
|									|
|   Toggle Item Resources	    					|
|									|
+----------------------------------------------------------------------*/
DItem_ToggleButtonRsc TOGGLEID_Demo1 =
    {
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,
    NOMASK, NOINVERT,
    TXT_Toggle1,
    "MYInfo.toggle1"
    };

DItem_ToggleButtonRsc TOGGLEID_Demo2 =
    {
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,
    NOMASK, NOINVERT,
    TXT_Toggle2,
    "MYInfo.toggle2"
    };

DItem_ToggleButtonRsc TOGGLEID_Demo3 =
    {
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, NOHOOK, NOARG,
    NOMASK, NOINVERT,
    TXT_Toggle3,
    "MYInfo.toggle3"
    };


/*----------------------------------------------------------------------+
|									|
|   Synonym List Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_SynonymsRsc SYNONYMID_ComboBox =
    {
	{
	{ComboBox, COMBOBOXID_Test1},
	}
    };

DItem_SynonymsRsc SYNONYMID_Range1 =
    {
	{
	{Text,	TEXTID_Range1},
	{Scale, SCALEID_Range1},
	}
    };

DItem_SynonymsRsc SYNONYMID_Range2 =
    {
	{
	{Text,	TEXTID_Range2},
	{Scale, SCALEID_Range2},
	}
    };

DItem_SynonymsRsc SYNONYMID_RadioButtons1 = 
    {
    	{
	{RadioButton,       RBUTTONID_Option1},
	{RadioButton,       RBUTTONID_Option2},
	{RadioButton,       RBUTTONID_Option3},
	}
    };


/*----------------------------------------------------------------------+
|                                                                       |
|   Icon Item Resource                                                  |
|                                                                       |
+----------------------------------------------------------------------*/
IconRsc ICONID_Music =
    {
    16,    12,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "",
        {
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11,11,11,11,11, 0,10,10,10,10,10,10, 0,11,11,
        11,11,11,11,11,11, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11,11,11,11,11, 0,11,11,11,11,11,11, 0,11,11,
        11,11,11,11,11,11, 0,11,11,11,11,11,11, 0,11,11,
        11,11,11,11,11,11, 0,11,11,11,11,11,11, 0,11,11,
        11,11,11, 0, 0, 0, 0,11,11,11, 0, 0, 0, 0,11,11,
        11,11, 0, 0, 0, 0, 0,11,11, 0, 0, 0, 0, 0,11,11,
        11,11, 0, 0, 0, 0, 0,11,11, 0, 0, 0, 0, 0,11,11,
        11,11,11, 0, 0, 0,11,11,11,11, 0, 0, 0,11,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
	}
    };

IconRsc ICONID_Container =
    {
    16,    12,    ICONFORMAT_FIXEDCOLORS,    BLACK_INDEX, "",
        {
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11, 0,10,10,10,10,10,10,10,10,10,10, 0,11,11,
        11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0,11,11,11,11,11,11,11,11,11,11, 0,11,11,
        11,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,11,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        }
    };