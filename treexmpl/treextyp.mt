/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/treexmpl/treextyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $RCSfile: treextyp.mt,v $
|   $Revision: 1.2.78.1 $
|   	$Date: 2013/07/01 20:40:58 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|	Tree Gui Item example dialog box structures			|
|									|
+----------------------------------------------------------------------*/

#include    "treexmpl.h"
    
publishStructures (treexmplinfo);
