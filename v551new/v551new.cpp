/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v551new/v551new.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   v551new.mc  $
|   $Revision: 1.1.32.1 $
|      	$Date: 2013/07/01 20:41:01 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>
#include <global.h>
#include <rscdefs.h>
#include <dlogids.h>
#include <dlogitem.h>
#include <deffiles.h>
#include <userfnc.h>
#include <string.h>
#include <cmdlist.h>

#include <dlogman.fdf>
#include <msdialog.fdf>
#include <msoutput.fdf>
#include <msparse.fdf>
#include <msrsrc.fdf>
#include <mssystem.fdf>
#include <mselemen.fdf>
#include <msstate.fdf>
#include <msfile.fdf>
#include <msinput.fdf>

#include "v551new.h"
#include "v551cmd.h"

/*----------------------------------------------------------------------+
|									|
| Use of predefined MicroStation symbol MSVERSION to compile the correct|
| version of your code.							|
|									|
|   #if defined (MSVERSION) && (MSVERSION >= 0x551)			|
|	code new to MicroStation Version 5.5.1 and higher		|
|   #endif								|
|									|
|   #if defined (MSVERSION) && (MSVERSION >= 0x550)			|
|	code new to MicroStation PowerDraft and higher			|
|   #endif								|
|									|
+----------------------------------------------------------------------*/


/*----------------------------------------------------------------------+
|									|
|   Local defines							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Local type definitions						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Local function definitions						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
unsigned long			chosenProject;

/*----------------------------------------------------------------------+
|									|
|   Public Global variables						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   External variables							|
|									|
+----------------------------------------------------------------------*/

/*======================================================================+
|									|
|   Private Utility Routines						|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
| name		displayInfoBox						|
|									|
| author	BSI 					04/95		|
|									|
+----------------------------------------------------------------------*/
Private void	displayInfoBox
(
int	    	msgId
)
    {
    char	msgString [256];

    if (mdlResource_loadFromStringList
	    (msgString, NULL, MESSAGELISTID_Strings, msgId) == SUCCESS)
	{
    	mdlDialog_openMessageBox (DIALOGID_MsgBoxOK, msgString, MSGBOX_ICON_WARNING);
	}
    }

/*======================================================================+
|									|
|   File Open Dialog Preprocess Example				    	|
|									|
+======================================================================*/
#if defined (MSVERSION) && (MSVERSION >= 0x551)
/*----------------------------------------------------------------------+
|									|
| name		fileOpenDialogPreprocess_getFileName			|
|									|
| author	BSI 					04/95		|
|									|
+----------------------------------------------------------------------*/
Private int fileOpenDialogPreprocess_getFileName
(
char 	*fileNameP
)
    {
    int 	status = SUCCESS;
    char	baseFile[MAXFILELENGTH];

/*  retrieve the base file name */
    if ((status=mdlResource_loadFromStringList(baseFile, 0, STRINGID_FileList, chosenProject)) == SUCCESS)
	{
/*  	retrieve the full path specification for the file */
	status = mdlFile_find (fileNameP, baseFile, "MS_DEF", NULL);
	}

    return  (status);
    }


/*----------------------------------------------------------------------+
|									|
| name		fileOpenDialogPreprocessExample				|
|									|
| author	BSI 					04/95		|
|									|
+----------------------------------------------------------------------*/
Private int	fileOpenDialogPreprocessExample
(
char	    	*fileNameP,    	/* <= choosen file name */
char	    	*extraOutArgP,	/* <= NOT USED */
FileOpenParams	*fopenParamsP,	/* => file open parameters from fileOpenExt (see filelist.h) */
				/*    This information is READONLY */
void	    	*extraInArgP	/* => NOT USED */
)
    {

/*
Notes:
======
1) A separate hook will be required to trap keyin input such as "RD="

2) This application must be a MS_INITAPPS for the File->close event
   to be trapped by this application.  (For example purposes, this application
   	is NOT setup to be a MS_INITAPPS).
   The File->close event will result in a call to this function, since 
   our application will remain loaded when a design file closes.  If our
   application is not a MS_INITAPPS, then it will be unloaded when the 
   design file closes.

3) The file history should probably be deleted, since the user can bypass
   our project open dialog box by selecting a design file in this history.

*/

/*----------------------------------------------------------------------+
|									|
|   Check if the user is opening a design file	       			|
|									|
|   See deffiles.h for file types to compare to fopenParamsP->defFileId.|
|									|
|   Possible values for fopenParamsP->openCreate are:		    	|
|   	FILELISTATTR_OPEN					    	|
|   	FILELISTATTR_CREATE					    	|
|   	FILELISTATTR_CREATEFROMSEED				    	|
|									|
+----------------------------------------------------------------------*/
    

    if ((DEFDGNFILE_ID == fopenParamsP->defFileId) && 
	    (FILELISTATTR_OPEN == fopenParamsP->openCreate))
	{
	/* opening a design file */
	int 	status;
    	int 	lastActionType = ACTIONBUTTON_CANCEL;


	/* open a dialog box to let the user select a project */

	/* Note: You cannot call mdlDialog_defFileOpen or 
	mdlDialog_fileOpen or any other function which opens
	the MicroStation  file open dialog
	or this user function will get called again.
	Build & open a separate dialog box with a file list to chose from.
	*/


    	mdlDialog_openModal (&lastActionType, NULL, DIALOGID_Project);

	if (lastActionType != ACTIONBUTTON_CANCEL)
	    {
	    /* get the design file path & name to be used for the current project */
	    status = fileOpenDialogPreprocess_getFileName(fileNameP);
	    if (status != SUCCESS)
		{
		/* Note: 
		  If a full path specification is not supplied for the file,
		  then MicroStation attempts to find the file in MicroStation's
		  current working directory.
		*/
            	strcpy (fileNameP, "default.dgn");
		}

    	    return INPUT_FOPENPREPROCESS_HANDLED;
	    }
	else /* user hit CANCEL button */
	    {
	    return INPUT_FOPENPREPROCESS_CANCELED;
	    }
	}
    else if ((DEFDGNFILE_ID == fopenParamsP->defFileId) && 
	     (FILELISTATTR_CREATEFROMSEED == fopenParamsP->openCreate))
	{
	displayInfoBox (MSGID_CreateDesignFile);

    	/* currently ignored, add code similar to above */	     	
	return INPUT_FOPENPREPROCESS_IGNORED;
	}
    else if ((DEFREFFILE_ID == fopenParamsP->defFileId) && 
	     (FILELISTATTR_OPEN == fopenParamsP->openCreate))
	{
	displayInfoBox (MSGID_AttachReferenceFile);

    	/* currently ignored, add code similar to above */	     	
	return INPUT_FOPENPREPROCESS_IGNORED;
	}
    else if ((DEFCELLFILE_ID == fopenParamsP->defFileId) && 
	     (FILELISTATTR_OPEN == fopenParamsP->openCreate))
	{
	displayInfoBox (MSGID_AttachCellLibrary);

    	/* currently ignored, add code similar to above */	     	
	return INPUT_FOPENPREPROCESS_IGNORED;
	}
    else
	{
	/* ignore this file type - let MicroStation or another
	  application handle it
	*/
	return INPUT_FOPENPREPROCESS_IGNORED;
	}

/*----------------------------------------------------------------------+
|									|
|   Returns:					       			|
|									|
|   INPUT_FOPENPREPROCESS_HANDLED - this function has supplied the file	|
|		 open information, do not open the standard file open 	|
|		dialog box.						|
|									|
|   INPUT_FOPENPREPROCESS_IGNORED - this function is not handling the	|
|		 open file of this file type.  Open the standard file 	|
|		open dialog box or let another application process this	|
|		file type.						|
|									|
|   INPUT_FOPENPREPROCESS_CANCELED - handle the event but cancel opening|
|		a file							|
|									|
+----------------------------------------------------------------------*/

    }

#endif /* (MSVERSION) && (MSVERSION >= 0x551) */

/*======================================================================+
|									|
|   Command functions for CmdItemListRsc example.		    	|
|									|
|  This command will have tool settings when the icon command is and	|
|  is NOT loaded.						    	|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          v551new_placeSomethingDone                              |
|                                                                       |
| author        BSI                                     06/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    v551new_placeSomethingDone
(
void
)
    {
    /*
    // Start the default command, after you have placed the text element.
    // This is a single shot tool.
    */
    mdlState_startDefaultCommand ();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          v551new_placeSomethingAtPoint                           |
|                                                                       |
| author        BSI                                     06/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    v551new_placeSomethingAtPoint
(
Dpoint3d        *pntP,
int	    	view
)
    {
    MSElement   el;
    char    	str[80];

    mdlResource_loadFromStringList (str, NULL, MESSAGELISTID_Strings,  MSGID_TextString);

    mdlText_create (&el, NULL, str, pntP, NULL, NULL, NULL, NULL);

    /* Display the new text element and add it to the design file */
    mdlElement_display (&el, NORMALDRAW);
    mdlElement_add (&el);

    /* Call our cleanup function terminating the primitive command */
    v551new_placeSomethingDone ();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          v551new_placeSomething                                  |
|                                                                       |
| author        BSI                                     06/95           |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    v551new_placeSomething
(
char            *unparsedP
)
//cmdNumber       CMD_PLACE_SOMETHING
    {

    mdlState_startPrimitive (v551new_placeSomethingAtPoint, 
			     v551new_placeSomethingDone,
			     MSGID_PlaceSomethingCmdName, 
			     MSGID_PlaceSomethingPrompt);
    }

/*----------------------------------------------------------------------+
|									|
| name		example_projectDialogHook				|
|									|
| author	BSI				10/93			|
|									|
+----------------------------------------------------------------------*/
Private void	example_projectDialogHook
(
DialogMessage   *dmP
)
    {						
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_ACTIONBUTTON:
	    {
	    int		row	    = -1;
	    int		column	    = -1;
	    BoolInt     found	    = FALSE;
	    char	*stringP    = NULL;
	    DialogItem  *diP	    = NULL;
	    StringList  *strListP   = NULL;

	    if (dmP->u.actionButton.actionType != ACTIONBUTTON_OK)
		break;

	    if ((diP = mdlDialog_itemGetByTypeAndId
			    (dmP->db, RTYPE_ListBox,
			    LISTBOXID_ProjectList, 0)) == NULL)
	        break;

	    mdlDialog_listBoxGetNextSelection
	        (&found, &row, &column, diP->rawItemP);

	    if (! found)
		break;

	    if ((strListP = mdlDialog_listBoxGetStrListP (diP->rawItemP)) == NULL)
		break;

	    mdlStringList_getMember (&stringP, NULL, strListP, row);

	    /* store the chosen project number */
	    chosenProject = row;

	    break;
	    }

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
    	}
    }

/*----------------------------------------------------------------------+
|									|
| name		example_projectListHook					|
|									|
| author	BSI				10/93			|
|									|
+----------------------------------------------------------------------*/
Private void	example_projectListHook
(
DialogItemMessage   *dimP
)
    {
    RawItemHdr  *rihP	= dimP->dialogItemP->rawItemP;

    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
	{
    	case DITEM_MESSAGE_CREATE:
	    {
	    StringList  *strListP   = NULL;

	    if ((strListP = mdlStringList_loadResource
				((RscFileHandle)NULL,
				STRINGID_ProjectList)) == NULL)
		{

		mdlOutput_rscPrintf(MSG_ERROR, 0, MESSAGELISTID_Strings, MSGID_StrLoadError);

		dimP->u.create.createFailed = TRUE;
		break;
		}

	    mdlDialog_listBoxSetStrListP
		(dimP->dialogItemP->rawItemP, strListP, 1);
	    break;
	    }
			
	case DITEM_MESSAGE_BUTTON:
	    {
    	    DialogItem *diP = NULL;

	    /* Have a double-click activate the OK */
            if (dimP->u.button.buttonTrans == BUTTONTRANS_UP)
                {
                if ((dimP->u.button.upNumber == 2) && dimP->u.button.clicked)
                    {
                    if ((diP = mdlDialog_itemGetByTypeAndId
                                (dimP->db, RTYPE_PushButton,
                                PUSHBUTTONID_OK, 0)) != NULL)
			{
                    	mdlDialog_pushButtonActivate (diP->rawItemP);
			}
                    }
                }
            break;
            }

    	case DITEM_MESSAGE_DESTROY:
	    {
	    StringList  *strListP   = NULL;

	    if (strListP = mdlDialog_listBoxGetStrListP (rihP))
		mdlStringList_destroy (strListP);
	    break;
	    }

	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}
    }

static DialogHookInfo  uHooks[] =
    {
    {HOOKITEMID_ProjectList,		  (PFDialogHook)example_projectListHook},
    {HOOKDIALOGID_ProjectDialog,	  (PFDialogHook)example_projectDialogHook},
    };

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI 					8/94		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int             argc,
char            *argv[]
)
    {
    RscFileHandle   		rfHandle;

    mdlResource_openFile (&rfHandle, NULL, RSC_READ);

    Private MdlCommandNumber  commandNumbers [] =
    {
    {v551new_placeSomething,CMD_PLACE_SOMETHING},
    0,
    };
    
    /* Register commands */
    mdlSystem_registerCommandNumbers (commandNumbers);



    if (mdlParse_loadCommandTable (NULL) == NULL)
	{
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Strings, MSGID_LoadCmdTbl);
	mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
	return  SUCCESS;
	}

    /* publish the dialog item hooks */
    mdlDialog_hookPublish (sizeof(uHooks)/sizeof(DialogHookInfo), uHooks);

    /*
    // mdlState_registerStringIds () takes two arguments. The first is the 
    // id of the stringlist holding the Command messages, the second is the id
    // of the stringlist holding the prompt messages.
    */
    mdlState_registerStringIds (MESSAGELISTID_Strings, MESSAGELISTID_Strings);

#if defined (MSVERSION) && (MSVERSION >= 0x551)
/*----------------------------------------------------------------------+
|									|
|   Preprocess file open dialogs		       			|
|									|
|   Note:								|
|    Applications which established an interest in the event		|
|   INPUT_FILEOPENDIALOG_PREPROCESS have their user functions called in |
|   the order in which the functions where published (app load order).	|
|   Only one application may preempt the file open dialog.  Therefore	|
|   the first application to return TRUE from its user function will	|
|   preempt the file open dialog.					|
|									|
+----------------------------------------------------------------------*/
    /* The following asynch is new for MicroStation V5.5.1 */
    mdlInput_setFunction (INPUT_FILEOPENDIALOG_PREPROCESS,
		 fileOpenDialogPreprocessExample);

#endif /* (MSVERSION) && (MSVERSION >= 0x551) */
    return  SUCCESS;
    }
