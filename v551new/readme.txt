About V551NEW
===============
This application demonstrates some of the major new features for MicroStation
V5.5 MDL programmers.


Preprocess file open dialogs			
============================
Demonstrates a new event type, INPUT_FILEOPENDIALOG_PREPROCESS, for
mdlInput_setFunction.  This event is used to preempt the opening of MicroStation's
file open dialog, for the purpose of opening an application supplied 
file selection dialog box.



CmdItemListRsc example			
======================
In V4 and V5 MicroStation it was possible for tool settings to not 
show up properly - especially if you were using key-ins, a	   
tablet/paper menu, or commands tagged to function keys.	  

This problem is solved in V5.5 with the introduction of a new resource
type, CmdItemListRsc. Since it is command based, (command numbers are 
used as the IDs) CmdItemListRscs let MicroStation find the tool   
settings for the current command no matter how the command was kicked 
off.  MicroStation will now look for a CmdItemListRsc associated with 
the current command _before_ checking for an item list in a       
IconCmdRsc.	
