/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v551new/english/v551msg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   v551msg.r  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:41:02 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "v551new.h"
#include "v551txt.h"

/*----------------------------------------------------------------------+
|									|
|   Messages								|
|									|
+----------------------------------------------------------------------*/
/*======================================================================+
|									|
|   Project List Resource						|
|									|
+======================================================================*/
StringList STRINGID_ProjectList =
{
    1,
    {
    { {0}, "Project 1" },
    { {0}, "Project 2" },
    { {0}, "Project 3" },
    { {0}, "Project 4" },
    }
};

MessageList STRINGID_FileList =
{
    {
    { 0, "cd9.dgn" },
    { 1, "cd10.dgn" },
    { 2, "mug.dgn" },
    { 3, "county.dgn" },
    }
};

MessageList MESSAGELISTID_Strings =
{
    {
    { MSGID_PlaceSomethingCmdName,  "Place Something" },
    { MSGID_TextString,		    "Placed something" },
    { MSGID_PlaceSomethingPrompt,   "Enter a datapoint" },
    { MSGID_LoadCmdTbl,		    "Unable to load command table" },
    { MSGID_AttachCellLibrary,      "Attaching a cell library." },
    { MSGID_CreateDesignFile,       "Creating a design file." },
    { MSGID_AttachReferenceFile,    "Attaching a reference file." },
    { MSGID_StrLoadError,	    "Unable to load string list" },
    }
};




