/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v551new/english/v551txt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   v551txt.h  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:41:02 $
|									|
+----------------------------------------------------------------------*/
#if !defined (__v551txtH__)
#define __v551txtH__

/*----------------------------------------------------------------------+
|									|
|   Header File Dependencies						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Defines								|
|									|
+----------------------------------------------------------------------*/
#define TXT_ProjectDialogTitle      "Project Selection"
#define TXT_PlaceToolBoxTitle       "Sample Placement Tools"

#define TXT_Flyover_PlaceSomething  "Place Something Flyover Help"
#define TXT_Balloon_PlaceSomething  "Place Something"

#endif /* if !defined (__v551txtH__) */
