/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v551new/v551new.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   v551new.h  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:41:02 $
|									|
+----------------------------------------------------------------------*/
#if !defined (__v551newH__)
#define       __v551newH__

/*----------------------------------------------------------------------+
|									|
|   Header File Dependencies						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   Defines								|
|									|
+----------------------------------------------------------------------*/
#define BASEID_V551NEW			    	5500

/*----------------------------------------------------------------------+
|									|
|   Dialog Box ID's						    	|
|									|
+----------------------------------------------------------------------*/
#define DIALOGID_Project		    	(BASEID_V551NEW - 1)
#define TOOLBOXID_Place			    	(BASEID_V551NEW - 2)

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ProjectList			(BASEID_V551NEW - 1)
#define HOOKDIALOGID_ProjectDialog		(BASEID_V551NEW - 2)

/*----------------------------------------------------------------------+
|									|
|   List Box ID's							|
|									|
+----------------------------------------------------------------------*/
#define	LISTBOXID_ProjectList			(BASEID_V551NEW - 1)

/*----------------------------------------------------------------------+
|									|
|   Message List IDs							|
|									|
+----------------------------------------------------------------------*/
#define STRINGID_ProjectList			(BASEID_V551NEW - 1)
#define STRINGID_FileList			(BASEID_V551NEW - 2)
#define MESSAGELISTID_Strings			(BASEID_V551NEW - 3)

/*----------------------------------------------------------------------+
|									|
|   Messages								|
|									|
+----------------------------------------------------------------------*/
#define MSGID_StrLoadError			(BASEID_V551NEW - 1)
#define MSGID_AttachReferenceFile		(BASEID_V551NEW - 2)
#define MSGID_CreateDesignFile			(BASEID_V551NEW - 3)
#define MSGID_AttachCellLibrary			(BASEID_V551NEW - 4)
#define MSGID_LoadCmdTbl			(BASEID_V551NEW - 5)
#define MSGID_PlaceSomethingPrompt	    	(BASEID_V551NEW - 6)
#define MSGID_TextString		    	(BASEID_V551NEW - 7)
#define MSGID_PlaceSomethingCmdName		(BASEID_V551NEW - 8)

/*----------------------------------------------------------------------+
|									|
|   Icon command ids							|
|									|
+----------------------------------------------------------------------*/
#define ICONCMDID_PlaceSomething	    	(BASEID_V551NEW - 1)


#endif	/* !defined (__v551newH__) */
