/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/v551new/v551new.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   v551new.r  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:41:02 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */
#include <deffiles.h>

#include "v551new.h"
#include "v551txt.h"
#include "v551cmd.h"

/*----------------------------------------------------------------------+
|									|
|   Project Dialog - contains a list box with icons			|
|									|
+----------------------------------------------------------------------*/
#define XSIZE		(38*XC)			/* Dialog Width	       */
#define YSIZE		(12*YC)			/* Dialog Height       */

#define BW		(BUTTON_STDWIDTH+3*XC)  /* PushButton Width    */

#define X1 		(3*XC)
#define X2		((XSIZE-(2.5*BW))/2)	/* OK Button X Origin  */
#define X3		(X2+(1.5*BW))		/* Cancel Button X Origin*/

DialogBoxRsc  DIALOGID_Project =
    {
    DIALOGATTR_DEFAULT | DIALOGATTR_MODAL,
    XSIZE, YSIZE, 
    NOHELP, MHELP, HOOKDIALOGID_ProjectDialog, NOPARENTID,
    TXT_ProjectDialogTitle,
{
{{X1,GENY(2), 0,  0}, ListBox, LISTBOXID_ProjectList, ON, 0, "", ""},
{{X2,GENY(8), BW, 0}, PushButton, PUSHBUTTONID_OK,    ON, 0, "", ""},
{{X3,GENY(8), BW, 0}, PushButton, PUSHBUTTONID_Cancel,ON, 0, "", ""},
}
    };

#undef XSIZE		/* undef symbols so they can be reused */
#undef YSIZE

#undef X1
#undef X2
#undef X3

/*======================================================================+
|									|
|   Item Resource Specifications					|
|									|
+======================================================================*/
/*----------------------------------------------------------------------+
|									|
|   List Box Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_ListBoxRsc LISTBOXID_ProjectList =
    {
    NOHELP, MHELP, 
    HOOKITEMID_ProjectList, NOARG,
    LISTATTR_DYNAMICSCROLL, 5, 0, "",
	{
	{30*XC, 50, 0, ""}
	}
    };

#if defined (MSVERSION) && (MSVERSION >= 0x550)
/*----------------------------------------------------------------------+
|									|
|   Tool Box resources						    	|
|									|
|   A tool box is defined by creating a dialog box resource and tool 	|
|   box resource which have the same identifier.			|
|									|
|   The dialog box resource contains only the ToolBox item.		|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc TOOLBOXID_Place = 
    {
    DIALOGATTR_TOOLBOXCOMMON,
    0, 0,
    NOHELP, MHELP, 
    NOHOOK, NOPARENTID, 
    "",
{
{{ 0, 0, 0, 0}, ToolBox, TOOLBOXID_Place, ON, 0, "", ""},
}
    };

/*----------------------------------------------------------------------+
|									|
|   The following sample toolbox contains 4 icon commands.  The first, 	|
|   second, and fourth icon commands are part of PowerDraft or      	|
|   MicroStation, and the third icon command is part of the this    	|
|   application.						    	|
|   Note that the auxilary info field (the last parameter) is NULL for	|
|   PowerDraft's or MicroStation's icon commands, but contains the  	|
|   string: "owner=\"V550NEW\"" for the icon command which belongs to	|
|   our application.						    	|
|									|
+----------------------------------------------------------------------*/
DItem_ToolBoxRsc TOOLBOXID_Place =
    {
    NOHELP, MHELPTOPIC, NOHOOK, NOARG, 0, TXT_PlaceToolBoxTitle,
{
{{ 0, 0, 0, 0}, IconCmd, ICONCMDID_PlaceSomething,   ON, 0, "", "owner=\"V551NEW\""},
}
    };

#endif /* if defined (MSVERSION) && (MSVERSION >= 0x550) */

/*----------------------------------------------------------------------+
|									|
|   Icon Command Resources						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
| In V4 and V5 MicroStation it was possible for tool settings to not 	|
| show up properly - especially if you were using key-ins, a	    	|
| tablet/paper menu, or commands tagged to function keys.	    	|
|								    	|
| This problem was due to a design inconsistency/flaw in IconCmdRscs.	|
| Since the tool settings item list was associated to the icon (instead |
| of the command the icon executed), the tool settings could only be 	|
| found if the icon was in memory (i.e. displayed). Users of	    	|
| tablet/paper menus found this problem extremely annoying because they	|
| tend to keep their screens free of palettes/tool boxes since all of 	|
| the commands are right on the menu.				    	|
|								    	|
| This problem is solved in V5.5 with the introduction of a new resource|
| type, CmdItemListRsc. Since it is command based, (command numbers are |
| used as the IDs) CmdItemListRscs let MicroStation find the tool   	|
| settings for the current command no matter how the command was kicked |
| off.  MicroStation will now look for a CmdItemListRsc associated with |
| the current command _before_ checking for an item list in a       	|
| IconCmdRsc.							    	|
|								    	|
| This enhancement does not change the resource definition of		|
| IconCmdRscs. However, to keep your sanity you should remove the item 	|
| list from the IconCmdRsc when you have added a CmdItemListRsc.    	|
| CmdItemListRscs have been added and tool settings removed from    	|
| IconCmdRscs for all MicroStation and MicroStation MDL application 	|
| commands.							    	|
|								    	|
| Adding CmdItemListRscs is pretty easy since its item list is of the 	|
| same form as the item list in the IconCmdRsc.			    	|
|								    	|
| Restrictions:							    	|
| 1) commands and their associated CmdItemListRscs must be in the same 	|
|   application								|
|								    	|
+----------------------------------------------------------------------*/

#if defined (MSVERSION) && (MSVERSION < 0x551)

/* ----- MicroStation V5 & PowerDraft Method ----- */
DItem_IconCmdRsc ICONCMDID_PlaceSomething =
{
    NOHELP, MHELP, 0,
    CMD_PLACE_SOMETHING, OTASKID, "", "",
{
{{2*XC, GENY(1), 0, 0}, ToggleButton, TOGGLEID_LockGrid, ON, 0, "", ""}, 
{{2*XC, GENY(2), 0, 0}, ToggleButton, TOGGLEID_LockGraphicGroup, ON, 0, "", ""},
}
};

#else

/* The above IconCmdRsc gets updated and a CmdItemListRsc gets added.*/

/* ----- MicroStation V5.5 Method ----- */
DItem_IconCmdRsc ICONCMDID_PlaceSomething =
    {
    NOHELP, MHELP, 0,
    CMD_PLACE_SOMETHING, OTASKID, "", "",
	{
    	/* Tool settings in CmdItemListRsc */
	}
    }
    extendedAttributes
    {{

    /* Tool Description/Flyover Help appears in status area */
    {EXTATTR_FLYTEXT, TXT_Flyover_PlaceSomething},     

    /* Tool Tip/Balloon Help appears in yellow text box near icon command */
    {EXTATTR_BALLOON, TXT_Balloon_PlaceSomething},     
    }};

CmdItemListRsc CMD_PLACE_SOMETHING =
    {{
    {{2*XC, GENY(1), 0, 0}, ToggleButton, TOGGLEID_LockGrid, ON, 0, "", ""}, 
    {{2*XC, GENY(2), 0, 0}, ToggleButton, TOGGLEID_LockGraphicGroup, ON, 0, "", ""},
    }};

#endif /* if defined (MSVERSION) && (MSVERSION < 0x551) */


/*----------------------------------------------------------------------+
|									|
|   Text Item Resources							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Multiline Text Item Resources                                       |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Option Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   PushButton Item Resources						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Toggle Button Item Resources					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Menu Resources							|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Synonym List Resources						|
|									|
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|  Icon Resources                                                       |
|									|
+----------------------------------------------------------------------*/
IconCmdSmallRsc ICONCMDID_PlaceSomething =
    {
    23, 23, FORMAT_MONOBITMAP, BLACK_INDEX, TXT_Balloon_PlaceSomething,
	{
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02,
        0x00, 0x00, 0x0e, 0x00, 0x00, 0x1c, 0x00, 0x00,
        0x7c, 0x00, 0x00, 0x98, 0x00, 0x03, 0x38, 0x00,
        0x04, 0x30, 0x00, 0x18, 0x70, 0x00, 0x3f, 0xe0,
        0x00, 0xc0, 0xe0, 0x01, 0x80, 0xc0, 0x06, 0x01,
        0xc0, 0x1e, 0x07, 0xc0, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 
	}
    };

IconCmdLargeRsc ICONCMDID_PlaceSomething =
    {
    31, 31, FORMAT_MONOBITMAP, BLACK_INDEX, TXT_Balloon_PlaceSomething,
	{
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00,
        0x03, 0xc0, 0x00, 0x00, 0x07, 0x80, 0x00, 0x00,
        0x1b, 0x80, 0x00, 0x00, 0x37, 0x00, 0x00, 0x00,
        0xc7, 0x00, 0x00, 0x01, 0x8e, 0x00, 0x00, 0x06,
        0x0e, 0x00, 0x00, 0x0c, 0x1c, 0x00, 0x00, 0x30,
        0x1c, 0x00, 0x00, 0x60, 0x38, 0x00, 0x01, 0xff,
        0xf8, 0x00, 0x03, 0xff, 0xf0, 0x00, 0x0c, 0x00,
        0x70, 0x00, 0x18, 0x00, 0xe0, 0x00, 0xf8, 0x07,
        0xf0, 0x01, 0xf0, 0x0f, 0xe0, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 
	}
    };
