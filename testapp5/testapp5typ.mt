/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/testapp5/testapp5typ.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/testapp5/testapp5typ.mtv  $
|   $Workfile:   testapp5typ.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	testapp5 Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "testapp5.h"

publishStructures (testapp5globals);

    
