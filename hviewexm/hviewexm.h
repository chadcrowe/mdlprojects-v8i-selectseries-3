/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/hviewexm/hviewexm.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   hviewexm.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:38:34 $
|									|
+----------------------------------------------------------------------*/
#if !defined (__hviewexmH__)
#define __hviewexmH__

/*----------------------------------------------------------------------+
|									|
|   Dialog Box IDs							|
|									|
+----------------------------------------------------------------------*/
#define DIALOGID_HideExample		    	1


/*----------------------------------------------------------------------+
|									|
|   Dialog Item IDs							|
|									|
+----------------------------------------------------------------------*/
#define	MENUBARID_HideExampleMenu	    	1

#define	OPTIONBUTTONID_HideExampleView      	1
#define	OPTIONBUTTONID_HideExampleDimension	2
#define OPTIONBUTTONID_LineStyleH	    	3
#define OPTIONBUTTONID_LineStyleV	    	4

#define	TEXTID_ElementColorH		    	1
#define TEXTID_ElementColorV		    	2

#define	PUSHBUTTONID_HideExampleButton      	1
#define	PUSHBUTTONID_HideExampleDone	    	2

#define	PULLDOWNID_HideExamplePDM		1

#define TOGGLEBUTTONID_HideExampleToggleHSym 	1
#define TOGGLEBUTTONID_HideExampleToggleVSym 	2

#define	COLORPICKERID_ElementColorH	    	1
#define COLORPICKERID_ElementColorV	    	2

#define SYNONYMID_ElementColorH		    	1
#define SYNONYMID_ElementColorV		    	2


/*----------------------------------------------------------------------+
|									|
|   Hook Function IDs							|
|									|
+----------------------------------------------------------------------*/
#define	HOOKDIALOGID_HideExampleDlog	    	1
#define	HOOKITEMID_HideExampleMenu		2
#define	HOOKITEMID_HideExampleDone		3
#define	HOOKITEMID_HideExample			4
#define	HOOKITEMID_HideExampleToggleSymH    	5
#define	HOOKITEMID_HideExampleToggleSymV    	6


/*----------------------------------------------------------------------+
|									|
|   Message list IDs							|
|									|
+----------------------------------------------------------------------*/
#define MESSAGELISTID_Msgs		    	1

/*----------------------------------------------------------------------+
|									|
|   Command IDs - used in the Message list definition for command names	|
|									|
+----------------------------------------------------------------------*/
#define	MSGID_LoadCmdTbl	1
#define	MSGID_DialogOpen	2
#define	MSGID_RscOpenErr	3
#define MSGID_ViewPrompt    	4

/*----------------------------------------------------------------------+
|									|
|   Typedefs for Dialog access strings					|
|									|
+----------------------------------------------------------------------*/
typedef struct dlogboxinfo
{
    double      fmtStr;
    int	      	styleH;
    int		styleV;
    int		SetSymH;
    int		SetSymV;
    int		colorH;
    int		colorV;
    int		viewNumber;
    int		dimension;
} DlogBoxInfo;

#endif /* !defined (__hviewexmH__) */