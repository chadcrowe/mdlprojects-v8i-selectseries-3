/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/hviewexm/hviewexm.mc,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   hviewexm.mc  $
|   $Revision: 1.4.36.1 $
|   	$Date: 2013/07/01 20:38:35 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   hviewexm.mc - HVIEWEXM source code.                                 |
|              Illustrates Hidden line MDL interface		    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Include Files                                                       |
|                                                                       |
+----------------------------------------------------------------------*/
#include <mdl.h>	/* MDL Library funcs structures & constants */
#include <tcb.h>	/* MicroStation terminal control block */
#include <dlogitem.h>	/* Dialog Box Manager structures & constants */
#include <dlogman.fdf>  /* Dialog Box Manager Function Prototypes */
#include <dlogids.h>
#include <cmdlist.h>	/* MicroStation command numbers */
#include <cexpr.h>	/* C Expression structures & constants */
#include <userfnc.h>	/* definitions for accessing MDL user functions */
#include <mselems.h>	/* structures that define MicroStation elements */
#include <rscdefs.h>	/* resource mgr structure definitions & constants */
#include <scanner.h>	/* typedefs and defines for design file scanner */
#include <mdlio.h>	/* File I/O definitions and data structs for MDL */
#include <toolset.h>	/* MicroStation tool set definitions */	  
#include <stdlib.h> 
#include <string.h>	/* String function prototypes */	  
#include <mstypes.h>
#include <mdlhview.h>   /* hidden line header files*/

#include <mdlhview.fdf> /* hidden line functions */
#include <msstate.fdf> 
#include <mssystem.fdf> 
#include <msoutput.fdf> 
#include <msparse.fdf> 
#include <mscexpr.fdf> 
#include <msrsrc.fdf> 

#include "hviewexm.h"	/* Contains Dialog Box IDs for this app */
#include "hviewcmd.h"   /* Contains Command numbers for this app */

/*----------------------------------------------------------------------+
|									|
|   Private Global variables						|
|									|
+----------------------------------------------------------------------*/
Private	DlogBoxInfo     *dlogBoxInfo;
	   

/*----------------------------------------------------------------------+
|                                                                       |
|   Local Function Declarations                                         |
|                                                                       |
+----------------------------------------------------------------------*/

/*----------------------------------------------------------------------+
|									|
|   HVIEWEXM Code Section  						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name		hideExample_processView					|
|									|
| author	BSI					11/93		|
|									|
+----------------------------------------------------------------------*/
Private void hideExample_processView
(
DPoint3d	*point,
int			actView
)
    {
    int		    viewNumber, fileType, options;
    HviewContextP   contextP;
    HLineSymbology  hiddenSymbology;
    HLineSymbology  visibleSymbology;

	  

    if (dlogBoxInfo->viewNumber == 0)
	viewNumber = actView;
    else
    	viewNumber = dlogBoxInfo->viewNumber - 1;

    if (viewNumber < 0 || viewNumber > 7 ) viewNumber = 0;

    if  (dlogBoxInfo->dimension == 2)
    	{
	fileType = MDLHVIEW_FILE_2D;
    	}
    else
        fileType = MDLHVIEW_FILE_3D;
    
    if( contextP = mdlHview_openContext(viewNumber) )
    	{
        mdlHview_clearView(contextP);
    
    	if (dlogBoxInfo->viewNumber == 0)
	    {
	    mdlHview_openFile(contextP, fileType, NULL);
	    }

    	/*  Change hidden line symbology 
	    This will over ride the color and line style of the display
	*/
	if ( dlogBoxInfo->SetSymH != 0)
	    {
	    hiddenSymbology.styleOverride = 1;
	    hiddenSymbology.colorOverride = 1;
	    hiddenSymbology.style = dlogBoxInfo->styleH;
	    hiddenSymbology.color = dlogBoxInfo->colorH;
	    mdlHview_message (contextP, MDLHVIEW_MSG_SET_INCLUDE_HIDDEN, TRUE, NULL);
	    mdlHview_message (contextP, MDLHVIEW_MSG_SET_HIDDEN_SYMBOLOGY, 0, &hiddenSymbology);
	    }

    	/*
	    Change Visible line symbology 
	    This will over ride the color and style of the display
	*/
	if ( dlogBoxInfo->SetSymV !=0)
	    {
	    visibleSymbology.styleOverride = 1;
	    visibleSymbology.colorOverride = 1;
	    visibleSymbology.style = dlogBoxInfo->styleV;
	    visibleSymbology.color = dlogBoxInfo->colorV;
	    mdlHview_message (contextP, MDLHVIEW_MSG_SET_VISIBLE_SYMBOLOGY, 0, &visibleSymbology);
	    }


    	mdlHview_processView(contextP);
    
    	mdlHview_closeContext(contextP);
    	}
    return;
    }

/*----------------------------------------------------------------------+
|									|
|	Event Handler routines						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          hideExample_done  					|
|                                                                       |
| author        BSI                                     8/90            |
|									|
+----------------------------------------------------------------------*/
Private void  hideExample_done
(
void
)
    {
    mdlState_setFunction(STATE_DATAPOINT, NULL);
    mdlState_startDefaultCommand();
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          hideViewFile					    	|
|                                                                       |
| author        BSI                                     8/90            |
|									|
+----------------------------------------------------------------------*/
Private void  hideViewFile
(
char *unparsedP
)
cmdNumber CMD_HIDE_VIEW_FILE
    {

    mdlState_startPrimitive(hideExample_processView, hideExample_done,0,0);

    mdlOutput_rscPrintf(MSG_PROMPT, NULL, MESSAGELISTID_Msgs,
				    MSGID_ViewPrompt);
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          hideViewSelected  					|
|                                                                       |
| author        BSI                                     8/90            |
|									|
+----------------------------------------------------------------------*/
Private void hideViewSelected
(
void
)
cmdNumber	CMD_HIDE_VIEW_NUMBER
    {
    Dpoint3d	pnt;
    int			actView;

    pnt.x	= 0.0;
    pnt.y	= 0.0;
    pnt.z	= 0.0;

    actView = dlogBoxInfo->viewNumber;
    hideExample_processView(&pnt, actView);
    hideExample_done();
    }


/*----------------------------------------------------------------------+
|                                                                       |
| name          hideExample_dlogHook  					|
|                                                                       |
| author        BSI                                     8/90            |
|									|
+----------------------------------------------------------------------*/
Private void    hideExample_dlogHook
(
DialogMessage   *dmP    /* => a ptr to a dialog message */
)
    {
    dmP->msgUnderstood = TRUE;

    switch (dmP->messageType)
	{
	case DIALOG_MESSAGE_DESTROY:
	    {
  	    mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
	    mdlState_startDefaultCommand ();
		dmP->msgUnderstood = FALSE;
	    break;
	    };

	default:
	    dmP->msgUnderstood = FALSE;
	    break;
	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Dialog Item Message		                                        |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
| name          hideExample_doneHook  	                                |
|                                                                       |
| author        BSI                                     8/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void    hideExample_doneHook
(
DialogItemMessage       *dimP   /* => a ptr to a dialog item message */
)
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
      	case DITEM_MESSAGE_BUTTON:
	    {
	    mdlDialog_cmdNumberQueue (FALSE, CMD_MDL_UNLOAD,
				      mdlSystem_getCurrTaskID(), TRUE);
	    mdlState_startDefaultCommand ();
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
| name          hideExample_toggleHookH	                                |
|                                                                       |
| author        BSI                                     8/93            |
|                                                                       |
+----------------------------------------------------------------------*/
Private void hideExample_toggleHookH
(
DialogItemMessage		*dimP
)
    {
    DialogItem	*dip1, *dip2, *dip3;


    dimP->msgUnderstood =TRUE;


    switch (dimP->messageType)
	{
	case DITEM_MESSAGE_ALLCREATED:
		 {
		dip1 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_Text,TEXTID_ElementColorH,
									   		  0);
		dip2 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_ColorPicker, COLORPICKERID_ElementColorH,
											  0);
		dip3 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_OptionButton,OPTIONBUTTONID_LineStyleH,
											  0);

		if (dlogBoxInfo->SetSymH == 0)
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, FALSE, FALSE);
			}
		else
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, TRUE, FALSE);
			}

		dimP->msgUnderstood = FALSE;
		break;
		}


	case DITEM_MESSAGE_STATECHANGED:
		{
		if ( dimP->u.stateChanged.reallyChanged == TRUE)
		{
		dip1 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_Text,TEXTID_ElementColorH,
									   		  0);
		dip2 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_ColorPicker, COLORPICKERID_ElementColorH,
											  0);
		dip3 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_OptionButton,OPTIONBUTTONID_LineStyleH,
											  0);

		if (dlogBoxInfo->SetSymH == 0)
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, FALSE, FALSE);
			}
		else
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, TRUE, FALSE);
			}
		}
		dimP->msgUnderstood = FALSE;
		break;
	    }
	default:
	    dimP->msgUnderstood = FALSE;
	    break;
	}

    }

/*--------------------------------------------------------------------------------------------------+
|                                                                                                   |
|   hideExample_toggleHookV				                                                                          |
|                                                                                                   |
+--------------------------------------------------------------------------------------------------*/

Private void hideExample_toggleHookV
(
DialogItemMessage		*dimP
)
{

DialogItem	*dip1, *dip2, *dip3;


dimP->msgUnderstood =TRUE;


switch (dimP->messageType)
	{
	case DITEM_MESSAGE_ALLCREATED:
		{
		dip1 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_Text,TEXTID_ElementColorV,
									   		  0);
		dip2 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_ColorPicker, COLORPICKERID_ElementColorV,
											  0);
		dip3 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_OptionButton,OPTIONBUTTONID_LineStyleV,
											  0);
	   
		if (dlogBoxInfo->SetSymV == 0)
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, FALSE, FALSE);
			}
		else
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, TRUE, FALSE);
			}
		
		dimP->msgUnderstood = FALSE;
		break;
		}


	case DITEM_MESSAGE_STATECHANGED:
		{
		if ( dimP->u.stateChanged.reallyChanged == TRUE)
		{
		dip1 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_Text,TEXTID_ElementColorV,
									   		  0);
		dip2 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_ColorPicker, COLORPICKERID_ElementColorV,
											  0);
		dip3 = mdlDialog_itemGetByTypeAndId ( dimP->db, RTYPE_OptionButton,OPTIONBUTTONID_LineStyleV,
											  0);
	   
		if (dlogBoxInfo->SetSymV == 0)
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, FALSE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, FALSE, FALSE);
			}
		else
			{
			mdlDialog_itemSetEnabledState (dimP->db, dip1->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip2->itemIndex, TRUE, FALSE);
			mdlDialog_itemSetEnabledState (dimP->db, dip3->itemIndex, TRUE, FALSE);
			}
		}
		dimP->msgUnderstood = FALSE;
		break;
		}
	default:
		 dimP->msgUnderstood = FALSE;
		 break;
	}

}



/*----------------------------------------------------------------------+
|									|
| name		hideExample_menuHook						|
|									|
| author	BSI					10/93		|
|									|
+----------------------------------------------------------------------*/
Private void    hideExample_menuHook
(
DialogItemMessage       *dimP   /* => a ptr to a dialog item message */
)
    {

   dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{
       	case DITEM_MESSAGE_BUTTON:
	    {

		if (dlogBoxInfo->viewNumber >= 1 )
     		mdlDialog_cmdNumberQueue(TRUE, CMD_HIDE_VIEW_NUMBER, NULL, FALSE);
		else
			mdlDialog_cmdNumberQueue(TRUE, CMD_HIDE_VIEW_FILE, NULL, FALSE);

		
	  	dimP->msgUnderstood = FALSE;	
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }


/*----------------------------------------------------------------------+
| Mycheck									|
| author	BSI					10/93		|
|									|
+----------------------------------------------------------------------*/
Private void    hideExample_function
(
DialogItemMessage       *dimP   /* => a ptr to a dialog item me*/
) 
    {
    dimP->msgUnderstood = TRUE;

    switch (dimP->messageType)
      	{								  
      	case DITEM_MESSAGE_BUTTON:
	    {
		
	   if (dlogBoxInfo->viewNumber >= 1 )
     		mdlDialog_cmdNumberQueue(TRUE, CMD_HIDE_VIEW_NUMBER, NULL, FALSE);
		else
			mdlDialog_cmdNumberQueue(TRUE, CMD_HIDE_VIEW_FILE, NULL, FALSE);

		
	  	dimP->msgUnderstood = FALSE;	
	    break;
	    };

      	default:
	    dimP->msgUnderstood = FALSE;
	    break;
      	}
    }

/*----------------------------------------------------------------------+
|                                                                       |
|   Main			                                        |
|                                                                       |
+----------------------------------------------------------------------*/

Private DialogHookInfo uHooks[]=
    {
    {HOOKDIALOGID_HideExampleDlog,	hideExample_dlogHook},
    {HOOKITEMID_HideExampleMenu,	hideExample_menuHook},
    {HOOKITEMID_HideExampleDone,	hideExample_doneHook},
    {HOOKITEMID_HideExampleToggleSymH,	hideExample_toggleHookH},
    {HOOKITEMID_HideExampleToggleSymV,	hideExample_toggleHookV},
    {HOOKITEMID_HideExample,     	hideExample_function},
    };

/*----------------------------------------------------------------------+
|                                                                       |
| name          main                                                    |
|                                                                       |
| author        BSI                                     6/95            |
|                                                                       |
+----------------------------------------------------------------------*/
Public int main 
(
int 	argc,
char 	*argv[]
)
    {
    RscFileHandle   rscFileH;   /* a resource file handle */
    SymbolSet	    *setP;      /* a ptr to a "C expression symbol set" */


    mdlResource_openFile (&rscFileH, NULL, 0);

    if (mdlParse_loadCommandTable (NULL) == NULL)
    	{
	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Msgs,
	    MSGID_LoadCmdTbl);
    	return ERROR;    	
    	}

    mdlDialog_hookPublish (sizeof (uHooks)/sizeof (DialogHookInfo), uHooks);

    dlogBoxInfo = malloc (sizeof (DlogBoxInfo));
    setP=mdlCExpression_initializeSet (VISIBILITY_DIALOG_BOX, 0, TRUE);
    mdlDialog_publishComplexPtr (setP, "dlogboxinfo", "dlogBoxInfo", &dlogBoxInfo);

    dlogBoxInfo->SetSymH = 0;
    dlogBoxInfo->SetSymV = 0;

    dlogBoxInfo->styleH = 0;
    dlogBoxInfo->styleV = 0;
    dlogBoxInfo->colorH = tcb->symbology.color;
    dlogBoxInfo->colorV = tcb->symbology.color;

    dlogBoxInfo->viewNumber = 0;

    dlogBoxInfo->dimension = 2;

    if (NULL == mdlDialog_open (NULL, DIALOGID_HideExample))
    	{
    	mdlOutput_rscPrintf (MSG_ERROR, NULL, MESSAGELISTID_Msgs,
	    MSGID_DialogOpen);
	return	ERROR;
    	}

    return SUCCESS;
    }



