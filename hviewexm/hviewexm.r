/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/hviewexm/hviewexm.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/hviewexm/hviewexm.r_v  $
|   $Workfile:   hviewexm.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:38:37 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   hviewexm.r - HVIEWEXM Dialog Box resource definitions		|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>
#include <cmdlist.h>

#include "hviewcmd.h"
#include "hviewexm.h"

/*----------------------------------------------------------------------+
|									|
|   Dialog Box Resource Specification					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Hviewexm Dialog Box							|
|									|
+----------------------------------------------------------------------*/
DialogBoxRsc DIALOGID_HideExample=
   {
   DIALOGATTR_DEFAULT | DIALOGATTR_SINKABLE,
   40*XC, 20*YC,
   NOHELP, MHELP, HOOKDIALOGID_HideExampleDlog, NOPARENTID,
   "Hide Example",
{
{{0, 0, 0, 0}, MenuBar, MENUBARID_HideExampleMenu, ON, 0, "", ""},
{{18*XC, GENY(3), 10*XC, 0}, OptionButton, OPTIONBUTTONID_HideExampleView, ON, 0, "",""},
{{18*XC, GENY(5), 10*XC, 0},OptionButton, OPTIONBUTTONID_HideExampleDimension, ON, 0, "", ""},
{{22*XC, GENY(7), 0, 0 }, ToggleButton, TOGGLEBUTTONID_HideExampleToggleHSym, ON, 0, "",""},
{{25*XC,  GENY(9), 5*XC,  0}, Text, TEXTID_ElementColorH, ON, 0, "", ""},
{{32*XC, GENY(9), 0,     0}, ColorPicker, COLORPICKERID_ElementColorH, ON, 0, "", ""},
{{25*XC,  GENY(11), 10*XC, 0}, OptionButton, OPTIONBUTTONID_LineStyleH, ON, 0, "", ""},
{{3*XC, GENY(7), 0, 0 }, ToggleButton, TOGGLEBUTTONID_HideExampleToggleVSym, ON, 0, "",""},
{{8*XC,  GENY(9), 5*XC,  0}, Text, TEXTID_ElementColorV, ON, 0, "", ""},
{{14*XC, GENY(9), 0,     0}, ColorPicker, COLORPICKERID_ElementColorV, ON, 0, "", ""},
{{6*XC,  GENY(11), 10*XC, 0}, OptionButton, OPTIONBUTTONID_LineStyleV, ON, 0, "", ""},
{{8*XC, GENY(14), 10*XC, 0}, PushButton, PUSHBUTTONID_HideExampleButton, ON, 0, "", ""},
{{22*XC,GENY(14), 10*XC, 0}, PushButton, PUSHBUTTONID_HideExampleDone, ON, 0, "", ""}, 
}
   };


/*----------------------------------------------------------------------+
|									|
|   Item Resource Specifications					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Menu Bar Item Resource      					|
|									|
+----------------------------------------------------------------------*/
DItem_MenuBarRsc MENUBARID_HideExampleMenu =
    {
    NOHOOK, NOARG,
	{
	{PulldownMenu, PULLDOWNID_HideExamplePDM},
	}
    };

/*----------------------------------------------------------------------+
|									|
|   PullDown Menu Item Resource						|
|									|
+----------------------------------------------------------------------*/
DItem_PulldownMenuRsc PULLDOWNID_HideExamplePDM =
    {
    NOHELP, MHELP, NOHOOK, ON | ALIGN_LEFT, "~menu",
{
{"hide", 0, ON, NOMARK, 0, NOSUBMENU, NOHELP, MHELP, HOOKITEMID_HideExampleMenu, NOARG, NOCMD, LCMD, ""}, 
}
    };

/*----------------------------------------------------------------------+
|                                                                       |
|   Option Button Item Resource Definitions				|
|                                                                       |
+----------------------------------------------------------------------*/

DItem_OptionButtonRsc OPTIONBUTTONID_HideExampleView =
    {
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    "View Number",
    "dlogBoxInfo->viewNumber",
    {
{NOTYPE, NOICON, NOCMD, LCMD, 0, NOMASK, ON, "To file"},
{NOTYPE, NOICON, NOCMD, LCMD, 1, NOMASK, ON, "View 1"},
{NOTYPE, NOICON, NOCMD, LCMD, 2, NOMASK, ON, "View 2"},
{NOTYPE, NOICON, NOCMD, LCMD, 3, NOMASK, ON, "View 3"},
{NOTYPE, NOICON, NOCMD, LCMD, 4, NOMASK, ON, "View 4"},
{NOTYPE, NOICON, NOCMD, LCMD, 5, NOMASK, ON, "View 5"},
{NOTYPE, NOICON, NOCMD, LCMD, 6, NOMASK, ON, "View 6"},
{NOTYPE, NOICON, NOCMD, LCMD, 7, NOMASK, ON, "View 7"},
{NOTYPE, NOICON, NOCMD, LCMD, 8, NOMASK, ON, "View 8"},
    }
    };

DItem_OptionButtonRsc OPTIONBUTTONID_HideExampleDimension =
    {
    NOSYNONYM,
    NOHELP,
    LHELP,
    NOHOOK,
    NOARG,
    "Dimension",
    "dlogBoxInfo->dimension",
    {
{NOTYPE, NOICON, NOCMD, LCMD, 2, NOMASK, ON, "- 2D -"},
{NOTYPE, NOICON, NOCMD, LCMD, 3, NOMASK, ON, "- 3D -"},
}
};

DItem_OptionButtonRsc OPTIONBUTTONID_LineStyleH =
    {
    SYNONYMID_ElementStyle, NOHELP, LHELP, 
    NOHOOK, OPTNBTNATTR_NEWSTYLE | NOARG, 
    "~Line:", "dlogBoxInfo->styleH", 
{
{Icon, ICONID_LineStyle0, CMD_ACTIVE_STYLE_CSELECT, MCMD, 0, NOMASK, ON, "~0"},
{Icon, ICONID_LineStyle1, CMD_ACTIVE_STYLE_CSELECT, MCMD, 1, NOMASK, ON, "~1"},
{Icon, ICONID_LineStyle2, CMD_ACTIVE_STYLE_CSELECT, MCMD, 2, NOMASK, ON, "~2"},
{Icon, ICONID_LineStyle3, CMD_ACTIVE_STYLE_CSELECT, MCMD, 3, NOMASK, ON, "~3"},
{Icon, ICONID_LineStyle4, CMD_ACTIVE_STYLE_CSELECT, MCMD, 4, NOMASK, ON, "~4"},
{Icon, ICONID_LineStyle5, CMD_ACTIVE_STYLE_CSELECT, MCMD, 5, NOMASK, ON, "~5"},
{Icon, ICONID_LineStyle6, CMD_ACTIVE_STYLE_CSELECT, MCMD, 6, NOMASK, ON, "~6"},
{Icon, ICONID_LineStyle7, CMD_ACTIVE_STYLE_CSELECT, MCMD, 7, NOMASK, ON, "~7"},
}
    };

DItem_OptionButtonRsc OPTIONBUTTONID_LineStyleV =
    {
    SYNONYMID_ElementStyle, NOHELP, LHELP, 
    NOHOOK, OPTNBTNATTR_NEWSTYLE | NOARG, 
    "~Line:", "dlogBoxInfo->styleV", 
{
{Icon, ICONID_LineStyle0, CMD_ACTIVE_STYLE_CSELECT, MCMD, 0, NOMASK, ON, "~0"},
{Icon, ICONID_LineStyle1, CMD_ACTIVE_STYLE_CSELECT, MCMD, 1, NOMASK, ON, "~1"},
{Icon, ICONID_LineStyle2, CMD_ACTIVE_STYLE_CSELECT, MCMD, 2, NOMASK, ON, "~2"},
{Icon, ICONID_LineStyle3, CMD_ACTIVE_STYLE_CSELECT, MCMD, 3, NOMASK, ON, "~3"},
{Icon, ICONID_LineStyle4, CMD_ACTIVE_STYLE_CSELECT, MCMD, 4, NOMASK, ON, "~4"},
{Icon, ICONID_LineStyle5, CMD_ACTIVE_STYLE_CSELECT, MCMD, 5, NOMASK, ON, "~5"},
{Icon, ICONID_LineStyle6, CMD_ACTIVE_STYLE_CSELECT, MCMD, 6, NOMASK, ON, "~6"},
{Icon, ICONID_LineStyle7, CMD_ACTIVE_STYLE_CSELECT, MCMD, 7, NOMASK, ON, "~7"},
}
    };




/*----------------------------------------------------------------------+
|                                                                       |
|   Push Button Item Resource Definitions				|
|                                                                       |
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_HideExampleDone =
   {
   DEFAULT_BUTTON, NOHELP, MHELP, 
   HOOKITEMID_HideExampleDone, NOARG, NOCMD, LCMD, "", 
   "D~one"
   };



DItem_PushButtonRsc PUSHBUTTONID_HideExampleButton =
   {
   NOT_DEFAULT_BUTTON, NOHELP, MHELP, 
   HOOKITEMID_HideExample, NOARG, NOCMD, LCMD, "", 
   "~Hide"
   };


DItem_TextRsc TEXTID_ElementColorH =
   {
   NOCMD, MCMD, SYNONYMID_ElementColorH, NOHELP, MHELP, NOHOOK, NOARG, 
   25, "%d", "%d", "", "", NOMASK, NOCONCAT, 
   "H~Color:", 
   "dlogBoxInfo->colorH"
   };

DItem_TextRsc TEXTID_ElementColorV =
   {
   NOCMD, MCMD, SYNONYMID_ElementColorV, NOHELP, MHELP, NOHOOK, NOARG, 
   25, "%d", "%d", "", "", NOMASK, NOCONCAT, 
   "V~Color:", 
   "dlogBoxInfo->colorV"
   };




DItem_ToggleButtonRsc TOGGLEBUTTONID_HideExampleToggleHSym =
	{
	NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_HideExampleToggleSymH, NOARG, NOMASK, NOINVERT,
	"hidden Line Sym",
	"dlogBoxInfo->SetSymH"
	};

DItem_ToggleButtonRsc TOGGLEBUTTONID_HideExampleToggleVSym =
	{
	NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP, HOOKITEMID_HideExampleToggleSymV, NOARG, NOMASK, NOINVERT,
	"Visible Line Sym",
	"dlogBoxInfo->SetSymV"
	};

DItem_ColorPickerRsc	COLORPICKERID_ElementColorH =
	{
	NOCMD, MCMD, SYNONYMID_ElementColorH, NOHELP, MHELP, NOHOOK, NOARG,
	TEXTID_ElementColorH, NOMASK, "", "dlogBoxInfo->colorH"
	};

DItem_ColorPickerRsc	COLORPICKERID_ElementColorV =
	{
	NOCMD, MCMD, SYNONYMID_ElementColorV, NOHELP, MHELP, NOHOOK, NOARG,
	TEXTID_ElementColorV, NOMASK, "", "dlogBoxInfo->colorV"
	};
