/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/hviewexm/english/hviewmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   hviewmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:38:41 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   hviewmsg.r - HVIEWEXM Message list resource   	    	    	|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <rscdefs.h>

#include "hviewexm.h"

/*----------------------------------------------------------------------+
|									|
|   Messages List Resource Definition					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|								        |
|   The Message List resource structure is defined as follows:		|
|									|
|   #if defined (resource)						|
|   	typedef struct __messagelist__					|
|   	    {								|
|   	    ULong	1;   # No. expected infoFields per string.	|
|   	    struct messages						|
|   	    	{							|
|   	    	ULong   infoFields[];					|
|       	char    msg[];						|
|       	} Messages [];						|
|	    } MessageList;						|
|									|
+----------------------------------------------------------------------*/

MessageList MESSAGELISTID_Msgs =
{
    {
    {MSGID_LoadCmdTbl, 		"Unable to load command table"},
    {MSGID_DialogOpen, 		"Unable to open main dialog"},
    {MSGID_RscOpenErr, 		"Unable to open resource file"},
    {MSGID_ViewPrompt,		"Select view"},
    }
};


