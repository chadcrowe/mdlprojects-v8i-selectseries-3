/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/hviewexm/english/hviewtxt.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/hviewexm/english/hviewtxt.h_v  $
|   $Workfile:   hviewtxt.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:38:43 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    hviewtxt.h	    English language defines for HVIEWEXM example	|
|									|
+----------------------------------------------------------------------*/

#if !defined (__hviewtxtH__)
#define __hviewtxtH__

/*----------------------------------------------------------------------+
|									|
|    Defines								|
|									|
+----------------------------------------------------------------------*/
#define TXT_Main		    		"MyApp Example"
#define TXT_CmdFrameTitle		"MyApp"
#define TXT_MyappPalette		"My Tools"
#define TXT_MainToolBox			"Myapp Tools and settings"

#define TXT_PlaceMyLine			"Place MyLine"
#define TXT_PlaceMyDate			"Place MyDate"

/* pulldown menu titles */
#define TXT_PaletteMenu			"~Palette"
#define TXT_Help		    		"~Help"

/* Palette pulldown menu item labels */
#define TXT_MyappMenuItem		"~MyApp"
#define TXT_ChangeSymItem		"Change My~Symbology"
#define TXT_PlaceLineItem		"Place My~Line"
#define TXT_PlaceDateItem		"Place My~Date"

/* Help pulldown menu item labels */
#define TXT_HelpContents	   "~Contents"
#define TXT_HelpSearch		   "~Search for Help On..."
#define TXT_HelpOnHelp		   "~How to Use Help"
#define TXT_HelpAllFiles	   "~All Available Files"


/* Dialog item labels */
#define TXT_DateItemLabel		"~Date:"
#define TXT_ColrItemLabel		"~Color:"
#define TXT_OptnItemLabel		"~Line:"
#define TXT_ButnItemLabel		"D~one"


/* Balloon and flyover help text */
#define TXT_Flyover_PlaceMyLine	"Place line command from Myapp"
#define TXT_Balloon_PlaceMyLine	"Place MyLine"
#define TXT_Flyover_PlaceMyDate	"Place date command from Myapp"
#define TXT_Balloon_PlaceMyDate	"Place MyDate"
#define TXT_Flyover_ChangeMySymbology	"Change symbology command from Myapp"
#define TXT_Balloon_ChangeMySymbology	"Change MySymbology"
#define TXT_Flyover_DoneButton	"Push Done button to unload Myapp"
#define TXT_Balloon_DoneButton	"Done with Myapp"
#define TXT_Flyover_OptionButton	"Line style options for place myline command"
#define TXT_Balloon_OptionButton	"Line style options"

#endif /* #if !defined (__hviewtxtH__) */