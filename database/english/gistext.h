/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/database/english/gistext.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|     $Source: /miscdev-root/miscdev/mdl/examples/database/english/gistext.h,v $
|   $Workfile:   gistext.h  $
|   $Revision: 1.4.76.1 $
|   	$Date: 2013/07/01 20:37:04 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	GIS Example application static dialog box text			|
|									|
+----------------------------------------------------------------------*/
#if !defined (__gistextH__)
#define	__gistextH__

/*----------------------------------------------------------------------+
|									|
|    Dialog box aspect ratio						|
|									|
+----------------------------------------------------------------------*/
#define ASPECT_LOCATEPARCEL	1.0	/* locate parcel dialog box */

/*----------------------------------------------------------------------+
|									|
|    Dialog box aspect ratio						|
|									|
+----------------------------------------------------------------------*/
#define	TXT_ParcelLocation				"Parcel Location"
#define	TXT_ParcelNumber				"Parcel Number:"
#define	TXT_LocateParcel				"Locate Parcel"

#endif /* if !defined (__gistextH__) */
