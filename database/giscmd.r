/*----------------------------------------------------------------------+
|                                                                       |
| $Copyright: (c) 2008 Bentley Systems, Incorporated. All rights reserved. $
|                                                                       |
| Limited permission is hereby granted to reproduce and modify this     |
| copyrighted material provided that the resulting code is used only in |
| conjunction with Bentley Systems products under the terms of the      |
| license agreement provided therein, and that this notice is retained  |
| in its entirety in any such reproduction or modification.             |
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   $Logfile:   J:/mdl/examples/database/giscmd.r_v  $
|   $Workfile:   giscmd.r  $
|   $Revision: 1.4 $
|   $Date: 2008/12/19 14:20:05 $
|                                                                       |
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|                                                                       |
|   Function -                                                          |
|                                                                       |
|        Main GIS Commands                                              |
|                                                                       |
+----------------------------------------------------------------------*/
#include <dlogids.h>
#include <rscdefs.h>
#include <cmdclass.h>

/*-----------------------------------------------------------------------
 Setup for native code only MDL app
-----------------------------------------------------------------------*/
#define  DLLAPP_PRIMARY     1

DllMdlApp   DLLAPP_PRIMARY =
    {
    "GIS", "gis"
    }

#define CT_NONE                         0
#define CT_EXAMPLEGISCOMMANDS           1
#define CT_PARCELCOMMAND                2
#define CT_PUSHPINCOMMAND               3
#define CT_LOCATEMODE                   4

Table CT_EXAMPLEGISCOMMANDS =
{
    {  1, CT_PARCELCOMMAND,     DATABASE,       TRY,        "PARCEL" },
    {  2, CT_NONE,              DATABASE,       DEF,        "PUSHPIN" },
};

/*------------------------------------------------ */
/*      Parcel Subtable                            */
/*------------------------------------------------ */
Table CT_PARCELCOMMAND =
{
    {  1, CT_NONE,              INHERIT,        DEF | TRY,  "ATTACH" },
    {  2, CT_NONE,              INHERIT,        TRY,        "LOCATE" },
    {  3, CT_NONE,              INHERIT,        TRY,        "LOADDAS"},
    {  4, CT_LOCATEMODE,        INHERIT,        TRY,        "SILENT"}
};

/*------------------------------------------------ */
/*      Silent Mode Subtable                       */
/*------------------------------------------------ */
Table CT_LOCATEMODE =
{
    {  1, CT_NONE,              INHERIT,        DEF | TRY,  "ON"},
    {  2, CT_NONE,              INHERIT,        TRY,        "OFF"}
};

