/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/database/gis.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/database/gis.r_v  $
|   $Workfile:   gis.r  $
|   $Revision: 5.8.76.1 $
|   	$Date: 2013/07/01 20:36:35 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	GIS Example dialog box resources				|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>
#include <dlogids.h>

#include "gisid.h"
#include "gistext.h"

/*----------------------------------------------------------------------+
|									|
|   Parcel Locate Dialog Box						|
|									|
+----------------------------------------------------------------------*/
#undef	    XC
#define	    XC		(DCOORD_RESOLUTION/2) * ASPECT_LOCATEPARCEL

#define DW	(35*XC)
#define DH	(5.5*YC)

#define X1	(15*XC)		/* text field */
#define X2	(10*XC)		/* Locate button */

#define Y1	(YC)		/* text field */
#define Y2	(2.75*YC)	/* Locate button */

DialogBoxRsc DIALOGID_Locate =
    {
    DIALOGATTR_DEFAULT,
    DW, DH,
    NOHELP, MHELP, NOHOOK, NOPARENTID,
    TXT_ParcelLocation,
{
{{ X1, Y1, 16*XC, 0},
	Text, TEXTID_ParcelID,  ON, 0, TXT_ParcelNumber, ""},
{{ X2, Y2, (16*XC), 0},
	PushButton, PUSHBUTTONID_Locate, ON, 0, "", ""},
}
};

/*----------------------------------------------------------------------+
|									|
|    PushButton Resources						|
|									|
+----------------------------------------------------------------------*/
DItem_PushButtonRsc PUSHBUTTONID_Locate =
    {
    NOT_DEFAULT_BUTTON, NOHELP, MHELP,
    HOOKITEMID_LocateButton, NOARG, NOCMD, MCMD, "",
    TXT_LocateParcel
    }

/*----------------------------------------------------------------------+
|									|
|    Text Resources							|
|									|
+----------------------------------------------------------------------*/
DItem_TextRsc TEXTID_ParcelID =
    {
    NOCMD, MCMD, NOSYNONYM, NOHELP, MHELP,
    NOHOOK, NOARG,
    16, "%s", "%s", "", "", NOMASK, CONCAT,
    "",
    ""
    };
