/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mline/mline.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/mline/mline.r_v  $
|   $Workfile:   mline.r  $
|   $Revision: 1.2.76.1 $
|      	$Date: 2013/07/01 20:40:29 $
|									|
+----------------------------------------------------------------------*/

#include "rscdefs.h"
#include "cmdclass.h"

#define	    CT_NONE	    0
#define	    CT_MAIN	    1
#define	    CT_PLACE	    2
#define	    CT_MLINE	    3

Table	CT_MAIN =
{ 
    { 1,  CT_PLACE,	PLACEMENT,   REQ,	"PLACE"     }, 
};

Table	CT_PLACE =
{ 
    { 1,  CT_MLINE,	INHERIT,     REQ,	"BLOCK"     },
};

Table	CT_MLINE =
{ 
    { 1,  CT_NONE,	INHERIT,     REQ,	"MLINE"     },
};

	    
MessageList 0 =
{
    {
    { 0, "" },
    { 1, "Place Multi-line Block" },
    { 2, "Enter first point" },
    { 3, "Enter opposite corner" },
    { 4, "Unable to load command table"},
    }
};



	    

