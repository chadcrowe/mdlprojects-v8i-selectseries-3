/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mline/mlinecmd.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mline/mlinecmd.r_v  $
|   $Workfile:   mlinecmd.r  $
|   $Revision: 1.3.32.1 $
|   $Date: 2013/07/01 20:40:30 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MLine application command table resources			|
|									|
+----------------------------------------------------------------------*/
#include "rscdefs.h"
#include "cmdclass.h"

#define  DLLAPP_MLINE        1

DllMdlApp DLLAPP_MLINE =
    {
    "MLINE", "mline"          // taskid, dllName
    }



#define	    CT_NONE	    0
#define	    CT_MAIN	    1
#define	    CT_PLACE	    2
#define	    CT_MLINE	    3

Table	CT_MAIN =
{ 
    { 1,  CT_PLACE,	PLACEMENT,   REQ,	"PLACE"     }, 
};

Table	CT_PLACE =
{ 
    { 1,  CT_MLINE,	INHERIT,     REQ,	"BLOCK"     },
};

Table	CT_MLINE =
{ 
    { 1,  CT_NONE,	INHERIT,     REQ,	"MLINE"     },
};


