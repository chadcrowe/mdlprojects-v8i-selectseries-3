/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mline/english/mlinemsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mline/english/mlinemsg.r_v  $
|   $Workfile:   mlinemsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:34 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MLine application messages					|
|									|
+----------------------------------------------------------------------*/
#include    <dlogbox.h>
#include    <dlogids.h>

#include    "mlineids.h"

MessageList STRINGID_Messages =
{
    {
    { MSGID_Null,		"" },
    { MSGID_PlaceMultiLine,	"Place Multi-line Block" },
    { MSGID_EnterFirstPoint,	"Enter first point" },
    { MSGID_EnterCorner,	"Enter opposite corner" },
    { MSGID_CommandTableError,	"Unable to load command table"},
    }
};

