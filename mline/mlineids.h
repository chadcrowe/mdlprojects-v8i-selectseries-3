/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mline/mlineids.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Logfile:   J:/mdl/examples/mline/mlineids.h_v  $
|   $Workfile:   mlineids.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:40:31 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	MLine application definitions					|
|									|
+----------------------------------------------------------------------*/
#if !defined (__mlineidsH__)
#define	__mlineidsH__

#define	STRINGID_Messages	    0

#define	MSGID_Null		    0
#define	MSGID_PlaceMultiLine	    1
#define	MSGID_EnterFirstPoint	    2
#define	MSGID_EnterCorner	    3
#define MSGID_CommandTableError	    4

#endif /* if !defined (__mlineidsH__) */

