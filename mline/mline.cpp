/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/mline/mline.cpp,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   $Workfile:   mline.mc  $
|   $Revision: 1.1.32.1 $
|   	$Date: 2013/07/01 20:40:21 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
| This example MDL application adds one new command to MicroStation.	|
|									|
| PLACE BLOCK MLINE - Place Multi-line block.				|
|    	    	      Command operation is the same as the standard	|
|    	    	      PLACE BLOCK command.				|
|		      The Multi-line produced uses the active		|
|		      multi-line definition.				|
|									|
| To use this application:						|
|    1. Compile the application  ->  bmake mline.mke			|
|    2. Start MicroStation						|
|    3. Load the application	 ->  keyin MDL LOAD MLINE		|
|    4. Use the new command listed above				|
|									|
| Note:	    	    	    	    	    	    	    	    	|
|									|
|    If multiline compatibility is ON (SET COMPATIBLE MLINE ON),	|
|    multi-lines will be saved as MicroStation 3.x (IGDS 8.8) compatible|
|    primitive elements.						|
|									|
|	- - - - - - - - - - - - - - - - - - - - - - - - - - - - -	|
|									|
|   Public Routine Summary -						|
|									|
|	main - main entry point						|
|	mline_blockFirstPoint - First data point function		|
|	mline_blockLastPoint - Last data point function			|
|	mline_blockComplete - Process multi-line placement in dgn file	|
|	mline_generateBlock - Multi-line generation/dynamics function	|
|	mline_blockStart - Place multi-line command function		|
|	mline_addBreaks - insert breaks into multi-line segments	|
|	mline_writeMline - write to dgn file				|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Include Files   							|
|									|
+----------------------------------------------------------------------*/
#include <mdl.h>	
#include <global.h>
#include <mselems.h>
#include <userfnc.h>
#include <rscdefs.h>

#include "mlineids.h"
#include "mlinecmd.h"

#include <mselemen.fdf>
#include <msmline.fdf>
#include <msrsrc.fdf>
#include <msparse.fdf>
#include <msstate.fdf>
#include <msvec.fdf>
#include <msoutput.fdf>
#include <mselmdsc.fdf>
#include <msmisc.fdf>
#include <mscurrtr.fdf>
#include <msvar.fdf>
#include <mssystem.fdf>
/*----------------------------------------------------------------------+
|									|
|   Local function declarations 					|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| ** PLACE BLOCK MLINE command						|
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
| name      mline_writeMline						|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void mline_writeMline
(
MSElementUnion *mline
)
    {
    MSElementDescr *edP;
    int     	   compatMode;

    mdlParams_getActive (&compatMode, ACTIVEPARAM_MLINECOMPAT);

    /*-------------------------------------------------------------------
    Check the setting of the current multiline compatibility mode.
    If multiline compatibility is ON, write the multiline to the file
    as IGDS 8.8 compatible primitives.
    If multiline compatibility is OFF, validate the multiline element
    and add it to the file.
    -------------------------------------------------------------------*/
    if (compatMode)
	{
    	if (mdlMline_getElementDescr (&edP, mline, MASTERFILE, TRUE))
	    return;

	mdlElmdscr_add (edP);
	mdlElmdscr_freeAll (&edP);
	}
    else
	{
    	if (mdlMline_validate (mline))
	    return;

    	mdlElement_add  (mline);
	}
    }

#ifdef ADD_BREAKS
/*----------------------------------------------------------------------+
|									|
| name      mline_addBreaks						|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void mline_addBreaks
(
MSElementUnion *mline
)
    {
    DPoint3d dPoints[2];
    double   segLength;
    int      i, nPoints;

    /*-------------------------------------------------------------------
    Insert a break into each segment of the multi-line. (done only for
    an example of inserting breaks)
    -------------------------------------------------------------------*/
    mdlMline_getInfo (&nPoints, NULL, NULL, NULL, NULL, mline);

    for (i=0; i<nPoints-1; i++)
	{
	mdlMline_extractPoints (dPoints, mline, MASTERFILE, i, 2);
	segLength = mdlVec_distance (dPoints, dPoints+1);
	mdlMline_insertBreak (mline, i, segLength/4.0, segLength/2.0,
			      0xff, 0);
	}
    }
#endif

/*----------------------------------------------------------------------+
|									|
| name      mline_generateBlock						|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void mline_generateBlock
(
Dpoint3d  *dataPoint,	     /* => Cursor location		       */
int	  view,		     /* => view (not used)		       */
int	  drawMode,	     /* => Current draw / erase mode	       */
int    	  writeMode	     /* => If TRUE, add elements to file       */
)
    {
    MSElementUnion  mline;
    DPoint3d	    dPoints[5];

    /*-------------------------------------------------------------------
    Define 5 points for the multiline based on the two corners entered.
    Note: The last point must be a repeat of the first point in order
          for the multiline to be closed.
    -------------------------------------------------------------------*/
    dPoints[0] = dPoints[1] = dPoints[3] = dPoints[4] =
						    statedata.dPointStack[0];
    dPoints[2] = *dataPoint;

    dPoints[1].x = dPoints[2].x;
    dPoints[3].y = dPoints[2].y;

    if (mdlVec_pointEqualUOR (dPoints, dPoints+1) ||
    	mdlVec_pointEqualUOR (dPoints, dPoints+3))
	return;

    if (mdlMline_create (&mline, NULL, NULL, dPoints, 5))
	return;

    mdlMline_setClosure (&mline, TRUE);

#ifdef ADD_BREAKS
    mline_addBreaks (&mline);
#endif

    mdlElement_display (&mline, (MstnDrawMode)drawMode);

    if (writeMode)
    	mline_writeMline  (&mline);
    }

/*----------------------------------------------------------------------+
|									|
| name      mline_blockLastPoint					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void mline_blockLastPoint
(
DPoint3d *dataPoint	     /* => Cursor location		       */
)
    {
    mline_generateBlock (dataPoint, 0, NORMALDRAW, TRUE);

    mdlState_restartCurrentCommand ();
    }

/*----------------------------------------------------------------------+
|									|
| name      mline_blockFirstPoint					|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void mline_blockFirstPoint
(
Dpoint3d *dataPoint,	     /* => Line start point (origin)	      */
int      view
)
    {
    /*-------------------------------------------------------------------
    Set the current transform to the view of the first data point.
    Then transform the first data point from world coordinates to
    the current transform.
    -------------------------------------------------------------------*/
    mdlCurrTrans_rotateByView (view);
    mdlCurrTrans_invtransPointArray (statedata.dPointStack, dataPoint, 1);

    /*-------------------------------------------------------------------
    Set dynamic and data point functions.
    -------------------------------------------------------------------*/
    mdlState_setFunction (STATE_DATAPOINT, mline_blockLastPoint); 
    mdlState_setFunction (STATE_COMPLEX_DYNAMICS, mline_generateBlock); 

    mdlOutput_rscPrintf (MSG_PROMPT, NULL, STRINGID_Messages,
			 MSGID_EnterCorner);
    }

/*----------------------------------------------------------------------+
|									|
| name      mline_blockStart - called for PLACE BLOCK MLINE keyin	|
|									|
| author    BSI						11/90		|
|									|
+----------------------------------------------------------------------*/
Private void mline_blockStart
(
char    *unparsedP
)
//cmdNumber	CMD_PLACE_BLOCK_MLINE
    {
    MSElementDescr  *pElmdscr = NULL;
    int		    status;

    status = mdlMlineStyle_activeToElement (&pElmdscr);

    mdlState_startPrimitive
	(mline_blockFirstPoint,		/* Data point function		*/
	 mline_blockStart,		/* Reset function		*/
	 MSGID_PlaceMultiLine,		/* Tool / function name		*/
	 MSGID_EnterFirstPoint);	/* Prompt number		*/

    /*-------------------------------------------------------------------
    Set the current transform to identity so that the point passed to
    mline_blockFirstPoint is in a known coordinate system (world coordinates)
    -------------------------------------------------------------------*/
    mdlCurrTrans_identity ();
    }

/*----------------------------------------------------------------------+
|									|
| name		main							|
|									|
| author	BSI					3/90		|
|									|
+----------------------------------------------------------------------*/
extern "C" DLLEXPORT  int MdlMain
(
int         argc,
char        *argv[]
)
    {
    RscFileHandle   rfHandle;

    mdlResource_openFile (&rfHandle, NULL, FALSE);

    /*-------------------------------------------------------------------
    Load the application command table and resources
    -------------------------------------------------------------------*/
    static MdlCommandNumber cmdNumbers[] =
    {
    {mline_blockStart,CMD_PLACE_BLOCK_MLINE},
    0,
    };

    mdlSystem_registerCommandNumbers (cmdNumbers);

    if (mdlParse_loadCommandTable (NULL) == NULL)
	mdlOutput_rscPrintf (MSG_ERROR, NULL, 0, 4);

    mdlCurrTrans_begin ();

    return  SUCCESS;
    }

