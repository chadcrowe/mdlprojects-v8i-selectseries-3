/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/autoLoad/autoLoad.h,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/autoLoad/autoLoad.h_v  $
|   $Workfile:   autoLoad.h  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	Constants & types used in autoLoad dialog example			|
|									|
+----------------------------------------------------------------------*/
#ifndef	    __autoLoadH__
#define	    __autoLoadH__

/*----------------------------------------------------------------------+
|									|
|   Resource ID's							|
|									|
+----------------------------------------------------------------------*/
#define	DIALOGID_autoLoad		1   /* dialog id for autoLoad Dialog */
#define DIALOGID_autoLoadModal	2   /* dialog id for autoLoad Modal Dialog */

#define OPTIONBUTTONID_autoLoad	1   /* id for "parameter 1" option button */
#define PUSHBUTTONID_OModal	1   /* id for "Open Modal" push button */
#define	TEXTID_autoLoad		1   /* id for "parameter 1" text item */
#define TOGGLEID_autoLoad		1   /* id for "Inc parameter 1?" toggle */

#define SYNONYMID_autoLoad		1   /* id for synonym resource */

#define MESSAGELISTID_autoLoadErrors   1	/* id for errors message list */

/*----------------------------------------------------------------------+
|									|
|   Error Message ID Definitions					|
|									|
+----------------------------------------------------------------------*/
#define	ERRID_CommandTable	1
#define	ERRID_autoLoadDialog	2
#define	ERRID_ModalDialog	3

/*----------------------------------------------------------------------+
|									|
|   Hook Function ID's							|
|									|
+----------------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_autoLoad	1   /* id for toggle item hook func */
#define HOOKDIALOGID_autoLoad		2   /* id for dialog hook func */

/*----------------------------------------------------------------------+
|									|
|   Typedefs								|
|									|
+----------------------------------------------------------------------*/
typedef struct autoLoadglobals
    {
    int	parameter1;	    /* used by text & option button item */
    int	parameter2;	    /* used by toggle button item */
    } autoLoadGlobals;

#endif
