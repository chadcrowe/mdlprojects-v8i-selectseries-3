/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/autoLoad/autoLoadtyp.mt,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/autoLoad/autoLoadtyp.mtv  $
|   $Workfile:   autoLoadtyp.mt  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:39 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	autoLoad Example Application Dialog Box Published Structures	|
|									|
+----------------------------------------------------------------------*/
#include    "autoLoad.h"

publishStructures (autoLoadglobals);

    
