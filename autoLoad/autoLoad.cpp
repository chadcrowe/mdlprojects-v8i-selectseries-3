
#include <mdl.h>	    /* MDL Library funcs structures & constants */
#include "autoLoad.h"	    /* autoLoad dialog box example constants & structs */
#include <autoLoadcmd.h>    /* autoLoad dialog box command numbers */
#include <msvba.fdf>
#include <mssystem.fdf>
#include <msdialog.fdf>
#include <cmdlist.h>
void  OnNewDesignFile(char  *filenameP, int  state);

extern "C"  DLLEXPORT int   MdlMain
(){
	mdlSystem_setFunction(SYSTEM_NEW_DESIGN_FILE, OnNewDesignFile);


	mdlDialog_cmdNumberQueue(FALSE, CMD_MDL_UNLOAD, mdlSystem_getCurrTaskID(), TRUE);
	return SUCCESS;

}

void  OnNewDesignFile(char  *filenameP, int  state)
{
	if (SYSTEM_NEWFILE_COMPLETE == state)
	{
		mdlVBA_loadProject("C://Users//ccrowe//Desktop//6_12_15.mvba");
		//  It's safe to do things with the newly-opened file
	}
}
