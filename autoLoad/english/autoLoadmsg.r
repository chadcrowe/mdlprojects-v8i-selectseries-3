/*--------------------------------------------------------------------------------------+
|
|     $Source: /miscdev-root/miscdev/mdl/examples/autoLoad/english/autoLoadmsg.r,v $
|
|  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
|
+--------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|    $Logfile:   J:/mdl/examples/autoLoad/english/autoLoadmsg.r_v  $
|   $Workfile:   autoLoadmsg.r  $
|   $Revision: 1.2.76.1 $
|   	$Date: 2013/07/01 20:35:40 $
|									|
+----------------------------------------------------------------------*/
/*----------------------------------------------------------------------+
|									|
|   Function -								|
|									|
|	autoLoad application message string resources			|
|									|
+----------------------------------------------------------------------*/
#include <dlogbox.h>	/* dlog box manager resource constants & structs */
#include <dlogids.h>	/* MicroStation resource IDs */

#include "autoLoad.h"	/* autoLoad dialog box example constants & structs */

/*----------------------------------------------------------------------+
|									|
|   Error Messages							|
|									|
+----------------------------------------------------------------------*/
MessageList MESSAGELISTID_autoLoadErrors =
    {
      {
      {ERRID_CommandTable,  "Unable to load command table."},
      {ERRID_autoLoadDialog,   "Unable to open autoLoad dialog box."},
      {ERRID_ModalDialog,   "Unable to open modal dialog box."},
      }
    };
